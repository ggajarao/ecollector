package ebilly.net;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.entity.GzipDecompressingEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HttpContext;

/**
 * Key features.
 * All HTTP connections are gziped
 * Auto retry - internal apache HttpClient
 * 
 * http://hc.apache.org/httpcomponents-client-ga/tutorial/html/fundamentals.html#d4e280
 * 
 * @author Vamsidhar kadiyal
 *
 */
public class HttpConnector {
	
	private DefaultHttpClient mHttpClient;
	public HttpConnector() {
		
	}
	
	/**
	 * Http request is made and response is returned
	 * 
	 * @param url
	 * @param params
	 * @return
	 * @throws IOException
	 */
	public ebilly.net.HttpResponse connect(String url, Map<String, String> params) 
	throws IOException {
		HttpResponse response = executeHttpRequest(url,params,null);
		return returnConnectorResponse(response);
	}
	
	private ebilly.net.HttpResponse returnConnectorResponse(
			HttpResponse response) throws IOException {
		InputStream is = null;
		try {
			ebilly.net.HttpResponse r = new ebilly.net.HttpResponse();
			is = response.getEntity().getContent();
			r.message = convertToString(is);
			r.statusCode = "" + response.getStatusLine().getStatusCode();
			return r;
		} finally {
			if (is != null) is.close();
		}
	}

	/**
	 * Http request is made and response is returned
	 * 
	 * @param url
	 * @param params
	 * @return
	 * @throws IOException
	 */
	public ebilly.net.HttpResponse connect(String url, Map<String, String> params, Map<String,String> headers) 
	throws IOException {
		HttpResponse response = executeHttpRequest(url,params,headers);
		return returnConnectorResponse(response);
	}
	
	public ebilly.net.HttpResponse postData(String url, String requestData, Map<String,String> headers) 
	throws IOException {
		HttpResponse response = executeHttpPostRequest(url,requestData,headers);
		return returnConnectorResponse(response);
	}
	
	/**
	 * Returns path to the downladed file
	 * 
	 * @param url
	 * @param params
	 * @param downloadDir
	 * @return
	 * @throws IOException
	 */
	public String downloadFile(String url, Map<String,String> params, String downloadDir, 
			DataTransferProgress dtp) 
	throws IOException {
		InputStream is = null;
		try {
			HttpResponse response = executeHttpRequest(url,params,null);
			String fileName = "NO_NAME_"+System.currentTimeMillis();
			if (response.containsHeader("Content-Disposition")) {
				Header[] headers = response.getHeaders("Content-Disposition");
				String v = headers[0].getValue();
				int i = v.indexOf("inline;filename=");
				if (i >= 0) {
					fileName = v.substring("inline;filename=".length());
				}
			}
			HttpEntity hEntity = response.getEntity();
			is = hEntity.getContent();
			dtp.transferBegan(hEntity.getContentLength());
			return writeToFile(is,downloadDir, fileName, dtp);
		} finally {
			if (is != null) is.close();
		}
	}
	
	private String writeToFile(InputStream is, String downloadDir, String fileName,
			DataTransferProgress dtp) 
	throws IOException {
		File file = new File(downloadDir,fileName);
		FileOutputStream fos = new FileOutputStream(file);
		int totalBytes = 0;
		try {
			int l;
			byte[] tmp = new byte[2048];
			while ((l = is.read(tmp)) != -1) {
				fos.write(tmp, 0, l);
				dtp.progressBytes(l);
				totalBytes += l;
			}
		} finally {
			fos.close();
		}
		dtp.transferCompleted(totalBytes);
		return file.getPath();
	}

	private HttpResponse executeHttpRequest(String url,
			Map<String, String> params, final Map<String,String> headers) throws IOException {
		HttpPost hMethod = new HttpPost(url);
		try {
			List<NameValuePair> formparams = new ArrayList<NameValuePair>();
			if (params != null) {
				for (Entry<String, String> param : params.entrySet()) {
					formparams.add(new BasicNameValuePair(param.getKey(), param.getValue()));
				}
				UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formparams, "UTF-8");
				if (headers != null && headers.containsKey("Content-Type")) {
					entity.setContentType(headers.get("Content-Type"));
				}
				hMethod.setEntity(entity);
			}
			if (headers != null) {
            	for (Entry<String,String> entry : headers.entrySet()) {
            		hMethod.setHeader(entry.getKey(),entry.getValue());
            	}
            }
			mHttpClient = createHttpClient();
			return httpClientDo(mHttpClient,hMethod);
		} catch(IOException e) {
			throw e;
		} catch (RuntimeException e) {
			hMethod.abort();
			throw e;
		}
	}
	
	private HttpResponse httpClientDo(DefaultHttpClient mHttpClient, HttpPost hMethod) 
	throws IOException {
		HttpResponse response = mHttpClient.execute(hMethod);
		if (response.getStatusLine().getStatusCode() == 401) {// Unautorized
			 throw new UnAuthorizedAccessException("Unauthorized access");
		}	
		return response;
	}

	private HttpResponse executeHttpPostRequest(String url,
			String requestData, final Map<String,String> headers) 
	throws IOException {
		HttpPost hMethod = new HttpPost(url);
		try {
			if (requestData != null) {
				StringEntity entity = new StringEntity(requestData,"UTF-8");
				if (headers != null && headers.containsKey("Content-Type")) {
					entity.setContentType(headers.get("Content-Type"));
				}
				hMethod.setEntity(entity);
			}
			if (headers != null) {
            	for (Entry<String,String> entry : headers.entrySet()) {
            		hMethod.setHeader(entry.getKey(),entry.getValue());
            	}
            }
			mHttpClient = createHttpClient();
			return httpClientDo(mHttpClient, hMethod);
		} catch(IOException e) {
			throw e;
		} catch (RuntimeException e) {
			hMethod.abort();
			throw e;
		}
	}
	
	private synchronized DefaultHttpClient createHttpClient() {
		if (mHttpClient != null) {
			return mHttpClient;
		}
		
		mHttpClient = new DefaultHttpClient();
		//Ref: http://stackoverflow.com/questions/6024376/apache-commons-httpcomponents-httpclient-timeout
		// Read timeout
		mHttpClient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 45 * 1000);
		// connection timeout
		mHttpClient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 10 * 1000);
		
		mHttpClient.addRequestInterceptor(new HttpRequestInterceptor() {
			public void process(
                    final HttpRequest request,
                    final HttpContext context) throws HttpException, IOException {
                if (!request.containsHeader("Accept-Encoding")) {
                    request.setHeader("Accept-Encoding", "gzip");
                    request.setHeader("User-Agent", "gzip");
                }
            }
        });
		
		mHttpClient.addResponseInterceptor(new HttpResponseInterceptor() {
            public void process(
                    final HttpResponse response,
                    final HttpContext context) throws HttpException, IOException {
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    Header ceheader = entity.getContentEncoding();
                    if (ceheader != null) {
                        HeaderElement[] codecs = ceheader.getElements();
                        for (int i = 0; i < codecs.length; i++) {
                            if (codecs[i].getName().equalsIgnoreCase("gzip")) {
                            	response.setEntity(
                                        new GzipDecompressingEntity(response.getEntity()));
                                return;
                            }
                        }
                    }
                }
            }

        });
		return mHttpClient;
	}
	
	public synchronized void releaseResources() {
		if (mHttpClient != null) {
			mHttpClient.getConnectionManager().shutdown();
			mHttpClient = null;
		}
	}

	private static String convertToString(InputStream instream) 
	throws IllegalStateException, IOException
	{
		StringBuffer responseString = new StringBuffer();
		int l;
		byte[] tmp = new byte[2048];
		while ((l = instream.read(tmp)) != -1) {
			responseString.append(new String(tmp,0,l));  
		}
		return responseString.toString().trim();
	}
	
	public ebilly.net.HttpResponse uploadFile(String fileName, String uploadUrl, 
			DataTransferProgress dtp) throws ParseException, IOException {
		mHttpClient = createHttpClient();
		File file = new File(fileName);
		HttpParams params = new BasicHttpParams();
	    params.setParameter(HttpProtocolParams.USE_EXPECT_CONTINUE, true);
	    HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_0);
	    HttpPost postMethod = new HttpPost(uploadUrl);

	    MultiPartEntityDecorater entity = new MultiPartEntityDecorater(dtp);
	    entity.addPart("file", new FileBody(file));
	    postMethod.setEntity(entity);

	    HttpResponse response = mHttpClient.execute(postMethod);
	    return returnConnectorResponse(response);
	}
	
	public static void main(String[] args) throws IOException {
		HttpConnector hc = new HttpConnector();
		Map<String,String> params = new HashMap<String,String>();
		/*params.put("a", "r");
		params.put("bk", "AMIfv978kpUVY2Ba3QtPCvHmmmxTq2ZBG69-fQv1Ryq06C4fChGyFRBA0s8xInY4ftwPZ2TQwQyc40kknzqD4gfNouwB6zl86JTozpjfZYupbqfP1MXgVwv4sR3215cxYSjYzblQWVJ3f3KFA5Vk-SK6vW1tB9QWtueVwOagsluaXpFr7gbEXEQ");
		hc.downloadFile("http://1-ecollector.appspot.com/cloudapp/serveFile",params,"d:\\");*/
		//params.put("bk", "EPczcf7Z8YntgcdH0HD32g");
		//hc.downloadFile("http://localhost:8888/cloudapp/serveFile",params,"d:\\");
		
		params.put("a", "P");
		params.put("1", "VAMSI");
		hc.connect("http://1-ecollector.appspot.com/cloudapp/ps",params);
	}	
}
