package ebilly.net;

public interface DataTransferProgress {

	void transferBegan(long contentLength);

	void progressBytes(int l);

	void transferCompleted(int totalBytes);

}
