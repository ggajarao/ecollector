package ebilly.net;

import java.io.IOException;

/**
 * Thrown when server responded with status 401 - Unauthorized access code
 * 
 */
public class UnAuthorizedAccessException extends IOException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UnAuthorizedAccessException() {
		super();
	}
	
	public UnAuthorizedAccessException(String message) {
		super(message);
	}
	
	public UnAuthorizedAccessException(String message, Throwable t) {
		super(message, t);
	}
}
