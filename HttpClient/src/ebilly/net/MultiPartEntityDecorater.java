package ebilly.net;

import java.io.IOException;
import java.io.OutputStream;

public class MultiPartEntityDecorater extends  org.apache.http.entity.mime.MultipartEntity {

    private OutputStreamProgress outstream;
    private DataTransferProgress dtp;
    public MultiPartEntityDecorater(DataTransferProgress dtp) {
    	super();
    	this.dtp = dtp;
    }

    @Override
    public void writeTo(OutputStream outstream) throws IOException {
    	dtp.transferBegan(getContentLength());
        this.outstream = new OutputStreamProgress(outstream, dtp);
        super.writeTo(this.outstream);
        dtp.transferCompleted(-1);
    }

    /**
     * Progress: 0-100
     */
    public int getProgress() {
        if (outstream == null) {
            return 0;
        }
        long contentLength = getContentLength();
        if (contentLength <= 0) { // Prevent division by zero and negative values
            return 0;
        }
        long writtenLength = outstream.getWrittenLength();
        return (int) (100*writtenLength/contentLength);
    }
    
    public boolean isStreaming() {
    	return false;
    }
}
