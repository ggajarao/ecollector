Follow these steps to sign the applet jar

keytool -genkey -keystore vamsisKeyStore -alias Vamsi

keytool -selfcert -keystore vamsisKeyStore -alias Vamsi -validity 10950

jarsigner -keystore vamsisKeyStore jFileUpload.jar Vamsi

jarsigner -keystore vamsisKeyStore lib/ebilly-httpClient.jar Vamsi
jarsigner -keystore vamsisKeyStore lib/commons-logging-1.1.1.jar Vamsi
jarsigner -keystore vamsisKeyStore lib/httpclient-4.2.2.jar Vamsi
jarsigner -keystore vamsisKeyStore lib/httpcore-4.2.2.jar Vamsi
jarsigner -keystore vamsisKeyStore lib/httpmime-4.2.2.jar Vamsi


to verify expiry of the signed jar
----------------------------
jarsigner -verbose -certs -verify jFileUpload.jar