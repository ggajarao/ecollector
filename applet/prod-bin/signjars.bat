echo off

jarsigner -keystore vamsisKeyStore jFileUpload.jar Vamsi

REM Uncomment below to sign them.
REM jarsigner -keystore vamsisKeyStore lib/ebilly-httpClient.jar Vamsi
REM jarsigner -keystore vamsisKeyStore lib/commons-logging-1.1.1.jar Vamsi
REM jarsigner -keystore vamsisKeyStore lib/httpclient-4.2.2.jar Vamsi
REM jarsigner -keystore vamsisKeyStore lib/httpcore-4.2.2.jar Vamsi
REM jarsigner -keystore vamsisKeyStore lib/httpmime-4.2.2.jar Vamsi
