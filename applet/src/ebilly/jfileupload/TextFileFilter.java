package ebilly.jfileupload;

import java.io.File;

import javax.swing.filechooser.FileFilter;

public class TextFileFilter extends FileFilter {
	
	public TextFileFilter() {
		
	}
	
	@Override
	public boolean accept(File file) {
		if (file == null) return false;
		if (file.isDirectory()) return true;
		if (file.getName().endsWith(".txt")) return true;
		if (file.getName().endsWith(".TXT")) return true;
		return false;
	}

	@Override
	public String getDescription() {
		return "Text Files";
	}

}
