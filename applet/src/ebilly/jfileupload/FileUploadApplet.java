/*******************************************************************************
 * EcollectoR CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] EcollectoR 
 *  proprietors Vamsidhar Kadiyala, Ravindra Reddy Araga
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of EcollectoR Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to EcollectoR Systems Incorporated
 * and its suppliers and may be covered by Indian and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from EcollectoR Systems Incorporated.
 ******************************************************************************/
package ebilly.jfileupload;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.Permission;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JPanel;

import netscape.javascript.JSObject;
import ebilly.net.DataTransferProgress;
import ebilly.net.HttpConnector;
import ebilly.net.HttpResponse;

public class FileUploadApplet extends JApplet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//private JLabel mLabel;
	private JComponent mRootPane;
	private JFileChooser mFileChooser;
	private JButton mSelectFile;
	
	private String mUploadUrl;
	
	private JSObject mJsObject;
	
	private String mJsNotifyListener;
	
	@Override
	public void init() {
		super.init();
	}
	
	@Override
	public void start() {
		super.start();
		
		/*
		 // Does not work.
		 try {
			log("Attempting to set custom Security manager set.");
			MySecurityManager msm = new MySecurityManager();
			System.setSecurityManager(msm);
			log("Security manager set.");
		} catch (Throwable t) {
			t.printStackTrace();
		}*/
		
		loadProperties();
		
		FlowLayout flowLayout = new FlowLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		mRootPane = new JPanel(flowLayout);
		//mRootPane.setSize(new Dimension(200,50));
		mRootPane.setBackground(Color.WHITE);
		
		//mLabel = new JLabel("No File Selected...");
		mSelectFile = new JButton("Select File");
		mSelectFile.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				selectFile();
			}			
		});
		mRootPane.add(mSelectFile);
		//mRootPane.add(mLabel);
		this.getContentPane().add(mRootPane);
		notifyAppletLoaded();
	}
	
	private JSObject getJsObject() {
		if (mJsObject == null) {
			try {
				mJsObject = JSObject.getWindow(this);
			} catch (Throwable t) {
				t.printStackTrace();
				sendMessageToJs(t.getMessage());
			}
		}
		return mJsObject;
	}
	
	public void setJsNotifyListener(String jsMethod) {
		mJsNotifyListener = jsMethod;
	}
	
	private void sendMessageToJs(final String message) {
		System.out.println(message);
		Runnable r = new Runnable() {
			public void run() {
				log(message);
				if (getJsObject() != null && message != null) {
					mJsObject.setMember("jfileuploadMessage", message);
					mJsObject.eval("jfileuploadRecieveMessages()");
				}
			};
		};
		Thread t = new Thread(r);
		t.start();
	}
	
	private void notifyJsListeners(final String response) {
		Runnable r = new Runnable() {
			public void run() {
				if (getJsObject() != null && mJsNotifyListener != null) {
					mJsObject.setMember("jfileuploadResponse", response);
					mJsObject.eval(mJsNotifyListener+"()");
				}
			};
		};
		Thread t = new Thread(r);
		t.start();
	}
	
	private void notifyAppletLoaded() {
		Runnable r = new Runnable() {
			public void run() {
				if (getJsObject() != null) {
					mJsObject.eval("jfileuploadLoaded()");
				}
			};
		};
		Thread t = new Thread(r);
		t.start();
	}
	
	public void setSelectFileEnabled(boolean flag) {
		mSelectFile.setEnabled(flag);
	}
	
	public void setUploadUrl(String uploadUrl) {
		mUploadUrl = uploadUrl;
	}
	
	public void notifyFileSelected(final String selectedFile) {
		Runnable r = new Runnable() {
			@Override
			public void run() {
				if (getJsObject() != null) {
					mJsObject.setMember("jfileuploadSelectedFile", selectedFile);
					mJsObject.eval("jfileuploadFileSelected()");
				}
			}
		};
		Thread t = new Thread(r);
		t.start();
	}
	
	private void selectFile() {
		try {
			TextFileFilter filter = new TextFileFilter();
		    if (mFileChooser == null) {
				mFileChooser = new JFileChooser();
				mFileChooser.setCurrentDirectory(getLastVisitedDirectory());
				mFileChooser.setFileFilter(filter);
			}
		    int returnVal = mFileChooser.showOpenDialog(FileUploadApplet.this);
		    if(returnVal == JFileChooser.APPROVE_OPTION) {
		    	setLastVisitedDirectory(mFileChooser.getCurrentDirectory());
		    	if (!filter.accept(mFileChooser.getSelectedFile())) {
		    		sendMessageToJs("Only Text files are allowed, please select a .TXT file");
		    	} else {
		    		sendMessageToJs(mFileChooser.getSelectedFile().getPath());
		    		notifyFileSelected(mFileChooser.getSelectedFile().getPath());
		    		startUpload();
		    	}
		    }
		} catch (Throwable t) {
			sendMessageToJs(t.getMessage());
			t.printStackTrace();
		}
	}
	
	private static final String JFILE_UPLOAD_PREFERENCES_FILE = "jfileupload.props";
	private static final String LAST_VISITED_DIR = "lastVisitedDir";
	private Properties mProps = new Properties();
	private File getLastVisitedDirectory() {
		String dir = (String)mProps.get(LAST_VISITED_DIR);
		if (dir != null && dir.trim().length() != 0) {
			return new File(dir);
		} else {
			return new File(System.getProperty("user.home"));
		}
	}
	private void setLastVisitedDirectory(File f) {
		if (f == null) return;
		mProps.setProperty(LAST_VISITED_DIR, f.getPath());
	}

	
	private File mPrefFile = null;
	private File getPrefFile() {
		if (mPrefFile == null) {
			mPrefFile = new File(System.getProperty("user.home"),JFILE_UPLOAD_PREFERENCES_FILE);;
			//mPrefFile = new File("C:"+File.pathSeparator+JFILE_UPLOAD_PREFERENCES_FILE);
		}
		return mPrefFile;
	}
	private void loadProperties() {
		mProps = new Properties();
		InputStream is = null;
		try {
			File prefFile = getPrefFile();
			if (!prefFile.exists()) return;
			is = new FileInputStream(mPrefFile);
			mProps.load(is);
		} catch (Throwable t) {
			t.printStackTrace();
		} finally {
			if (is != null) {
				try{is.close();}catch(Exception e){};
			}
		}
	}
	
	private void saveProperties() {
		if (mProps == null) return;
		File prefFile = getPrefFile();
		OutputStream os = null;
		try {
			os = new FileOutputStream(prefFile);
			mProps.store(os,"jfileupload applet preference file");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (os != null) {
				try{os.close();}catch(Exception e){};
			}
		}
	}
	
	private void deleteFile(String filePath) {
		try {
			File f = new File(filePath);
			if (f.delete()) {
				log("temp file deleted, file: "+filePath);
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	
	private void log(String message) {
		System.out.println(message);
	}
	
	private String zipFile(File file) throws Exception {
		if (file == null) return null;
		File outFile = createOutFile(file);
		//File outFile = new File(file.getPath()+".zip");
		ZipOutputStream zos = null;
		FileInputStream fis = null;
		try {
			zos = new ZipOutputStream(new FileOutputStream(outFile));
			fis = new FileInputStream(file);
			
			zos.putNextEntry(new ZipEntry(file.getName()));
			int bytesRead = 0;
			byte[] buffer = new byte[1024];
			while ((bytesRead = fis.read(buffer)) != -1) {
				zos.write(buffer, 0, bytesRead);
			}
			zos.finish();
		} finally {
			if (zos != null) zos.close();
			if (fis != null) fis.close();
		}
		return outFile.getPath();
	}
	
	private File createOutFile(File file) {
		String userHome = System.getProperty("user.home");
		return new File(userHome,file.getName()+".zip");
	}
	
	private void startUpload() {
		Thread worker = new Thread(new Runnable(){
			public void run() {
				String zipFilePath = null;
				try {
					sendMessageToJs("Zipping file, please wait...");
					zipFilePath = zipFile(mFileChooser.getSelectedFile());
					sendMessageToJs("Uploading file, please wait...");
					log("Url "+mUploadUrl);
					//mUploadUrl = "http://stage-ecollector.appspot.com/_ah/upload/AMmfu6Zaw301w0dpXqDb9tQq1_z5OsEcMSbFeHHX2QfzEGi8svjGDX3XhK7JvoX4AVx98kLt4AejxJxCFyMNYAUGauC1fWITstP6TFqHN_bf7kV1UbyYlvP2wiQo3ILWQlT5hUnR5q5dLjy8DskMLb6wnWT4lre5r-vikCgBhrhh-UZCWZvYiEw/ALBNUaYAAAAAUWdfqxqF-d5buFrUp2Ox7Gk0NF4D7zYo/";
					String serverResponse = uploadFile(mUploadUrl,zipFilePath);
					//sendMessageToJs("Uploading file Successful");
					//mLabel.setText(serverResponse);
					sendMessageToJs("Uploading file completed");
					notifyJsListeners(serverResponse);
				} catch (Throwable t) {
					sendMessageToJs(t.getMessage());
					t.printStackTrace();
				} finally {
					deleteFile(zipFilePath);
				}
			}
		});
		worker.start();
	}
	
	// Ref: http://stackoverflow.com/questions/1599018/java-applet-to-upload-a-file
	@SuppressWarnings("unused")
	private String uploadFileOld(String uploadUrl, String file) throws Exception {
		File f = new File(file);
		int i = 0;
		final String BOUNDARY = "--7d021a37605f0";
		final String BOUNDARY_START = 
			"--" + BOUNDARY + "\r\n"
			+ "Content-Disposition: form-data; name=\"file" + i + "\"; filename=\"" + f.getName() + "\"\r\n"
			+ "Content-Type: application/zip\r\n"
			+ "\r\n";
		final String BOUNDARY_END_MARK = "--" + BOUNDARY + "--\r\n";
		
		log("Upload URL: "+uploadUrl);
		URL url = new URL(uploadUrl);
		
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		log("Http Connection opened");
		//con.setInstanceFollowRedirects(false);
		log("Follow redirects? "+con.getInstanceFollowRedirects());
		con.setDoOutput(true);
		con.setDoInput(true);
		con.setUseCaches(false);
		//con.setChunkedStreamingMode(1024);
		con.setRequestProperty("Content-Type", "multipart/form-data; boundary="
				+ BOUNDARY);
		long contentLenght = getContentLenght(f);
		con.setRequestProperty("Content-Length", contentLenght+"");
		
		DataOutputStream httpOut = null;
		FileInputStream uploadFileReader = null;
		int numBytesToRead = 1024;
		try {
			httpOut = new DataOutputStream(con.getOutputStream());
			uploadFileReader = new FileInputStream(f);
			httpOut.write(BOUNDARY_START.getBytes("UTF-8"));
			int availableBytesToRead;
			int cumulativeRead = 0;
			while ((availableBytesToRead = uploadFileReader.available()) > 0)
			{
				byte[] bufferBytesRead;
				bufferBytesRead = 
					availableBytesToRead >= numBytesToRead ? 
							new byte[numBytesToRead]
							         : new byte[availableBytesToRead];
				uploadFileReader.read(bufferBytesRead);
				cumulativeRead += bufferBytesRead.length;
				httpOut.write(bufferBytesRead);
				httpOut.flush();
				String progress = "Uploading..."+percentage(cumulativeRead, contentLenght)+"%";
				sendMessageToJs(progress);
				log(progress);
			}
			httpOut.write((BOUNDARY_END_MARK).getBytes("UTF-8"));
			httpOut.write((BOUNDARY_END_MARK).getBytes("UTF-8"));
			httpOut.flush();
			//httpOut.close();
		} finally {
			if (uploadFileReader != null) uploadFileReader.close();
			if (httpOut != null) httpOut.close();
		}
		
		int respCode = con.getResponseCode();
		log("Response code: "+respCode);
		if (respCode != HttpURLConnection.HTTP_OK) {
			return respCode+"";
		}

		sendMessageToJs("Connected... reading response");
		// read & parse the response
		InputStream is = null;
		try {
			is = con.getInputStream();
			StringBuilder response = new StringBuilder();
			byte[] respBuffer = new byte[4096];
			while (is.read(respBuffer) >= 0)
			{
				response.append(new String(respBuffer).trim());
			}
			log("response reading completed, response: "+response.toString());
			return response.toString();
		} finally {
			if (is != null) is.close();
		}
	}
	
	long uploadFileContentLenght = 0;
	long cumulativeBytesUpload = 0;
	private String uploadFile(String uploadUrl, String fileName) throws Exception {
		uploadFileContentLenght = 0;
		cumulativeBytesUpload = 0;
		
		HttpConnector hc = new HttpConnector();
		
		DataTransferProgress dtp = new DataTransferProgress() {
			@Override
			public void progressBytes(int progress) {
				cumulativeBytesUpload += progress;
				String msg = "Uploading..."+percentage(cumulativeBytesUpload,uploadFileContentLenght)+"%";
				sendMessageToJs(msg);
			}
			@Override
			public void transferBegan(long contentLenght) {
				uploadFileContentLenght = contentLenght;
			}

			@Override
			public void transferCompleted(int arg0) {
				sendMessageToJs("Finishing...");
			}
		};
		HttpResponse uploadFile = hc.uploadFile(fileName, uploadUrl, dtp);
		return uploadFile.message;
	}
	
	private int percentage(long cumulativeRead, long contentLenght) {
		return (int)(((double)cumulativeRead/contentLenght)*100);
	}

	private long getContentLenght(File f) throws Exception {
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(f);
			byte[] buffer = new byte[1024];
			long totalBytes = 0;
			int bytesRead = 0;
			while ((bytesRead = fis.read(buffer)) != -1) {
				totalBytes += bytesRead;
			}
			return totalBytes;
		} finally {
			if (fis != null) fis.close();
		}
	}

	@Override
	public void stop() {
		super.stop();
		saveProperties();
	}
	
	@Override
	public void destroy() {
		super.destroy();
	}
}


class MySecurityManager extends SecurityManager {
    @Override
    public void checkPermission(Permission perm) {
        return;
    }
}