package ecollector.crypto;

import java.math.BigInteger;
import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class CryptoUtil {

	public static String generateKey() throws Exception {
		KeyGenerator keyGen = KeyGenerator.getInstance("AES");
		keyGen.init(128);
		Key key = keyGen.generateKey();
		byte[] encoded = key.getEncoded();
		String output = new BigInteger(1, encoded).toString(16);
		return output;
	}

	public static Key getStoredKey() throws Exception {
		String s = "2eb17fe666f0001176af694f8c66501c";// DO NOT MODIFY THIS
		byte[] encoded = new BigInteger(s, 16).toByteArray();
		return new SecretKeySpec(encoded, "AES");
	}

	public static String encode(String content) throws Exception {
		if (content == null)
			return content;
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE, getStoredKey());
		byte[] encValue = cipher.doFinal(content.getBytes());
        byte[] encryptedByteValue = new Base64().encode(encValue);
        String encryptedValue = new String(encryptedByteValue,"UTF8");
        return encryptedValue;
	}

	public static String decode(String encryptedContent)
			throws Exception {
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.DECRYPT_MODE, getStoredKey());
		byte[] decodedValue = new Base64().decode(encryptedContent.getBytes());
		byte[] decryptedVal = cipher.doFinal(decodedValue);
		return new String(decryptedVal,"UTF8");
	}

	public static void main(String[] args) throws Exception {
		/*
		 * for (int i = 0; i < 10; i++) { String key = generateKey();
		 * System.out.println("Key: ["+key+"]"); }
		 */

		for (int i = 9; i < 10; i++) {
			try {
				String key = generateKey();
				System.out.println("Key: [" + key + "]");
				String content = "vamsi.vamsi@gmail.com";
				System.out.println("content: " + content);
				String encrypted = encode(content);
				System.out.println("Encrypted: " + encrypted);
				String decrypted = decode(encrypted);
				System.out.println("Decrypted: " + decrypted);
				System.out.println("\n\n");
			} catch (Throwable t) {
				t.printStackTrace();
				System.out.println("\n\n");
			}
		}
	}

}
