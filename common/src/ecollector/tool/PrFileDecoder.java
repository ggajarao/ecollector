package ecollector.tool;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import ecollector.crypto.CryptoUtil;

public class PrFileDecoder {
	
	public static void main(String[] args) {
		
		JFileChooser fc = new JFileChooser();
		int returnVal = fc.showOpenDialog(new JFrame());
	    if(returnVal == JFileChooser.APPROVE_OPTION) {
	    	startDecoding(fc.getSelectedFile());
	    }
	    System.exit(0);
	}
	
	private static void startDecoding(File selectedFile) {
		BufferedReader r = null;
		BufferedWriter w = null;
		boolean success = false;
		File decodedFile = null;
		try {
			decodedFile = new File(selectedFile.getParentFile(),selectedFile.getName()+"-Decoded.txt");
			r = new BufferedReader(new FileReader(selectedFile));
			w = new BufferedWriter(new FileWriter(decodedFile));
			String line = null;
			while ((line = r.readLine()) != null) {
				w.write(CryptoUtil.decode(line)+"\n");
			}
			w.flush();
			success = true;
		} catch (Exception e) {
			JOptionPane.showConfirmDialog(new JFrame(),"Failed to decode, reason: "+e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				if (r != null) r.close();
				if (w != null) w.close();
			} catch (Exception ex){}
		}
		if (success) {
			JOptionPane.showMessageDialog(new JFrame(), "Decoded to file: "+decodedFile.getAbsolutePath());
		}
	}	
}
