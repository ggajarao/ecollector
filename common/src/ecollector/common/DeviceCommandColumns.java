package ecollector.common;

public class DeviceCommandColumns {
	public static final String _ID = "_id";
	public static final String COMMAND_ID = "COMMAND_ID";
	public static final String STATE = "STATE";
	public static final String COMMAND_DATE = "COMMAND_DATE";
	public static final String COMMANDCODE = "COMMANDCODE";
	public static final String COMMANDPARAM1 = "COMMANDPARAM1";
	public static final String COMMANDPARAM2 = "COMMANDPARAM2";
	public static final String COMMANDPARAM3 = "COMMANDPARAM3";
	public static final String EXECUTIONDATE = "EXECUTIONDATE";
	public static final String EXECUTIONNOTES = "EXECUTIONNOTES";
	public static final String COMMANDTIMESTAMP = "COMMANDTIMESTAMP";
}
