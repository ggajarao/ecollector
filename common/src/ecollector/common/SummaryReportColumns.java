package ecollector.common;

public class SummaryReportColumns {
	public static final String _ID = "_id";
	public static final String TOTAL_SERVICES_COUNT = "TOTAL_SERVICES_COUNT";
	public static final String TOTAL_PR_COUNT = "TOTAL_PR_COUNT";
	public static final String TOTAL_PR_TO_PUSH_COUNT = "TOTAL_PR_TO_PUSH_COUNT";
	public static final String TOTAL_COLLECTION = "TOTAL_COLLECTION";
}

