package ecollector.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipInputStream;

/**
 * Reads fiexed field lenght text file and 
 * provides record data as array of fields
 * 
 */
public class DeviceFileReader {
	
	private int recLenght;
	private File parentDir;
	private String fileName;
	private String fileToImport;
	private int numLines;
	private BufferedReader reader = null;
	private String currentLine;
	private DeviceFileReader() {
		recLenght = 0;
		for (String fieldName : ServiceColumns.IMPORT_COLUMN_ORDER) {
			recLenght += ServiceColumns.FL_MAP.get(fieldName).fieldLenght.intValue();
		}
		System.out.println("Record lenght: "+recLenght);
	}
	
	public DeviceFileReader(String fileToImport) throws IOException {
		this();
		this.fileToImport = fileToImport;
		initNumberOfLines();
		initFileHandlers();
	}
	
	public DeviceFileReader(File parentDir, String fileName) throws IOException {
		this();
		this.parentDir = parentDir;
		this.fileName = fileName;
		initNumberOfLines();
		initFileHandlers();
	}
	
	public DeviceFileReader(BufferedReader reader) {
		this();
		this.reader = reader;
	}
	
	public int getNumberOfLines() {
		return this.numLines;		
	}
	
	private void initFileHandlers() throws IOException {
		if ((parentDir == null || fileName == null) && fileToImport == null) {
			throw new IllegalArgumentException("Line numbers not available, please use DeviceFileReader(parentDir,fileName) or DeviceFileReader(File) version of constructor");
		}
		
		File dataFile = null;
		if (parentDir != null && fileName != null) {
			dataFile = new File(parentDir,fileName);
		}  else {
			dataFile = new File(fileToImport);
		}
		
		FileReader fr = new FileReader(dataFile);
		
		String fn = dataFile.getName().toUpperCase();
		if (fn.endsWith(".ZIP")) {
			ZipInputStream zis = new ZipInputStream(new FileInputStream(dataFile));
			zis.getNextEntry();
			this.reader = new BufferedReader(new InputStreamReader(zis));
			
			/*
			 * Following code is based on Apache compress 1.5 library
			 * This is used in a attempt to support APSPDCL exported
			 * zip format. but it was realized that the zip file they
			 * generate is not in recognizable format, and thus
			 * this code is not helpful.
			 */
			/*ZipFile zf = new ZipFile(dataFile);
			Enumeration<ZipArchiveEntry> entries = zf.getEntries();
			if (entries.hasMoreElements()) {
				ZipArchiveEntry zae = entries.nextElement();
				this.reader = new BufferedReader(new InputStreamReader(zf.getInputStream(zae)));
			} else {
				throw new IOException("Invalid file format, file "+dataFile.getName());
			}*/
			// Another way, using Apache compress 1.5
			/*ZipArchiveInputStream zais = new ZipArchiveInputStream(new FileInputStream(dataFile));
			ArchiveEntry nextEntry = zais.getNextEntry();
			this.reader = new BufferedReader(new InputStreamReader(zais));*/
		} else if (fn.endsWith(".TXT")){
			this.reader = new BufferedReader(fr);
		} else {
			throw new IOException("Invalid file format, file "+dataFile.getName());
		}
		
		
	}
	
	private void initNumberOfLines() throws IOException {
		try {
			this.initFileHandlers();
			while ((this.reader.readLine()) != null) {
				this.numLines++;
			}
		} finally {
			this.close();
		}
	}
	
	public String[] nextRecord() throws IOException{
		currentLine = this.reader.readLine();
		if (currentLine == null) {
			return null;
		}
		return convertAsRecord(currentLine);
	}
	
	public String getCurrentLine() {
		return currentLine;
	}
	
	private String[] convertAsRecord(String line) {
		if (line == null || "".equals(line.trim())) {
			return new String[]{};
		}
		if (recLenght != line.length()) {
			System.out.println("Line lenght mismatch, expected: "+recLenght+", got: "+line.length()+", line: "+line);
			return new String[]{};
		}
		List<String> values = new ArrayList<String>();
		int offset = 0;
		//for(Entry<String, Integer> e : ServiceColumns.IMPORT_FL_MAP.entrySet()) {
		for (String fieldName : ServiceColumns.IMPORT_COLUMN_ORDER) {
			//int fieldLength = e.getValue();
			int fieldLength = 0;
			FormatProps formatProps = ServiceColumns.FL_MAP.get(fieldName);
			if (formatProps == null) {
				throw new IllegalArgumentException(
						"FormatProps not found for fieldName: "+fieldName);
			}
			fieldLength = formatProps.fieldLenght.intValue();
			if (offset < line.length()) {
				String value = line.substring(offset, offset + fieldLength);
				values.add(value.trim());
				offset += fieldLength;
				//System.out.println((1+e.getKey())+"["+fieldLength+"]"+" = ["+values.get(e.getKey())+"]");
			}
		}
		return (String[])values.toArray(new String[]{});
	}
	public void close() {
		if (this.reader != null) {
			try {
				this.reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) throws Exception {
		File parentDirectory = new File("D:/Work/Incubator/electric-collector/docs/datafiles");
		DeviceFileReader r = new DeviceFileReader(parentDirectory,"SC12131SL39.txt.zip");
		String[] s = null;
		try {
			while((s = r.nextRecord()) != null) {
				System.out.println(">> "+Arrays.toString(s));
			}
		} finally {
			r.close();
		}
	}
	
	/**
	 * reads throug first 50 records ( or less for small size files )
	 * and validates records are valid.
	 * 
	 * If determined valid returns "YES" if not valid returns "NO"
	 * in case of any error reading input stream returns error message.
	 * 
	 * Note that 'is' InputStream is closed after end of this method call.
	 * 
	 * @param is
	 * @return "YES" or "NO" or error message
	 */
	public static String isValidFile(InputStream is) {
		DeviceFileReader d = null;
		try {
			d = new DeviceFileReader(new BufferedReader(new InputStreamReader(is)));
			String[] record = null;
			
			int recCount = 0;
			int successCount = 0;
			while ((record = d.nextRecord()) != null) {
				if (record.length == ServiceColumns.IMPORT_COLUMN_ORDER.size()) {
					successCount++;
				}
				recCount++;
				if (recCount > 50) {
					break;
				}
			}
			if (recCount == successCount && successCount != 0) {
				return "YES";
			} else {
				return "NO";
			}
		} catch (Throwable t) {
			t.printStackTrace();
			return t.getMessage();
		} finally {
			if (d != null) {
				d.close();
			}
		}
	}
}