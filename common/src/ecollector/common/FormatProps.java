package ecollector.common;

public class FormatProps {
	public final Integer fieldLenght;
	public final String importFormat;
	public final String exportFormat;
	public FormatProps(Integer fieldLenght, 
					   String importFormat,
					   String exportFormat) {
		this.fieldLenght = fieldLenght;
		this.importFormat = importFormat;
		this.exportFormat = exportFormat;
	}
	public boolean isLeftPaddingRequired() {
		if (exportFormat.indexOf("[L]") != -1) {
			return true;
		} else {
			return false;
		}
	}
	public boolean isDateField() {
		if (exportFormat.indexOf("[D]") != -1) {
			return true;
		} else {
			return false;
		}
	}
	public boolean isDecimalField() {
		if (exportFormat.indexOf("[F:") != -1) {
			return true;
		} else {
			return false;
		}
	}
	
	public int getExportFieldLenght() {
		if (exportFormat.indexOf("[LEN:") != -1) {
			String[] els = exportFormat.replace("[", "").replace("]", "").split(":");
			if (els.length != 2 || els[1] == null || els[1].trim().length() == 0) {
				return this.fieldLenght;
			} else {
				return Integer.parseInt(els[1]);
			}
		} else {
			return this.fieldLenght;
		}
	}
}
