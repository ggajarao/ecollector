package ecollector.common;

import java.util.LinkedHashSet;
import java.util.Set;

public class PrColumns {
	
	public static final String _ID = "_id";
	public static final String USC_NO = "USC_NO";
	public static final String SC_NO = "SC_NO";
	public static final String ERO = "ERO";
	public static final String SECTION = "SECTION";
	public static final String DISTRIBUTION = "DISTRIBUTION";
	public static final String RECEIPT_NUMBER = "RECEIPT_NUMBER";
	public static final String COLLECTION_DATE = "COLLECTION_DATE";
	public static final String COLLECTION_TIME = "COLLECTION_TIME";
	public static final String ARREARS_N_DEMAND = "ARREARS_N_DEMAND";
	public static final String RC_COLLECTED = "RC_COLLECTED";
	public static final String ACD_COLLECTED = "ACD_COLLECTED";
	public static final String OTHERS_COLLECTED = "OTHERS_COLLECTED";
	public static final String RC_CODE = "RC_CODE";
	public static final String MACHINE_CODE = "MACHINE_CODE";
	public static final String LAST_PAID_RECEIPT_NO = "LAST_PAID_RECEIPT_NO";
	public static final String LAST_PAID_DATE = "LAST_PAID_DATE";
	public static final String LAST_PAID_AMOUNT = "LAST_PAID_AMOUNT";
	public static final String TC_SEAL_NO = "TC_SEAL_NO";
	public static final String REMARKS = "REMARKS";
	public static final String REMARKS_AMOUNT = "REMARKS_AMOUNT";
	public static final String AGL_AMOUNT = "AGL_AMOUNT";
	public static final String AGL_SERVICES = "AGL_SERVICES";
	public static final String MACHINE_NUMBER = "MACHINE_NUMBER";
	public static final String NEW_ARREARS = "NEW_ARREARS";
	public static final String CMD_COLLECTED = "CMD_COLLECTED";
	public static final String COLLECTION_DELTA = "COLLECTION_DELTA";
	public static final String AMOUNT_COLLECTED = "AMOUNT_COLLECTED";
	
	public static final String _SC_NO = "_SC_NO"; //For search, this field will not contain leading zeros.
	public static final String _N_USC_NO = "_N_USC_NO"; //For numeric sorting of service numbers
	public static final String _DEVICE_PR_CODE = "_DEVICE_PR_CODE";	// Unique id across all devices in cloud
	public static final String _PUSH_STATE = "_PUSH_STATE";
	public static final String _ARCHIVE_STATE = "_ARCHIVE_STATE";
	
	public static final String _COMPANY_CODE = "companyCode";
	public static final String _CIRCLE_CODE = "circleCode";
	public static final String _DIVISION_CODE = "divisionCode";
	public static final String _ERO_CODE = "eroCode";
	public static final String _SUBDIVISION_CODE = "subDivisionCode";
	public static final String _SECTION_CODE = "sectionCode";
	public static final String _DIST_CODE = "distCode";
	public static final String _DEVICE_NAME = "deviceName";
	
	public static final String C_ARCHIVE_STATE_READY = "R";
	public static final String C_ARCHIVE_STATE_ARCHIVED = "A";
	
	
	public static final Set<String> PUSH_PULL_COLUMN_ORDER = new LinkedHashSet<String>();
	public static final Set<String> ALL_COLUMNS = new LinkedHashSet<String>();
	
	static {
		PUSH_PULL_COLUMN_ORDER.add(PrColumns.USC_NO);
		PUSH_PULL_COLUMN_ORDER.add(PrColumns.SC_NO);
		PUSH_PULL_COLUMN_ORDER.add(PrColumns.ERO);
		PUSH_PULL_COLUMN_ORDER.add(PrColumns.SECTION);
		PUSH_PULL_COLUMN_ORDER.add(PrColumns.DISTRIBUTION);
		PUSH_PULL_COLUMN_ORDER.add(PrColumns.RECEIPT_NUMBER);
		PUSH_PULL_COLUMN_ORDER.add(PrColumns.COLLECTION_DATE);
		PUSH_PULL_COLUMN_ORDER.add(PrColumns.COLLECTION_TIME);
		PUSH_PULL_COLUMN_ORDER.add(PrColumns.ARREARS_N_DEMAND);
		PUSH_PULL_COLUMN_ORDER.add(PrColumns.RC_COLLECTED);
		PUSH_PULL_COLUMN_ORDER.add(PrColumns.ACD_COLLECTED);
		PUSH_PULL_COLUMN_ORDER.add(PrColumns.OTHERS_COLLECTED);
		PUSH_PULL_COLUMN_ORDER.add(PrColumns.RC_CODE);
		PUSH_PULL_COLUMN_ORDER.add(PrColumns.MACHINE_CODE);
		PUSH_PULL_COLUMN_ORDER.add(PrColumns.LAST_PAID_RECEIPT_NO);
		PUSH_PULL_COLUMN_ORDER.add(PrColumns.LAST_PAID_DATE);
		PUSH_PULL_COLUMN_ORDER.add(PrColumns.LAST_PAID_AMOUNT);
		PUSH_PULL_COLUMN_ORDER.add(PrColumns.TC_SEAL_NO);
		PUSH_PULL_COLUMN_ORDER.add(PrColumns.REMARKS);
		PUSH_PULL_COLUMN_ORDER.add(PrColumns.REMARKS_AMOUNT);
		PUSH_PULL_COLUMN_ORDER.add(PrColumns.AGL_AMOUNT);
		PUSH_PULL_COLUMN_ORDER.add(PrColumns.AGL_SERVICES);
		PUSH_PULL_COLUMN_ORDER.add(PrColumns.MACHINE_NUMBER);
		PUSH_PULL_COLUMN_ORDER.add(PrColumns.NEW_ARREARS);
		PUSH_PULL_COLUMN_ORDER.add(PrColumns.CMD_COLLECTED);
		PUSH_PULL_COLUMN_ORDER.add(PrColumns.COLLECTION_DELTA);
		PUSH_PULL_COLUMN_ORDER.add(PrColumns._DEVICE_PR_CODE);
		
		ALL_COLUMNS.add(PrColumns._ID);
		ALL_COLUMNS.addAll(PUSH_PULL_COLUMN_ORDER);
		ALL_COLUMNS.add(PrColumns._N_USC_NO);
		ALL_COLUMNS.add(PrColumns._PUSH_STATE);
		ALL_COLUMNS.add(PrColumns._DEVICE_PR_CODE);
		ALL_COLUMNS.add(PrColumns._SC_NO);
		ALL_COLUMNS.add(PrColumns.AMOUNT_COLLECTED);
		ALL_COLUMNS.add(PrColumns._ARCHIVE_STATE);
	}
}
