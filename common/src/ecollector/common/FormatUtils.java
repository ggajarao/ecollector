package ecollector.common;

import java.util.ArrayList;
import java.util.List;

public class FormatUtils {
	
	/**
	 * Prepends 0's before the given number. Number of 
	 * 0's prepended can be speicified in the param numZeros
	 * 
	 * Eg: n =   12, places=3 return value: 012
	 * Eg: n =  230, places=3 return value: 230
	 * @param n
	 * @param places
	 * @return
	 */
	public static String numberPadding(String n, int places) {
		int number = 0;
		try {
			number = Integer.parseInt(n);
		} catch(NumberFormatException nfe) {
		}
		String s = number+"";
		StringBuffer sb = new StringBuffer();
		for (int i = s.length(); i < places; i++) {
			sb.append("0");
		}
		return sb.append(s).toString();
	}
	
	/**
	 * Pads 0's upto two decimal places. 
	 * Also truncates beyond two decimal places
	 * 
	 * eg: input 100 returns 100.00
	 *     input .21 returns 0.21
	 *     input .7 returns 0.70
	 *     input 12.3212 return 12.32
	 * 
	 * @param d
	 * @return
	 */
	public static String decimalPadding(String d) {
		if (d == null) return "0.00";
		if (d.trim().isEmpty()) return "0.00";
		
		// Padding zeros to 2 places
		StringBuffer s = new StringBuffer(d.trim());
		int decIndex = (s.indexOf("."));
		if (s.length() == 0) {
			s.append("0.00");
			return s.toString();
		}
		
		if (s.toString().startsWith(".")) {
			s = new StringBuffer("0");
			s.append(d.trim());
			decIndex = s.indexOf(".");
		}
		
		if (decIndex == -1) {
			s.append(".00");
		} else {
			String dec = s.substring(decIndex+1);
			if (dec.length() < 2) {
				for (int i = dec.length(); i < 2; i++ ) {
					s.append("0");
				}
			} else if (dec.length() > 2) {
				s = new StringBuffer(s.substring(0,decIndex+1));
				s.append(dec.substring(0, 2));
			}
		}
		return s.toString();
	}
	
	/**
	 * Given 1000.00 returns 1,000.00
	 * @param d
	 * @return
	 */
	public static String amountFormat(String d) {
		if (d == null) return "";
		d = d.trim();
		StringBuffer sb = new StringBuffer(d);
		int decIndex = sb.indexOf(".");
		if (decIndex == -1) decIndex = sb.length();
		int pos = -1;
		for ( int i = decIndex; i > 0; i--) {
			pos++;
			if (pos % 3 == 0 && pos != 0) {
				sb.insert(i, ",");
			}
		}
		return sb.toString();
	}

	
	public static String listAsCsv(List<String> list) {
		StringBuffer s = new StringBuffer();
		for (int i = 0; i < list.size(); i++) {
			String li = list.get(i);
			if (li == null) li = "";
			s.append(li);
			if ( i < list.size() -1 ) {
				s.append(",");
			}
		}
		return s.toString();
	}
	
	public static List<String> csvAsList(String csv) {
		if (csv == null || csv.trim().length() == 0) return new ArrayList<String>();
		List<String> s = new ArrayList<String>();
		String[] vs = csv.split(",");
		for (String v : vs) {
			s.add(v);
		}
		return s;
	}
	
	private static int SVC_LENGTH = 13;
	public static List<String> parseAglServices(String services) {
		List<String> l = new ArrayList<String>();
		if (services == null || services.trim().length() == 0) {
			return l;
		}
		services = services.trim();
		if (services.length() % SVC_LENGTH != 0) {
			l.add(services);
			return l;
		}
		for (int i = 0; i+SVC_LENGTH <= services.length(); i += SVC_LENGTH) {
			l.add(services.substring(i,i+SVC_LENGTH));
		}
		return l;
	}
	
	public static String formatAglServicesForExport(String aglServices) {
		int numServices = 11;
		List<String> serviceList = FormatUtils.parseAglServices(aglServices);
		StringBuffer r = new StringBuffer();
		if (serviceList.isEmpty()) return r.toString();
		for (int i = 0; i < numServices; i++) {
			String service = null;
			if (serviceList.size() > i) {
				service = serviceList.get(i);
				if (service != null) service = service.trim();
			}
			if (service == null) {
				r.append("             ");
			} else {
				r.append(service);
			}
			
			if (i < numServices - 1) {
				r.append(":");
			}
		}
		return r.toString();
	}
	
	// Copied from PrintUtil
	// Prepares a printable line, with left padding
	public static StringBuffer prepareLineLpad(String value, int maxLenght) {
		if (value == null || value.trim().length() == 0) value = "";
		StringBuffer sb = new StringBuffer();
		sb.append(pad(maxLenght - value.length())).append(value);
		return sb;
	}
	
	// Copied from PrintUtil
	public static StringBuffer pad(int numSpaces) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < numSpaces; i++) {
			sb.append(' ');
		}
		return sb;
	}
	
	// Copied from PrintUtil
	public static StringBuffer prepareLineRpad(String value, int maxLenght) {
		if (value == null || value.trim().length() == 0) value = "";
		StringBuffer sb = new StringBuffer();
		sb.append(value).append(pad(maxLenght - value.length()));
		return sb;
	}
	
	// Copied from PrintUtil
	public static CharSequence prepareLineCalign(String string, int maxLength) {
		StringBuffer s = new StringBuffer();
		if (string == null || string.trim().length() == 0) {
			return s.append(pad(maxLength));
		}
		string = string.trim();
		int leftPadding = (maxLength - string.length())/2;
		s.append(pad(leftPadding))
		.append(prepareLineRpad(string,maxLength-leftPadding));
		return s;
	}
}