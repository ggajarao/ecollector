package ecollector.common;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ServiceDataExporter {
	
	private Writer mWriter;
	private OutputStream mOutputStream;
	private boolean mIsCompressMode = false;
	
	public ServiceDataExporter() {
		
	}
	
	public ServiceDataExporter(Writer writer) {
		mWriter = writer;
	}
	
	public ServiceDataExporter(OutputStream outputStream, boolean compress, String fileName) throws IOException {
		mIsCompressMode = compress;
		if (mIsCompressMode) {
			mOutputStream = new ZipOutputStream(outputStream);
			((ZipOutputStream)mOutputStream).putNextEntry(new ZipEntry(fileName));
		} else {
			mOutputStream = outputStream;
		}
	}
	
	public void export(Map<String,String> fieldValueMap) 
	throws IOException {
		write(formattedString(fieldValueMap)+"\n");		
	}
	
	public String formattedString(Map<String,String> fieldValueMap) {
		StringBuffer sb = new StringBuffer();
		int i = 0;
		int N = ServiceColumns.EXPORT_COLUMN_ORDER.size();
		for (String fieldName : ServiceColumns.EXPORT_COLUMN_ORDER) {
			String fieldValue = fieldValueMap.get(fieldName);
			FormatProps fProps = ServiceColumns.FL_MAP.get(fieldName);
			StringBuffer ff = new StringBuffer();
			formattedFieldText(ff,fieldName,fieldValue,fProps);
			sb.append(ff);
			if (i < N-1) {
				sb.append("|");
			}
			i++;
		}
		return sb.toString();
	}
	
	private void write(String data) throws IOException {
		if (mWriter != null) {
			mWriter.write(data);
		} else if (mOutputStream != null) {
			mOutputStream.write(data.getBytes("UTF-8"));
		} else {
			new IOException("Data exporter underlying output channel not initialized");
		}
	}
	
	public void close() throws IOException {
		if (mWriter != null) {
			mWriter.close();
		} else if (mOutputStream != null) {
			if (mIsCompressMode) {
				((ZipOutputStream)mOutputStream).finish();
			}
			mOutputStream.close();
		} else {
			new IOException("Data exporter underlying output channel not initialized");
		}
	}

	private void formattedFieldText(StringBuffer ff, String fieldName,
			String fieldValue, FormatProps fProps) {
		if (fProps == null) {
			throw new NullPointerException("FormatProps missing for fieldName: "+fieldName);
		}
		if (fieldValue == null || fieldValue.trim().length() == 0) {
			fieldValue = "";
		}
		boolean isLeftPad = fProps.isLeftPaddingRequired();
		boolean isDateField = fProps.isDateField();
		boolean isDecimalField = fProps.isDecimalField();
		int fieldLenght = fProps.fieldLenght;
		
		if (isDateField) {
			fieldValue = this.getReadableDateFromSystemDate(fieldValue);
		} else if (isDecimalField) {
			fieldValue = FormatUtils.decimalPadding(fieldValue);
		}
		
		boolean isAglServicesField = PrColumns.AGL_SERVICES.equals(fieldName);
		if (isAglServicesField) {
			fieldValue = FormatUtils.formatAglServicesForExport(fieldValue);
			fieldLenght = fProps.getExportFieldLenght();
		}
		
		if (isLeftPad) {
			ff.append(pad(fieldLenght-fieldValue.length())).append(fieldValue);
		} else {
			ff.append(fieldValue).append(pad(fieldLenght-fieldValue.length()));
		}
		// Trim chars if excess fieldLenght 
		ff.setLength(fieldLenght);
	}

	private String pad(int numSpaces) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < numSpaces; i++) {
			sb.append(' ');
		}
		return sb.toString();
	}
	
	private String getReadableDateFromSystemDate(String sysFormatDate) {
		SimpleDateFormat machineFormat = new SimpleDateFormat(ServiceColumns.MACHINE_DATE_FORMAT);
		SimpleDateFormat readableDateFormat = new SimpleDateFormat(ServiceColumns.READABLE_DATE_FORMAT);
		
		try {
			return readableDateFormat.format(machineFormat.parse(sysFormatDate));
		} catch (Exception e) {}
		return "";
	}
}