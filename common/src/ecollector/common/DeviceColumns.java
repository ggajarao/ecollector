package ecollector.common;

public class DeviceColumns {
	public static final String _ID = "_id";
	public static final String DEVICE_NAME = "DEVICE_NAME";
	public static final String DEVICE_IDENTIFIER = "DEVICE_IDENTIFIER";
	public static final String DEVICE_CLD_UN = "DEVICE_CLD_UN";
	public static final String DEVICE_CLD_PWD = "DEVICE_CLD_PWD";
	public static final String DEVICE_PRINTER_NAME = "DEVICE_PRINTER_NAME";
	public static final String DEVICE_PRINTER_ADDR = "DEVICE_PRINTER_ADDR";
	public static final String DEVICE_CLOUD_TIME = "DEVICE_CLOUD_TIME";
	public static final String DEVICE_COLLECTION_EXPIRATION_DATE = "DEVICE_COLLECTION_EXPIRATION_DATE";
	public static final String DEVICE_COLLECTION_LIMIT = "DEVICE_COLLECTION_LIMIT";
	public static final String DEVICE_EXPIRATION_DELTA_TICKS = "DEVICE_EXPIRATION_DELTA_TICKS";
	public static final String DEVICE_LAST_TSL_RESET_TIME = "DEVICE_LAST_TSL_RESET_TIME";
	public static final String DEVICE_PRINT_FORMAT = "DEVICE_PRINT_FORMAT";
	
	public static final int DEVICE_EXPIRATION_DELTA_TICKS_COL_INDEX = 10;
}
