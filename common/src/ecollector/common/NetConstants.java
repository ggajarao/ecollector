package ecollector.common;

public class NetConstants {
	
	public static final String VERSION = "v2014march21"; 
		//"v2013june18";
	
	//public static final String CLOUD_URL = "http://1-ecollector.appspot.com/cloudapp/";
	//public static final String CLOUD_URL = "http://d1-ecollector.appspot.com/cloudapp/";
	public static final String CLOUD_URL = "http://192.168.1.2:8888/cloudapp/";
	//public static final String CLOUD_URL = "http://stage-ecollector.appspot.com/cloudapp/";
	//public static final String CLOUD_URL = "http://stage2-ecollector.appspot.com/cloudapp/";
	
	public static final String PARING_SERVICE = CLOUD_URL + "ps";
	public static final String PUSH_SERVICE = CLOUD_URL + "push";
	public static final String PUSH_SERVICE_PAYLOAD = "pl";
	
	/* /cloudapp/secure/serveFile?a=r&bk="+blobKey; */
	public static final String DOWNLOAD_SERVICE = CLOUD_URL + "secure/serveFile";
	
	public static final String REQUEST_PARAM_ACTION = "a";
	public static final String REQUEST_PARAM_DEVICE_IDENTIFIER = "di";
	public static final String ACTION_PUSH = "p";
	
	public static final String CLOUD_SERVICE_LOGIN = "login";
	public static final String CLOUD_SERVICE_SECURE = "secure";
}