package ecollector.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ServiceColumns {
	
	public static final String MACHINE_DATE_FORMAT = "ddMMyyyy";
	public static final String READABLE_DATE_FORMAT = "dd/MM/yyyy";
	public static final String MACHINE_TIME_FORMAT = "hh:mm:ss a";

	public static final String _ID = "_id";
	public static final String USC_NO = "USC_NO";
	public static final String SC_NO = "SC_NO";
	public static final String NAME  = "NAME";   
	public static final String ADDRESS = "ADDRESS";
	public static final String CATEGORY	= "CATEGORY";
	public static final String GROUP_X = "GROUP_X"; 
	public static final String ERO = "ERO";
	public static final String SECTION = "SECTION";
	public static final String DISTRIBUTION = "DISTRIBUTION";
	public static final String ARRIERS = "ARRIERS";
	public static final String CMD = "CMD";
	public static final String ACD = "ACD";
	public static final String SD_AVAILABLE = "SD_AVAILABLE";
	public static final String BILLED_MONTHS = "BILLED_MONTHS";  
	public static final String DC_DATE = "DC_DATE";
	public static final String RC_CODE = "RC_CODE";
	public static final String MACHINE_CODE = "MACHINE_CODE";
	public static final String PEFCTDT = "PEFCTDT";
	public static final String AGL_AMOUNT = "AGL_AMOUNT";
	public static final String AGL_SERVICES = "AGL_SERVICES";
	public static final String SUB_CATEGORY = "SUB_CATEGORY";
	  
	/*public static final String RECEIPT_NUMBER = "RECEIPT_NUMBER";
	public static final String COLLECTION_DATE = "COLLECTION_DATE";
	public static final String COLLECTION_TIME = "COLLECTION_TIME";
	public static final String ARREARS_N_DEMAND = "ARREARS_N_DEMAND";
	public static final String RC_COLLECTED = "RC_COLLECTED";
	public static final String ACD_COLLECTED = "ACD_COLLECTED";
	public static final String OTHERS_COLLECTED = "OTHERS_COLLECTED";
	public static final String LAST_PAID_RECEIPT_NO = "LAST_PAID_RECEIPT_NO";
	public static final String LAST_PAID_DATE = "LAST_PAID_DATE";
	public static final String LAST_PAID_AMOUNT = "LAST_PAID_AMOUNT";
	public static final String TC_SEAL_NO = "TC_SEAL_NO";
	public static final String REMARKS = "REMARKS";
	public static final String REMARKS_AMOUNT = "REMARKS_AMOUNT";*/
	
	public static final String _PR_IDS = "_PR_IDS"; // Holds csv of pr ids
	public static final String _SC_NO = "_SC_NO"; // Userd in device, this field holds service number in string version with stripped leading zeros.
	public static final String _N_USC_NO = "_N_USC_NO"; // Used in device, holds number value of service which is used to sort services
	//public static final String _PUSH_STATE = "_PUSH_STATE"; // Status of service record in device whether pushed to internet or not

	public static final List<String> IMPORT_COLUMN_ORDER = new ArrayList<String>();
	public static final List<String> EXPORT_COLUMN_ORDER = new ArrayList<String>();
	public static final List<String> PUSH_PULL_COLUMN_ORDER = new ArrayList<String>(); // used to serialize and deserialize a record
	static {
		IMPORT_COLUMN_ORDER.add(ServiceColumns.USC_NO);
		IMPORT_COLUMN_ORDER.add(ServiceColumns.SC_NO);
		IMPORT_COLUMN_ORDER.add(ServiceColumns.NAME);
		IMPORT_COLUMN_ORDER.add(ServiceColumns.ADDRESS);
		IMPORT_COLUMN_ORDER.add(ServiceColumns.CATEGORY);
		IMPORT_COLUMN_ORDER.add(ServiceColumns.GROUP_X);
		IMPORT_COLUMN_ORDER.add(ServiceColumns.ERO);
		IMPORT_COLUMN_ORDER.add(ServiceColumns.SECTION);
		IMPORT_COLUMN_ORDER.add(ServiceColumns.DISTRIBUTION);
		IMPORT_COLUMN_ORDER.add(ServiceColumns.ARRIERS);
		IMPORT_COLUMN_ORDER.add(ServiceColumns.CMD);
		IMPORT_COLUMN_ORDER.add(ServiceColumns.ACD);
		IMPORT_COLUMN_ORDER.add(ServiceColumns.SD_AVAILABLE);
		IMPORT_COLUMN_ORDER.add(ServiceColumns.BILLED_MONTHS);
		IMPORT_COLUMN_ORDER.add(ServiceColumns.DC_DATE);
		IMPORT_COLUMN_ORDER.add(ServiceColumns.RC_CODE);
		IMPORT_COLUMN_ORDER.add(ServiceColumns.MACHINE_CODE);
		IMPORT_COLUMN_ORDER.add(ServiceColumns.PEFCTDT);
		IMPORT_COLUMN_ORDER.add(ServiceColumns.AGL_AMOUNT);
		IMPORT_COLUMN_ORDER.add(ServiceColumns.AGL_SERVICES);
		IMPORT_COLUMN_ORDER.add(ServiceColumns.SUB_CATEGORY);
		
		EXPORT_COLUMN_ORDER.add(PrColumns.USC_NO);
		EXPORT_COLUMN_ORDER.add(PrColumns.SC_NO);
		EXPORT_COLUMN_ORDER.add(PrColumns.ERO);
		EXPORT_COLUMN_ORDER.add(PrColumns.SECTION);
		EXPORT_COLUMN_ORDER.add(PrColumns.DISTRIBUTION);
		EXPORT_COLUMN_ORDER.add(PrColumns.RECEIPT_NUMBER);
		EXPORT_COLUMN_ORDER.add(PrColumns.COLLECTION_DATE);
		EXPORT_COLUMN_ORDER.add(PrColumns.COLLECTION_TIME);
		EXPORT_COLUMN_ORDER.add(PrColumns.ARREARS_N_DEMAND);
		EXPORT_COLUMN_ORDER.add(PrColumns.RC_COLLECTED);
		EXPORT_COLUMN_ORDER.add(PrColumns.ACD_COLLECTED);
		EXPORT_COLUMN_ORDER.add(PrColumns.OTHERS_COLLECTED);
		EXPORT_COLUMN_ORDER.add(PrColumns.RC_CODE);
		EXPORT_COLUMN_ORDER.add(PrColumns.MACHINE_CODE);
		EXPORT_COLUMN_ORDER.add(PrColumns.LAST_PAID_RECEIPT_NO);
		EXPORT_COLUMN_ORDER.add(PrColumns.LAST_PAID_DATE);
		EXPORT_COLUMN_ORDER.add(PrColumns.LAST_PAID_AMOUNT);
		EXPORT_COLUMN_ORDER.add(PrColumns.TC_SEAL_NO);
		EXPORT_COLUMN_ORDER.add(PrColumns.REMARKS);
		EXPORT_COLUMN_ORDER.add(PrColumns.REMARKS_AMOUNT);
		EXPORT_COLUMN_ORDER.add(PrColumns.AGL_AMOUNT);
		EXPORT_COLUMN_ORDER.add(PrColumns.AGL_SERVICES);
		EXPORT_COLUMN_ORDER.add(PrColumns.MACHINE_NUMBER);
		
		// initialize push pull column order
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.USC_NO);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.SC_NO);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.NAME);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.ADDRESS);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.CATEGORY);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.GROUP_X);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.ERO);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.SECTION);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.DISTRIBUTION);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.ARRIERS);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.CMD);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.ACD);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.SD_AVAILABLE);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.BILLED_MONTHS);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.DC_DATE);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.RC_CODE);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.MACHINE_CODE);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.PEFCTDT);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.AGL_AMOUNT);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.AGL_SERVICES);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.SUB_CATEGORY);
		
		/*PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.USC_NO);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.SC_NO);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.ERO);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.SECTION);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.DISTRIBUTION);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.RECEIPT_NUMBER);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.COLLECTION_DATE);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.COLLECTION_TIME);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.ARREARS_N_DEMAND);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.RC_COLLECTED);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.ACD_COLLECTED);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.OTHERS_COLLECTED);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.RC_CODE);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.MACHINE_CODE);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.LAST_PAID_RECEIPT_NO);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.LAST_PAID_DATE);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.LAST_PAID_AMOUNT);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.TC_SEAL_NO);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.REMARKS);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.REMARKS_AMOUNT);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.AGL_AMOUNT);
		PUSH_PULL_COLUMN_ORDER.add(ServiceColumns.AGL_SERVICES);*/
	}
	
	// Field Lenght map, in the order of IMPORT_COLUMN_ORDER
	// key=index of field, Value=Lenght, in characters, of field.
	public static Map<String, FormatProps> FL_MAP = new LinkedHashMap<String, FormatProps>();
	
	static {
		FL_MAP.put(ServiceColumns.USC_NO, new FormatProps(13,"",""));
		FL_MAP.put(ServiceColumns.SC_NO, new FormatProps(6,"",""));
		FL_MAP.put(ServiceColumns.NAME, new FormatProps(20,"",""));
		FL_MAP.put(ServiceColumns.ADDRESS, new FormatProps(15,"",""));
		FL_MAP.put(ServiceColumns.CATEGORY, new FormatProps(1,"",""));
		FL_MAP.put(ServiceColumns.GROUP_X, new FormatProps(2,"",""));
		FL_MAP.put(ServiceColumns.ERO, new FormatProps(10,"",""));
		FL_MAP.put(ServiceColumns.SECTION, new FormatProps(10,"",""));
		FL_MAP.put(ServiceColumns.DISTRIBUTION, new FormatProps(10,"",""));
		FL_MAP.put(ServiceColumns.ARRIERS, new FormatProps(10,"",""));
		FL_MAP.put(ServiceColumns.CMD, new FormatProps(10,"",""));
		FL_MAP.put(ServiceColumns.ACD, new FormatProps(9,"",""));
		FL_MAP.put(ServiceColumns.SD_AVAILABLE, new FormatProps(9,"",""));
		FL_MAP.put(ServiceColumns.BILLED_MONTHS, new FormatProps(12,"",""));
		FL_MAP.put(ServiceColumns.DC_DATE, new FormatProps(8,"",""));
		FL_MAP.put(ServiceColumns.RC_CODE, new FormatProps(6,"",""));
		FL_MAP.put(ServiceColumns.MACHINE_CODE, new FormatProps(6,"",""));
		FL_MAP.put(ServiceColumns.PEFCTDT, new FormatProps(8,"",""));
		FL_MAP.put(ServiceColumns.AGL_AMOUNT, new FormatProps(7,"","[L]"));
		FL_MAP.put(ServiceColumns.AGL_SERVICES, new FormatProps(143,"","[LEN:153]"));
		FL_MAP.put(ServiceColumns.SUB_CATEGORY, new FormatProps(2,"",""));
		
		FL_MAP.put(PrColumns.RECEIPT_NUMBER, new FormatProps(10,"","[L]"));
		FL_MAP.put(PrColumns.COLLECTION_DATE, new FormatProps(8,"",""));
		FL_MAP.put(PrColumns.COLLECTION_TIME, new FormatProps(11,"",""));
		FL_MAP.put(PrColumns.ARREARS_N_DEMAND, new FormatProps(10,"","[L]"));
		FL_MAP.put(PrColumns.RC_COLLECTED, new FormatProps(6,"","[L]"));
		FL_MAP.put(PrColumns.ACD_COLLECTED, new FormatProps(9,"","[L]"));
		FL_MAP.put(PrColumns.OTHERS_COLLECTED, new FormatProps(10,"","[L]"));
		
		FL_MAP.put(PrColumns.LAST_PAID_RECEIPT_NO, new FormatProps(10,"",""));
		FL_MAP.put(PrColumns.LAST_PAID_DATE, new FormatProps(8,"",""));
		FL_MAP.put(PrColumns.LAST_PAID_AMOUNT, new FormatProps(10,"","[L]"));
		FL_MAP.put(PrColumns.TC_SEAL_NO, new FormatProps(9,"",""));
		FL_MAP.put(PrColumns.REMARKS, new FormatProps(2,"","[L]"));
		FL_MAP.put(PrColumns.REMARKS_AMOUNT, new FormatProps(10,"","[L]"));
		FL_MAP.put(PrColumns.MACHINE_NUMBER, new FormatProps(10,"",""));
		
	}
	
	/*when billing is completed, its ready*/
	public static final String C_PUSH_STATE_READY = "R";
	/*when pushed to cloud, its complete*/
	public static final String C_PUSH_STATE_COMPLETE = "C";
	/*when service is imported into device, its new*/
	public static final String C_PUSH_STATE_NEW = "N";
	
	public static final String C_FIELD_SEPERATOR = ":::";
	public static final String C_RECORD_SEPERATOR = ":#:";
	
	public List<Map<String, String>> deserializeServices(String serializedString) {
		List<Map<String, String>> services = new ArrayList<Map<String,String>>();
		if (serializedString == null || serializedString.trim().equals("")) {
			return services;
		}
		String[] records = serializedString.split(ServiceColumns.C_RECORD_SEPERATOR);
		for (String record : records) {
			if (record != null && record.trim().equals("")) {
				continue;
			}
			String[] fields = record.split(ServiceColumns.C_FIELD_SEPERATOR);
			Map<String,String> m = new HashMap<String,String>();
			int i = 0;
			for (String fieldName : ServiceColumns.PUSH_PULL_COLUMN_ORDER) {
				m.put(fieldName, fields[i]);
				i++;
			}
		}
		return services;
	}
}