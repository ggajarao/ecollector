package ecollector.common;

public interface ReportAbstractColumns {
	public static final String ARREARS_N_DEMAND = "ARREARS_N_DEMAND";
	public static final String ARREARS_N_DEMAND_COUNT = "ARREARS_N_DEMAND_COUNT";
	public static final String RC_COLLECTED = "RC_COLLECTED";
	public static final String RC_COLLECTED_COUNT = "RC_COLLECTED_COUNT";
	public static final String ACD_COLLECTED = "ACD_COLLECTED";
	public static final String ACD_COLLECTED_COUNT = "ACD_COLLECTED_COUNT";
	public static final String AGL_AMOUNT = "AGL_AMOUNT";
	public static final String AGL_AMOUNT_COUNT = "AGL_AMOUNT_COUNT";
	public static final String GRAND_TOTAL = "GRAND_TOTAL";
	public static final String TOTAL_PRS = "TOTAL_PRS";
	public static final String SECTION = "SECTION";
	public static final String DISTRIBUTION = "DISTRIBUTION";
}
