package ecollector.net;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class JavaCallProxy {
	
	public static String call(String url, Map<String,String> headerParams, 
			HttpPostStore postParams) throws IOException 
	{

		URL uUrl =  new URL(url);
		/*try {*/
            
            HttpURLConnection connection = (HttpURLConnection) uUrl.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");

            setHeaderParams(headerParams, connection);
    		
            /*GZIPOutputStream gzipOs = new GZIPOutputStream(connection.getOutputStream());
            OutputStreamWriter writer = new OutputStreamWriter(gzipOs);*/
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(postParams.getPostMessage());
            writer.close();
            
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            	//String contentEncoding = connection.getContentEncoding();
            	/*if ("gzip".equalsIgnoreCase(contentEncoding)) {
            		return convertToStringGunzip(connection.getInputStream());
            	} else {*/
            		return convertToString(connection.getInputStream());
            	/*}*/
            	
            } else {
            	throw new IOException("HTTP ResponseCode: "+connection.getResponseCode());
            }
        /*} catch (Exception e) {
        	Log.i("JavaCallProxy","Exception while http request",e);
        	throw e;
        }*/
        //return "";
	}

	private static void setHeaderParams(Map<String, 
			String> headerParams,
			HttpURLConnection connection) {
		if (headerParams == null) {
			headerParams = new HashMap<String,String>();
			headerParams.put("Accept-Encoding", "gzip");
		}
		Iterator<String> i = headerParams.keySet().iterator();
		String key;
		while (i.hasNext())
		{
			key = i.next();
			connection.setRequestProperty(key,headerParams.get(key));
		}
	}
	
	private static String convertToString(InputStream instream) 
	throws IllegalStateException, IOException
	{
		StringBuffer responseString = new StringBuffer();
		int l;
		byte[] tmp = new byte[2048];
		while ((l = instream.read(tmp)) != -1) {
			responseString.append(new String(tmp,0,l));  
		}
		return responseString.toString().trim();
	}
}
