package ecollector.net;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class HttpPostStore
{
	Map<String,String> store = new HashMap<String,String>(); 
	public HttpPostStore()
	{
				
	}
	
	public void set(String key, String value) 
	throws UnsupportedEncodingException
	{
		store.put(key, URLEncoder.encode(value, "UTF-8"));		
	}
	
	public String getPostMessage()
	{
		StringBuffer m = new StringBuffer();
		Iterator<String> s = store.keySet().iterator();
		int i = 0;
		String key;
		while (s.hasNext())
		{
			key = s.next();			
			if (i > 0)
				m.append("&");
			String value = store.get(key);
			if (value != null) {
				value = value.trim();
			} else {
				value = "";
			}
			m.append(key).append("=").append(value);		
			i++;
		}
		return m.toString();
	}
}