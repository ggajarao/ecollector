package ecollector.common;

import ecollector.common.FormatUtils;
import junit.framework.TestCase;

public class FormatUtilTest extends TestCase {
	@Override
	protected void setUp() throws Exception {
		super.setUp();
	}
	
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public void testDecimalPadding() {
		
		String input = "";
		String expected = "0.00";
		assertEquals(expected,FormatUtils.decimalPadding(input));
		
		input = "0";
		expected = "0.00";
		assertEquals(expected,FormatUtils.decimalPadding(input));
		
		input = "1";
		expected = "1.00";
		assertEquals(expected,FormatUtils.decimalPadding(input));
		
		input = "-10";
		expected = "-10.00";
		assertEquals(expected,FormatUtils.decimalPadding(input));
		
		input = "12023";
		expected = "12023.00";
		assertEquals(expected,FormatUtils.decimalPadding(input));
		
		input = ".0";
		expected = "0.00";
		assertEquals(expected,FormatUtils.decimalPadding(input));
		
		input = ".1";
		expected = "0.10";
		assertEquals(expected,FormatUtils.decimalPadding(input));
		
		input = ".01";
		expected = "0.01";
		assertEquals(expected,FormatUtils.decimalPadding(input));
		
		input = "0.1234";
		expected = "0.12";
		assertEquals(expected,FormatUtils.decimalPadding(input));
		
		input = ".1234";
		expected = "0.12";
		assertEquals(expected,FormatUtils.decimalPadding(input));
		
		input = "0.00";
		expected = "0.00";
		assertEquals(expected,FormatUtils.decimalPadding(input));
		
		input = "2325278.12395832123000123";
		expected = "2325278.12";
		assertEquals(expected,FormatUtils.decimalPadding(input));		
	}
}
