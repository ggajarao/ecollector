/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package ecollector.device;

public final class R {
    public static final class attr {
    }
    public static final class dimen {
        public static final int padding_large=0x7f050002;
        public static final int padding_medium=0x7f050001;
        public static final int padding_small=0x7f050000;
    }
    public static final class drawable {
        public static final int black_white_gradient=0x7f020000;
        public static final int curren_focus_field_animated=0x7f020001;
        public static final int curren_focus_field_error_animated=0x7f020002;
        public static final int current_focus_field=0x7f020003;
        public static final int current_focus_field_error=0x7f020004;
        public static final int dashboard_panel=0x7f020005;
        public static final int filter_button_background=0x7f020006;
        public static final int filter_on_button_background=0x7f020007;
        public static final int green_button=0x7f020008;
        public static final int green_button_old=0x7f020009;
        public static final int ic_action_search=0x7f02000a;
        public static final int ic_launcher=0x7f02000b;
        public static final int input_field=0x7f02000c;
        public static final int input_field_error=0x7f02000d;
        public static final int item_highlighter=0x7f02000e;
        public static final int keypad_button=0x7f02000f;
        public static final int keypad_button_pressed_animation=0x7f020010;
        public static final int red_button=0x7f020011;
        public static final int red_button_old=0x7f020012;
        public static final int red_ltr_gradient=0x7f020013;
    }
    public static final class id {
        public static final int acdAmountTv=0x7f090039;
        public static final int acdTv=0x7f090083;
        public static final int addDemandBtn=0x7f09008f;
        public static final int address=0x7f090094;
        public static final int aglAmount1Tv=0x7f090038;
        public static final int aglTv=0x7f090084;
        public static final int amountAbstractBtn=0x7f090085;
        public static final int amountCollectedTv=0x7f090032;
        public static final int appTitle=0x7f090056;
        public static final int arrearsAmtTv=0x7f09008d;
        public static final int arrearsNcmdAmtTv=0x7f090036;
        public static final int arrearsTv=0x7f09002a;
        public static final int billMonthsTv=0x7f090092;
        public static final int bluetoothDevicesSp=0x7f09006e;
        public static final int bluetoothStatusTv=0x7f09006d;
        public static final int category=0x7f090096;
        public static final int ccTv=0x7f090081;
        public static final int checkDataUpdatesBtn=0x7f0900a0;
        public static final int clearButton=0x7f090020;
        public static final int cmdAmtTv=0x7f090073;
        public static final int cmdTv=0x7f09002d;
        public static final int collectableAmountTv=0x7f090031;
        public static final int collectionTxt=0x7f090048;
        public static final int column1=0x7f090029;
        public static final int column2=0x7f09002c;
        public static final int columnSpacer=0x7f09002b;
        public static final int confirmCollectedTv=0x7f09002f;
        public static final int consumerName=0x7f09008a;
        public static final int dashboardPnl=0x7f09005b;
        public static final int dateRangeTv=0x7f09007c;
        public static final int dcDateTv=0x7f09008b;
        public static final int deleteButton=0x7f090022;
        public static final int detailedReportBtn=0x7f090087;
        public static final int deviceFilesPnl=0x7f090055;
        public static final int deviceNameEditText=0x7f090043;
        public static final int deviceNameTxt=0x7f090047;
        public static final int distributionName=0x7f09003b;
        public static final int eroName=0x7f090091;
        public static final int excessAmountTv=0x7f090030;
        public static final int excludeArrearsBtn=0x7f09008e;
        public static final int exemptorSp=0x7f090072;
        public static final int expiresOnTxt=0x7f09004e;
        public static final int fileDate=0x7f090051;
        public static final int fileImportMethodTv=0x7f090052;
        public static final int fileMessage=0x7f090054;
        public static final int fileName=0x7f090050;
        public static final int filterBtn=0x7f09009c;
        public static final int filterText=0x7f09009d;
        public static final int fromDateDp=0x7f090076;
        public static final int group=0x7f090095;
        public static final int headerTitle=0x7f090042;
        public static final int homeBtn=0x7f09009e;
        public static final int importBtn=0x7f090053;
        public static final int informationPnl=0x7f09004f;
        public static final int inputView=0x7f09009b;
        public static final int itemCountTv=0x7f090079;
        public static final int itemNameTv=0x7f090078;
        public static final int itemTotalTv=0x7f09007a;
        public static final int key0=0x7f090067;
        public static final int key1=0x7f090064;
        public static final int key2=0x7f090065;
        public static final int key3=0x7f090066;
        public static final int key4=0x7f090061;
        public static final int key5=0x7f090062;
        public static final int key6=0x7f090063;
        public static final int key7=0x7f09005e;
        public static final int key8=0x7f09005f;
        public static final int key9=0x7f090060;
        public static final int keyA=0x7f09000c;
        public static final int keyAlphabetKeyPad=0x7f090068;
        public static final int keyAsterisk=0x7f090016;
        public static final int keyB=0x7f09001b;
        public static final int keyC=0x7f090019;
        public static final int keyD=0x7f09000e;
        public static final int keyE=0x7f090003;
        public static final int keyF=0x7f09000f;
        public static final int keyG=0x7f090010;
        public static final int keyH=0x7f090011;
        public static final int keyHyphen=0x7f09001e;
        public static final int keyI=0x7f090008;
        public static final int keyJ=0x7f090012;
        public static final int keyK=0x7f090013;
        public static final int keyL=0x7f090014;
        public static final int keyM=0x7f09001d;
        public static final int keyN=0x7f09001c;
        public static final int keyNext=0x7f09005d;
        public static final int keyNumericKeyPad=0x7f090025;
        public static final int keyO=0x7f090009;
        public static final int keyP=0x7f09000a;
        public static final int keyPrevious=0x7f09005c;
        public static final int keyQ=0x7f090001;
        public static final int keyR=0x7f090004;
        public static final int keyS=0x7f09000d;
        public static final int keySpace=0x7f090021;
        public static final int keyT=0x7f090005;
        public static final int keyTabLeft=0x7f090024;
        public static final int keyTabRight=0x7f090026;
        public static final int keyU=0x7f090007;
        public static final int keyV=0x7f09001a;
        public static final int keyW=0x7f090002;
        public static final int keyX=0x7f090018;
        public static final int keyY=0x7f090006;
        public static final int keyZ=0x7f090017;
        public static final int marginTxt=0x7f090049;
        public static final int meeterReadingView=0x7f090027;
        public static final int menu_settings=0x7f0900a3;
        public static final int messageText=0x7f090028;
        public static final int messageTv=0x7f090075;
        public static final int newCmdTv=0x7f09002e;
        public static final int nextPrNumberBtn=0x7f0900a2;
        public static final int nextPrNumberTv=0x7f09006c;
        public static final int notifyPnl=0x7f090045;
        public static final int notifyText=0x7f090046;
        public static final int oldPrAmountTv=0x7f090034;
        public static final int oldPrDateDp=0x7f090035;
        public static final int oldPrNumberTv=0x7f090033;
        public static final int pairBtn=0x7f090044;
        public static final int partPmtBtn=0x7f090090;
        public static final int pefctdtTv=0x7f09008c;
        public static final int prList=0x7f09003f;
        public static final int prListPnl=0x7f090097;
        public static final int prTitle=0x7f090069;
        public static final int printDuplicatePrBtn=0x7f09006b;
        public static final int printFormatSp=0x7f09006f;
        public static final int printPreviewTv=0x7f09003d;
        public static final int printerSetupBtn=0x7f0900a1;
        public static final int pushOnDemandBtn=0x7f09004c;
        public static final int rcAmountSp=0x7f090074;
        public static final int rcAmountTv=0x7f090037;
        public static final int rcCodeTv=0x7f09007b;
        public static final int rcTv=0x7f090082;
        public static final int readyCount=0x7f09004b;
        public static final int remarksSp=0x7f090071;
        public static final int reportContentPane=0x7f09007f;
        public static final int reportsBtn=0x7f090059;
        public static final int scNoTv=0x7f090080;
        public static final int scrollView=0x7f09003e;
        public static final int searchFiltersView=0x7f090088;
        public static final int searchResultsListView=0x7f09009f;
        public static final int searchResultsView=0x7f09009a;
        public static final int searchServicesBtn=0x7f090058;
        public static final int secDistLv=0x7f090089;
        public static final int sectionAbstractBtn=0x7f090086;
        public static final int sectionDistributionContentPane=0x7f09007e;
        public static final int sectionName=0x7f09003a;
        public static final int sectionTv=0x7f09007d;
        public static final int serviceNumber=0x7f090093;
        public static final int setupBtn=0x7f09005a;
        public static final int tableRow1=0x7f090023;
        public static final int tableRow2=0x7f090000;
        public static final int tableRow3=0x7f09000b;
        public static final int tableRow4=0x7f090015;
        public static final int tableRow5=0x7f09001f;
        public static final int testPrintBtn=0x7f090070;
        public static final int text1=0x7f09006a;
        public static final int text2=0x7f090099;
        public static final int text3=0x7f090098;
        public static final int textView1=0x7f090040;
        public static final int textView2=0x7f090041;
        public static final int toDateDp=0x7f090077;
        public static final int totalPrsTxt=0x7f09004a;
        public static final int totalServicesTxt=0x7f09004d;
        public static final int uscNumber=0x7f09003c;
        public static final int versionText=0x7f090057;
    }
    public static final class layout {
        public static final int alpha_key_pad=0x7f030000;
        public static final int bill_collection_add_demand_view=0x7f030001;
        public static final int bill_collection_confirmation_view=0x7f030002;
        public static final int bill_collection_exclude_arrear=0x7f030003;
        public static final int bill_collection_view=0x7f030004;
        public static final int bill_details_view=0x7f030005;
        public static final int bill_item=0x7f030006;
        public static final int d_p_a_view=0x7f030007;
        public static final int dashboard=0x7f030008;
        public static final int device_file_item=0x7f030009;
        public static final int device_files_view=0x7f03000a;
        public static final int horizontal_line=0x7f03000b;
        public static final int horizontal_spacer=0x7f03000c;
        public static final int main=0x7f03000d;
        public static final int navigation_key_pad=0x7f03000e;
        public static final int numeric_key_pad=0x7f03000f;
        public static final int numeric_nav_key_pad=0x7f030010;
        public static final int numeric_tabbed_key_pad=0x7f030011;
        public static final int old_navigation_key_pad=0x7f030012;
        public static final int old_numeric_key_pad=0x7f030013;
        public static final int old_numeric_nav_key_pad=0x7f030014;
        public static final int pr_details_view=0x7f030015;
        public static final int pr_list_item=0x7f030016;
        public static final int pr_number_setup_view=0x7f030017;
        public static final int printer_setup_view=0x7f030018;
        public static final int remarks_view=0x7f030019;
        public static final int report_amount_abstract_filter_view=0x7f03001a;
        public static final int report_amount_abstract_footer_item=0x7f03001b;
        public static final int report_amount_abstract_item=0x7f03001c;
        public static final int report_amount_abstract_section_or_distribution_item=0x7f03001d;
        public static final int report_amount_abstract_view=0x7f03001e;
        public static final int report_pr_heads_item=0x7f03001f;
        public static final int report_pr_heads_section_distribution_item=0x7f030020;
        public static final int report_pr_heads_view=0x7f030021;
        public static final int report_section_abstract_view=0x7f030022;
        public static final int reports_view=0x7f030023;
        public static final int search_filters_view=0x7f030024;
        public static final int service_details_view=0x7f030025;
        public static final int service_search_filter_item=0x7f030026;
        public static final int service_search_item=0x7f030027;
        public static final int servicesearch=0x7f030028;
        public static final int servicesearchview=0x7f030029;
        public static final int setup=0x7f03002a;
        public static final int simple_list_item_1_black_bg=0x7f03002b;
        public static final int spinner_item=0x7f03002c;
    }
    public static final class menu {
        public static final int ecollector=0x7f080000;
    }
    public static final class raw {
        public static final int small_bell_ring_01=0x7f040000;
    }
    public static final class string {
        public static final int PrinterSetup=0x7f06000b;
        public static final int amountAbstract=0x7f06000c;
        public static final int app_name=0x7f060000;
        public static final int checkDataUpdates=0x7f06000a;
        public static final int detailedReport=0x7f06000e;
        public static final int hello_world=0x7f060001;
        public static final int importData=0x7f060008;
        public static final int menu_settings=0x7f060002;
        public static final int nextPrNumber=0x7f060010;
        public static final int pairDevice=0x7f060009;
        public static final int prepareStats=0x7f06000f;
        public static final int reports=0x7f060006;
        public static final int searchServiceTitle=0x7f060005;
        public static final int sectionAbstract=0x7f06000d;
        public static final int serviceSearch=0x7f060004;
        public static final int setup=0x7f060007;
        public static final int title_activity_ecollector=0x7f060003;
    }
    public static final class style {
        public static final int AppTheme=0x7f070000;
    }
}
