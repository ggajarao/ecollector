package ecollector.device;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import ebilly.gwtsvcproxy.SyncProxy;
import ecollector.common.NetConstants;
import ecollector.device.db.EcollectorDB;
import ecollector.device.net.CloudServices;
import ecollector.device.print.BlueToothPrinter;
import ecollector.device.view.action.ReportsAction;
import ecollector.device.view.action.ServiceSearchAction;
import ecollector.device.view.action.SetupAction;
import ecollector.device.view.action.ShowDevicePairingActivity;

public class Ecollector extends Activity {
	
	private ViewGroup mDashboardPnl;
	private DashboardView mDashboardView;
	private Button mServiceSearchBtn;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        /*Initializes network connection pool*/
        SyncProxy.releaseResources();
        /* Initializes cloud services */
		CloudServices.init(this);
		
		/*Initialize Bluetooth printer*/
		BlueToothPrinter.initPrinter(this.getContentResolver());
        
		ExternalStorageState.startWatchingExternalStorage(this);
		
		createUi();
		
		// Schedule delta tick consumer
		Util.scheduleDeltaTickConsumer(this.getApplicationContext());
		
		// Schedule Pr Push Service
		Util.schedulePrPushService(this.getApplicationContext());
		
		// Schedule Command Execution Service
		Util.scheduleCommandExecutorService(this.getApplicationContext());
		
		// Schedule Pr Archive Service
		Util.schedulePrArchiveService(this.getApplicationContext());
		
		// WARNING: THIS SNIPPET IS ONLY FOR TEST PURPOSE
		// DO NOT DISTRIBUTE IN PRODUCTION. MAKE SURE THE SERVICE
		// ENTIRY IN AndroidManifest.xml IS ALSO REMOVED.
		/*TextView appTitle = (TextView)findViewById(R.id.appTitle);
		appTitle.setBackgroundColor(Color.RED);
		appTitle.setText(appTitle.getText()+" - TEST VERSION");
		Util.schedulePrGenerator(this.getApplicationContext());*/
    }
    
    private void createUi() {
    	setContentView(R.layout.main);
        
    	TextView versionText = (TextView)findViewById(R.id.versionText);
    	versionText.setText(NetConstants.VERSION);
    	
        mServiceSearchBtn = (Button)findViewById(R.id.searchServicesBtn);
        mServiceSearchBtn.setOnClickListener(new ServiceSearchAction(this));
        
        Button reportsBtn = (Button)findViewById(R.id.reportsBtn);
        reportsBtn.setOnClickListener(new ReportsAction(this));
        
        Button setupButton = (Button)findViewById(R.id.setupBtn);
        setupButton.setOnClickListener(new SetupAction(this));
        
        mDashboardPnl = (ViewGroup)findViewById(R.id.dashboardPnl);
        
        mDashboardView = new DashboardView(this,mDashboardPnl);
        mDashboardView.onCreate();
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    	
    	boolean isDevicePaired = EcollectorDB.isDevicePaired(this.getContentResolver());
        if (isDevicePaired) {
        	//Toast.makeText(this, "Device is paired", Toast.LENGTH_SHORT).show();
        } else {
        	ShowDevicePairingActivity dpa = new ShowDevicePairingActivity(this);
        	dpa.onClick(null);
        }
        
        if (EcollectorDB.isCollectionLocked(this)) {
        	mServiceSearchBtn.setEnabled(false);
        	mServiceSearchBtn.setTextColor(Color.GRAY);
        }
        mDashboardView.onResume();
    }
    
    @Override
    protected void onPause() {
    	super.onPause();
    	mDashboardView.onPause();
    	CloudServices.release();
    }
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
    	ExternalStorageState.stopWatchingExternalStorage(this);
    	SyncProxy.releaseResources();
    	mDashboardView.onDestroy();
    }
}