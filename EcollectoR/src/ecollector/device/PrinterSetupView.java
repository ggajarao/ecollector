package ecollector.device;

import android.bluetooth.BluetoothAdapter;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import ecollector.device.android.framework.SimpleCursorAdapter;
import ecollector.device.db.EcollectorDB;
import ecollector.device.print.BlueToothPrinter;
import ecollector.device.view.FragmentView;
import ecollector.device.view.FragmentViewManager;
import ecollector.device.view.ViewManager;
import ecollector.device.view.input.KeyPad;
import ecollector.device.view.input.KeyPadListener;
import ecollector.device.view.input.NavigationKeyPad;

public class PrinterSetupView extends FragmentView implements KeyPadListener {
	
	//private ViewManager mViewManager;
	private NavigationKeyPad mNavikationKeyPad;
	
	//private LayoutInflater mInflater;
	private LinearLayout mView;
	//private TextView mMessageTextTv;
	private TextView mBluetoothStatusTv;
	private String mDevicePrinterName;
	private String mDevicePrinterAddrs;
	private Button testPrintBtn;
	private String mPrintFormat;
	
	public PrinterSetupView() {
		
	}
	
	/*public PrinterSetupView(ViewManager viewManager) {
		//mViewManager = viewManager;
		//initPrinterCfg();
	}*/
	
	
	private void initPrinterCfg() {
		String[] printerCfg = EcollectorDB.fetchPrinterConfig(this.getActivity().getContentResolver());
		if (printerCfg != null && printerCfg.length == 2) {
			mDevicePrinterName = printerCfg[0];
			mDevicePrinterAddrs = printerCfg[1];
		}
		mPrintFormat = EcollectorDB.fetchPrintFormat(
				this.getActivity().getApplicationContext()).toString();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		initPrinterCfg();
		//mInflater = inflater;
		mView = (LinearLayout)inflater.inflate(R.layout.printer_setup_view, null);
		//mMessageTextTv = (TextView)mView.findViewById(R.id.messageText);
		mBluetoothStatusTv = (TextView)mView.findViewById(R.id.bluetoothStatusTv);
		updateBluetoothStatus();
		testPrintBtn = (Button)mView.findViewById(R.id.testPrintBtn);
		
		testPrintBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				testPrint();
			}
		});
		
		prepareBtDevicesUi();
		preparePrintFormatUi();
		
		mNavikationKeyPad = new NavigationKeyPad();
		mNavikationKeyPad.setNextEnabled(false);
		//((FragmentViewManager)getActivity()).showKeyPad(mNavikationKeyPad);
		getFragmentViewManager().showKeyPad(mNavikationKeyPad);
		return mView;
	}
	
	private void prepareBtDevicesUi() {
		Cursor btDevicesCursor = EcollectorDB.createBluetoothDevicesCursor();
		Spinner btDevicesSp = (Spinner)mView.findViewById(R.id.bluetoothDevicesSp);
		SimpleCursorAdapter adapter = new SimpleCursorAdapter(
				this.getActivity(), // context
				R.layout.spinner_item, // layout
				btDevicesCursor, // cursor
				new String[]{"_ID","BT_DEVICE","BT_DEVICE_ADDRESS"}, // Columns for binding
				new int[] {android.R.id.text1} // Corresponding column binding View 
				);
		adapter.setViewBinder(new SimpleCursorAdapter.ViewBinder(){
			public boolean setViewValue(View view, Cursor cursor,
					int columnIndex) {
				TextView tv = (TextView)view.findViewById(android.R.id.text1);
				String id = cursor.getString(0);
				String btDeviceName = cursor.getString(1);
				String btDeviceAddrs = cursor.getString(2);
				if (id == null) {
					return false;
				}
				if (id.equalsIgnoreCase("-1")) {
					tv.setText(btDeviceAddrs);
				} else {
					tv.setText(btDeviceName);//+" "+btDeviceAddrs);
				}
				
				return true;
			}
		});
		
		btDevicesSp.setAdapter(adapter);
		btDevicesSp.setOnItemSelectedListener(new OnItemSelectedListener(){
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				Cursor cursor = (Cursor)parent.getItemAtPosition(position);
				onBtDeviceSelected(cursor.getString(1),cursor.getString(2));// Index of btName, btAddress, refer EcollectorDB.createBluetoothDevicesCursor();
			}
			
			public void onNothingSelected(AdapterView<?> parent) {
				
			}
		});
		
		// Pre-populate printer selection
		if (mDevicePrinterName != null && mDevicePrinterAddrs != null) {
			btDevicesCursor.moveToFirst();
			int p = 0;
			do {
				String printerName = btDevicesCursor.getString(1);
				String printerAddrs = btDevicesCursor.getString(2);
				if (printerAddrs != null && printerAddrs.equalsIgnoreCase(
						mDevicePrinterAddrs) &&
					printerName != null && printerName.equalsIgnoreCase(
							mDevicePrinterName)) {
					btDevicesSp.setSelection(p);
					break;
				}
				p++;
			} while (btDevicesCursor.moveToNext());
		}
	}
	
	private void preparePrintFormatUi() {
		Cursor printFormatsCursor = EcollectorDB.createPrintFormatsCursor();
		Spinner printFormatSp = (Spinner)mView.findViewById(R.id.printFormatSp);
		SimpleCursorAdapter adapter = new SimpleCursorAdapter(
				this.getActivity(), // context
				R.layout.spinner_item, // layout
				printFormatsCursor, // cursor
				new String[]{"_ID","FORMAT"}, // Columns for binding
				new int[] {android.R.id.text1} // Corresponding column binding View 
				);
		adapter.setViewBinder(new SimpleCursorAdapter.ViewBinder(){
			public boolean setViewValue(View view, Cursor cursor,
					int columnIndex) {
				TextView tv = (TextView)view.findViewById(android.R.id.text1);
				String id = cursor.getString(0);
				String printFormatName = cursor.getString(1);
				if (id == null) {
					return false;
				}
				tv.setText(printFormatName);
				return true;
			}
		});
		
		printFormatSp.setAdapter(adapter);
		printFormatSp.setOnItemSelectedListener(new OnItemSelectedListener(){
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				Cursor cursor = (Cursor)parent.getItemAtPosition(position);
				onPrintFormatSelected(cursor.getString(1));// Index of fromat name, btAddress, refer EcollectorDB.createBluetoothDevicesCursor();
			}
			
			public void onNothingSelected(AdapterView<?> parent) {
				
			}
		});
		
		// Pre-populate printer selection
		if (mPrintFormat != null && mPrintFormat != null) {
			printFormatsCursor.moveToFirst();
			int p = 0;
			do {
				String formatName = printFormatsCursor.getString(1);
				if (formatName != null && formatName.equalsIgnoreCase(
						mPrintFormat)) {
					printFormatSp.setSelection(p);
					break;
				}
				p++;
			} while (printFormatsCursor.moveToNext());
		}
	}


	private void updateBluetoothStatus() {
		if (BluetoothAdapter.getDefaultAdapter().isEnabled()) {
			mBluetoothStatusTv.setText("Bluetooth is ON");
			mBluetoothStatusTv.setTextColor(Color.GREEN);
			prepareBtDevicesUi();
		} else {
			mBluetoothStatusTv.setText("Bluetooth is OFF. Turning on, please wait...");
			mBluetoothStatusTv.setTextColor(Color.RED);
			
			if (btOnAttempts < 5) {
				mHandler.postDelayed(new Runnable(){
					public void run() {
						turnBluetoothOn();
					}
				}, 1000);
			}
		}
	}

	Handler mHandler = new Handler();
	private int btOnAttempts = 0;
	private void turnBluetoothOn() {
		btOnAttempts++;
		BluetoothAdapter.getDefaultAdapter().enable();
		try{Thread.sleep(1000);}catch(Exception e){}
		updateBluetoothStatus();
	}


	private void testPrint() {
		if (mDevicePrinterName == null || mDevicePrinterAddrs == null) {
			Toast.makeText(this.getActivity(), "Please configure printer", Toast.LENGTH_SHORT).show();
		} else {
			
			initPrinterCfg();
			String printText =  
				"Printer Test Successful, date: "+Util.getReadableDate(Util.getCurrentDate());
			BlueToothPrinter.getInstance(mDevicePrinterAddrs).print(printText.getBytes());
		}
	}
	
	protected void onBtDeviceSelected(String btDeviceName, String btDeviceAddrs) {
		if (btDeviceName != null && btDeviceName.equalsIgnoreCase("NONE")) {
			return;
		}
		EcollectorDB.savePrinterConfig(this.getActivity().getContentResolver(), 
				btDeviceName, btDeviceAddrs);
		BlueToothPrinter.releasePrinter();
	}
	
	protected void onPrintFormatSelected(String formatName) {
		EcollectorDB.PrintFormat printFormat = null;
		if (EcollectorDB.PrintFormat.PLAIN_PAPER.toString().equals(formatName)) {
			printFormat = EcollectorDB.PrintFormat.PLAIN_PAPER;
		} else if (EcollectorDB.PrintFormat.PRE_PRINTED_PAPER.toString().equals(formatName)) {
			printFormat = EcollectorDB.PrintFormat.PRE_PRINTED_PAPER;
		}
		EcollectorDB.savePrintFormat(this.getActivity().getContentResolver(), 
				printFormat);
	}


	@Override
	public void onKeyClick(char key) {
		switch(key) {
		case KeyPad.KEY_PREVIOUS:
			//((FragmentViewManager)getActivity()).showPrevious();
			getFragmentViewManager().showPrevious();
			break;
		}
	}
}
