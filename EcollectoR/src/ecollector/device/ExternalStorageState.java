package ecollector.device;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Environment;
import android.util.Log;

public class ExternalStorageState {
	
	private static BroadcastReceiver mExternalStorageReceiver;
	private static IntentFilter filter;
	static {
		mExternalStorageReceiver = new BroadcastReceiver() {
	        @Override
	        public void onReceive(Context context, Intent intent) {
	            Log.i("ebilly.ExternalStorageState", "Storage: " + intent.getData());
	            updateExternalStorageState();
	        }
	    };
	    
	    filter = new IntentFilter();
	    filter.addAction(Intent.ACTION_MEDIA_MOUNTED);
	    filter.addAction(Intent.ACTION_MEDIA_REMOVED);
	}
	public static boolean isExternalStorageAvailable = false;
	public static boolean isExternalStorageWriteable = false;
	
	private static void updateExternalStorageState() {
	    String state = Environment.getExternalStorageState();
	    if (Environment.MEDIA_MOUNTED.equals(state)) {
	        isExternalStorageAvailable = isExternalStorageWriteable = true;
	    } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
	        isExternalStorageAvailable = true;
	        isExternalStorageWriteable = false;
	    } else {
	        isExternalStorageAvailable = isExternalStorageWriteable = false;
	    }
	    // TOTDO: probably we should fire an event to notify this event !
	    // 
	}
	
	public static boolean isExternalStorageAvailable() {
		String state = Environment.getExternalStorageState();
		return Environment.MEDIA_MOUNTED.equalsIgnoreCase(state);
	}

	public static void startWatchingExternalStorage(Activity activity) {
	    activity.registerReceiver(mExternalStorageReceiver, filter);
	    updateExternalStorageState();
	}

	public static void stopWatchingExternalStorage(Activity activity) {
	    activity.unregisterReceiver(mExternalStorageReceiver);
	}
}
