package ecollector.device;

import android.content.ContentResolver;
import android.graphics.Color;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.DeviceFileUi;
import ecollector.device.view.action.DownloadAndImportDataAction;
import ecollector.device.view.action.HandlerActions;
import ecollector.device.view.input.NavigationKeyPad;

public class DownloadableDeviceFileItem {
	
	class MyHandler extends Handler {
		public int mCurrentMessage;
		public Message mMsg;
		public DownloadableDeviceFileItem mParent;
		public MyHandler(DownloadableDeviceFileItem parent, Looper looper) {
			super(looper);
			mParent = parent;
		}
		public void handleMessage(Message msg) {
			mCurrentMessage = msg.arg1;
			mMsg = msg;
			mParent.updateUi();
		}
	};
	
	private MyHandler mHandler = null;
	private DeviceFileUi mDeviceFile;
	private DownloadAndImportDataAction mAction;
	private ContentResolver mContentResolver;
	private TextView mFileMessageTv;
	private Button mImportBtn;
	private ViewGroup mFileItemView;
	private ViewGroup mInfromationPnl;
	private ProgressBar mProgressBar;
	private int mTotalRecords;
	private int mImportedCount;
	private int mTotalBytes = 0;
	private int mDownloadedBytes;
	private DownloadableDeviceFiles mParent;
	private boolean mIsImportInProgress;
	private boolean mIsImportCompleted = false;
	
	public DownloadableDeviceFileItem(DeviceFileUi deviceFile, ContentResolver contentResolver,
			DownloadableDeviceFiles downloadableDeviceFiels) {
		mParent = downloadableDeviceFiels;
		mContentResolver = contentResolver;
		mDeviceFile = deviceFile;
		/*mAction = new DownloadAndImportDataAction(
				mDeviceFile.getDeviceServicesFileKey(),
				Util.getDownloadDir(),mHandler,mContentResolver);*/
	}
	public void prepareUi(LayoutInflater inflater, ViewGroup parent, Looper looper) {
		
		mHandler = new MyHandler(this, looper);
		mFileItemView = (ViewGroup)inflater.inflate(R.layout.device_file_item, null);
		parent.addView(mFileItemView);
		
		mInfromationPnl = (ViewGroup)mFileItemView.findViewById(R.id.informationPnl);

		TextView tv = (TextView)mFileItemView.findViewById(R.id.fileName);
		tv.setText(mDeviceFile.getDeviceServicesFileName());

		tv = (TextView)mFileItemView.findViewById(R.id.fileDate);
		tv.setText(Util.getReadableDate(mDeviceFile.getDeviceFileStagingDate()));
		
		tv = (TextView)mFileItemView.findViewById(R.id.fileImportMethodTv);
		String importMethod = mDeviceFile.getRemoteDeviceImportMethod();
		if (AppConfig.REMOTE_DEVICE_IMPORT_METHOD.ADD_TO_EXISTING_DATA.toString()
				.equals(importMethod)) {
			tv.setText("Adds to existing Services");
			tv.setTextColor(Color.GREEN);
		} else if (AppConfig.REMOTE_DEVICE_IMPORT_METHOD.CLEAR_EXISTING_DATA.toString()
				.equals(importMethod)) {
			tv.setText("Removes existing Services");
			tv.setTextColor(Color.RED);
		}

		mFileMessageTv = (TextView)mFileItemView.findViewById(R.id.fileMessage);
		mFileMessageTv.setText("");
		
		mImportBtn = (Button)mFileItemView.findViewById(R.id.importBtn);
		
		if (mAction == null) {
			mAction = new DownloadAndImportDataAction(
					mDeviceFile,
					Util.getDownloadDir(),mHandler,mContentResolver,
					mFileItemView.getContext());
		} else {
			mAction.prepareUi(mHandler);
		}
		
		mImportBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Some times, due to some performance issue..
				// this action is taking long time 
				// to begin. So disabling the button soonest.
				mImportBtn.setVisibility(View.GONE);
				Message msg = Message.obtain();
				msg.arg1 = HandlerActions.BUSSY;
				mHandler.sendMessage(msg);
				mAction.onClick(v);
			}
		});
		
		ViewGroup hLine = (ViewGroup)inflater.inflate(R.layout.horizontal_line, null);
		mFileItemView.addView(hLine);
	}
	
	private void updateUi() {
		boolean freezeActionControls = true;
		switch(mHandler.mCurrentMessage) {
		case HandlerActions.DOWNLOADING_STARTED:
			if (mHandler.mMsg.obj != null) {
				mTotalBytes = (int)((Long)mHandler.mMsg.obj).longValue();
				if (mTotalBytes < 0) mTotalBytes = 0;
			}
			if (mTotalBytes > 0) {
				createProgressBar(mTotalBytes);
			}
			mFileMessageTv.setText(mTotalBytes+"b");
			break;
		case HandlerActions.DOWNLOADING_PROGRESS:
			mDownloadedBytes += mHandler.mMsg.arg2;
			if (mTotalBytes == 0) {
				mFileMessageTv.setText(mDownloadedBytes+"bytes downloaded");
			} else {
				updateProgressBar(mDownloadedBytes);
				int p = (mDownloadedBytes*100/mTotalBytes);
				mFileMessageTv.setText("Downloaded "+p+"%   "+((int)(mDownloadedBytes/1024))+"kb/"+((int)(mTotalBytes/1024))+"kb");
			}
			break;
		case HandlerActions.DOWNLOADING_COMPLETE:
			hideProgressBar();
			mFileMessageTv.setText("File Downloaded...");
			break;
		case HandlerActions.DOWNLOADING_FAILED:
			hideProgressBar();
			freezeActionControls = false;
			mFileMessageTv.setText("Download Failed");
			break;
		case HandlerActions.DOWNLOADING_FILE:
			mFileMessageTv.setText("Downloading File...");
			break;
		case HandlerActions.IMPORTING_STARTED:
			mFileMessageTv.setText("Starting Import...");
			break;
		case HandlerActions.DTERMINING_TOTAL_RECORDS:
			mTotalRecords = mHandler.mMsg.arg2;
			createProgressBar(mTotalRecords);
			mFileMessageTv.setText("");
			break;
		case HandlerActions.IMPORTING_COMPLETED:
			mFileMessageTv.setText("File import Completed");
			hideProgressBar();
			freezeActionControls = false;
			mIsImportCompleted = true;
			break;
		case HandlerActions.IMPORTING_COMPUTING_STATS:
			mFileMessageTv.setText("Computing stats...");
			break;
		case HandlerActions.IMPORTING_MEDIA_UNAVAILABLE:
			mFileMessageTv.setText("Memory card unplugged");
			hideProgressBar();
			freezeActionControls = false;
			break;
		case HandlerActions.IMPORTING_RECORDS:
			mImportedCount = mHandler.mMsg.arg2;
			updateProgressBar(mImportedCount);
			int percent = (mImportedCount*100/mTotalRecords);
			mFileMessageTv.setText("Imported "+percent+"%   "+mImportedCount+"/"+mTotalRecords);
			break;
		case HandlerActions.PROBLEM_READING_IMPORT_FILE:
			hideProgressBar();
			freezeActionControls = false;
			break;
		case HandlerActions.DELETING_SERVICES_STARTED:
			mFileMessageTv.setText("Started Deleting Services...");
			break;
		case HandlerActions.DELETING_SERVICES_ENDED:
			mFileMessageTv.setText("Completed Deleting Services");
			break;
		case HandlerActions.BUSSY:
			mFileMessageTv.setText("Please wait...");
			break;
		}
		
		mIsImportInProgress = freezeActionControls;
		mParent.freezeActionControls(freezeActionControls);
	}
	
	void enableImportButton(boolean enableImportButton) {
		if (mIsImportCompleted) {
			mImportBtn.setVisibility(View.GONE);
		} else if (enableImportButton) {
			mImportBtn.setVisibility(View.VISIBLE);
		} else {
			mImportBtn.setVisibility(View.GONE);
		}
	}
	
	private void createProgressBar(int max) {
		mProgressBar = 
			new ProgressBar(mFileItemView.getContext(),
					null,android.R.attr.progressBarStyleHorizontal);
		mInfromationPnl.addView(mProgressBar);
		mProgressBar.setMax(max);
	}
	
	private void updateProgressBar(int progress) {
		if (mProgressBar != null) {
			mProgressBar.setProgress(progress);
		}
	}
	
	private void hideProgressBar() {
		if (mProgressBar != null) {
			mProgressBar.setVisibility(View.GONE);
		}
	}
	
	public String getFileKey() {
		return mDeviceFile.getDeviceServicesFileKey();
	}
	
	public boolean isImportInProgress() {
		return mIsImportInProgress;
	}
}
