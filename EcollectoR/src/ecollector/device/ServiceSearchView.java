package ecollector.device;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.FilterQueryProvider;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import ecollector.common.ServiceColumns;
import ecollector.device.android.framework.SimpleCursorAdapter;
import ecollector.device.db.EcollectorDB;
import ecollector.device.view.FragmentView;
import ecollector.device.view.ViewManager;
import ecollector.device.view.action.ShowEcollector;
import ecollector.device.view.input.KeyPadListener;
import ecollector.device.view.input.NumericKeyPad;

public class ServiceSearchView extends FragmentView implements KeyPadListener {
	
	class ServiceCursorAdapter1 extends SimpleCursorAdapter {
		public ServiceCursorAdapter1(Context context, int layout, Cursor c,
				String[] from, int[] to) {
			super(context, layout, c, from, to);
		}
		
		public CharSequence convertToString(Cursor cursor) {
			int colCount = cursor.getColumnCount();
			if (colCount >= 2) {
				return cursor.getString(1);
			}
			return "N/A";
		}
	}
	
	private class ServiceFilterQueryProvider1 implements FilterQueryProvider {
		private Activity activity;
		public ServiceFilterQueryProvider1(Activity activity) {
			this.activity = activity;
		}
		public Cursor runQuery(CharSequence constraint) {
			return EcollectorDB.createServiceSearchCursor(this.activity, constraint,ServiceSearchFilterBean.i);
		}
	}
	
	private static final CharSequence FILTER_DEFAULT_STRING = "Enter Service Number";
	
	private Cursor mCursor;
	private ServiceCursorAdapter1 mCursorAdapter;
	private ServiceFilterQueryProvider1 mFilterQueryProvider;
	
	private Button mFilterBtn;
	private TextView mFilterTextView;
	private ListView mListView;
	private StringBuffer mFilter = new StringBuffer();
	private ViewManager mViewManager;
	
	/*public ServiceSearchView() {
		
	}*/
	
	public ServiceSearchView(/*ViewManager viewManager*/) {
		//mViewManager = viewManager;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mViewManager = getFragmentViewManager();
		mViewManager.setHome(this);
		
		//Reset filter string
		mFilter.setLength(0);
		
		LinearLayout view = (LinearLayout)inflater.inflate(R.layout.servicesearchview, null);
		ListView mListView = (ListView)view.findViewById(R.id.searchResultsListView);
		mListView.setOnItemClickListener(new OnItemClickListener(){
			public void onItemClick(AdapterView<?> adapter, View view,
					int position, long id) {
				processItemClick(adapter,view,position,id);
			}			
		});
		
		if (mCursor == null) {
			mCursor = EcollectorDB.createServiceSearchCursor(this.getActivity(),null,ServiceSearchFilterBean.i);
			mCursorAdapter = new ServiceCursorAdapter1(
					this.getActivity(), // context
					//android.R.layout.simple_list_item_2, // layout
					R.layout.service_search_item,
					mCursor, // cursor
					new String[]{ServiceColumns.SC_NO, ServiceColumns.NAME, ServiceColumns._PR_IDS}, // Columns for binding
					new int[] {R.id.text1,R.id.text2,R.id.text3} // Corresponding column binding View 
					);
			mFilterQueryProvider = new ServiceFilterQueryProvider1(this.getActivity());
			mCursorAdapter.setFilterQueryProvider(mFilterQueryProvider);
			mCursorAdapter.setViewBinder(new SimpleCursorAdapter.ViewBinder(){
				public boolean setViewValue(View view, Cursor cursor,
						int columnvIndex) {
					// Get the parent view, so that we can manipulate the other
					// view items in it.
					view = (view.getParent() instanceof View) ? (View)view.getParent():view;
					// one more level up, depends on the layout !
					view = (view.getParent() instanceof View) ? (View)view.getParent():view;
					
					TextView serviceNoTv = (TextView)view.findViewById(R.id.text1);
					TextView custNameTv = (TextView)view.findViewById(R.id.text2);
					TextView prNosTv = (TextView)view.findViewById(R.id.text3);
					String id = cursor.getString(0);
					String serviceNo = cursor.getString(1);
					String custName = cursor.getString(2);
					String prNos = cursor.getString(3);

					if (id == null) {
						return true;
					}
					serviceNoTv.setText(serviceNo);
					custNameTv.setText(custName);
					if (prNos != null) {
						prNosTv.setVisibility(View.VISIBLE);
						prNosTv.setText(prNos);
					} else {
						prNosTv.setVisibility(View.GONE);
					}
					return true;
				}
			});
			
		}
		mListView.setAdapter(mCursorAdapter);
		
		mFilterTextView = (TextView)view.findViewById(R.id.filterText);
		showMessage(mFilter);
		
		mFilterBtn = (Button)view.findViewById(R.id.filterBtn);
		mFilterBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ServiceSearchFilters s = new ServiceSearchFilters(mViewManager);
				mViewManager.showView(s);
			}
		});
		
		Button homeBtn = (Button)view.findViewById(R.id.homeBtn);
		homeBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				ShowEcollector s = new ShowEcollector((Activity)mViewManager.getActivity());
				s.onClick(null);
			}
		});
		
		updateFilterBtnState();
		setFilter(mFilter);
		
		NumericKeyPad nkp = new NumericKeyPad();
		mViewManager.showKeyPad(nkp);
		return view;
	}
	
	private void updateFilterBtnState() {
		if (ServiceSearchFilterBean.i.isFiltering()) {
			mFilterBtn.setBackgroundDrawable(
					Util.getFilterOnBackground(mViewManager.getActivity()));
		} else {
			mFilterBtn.setBackgroundDrawable(
					Util.getFilterOffBackground(mViewManager.getActivity()));
		}
		mFilterBtn.setPadding(0, 0, 0, 0);
	}
	
	private void processItemClick(AdapterView<?> adapter, View view,
			int position, long id) {
		//Toast.makeText(ServiceSearchView.this.getActivity(), "Service Id selected: "+id, Toast.LENGTH_SHORT).show();
		if (EcollectorDB.isCollectionLocked(mViewManager.getActivity())) {
			Toast.makeText(mViewManager.getActivity(), "Collection Locked !!", Toast.LENGTH_SHORT).show();
			return;
		}
		
		BillCollectionBean mrBean = new BillCollectionBean(id, mViewManager);
		Fragment f;
		if (mrBean.isAmountCollected()) {
			f = new BillDetailsView(/*mViewManager,*/mrBean);
		} else {
			f = new ServiceDetailsView(/*mViewManager,*/ mrBean);
		}
		mViewManager.showView(f);
	}
	
	public void setFilter(CharSequence filter) {
		mCursorAdapter.getFilter().filter(filter);
	}
	
	public void showMessage(CharSequence filter) {
		if (filter == null || filter.length() == 0) {
			mFilterTextView.setText(FILTER_DEFAULT_STRING);
		} else {
			mFilterTextView.setText(filter);
		}
	}

	public void onKeyClick(char key) {
		if (key == NumericKeyPad.KEY_DEL) {
			mFilter.setLength(0);
		} else if (key == NumericKeyPad.KEY_BACKSPACE) {
			if (mFilter.length() > 1)
				mFilter.setLength(mFilter.length()-1);
			else {
				mFilter.setLength(0);
			}
		} else {
			mFilter.append(key);
		}
		showMessage(mFilter);
		setFilter(mFilter);	
	}
}