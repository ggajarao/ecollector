package ecollector.device.autobot;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import ecollector.device.db.EcollectorDB;

public class AutoPrGeneratorService extends IntentService {

	private static final String TAG = "AutoPrGeneratorService";
	
	public static final int EXECUTION_INTERVAL_SECS = 24*60*60;

	public AutoPrGeneratorService() {
		super("AutoPrGeneratorService");
	}

	/**
	 * The IntentService calls this method from the default worker thread with
	 * the intent that started the service. When this method returns, IntentService
	 * stops the service, as appropriate.
	 */
	@Override
	protected void onHandleIntent(Intent intent) {
		
		//WARNING! DO NOT HAVE THIS UNCOMMENT IN PRODUCTION
		// THIS WOULD HANG THE SERVICE
		//android.os.Debug.waitForDebugger(); 
		
		try {
			EcollectorDB.autoGeneratePrs(this);
		} catch (Throwable t) {
			Log.i(TAG, "Error",t);
		}
	}
}