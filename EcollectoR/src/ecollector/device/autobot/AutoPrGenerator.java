package ecollector.device.autobot;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import ecollector.device.BillCollectionBean;
import ecollector.device.view.ViewManager;
import ecollector.device.view.input.KeyPad;

/**
 *  Automating Bill collection for the purpose of testing

	// Service Details View
	BillCollectionBean mrBean = new BillCollectionBean(id, mViewManager);
	mBcBean.startOver();
	mBcBean.resetArrearsSelectedToAddFlag();
	if (!mBcBean.mIsCursorEmpty) then procede
	
	Next
	
	// Bill collection view
	
	if (!mBcBean.mIsCursorEmpty) then procede
	mBcBean.prepareForCollection();
	input into mBcBean.mVolatileAmountCollected from mBcBean.mVolatileCollectableAmount
	if (mBcBean.isCollectedAmountValid()) 
		then mBcBean.computeBill();
	             Next
	
	//
	
	// BillDetailsView
	mBcBean.computeBill();
	if (!mBcBean.isAmountCollected() && !mBcBean.isForPrintDuplicatePr()) {
	    mBcBean.save();
	
 * 
 * @author vkadiyal
 */
public class AutoPrGenerator {
	// IMPLEMENTATION PENDING.
	public static class AutoPrGenViewManager implements ViewManager {
		
		private Context mContext = null;
		public AutoPrGenViewManager(Context context) {
			mContext = context;
		}

		@Override
		public void showView(Fragment mrv) {
			throw new IllegalArgumentException("Unsupported operation");
		}

		@Override
		public void removeScreenFromNavigation() {
			throw new IllegalArgumentException("Unsupported operation");
		}

		@Override
		public void showPrevious() {
			throw new IllegalArgumentException("Unsupported operation");
		}

		@Override
		public void showKeyPad(KeyPad kp) {
			throw new IllegalArgumentException("Unsupported operation");
		}

		@Override
		public void exclusiveContent() {
			throw new IllegalArgumentException("Unsupported operation");
		}

		@Override
		public Context getActivity() {
			return mContext;
		}

		@Override
		public void setHome(Fragment homeView) {
			throw new IllegalArgumentException("Unsupported operation");
			
		}

		@Override
		public void showHome() {
			throw new IllegalArgumentException("Unsupported operation");
			
		}
	};
	
	private ViewManager mViewManager = null;
	
	public AutoPrGenerator(Context context) {
		mViewManager = new AutoPrGenViewManager(context);
	}
	
	public void generatePr(long serviceId) {
		// Service Details View
		BillCollectionBean mBcBean = new BillCollectionBean(serviceId, mViewManager);
		mBcBean.startOver();
		mBcBean.resetArrearsSelectedToAddFlag();
		if (mBcBean.mIsCursorEmpty) return;
		
		//Next
		
		// Bill collection view
		mBcBean.prepareForCollection();
		//input into mBcBean.mVolatileAmountCollected from 
		//mBcBean.mVolatileCollectableAmount
		mBcBean.mVolatileAmountCollected = 
			new StringBuffer(mBcBean.mVolatileCollectableAmount);
		if (mBcBean.isCollectedAmountValid()) {
			mBcBean.computeBill(); 
		}
			
		// Next
		
		// BillDetailsView
		mBcBean.computeBill();
		if (!mBcBean.isAmountCollected() && !mBcBean.isForPrintDuplicatePr()) {
		    mBcBean.save();
		}
	}
}


/*

Address the following areas in BillCollectionBean to avoid
usage of mViewManager. By doing so, BillCollectionBean can 
be used in Services along with Activities

mCursor = EcollectorDB.findServiceById(mViewManager.getActivity(), _id);

Cursor c = 
			EcollectorDB.findPrsByIds(mViewManager.getActivity(),
				prNumbers);

List<String> prNumbers = this.savePrs(mViewManager.getActivity());


EcollectorDB.saveServiceDetails(this._id+"", mViewManager.getActivity(), 
				serviceValues);


EcollectorDB.bumpSummaryReportPrStats(mViewManager.getActivity(), values);


s.append(prBean.getPrintBuffer(mViewManager.getActivity().getApplicationContext()));


*/