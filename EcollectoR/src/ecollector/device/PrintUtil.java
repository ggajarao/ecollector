package ecollector.device;

import ecollector.device.db.EcollectorDB;
import ecollector.device.db.EcollectorDB.PrintFormat;
import android.content.Context;
import android.util.Log;

public class PrintUtil {
	
	private static final String TAG = PrintUtil.class.getName();

	/*
	 Following is a rough estimate of printing time.
	 36 Lines takes 15 seconds.
	 Avg each line takes 15/36 = 0.42 secs/line
	 */
	public static double DELAY_SECS_PER_LINE = 0.45;
	
	public static String BLANK_LINE  = "                        ";
	public static String DASHED_LINE = "------------------------";
	//public static String DASHED_LINE = "_ _ _ _ _ _ _ _ _ _ _ __";
	public static int MAX_CHARS_PER_LINE = 24;
	
	public static long estimatePrintingTime(byte[] printBuffer) {
		int estimatedPrintingTime = 
			1000 *
			(int)(PrintUtil.DELAY_SECS_PER_LINE*(Math.round(
					printBuffer.length/PrintUtil.MAX_CHARS_PER_LINE)));
		Log.i(TAG,"Estimated printing time (millies): "+estimatedPrintingTime);
		return estimatedPrintingTime;
	}
	
	/**
	 * contentJustification can be
	 *   1 - left align
	 *   2 - center align
	 *   3 - right align
	 * 
	 * @param caption
	 * @param maxCaptionLength
	 * @param content
	 * @param contentJustification
	 * @return
	 */
	public static StringBuffer preparePrintLineItem(
			String caption, int maxCaptionLength, 
			String content, int contentJustification, 
			int maxLength) {
		if (caption == null && content == null) return pad(maxLength);

		if (caption == null) caption = "";
		if (content == null) content = "";

		caption = caption.trim();
		content = content.trim();

		String[] captionElements = caption.split(" ");

		if (contentJustification < 1 && contentJustification > 3) {
			throw new IllegalArgumentException("contentJustification unknown !");
		}

		if (caption.length() > maxCaptionLength)
			throw new IllegalArgumentException("caption lenght more than captionIndent");

		if (maxCaptionLength > maxLength) 
			throw new IllegalArgumentException("maxCaptionLenght is greater than maxLenght");

		StringBuffer c = new StringBuffer();
		for (int i = 0; i < captionElements.length; i++) {
			if (i >= captionElements.length - 1) {
				// LAST ELEMENT
				c.append(prepareLineLpad(captionElements[i], maxCaptionLength-c.length()));
			} else {
				c.append(captionElements[i]).append(' ');
			}
		}

		if (contentJustification == 1) {
			c.append(prepareLineRpad(content, maxLength - maxCaptionLength));
		} else if (contentJustification == 2) {
			c.append(prepareLineCalign(content, maxLength - maxCaptionLength));
		} else if (contentJustification == 3) {
			c.append(prepareLineLpad(content, maxLength - maxCaptionLength));
		}

		return new StringBuffer(c.subSequence(0, maxLength));
	}

	public static CharSequence prepareLineCalign(String string, int maxLength) {
		StringBuffer s = new StringBuffer();
		if (string == null || string.trim().length() == 0) {
			return s.append(pad(maxLength));
		}
		string = string.trim();
		int leftPadding = (maxLength - string.length())/2;
		s.append(pad(leftPadding))
		.append(prepareLineRpad(string,maxLength-leftPadding));
		return s;
	}

	// Prepares a printable line, with left padding
	//
	public static StringBuffer prepareLineLpad(String value, int maxLenght) {
		if (value == null || value.trim().length() == 0) value = "";
		StringBuffer sb = new StringBuffer();
		sb.append(pad(maxLenght - value.length())).append(value);
		return sb;
	}

	public static StringBuffer prepareLineRpad(String value, int maxLenght) {
		if (value == null || value.trim().length() == 0) value = "";
		StringBuffer sb = new StringBuffer();
		sb.append(value).append(pad(maxLenght - value.length()));
		return sb;
	}
	
	public static StringBuffer prepareLineRpadMultiLine(String value) {
		if (value == null || value.trim().length() == 0) value = "";
		StringBuffer sb = new StringBuffer();
		sb.append(value);
		return padToNewLine(sb,MAX_CHARS_PER_LINE);
	}

	public static StringBuffer pad(int numSpaces) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < numSpaces; i++) {
			sb.append(' ');
		}
		return sb;
	}
	
	public static StringBuffer padToNewLine(StringBuffer sb, int maxLineLenght) {
		int spill = sb.length() % maxLineLenght;
		if (spill == 0) return sb;
		int padLength = maxLineLenght - (spill);
		return sb.append(pad(padLength));
	}
	
	public static boolean isPrePrintedStationaryFormat(Context context) {
		boolean isPrePrintedFormat = 
			EcollectorDB.fetchPrintFormat(context).equals(PrintFormat.PRE_PRINTED_PAPER);
		return isPrePrintedFormat;
	}
}
