package ecollector.device;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.FilterQueryProvider;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import ecollector.common.ServiceColumns;
import ecollector.device.android.framework.SimpleCursorAdapter;
import ecollector.device.db.EcollectorDB;
import ecollector.device.view.ViewManager;
import ecollector.device.view.input.AlphaKeyPad;
import ecollector.device.view.input.KeyPad;
import ecollector.device.view.input.KeyPadListener;
import ecollector.device.view.input.NumericKeyPad;
import ecollector.device.view.input.NumericTabbedKeyPad;

public class ServiceSearchFilters extends Fragment implements KeyPadListener {
	
	class DistributionCursorAdapter extends SimpleCursorAdapter {
		public DistributionCursorAdapter(Context context, int layout, Cursor c,
				String[] from, int[] to) {
			super(context, layout, c, from, to);
		}
		
		public CharSequence convertToString(Cursor cursor) {
			int colCount = cursor.getColumnCount();
			if (colCount >= 2) {
				return cursor.getString(1)+" - "+cursor.getString(2);
			}
			return "N/A";
		}
	}
	
	private class SectionDistributionFilterQueryProvider implements FilterQueryProvider {
		private Activity activity;
		public SectionDistributionFilterQueryProvider(Activity activity) {
			this.activity = activity;
		}
		public Cursor runQuery(CharSequence constraint) {
			return EcollectorDB.createAllDistributionsCursor(
					this.activity.getContentResolver(), 
					constraint);
		}
	}
	
	private static final String[] mBillingStatusValues = new String[]{
			"NONE",
			"COLLECTED",
			"UNCOLLECTED"
	};
	
	private Cursor mAllDistributionsCursor;
	private DistributionCursorAdapter mDistributionCursorAdapter;
	private ListView mSectionDistLv;
	private SectionDistributionFilterQueryProvider mFilterQueryProvider;
	
	//private Cursor mDistributionPollAddressCursor;
	//private DistributionCursorAdapter mDistributionPollAddressCursorAdapter;
	//private Spinner mAddressSpinner;
	
	private ViewManager mViewManager;
	private TextView mFilterTextView;
	private StringBuffer mFilter = new StringBuffer();
	
	private static final CharSequence FILTER_DEFAULT_STRING = "Select Section, Distribution";

	public ServiceSearchFilters(ViewManager viewManager) {
		mViewManager = viewManager;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.search_filters_view, null);
		mFilterTextView = (TextView)view.findViewById(R.id.messageText);
		
		if (mAllDistributionsCursor == null) {
			mAllDistributionsCursor = EcollectorDB.createAllDistributionsCursor(this.getActivity().getContentResolver(),null);
			mDistributionCursorAdapter = new DistributionCursorAdapter(
					this.getActivity(), // context
					//R.layout.simple_list_item_1_black_bg, // layout
					R.layout.service_search_filter_item, // layout
					mAllDistributionsCursor, // cursor
					new String[]{"_ID",ServiceColumns.DISTRIBUTION.toUpperCase()}, // Columns for binding
					new int[] {R.id.textView1, R.id.textView2} // Corresponding column binding View 
					);
			mDistributionCursorAdapter.setViewBinder(new SimpleCursorAdapter.ViewBinder(){
				public boolean setViewValue(View view, Cursor cursor,
						int columnvIndex) {
					// Get the parent view, so that we can manipulate the other
					// view items in it.
					view = (view.getParent() instanceof View) ? (View)view.getParent():view;
					
					// See if we should highlight this
					// view item as selected.
					
					TextView sectionTv = (TextView)view.findViewById(R.id.textView1);
					TextView distributionTv = (TextView)view.findViewById(R.id.textView2);
					String id = cursor.getString(0);
					String section = cursor.getString(1);
					String distribution = cursor.getString(2);
					
					if (ServiceSearchFilterBean.i.isFiltering(section,distribution)) {
						view.setBackgroundDrawable(Util.getItemHighlighter(ServiceSearchFilters.this.getActivity()));
					} else {
						view.setBackgroundDrawable(null);
					}
					
					if (id == null) {
						return true;
					}
					if (id != null && id.equals("-1")) {
						sectionTv.setText("");
						distributionTv.setText("NONE");
					} else {
						sectionTv.setText(section);
						distributionTv.setText(distribution);
					}
					return true;
				}
			});
			mFilterQueryProvider = new SectionDistributionFilterQueryProvider(this.getActivity());
			mDistributionCursorAdapter.setFilterQueryProvider(mFilterQueryProvider);
		}
		
		//mDistributionsSp = (Spinner)view.findViewById(R.id.distributionSelectBox);
		//secDistLv
		mSectionDistLv = (ListView)view.findViewById(R.id.secDistLv);
		mSectionDistLv.setAdapter(mDistributionCursorAdapter);
		mSectionDistLv.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Cursor cursor = (Cursor)parent.getItemAtPosition(position);
				if (id == -1) {
					ServiceSearchFilterBean.i.setDistribution("-1");
					ServiceSearchFilterBean.i.setSection("-1");
				} else {
					String sectionCode = cursor.getString(1);
					String distributionCode = cursor.getString(2);
					Log.i("MeeterReadingSearchFilters", "Distribution code selected : "+distributionCode);
					ServiceSearchFilterBean.i.setDistribution(distributionCode);
					ServiceSearchFilterBean.i.setSection(sectionCode);
				}
				showNext();
			}
		});
		
		showSelectedFilter();
		showMessage(mFilter);
		
		//NavigationKeyPad nkp = new NavigationKeyPad("Clear","Ok");
		AlphaKeyPad kp = new AlphaKeyPad();
		//kp.setNumericKeyPadVisibility(true);
		kp.setLeftTabLabel("Clear");
		kp.setRightTabLabel("Ok");
		mViewManager.showKeyPad(kp);
		
		return view;
	}
	
	private void showSelectedFilter() {
		for (int p = 0; p < mAllDistributionsCursor.getCount(); p++) {
			if (mAllDistributionsCursor.moveToPosition(p)) {
				String secCode = mAllDistributionsCursor.getString(1);
				String distCode = mAllDistributionsCursor.getString(2);
				if (distCode != null && distCode.equalsIgnoreCase(ServiceSearchFilterBean.i.getDistribution()) &&
					secCode != null && secCode.equalsIgnoreCase(ServiceSearchFilterBean.i.getSection())) {
					//mSectionDistLv.setSelection(p);
					mSectionDistLv.smoothScrollToPosition(p);
					return;
				}
			}
		}
		mSectionDistLv.smoothScrollToPosition(0);
	}
	
	public void showMessage(CharSequence filter) {
		if (filter == null || filter.length() == 0) {
			mFilterTextView.setText(FILTER_DEFAULT_STRING);
		} else {
			mFilterTextView.setText(filter);
		}
	}
	
	public void setFilter(CharSequence filter) {
		mDistributionCursorAdapter.getFilter().filter(filter);
		mSectionDistLv.smoothScrollToPosition(0);
	}
	
	private void showNext() {
		ServiceSearchView ssv = new ServiceSearchView(/*mViewManager*/);
		mViewManager.showView(ssv);
	}

	public void onKeyClick(char key) {
		switch (key) {
		case KeyPad.KEY_NEXT:
		case KeyPad.KEY_TAB_RIGHT:
			showNext();
			break;
		case KeyPad.KEY_PREVIOUS:
		case KeyPad.KEY_TAB_LEFT:
			mSectionDistLv.setSelection(0);
			ServiceSearchFilterBean.i.setDistribution("-1");
			ServiceSearchFilterBean.i.setSection("-1");
			mViewManager.showPrevious();
			break;
		case KeyPad.KEY_DEL:
			mFilter.setLength(0);
			break;
		case KeyPad.KEY_BACKSPACE:
			if (mFilter.length() > 1)
				mFilter.setLength(mFilter.length()-1);
			else {
				mFilter.setLength(0);
			}
			break;
		case KeyPad.KEY_NUMERIC_KEY_PAD:
			NumericTabbedKeyPad nkp = new NumericTabbedKeyPad();
			mViewManager.showKeyPad(nkp);
			break;
		case KeyPad.KEY_ALPHABET_KEY_PAD:
			AlphaKeyPad akp = new AlphaKeyPad();
			mViewManager.showKeyPad(akp);
			break;
		default:
			mFilter.append(key);
		}
		showMessage(mFilter);
		setFilter(mFilter);
	}
}