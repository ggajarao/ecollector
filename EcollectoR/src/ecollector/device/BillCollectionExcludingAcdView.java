package ecollector.device;

import java.util.Date;

import ecollector.device.view.ViewManager;

public class BillCollectionExcludingAcdView extends BillCollectionBasePRFormView {
	
	public BillCollectionExcludingAcdView() {
		
	}
	
	public BillCollectionExcludingAcdView(/*ViewManager viewManager, */BillCollectionBean bcBean) {
		super(/*viewManager,*/bcBean);
	}
	
	protected CharSequence getTitle() {
		return "Enter Old PR Details";
	}

	protected boolean isFormValid() {
		int prNumber = Util.asInteger(mOldPrNumber.toString());
		int prAmount = Util.asInteger(mOldPrAmount.toString());
		int iAcd = (int)Util.asDouble(mBcBean.acd);
		Date prDate = Util.getDateFromDp(mOldPrDateDp);
		Date currentDate = new Date();
		return 	prNumber > 0 && 
				(prAmount > 0 && prAmount == iAcd) && 
				prDate.before(currentDate);
	}
	
	protected void showNext() {
		mBcBean.excludeAcd(mOldPrAmount.toString());
		mViewManager.showView(new BillCollectionView(mViewManager,mBcBean));
	}
	
	protected void showPrevious() {
		mViewManager.showPrevious();
	}
}