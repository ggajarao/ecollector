package ecollector.device;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import ecollector.device.view.FragmentViewManager;

public class ReportsActivity extends FragmentViewManager {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/*SyncProxy.releaseResources();
		CloudServices.init(this);*/
	}
	
	@Override
	protected Fragment getHomeView() {
		return new ReportsView(/*this*/);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		/*SyncProxy.releaseResources();*/
	}
	
	@Override
	public void onBackPressed() {
		
	}
}
