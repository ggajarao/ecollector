package ecollector.device.receiver;

import ecollector.device.Util;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BroadcastDispatcher extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		if (Intent.ACTION_BOOT_COMPLETED.equalsIgnoreCase(action)) {
			Util.scheduleDeltaTickConsumer(context);
			Util.schedulePrPushService(context);
			Util.scheduleCommandExecutorService(context);
			Util.schedulePrArchiveService(context);
		}
	}
}
