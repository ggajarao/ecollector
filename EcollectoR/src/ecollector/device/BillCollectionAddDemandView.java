package ecollector.device;

import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import ecollector.device.view.FragmentView;
import ecollector.device.view.ViewManager;
import ecollector.device.view.input.KeyPad;
import ecollector.device.view.input.KeyPadListener;
import ecollector.device.view.input.NumericNavKeyPad;

public class BillCollectionAddDemandView extends FragmentView implements KeyPadListener {
	private ViewManager mViewManager;
	private TextView mMessageText;
	private TextView mNewDemandTv;
	private StringBuffer mNewDemand = new StringBuffer();
	//private int mExcessAmountToConfirm;
	
	private static final int F_NEW_CMD = 0;
	
	private StringBuffer mCurrentFieldValue;
	private int mCurrentFieldLenght;
	private TextView mCurrentTextView;
	private int mCurrentFocusField;
	
	private NumericNavKeyPad mNumericNavKp;
	
	private BillCollectionBean mBcBean;
	
	public BillCollectionAddDemandView() {
		
	}
	
	public BillCollectionAddDemandView(BillCollectionBean bcBean) {
		//mViewManager = viewManager;
		mBcBean = bcBean;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mViewManager = getFragmentViewManager();
		return prepareView(inflater);
	}

	private View prepareView(LayoutInflater inflater) {
		View view = inflater.inflate(R.layout.bill_collection_add_demand_view, null);
		
		mMessageText = (TextView)view.findViewById(R.id.messageText);
		mMessageText.setText("Enter New Demand");
		mNumericNavKp = new NumericNavKeyPad("Cancel","Add");
		mViewManager.showKeyPad(mNumericNavKp);
		
		TextView tv = null;
		tv = (TextView)view.findViewById(R.id.arrearsTv);
		tv.setText(mBcBean.getArrears()+"");
		highlightReadonly(tv);
		
		tv = (TextView)view.findViewById(R.id.cmdTv);
		tv.setText(mBcBean.cmd+"");
		highlightReadonly(tv);
		
		mNewDemandTv = (TextView)view.findViewById(R.id.newCmdTv);
		updateDependentView();
		
		highlightReadonly(mNewDemandTv);
		registerEventHandlers(mNewDemandTv,F_NEW_CMD);
		
		
		// default focus
		final Handler h = new Handler();
		(new Thread(
			new Runnable() {
				public void run() {
					h.postDelayed(new Runnable(){
						@Override
						public void run() {
							setFocus(F_NEW_CMD);
						}
					}, 10);
				}
			}
		)).start();
		
		updateNextButtonState();
		return view;
	}
	
	private void updateNextButtonState() {
		if (isNewCmdValid()) {
			mNumericNavKp.setNextEnabled(true);
		} else {
			mNumericNavKp.setNextEnabled(false);
		}
	}
	
	private boolean isNewCmdValid() {
		int v = Util.asInteger(mNewDemand.toString());
		return v > 0;
	}
	
	
	
	private void updateDependentView() {
		int newCmd = Util.asInteger(mNewDemand.toString());
		mNewDemand.setLength(0);
		mNewDemand.append(newCmd);
		mNewDemandTv.setText(mNewDemand.toString());
		setFocus(mCurrentFocusField);
		updateNextButtonState();
		
		// warns if wrong amount entered, by 
		// changing background
		setFocus(F_NEW_CMD);
		updateNextButtonState();
	}
	
	private void registerEventHandlers(TextView tv, final int focusField) {
		tv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setFocus(focusField);
			}
		});
	}

	private void setFocus(int focusField) {
		unHighlightAll();
		mCurrentFocusField = focusField;
		switch (focusField) {
		case F_NEW_CMD:
			focusBackground(mNewDemandTv);
			mCurrentFieldValue = mNewDemand;
			mCurrentFieldLenght = 7;
			mCurrentTextView = mNewDemandTv;
			break;
		default:
			mCurrentFieldValue = null;
			mCurrentFieldLenght = 0;
			mCurrentTextView = null;
		}
	}
	
	private void focusBackground(View tv) {
		//tv.setBackgroundDrawable(Util.getInputFieldFocusStyle(mViewManager));
		AnimationDrawable background = 
			Util.getInputFieldFocusStyleAnimated(mViewManager);
		if (tv.equals(mNewDemandTv)) {
			if (!isNewCmdValid()) {
				background = Util.getInputFieldFocusErrorStyleAnimated(
						mViewManager);
			}
		}
		tv.setBackgroundDrawable(background);
		((AnimationDrawable)tv.getBackground()).start();
	}
	
	private void highlightReadonly(View tv) {
		tv.setBackgroundDrawable(Util.getItemHighlighter(mViewManager.getActivity()));
		tv.setOnClickListener(null);
	}

	private void unHighlightAll() {
		Drawable background = 
			Util.getInputFieldStyle(mViewManager);
		mNewDemandTv.setBackgroundDrawable(background);
	}
	
	private void showNext() {
		mBcBean.addNewCmd(mNewDemand.toString());
		showPrevious();
	}
	
	private void showPrevious() {
		mViewManager.showPrevious();
	}
	
	public void onKeyClick(char key) {
		if (mCurrentFieldValue == null) return;
		switch (key) {
		case KeyPad.KEY_DEL:
			mCurrentFieldValue.setLength(0);
			break;
		case KeyPad.KEY_BACKSPACE:
			if (mCurrentFieldValue.length() > 1)
				mCurrentFieldValue.setLength(mCurrentFieldValue.length()-1);
			else {
				mCurrentFieldValue.setLength(0);
			}
			break;
		case KeyPad.KEY_PREVIOUS:
			showPrevious();
			break;
		case KeyPad.KEY_NEXT:
			showNext();
			break;
		default:
			if (mCurrentFieldValue.length() < mCurrentFieldLenght){
				mCurrentFieldValue.append(key);
			}
		}
		if (mCurrentTextView != null) {
			int v = (int)Util.asDouble(mCurrentFieldValue.toString());
			mCurrentFieldValue.setLength(0);
			mCurrentFieldValue.append(v+"");
			mCurrentTextView.setText(v+"");
		}
		
		updateDependentView();
	}	
}