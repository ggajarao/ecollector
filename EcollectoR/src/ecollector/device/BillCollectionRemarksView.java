package ecollector.device;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import ecollector.device.view.FragmentView;
import ecollector.device.view.ViewManager;
import ecollector.device.view.input.KeyPad;
import ecollector.device.view.input.KeyPadListener;
import ecollector.device.view.input.NavigationKeyPad;
import ecollector.device.view.input.NumericNavKeyPad;

public class BillCollectionRemarksView extends FragmentView implements KeyPadListener {
	
	private ViewManager mViewManager;
	private TextView mMessageText;
	//private TextView mRcAmountText;
	private TextView mCmdAmountText;
	//private String mOldRemarks;
	private String mOldCmdCollected;
	private String mOldRcAmount;
	
	private static final int F_RC_AMOUNT = 0;
	private static final int F_CMD_AMOUNT = 1;
	
	private StringBuffer mCurrentFieldValue;
	private int mCurrentFieldLenght;
	private TextView mCurrentTextView;
	
	/*private Cursor mExemptorCursor;
	private Cursor mRcAmountCursor;
	private Spinner mExemptorSp;
	private Spinner mRcAmountSp;*/ 
	
	private BillCollectionBean mBcBean;
	public BillCollectionRemarksView() {
		
	}
	public BillCollectionRemarksView(/*ViewManager viewManager, */BillCollectionBean bcBean) {
		//mViewManager = viewManager;
		mBcBean = bcBean;
		//mOldRemarks = mBcBean.remarks;
		mOldCmdCollected = mBcBean.mVolatile_cmd_collected.toString();
		mOldRcAmount = mBcBean.mVolatile_rc_collected.toString();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mViewManager = getFragmentViewManager();
		View view = inflater.inflate(R.layout.remarks_view, null);
		mMessageText = (TextView)view.findViewById(R.id.messageText);
		
		/*Cursor remarksCursor = EcollectorDB.createRemarksCursor();
		Spinner remarksSp = (Spinner)view.findViewById(R.id.remarksSp);
		RemarksCursorAdapter adapter = new RemarksCursorAdapter(
				this.getActivity(), // context
				R.layout.spinner_item, // layout
				remarksCursor, // cursor
				new String[]{"_ID","REMARK","DESCRIPTION"}, // Columns for binding
				new int[] {android.R.id.text1} // Corresponding column binding View 
				);
		adapter.setViewBinder(new SimpleCursorAdapter.ViewBinder(){
			public boolean setViewValue(View view, Cursor cursor,
					int columnIndex) {
				TextView tv = (TextView)view.findViewById(android.R.id.text1);
				String id = cursor.getString(0);
				String remarksCode = cursor.getString(1);
				String description = cursor.getString(2);
				if (id == null) {
					return false;
				}
				if (id.equalsIgnoreCase("-1")) {
					tv.setText(description);
				} else {
					tv.setText(remarksCode+" "+description);
				}
				
				return true;
			}
		});
		
		remarksSp.setAdapter(adapter);
		remarksSp.setOnItemSelectedListener(new OnItemSelectedListener(){
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				Cursor cursor = (Cursor)parent.getItemAtPosition(position);
				onRemarkSelected(cursor.getString(1));// Index of remarks code, refer EcollectorDB.createRemarksCursor();
			}
			
			public void onNothingSelected(AdapterView<?> parent) {
				
			}
		});
		
		// Pre-populate remarks selection
		if (mBcBean.remarks != null) {
			remarksCursor.moveToFirst();
			int p = 0;
			do {
				String remarkCode = remarksCursor.getString(1);
				if (remarkCode != null && remarkCode.equalsIgnoreCase(
						mBcBean.remarks)) {
					remarksSp.setSelection(p);
					break;
				}
				p++;
			} while (remarksCursor.moveToNext());
		}
		
		mExemptorCursor = EcollectorDB.createExemptorCursor();
		mExemptorSp = (Spinner)view.findViewById(R.id.exemptorSp);
		RemarksCursorAdapter exemptorAdapter = new RemarksCursorAdapter(
				this.getActivity(), // context
				R.layout.spinner_item, // layout
				mExemptorCursor, // cursor
				new String[]{"_ID","EXEMPTOR","LABEL"}, // Columns for binding
				new int[] {android.R.id.text1} // Corresponding column binding View 
				);
		exemptorAdapter.setViewBinder(new SimpleCursorAdapter.ViewBinder(){
			public boolean setViewValue(View view, Cursor cursor,
					int columnIndex) {
				TextView tv = (TextView)view.findViewById(android.R.id.text1);
				String id = cursor.getString(0);
				String exemptorCode = cursor.getString(1);
				String exemptorLabel = cursor.getString(2);
				if (id == null) {
					return false;
				}
				if (id.equalsIgnoreCase("-1")) {
					tv.setText(exemptorLabel);
				} else {
					tv.setText(exemptorCode+" "+exemptorLabel);
				}
				
				return true;
			}
		});
		
		mExemptorSp.setAdapter(exemptorAdapter);
		mExemptorSp.setOnItemSelectedListener(new OnItemSelectedListener(){
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				Cursor cursor = (Cursor)parent.getItemAtPosition(position);
				onExemptorSelected(cursor.getString(1));// Index of remarks code, refer EcollectorDB.createRemarksCursor();
			}
			
			public void onNothingSelected(AdapterView<?> parent) {
				
			}
		});
		
		populateExempterSpinner();*/
		
		/*RC Amount spinner*/
		/*mRcAmountCursor = EcollectorDB.createRcAmountCursor();
		mRcAmountSp = (Spinner)view.findViewById(R.id.rcAmountSp);
		RemarksCursorAdapter rcAmountAdapter = new RemarksCursorAdapter(
				this.getActivity(), // context
				R.layout.spinner_item, // layout
				mRcAmountCursor, // cursor
				new String[]{"_ID","AMOUNT"}, // Columns for binding
				new int[] {android.R.id.text1} // Corresponding column binding View 
				);
		rcAmountAdapter.setViewBinder(new SimpleCursorAdapter.ViewBinder(){
			public boolean setViewValue(View view, Cursor cursor,
					int columnIndex) {
				TextView tv = (TextView)view.findViewById(android.R.id.text1);
				String id = cursor.getString(0);
				if (id == null) {
					return false;
				}
				tv.setText(id);
				return true;
			}
		});
		
		mRcAmountSp.setAdapter(rcAmountAdapter);
		mRcAmountSp.setOnItemSelectedListener(new OnItemSelectedListener(){
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				Cursor cursor = (Cursor)parent.getItemAtPosition(position);
				mBcBean.mVolatile_rc_collected.setLength(0);
				mBcBean.mVolatile_rc_collected.append(cursor.getString(0));
			}
			
			public void onNothingSelected(AdapterView<?> parent) {
				
			}
		});
		
		populateRcAmountSpinner();*/
		
		//mRcAmountText = (TextView)view.findViewById(R.id.rcAmountTv);
		mCmdAmountText = (TextView)view.findViewById(R.id.cmdAmtTv);
		
		updateDependentViews();
		
		if (mBcBean.isForOnlyRc()) {
			mViewManager.showKeyPad(new NavigationKeyPad("Cancel","Ok"));
		} else {
			mViewManager.showKeyPad(new NumericNavKeyPad("Cancel","Ok"));
		}
		return view;
	}
	
	/*private void populateRcAmountSpinner() {
		// Pre-populate remarks selection
		if (mBcBean.mVolatile_rc_collected.length() != 0) {
			mRcAmountCursor.moveToFirst();
			int p = 0;
			do {
				String rcAmount = mRcAmountCursor.getString(0);
				if (rcAmount != null && rcAmount.equalsIgnoreCase(
						mBcBean.mVolatile_rc_collected.toString())) {
					mRcAmountSp.setSelection(p);
					break;
				}
				p++;
			} while (mRcAmountCursor.moveToNext());
		}
	}*/
	
	/*private void populateExempterSpinner() {
		// Pre-populate remarks selection
		if (mBcBean.exemptor != null) {
			mExemptorCursor.moveToFirst();
			int p = 0;
			do {
				String exemptorCode = mExemptorCursor.getString(1);
				if (exemptorCode != null && exemptorCode.equalsIgnoreCase(
						mBcBean.exemptor)) {
					mExemptorSp.setSelection(p);
					break;
				}
				p++;
			} while (mExemptorCursor.moveToNext());
		}
	}*/
	
	private void showPrevious() {
		mViewManager.showPrevious();
	}
	
	/*private void onRemarkSelected(String remarkCode) {
		if (mBcBean.isForOnlyRc()) {
			highlightReadonly(mCmdAmountText);
			
			if (remarkCode != null && !remarkCode.equalsIgnoreCase("NONE")) {
				makeAeAsDefaultExemptor();	
			}
			
			mRcAmountSp.setEnabled(false);
			resetTextFields();
			return;
		}
		
		if (remarkCode != null && !remarkCode.equalsIgnoreCase("NONE")) {
			mBcBean.remarks = remarkCode;
			registerEventHandlers(mCmdAmountText, F_CMD_AMOUNT);
			//registerEventHandlers(mRcAmountText, F_RC_AMOUNT);
			mRcAmountSp.setEnabled(true);
			
			makeAeAsDefaultExemptor();
			
			// Remarks selection should make RC amount zero
			mBcBean.mVolatile_rc_collected.setLength(0);
			mBcBean.mVolatile_rc_collected.append("0");
			populateRcAmountSpinner();
			
			unHighlightAll();
		} else {
			mBcBean.remarks = "";
			//highlightReadonly(mRcAmountText);
			highlightReadonly(mCmdAmountText);
			mBcBean.exemptor = "";
			mExemptorSp.setSelection(0);
			resetTextFields();
			mRcAmountSp.setEnabled(false);
		}
		updateDependentViews();
	}*/
	
	/*private void makeAeAsDefaultExemptor() {
		// Make AE as default exemptor
		if (TextUtils.isEmpty(mBcBean.exemptor)) {
			mBcBean.exemptor = "2"; //AE EXEMPTER code
			populateExempterSpinner();
		}
	}*/

	private void resetTextFields() {
		mBcBean.resetCmdCollected();
		mBcBean.resetRcCollected();
		//populateRcAmountSpinner();
	}
	
	private void updateDependentViews() {
		//mRcAmountText.setText(mBcBean.mVolatile_rc_collected);
		mCmdAmountText.setText(mBcBean.mVolatile_cmd_collected);
	}
	
	/*private void onExemptorSelected(String exemptorCode) {
		if (exemptorCode != null && !exemptorCode.equalsIgnoreCase("NONE")) {
			mBcBean.exemptor = exemptorCode;
		} else {
			mBcBean.exemptor = "";
		}
	}*/
	
	private void registerEventHandlers(TextView tv, final int focusField) {
		tv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setFocus(focusField);
			}
		});
		/*tv.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				setFocus(focusField);
				return true;
			}
		});*/
		
	}
	
	private void highlightReadonly(View tv) {
		tv.setBackgroundDrawable(Util.getItemHighlighter(mViewManager.getActivity()));
		tv.setClickable(false);
	}
	
	private void setFocus(int focusField) {
		unHighlightAll();
		switch (focusField) {
		case F_CMD_AMOUNT:
			focusBackground(mCmdAmountText);
			mCurrentFieldValue = mBcBean.mVolatile_cmd_collected;
			mCurrentFieldLenght = 7;
			mCurrentTextView = mCmdAmountText;
			break;
		case F_RC_AMOUNT:
			/*focusBackground(mRcAmountText);
			mCurrentFieldValue = mBcBean.mVolatile_rc_collected;
			mCurrentFieldLenght = 7;
			mCurrentTextView = mRcAmountText;*/
			break;
		default:
			mCurrentFieldValue = null;
			mCurrentFieldLenght = 0;
			mCurrentTextView = null;
		}
	}
	
	private void focusBackground(View tv) {
		//tv.setBackgroundDrawable(Util.getInputFieldFocusStyle(mViewManager));
		tv.setBackgroundDrawable(Util.getInputFieldFocusStyleAnimated(mViewManager));
		((AnimationDrawable)tv.getBackground()).start();
	}
	
	private void unHighlightAll() {
		//mRcAmountText.setBackgroundDrawable(Util.getInputFieldStyle(mViewManager));
		mCmdAmountText.setBackgroundDrawable(Util.getInputFieldStyle(mViewManager));
	}
	
	public void onKeyClick(char key) {
		switch (key) {
		case KeyPad.KEY_PREVIOUS:
			cancel();
			return;
		case KeyPad.KEY_NEXT:
			next();
			return;
		default:
			;
		}
		
		if (mCurrentFieldValue == null) return;
		switch (key) {
		case KeyPad.KEY_DEL:
			mCurrentFieldValue.setLength(0);
			break;
		case KeyPad.KEY_BACKSPACE:
			if (mCurrentFieldValue.length() > 1)
				mCurrentFieldValue.setLength(mCurrentFieldValue.length()-1);
			else {
				mCurrentFieldValue.setLength(0);
			}
			break;
		default:
			if (mCurrentFieldValue.length() < mCurrentFieldLenght){
				mCurrentFieldValue.append(key);
			}
		}
		if (mCurrentTextView != null) {
			int v = (int)Util.asDouble(mCurrentFieldValue.toString());
			mCurrentFieldValue.setLength(0);
			mCurrentFieldValue.append(v+"");
			mCurrentTextView.setText(v+"");
		}
	}
	
	private void cancel() {
		//mBcBean.remarks = mOldRemarks;
		mBcBean.mVolatile_cmd_collected.setLength(0);
		mBcBean.mVolatile_cmd_collected.append(mOldCmdCollected);
		mBcBean.mVolatile_rc_collected.setLength(0);
		mBcBean.mVolatile_rc_collected.append(mOldRcAmount);
		showPrevious();
	}
	
	private void next() {
		if (mBcBean.isForOnlyRc()) {
			BillDetailsView v = new BillDetailsView(/*mViewManager,*/ mBcBean);
			mViewManager.showView(v);
		} else {
			// go back to collection view
			mBcBean.prepareForCollection();
			showPrevious();
		}
	}
}
