package ecollector.device.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ebilly.admin.server.core.reports.ResultAggregator;
import ebilly.admin.shared.viewdef.PivotCriteria;
import ebilly.admin.shared.viewdef.ResultRecord;
import ebilly.admin.shared.viewdef.SearchRequest;
import ebilly.admin.shared.viewdef.SearchResult;
import ecollector.common.FormatUtils;
import ecollector.common.ReportAbstractColumns;
import ecollector.device.ReportFilterBean;
import ecollector.device.Util;
import android.content.ContentResolver;
import android.database.Cursor;
import android.database.MatrixCursor;

public abstract class AbstractReport {
	
	protected ReportFilterBean mReportFilter;
	protected ContentResolver mContentResolver;
	protected MatrixCursor mReportCursor;

	public AbstractReport(ReportFilterBean reportFilter,
			ContentResolver contentResolver) {
		mReportFilter = reportFilter;
		mContentResolver = contentResolver;
	}

	private void initReport() {
		Cursor cursor = EcollectorDB.createPrSearchCursor(
				mReportFilter.getSection(), mReportFilter.getDistribution(), 
				mContentResolver);
		prepareReport(cursor);
	}
	
	protected MatrixCursor createReportCursor() {
		return new MatrixCursor(
				new String[]{
						ReportAbstractColumns.ARREARS_N_DEMAND,
						ReportAbstractColumns.ARREARS_N_DEMAND_COUNT,
						ReportAbstractColumns.RC_COLLECTED,
						ReportAbstractColumns.RC_COLLECTED_COUNT,
						ReportAbstractColumns.ACD_COLLECTED,
						ReportAbstractColumns.ACD_COLLECTED_COUNT,
						ReportAbstractColumns.AGL_AMOUNT,
						ReportAbstractColumns.AGL_AMOUNT_COUNT,
						ReportAbstractColumns.GRAND_TOTAL,
						ReportAbstractColumns.TOTAL_PRS,
						ReportAbstractColumns.SECTION,
						ReportAbstractColumns.DISTRIBUTION
						});
	}
	
	private void prepareReport(Cursor cursor) {
		Date fromDate = mReportFilter.getFromDate();
		Date toDate = mReportFilter.getToDate();
		mReportCursor = this.createReportCursor();
		if (!cursor.moveToFirst()) {
			return;
		}
		
		/*Begin preparing result records prior aggregating*/
		SearchRequest request = ReportUtil.createSearchRequest();
		SearchResult result = ReportUtil.createSearchResult(cursor, request, fromDate, toDate);
		/*End preparing result records prior aggregating*/
		applyAggregation(request, result);
	}
	
	protected abstract void applyAggregation(SearchRequest request, SearchResult result);

	protected static Object[] recordAsStringArray(ResultRecord resultRecord, SearchRequest request) {
		String grandTotal = 
			FormatUtils.amountFormat(
				FormatUtils.decimalPadding(
					""+
					Util.roundDec2((double)Math.round(resultRecord.doubleValue(ReportAbstractColumns.ACD_COLLECTED) +
					 resultRecord.doubleValue(ReportAbstractColumns.AGL_AMOUNT) +
					 resultRecord.doubleValue(ReportAbstractColumns.ARREARS_N_DEMAND) +
					 resultRecord.doubleValue(ReportAbstractColumns.RC_COLLECTED)))));
		
		//request.setValue(ReportAmountAbstractColumns.GRAND_TOTAL, grandTotal, resultRecord);
		
		// CAUTION: ORDER OF VALUES IN STRING ARRAY SHOULD BE IN SYNC WITH CALLER, PL REFER THERE.
		return new String[]{
				FormatUtils.amountFormat(FormatUtils.decimalPadding(
						""+Util.roundDec2(resultRecord
						.doubleValue(ReportAbstractColumns.ARREARS_N_DEMAND)))),
				resultRecord
						.stringValue(ReportAbstractColumns.ARREARS_N_DEMAND_COUNT),
				FormatUtils.amountFormat(FormatUtils.decimalPadding(resultRecord
						.stringValue(ReportAbstractColumns.RC_COLLECTED))),
				resultRecord
						.stringValue(ReportAbstractColumns.RC_COLLECTED_COUNT),
				FormatUtils.amountFormat(FormatUtils.decimalPadding(resultRecord
						.stringValue(ReportAbstractColumns.ACD_COLLECTED))),
				resultRecord
						.stringValue(ReportAbstractColumns.ACD_COLLECTED_COUNT),
				FormatUtils.amountFormat(FormatUtils.decimalPadding(resultRecord
						.stringValue(ReportAbstractColumns.AGL_AMOUNT))),
				resultRecord
						.stringValue(ReportAbstractColumns.AGL_AMOUNT_COUNT),
				grandTotal,
				resultRecord
						.stringValue(ReportAbstractColumns.TOTAL_PRS),
				resultRecord
						.stringValue(ReportAbstractColumns.SECTION),
				resultRecord
					.stringValue(ReportAbstractColumns.DISTRIBUTION)
		};
	}

	public Cursor getReportCursor() {
		if (mReportCursor == null) {
			initReport();
		}
		return mReportCursor;
	}
	
	boolean isUsed = false;
	public boolean hasNext() {
		getReportCursor();
		if (!isUsed) {
			isUsed = true;
			return mReportCursor.moveToFirst(); 
		} else {
			return mReportCursor.moveToNext();
		}
	}
	
	public void resetCursorPosition() {
		isUsed = false;
	}
	
	public MatrixCursor next() {
		return mReportCursor;
	}
}