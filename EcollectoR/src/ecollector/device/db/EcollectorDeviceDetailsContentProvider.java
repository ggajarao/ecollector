package ecollector.device.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.text.TextUtils;
import ecollector.common.DeviceColumns;

public class EcollectorDeviceDetailsContentProvider extends ContentProvider {
	
	public static final String AUTHORITY = "ecollector.provider.devicepairing";
	
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/device");
	
	public static final String CONTENT_TYPE = "vnd.ecollector.devicepairing.protocol/vnd.ecollector.device";

	private static final String TAG = "DevicePairingContentProvider";

	// Database helper
	private EcollectorDB dbHelper;
	
	@Override
	public boolean onCreate() {
		this.dbHelper = EcollectorDB.getInstance(getContext());
		return true;
	}
	
	@Override
	public String getType(Uri uri) {
		return CONTENT_TYPE;
	}

	@Override
	public Uri insert(Uri uri, ContentValues contentvalues) {
		SQLiteDatabase db = null;
		try {
			db = this.dbHelper.getDatabaseToWrite();
			long rowId = db.insert(EcollectorDB.TABLE_DEVICE, "TITLE", contentvalues);
			if (rowId > 0) {
				Uri deviceUri = ContentUris.withAppendedId(CONTENT_URI, rowId);
	            //getContext().getContentResolver().notifyChange(deviceUri, null);
	            return deviceUri;
			}
			return null;
		} finally {
			//this.dbHelper.closeWritableDb();
		}
	}

	@Override
	public Cursor query(Uri uri, String[] projection, 
						String selection, String[] selectionArgs, 
						String sortOrder) {
		SQLiteDatabase db = null;
		try {
			db = this.dbHelper.getDatabaseToWrite();
			Cursor cursor = db.query(EcollectorDB.TABLE_DEVICE, projection, 
						selection, selectionArgs, null, null, sortOrder);
	        // Tell the cursor what uri to watch, so it knows when its source data changes
			cursor.setNotificationUri(getContext().getContentResolver(), uri);
			return cursor;
		} finally {
			//this.dbHelper.closeWritableDb();
		}
	}

	@Override
	public int update(Uri uri, ContentValues contentvalues, String whereClause,
			String[] whereArgs) {
		SQLiteDatabase db = null;
		try {
			db = this.dbHelper.getDatabaseToWrite();
			String deviceName = uri.getLastPathSegment();
			if (deviceName == null || deviceName.trim().equals("")) {
				return -1;
			}	
			whereClause = DeviceColumns.DEVICE_NAME + " = '" + deviceName + "'" + 
					( !TextUtils.isEmpty(whereClause) ? " AND (" + whereClause + ")" : "");
			int updateCount = db.update(EcollectorDB.TABLE_DEVICE, contentvalues, whereClause, whereArgs);
			
			// Notify the observers on this uri about this update
			//getContext().getContentResolver().notifyChange(uri, null);
			return updateCount;
		} finally {
			//this.dbHelper.closeWritableDb();
		}
	}
	
	@Override
	public int delete(Uri uri, String whereClause, String[] whereArgs) {
		SQLiteDatabase db = null;
		try {
			db = this.dbHelper.getDatabaseToWrite();
			String deviceName = uri.getLastPathSegment();
			if (deviceName == null || deviceName.trim().equals("")) {
				return -1;
			}	
			whereClause = DeviceColumns.DEVICE_NAME + " = " + deviceName + 
					( !TextUtils.isEmpty(whereClause) ? " AND (" + whereClause + ")" : "");
			int deleteCount = db.delete(EcollectorDB.TABLE_DEVICE, whereClause, whereArgs);
			//getContext().getContentResolver().notifyChange(uri, null);
			return deleteCount;
		} finally {
			//this.dbHelper.closeWritableDb();
		}
	}
}
