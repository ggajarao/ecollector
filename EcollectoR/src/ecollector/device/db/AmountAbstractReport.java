package ecollector.device.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.ContentResolver;
import android.database.Cursor;
import ebilly.admin.server.core.reports.ResultAggregator;
import ebilly.admin.shared.viewdef.PivotCriteria;
import ebilly.admin.shared.viewdef.ResultRecord;
import ebilly.admin.shared.viewdef.SearchRequest;
import ebilly.admin.shared.viewdef.SearchResult;
import ecollector.common.ReportAbstractColumns;
import ecollector.device.PrintUtil;
import ecollector.device.ReportFilterBean;
import ecollector.device.Util;

public class AmountAbstractReport extends AbstractReport {

	public AmountAbstractReport(ReportFilterBean reportFilter,
			ContentResolver contentResolver) {
		super(reportFilter, contentResolver);
	}
		
	@Override
	protected void applyAggregation(SearchRequest request, SearchResult result) {
		List<PivotCriteria> pivotList = new ArrayList<PivotCriteria>();
		pivotList.add(request.pivotCriteriaFor(ReportAbstractColumns.ARREARS_N_DEMAND, "SUM"));
		pivotList.add(request.pivotCriteriaFor(ReportAbstractColumns.ACD_COLLECTED, "SUM"));
		pivotList.add(request.pivotCriteriaFor(ReportAbstractColumns.AGL_AMOUNT, "SUM"));
		pivotList.add(request.pivotCriteriaFor(ReportAbstractColumns.RC_COLLECTED, "SUM"));
		pivotList.add(request.pivotCriteriaFor(ReportAbstractColumns.TOTAL_PRS, "COUNT"));
		
		ResultAggregator resultAggregator = 
			new ResultAggregator(
				result, 
				new String[]{"-"}, 
				pivotList, request);
		//SearchResult aggSearchResult = resultAggregator.applyAggregation();
		SearchResult aggSearchResult = resultAggregator.getAggregateSearchResultWithTypedData();
		List<ResultRecord> aggResults = aggSearchResult.getResults();
		
		// Aggregation in the above step couts the non zero values, 
		// and following step copies such counted values into respective
		// count fields
		for (ResultRecord r : aggSearchResult.getResults()) {
			String countValue = ""+r.countedValue(ReportAbstractColumns.ARREARS_N_DEMAND);
			r.setValue(ReportAbstractColumns.ARREARS_N_DEMAND_COUNT, 
					countValue);
			countValue = ""+r.countedValue(ReportAbstractColumns.ACD_COLLECTED);
			r.setValue(ReportAbstractColumns.ACD_COLLECTED_COUNT, 
					countValue);
			countValue = ""+r.countedValue(ReportAbstractColumns.AGL_AMOUNT);
			r.setValue(ReportAbstractColumns.AGL_AMOUNT_COUNT, 
					countValue);
			countValue = ""+r.countedValue(ReportAbstractColumns.RC_COLLECTED);
			r.setValue(ReportAbstractColumns.RC_COLLECTED_COUNT, 
					countValue);
		}
		
		for (ResultRecord aggResult : aggResults) {
			aggResult.setValue(ReportAbstractColumns.DISTRIBUTION, null);
			aggResult.setValue(ReportAbstractColumns.SECTION, null);
			mReportCursor.addRow(recordAsStringArray(aggResult,request));
		}
	}
	
	public StringBuffer getPrintBuffer() {
		//WARNING! DO NOT HAVE THIS UNCOMMENT IN PRODUCTION
		// THIS WOULD HANG THE THREAD
		//android.os.Debug.waitForDebugger();
		
		Date fromDate = mReportFilter.getFromDate();
		Date toDate = mReportFilter.getToDate();
		
		resetCursorPosition();
		
		StringBuffer s = new StringBuffer();
		
		while (this.hasNext()) {
			Cursor c = this.next();
			
			s.append(PrintUtil.prepareLineRpad("Amount Abstract Report",PrintUtil.MAX_CHARS_PER_LINE));
			s.append(PrintUtil.prepareLineCalign("APSPDCL",PrintUtil.MAX_CHARS_PER_LINE));
			
			String deviceName = "SCM: "+EcollectorDB.fetchDeviceName(mContentResolver);
			s.append(PrintUtil.prepareLineRpad(deviceName,PrintUtil.MAX_CHARS_PER_LINE));
			
			String dateRange = Util.getReadableDate(fromDate)+" - "+Util.getReadableDate(toDate);
			s.append(PrintUtil.prepareLineRpad(dateRange,PrintUtil.MAX_CHARS_PER_LINE));
			
			/*String sectionName = c.getString(c.getColumnIndex(ReportAbstractColumns.SECTION));
			String dist = c.getString(c.getColumnIndex(ReportAbstractColumns.DISTRIBUTION));
			
			String secTitle = "Sec: " + (sectionName==null?"N/A":sectionName);
			s.append(PrintUtil.prepareLineRpad(secTitle,PrintUtil.MAX_CHARS));
			
			if (dist != null && dist.trim().length() != 0) {
				String distTitle = "Dist: "+dist;
				s.append(PrintUtil.prepareLineRpad(distTitle,PrintUtil.MAX_CHARS));
			}*/
			
			s.append(PrintUtil.DASHED_LINE);
			
			String amt = c.getString(c.getColumnIndex(ReportAbstractColumns.ARREARS_N_DEMAND));
			String count = c.getString(c.getColumnIndex(ReportAbstractColumns.ARREARS_N_DEMAND_COUNT));
			s.append(PrintUtil.prepareLineRpad(
					PrintUtil.prepareLineRpad("CC", 5).append(
					PrintUtil.prepareLineLpad(count, 6).append(
					PrintUtil.prepareLineLpad(amt, 13)
					)).toString()
					,PrintUtil.MAX_CHARS_PER_LINE));
			
			amt = c.getString(c.getColumnIndex(ReportAbstractColumns.RC_COLLECTED));
			count = c.getString(c.getColumnIndex(ReportAbstractColumns.RC_COLLECTED_COUNT));
			s.append(PrintUtil.prepareLineRpad(
					PrintUtil.prepareLineRpad("RC", 5).append(
					PrintUtil.prepareLineLpad(count, 6).append(
					PrintUtil.prepareLineLpad(amt, 13)
					)).toString()
					,PrintUtil.MAX_CHARS_PER_LINE));
			
			amt = c.getString(c.getColumnIndex(ReportAbstractColumns.ACD_COLLECTED));
			count = c.getString(c.getColumnIndex(ReportAbstractColumns.ACD_COLLECTED_COUNT));
			s.append(PrintUtil.prepareLineRpad(
					PrintUtil.prepareLineRpad("ACD", 5).append(
					PrintUtil.prepareLineLpad(count, 6).append(
					PrintUtil.prepareLineLpad(amt, 13)
					)).toString()
					,PrintUtil.MAX_CHARS_PER_LINE));
			
			amt = c.getString(c.getColumnIndex(ReportAbstractColumns.AGL_AMOUNT));
			count = c.getString(c.getColumnIndex(ReportAbstractColumns.AGL_AMOUNT_COUNT));
			s.append(PrintUtil.prepareLineRpad(
					PrintUtil.prepareLineRpad("AGL", 5).append(
					PrintUtil.prepareLineLpad(count, 6).append(
					PrintUtil.prepareLineLpad(amt, 13)
					)).toString()
					,PrintUtil.MAX_CHARS_PER_LINE));
			
			s.append(PrintUtil.DASHED_LINE);
			
			amt = c.getString(c.getColumnIndex(ReportAbstractColumns.GRAND_TOTAL));
			count = c.getString(c.getColumnIndex(ReportAbstractColumns.TOTAL_PRS));
			s.append(PrintUtil.prepareLineRpad(
					PrintUtil.prepareLineRpad("TOTAL", 5).append(
					PrintUtil.prepareLineLpad(count, 6).append(
					PrintUtil.prepareLineLpad(amt, 13)
					)).toString()
					,PrintUtil.MAX_CHARS_PER_LINE));
			
			s.append(PrintUtil.DASHED_LINE);
			s.append(PrintUtil.BLANK_LINE);
		}
		s.append(PrintUtil.BLANK_LINE);
		return s;
	}
}