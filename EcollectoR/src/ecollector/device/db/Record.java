package ecollector.device.db;

import java.util.Arrays;

import android.content.ContentValues;
import android.util.Log;

public class Record {
	private String[] recordValues;
	private String[] fieldNames;
	
	public Record(String[] recordValues, String[] fieldNames) {
		this.recordValues = recordValues;
		this.fieldNames = fieldNames;
	}
	
	public Record(String[] fieldNames) {
		this(null, fieldNames);
	}
	
	public ContentValues asContentValues(String[] recordValues) {
		
		String[] rv = recordValues;
		if (rv == null) rv = this.recordValues;
		if (rv == null) return null;
		if (rv.length != this.fieldNames.length) {
			/*throw new IllegalArgumentException(
			  "Invalid recordValues, lenght mismatch that of fieldValues");*/
			Log.i("Record", "Invalid recordValues, lenght mismatch that of fieldValues, recordValues: "+Arrays.toString(recordValues));
			return null;
		}
		
		ContentValues cv = new ContentValues();
		for (int i = 0; i < fieldNames.length; i++) {
			cv.put(fieldNames[i], rv[i]);
			//Log.i("Record",i+" "+fieldNames[i]+" - ["+rv[i]+"]");
		}
		return cv;
	}
}