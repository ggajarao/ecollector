package ecollector.device.db;


public interface BatchListener<T> {

	void batchComplete(T batchItem);

}
