package ecollector.device.db;

import android.content.ContentResolver;
import android.database.Cursor;
import android.util.Log;
import ebilly.admin.shared.DeviceSummaryReport;
import ecollector.common.SummaryReportColumns;
import ecollector.device.Util;

public class SummaryReport {
	
	private int mTotalServicesCount;
	private int mTotalPrCount;
	//private int mTotalPushedServicesCount;
	private int mTotalReadyToPushServicesCount;
	private String mTotalCollection;
	
	public SummaryReport(Cursor c) {
		initReport(c);
	}
	
	private void initReport(Cursor c) {
		if (!c.moveToFirst()) {
			Log.i("SummaryReport", "SummaryReport is not available");
			return;
		}
		mTotalServicesCount = c.getInt(c.getColumnIndex(SummaryReportColumns.TOTAL_SERVICES_COUNT));
		mTotalPrCount = c.getInt(c.getColumnIndex(SummaryReportColumns.TOTAL_PR_COUNT));
		mTotalReadyToPushServicesCount = c.getInt(c.getColumnIndex(SummaryReportColumns.TOTAL_PR_TO_PUSH_COUNT));
		mTotalCollection = c.getString(c.getColumnIndex(SummaryReportColumns.TOTAL_COLLECTION));
	}

	public int getmTotalServicesCount() {
		return mTotalServicesCount;
	}

	public int getmTotalPushedServicesCount() {
		//return mTotalPushedServicesCount;
		throw new IllegalArgumentException("TotalPushedServicesCount implementation pending.");
	}

	public int getmTotalReadyToPushServicesCount() {
		return mTotalReadyToPushServicesCount;
	}

	public int getmTotalPrCount() {
		return mTotalPrCount;
	}

	public void setmTotalPrCount(int totalPrCount) {
		this.mTotalPrCount = totalPrCount;
	}
	
	public String getTotalCollection() {
		return this.mTotalCollection;
	}

	public Double getCollectionMargin(ContentResolver contentResolver) {
		String collectionAmount = this.getTotalCollection();
    	Double collectionLimit = EcollectorDB.fetchCollectionLimit(contentResolver);
    	Double margin = 0d;
    	if (collectionLimit > 0 && collectionAmount != null && collectionAmount.trim().length() != 0) {
    		margin = collectionLimit - Util.asDouble(collectionAmount);
    	}
    	return margin;
	}
	
	public DeviceSummaryReport asDeviceSummaryReport(ContentResolver contentResolver) {
		DeviceSummaryReport dsr = new DeviceSummaryReport();
		dsr.setmTotalCollection(this.getTotalCollection());
		dsr.setmTotalPrCount(this.getmTotalPrCount()+"");
		dsr.setmTotalReadyToPushServicesCount(this.getmTotalReadyToPushServicesCount()+"");
		dsr.setmTotalServicesCount(this.getmTotalServicesCount()+"");
		dsr.setmCollectionMargin(this.getCollectionMargin(contentResolver)+"");
		//dsr.setmTotalPushedServicesCount(this.getmTotalPushedServicesCount()+"");
		return dsr;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}