package ecollector.device.db;

import ecollector.common.ServiceColumns;
import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

public class EcollectorContentProvider extends ContentProvider {
	
	public static final String AUTHORITY = "ecollector.provider.devisedata";
	
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/services");
	
	public static final String CONTENT_TYPE = "vnd.ecollector.cursor.services/vnd.ecollector.service";

	private static final String TAG = "EbillyContentProvider";

	// Database helper
	private EcollectorDB dbHelper;
	
	@Override
	public boolean onCreate() {
		this.dbHelper = EcollectorDB.getInstance(getContext());
		return true;
	}
	
	@Override
	public String getType(Uri uri) {
		return CONTENT_TYPE;
	}

	@Override
	public Uri insert(Uri uri, ContentValues contentvalues) {
		if (contentvalues == null) return null;
		SQLiteDatabase db = this.dbHelper.getDatabaseToWrite();
		long rowId = db.insert(EcollectorDB.TABLE_SERVICE, null, contentvalues);
		if (rowId > 0) {
			Uri serviceUri = ContentUris.withAppendedId(CONTENT_URI, rowId);
            getContext().getContentResolver().notifyChange(serviceUri, null);
            return serviceUri;
		}
		return null;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, 
						String selection, String[] selectionArgs, 
						String sortOrder) {
		SQLiteDatabase db = this.dbHelper.getReadableDatabase();
		Cursor cursor = db.query(EcollectorDB.TABLE_SERVICE, projection, 
					selection, selectionArgs, null, null, sortOrder);
        // Tell the cursor what uri to watch, so it knows when its source data changes
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues contentvalues, String whereClause,
			String[] whereArgs) {
		SQLiteDatabase db = this.dbHelper.getDatabaseToWrite();
		String serviceId = uri.getLastPathSegment();
		long iserviceId = -1;
		try {
			iserviceId = Long.parseLong(serviceId);
		} catch (NumberFormatException e) {
			Log.i(TAG,"Illegal service id: "+serviceId, e);
			return -1;
		}
		whereClause = ServiceColumns._ID + " = " + iserviceId + 
				( !TextUtils.isEmpty(whereClause) ? " AND (" + whereClause + ")" : "");
		int updateCount = db.update(EcollectorDB.TABLE_SERVICE, contentvalues, whereClause, whereArgs);
		
		// Notify the observers on this uri about this update
		getContext().getContentResolver().notifyChange(uri, null);
		return updateCount;
	}
	
	@Override
	public int delete(Uri uri, String whereClause, String[] whereArgs) {
		SQLiteDatabase db = this.dbHelper.getDatabaseToWrite();
		String serviceId = uri.getLastPathSegment();
		int iserviceId = -1;
		try {
			iserviceId = Integer.parseInt(serviceId);
		} catch (NumberFormatException e) {
			Log.i(TAG,"Illegal service id: "+serviceId, e);
			return -1;
		}
		whereClause = ServiceColumns._ID + " = " + iserviceId + 
				( !TextUtils.isEmpty(whereClause) ? " AND (" + whereClause + ")" : "");
		int deleteCount = db.delete(EcollectorDB.TABLE_SERVICE, whereClause, whereArgs);
		getContext().getContentResolver().notifyChange(uri, null);
		return deleteCount;
	}
}