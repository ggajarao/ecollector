package ecollector.device.db;

import ecollector.common.PrColumns;
import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

public class EcollectorPrContentProvider extends ContentProvider {
	
	public static final String AUTHORITY = "ecollector.provider.prdata";
	
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/pr");
	
	public static final String CONTENT_TYPE = "vnd.ecollector.cursor.services/vnd.ecollector.pr";

	private static final String TAG = "EcollectorPrContentProvider";

	// Database helper
	private EcollectorDB dbHelper;
	
	@Override
	public boolean onCreate() {
		this.dbHelper = EcollectorDB.getInstance(getContext());
		return true;
	}
	
	@Override
	public String getType(Uri uri) {
		return CONTENT_TYPE;
	}

	@Override
	public Uri insert(Uri uri, ContentValues contentvalues) {
		if (contentvalues == null) return null;
		SQLiteDatabase db = this.dbHelper.getDatabaseToWrite();
		long rowId = db.insert(EcollectorDB.TABLE_PR, null, contentvalues);
		if (rowId > 0) {
			Uri serviceUri = ContentUris.withAppendedId(CONTENT_URI, rowId);
            getContext().getContentResolver().notifyChange(serviceUri, null);
            return serviceUri;
		}
		return null;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, 
						String selection, String[] selectionArgs, 
						String sortOrder) {
		SQLiteDatabase db = this.dbHelper.getReadableDatabase();
		Cursor cursor = db.query(EcollectorDB.TABLE_PR, projection, 
					selection, selectionArgs, null, null, sortOrder);
        // Tell the cursor what uri to watch, so it knows when its source data changes
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues contentvalues, String whereClause,
			String[] whereArgs) {
		SQLiteDatabase db = this.dbHelper.getDatabaseToWrite();
		String prId = uri.getLastPathSegment();
		long iPrId = -1;
		try {
			iPrId = Long.parseLong(prId);
		} catch (NumberFormatException e) {
			Log.i(TAG,"Illegal pr id: "+prId, e);
			return -1;
		}
		whereClause = PrColumns._ID + " = " + iPrId + 
				( !TextUtils.isEmpty(whereClause) ? " AND (" + whereClause + ")" : "");
		int updateCount = db.update(EcollectorDB.TABLE_PR, contentvalues, whereClause, whereArgs);
		
		// Notify the observers on this uri about this update
		getContext().getContentResolver().notifyChange(uri, null);
		return updateCount;
	}
	
	@Override
	public int delete(Uri uri, String whereClause, String[] whereArgs) {
		SQLiteDatabase db = this.dbHelper.getDatabaseToWrite();
		String prId = uri.getLastPathSegment();
		int iPrId = -1;
		try {
			iPrId = Integer.parseInt(prId);
		} catch (NumberFormatException e) {
			Log.i(TAG,"Illegal pr id: "+prId, e);
			return -1;
		}
		whereClause = PrColumns._ID + " = " + iPrId + 
				( !TextUtils.isEmpty(whereClause) ? " AND (" + whereClause + ")" : "");
		int deleteCount = db.delete(EcollectorDB.TABLE_PR, whereClause, whereArgs);
		getContext().getContentResolver().notifyChange(uri, null);
		return deleteCount;
	}
}