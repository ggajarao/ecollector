package ecollector.device.db;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.MergeCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.AppConfig.DEVICE_COMMAND_STATES;
import ebilly.admin.shared.DeviceCommandUi;
import ebilly.admin.shared.DevicePrUi;
import ebilly.admin.shared.DeviceUi;
import ebilly.admin.shared.PushPrRequest;
import ebilly.admin.shared.UserContext;
import ebilly.admin.shared.viewdef.CrudResponse;
import ecollector.common.DeviceColumns;
import ecollector.common.DeviceCommandColumns;
import ecollector.common.DistributionsColumns;
import ecollector.common.FormatUtils;
import ecollector.common.PrColumns;
import ecollector.common.SequenceHolderColumns;
import ecollector.common.ServiceColumns;
import ecollector.common.ServiceDataExporter;
import ecollector.common.SummaryReportColumns;
import ecollector.crypto.CryptoUtil;
import ecollector.device.ExternalStorageState;
import ecollector.device.NetworkAvailabilityState;
import ecollector.device.ServiceSearchFilterBean;
import ecollector.device.Util;
import ecollector.device.autobot.AutoPrGenerator;
import ecollector.device.net.CloudServices;
import ecollector.device.net.PushPrsCommand;
import ecollector.device.view.action.HandlerActions;
import ecollector.device.view.action.Messinger;
import ecollector.device.view.command.CommandAction;
import ecollector.device.view.command.CommandFactory;

public class EcollectorDB extends SQLiteOpenHelper {
	
	public static final String TABLE_SERVICE = "SERVICES";
	public static final String TABLE_DEVICE = "DEVICE";
	public static final String TABLE_DEVICE_COMMAND = "DEVICE_COMMAND";
	public static final String TABLE_SEQ_HOLDER = "SEQ_HOLDER";
	public static final String TABLE_PR = "PR";
	public static final String TABLE_SUMMARY_REPORT = "SUMMARY_REPORT";
	public static final String TABLE_DISTRIBUTIONS = "DISTRIBUTIONS";
	
	private static final int DELETE_ARCHIVE_FILES_OLDER_THAN_DAYS = 45;
	
	private static final String DB_NAME = "EcollectoR";
	private static final int DB_VERSION = 12;
	private Context mContext;
	private static final String TAG = "EbillyDB";
	
	public static enum PrintFormat {
		PRE_PRINTED_PAPER("PRE PRINTED PAPER"), PLAIN_PAPER("PLAIN PAPER");
		
		private String displayName;
		PrintFormat(String displayName) {
			this.displayName = displayName;
		}
		public String toString() {
			return this.displayName;
		}
	}
	
	EcollectorDB(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
		this.mContext = context;
	}

	@Override
	public void onCreate(SQLiteDatabase sqliteDb) {
		Log.i(TAG, "Creating Database, path: "+sqliteDb.getPath());
		String[] statements = getDbStatements();
		executeStatements(statements, sqliteDb);
		Log.i(TAG, "Successfully created Database");
	}

	private void executeStatements(String[] statements, SQLiteDatabase sqliteDb) {
		for (String statement : statements) {
			if (statement != null && !statement.trim().equals("")) {
				try {
					Log.i(TAG, "Executing statement: "+statement);
					sqliteDb.execSQL(statement + ";");
					Log.i(TAG, "Successfully executed statement.");
				} catch(SQLException e) {
					Log.i(TAG, "Error executing statement: "+statement,e);
					throw new RuntimeException(
							"Error executing statement: "+statement,e);
				}
			}
		}
	}

	private String[] getDbStatements() {
		String[] statements = readDbFileForStatements();
		List<String> lStatements = new ArrayList<String>();
		for (String s : statements) {
			s = s.trim();
			if (s != null && s.startsWith("--")) continue;
			lStatements.add(s);
		}
		return lStatements.toArray(new String[]{});
	}
	
	private String[] getDbStatements(String version) {
		String[] statements = readDbFileForStatements();
		List<String> lStatements = new ArrayList<String>();
		String versionString = "--VERSION="+version+";";
		
		boolean versionStarted = false;
		for (String s : statements) {
			s = s.trim();
			String s1 = s+";";
			if (!versionStarted) {
				if (s1 != null && s1.equalsIgnoreCase(versionString)) {
					versionStarted = true;
				}
			} else {
				if (s != null && s.startsWith("--")) continue;
				lStatements.add(s);
			}
		}
		return lStatements.toArray(new String[]{});
	}
	
	private String[] readDbFileForStatements() {
		InputStream is = null;
		InputStreamReader isr = null;
		BufferedReader br = null;
		StringBuffer sqlBuffer = new StringBuffer();
		String line = null;
		try {
			is = mContext.getAssets().open("db.sql");
			if (is == null) {
				Log.i(TAG, "Failed to create DB. Unable to read "+"db.sql");
				return new String[]{};
			}
			isr = new InputStreamReader(is);
			br = new BufferedReader(isr);
			while ((line = br.readLine()) != null) {
				sqlBuffer.append(line).append("\n");
			}
		} catch (IOException e) {
			Log.i(TAG, "Error creating db",e);
			throw new RuntimeException("Error creating db",e);
		} finally {
			try {
				if (is != null) is.close();
				if (isr != null) isr.close();
				if (br != null) br.close();
			} catch (IOException e) {
				Log.i(TAG, "Error while closing stream", e);
			}
		}
		String[] statements = sqlBuffer.toString().split(";");
		return statements;
	}

	@Override
	public void onUpgrade(SQLiteDatabase sqliteDb, int i, int j) {
		Log.i(TAG, "upgrading db, oldVersion: "+i+", newVersion: "+j);
		String[] statements = getDbStatements((i+1)+"");
		executeStatements(statements, sqliteDb);
		Log.i(TAG, "Successfully created Database");
	}
	
	private static EcollectorDB sDbInstance = null;
	public static EcollectorDB getInstance(Context context) {
		EcollectorDB returnInstance = null;
		if (sDbInstance == null) {
			synchronized (EcollectorDB.class) {
				if (sDbInstance == null) {
					sDbInstance = new EcollectorDB(context);
					returnInstance = sDbInstance;
				} else {
					returnInstance = sDbInstance;
				}
			}
		} else {
			returnInstance = sDbInstance;
		}
		return returnInstance;
	}
	
	private SQLiteDatabase sWritableDb;
	public synchronized SQLiteDatabase getDatabaseToWrite() {
		EcollectorDB instance = this;
		if (sWritableDb == null) {
			sWritableDb = instance.getWritableDatabase();
		} else if (!sWritableDb.isOpen()) {
			sWritableDb.close();
			sWritableDb = instance.getWritableDatabase();;
		}
		while (sWritableDb.isDbLockedByCurrentThread() || sWritableDb.isDbLockedByOtherThreads()) {
			;
		}
		return sWritableDb;
	}
	
	public void closeWritableDb() {
		if (sWritableDb != null) {
			sWritableDb.close();
		}
	}
	
	public void finalize() {
		closeWritableDb();
	}
	
	private static Cursor createServiceSearchCursor(ContentResolver contentResolver, CharSequence constraint, ServiceSearchFilterBean i) {
		StringBuffer whereClause = prepareServiceSearchWhereClause(constraint,i);
		Cursor cursor = contentResolver.query(
				EcollectorContentProvider.CONTENT_URI,// URL
				new String[]{ 
						ServiceColumns._ID,
						ServiceColumns.SC_NO,
						ServiceColumns.NAME,
						ServiceColumns._PR_IDS
						}, // Projection
				whereClause.toString(), // selection 
				null, // selectionArgs
				ServiceColumns._N_USC_NO  + " ASC"  // SortOrder eg: COLLATE LOCALIZED ASC
				);
		return cursor;
	}
	
	public static Cursor createServiceSearchCursor(Activity activity, CharSequence constraint, ServiceSearchFilterBean i) {
		StringBuffer whereClause = prepareServiceSearchWhereClause(constraint,i);
		
	    Cursor cursor = activity.managedQuery(
				EcollectorContentProvider.CONTENT_URI,// URL
				new String[]{ 
						ServiceColumns._ID,
						ServiceColumns.SC_NO,
						ServiceColumns.NAME,
						ServiceColumns._PR_IDS
						}, // Projection
				whereClause.toString(), // selection 
				null, // selectionArgs
				ServiceColumns._N_USC_NO  + " ASC LIMIT 100"  // SortOrder eg: COLLATE LOCALIZED ASC
				);
		return cursor;
	}
	
	private static StringBuffer prepareServiceSearchWhereClause(
			CharSequence constraint, ServiceSearchFilterBean i) {
		StringBuffer whereClause = new StringBuffer();
		if (constraint != null && !constraint.equals("")) {
			whereClause
			.append(ServiceColumns._SC_NO)
			.append(" LIKE ").append("'").append(constraint).append("%'");
		} else {
			whereClause
				.append(" 1 = 1 ");
		}
		
		if (!i.getCollectionStatus().equals("") &&
				!i.getCollectionStatus().equalsIgnoreCase("NONE")) {
			if (whereClause.length() > 0) {
				whereClause.append(" AND ");
			}
			if (i.getCollectionStatus().equalsIgnoreCase("COLLECTED")) {
				whereClause.append(ServiceColumns._PR_IDS).append(" IS NOT NULL ");
			} else if (i.getCollectionStatus().equalsIgnoreCase("UNCOLLECTED")) {
				whereClause.append(ServiceColumns._PR_IDS).append(" IS NULL ");
			}
		}
		
		if (!i.getDistribution().equalsIgnoreCase("-1") &&
				!i.getDistribution().equalsIgnoreCase("NONE")) {
			if (whereClause.length() > 0) {
				whereClause.append(" AND ");
			}
			whereClause
				.append(ServiceColumns.DISTRIBUTION)
				.append(" = ").append("'").append(i.getDistribution()).append("'");
		}
		
		if (!i.getSection().equalsIgnoreCase("-1") &&
				!i.getSection().equalsIgnoreCase("NONE")) {
			if (whereClause.length() > 0) {
				whereClause.append(" AND ");
			}
			whereClause
				.append(ServiceColumns.SECTION)
				.append(" = ").append("'").append(i.getSection()).append("'");
		}
		
		if (!i.getPollAddress().equalsIgnoreCase("-1")) {
			if (whereClause.length() > 0) {
				whereClause.append(" AND ");
			}
			whereClause
				.append(ServiceColumns.ADDRESS)
				.append(" = ").append("'").append(i.getPollAddress()).append("'");
		}
		return whereClause;
	}
	
	private static Cursor fetchAllDistributions(ContentResolver contentResolver) {
		Cursor cursor = contentResolver.query(
				EcollectorContentProvider.CONTENT_URI, 
				new String[]{ 
						"DISTINCT "+ServiceColumns.SECTION,
						ServiceColumns.DISTRIBUTION,
						"1 AS _ID"
						//ServiceColumns.DIST_OR_ZONE_NAME.toUpperCase()
						}, // Projection 
			    null, // Selection
				null, //SelectionArgs
				null //Sort order
				);
		return cursor;
		/*MatrixCursor defaultItemCursor = new MatrixCursor(new String[]{"_ID",ServiceColumns.DISTRIBUTION.toUpperCase()},1);
		defaultItemCursor.addRow(new String[]{"-1","NONE"});
		MergeCursor finalCursor = new MergeCursor(new Cursor[]{defaultItemCursor,cursor});
		return finalCursor;*/
	}

	public static Cursor createAllDistributionsCursor(ContentResolver contentResolver, CharSequence distName) {
		StringBuffer whereClause = new StringBuffer();
		if (distName != null && !distName.equals("")) {
			whereClause
			.append(DistributionsColumns.DISTRIBUTION)
			.append(" LIKE ").append("'").append(distName).append("%'");
		} else {
			whereClause
				.append(" 1 = 1 ");
		}
		
		Cursor cursor = contentResolver.query(
				DistributionsContentProvider.CONTENT_URI, 
				new String[]{ 
						DistributionsColumns._ID,
						DistributionsColumns.SECTION,
						DistributionsColumns.DISTRIBUTION
						}, // Projection 
				whereClause.toString(), // Selection
				null, //SelectionArgs
				null //Sort order
				);
		/*MatrixCursor defaultItemCursor = new MatrixCursor(new String[]{"_ID",DistributionsColumns.SECTION.toUpperCase(),DistributionsColumns.DISTRIBUTION.toUpperCase()},1);
		defaultItemCursor.addRow(new String[]{"-1","NONE","NONE"});
		MergeCursor finalCursor = new MergeCursor(new Cursor[]{defaultItemCursor,cursor});
		return finalCursor;*/
		return cursor;
	}
	
	public static Cursor createDistributionPollCursor(Activity activity, CharSequence distributionCode) {
		StringBuffer whereClause = new StringBuffer();
		if (distributionCode != null && !distributionCode.equals("-1")) {
			whereClause
				//.append(ServiceColumns.DIST_OR_AREA_CODE)
				.append(ServiceColumns.DISTRIBUTION)
				.append(" LIKE ").append("'%").append(distributionCode).append("%'");
		} else {
			whereClause.append(" 1 = 1 ");
		}
		Cursor cursor = activity.getContentResolver().query(
				EcollectorContentProvider.CONTENT_URI, 
				new String[]{ 
						"DISTINCT "+ServiceColumns.ADDRESS + " AS " + "_ID",
						}, // Projection 
				whereClause.toString(), // selection 
				null, //SelectionArgs
				null //Sort order
				);
		MatrixCursor defaultItemCursor = new MatrixCursor(new String[]{"_ID"},1);
		defaultItemCursor.addRow(new String[]{"-1"});
		MergeCursor finalCursor = new MergeCursor(new Cursor[]{defaultItemCursor,cursor});
		return finalCursor;
	}
	
	public static SummaryReport fetchSummaryReport(ContentResolver contentResolver) {
		/*ServiceSearchFilterBean i = new ServiceSearchFilterBean();
        Cursor c = createServiceSearchCursor(activity, null, i);
        return new SummaryReport(c);*/
		Cursor c = null;
		try {
			c = EcollectorDB.fetchSummaryReportCursor(contentResolver);
			return new SummaryReport(c);
		} finally {
			if (c != null) c.close();
		}
	}
	
	public static Cursor findServiceById(Context context, long serviceId) {
		StringBuffer whereClause = new StringBuffer();
		whereClause.append(ServiceColumns._ID).append(" = ").append(serviceId);
		
		EcollectorDB instance = EcollectorDB.getInstance(context);
		Cursor cursor = instance.getReadableDatabase().query(
				EcollectorDB.TABLE_SERVICE, // TABLE Name
				new String[]{ 
						ServiceColumns._ID,
						ServiceColumns.USC_NO,
						ServiceColumns.SC_NO,
						ServiceColumns.NAME,
						ServiceColumns.ADDRESS,
						ServiceColumns.CATEGORY,
						ServiceColumns.GROUP_X,
						ServiceColumns.ERO,
						ServiceColumns.SECTION,
						ServiceColumns.DISTRIBUTION,
						ServiceColumns.ARRIERS,
						ServiceColumns.CMD,
						ServiceColumns.ACD,
						ServiceColumns.SD_AVAILABLE,
						ServiceColumns.BILLED_MONTHS,  
						ServiceColumns.DC_DATE,
						ServiceColumns.RC_CODE,
						ServiceColumns.MACHINE_CODE,
						ServiceColumns.PEFCTDT,
						ServiceColumns.AGL_AMOUNT,
						ServiceColumns.AGL_SERVICES,
						ServiceColumns.SUB_CATEGORY,
						ServiceColumns._SC_NO,
						ServiceColumns._N_USC_NO,
						ServiceColumns._PR_IDS
						}, // Projection/Columns
				whereClause.toString(), // selection 
				null, // selectionArgs
				null, // GroupBy
				null, // having
				null,  // SortOrder eg: COLLATE LOCALIZED ASC
				null // limit
				);
		
		/*Cursor cursor = activity.managedQuery(
				EcollectorContentProvider.CONTENT_URI,// URL
				new String[]{ 
						ServiceColumns._ID,
						ServiceColumns.USC_NO,
						ServiceColumns.SC_NO,
						ServiceColumns.NAME,
						ServiceColumns.ADDRESS,
						ServiceColumns.CATEGORY,
						ServiceColumns.GROUP_X,
						ServiceColumns.ERO,
						ServiceColumns.SECTION,
						ServiceColumns.DISTRIBUTION,
						ServiceColumns.ARRIERS,
						ServiceColumns.CMD,
						ServiceColumns.ACD,
						ServiceColumns.SD_AVAILABLE,
						ServiceColumns.BILLED_MONTHS,  
						ServiceColumns.DC_DATE,
						ServiceColumns.RC_CODE,
						ServiceColumns.MACHINE_CODE,
						ServiceColumns.PEFCTDT,
						ServiceColumns.AGL_AMOUNT,
						ServiceColumns.AGL_SERVICES,
						ServiceColumns.SUB_CATEGORY,
						ServiceColumns._SC_NO,
						ServiceColumns._N_USC_NO,
						ServiceColumns._PR_IDS
						}, // Projection
				whereClause.toString(), // selection 
				null, // selectionArgs
				null  // SortOrder eg: COLLATE LOCALIZED ASC
				);*/
		return cursor;
	}
	
	public static Cursor findPrsByIds(Context context, List<String> prIds) {
		if (prIds == null || prIds.size() == 0) return null;
		StringBuffer whereClause = prepareInClause(PrColumns.RECEIPT_NUMBER,
				prIds.toArray(new String[]{}));
		
		//Context context = null;
		EcollectorDB instance = EcollectorDB.getInstance(context);
		Cursor cursor = 
			instance.getReadableDatabase().query(
				EcollectorDB.TABLE_PR, 
				(String[])PrColumns.ALL_COLUMNS.toArray(new String[]{}), //columns 
				whereClause.toString(), //selection 
				null, //selectionArgs 
				null, //groupBy
				null, //having
				null  //orderBy
		); 
		
		/*Cursor cursor = activity.managedQuery(
				EcollectorPrContentProvider.CONTENT_URI,// URL
				(String[])PrColumns.ALL_COLUMNS.toArray(new String[]{}), // Projection
				whereClause.toString(), // selection 
				null, // selectionArgs
				null  // SortOrder eg: COLLATE LOCALIZED ASC
				);*/
		return cursor;
	}
	
	public static Cursor createPrCursor(ContentResolver contentResolver) {
		StringBuffer whereClause = new StringBuffer();
		Cursor cursor = contentResolver.query(
				EcollectorPrContentProvider.CONTENT_URI,// URL
				(String[])PrColumns.ALL_COLUMNS.toArray(new String[]{}), // Projection
				whereClause.toString(), // selection 
				null, // selectionArgs
				null  // SortOrder eg: COLLATE LOCALIZED ASC
				);
		return cursor;
	}
	
	public static Cursor createPrCursor(Context context) {
		StringBuffer whereClause = new StringBuffer();
		EcollectorDB instance = EcollectorDB.getInstance(context); 
		SQLiteDatabase db = instance.getDatabaseToWrite();
		Cursor cursor = 
			db.query(EcollectorDB.TABLE_PR, 
					(String[])PrColumns.ALL_COLUMNS.toArray(new String[]{}), // Projection 
					whereClause.toString(), //selection, 
					null, //selectionArgs, 
					null, //groupBy, 
					null, //having, 
					null //orderBy
			);
		return cursor;
	}
	
	public static Cursor findPuashablePrs(Context context) {
		StringBuffer whereClause = new StringBuffer();
		whereClause.append(PrColumns._PUSH_STATE)
			.append(" = '").append(ServiceColumns.C_PUSH_STATE_READY).append("'");
		
		EcollectorDB instance = EcollectorDB.getInstance(context);
		SQLiteDatabase db = instance.getDatabaseToWrite();
		Cursor cursor = db.query(EcollectorDB.TABLE_PR, 
				(String[])PrColumns.ALL_COLUMNS.toArray(new String[]{}), // Columns 
				whereClause.toString(), // selection 
				null,//selectionArgs 
				null, //groupBy 
				null, //having 
				null //orderBy
				);
		return cursor;
	}
	
	private static Cursor createDeviceSearchCursor(ContentResolver contentResolver, CharSequence constraint) {
		StringBuffer whereClause = new StringBuffer();
		if (constraint != null && !constraint.equals("")) {
			whereClause
			.append(DeviceColumns.DEVICE_NAME)
			.append(" LIKE ").append("'%").append(constraint).append("%'");
		} else {
			whereClause
				.append(" 1 = 1 ");
		}
		Cursor cursor = contentResolver.query(
				EcollectorDeviceDetailsContentProvider.CONTENT_URI,// URL
				new String[]{
						DeviceColumns._ID,
						DeviceColumns.DEVICE_NAME,
						DeviceColumns.DEVICE_IDENTIFIER,
						DeviceColumns.DEVICE_CLD_UN,
						DeviceColumns.DEVICE_CLD_PWD,
						DeviceColumns.DEVICE_PRINTER_NAME,
						DeviceColumns.DEVICE_PRINTER_ADDR,
						DeviceColumns.DEVICE_CLOUD_TIME,
						DeviceColumns.DEVICE_COLLECTION_EXPIRATION_DATE,
						DeviceColumns.DEVICE_COLLECTION_LIMIT,
						DeviceColumns.DEVICE_EXPIRATION_DELTA_TICKS,
						DeviceColumns.DEVICE_LAST_TSL_RESET_TIME,
						DeviceColumns.DEVICE_PRINT_FORMAT
						}, // Projection
				whereClause.toString(), // selection 
				null, // selectionArgs
				null  // SortOrder eg: COLLATE LOCALIZED ASC
				);
		return cursor;
	}
	
	public static Cursor createDeviceSearchCursor(Context context, CharSequence constraint) {
		StringBuffer whereClause = new StringBuffer();
		if (constraint != null && !constraint.equals("")) {
			whereClause
			.append(DeviceColumns.DEVICE_NAME)
			.append(" LIKE ").append("'%").append(constraint).append("%'");
		} else {
			whereClause
				.append(" 1 = 1 ");
		}
		
		EcollectorDB instance = EcollectorDB.getInstance(context);
		SQLiteDatabase db = null;
		try {
			db = instance.getDatabaseToWrite();
			Cursor cursor = db.query(EcollectorDB.TABLE_DEVICE, 
					new String[]{
						DeviceColumns._ID,
						DeviceColumns.DEVICE_NAME,
						DeviceColumns.DEVICE_IDENTIFIER,
						DeviceColumns.DEVICE_CLD_UN,
						DeviceColumns.DEVICE_CLD_PWD,
						DeviceColumns.DEVICE_PRINTER_NAME,
						DeviceColumns.DEVICE_PRINTER_ADDR,
						DeviceColumns.DEVICE_CLOUD_TIME,
						DeviceColumns.DEVICE_COLLECTION_EXPIRATION_DATE,
						DeviceColumns.DEVICE_COLLECTION_LIMIT,
						DeviceColumns.DEVICE_EXPIRATION_DELTA_TICKS,
						DeviceColumns.DEVICE_LAST_TSL_RESET_TIME,
						DeviceColumns.DEVICE_PRINT_FORMAT
					}, // Projection 
					whereClause.toString(), // selection
					null, //selectionArgs, 
					null, null, null);
	        return cursor;
		} finally {
			//this.dbHelper.closeWritableDb();
		}
	}
	
	public static Cursor findDeviceByName(Activity activity, String deviceName) {
		StringBuffer whereClause = new StringBuffer();
		whereClause.append(DeviceColumns.DEVICE_NAME).append(" = '").append(deviceName).append("'");
		
		Cursor cursor = activity.managedQuery(
				EcollectorDeviceDetailsContentProvider.CONTENT_URI,// URL
				new String[]{ 
						DeviceColumns._ID,
						DeviceColumns.DEVICE_NAME,
						DeviceColumns.DEVICE_IDENTIFIER
						}, // Projection
				whereClause.toString(), // selection 
				null, // selectionArgs
				null  // SortOrder eg: COLLATE LOCALIZED ASC
				);
		return cursor;
	}
	
	public static boolean isDevicePaired(ContentResolver contentResolver) {
		Cursor c = null;
		try {
			c = EcollectorDB.createDeviceSearchCursor(contentResolver, null);
			logCursor(c);
			
			if (c.getCount() == 0) return false;
			if (!c.moveToFirst()) return false;
			String deviceIdentifier = c.getString(2);
			if (TextUtils.isEmpty(deviceIdentifier)) {
				return false;
			} else {
				return true;
			}
		} finally {
			if (c != null) c.close();
		}
	}
	
	public static String fetchDeviceIdentifier(ContentResolver contentResolver) {
		Cursor c = null;
		try {
			c = EcollectorDB.createDeviceSearchCursor(contentResolver, null);
			if (c.getCount() == 0) return null;
			if (!c.moveToFirst()) return null;
			return c.getString(2);	
		} finally {
			if (c != null) c.close();
		}
	}
	
	public static String fetchDeviceIdentifier(Context context) {
		Cursor c = null;
		try {
			c = EcollectorDB.createDeviceSearchCursor(context, null);
			if (c.getCount() == 0) return null;
			if (!c.moveToFirst()) return null;
			return c.getString(2);	
		} finally {
			if (c != null) c.close();
		}
	}
	
	public static String fetchDeviceName(ContentResolver contentResolver) {
		Cursor c = null;
		try {
			c = EcollectorDB.createDeviceSearchCursor(contentResolver, null);
			if (c == null || c.getCount() == 0) return null;
			if (!c.moveToFirst()) return null;
			String deviceName = c.getString(1); 
			return deviceName;
		} finally {
			if (c != null) c.close();
		}
	}
	
	public static String fetchDeviceName(Context context) {
		Cursor c = null;
		try {
			c = EcollectorDB.createDeviceSearchCursor(context, null);
			if (c == null || c.getCount() == 0) return null;
			if (!c.moveToFirst()) return null;
			String deviceName = c.getString(1); 
			return deviceName;
		} finally {
			if (c != null) c.close();
		}
	}
	
	public static long fetchDeviceLastTslResetTime(Context context) {
		Cursor c = null;
		try {
			c = EcollectorDB.createDeviceSearchCursor(context, null);
			if (c == null || c.getCount() == 0) return 0;
			if (!c.moveToFirst()) return 0;
			long lastTslResetTime = c.getLong(11); 
			return lastTslResetTime;
		} finally {
			if (c != null) c.close();
		}
	}
	
	public static String[] fetchCredentials(Context context) {
		/*Cursor c = EcollectorDB.createDeviceSearchCursor(context, null);
		if (c.getCount() == 0) return null;
		if (!c.moveToFirst()) return null;
		return new String[]{c.getString(3),c.getString(4)};*/
		Cursor c = null;
		try {
			c = EcollectorDB.createDeviceSearchCursor(context, null);
			if (c.getCount() == 0) return null;
			if (!c.moveToFirst()) return null;
			return new String[]{c.getString(3),c.getString(4)};	
		} finally {
			if (c != null) c.close();
		}
	}
	
	public static Double fetchCollectionLimit(ContentResolver contentResolver) {
		Cursor c = null;
		try {
			c = EcollectorDB.createDeviceSearchCursor(contentResolver, null);
			if (c.getCount() == 0) return -1d;
			if (!c.moveToFirst()) return -1d;
			String colLimit = c.getString(9);
			if (colLimit == null || colLimit.trim().length() == 0) return -1d;
			return Util.asDouble(colLimit);
		} finally {
			if (c != null) c.close();
		}
	}
	
	public static String fetchExpirationDate(ContentResolver contentResolver) {
		Cursor c = null;
		try {
			c = EcollectorDB.createDeviceSearchCursor(contentResolver, null);
			if (c.getCount() == 0) return null;
			if (!c.moveToFirst()) return null;
			String expDate = c.getString(8);
			return expDate;
		} finally {
			if (c != null) c.close();
		}
	}
	
	public static long fetchExpirationDeltaTicks(ContentResolver contentResolver) {
		Cursor c = null;
		try {
			c = EcollectorDB.createDeviceSearchCursor(contentResolver, null);
			if (c.getCount() == 0) return 0;
			if (!c.moveToFirst()) return 0;
			long deltaTicks = c.getLong(DeviceColumns.DEVICE_EXPIRATION_DELTA_TICKS_COL_INDEX);
			return deltaTicks;
		} finally {
			if (c != null) c.close();
		}
	}
	
	public static String fetchExpirationDeltaTicksReadable(ContentResolver contentResolver) {
		long deltaTicks = EcollectorDB.fetchExpirationDeltaTicks(contentResolver);
		if (deltaTicks < 0) return "--:--:--";
		
		int SECOND = 1;
		int MINUTE = 60 * 1;
		int HOUR = MINUTE * 60;
		int DAY = HOUR * 24;
		
		int days = (int) (deltaTicks / DAY);
		int hours = (int)( (deltaTicks - (days*DAY)) / HOUR);  
		int minutes = (int)( (deltaTicks - (days*DAY + hours*HOUR)  ) / MINUTE );
		//int seconds = (int)( (deltaTicks - (days*DAY + hours*HOUR + minutes*MINUTE)  ) / SECOND );
		return days+"d "+hours+"h " + minutes + "m "/* + seconds + "s"*/;
	}
	
	/*Returns string[], 0 - printer name, 1 - printer address */
	public static String[] fetchPrinterConfig(ContentResolver contentResolver) {
		Cursor c = null;
		try {
			c = EcollectorDB.createDeviceSearchCursor(contentResolver, null);
			if (c.getCount() == 0) return null;
			if (!c.moveToFirst()) return null;
			return new String[]{c.getString(5),c.getString(6)};
		} finally {
			if (c != null) c.close();
		}
	}
	
	public static PrintFormat fetchPrintFormat(Context context) {
		Cursor c = null;
		String printFormat = null;
		try {
			c = EcollectorDB.createDeviceSearchCursor(context, null);
			if (c.getCount() == 0) return null;
			if (!c.moveToFirst()) return null;
			printFormat = c.getString(12);
		} finally {
			if (c != null) c.close();
		}
		
		PrintFormat pf = PrintFormat.PLAIN_PAPER;
		if (printFormat == null) {
			return pf;
		}
		
		if (printFormat.equalsIgnoreCase(PrintFormat.PLAIN_PAPER.toString())) {
			pf = PrintFormat.PLAIN_PAPER;
		} else if (printFormat.equalsIgnoreCase(PrintFormat.PRE_PRINTED_PAPER.toString())) {
			pf = PrintFormat.PRE_PRINTED_PAPER;
		}
		return pf;
	}
	
	public static void pairDevice(Context context, 
			String deviceName, String deviceIdentifier, 
			String cloudUserName, byte[] cloudPassword) {
		
		if (deviceName == null || deviceName.trim().length() == 0) {
			String msg = "DevicePairing device name is empty, pairing failed";
			Log.i(TAG, msg);
			throw new IllegalArgumentException(msg);
		}
		
		ContentResolver contentResolver = context.getContentResolver();
		EcollectorDB.deleteDevices(context);
		Cursor c = null;
		try {
			c = EcollectorDB.createDeviceSearchCursor(context.getContentResolver(), deviceName);
			ContentValues cv = new ContentValues();
			cv.put(DeviceColumns.DEVICE_NAME, deviceName);
			cv.put(DeviceColumns.DEVICE_IDENTIFIER, deviceIdentifier);
			cv.put(DeviceColumns.DEVICE_CLD_UN, cloudUserName);
			cv.put(DeviceColumns.DEVICE_CLD_PWD, new String(cloudPassword));
			if (c.getCount() == 0) {
				contentResolver.insert(
						EcollectorDeviceDetailsContentProvider.CONTENT_URI, cv);
			} else {
				Uri uri = Uri.withAppendedPath(
						EcollectorDeviceDetailsContentProvider.CONTENT_URI, deviceName);
				contentResolver.update(uri, cv, null, null);
			}
		} finally {
			if (c != null) c.close();
		}
	}
	
	public static void savePrinterConfig(ContentResolver contentResolver, 
			String printerName, String printerAddress) {
		String deviceName = EcollectorDB.fetchDeviceName(contentResolver);
		Cursor c = null;
		try {
			c = EcollectorDB.createDeviceSearchCursor(contentResolver, deviceName);
			ContentValues cv = new ContentValues();
			cv.put(DeviceColumns.DEVICE_NAME, deviceName);
			cv.put(DeviceColumns.DEVICE_PRINTER_NAME, printerName);
			cv.put(DeviceColumns.DEVICE_PRINTER_ADDR, printerAddress);
			if (c.getCount() == 0) {
				contentResolver.insert(
						EcollectorDeviceDetailsContentProvider.CONTENT_URI, cv);
			} else {
				Uri uri = Uri.withAppendedPath(
						EcollectorDeviceDetailsContentProvider.CONTENT_URI, deviceName);
				contentResolver.update(uri, cv, null, null);
			}
		} finally {
			if (c != null) c.close();
		}
	}
	
	public static void savePrintFormat(ContentResolver contentResolver, PrintFormat printFormat) {
		String deviceName = EcollectorDB.fetchDeviceName(contentResolver);
		Cursor c = null;
		try {
			c = EcollectorDB.createDeviceSearchCursor(contentResolver, deviceName);
			ContentValues cv = new ContentValues();
			cv.put(DeviceColumns.DEVICE_NAME, deviceName);
			cv.put(DeviceColumns.DEVICE_PRINT_FORMAT, printFormat.toString());
			if (c.getCount() == 0) {
				contentResolver.insert(
						EcollectorDeviceDetailsContentProvider.CONTENT_URI, cv);
			} else {
				Uri uri = Uri.withAppendedPath(
						EcollectorDeviceDetailsContentProvider.CONTENT_URI, deviceName);
				contentResolver.update(uri, cv, null, null);
			}
		} finally {
			if (c != null) c.close();
		}
	}
	
	public static void updateDeviceState(UserContext userContext, Context context) {
		DeviceUi d = userContext.getDeviceUi();
		if (d == null) return;
		
		/* Sync device state */
		String deviceName = EcollectorDB.fetchDeviceName(context);
		Cursor c = EcollectorDB.createDeviceSearchCursor(context, deviceName);
		ContentValues cv = new ContentValues();
		
		String cloudTime = Util.getFormattedDate(d.getCloudTime(),Util.MACHINE_DATE_TIME_FORMAT);
		Date expirationDate = d.getCollectionExpirationDate();
		if (expirationDate == null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(d.getCloudTime());
			cal.add(Calendar.DAY_OF_MONTH, 30);
			expirationDate = cal.getTime();
		}
		String collectionExpiryDate = 
			Util.getFormattedDate(Util.trimTime(expirationDate),Util.MACHINE_DATE_TIME_FORMAT);
		long deltaSecs = Util.computeDeltaSecs(d.getCloudTime(),Util.trimTime(d.getCollectionExpirationDate()));

		cv.put(DeviceColumns.DEVICE_NAME, deviceName);
		cv.put(DeviceColumns.DEVICE_CLOUD_TIME, cloudTime);
		cv.put(DeviceColumns.DEVICE_COLLECTION_EXPIRATION_DATE, collectionExpiryDate);
		cv.put(DeviceColumns.DEVICE_COLLECTION_LIMIT, d.getCollectionLimit());
		cv.put(DeviceColumns.DEVICE_EXPIRATION_DELTA_TICKS, deltaSecs);
		
		if (c.getCount() == 0) {
			/*contentResolver.insert(
					EcollectorDeviceDetailsContentProvider.CONTENT_URI, cv);*/
			EcollectorDB.insertDeviceDetails(context, cv);
		} else {
			/*Uri uri = Uri.withAppendedPath(
					EcollectorDeviceDetailsContentProvider.CONTENT_URI, deviceName);*/
			EcollectorDB.updateDeviceDetails(context, cv, deviceName);
		}
		/* End of Sync device state */
		
		/* Sync Device commands */
		List<DeviceCommandUi> deviceCommands = d.getDeviceCommands();
		EcollectorDB.syncDeviceCommandsFromCloud(context, deviceCommands);
		/* Sync Device commands */
	}
	
	private static void syncDeviceCommandsFromCloud(Context context,
			List<DeviceCommandUi> deviceCommands) {
		if (deviceCommands == null || deviceCommands.isEmpty()) {
			Log.i(TAG,"No device commands to sync");
			return;
		}
		Log.i(TAG,"Syncing device commands (count) "+deviceCommands.size());
		for (DeviceCommandUi dc : deviceCommands) {
			try {
				if (!AppConfig.DEVICE_COMMAND_STATES.NEW.toString().equals(dc.getState())) {
					Log.i(TAG, "Command not synced as its state is not new, state: "+dc.getState()
							+", code: "+dc.getCommandCode()+", id: "+dc.getId());
					continue;
				}
				String deviceCommandId = dc.getId();
				Log.i(TAG, "Syncing device command, state: "+dc.getState()
						+", code: "+dc.getCommandCode()+", id: "+deviceCommandId);
				if (!EcollectorDB.doesDeviceCommandExists(context, deviceCommandId)) {
					ContentValues cv = new ContentValues();
					cv.put(DeviceCommandColumns.COMMAND_DATE, Util.getFormattedDate(dc.getCommandDate()));
					cv.put(DeviceCommandColumns.COMMAND_ID, deviceCommandId);
					cv.put(DeviceCommandColumns.COMMANDCODE, dc.getCommandCode());
					cv.put(DeviceCommandColumns.COMMANDPARAM1, dc.getCommandParam1());
					cv.put(DeviceCommandColumns.COMMANDPARAM2, dc.getCommandParam2());
					cv.put(DeviceCommandColumns.COMMANDPARAM3, dc.getCommandParam3());
					cv.put(DeviceCommandColumns.COMMANDTIMESTAMP, dc.getCommandTimestamp());
					cv.put(DeviceCommandColumns.EXECUTIONDATE, Util.getFormattedDate(dc.getExecutionDate()));
					cv.put(DeviceCommandColumns.EXECUTIONNOTES, dc.getExecutionNotes());
					EcollectorDB.insertDeviceCommand(context, cv);
				} else {
					Log.i(TAG, "Device command already exists."+" Code: "+dc.getCommandCode()+
						  ", id: "+deviceCommandId);
				}
			} catch(Throwable t) {
				Log.e(TAG, "Failed to create device command, "+dc.getCommandCode()+
						", id: "+dc.getId()+", errorMessage: "+t.getMessage());
			}
		}
	}
	
	private static boolean doesDeviceCommandExists(Context context, String deviceCommandId) {
		Cursor c = null;
		try {
			 c = EcollectorDB.createDeviceCommandSearchCursor(context, null, deviceCommandId);
			 return c != null && c.moveToFirst();
		} finally {
			if (c != null) c.close();
		}
	}

	public static void consumeExpirationDelta(Context context,
			long amount) {
		Cursor c = null;
		long oldDeltaTicks = 0;
		String deviceName = null;
		try {
			deviceName = EcollectorDB.fetchDeviceName(context);
		} catch (Throwable t) {
			Log.i(TAG,"Error fetching device name",t);
			return;
		}
		
		if (deviceName == null) {
			Log.i(TAG,"Device is not paired, cannot consume expiration delta");
			return;
		}
		
		try {
			c = EcollectorDB.createDeviceSearchCursor(context, deviceName);
			if (c.getCount() == 0) {
				Log.i(TAG,"Device Not Paired, cannot consume expiration delta");
				return;
			}
			if (c.moveToFirst()) {
				oldDeltaTicks = c.getLong(DeviceColumns.DEVICE_EXPIRATION_DELTA_TICKS_COL_INDEX);
			}
		} catch(Throwable t) {
			Log.w(TAG,"Error fetching device details",t);
			return;
		}
		
		finally {
			if (c != null) c.close();
		}
		
		long newValue = oldDeltaTicks - amount;
		Log.i(TAG,"Consuming ExpirationDelta by (secs): "+amount+
				", Old Value: "+oldDeltaTicks+
				", New Value: "+newValue);
		ContentValues cv = new ContentValues();
		cv.put(DeviceColumns.DEVICE_EXPIRATION_DELTA_TICKS, newValue);
		/*Uri uri = Uri.withAppendedPath(
					EcollectorDeviceDetailsContentProvider.CONTENT_URI, deviceName);
		context.update(uri, cv, null, null);*/
		
		try {
			EcollectorDB.updateDeviceDetails(context, cv, deviceName);
		} catch (Throwable t) {
			Log.w(TAG, "Error updating device details", t);
		}
	}
	
	public static void setDeviceLastTslResetTime(long value, Context context) {
		String deviceName = EcollectorDB.fetchDeviceName(context);
		if (deviceName == null || deviceName.trim().length() == 0) {
			return;
		}
		
		ContentValues cv = new ContentValues();
		cv.put(DeviceColumns.DEVICE_LAST_TSL_RESET_TIME, value);
		try {
			EcollectorDB.updateDeviceDetails(context, cv, deviceName);
		} catch (Throwable t) {
			Log.w(TAG, "Error updating device details", t);
		}
	}
	
	public static int updateDeviceDetails(Context context, ContentValues contentvalues, String deviceName) {
		EcollectorDB instance = EcollectorDB.getInstance(context);
		SQLiteDatabase db = null;
		try {
			db = instance.getDatabaseToWrite();
			if (deviceName == null || deviceName.trim().equals("")) {
				return -1;
			}	
			String whereClause = DeviceColumns.DEVICE_NAME + " = '" + deviceName + "'";
			int updateCount = db.update(
					EcollectorDB.TABLE_DEVICE, contentvalues, whereClause, null);
			//Log.i(TAG,"update device details, count: "+updateCount);
			return updateCount;
		} finally {
			//this.dbHelper.closeWritableDb();
		}
	}
	
	public static long insertDeviceDetails(Context context, 
			ContentValues contentvalues) {
		EcollectorDB instance = EcollectorDB.getInstance(context);
		SQLiteDatabase db = null;
		try {
			db = instance.getDatabaseToWrite();
			long rowId = db.insert(
					EcollectorDB.TABLE_DEVICE, "TITLE", contentvalues);
			//Log.i(TAG,"update device details, count: "+updateCount);
			return rowId;
		} finally {
			//this.dbHelper.closeWritableDb();
		}
	}
	
	@Deprecated
	private static List<CharSequence> serializePrs(Cursor cursor, int batchSize) {
		if (cursor == null) return Collections.EMPTY_LIST;
		
		if (batchSize < 10) batchSize = 10;
		List<CharSequence> sList = new ArrayList<CharSequence>();
		StringBuffer s = new StringBuffer();
		int rc = 0;
		for (rc = 0; rc < cursor.getCount(); rc++) {
			cursor.moveToPosition(rc);
			int i = 0;
			for (String columnName : PrColumns.PUSH_PULL_COLUMN_ORDER) {
				String columnValue = cursor.getString(cursor.getColumnIndex(columnName));
				if (columnValue == null) columnValue = "";
				s.append(columnValue);
				
				if (i < PrColumns.PUSH_PULL_COLUMN_ORDER.size()-1) {
					s.append(ServiceColumns.C_FIELD_SEPERATOR);
				}
				i++;
			}
			if (rc < cursor.getCount() - 1) {
				s.append(ServiceColumns.C_RECORD_SEPERATOR);
			}
			
			if (rc+1 % batchSize == 0) {
				sList.add(s);
				s = new StringBuffer();
			}
 		}
		
		if (rc+1 % batchSize > 0) {
			sList.add(s);
		}
		
		return sList;
	}
	
	public static void prepareDevicePrList(Cursor cursor, BatchListener<List<DevicePrUi>> b, int batchSize) {
		if (cursor == null) return;
		List<DevicePrUi> sList = new ArrayList<DevicePrUi>();
		int rc = 0;
		DevicePrUi devicePr;
		for (rc = 0; rc < cursor.getCount(); rc++) {
			cursor.moveToPosition(rc);
			devicePr = new DevicePrUi();
			try {
				devicePr.set_DEVICE_PR_CODE(cursor.getString(cursor.getColumnIndex(PrColumns._DEVICE_PR_CODE)));
				devicePr.setACD_COLLECTED(cursor.getString(cursor.getColumnIndex(PrColumns.ACD_COLLECTED)));
				devicePr.setAGL_AMOUNT(cursor.getString(cursor.getColumnIndex(PrColumns.AGL_AMOUNT)));
				devicePr.setAGL_SERVICES(cursor.getString(cursor.getColumnIndex(PrColumns.AGL_SERVICES)));
				devicePr.setARREARS_N_DEMAND(cursor.getString(cursor.getColumnIndex(PrColumns.ARREARS_N_DEMAND)));
				devicePr.setCMD_COLLECTED(cursor.getString(cursor.getColumnIndex(PrColumns.CMD_COLLECTED)));
				devicePr.setCOLLECTION_DATE(cursor.getString(cursor.getColumnIndex(PrColumns.COLLECTION_DATE)));
				devicePr.setCOLLECTION_DELTA(cursor.getString(cursor.getColumnIndex(PrColumns.COLLECTION_DELTA)));
				devicePr.setCOLLECTION_TIME(cursor.getString(cursor.getColumnIndex(PrColumns.COLLECTION_TIME)));
				devicePr.setDISTRIBUTION(cursor.getString(cursor.getColumnIndex(PrColumns.DISTRIBUTION)));
				devicePr.setERO(cursor.getString(cursor.getColumnIndex(PrColumns.ERO)));
				devicePr.setLAST_PAID_AMOUNT(cursor.getString(cursor.getColumnIndex(PrColumns.LAST_PAID_AMOUNT)));
				devicePr.setLAST_PAID_DATE(cursor.getString(cursor.getColumnIndex(PrColumns.LAST_PAID_DATE)));
				devicePr.setLAST_PAID_RECEIPT_NO(cursor.getString(cursor.getColumnIndex(PrColumns.LAST_PAID_RECEIPT_NO)));
				devicePr.setMACHINE_CODE(cursor.getString(cursor.getColumnIndex(PrColumns.MACHINE_CODE)));
				devicePr.setMACHINE_NUMBER(cursor.getString(cursor.getColumnIndex(PrColumns.MACHINE_NUMBER)));
				devicePr.setNEW_ARREARS(cursor.getString(cursor.getColumnIndex(PrColumns.NEW_ARREARS)));
				devicePr.setOTHERS_COLLECTED(cursor.getString(cursor.getColumnIndex(PrColumns.OTHERS_COLLECTED)));
				devicePr.setRC_CODE(cursor.getString(cursor.getColumnIndex(PrColumns.RC_CODE)));
				devicePr.setRC_COLLECTED(cursor.getString(cursor.getColumnIndex(PrColumns.RC_COLLECTED)));
				devicePr.setRECEIPT_NUMBER(cursor.getString(cursor.getColumnIndex(PrColumns.RECEIPT_NUMBER)));
				devicePr.setREMARKS(cursor.getString(cursor.getColumnIndex(PrColumns.REMARKS)));
				devicePr.setREMARKS_AMOUNT(cursor.getString(cursor.getColumnIndex(PrColumns.REMARKS_AMOUNT)));
				devicePr.setSC_NO(cursor.getString(cursor.getColumnIndex(PrColumns.SC_NO)));
				devicePr.setSECTION(cursor.getString(cursor.getColumnIndex(PrColumns.SECTION)));
				devicePr.setTC_SEAL_NO(cursor.getString(cursor.getColumnIndex(PrColumns.TC_SEAL_NO)));
				devicePr.setUSC_NO(cursor.getString(cursor.getColumnIndex(PrColumns.USC_NO)));
				devicePr.setAMOUNT_COLLECTED(cursor.getString(cursor.getColumnIndex(PrColumns.AMOUNT_COLLECTED)));
				sList.add(devicePr);
			} catch (Throwable e) {
				Log.i("EcollectorDB","Error while preparing Pushable pr",e);
			}
			if ( (rc+1) % batchSize == 0) {
				b.batchComplete(sList);
				sList.clear();
			}
 		}
		if (sList.size() > 0) {
			b.batchComplete(sList);
		}
	}

	public static void saveServiceDetails(String serviceId, Context context,
			ContentValues values) {
		Uri uri = Uri.withAppendedPath(EcollectorContentProvider.CONTENT_URI, 
				serviceId);
		context.getContentResolver().update(
				uri, values, null, null);
	}
	
	public static int updatePrsAsPushComplete(String[] prCodes, 
			String status, Context context) {
		if (prCodes != null && prCodes.length == 0) {
			return -1;
		}
		
		List<String> prCodesQuoted = new ArrayList<String>();
		for (String prId : prCodes) {
			if (prId != null && !prId.trim().equals("")) {
				prCodesQuoted.add("'"+prId+"'");
			}
		}
		
		if (prCodesQuoted.isEmpty()) {
			return -1;
		}
		
		ContentValues values = new ContentValues();
		values.put(PrColumns._PUSH_STATE, 
					ServiceColumns.C_PUSH_STATE_COMPLETE);
		
		StringBuffer whereClause = prepareInClause(PrColumns._DEVICE_PR_CODE,(String[])prCodesQuoted.toArray(new String[]{}));
		SQLiteDatabase db = EcollectorDB.getInstance(context).getDatabaseToWrite();;
		return db.update(EcollectorDB.TABLE_PR, values, 
				whereClause.toString(), null);
	}
	
	public static void tuneDbForFasterInserts(Context context) {
		// Ref: http://sqlite.org/pragma.html#pragma_synchronous
		// for faster bulk inserts
		SQLiteDatabase db = EcollectorDB.getInstance(context).getDatabaseToWrite();;
		
		Cursor c = db.rawQuery("PRAGMA synchronous", null);
		printCursor(c);
		
		c = db.rawQuery("PRAGMA synchronous = 0", null);
		printCursor(c);
		
		c = db.rawQuery("PRAGMA synchronous", null);
		printCursor(c);
	}
	
	private static void printCursor(Cursor c) {
		if (c.moveToFirst()) {
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < c.getColumnCount(); i ++ ){
				sb.append(c.getColumnName(i)).append("::").append(c.getString(i));
			}
			Log.i(TAG, "tuneDb: "+sb);
		}
	}
	
	private static StringBuffer prepareInClause(String idColumn, String[] prIds) {
		StringBuffer whereClause = new StringBuffer();
		whereClause.append(idColumn).append(" IN (");
		for (int i = 0; i < prIds.length; i++) {
			whereClause.append(prIds[i]);
			if (i < prIds.length - 1) {
				whereClause.append(",");
			}
		}
		whereClause.append(")");
		return whereClause;
	}

	public static final String REMARKS_EXCLUDING_ARREAR = "A";
	public static final String REMARKS_EXCLUDING_DEMAND = "B";
	public static final String REMARKS_PART_PAYMENT = "C";
	public static final String REMARKS_RC_EXCLUDED = "D";
	public static final String REMARKS_NEGATIVE_BALANCE_EXCLUDED = "E";
	public static final String REMARKS_ACD_COLLECTED = "F"; // COLLECT ONLY ACD
	public static final String REMARKS_THEFT_MALPRACTICE = "G";
	public static final String REMARKS_BACKBILLINB = "H";
	public static final String REMARKS_INSTALLMENT = "I"; // EXCLUDING ACD
	public static final String REMARKS_DEMAND_ADDED = "J"; // PROVIDED AS A BUTTON DO NOT USE IN CURSOR
	public static final String REMARKS_AGL_COLLECTION = "K"; // DO NOT SHOW IN CURSOR
	public static final String REMARKS_EXCLUDING_ACD = "L";
	
	private static Map<String,String> remarksDescriptionMap = new LinkedHashMap<String,String>();
	private static Map<String,String> remarksPrintableMap = new HashMap<String,String>();
	private static Map<String,String> exemptorMap = new LinkedHashMap<String,String>();
	
	static {
		remarksDescriptionMap.put(REMARKS_EXCLUDING_ARREAR,"Excluding Arrear");
		remarksPrintableMap.put(REMARKS_EXCLUDING_ARREAR, "EXMPTD.ARREARS");
		remarksDescriptionMap.put(REMARKS_EXCLUDING_DEMAND,"Excluding Demand");
		remarksPrintableMap.put(REMARKS_EXCLUDING_DEMAND, "EXMPTD.DMD");
		remarksDescriptionMap.put(REMARKS_PART_PAYMENT,"Part Payment");
		remarksPrintableMap.put(REMARKS_PART_PAYMENT,"PART.PMT");
		remarksDescriptionMap.put(REMARKS_RC_EXCLUDED,"RC Excluded");
		remarksPrintableMap.put(REMARKS_RC_EXCLUDED,"RC.EXMPTD");
		remarksDescriptionMap.put(REMARKS_NEGATIVE_BALANCE_EXCLUDED,"-Ve balance excluded");
		remarksPrintableMap.put(REMARKS_NEGATIVE_BALANCE_EXCLUDED,"NEG.BAL.EXMPTD");
		remarksDescriptionMap.put(REMARKS_ACD_COLLECTED,"ACD Collected");
		remarksPrintableMap.put(REMARKS_ACD_COLLECTED,"ACD.CLCTD");
		/*remarksDescriptionMap.put(REMARKS_THEFT_MALPRACTICE,"Theft/Malpractice");
		remarksPrintableMap.put(REMARKS_THEFT_MALPRACTICE,"THFT.MLPRCT");
		remarksDescriptionMap.put(REMARKS_BACKBILLINB,"Backbilling");
		remarksPrintableMap.put(REMARKS_BACKBILLINB,"BCK.BILG");*/
		remarksDescriptionMap.put(REMARKS_INSTALLMENT,"Installment");
		remarksPrintableMap.put(REMARKS_INSTALLMENT,"INSTLMT");
		remarksDescriptionMap.put(REMARKS_DEMAND_ADDED,"Demand added");
		remarksPrintableMap.put(REMARKS_DEMAND_ADDED,"DMD.ADDED");
		remarksDescriptionMap.put(REMARKS_AGL_COLLECTION,"AGL collection");
		remarksPrintableMap.put(REMARKS_AGL_COLLECTION,"AGL.CLCTN");
		remarksDescriptionMap.put(REMARKS_EXCLUDING_ACD,"Excluding ACD");
		remarksPrintableMap.put(REMARKS_EXCLUDING_ACD,"EXMPTD.ACD");
		
		exemptorMap.put("1","AAO");
		exemptorMap.put("2","AE");
		exemptorMap.put("3","ADE");
		exemptorMap.put("4","DE");
		exemptorMap.put("5","OTHERS");
	}
	public static Cursor createRemarksCursor() {
		MatrixCursor mc = 
			new MatrixCursor(new String[]{"_ID","REMARK","DESCRIPTION"},1);
		mc.addRow(new String[]{"-1","NONE","NONE"});
		int i = 0;
		for (Entry<String,String> e : remarksDescriptionMap.entrySet()) {
			if (e.getKey().equals(REMARKS_DEMAND_ADDED) || 
				e.getKey().equals(REMARKS_AGL_COLLECTION)	) {
				continue;
			}
				
			mc.addRow(new String[]{i+"",e.getKey(),e.getValue()});
			i++;
		}
		return mc;
	}
	
	public static Cursor createExemptorCursor() {
		MatrixCursor mc = 
			new MatrixCursor(new String[]{"_ID","EXEMPTOR","LABEL"},1);
		mc.addRow(new String[]{"-1","NONE","NONE"});
		int i = 0;
		for (Entry<String,String> e : exemptorMap.entrySet()) {
			mc.addRow(new String[]{i+"",e.getKey(),e.getValue()});
			i++;
		}
		return mc;
	}
	
	public static Cursor createRcAmountCursor() {
		MatrixCursor mc = 
			new MatrixCursor(new String[]{"_ID","AMOUNT"},1);
		mc.addRow(new String[]{"0","0"});
		mc.addRow(new String[]{"25","25"});
		mc.addRow(new String[]{"75","75"});
		return mc;
	}
	
	public static String getRemarksPrintable(String remarks) {
		return sensibleRemarksString(remarks, remarksPrintableMap);
	}
	
	public static String getRemarksDescription(String remarks) {
		return sensibleRemarksString(remarks, remarksDescriptionMap);
	}
	
	public static String sensibleRemarksString(String remarks, Map<String, String> map) {
		if (remarks == null || remarks.trim().length() == 0) {
			return "";
		}
		remarks = remarks.trim();
		StringBuffer r = new StringBuffer();
		for (int i = 0; i < remarks.length(); i++) {
			String remark = map.get(remarks.charAt(i) + "");
			if (remark == null) continue;
			r.append(remark);
			if (i < remarks.length() - 1) {
				r.append(",");
			}
		}
		return r.toString();
	}
	
	public static String getExemptorLabel(String exemptor) {
		return exemptorMap.get(exemptor);
	}
	
	public static long getNextRecieptNo(Context context) {
		return EcollectorDB.getNextSequence("RECIEPT_NO", context);
	}
	
	public static String getNextTslNo(Context context) {
		long tslNo = EcollectorDB.getNextSequence("TSL_NO", context);
		return FormatUtils.numberPadding(tslNo+"",3);
	}
	
	private static long getNextSequence(String key, Context context) {
		String keyWhereClause = SequenceHolderColumns.SEQ_KEY + " = '"+key+"'";
		SQLiteDatabase db = EcollectorDB.getInstance(context).getReadableDatabase();
		Cursor cursor = db.query(EcollectorDB.TABLE_SEQ_HOLDER, 
					new String[]{
						SequenceHolderColumns._ID,
						SequenceHolderColumns.SEQ_KEY,
						SequenceHolderColumns.NEXT_VALUE
						}, 
					keyWhereClause, 
					null,null, null, null);
		if (cursor == null) {
			throw new NullPointerException("sequence holder cursor got null");
		}
		
		String keyDb = null;
		long nextValue = 1;
		if (cursor.moveToFirst()) {
			keyDb = cursor.getString(1);
			nextValue = cursor.getLong(2);
		}
		
		if (keyDb == null) {
			ContentValues values = new ContentValues();
			values.put(SequenceHolderColumns.SEQ_KEY, key);
			values.put(SequenceHolderColumns.NEXT_VALUE, nextValue+1);
			db.insert(EcollectorDB.TABLE_SEQ_HOLDER, SequenceHolderColumns.SEQ_KEY, 
					values);
		} else {
			ContentValues values = new ContentValues();
			values.put(SequenceHolderColumns.NEXT_VALUE, nextValue+1);
			db.update(EcollectorDB.TABLE_SEQ_HOLDER, values,
					keyWhereClause,null);
		}
		return nextValue;
	}
	
	public static void setNextRecieptNo(int value, Context context) {
		EcollectorDB.setNextSequence("RECIEPT_NO", value, context);
	}
	
	public static void setNetxtTslNo(int value, Context context) {
		EcollectorDB.setNextSequence("TSL_NO", value, context);
	}

	private static void setNextSequence(String key, int value, Context context) {
		String keyWhereClause = SequenceHolderColumns.SEQ_KEY + " = '"+key+"'";
		SQLiteDatabase db = EcollectorDB.getInstance(context).getReadableDatabase();
		Cursor cursor = db.query(EcollectorDB.TABLE_SEQ_HOLDER, 
					new String[]{
						SequenceHolderColumns._ID,
						SequenceHolderColumns.SEQ_KEY,
						SequenceHolderColumns.NEXT_VALUE
						}, 
					keyWhereClause, 
					null,null, null, null);
		if (cursor == null) {
			throw new NullPointerException("sequence holder cursor got null");
		}
		
		String keyDb = null;
		if (cursor.moveToFirst()) {
			keyDb = cursor.getString(1);
		}
		
		if (keyDb == null) {
			ContentValues values = new ContentValues();
			values.put(SequenceHolderColumns.SEQ_KEY, key);
			values.put(SequenceHolderColumns.NEXT_VALUE, value);
			db.insert(EcollectorDB.TABLE_SEQ_HOLDER, SequenceHolderColumns.SEQ_KEY, 
					values);
		} else {
			ContentValues values = new ContentValues();
			values.put(SequenceHolderColumns.NEXT_VALUE, value);
			db.update(EcollectorDB.TABLE_SEQ_HOLDER, values,
					keyWhereClause,null);
		}
	}
	
	public static void savePrDetails(Context activity,ContentValues prValues) {
		EcollectorDB.savePrDetails(activity.getContentResolver(), prValues);
	}
	
	public static void savePrDetails(ContentResolver contentResolver,ContentValues prValues) {
		
		String deviceName = EcollectorDB.fetchDeviceName(contentResolver);
		
		prValues.put(PrColumns._PUSH_STATE, ServiceColumns.C_PUSH_STATE_READY);
		prValues.put(PrColumns._ARCHIVE_STATE, PrColumns.C_ARCHIVE_STATE_READY);
		prValues.put(PrColumns.MACHINE_NUMBER, deviceName);
		Uri uri = EcollectorPrContentProvider.CONTENT_URI;
		contentResolver.insert(
				uri, prValues);		
		
		// Perform servcie push to cloud
		/*ServicePushAction spa = new ServicePushAction(activity);
		spa.onClick(null);*/
	}
	
	public static void saveSummaryReport(ContentResolver contentResolver, 
			ContentValues values) {
		Uri updateUri = Uri.withAppendedPath(SummaryReportContentProvider.CONTENT_URI,"1");
		int rowCount = contentResolver.update(updateUri, values, null, null);
		if (rowCount == 0) {
			Uri uri = SummaryReportContentProvider.CONTENT_URI;
			values.put(SummaryReportColumns._ID, 1);
			contentResolver.insert(uri, values);
		}
	}
	
	public static void saveSummaryReport(Context context, 
			ContentValues values) {
		StringBuffer whereClause = new StringBuffer();
		whereClause.append(SummaryReportColumns._ID).append(" = 1 ");
		
		SQLiteDatabase db = EcollectorDB.getInstance(context).getDatabaseToWrite();
		int rowCount = db.update(EcollectorDB.TABLE_SUMMARY_REPORT, 
				values, // values 
				whereClause.toString(), 
				null // where args
				);
		if (rowCount == 0) {
			values.put(SummaryReportColumns._ID, 1);
			db.insert(EcollectorDB.TABLE_SUMMARY_REPORT, 
					null, // nullCheckHack 
					values);
		}
	}
	
	public static Cursor fetchSummaryReportCursor(ContentResolver contentResolver) {
		StringBuffer whereClause = new StringBuffer();
		whereClause.append(SummaryReportColumns._ID).append(" = 1 ");
		Cursor cursor = contentResolver.query(
				SummaryReportContentProvider.CONTENT_URI,// URL
				new String[]{ 
						SummaryReportColumns._ID,
						SummaryReportColumns.TOTAL_PR_COUNT,
						SummaryReportColumns.TOTAL_PR_TO_PUSH_COUNT,
						SummaryReportColumns.TOTAL_SERVICES_COUNT,
						SummaryReportColumns.TOTAL_COLLECTION
						}, // Projection
				whereClause.toString(), // selection 
				null, // selectionArgs
				null  // SortOrder eg: COLLATE LOCALIZED ASC
				);
		return cursor;
	}
	
	public static void computeSummaryReportTotalServices(ContentResolver contentResolver, 
			ContentValues values) {
		ServiceSearchFilterBean i = new ServiceSearchFilterBean();
        Cursor c = null;
        try {
			c = createServiceSearchCursor(contentResolver, null, i);
            int count = c.getCount();
    		values.put(SummaryReportColumns.TOTAL_SERVICES_COUNT, count);
        } finally {
        	if (c != null) c.close();
        }
	}
	
	public static void computeSummaryReportPrStats(ContentResolver contentResolver,
			ContentValues values) {
		Cursor c = null;
		try {
			c = createPrCursor(contentResolver);
			fillSummaryReportPrStats(c, values);
		} finally {
			if (c != null) c.close();
		}
	}
	
	public static void computeSummaryReportPrStats(Context context,
			ContentValues values) {
		Cursor c = null;
		try {
			c = createPrCursor(context);
			fillSummaryReportPrStats(c, values);
		} finally {
			if (c != null) c.close();
		}
	}
	
	private static void fillSummaryReportPrStats(Cursor c, ContentValues values) {
		int count = c.getCount();
		values.put(SummaryReportColumns.TOTAL_PR_COUNT, count);
		values.put(SummaryReportColumns.TOTAL_COLLECTION, 0);
		
		if (count == 0) return;
		
		int readyToPushCount = 0;
		double collectedAmount = 0;
		int lastPaidAmtColIndex = c.getColumnIndex(PrColumns.AMOUNT_COLLECTED);
		int pushStateColIndex = c.getColumnIndex(PrColumns._PUSH_STATE);
		c.moveToFirst();
		do {
			if (ServiceColumns.C_PUSH_STATE_READY
					.equals(c.getString(pushStateColIndex))) {
				readyToPushCount++;
			}
			collectedAmount += c.getDouble(lastPaidAmtColIndex);
		} while (c.moveToNext());
		values.put(SummaryReportColumns.TOTAL_PR_TO_PUSH_COUNT, readyToPushCount);
		values.put(SummaryReportColumns.TOTAL_COLLECTION, collectedAmount);
	}
	
	public static void prepareSummaryReport(Context context) {
		ContentResolver contentResolver = context.getContentResolver();
		ContentValues values = new ContentValues();
		EcollectorDB.computeSummaryReportTotalServices(contentResolver, values);
		EcollectorDB.computeSummaryReportPrStats(contentResolver, values);
		EcollectorDB.saveSummaryReport(contentResolver, values);
		EcollectorDB.createDistributions(context);
	}

	public static void prepareSummaryReportPrStats(ContentResolver contentResolver) {
		ContentValues values = new ContentValues();
		EcollectorDB.computeSummaryReportPrStats(contentResolver, values);
		EcollectorDB.saveSummaryReport(contentResolver, values);
	}
	
	public static void prepareSummaryReportPrStats(Context context) {
		ContentValues values = new ContentValues();
		EcollectorDB.computeSummaryReportPrStats(context, values);
		EcollectorDB.saveSummaryReport(context, values);
	}
	
	public static void bumpSummaryReportPrStats(ContentResolver contentResolver,
			ContentValues values) {
		Cursor srCursor = 
			EcollectorDB.fetchSummaryReportCursor(contentResolver);
		int totalPrCount = 0;
		int totalPrToPushCount = 0;
		double totalCollection = 0;
		
		if (srCursor.moveToFirst()) {
			totalPrCount = srCursor.getInt(srCursor.getColumnIndex(SummaryReportColumns.TOTAL_PR_COUNT));
			totalPrToPushCount = srCursor.getInt(srCursor.getColumnIndex(SummaryReportColumns.TOTAL_PR_TO_PUSH_COUNT));
			totalCollection = srCursor.getDouble(srCursor.getColumnIndex(SummaryReportColumns.TOTAL_COLLECTION));
		}
		
		totalPrCount += values.getAsInteger(SummaryReportColumns.TOTAL_PR_COUNT);
		totalPrToPushCount += values.getAsInteger(SummaryReportColumns.TOTAL_PR_TO_PUSH_COUNT);
		totalCollection += values.getAsDouble(SummaryReportColumns.TOTAL_COLLECTION);
		
		values.put(SummaryReportColumns.TOTAL_PR_COUNT, totalPrCount);
		values.put(SummaryReportColumns.TOTAL_PR_TO_PUSH_COUNT, totalPrToPushCount);
		values.put(SummaryReportColumns.TOTAL_COLLECTION, totalCollection);
		
		EcollectorDB.saveSummaryReport(contentResolver, values);
	}
	
	public static void deleteAllServices(Context context) {
		SQLiteDatabase db = EcollectorDB.getInstance(context).getDatabaseToWrite();;
		int deleteCount = db.delete(TABLE_SERVICE, "1", null);
		Log.i("EcollectorDB",deleteCount+" Service records deleted");
	}
	
	public static void deleteAllPrs(Context context) {
		SQLiteDatabase db = EcollectorDB.getInstance(context).getDatabaseToWrite();;
		int deleteCount = db.delete(TABLE_PR, "1", null);
		Log.i("EcollectorDB",deleteCount+" PR records deleted");
	}
	
	public static void deleteAllDistributions(Context context) {
		SQLiteDatabase db = EcollectorDB.getInstance(context).getDatabaseToWrite();;
		int deleteCount = db.delete(TABLE_DISTRIBUTIONS, "1", null);
		Log.i("EcollectorDB",deleteCount+" Distribution records deleted");
	}
	
	public static void deleteDevices(Context context) {
		Log.i(TAG, "Method entry deleteDevices");
		
		Log.i(TAG, "Device record dump");
		Cursor c = null;
		try {
			c = EcollectorDB.createDeviceSearchCursor(context.getContentResolver(), null);
			logCursor(c);
		} finally {
			if (c != null) c.close();
		}
		
		SQLiteDatabase db = EcollectorDB.getInstance(context).getDatabaseToWrite();;
		int deleteCount = db.delete(TABLE_DEVICE, "1", null);
		Log.i("EcollectorDB",deleteCount+" Device records deleted");
	}
	
	public static void pushPrsToCloud(final Context context, final Messinger msngr) {
		Message msg;
		if (!NetworkAvailabilityState.isNetworkAvailable(context)) {
			msg = Message.obtain();
			msg.arg1 = HandlerActions.NETWORK_UNAVAILABLE;
			msngr.sendMessage(msg);
			return;
		}
		
		msg = Message.obtain();
		msg.arg1 = HandlerActions.PUSH_PR_STARTED;
		msngr.sendMessage(msg);
		
		final String deviceIdentifier = EcollectorDB.fetchDeviceIdentifier(context);
		if (TextUtils.isEmpty(deviceIdentifier)) {
			Log.i(TAG, "Device name got null, require pairing");
			msg = Message.obtain();
			msg.arg1 = HandlerActions.DEVICE_PAIRING_REQUIRED;
			msngr.sendMessage(msg);
			return;
		}
		
		Cursor services = EcollectorDB.findPuashablePrs(context);
		if (services == null) {
			Log.i(TAG, "pushable services cursor got null");
			msg = Message.obtain();
			msg.arg1 = HandlerActions.OTHER_ERROR;
			msngr.sendMessage(msg);
			return;
		}
		
		int servicesCount = services.getCount();
		if (servicesCount == 0) {
			Log.i(TAG, "No services to push at this time");
			msg = Message.obtain();
			msg.arg1 = HandlerActions.PUSH_PR_NO_SERVICES_TO_PUSH;
			msngr.sendMessage(msg);
			return;
		} else {
			Log.i(TAG, "Services to push: "+servicesCount);
			msg = Message.obtain();
			msg.arg1 = HandlerActions.PUSH_PR_PREPARING_TO_PUSH;
			msg.obj = servicesCount;
			msngr.sendMessage(msg);
		}
		
		boolean isSuccess = false;
		try {
			EcollectorDB.prepareDevicePrList(services,
					new BatchListener<List<DevicePrUi>>() {
						int totalRecords = 0;
						public void batchComplete(List<DevicePrUi> batchItem) {
							pushPrs(batchItem, deviceIdentifier, context, msngr);
							totalRecords += batchItem.size();
							Message msg = Message.obtain();
							msg.arg1 = HandlerActions.PUSH_PR_PROGRESSING;
							msg.obj = totalRecords;
							msngr.sendMessage(msg);
						}				
					}, 50/*Batch size*/);
			isSuccess = true;
		} finally {
			EcollectorDB.prepareSummaryReportPrStats(context.getContentResolver());
			msg = Message.obtain();
			msg.arg1 = HandlerActions.PUSH_PR_COMPLETED;
			msngr.sendMessage(msg);
		}
	}
	
	private static void pushPrs(List<DevicePrUi> batchItem, String deviceIdentifier, Context context, Messinger msngr) {
		PushPrRequest pushPrRequest = new PushPrRequest();
		pushPrRequest.deviceIdentifier = deviceIdentifier;
		pushPrRequest.devicePrs = batchItem;
		PushPrsCommand ppc = new PushPrsCommand(context,msngr,pushPrRequest);
		ppc.execute();
		CrudResponse response = (CrudResponse)ppc.getResults();
		if (response == null) {
			Log.i(TAG, "CrudResponse from cloud got null");
			return;
		} else if (!response.isSuccess()) {
			Log.i(TAG, "Push Prs command failed, reason: "+response.errorMessage);
			return;
		}
		String result = response.successMessage;
		if (TextUtils.isEmpty(result)) {
			Log.i(TAG, "No success record identifers acknowledged from cloud");
			return;
		}
		String[] prIds = result.split(",");
		try {
			EcollectorDB.updatePrsAsPushComplete(prIds, 
					ServiceColumns.C_PUSH_STATE_COMPLETE, 
					context);
		} catch (Throwable t) {
			Log.i(TAG, "Failed to update pr status to push complete",t);
		}
		EcollectorDB.prepareSummaryReportPrStats(context);
	}
	
	public static Cursor createBluetoothDevicesCursor() {
		MatrixCursor mc = 
			new MatrixCursor(new String[]{"_ID","BT_DEVICE","BT_DEVICE_ADDRESS"},1);
		mc.addRow(new String[]{"-1","NONE","NONE"});
		int i = 0;
		BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		Set<BluetoothDevice> bondedDevices = bluetoothAdapter.getBondedDevices();
		for (BluetoothDevice bondedDevice : bondedDevices) {
			Log.i(TAG, "BT Device: "+bondedDevice.getName() +" @: "+bondedDevice.getAddress());
			mc.addRow(new String[]{i+"",bondedDevice.getName(),bondedDevice.getAddress()});
			i++;
		}
		return mc;
	}
	
	public static Cursor createPrintFormatsCursor() {
		MatrixCursor mc = 
			new MatrixCursor(new String[]{"_ID","FORMAT"},1);
		mc.addRow(new String[]{"1",PrintFormat.PLAIN_PAPER.toString()});
		mc.addRow(new String[]{"2",PrintFormat.PRE_PRINTED_PAPER.toString()});
		return mc;
	}
	
	public static Cursor createPrSearchCursor(String section, String distribution,
			ContentResolver contentResolver) {
		StringBuffer whereClause = new StringBuffer();
		if (distribution != null && !"-1".equalsIgnoreCase(distribution)) {
			if (whereClause.length() > 0) {
				whereClause.append(" AND ");
			}
			whereClause
				.append(PrColumns.DISTRIBUTION)
				.append(" LIKE ").append("'%").append(distribution.trim()).append("%'");
		}
		
		if (section != null && !section.equalsIgnoreCase("-1")) {
			if (whereClause.length() > 0) {
				whereClause.append(" AND ");
			}
			whereClause
				.append(ServiceColumns.SECTION)
				.append(" = ").append("'").append(section).append("'");
		}
		
		Cursor cursor = contentResolver.query(
				EcollectorPrContentProvider.CONTENT_URI,// URL
				new String[]{ 
						PrColumns._ID,
						PrColumns.ARREARS_N_DEMAND,
						PrColumns.RC_COLLECTED,
						PrColumns.ACD_COLLECTED,
						PrColumns.AGL_AMOUNT,
						PrColumns.COLLECTION_DATE,
						PrColumns.SECTION,
						PrColumns.DISTRIBUTION,
						PrColumns.SC_NO,
						PrColumns.RECEIPT_NUMBER
						}, // Projection
				whereClause.toString(), // selection 
				null, // selectionArgs
				PrColumns._SC_NO  + " ASC"  // SortOrder eg: COLLATE LOCALIZED ASC
				);
		return cursor;
	}
	
	public static Cursor createDeviceCommandSearchCursor(Context context, 
			List<String> states, String deviceCommandId) {
		StringBuffer whereClause = new StringBuffer();
		if (states != null && !states.isEmpty()) {
			StringBuffer statesAsList = new StringBuffer();
			int i = 0;
			for (String state : states) {
				statesAsList.append("'").append((state+"").trim()).append("'");
				if (i < states.size()-1) {
					statesAsList.append(",");
				}
				i++;
			}
			whereClause
			.append(DeviceCommandColumns.STATE)
			.append(" in ( ").append(statesAsList).append(" ) ");
		} 
		
		if (deviceCommandId != null && deviceCommandId.trim().length() != 0) {
			if (whereClause.length() != 0) {
				whereClause.append(" AND ");
			}
			whereClause.append(DeviceCommandColumns.COMMAND_ID).append(" = '")
				.append(deviceCommandId.trim()).append("'");
		}
		
		/*{
			whereClause
				.append(" 1 = 1 ");
		}*/
		
		EcollectorDB instance = EcollectorDB.getInstance(context);
		SQLiteDatabase db = null;
		try {
			db = instance.getDatabaseToWrite();
			Cursor cursor = db.query(EcollectorDB.TABLE_DEVICE_COMMAND, 
					new String[]{
						DeviceCommandColumns._ID,
						DeviceCommandColumns.COMMAND_ID,
						DeviceCommandColumns.STATE,
						DeviceCommandColumns.COMMAND_DATE,
						DeviceCommandColumns.COMMANDCODE,
						DeviceCommandColumns.COMMANDPARAM1,
						DeviceCommandColumns.COMMANDPARAM2,
						DeviceCommandColumns.COMMANDPARAM3,
						DeviceCommandColumns.EXECUTIONDATE,
						DeviceCommandColumns.EXECUTIONNOTES,
						DeviceCommandColumns.COMMANDTIMESTAMP
					}, // Projection 
					whereClause.toString(), // selection
					null, //selectionArgs, 
					null, null, 
					DeviceCommandColumns.COMMANDTIMESTAMP + " ASC" // Orderby clause
					);
	        return cursor;
		} finally {
			//this.dbHelper.closeWritableDb();
		}
	}
	
	public static void insertDeviceCommand(Context context, ContentValues values) {
		SQLiteDatabase db = EcollectorDB.getInstance(context).getDatabaseToWrite();
		try {
			values.put(DeviceCommandColumns.STATE, AppConfig.DEVICE_COMMAND_STATES.READY_TO_EXECUTE.toString());
			db.insert(EcollectorDB.TABLE_DEVICE_COMMAND, 
					DeviceCommandColumns._ID, 
					values);
		} finally {
			//
		}
	}
	
	public static int updateDeviceCommandState(Context context, String commandId,
			DEVICE_COMMAND_STATES commandState, String executionNotes) {
		if (commandId == null || commandId.trim().equals("") || commandState == null) {
			return -1;
		}
		
		EcollectorDB instance = EcollectorDB.getInstance(context);
		SQLiteDatabase db = null;
		ContentValues contentValues = new ContentValues();
		contentValues.put(DeviceCommandColumns.EXECUTIONNOTES, executionNotes);
		contentValues.put(DeviceCommandColumns.STATE, commandState.toString());
		try {
			db = instance.getDatabaseToWrite();
			String whereClause = DeviceCommandColumns._ID + " = " + commandId + "";
			int updateCount = db.update(
					EcollectorDB.TABLE_DEVICE_COMMAND, contentValues, whereClause, null);
			//Log.i(TAG,"update device details, count: "+updateCount);
			return updateCount;
		} finally {
			//this.dbHelper.closeWritableDb();
		}
	}
	
	private static void createDistributions(Context context) {
		ContentResolver contentResolver = context.getContentResolver();
		EcollectorDB.deleteAllDistributions(context);
		Cursor c = EcollectorDB.fetchAllDistributions(contentResolver);
		SQLiteDatabase db = EcollectorDB.getInstance(context).getDatabaseToWrite();
		try {
			if (!c.moveToFirst()) return; 
			do {
				String distribution = c.getString(c.getColumnIndex(ServiceColumns.DISTRIBUTION));
				String section = c.getString(c.getColumnIndex(ServiceColumns.SECTION));
				if (distribution != null && distribution.trim().length() != 0) {
					ContentValues values = new ContentValues();
					values.put(DistributionsColumns.DISTRIBUTION, distribution);
					values.put(DistributionsColumns.SECTION, section);
					db.insert(EcollectorDB.TABLE_DISTRIBUTIONS, ServiceColumns._ID, 
							values);
				}
			} while(c.moveToNext());
		} finally {
			if (c != null) c.close();
		}
	}
	
	
	
	public static void logCursor(Cursor c) {
		if (c == null) return;
		if (!c.moveToFirst()) {
			Log.i(TAG, "Cursor is empty");
			return;
		}
		String[] colNames = c.getColumnNames();
		do {
			StringBuffer rec = new StringBuffer();
			for(int i = 0; i < c.getColumnCount(); i++) {
				if (DeviceColumns.DEVICE_CLD_PWD.equals(colNames[i])) continue;
				rec.append(colNames[i]).append("[").append(c.getString(i)).append("]");
			}
			Log.i("TAG", rec.toString());
		} while (c.moveToNext());
	}
	
	public static Map<String,String> cursorAsMap(Cursor c) {
		if (c == null) return Collections.emptyMap();
		Map<String,String> rMap = new HashMap<String, String>();
		String[] colNames = c.getColumnNames();
		StringBuffer rec = new StringBuffer();
		for(int i = 0; i < c.getColumnCount(); i++) {
			rec.append(colNames[i]).append("[").append(c.getString(i)).append("]");
			//Log.i(TAG,"Cursor Values - name:value="+colNames[i]+":"+c.getString(i));
			rMap.put(colNames[i],c.getString(i));
		}
		return rMap;
	}
	
	public static boolean isCollectionLocked(Context context) {
		// Check if DeltaTicks are > 0
		// check if margin is > 0
		long deltaTicks = EcollectorDB.fetchExpirationDeltaTicks(context.getContentResolver());
		double collectionMargin = EcollectorDB.collectionMargin(context);
		return deltaTicks < 0 || collectionMargin < 0;
	}

	private static double collectionMargin(Context context) {
		SummaryReport sr = EcollectorDB.fetchSummaryReport(context.getContentResolver());
		return sr.getCollectionMargin(context.getContentResolver());
	}
	
	public static void executeDeviceCommands(Context context) {
		Cursor cc = null;
		try {
			List<String> stateList = new ArrayList<String>();
			stateList.add(AppConfig.DEVICE_COMMAND_STATES.READY_TO_EXECUTE.toString());
			cc = EcollectorDB.createDeviceCommandSearchCursor(context, 
					stateList, null);
			logCursor(cc);
			if (cc == null) return;
			if (!cc.moveToFirst()) return;
			do {
				ContentValues commandInfo = new ContentValues();
				commandInfo.put(DeviceCommandColumns._ID, cc.getString(cc.getColumnIndex(DeviceCommandColumns._ID.toUpperCase())));
				commandInfo.put(DeviceCommandColumns.COMMAND_ID, cc.getString(cc.getColumnIndex(DeviceCommandColumns.COMMAND_ID)));
				commandInfo.put(DeviceCommandColumns.STATE, cc.getString(cc.getColumnIndex(DeviceCommandColumns.STATE)));
				commandInfo.put(DeviceCommandColumns.COMMAND_DATE, cc.getString(cc.getColumnIndex(DeviceCommandColumns.COMMAND_DATE)));
				commandInfo.put(DeviceCommandColumns.COMMANDCODE, cc.getString(cc.getColumnIndex(DeviceCommandColumns.COMMANDCODE)));
				commandInfo.put(DeviceCommandColumns.COMMANDPARAM1, cc.getString(cc.getColumnIndex(DeviceCommandColumns.COMMANDPARAM1)));
				commandInfo.put(DeviceCommandColumns.COMMANDPARAM2, cc.getString(cc.getColumnIndex(DeviceCommandColumns.COMMANDPARAM2)));
				commandInfo.put(DeviceCommandColumns.COMMANDPARAM3, cc.getString(cc.getColumnIndex(DeviceCommandColumns.COMMANDPARAM3)));
				commandInfo.put(DeviceCommandColumns.EXECUTIONDATE, cc.getString(cc.getColumnIndex(DeviceCommandColumns.EXECUTIONDATE)));
				commandInfo.put(DeviceCommandColumns.EXECUTIONNOTES, cc.getString(cc.getColumnIndex(DeviceCommandColumns.EXECUTIONNOTES)));
				commandInfo.put(DeviceCommandColumns.COMMANDTIMESTAMP, cc.getString(cc.getColumnIndex(DeviceCommandColumns.COMMANDTIMESTAMP)));
				CommandAction commandAction = CommandFactory.createCommandFor(context, commandInfo);
				if (commandAction != null) {
					commandAction.execute();
				}
			} while (cc.moveToNext());
		} finally {
			if (cc != null) cc.close();
		}
	}

	public static void pushDeviceCommandStatesToCloud(
			Context context) {
		Cursor c = null;
		List<String> stateList = new ArrayList<String>();
		stateList.add(AppConfig.DEVICE_COMMAND_STATES.EXECUTED.toString());
		stateList.add(AppConfig.DEVICE_COMMAND_STATES.EXECUTION_FAILED.toString());
		try {
			c = EcollectorDB.createDeviceCommandSearchCursor(context, 
					stateList, null);
			if (c == null || !c.moveToFirst()) {
				Log.i(TAG,"No Device commands to push");
				return;
			}
			
			do {
				String commandId = c.getString(c.getColumnIndex(DeviceCommandColumns._ID.toUpperCase()));
				String deviceCommandId = c.getString(c.getColumnIndex(DeviceCommandColumns.COMMAND_ID));
				String commandState = c.getString(c.getColumnIndex(DeviceCommandColumns.STATE));
				String executionNotes = c.getString(c.getColumnIndex(DeviceCommandColumns.EXECUTIONNOTES));
				String commandCode = c.getString(c.getColumnIndex(DeviceCommandColumns.COMMANDCODE));
				boolean isPushSuccess = false;
				try {
					Log.i(TAG,"Pushing device command state to cloud, commandCode: "+commandCode+
							", commandId: "+commandId+
							", deviceCommandId: "+deviceCommandId);
					CloudServices.updateDeviceCommandState(deviceCommandId, commandState, executionNotes);
					isPushSuccess = true;
				} catch (Throwable t) {
					Log.e(TAG, "Error during command state push to cloud, commandCode: "+commandCode,t);
				} finally {
					if (isPushSuccess) {
						EcollectorDB.updateDeviceCommandState(context, commandId, 
								AppConfig.DEVICE_COMMAND_STATES.UPDATED_TO_CLOUD, 
								executionNotes);
					}
				}
			} while (c.moveToNext());
		} finally {
			if (c != null) c.close();
		}
	}

	public static void attemptResettingTsl(Context context) {
		try {
			long lastTslResetTime = EcollectorDB.fetchDeviceLastTslResetTime(context);
			Calendar cal = Calendar.getInstance();
			int currentDay = cal.get(Calendar.DAY_OF_MONTH);
			cal.setTimeInMillis(lastTslResetTime);
			int resetDay = cal.get(Calendar.DAY_OF_MONTH);
			boolean shouldReset = currentDay != resetDay;
			if ( shouldReset ) {
				long resetTime = Util.trimTime(System.currentTimeMillis());
				EcollectorDB.setNetxtTslNo(1, context);
				EcollectorDB.setDeviceLastTslResetTime(resetTime, context);
			}
		} catch (Throwable t) {
			Log.i(TAG,"Error executing attempt resetting tsl", t);
		}
	}
	
	public static void resetReciptNumber(Context context) {
		EcollectorDB.setNextRecieptNo(1, context);
	}
	
	public static Cursor findArchivablePrs(Context context) {
		StringBuffer whereClause = new StringBuffer();
		whereClause.append(PrColumns._ARCHIVE_STATE)
			.append(" = '").append(PrColumns.C_ARCHIVE_STATE_READY).append("'");
		
		EcollectorDB instance = EcollectorDB.getInstance(context);
		SQLiteDatabase db = instance.getDatabaseToWrite();
		Cursor cursor = db.query(EcollectorDB.TABLE_PR, 
				(String[])PrColumns.ALL_COLUMNS.toArray(new String[]{}), // Columns 
				whereClause.toString(), // selection 
				null,//selectionArgs 
				null, //groupBy 
				null, //having 
				null //orderBy
				);
		return cursor;
	}
	
	private static int updatePrsAsArchived(String[] prCodes,
			Context context) {
		if (prCodes != null && prCodes.length == 0) {
			return -1;
		}
		
		List<String> prCodesQuoted = new ArrayList<String>();
		for (String prId : prCodes) {
			if (prId != null && !prId.trim().equals("")) {
				prCodesQuoted.add("'"+prId+"'");
			}
		}
		
		if (prCodesQuoted.isEmpty()) {
			return -1;
		}
		
		ContentValues values = new ContentValues();
		values.put(PrColumns._ARCHIVE_STATE, 
					PrColumns.C_ARCHIVE_STATE_ARCHIVED);
		
		StringBuffer whereClause = prepareInClause(PrColumns._DEVICE_PR_CODE,(String[])prCodesQuoted.toArray(new String[]{}));
		SQLiteDatabase db = EcollectorDB.getInstance(context).getDatabaseToWrite();;
		return db.update(EcollectorDB.TABLE_PR, values, 
				whereClause.toString(), null);
	}

	public static void archivePrs(Context context) {
		
		if (!ExternalStorageState.isExternalStorageAvailable()) {
			Log.i(TAG,"External storage not available");
			return;
		}
		Cursor c = null;
		PrintWriter out = null;
		try {
			c = EcollectorDB.findArchivablePrs(context);
			if (c.getCount() == 0) {
				Log.i(TAG,"No PRs to archive this time");
				return;
			}
			if (!c.moveToFirst()) {
				Log.i(TAG,"No PRs to archive this time");
				return;
			}
			String archiveFileName = Util.getTodaysArchiveFileName();
			File archiveFile = new File(Util.getArchiveDir(),archiveFileName);
			Log.i(TAG, "Archiving "+c.getCount()+" PRs into "+archiveFile.getAbsolutePath());
			out = new PrintWriter(new BufferedWriter(new FileWriter(archiveFile, true)));
			int prCodeColIndex = c.getColumnIndex(PrColumns._DEVICE_PR_CODE);
			List<String> archivedPrIds = new ArrayList<String>();
			ServiceDataExporter dataExporter = new ServiceDataExporter();
			do {
				String prCode = c.getString(prCodeColIndex);
				String formattedPr = dataExporter.formattedString(EcollectorDB.cursorAsMap(c));
				try {
					formattedPr = CryptoUtil.encode(formattedPr);
				} catch (Throwable t) {
					Log.i(TAG,"Error encrypting pr",t);
				}
				out.append(formattedPr+"\n");
				archivedPrIds.add(prCode);
			} while (c.moveToNext());
			out.close();
			if (out.checkError()) {
				Log.i(TAG, "Something went wrong saving file");
			}
			EcollectorDB.updatePrsAsArchived((String[])archivedPrIds.toArray(new String[]{}), context);
			Log.i(TAG, "Archiving completed");
		} catch (IOException e) {
			Log.i(TAG,"Error archiving prs",e);
		} finally {
			if (c != null) c.close();
			if (out != null) out.close();
		}
	}

	public static void clearArchiveFiles(Context context) {
		if (!ExternalStorageState.isExternalStorageAvailable()) {
			Log.i(TAG,"External storage not available");
			return;
		}
		
		String archiveFileName = Util.getArchiveFileName(DELETE_ARCHIVE_FILES_OLDER_THAN_DAYS);
		File archiveFile = new File(Util.getArchiveDir(),archiveFileName);
		if (archiveFile.exists()) {
			Log.i(TAG,"Removing archive file to reclaim space, file Name: "+archiveFile.getAbsolutePath());
			archiveFile.delete();
		}
	}
	
	/**
	 * WARNING: Callers of this method must explicitly close the Cursor 
	 * 			returned by this method.
	 * @param contentResolver
	 * @return
	 */
	public static Cursor fetchAllServices(Context context) {
		StringBuffer whereClause = new StringBuffer();
		Cursor cursor = context.getContentResolver().query(
				EcollectorContentProvider.CONTENT_URI,// URL
				new String[]{ 
						ServiceColumns._ID,
						ServiceColumns.USC_NO,
						ServiceColumns.SC_NO,
						ServiceColumns.NAME,
						ServiceColumns.ADDRESS,
						ServiceColumns.CATEGORY,
						ServiceColumns.GROUP_X,
						ServiceColumns.ERO,
						ServiceColumns.SECTION,
						ServiceColumns.DISTRIBUTION,
						ServiceColumns.ARRIERS,
						ServiceColumns.CMD,
						ServiceColumns.ACD,
						ServiceColumns.SD_AVAILABLE,
						ServiceColumns.BILLED_MONTHS,  
						ServiceColumns.DC_DATE,
						ServiceColumns.RC_CODE,
						ServiceColumns.MACHINE_CODE,
						ServiceColumns.PEFCTDT,
						ServiceColumns.AGL_AMOUNT,
						ServiceColumns.AGL_SERVICES,
						ServiceColumns.SUB_CATEGORY,
						ServiceColumns._SC_NO,
						ServiceColumns._N_USC_NO,
						ServiceColumns._PR_IDS
						}, // Projection
				whereClause.toString(), // selection 
				null, // selectionArgs
				ServiceColumns._N_USC_NO  + " ASC"  // SortOrder eg: COLLATE LOCALIZED ASC
				);
		return cursor;
	}
	
	public static void autoGeneratePrs(Context context) {
		Cursor c = null;
		try {
			c = EcollectorDB.fetchAllServices(context);
			if (c.getCount() == 0) {
				Log.i(TAG,"No Services to Auto generate PRs");
				return;
			}
			if (!c.moveToFirst()) {
				Log.i(TAG,"No Services to auto generate PRs");
				return;
			}
			
			AutoPrGenerator apg = new AutoPrGenerator(context);
			Log.i(TAG,"Auto Generating PRs, count: "+c.getCount());
			do {
				/*ContentValues v = new ContentValues();
				String rcCode = c.getString(c.getColumnIndex(ServiceColumns.RC_CODE));				
				
				String rc_collected = Math.random()%2==0? "75" : "0";
				String acd_collected = "0";
				String others_collected = "0";
				String machine_code = EcollectorDB.fetchDeviceName(context);
				String agl_amount = "0";
				String new_arrears = "0";
				String cmd_collected = c.getString(c.getColumnIndex(ServiceColumns.CMD));
				String amount_collected = cmd_collected;
				String collection_date = Util.getCurrentDate();
				String collection_time = Util.getCurrentTime();
				String arrears_n_demand = c.getString(c.getColumnIndex(ServiceColumns.CMD));
				
				String receipt_number = EcollectorDB.getNextRecieptNo(context)+"";
				String tc_seal_no = rcCode + EcollectorDB.getNextTslNo(context);
				v.put(PrColumns.RECEIPT_NUMBER, receipt_number);
				v.put(PrColumns.USC_NO,c.getString(c.getColumnIndex(ServiceColumns.USC_NO)));
				v.put(PrColumns.SC_NO,c.getString(c.getColumnIndex(ServiceColumns.SC_NO)));
				v.put(PrColumns.ERO,c.getString(c.getColumnIndex(ServiceColumns.ERO)));
				v.put(PrColumns.SECTION,c.getString(c.getColumnIndex(ServiceColumns.SECTION)));
				v.put(PrColumns.DISTRIBUTION,c.getString(c.getColumnIndex(ServiceColumns.DISTRIBUTION)));
				v.put(PrColumns.COLLECTION_DATE,collection_date);
				v.put(PrColumns.COLLECTION_TIME,collection_time);
				v.put(PrColumns.ARREARS_N_DEMAND,arrears_n_demand);
				v.put(PrColumns.RC_COLLECTED,rc_collected);
				v.put(PrColumns.ACD_COLLECTED,acd_collected);
				v.put(PrColumns.OTHERS_COLLECTED,others_collected);
				v.put(PrColumns.RC_CODE,rcCode);
				v.put(PrColumns.MACHINE_CODE,machine_code);
				v.put(PrColumns.LAST_PAID_RECEIPT_NO,"");
				v.put(PrColumns.LAST_PAID_DATE,"");
				v.put(PrColumns.LAST_PAID_AMOUNT,"");
				v.put(PrColumns.TC_SEAL_NO,tc_seal_no);
				v.put(PrColumns.REMARKS,"");
				v.put(PrColumns.REMARKS_AMOUNT,"");
				v.put(PrColumns.AGL_AMOUNT,agl_amount);
				v.put(PrColumns.AGL_SERVICES,"");
				v.put(PrColumns.NEW_ARREARS, new_arrears);
				v.put(PrColumns.CMD_COLLECTED, cmd_collected);
				v.put(PrColumns.COLLECTION_DELTA, "");
				v.put(PrColumns.AMOUNT_COLLECTED, amount_collected);
				
				String devicePrCode = 
					EcollectorDB.fetchDeviceName(context) +
					Util.getCurrentTimestamp()+receipt_number;
				v.put(PrColumns._DEVICE_PR_CODE, devicePrCode);
				
				EcollectorDB.savePrDetails(context.getContentResolver(),v);*/
				
				long serviceId = c.getLong(c.getColumnIndex(ServiceColumns._ID));
				apg.generatePr(serviceId);
				
				Log.i(TAG,"Generated a PR");
				try {
					Thread.sleep(1000*1); // Sleep for one second
				} catch(Exception e) {}
				
			} while (c.moveToNext());
			Log.i(TAG, "Auto generation of PRs complete");
		} catch (Exception e) {
			Log.i(TAG,"Error while Auto generating PRs",e);
		} finally {
			if (c != null) c.close();
		}
	}
}