package ecollector.device.db;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.content.ContentResolver;
import android.database.Cursor;
import android.database.MatrixCursor;
import ecollector.common.PrColumns;
import ecollector.common.ReportAbstractColumns;
import ecollector.device.PrintUtil;
import ecollector.device.ReportFilterBean;
import ecollector.device.Util;

public class PrHeadsReport {
	
	private ReportFilterBean mReportFilter;
	private ContentResolver mContentResolver;
	
	private MatrixCursor mReportCursor;
	public PrHeadsReport(ReportFilterBean reportFilter,
			ContentResolver contentResolver) {
		mReportFilter = reportFilter;
		mContentResolver = contentResolver;
	}
	
	private void initReport() {
		Cursor cursor = EcollectorDB.createPrSearchCursor(
				mReportFilter.getSection(), mReportFilter.getDistribution(), 
				mContentResolver);
		prepareReport(cursor);
	}
	
	private void prepareReport(Cursor cursor) {
		Date fromDate = mReportFilter.getFromDate();
		Date toDate = mReportFilter.getToDate();
		
		mReportCursor = new MatrixCursor(
				new String[]{
						PrColumns.SC_NO,
						PrColumns.ARREARS_N_DEMAND,
						PrColumns.RC_COLLECTED,
						PrColumns.ACD_COLLECTED,
						PrColumns.AGL_AMOUNT,
						PrColumns.SECTION,
						PrColumns.DISTRIBUTION
						});
		if (!cursor.moveToFirst()) {
			return;
		}
		
		int scNodColIndex = cursor.getColumnIndex(PrColumns.SC_NO);
		int arrearsNDemandColIndex = cursor.getColumnIndex(PrColumns.ARREARS_N_DEMAND);
		int rcCollectedColIndex = cursor.getColumnIndex(PrColumns.RC_COLLECTED);
		int acdCollectedColIndex = cursor.getColumnIndex(PrColumns.ACD_COLLECTED);
		int aglAmountColIndex = cursor.getColumnIndex(PrColumns.AGL_AMOUNT);
		int collectionDateColIndex = cursor.getColumnIndex(PrColumns.COLLECTION_DATE);
		int sectionColIndex = cursor.getColumnIndex(PrColumns.SECTION);
		int distColIndex = cursor.getColumnIndex(PrColumns.DISTRIBUTION);
		
		Map<String, List<String[]>> sectionReportMap = new HashMap<String, List<String[]>>(); 
		do{
			String collectionDate = cursor.getString(collectionDateColIndex);
			Date collDate = Util.getDate(collectionDate);
			if (collDate == null) continue;
			if (fromDate != null && fromDate.after(collDate)) {
				continue;
			}
			if (toDate != null && toDate.before(collDate)) {
				continue;
			}
			
			String sectionName = cursor.getString(sectionColIndex);
			List<String[]> sectionReport = sectionReportMap.get(sectionName);
			if (sectionReport == null) {
				sectionReport = new ArrayList<String[]>();
				sectionReportMap.put(sectionName, sectionReport);
			}
			
			String scNo = cursor.getString(scNodColIndex);
			String[] reportItem = new String[]{
					scNo,
					cursor.getString(arrearsNDemandColIndex),
					cursor.getString(rcCollectedColIndex),
					cursor.getString(acdCollectedColIndex),
					cursor.getString(aglAmountColIndex),
					cursor.getString(sectionColIndex),
					cursor.getString(distColIndex)
				};
			sectionReport.add(reportItem);
		} while (cursor.moveToNext());
		
		cursor.close();
		
		for (Entry<String, List<String[]>> secReportEntry : sectionReportMap.entrySet()) {
			List<String[]> secReport = secReportEntry.getValue();
			// SORT ON DISTRIBUTION, SC_NO
			Collections.sort(secReport, new Comparator<String[]>() {
				public int compare(String[] o1, String[] o2) {
					if (o1.length != 7 || o2.length != 7) {
						return 0;
					}
					int sectionIndex = 6;
					if (o1[sectionIndex] == null) o1[sectionIndex]="";
					int secComparision = o1[sectionIndex].compareToIgnoreCase(o2[sectionIndex]);
					if (secComparision == 0) {
						// compare sc number
						int scNoIndex = 0;
						if (o1[scNoIndex] == null) o1[scNoIndex] = "";
						try {
							int o1ScNo = Integer.parseInt(o1[scNoIndex]);
							int o2ScNo = Integer.parseInt(o2[scNoIndex]);
							return o1ScNo - o2ScNo;
						} catch (Exception e) {}
						return 0;
					} else {
						return secComparision;
					}
				}
			});
			for (String[] columnValues : secReport) {
				mReportCursor.addRow(columnValues);
			}
		}
	}
	
	public Cursor getReportCursor() {
		if (mReportCursor == null) {
			initReport();
		}
		return mReportCursor;
	}
	
	boolean isUsed = false;
	public boolean hasNext() {
		getReportCursor();
		if (!isUsed) {
			isUsed = true;
			return mReportCursor.moveToFirst(); 
		} else {
			return mReportCursor.moveToNext();
		}
	}
	
	public void resetCursorPosition() {
		isUsed = false;
	}
	
	public MatrixCursor next() {
		return mReportCursor;
	}
	
	public StringBuffer getPrintBuffer() {
		//WARNING! DO NOT HAVE THIS UNCOMMENT IN PRODUCTION
		// THIS WOULD HANG THE THREAD
		//android.os.Debug.waitForDebugger();
		
		Date fromDate = mReportFilter.getFromDate();
		Date toDate = mReportFilter.getToDate();
		
		resetCursorPosition();
		
		StringBuffer s = new StringBuffer();
		
		while (this.hasNext()) {
			Cursor c = this.next();
			
			s.append(PrintUtil.prepareLineRpad("Amount Abstract Report",PrintUtil.MAX_CHARS_PER_LINE));
			s.append(PrintUtil.prepareLineCalign("APSFDCL",PrintUtil.MAX_CHARS_PER_LINE));
			
			String deviceName = "SCM: "+EcollectorDB.fetchDeviceName(mContentResolver);
			s.append(PrintUtil.prepareLineRpad(deviceName,PrintUtil.MAX_CHARS_PER_LINE));
			
			String dateRange = Util.getReadableDate(fromDate)+" - "+Util.getReadableDate(toDate);
			s.append(PrintUtil.prepareLineRpad(dateRange,PrintUtil.MAX_CHARS_PER_LINE));
			
			String sectionName = c.getString(c.getColumnIndex(ReportAbstractColumns.SECTION));
			String dist = c.getString(c.getColumnIndex(ReportAbstractColumns.DISTRIBUTION));
			
			String secTitle = "Sec: " + (sectionName==null?"N/A":sectionName);
			s.append(PrintUtil.prepareLineRpad(secTitle,PrintUtil.MAX_CHARS_PER_LINE));
			
			if (dist != null && dist.trim().length() != 0) {
				String distTitle = "Dist: "+dist;
				s.append(PrintUtil.prepareLineRpad(distTitle,PrintUtil.MAX_CHARS_PER_LINE));
			}
			
			s.append(PrintUtil.DASHED_LINE);
			
			String amt = c.getString(c.getColumnIndex(ReportAbstractColumns.ARREARS_N_DEMAND));
			String count = c.getString(c.getColumnIndex(ReportAbstractColumns.ARREARS_N_DEMAND_COUNT));
			s.append(PrintUtil.prepareLineRpad(
					PrintUtil.prepareLineRpad("CC", 5).append(
					PrintUtil.prepareLineLpad(count, 6).append(
					PrintUtil.prepareLineLpad(amt, 13)
					)).toString()
					,PrintUtil.MAX_CHARS_PER_LINE));
			
			amt = c.getString(c.getColumnIndex(ReportAbstractColumns.RC_COLLECTED));
			count = c.getString(c.getColumnIndex(ReportAbstractColumns.RC_COLLECTED_COUNT));
			s.append(PrintUtil.prepareLineRpad(
					PrintUtil.prepareLineRpad("RC", 5).append(
					PrintUtil.prepareLineLpad(count, 6).append(
					PrintUtil.prepareLineLpad(amt, 13)
					)).toString()
					,PrintUtil.MAX_CHARS_PER_LINE));
			
			amt = c.getString(c.getColumnIndex(ReportAbstractColumns.ACD_COLLECTED));
			count = c.getString(c.getColumnIndex(ReportAbstractColumns.ACD_COLLECTED_COUNT));
			s.append(PrintUtil.prepareLineRpad(
					PrintUtil.prepareLineRpad("ACD", 5).append(
					PrintUtil.prepareLineLpad(count, 6).append(
					PrintUtil.prepareLineLpad(amt, 13)
					)).toString()
					,PrintUtil.MAX_CHARS_PER_LINE));
			
			amt = c.getString(c.getColumnIndex(ReportAbstractColumns.AGL_AMOUNT));
			count = c.getString(c.getColumnIndex(ReportAbstractColumns.AGL_AMOUNT_COUNT));
			s.append(PrintUtil.prepareLineRpad(
					PrintUtil.prepareLineRpad("AGL", 5).append(
					PrintUtil.prepareLineLpad(count, 6).append(
					PrintUtil.prepareLineLpad(amt, 13)
					)).toString()
					,PrintUtil.MAX_CHARS_PER_LINE));
			
			s.append(PrintUtil.DASHED_LINE);
			
			amt = c.getString(c.getColumnIndex(ReportAbstractColumns.GRAND_TOTAL));
			count = c.getString(c.getColumnIndex(ReportAbstractColumns.TOTAL_PRS));
			s.append(PrintUtil.prepareLineRpad(
					PrintUtil.prepareLineRpad("TOTAL", 5).append(
					PrintUtil.prepareLineLpad(count, 6).append(
					PrintUtil.prepareLineLpad(amt, 13)
					)).toString()
					,PrintUtil.MAX_CHARS_PER_LINE));
			
			s.append(PrintUtil.DASHED_LINE);
			s.append(PrintUtil.BLANK_LINE);
		}
		s.append(PrintUtil.BLANK_LINE);
		return s;
	}
}