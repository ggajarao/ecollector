package ecollector.device.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.database.Cursor;
import ebilly.admin.shared.viewdef.ResultField;
import ebilly.admin.shared.viewdef.ResultRecord;
import ebilly.admin.shared.viewdef.SearchRequest;
import ebilly.admin.shared.viewdef.SearchResult;
import ecollector.common.PrColumns;
import ecollector.common.ReportAbstractColumns;
import ecollector.device.Util;

public class ReportUtil {

	public static SearchRequest createSearchRequest() {
		List<ResultField> resultFields = new ArrayList<ResultField>();
		SearchRequest request = new SearchRequest(null, resultFields);
		resultFields.add(new ResultField(ReportAbstractColumns.ARREARS_N_DEMAND,java.lang.Double.class.getName()));
		resultFields.add(new ResultField(ReportAbstractColumns.ARREARS_N_DEMAND_COUNT,java.lang.Long.class.getName()));
		resultFields.add(new ResultField(ReportAbstractColumns.RC_COLLECTED,java.lang.Double.class.getName()));
		resultFields.add(new ResultField(ReportAbstractColumns.RC_COLLECTED_COUNT,java.lang.Long.class.getName()));
		resultFields.add(new ResultField(ReportAbstractColumns.ACD_COLLECTED,java.lang.Double.class.getName()));
		resultFields.add(new ResultField(ReportAbstractColumns.ACD_COLLECTED_COUNT,java.lang.Long.class.getName()));
		resultFields.add(new ResultField(ReportAbstractColumns.AGL_AMOUNT,java.lang.Double.class.getName()));
		resultFields.add(new ResultField(ReportAbstractColumns.AGL_AMOUNT_COUNT,java.lang.Long.class.getName()));
		resultFields.add(new ResultField(ReportAbstractColumns.GRAND_TOTAL,java.lang.Double.class.getName()));
		resultFields.add(new ResultField(ReportAbstractColumns.TOTAL_PRS,java.lang.Long.class.getName()));
		resultFields.add(new ResultField(ReportAbstractColumns.SECTION,java.lang.String.class.getName()));
		resultFields.add(new ResultField(ReportAbstractColumns.DISTRIBUTION,java.lang.String.class.getName()));
		return request;
	}

	public static SearchResult createSearchResult(Cursor cursor, SearchRequest request, Date fromDate, Date toDate) {
		int arrearsNDemandColIndex = cursor.getColumnIndex(PrColumns.ARREARS_N_DEMAND);
		int rcCollectedColIndex = cursor.getColumnIndex(PrColumns.RC_COLLECTED);
		int acdCollectedColIndex = cursor.getColumnIndex(PrColumns.ACD_COLLECTED);
		int aglAmountColIndex = cursor.getColumnIndex(PrColumns.AGL_AMOUNT);
		int collectionDateColIndex = cursor.getColumnIndex(PrColumns.COLLECTION_DATE); 
		int sectionColIndex = cursor.getColumnIndex(PrColumns.SECTION);
		int distributionColIndex = cursor.getColumnIndex(PrColumns.DISTRIBUTION);
		
		List<ResultRecord> resultRecords = new ArrayList<ResultRecord>();
		SearchResult result = new SearchResult(resultRecords);
		List<ResultField> rfList = request.getResultFields();
		do{
			String collectionDate = cursor.getString(collectionDateColIndex);
			Date collDate = Util.getDate(collectionDate);
			if (collDate == null) continue;
			if (fromDate != null && fromDate.after(collDate)) {
				continue;
			}
			if (toDate != null && toDate.before(collDate)) {
				continue;
			}
			
			ResultRecord resultRec = new ResultRecord();
			resultRec.fieldValues.add(cursor.getString(arrearsNDemandColIndex));
			resultRec.fieldValues.add("0"); //ARREARS_N_DEMAND_COUNT
			resultRec.fieldValues.add(cursor.getString(rcCollectedColIndex));
			resultRec.fieldValues.add("0"); // RC_COLLECTED_COUNT
			resultRec.fieldValues.add(cursor.getString(acdCollectedColIndex));
			resultRec.fieldValues.add("0"); //ACD_COLLECTED_COUNT
			resultRec.fieldValues.add(cursor.getString(aglAmountColIndex));
			resultRec.fieldValues.add("0"); //AGL_AMOUNT_COUNT
			resultRec.fieldValues.add("0"); //GRAND_TOTAL
			resultRec.fieldValues.add("0"); //TOTAL_PRS
			resultRec.fieldValues.add(cursor.getString(sectionColIndex));
			resultRec.fieldValues.add(cursor.getString(distributionColIndex));
			
			resultRecords.add(resultRec);
		} while (cursor.moveToNext());
		return result;
	}
}
