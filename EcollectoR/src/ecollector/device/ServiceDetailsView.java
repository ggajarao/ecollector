package ecollector.device;

import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import ecollector.device.android.framework.SimpleCursorAdapter;
import ecollector.device.db.EcollectorDB;
import ecollector.device.view.FragmentView;
import ecollector.device.view.ViewManager;
import ecollector.device.view.action.AddDemandAction;
import ecollector.device.view.action.OnlyAcdAction;
import ecollector.device.view.action.PrintDuplicatePrAction;
import ecollector.device.view.input.KeyPad;
import ecollector.device.view.input.KeyPadListener;
import ecollector.device.view.input.NavigationKeyPad;

public class ServiceDetailsView extends FragmentView implements KeyPadListener {

	private ViewManager mViewManager;

	private BillCollectionBean mBcBean;
	private Button mExcludeArrearsBtn;
	private Button mAddDemandBtn;
	
	private Cursor mExemptorCursor;
	private Spinner mExemptorSp;

	public ServiceDetailsView() {
		
	}
	
	public ServiceDetailsView(/*ViewManager viewManager, */BillCollectionBean meeterReadingBean) {
		//mViewManager = viewManager;
		mBcBean = meeterReadingBean;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mViewManager = getFragmentViewManager();
		// in case user navigated back to this screen
		// we shall begin with a freshly loaded bean details
		mBcBean.startOver();
		
		mBcBean.resetArrearsSelectedToAddFlag();
		if (mBcBean.mIsCursorEmpty) {
			//Toast.makeText((Context) mViewManager, "Service Record not found with id: "+mServiceId, Toast.LENGTH_SHORT).show();
			TextView tv = new TextView((Context)mViewManager);
			tv.setText("Service Number "+ mBcBean._id +" not found");
			return tv;
		}
		
		return prepareDetailsView(inflater);

		//prepareNewReadingView(inflater);
	}

	private View prepareDetailsView(LayoutInflater inflater) {
		View view = inflater.inflate(R.layout.service_details_view, null);

		TextView tv = (TextView)view.findViewById(R.id.uscNumber);
		tv.setText(mBcBean.usc_No);

		tv = (TextView)view.findViewById(R.id.consumerName);
		tv.setText(mBcBean.name);

		tv = (TextView)view.findViewById(R.id.eroName);
		tv.setText(mBcBean.ero);

		tv = (TextView)view.findViewById(R.id.sectionName);
		tv.setText(mBcBean.section);

		tv = (TextView)view.findViewById(R.id.distributionName);
		tv.setText(mBcBean.distribution);

		tv = (TextView)view.findViewById(R.id.serviceNumber);
		tv.setText(mBcBean.sc_No);

		tv = (TextView)view.findViewById(R.id.address);
		tv.setText(mBcBean.address);

		tv = (TextView)view.findViewById(R.id.category);
		tv.setText(mBcBean.category);

		tv = (TextView)view.findViewById(R.id.group);
		tv.setText(mBcBean.group_x);
		
		tv = (TextView)view.findViewById(R.id.billMonthsTv);
		tv.setText(Util.getReadableDateRange(mBcBean.billed_months));
		
		tv = (TextView)view.findViewById(R.id.dcDateTv);
		tv.setText(Util.getReadableDate(mBcBean.dc_date));
		
		tv = (TextView)view.findViewById(R.id.pefctdtTv);
		tv.setText(Util.getReadableDate(mBcBean.pefctdt));
		
		tv = (TextView)view.findViewById(R.id.arrearsAmtTv);
		//tv.setBackgroundDrawable(Util.getItemHighlighter(mViewManager.getActivity()));
		tv.setText(mBcBean.getArrearsSkipCondition());
		
		tv = (TextView)view.findViewById(R.id.cmdTv);
		//tv.setBackgroundDrawable(Util.getItemHighlighter(mViewManager.getActivity()));
		tv.setText(mBcBean.cmd);
		
		mExcludeArrearsBtn = (Button)view.findViewById(R.id.excludeArrearsBtn);
		mExcludeArrearsBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mBcBean.toggleArrearsSelectedToAdd();
				next();
			}
		});
		updateExcludeButtonState();
		
		/*Button onlyRcBtn = (Button)view.findViewById(R.id.onlyRcBtn);
		onlyRcBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				OnlyRcAction a = new OnlyRcAction(mBcBean._id,mViewManager);
				a.onClick(null);
			}
		});*/
		
		Button partPmtBtn = (Button)view.findViewById(R.id.partPmtBtn);
		partPmtBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				mBcBean.setLastRemark(EcollectorDB.REMARKS_PART_PAYMENT);
				Fragment v = new BillCollectionView(mViewManager,mBcBean);
				mViewManager.showView(v);
			}
		});
		
		mAddDemandBtn = (Button)view.findViewById(R.id.addDemandBtn);
		mAddDemandBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				AddDemandAction a = new AddDemandAction(mBcBean,mViewManager);
				a.onClick(null);
			}
		});
		updateAddDemandButtonState();
		
		Cursor remarksCursor = EcollectorDB.createRemarksCursor();
		Spinner remarksSp = (Spinner)view.findViewById(R.id.remarksSp);
		RemarksCursorAdapter adapter = new RemarksCursorAdapter(
				this.getActivity(), // context
				R.layout.spinner_item, // layout
				remarksCursor, // cursor
				new String[]{"_ID","REMARK","DESCRIPTION"}, // Columns for binding
				new int[] {android.R.id.text1} // Corresponding column binding View 
				);
		adapter.setViewBinder(new SimpleCursorAdapter.ViewBinder(){
			public boolean setViewValue(View view, Cursor cursor,
					int columnIndex) {
				TextView tv = (TextView)view.findViewById(android.R.id.text1);
				String id = cursor.getString(0);
				String remarksCode = cursor.getString(1);
				String description = cursor.getString(2);
				if (id == null) {
					return false;
				}
				if (id.equalsIgnoreCase("-1")) {
					tv.setText(description);
				} else {
					tv.setText(remarksCode+" "+description);
				}
				
				return true;
			}
		});
		
		remarksSp.setAdapter(adapter);
		remarksSp.setOnItemSelectedListener(new OnItemSelectedListener(){
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				Cursor cursor = (Cursor)parent.getItemAtPosition(position);
				onRemarkSelected(cursor.getString(1));// Index of remarks code, refer EcollectorDB.createRemarksCursor();
			}
			
			public void onNothingSelected(AdapterView<?> parent) {
				
			}
		});
		
		// Pre-populate remarks selection
		if (mBcBean.getLastRemark() != null) {
			remarksCursor.moveToFirst();
			int p = 0;
			do {
				String remarkCode = remarksCursor.getString(1);
				if (remarkCode != null && remarkCode.equalsIgnoreCase(
						mBcBean.getLastRemark())) {
					remarksSp.setSelection(p);
					break;
				}
				p++;
			} while (remarksCursor.moveToNext());
		}
		
		mExemptorCursor = EcollectorDB.createExemptorCursor();
		mExemptorSp = (Spinner)view.findViewById(R.id.exemptorSp);
		RemarksCursorAdapter exemptorAdapter = new RemarksCursorAdapter(
				this.getActivity(), // context
				R.layout.spinner_item, // layout
				mExemptorCursor, // cursor
				new String[]{"_ID","EXEMPTOR","LABEL"}, // Columns for binding
				new int[] {android.R.id.text1} // Corresponding column binding View 
				);
		exemptorAdapter.setViewBinder(new SimpleCursorAdapter.ViewBinder(){
			public boolean setViewValue(View view, Cursor cursor,
					int columnIndex) {
				TextView tv = (TextView)view.findViewById(android.R.id.text1);
				String id = cursor.getString(0);
				String exemptorCode = cursor.getString(1);
				String exemptorLabel = cursor.getString(2);
				if (id == null) {
					return false;
				}
				if (id.equalsIgnoreCase("-1")) {
					tv.setText(exemptorLabel);
				} else {
					tv.setText(exemptorCode+" "+exemptorLabel);
				}
				
				return true;
			}
		});
		
		mExemptorSp.setAdapter(exemptorAdapter);
		mExemptorSp.setOnItemSelectedListener(new OnItemSelectedListener(){
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				Cursor cursor = (Cursor)parent.getItemAtPosition(position);
				onExemptorSelected(cursor.getString(1));// Index of remarks code, refer EcollectorDB.createRemarksCursor();
			}
			
			public void onNothingSelected(AdapterView<?> parent) {
				
			}
		});
		
		populateExempterSpinner();
		
		// Pr details
		LinearLayout prListPnl = (LinearLayout)view.findViewById(R.id.prListPnl);
		List<PrBean> prList = mBcBean.fetchPrBeans(mBcBean.getPrIds());
		if (prList.isEmpty()) {
			prListPnl.setVisibility(View.GONE);
		} else {
			for (final PrBean prBean : prList) {
				View v = inflater.inflate(R.layout.pr_list_item, null);
				TextView printReciptNo = (TextView)v.findViewById(R.id.text1);
				Button printDuplicatePrBtn = (Button)v.findViewById(R.id.printDuplicatePrBtn);
				
				printReciptNo.setText(prBean.receipt_number);
				printDuplicatePrBtn.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						PrintDuplicatePrAction a = 
							new PrintDuplicatePrAction(
									ServiceDetailsView.this.mBcBean._id,
									ServiceDetailsView.this.mViewManager,
									prBean.receipt_number);
						a.onClick(null);
					}
				});
				prListPnl.addView(v);
			}
		}
		
		NavigationKeyPad nkp = new NavigationKeyPad();
		mViewManager.showKeyPad(nkp);
		
		return view;
	}
	
	private void onRemarkSelected(String remarkCode) {
		if (mBcBean.isForOnlyRc()) {
			//highlightReadonly(mCmdAmountText);
			if (remarkCode != null && !remarkCode.equalsIgnoreCase("NONE")) {
				makeAeAsDefaultExemptor();	
			}
			//mRcAmountSp.setEnabled(false);
			//resetTextFields();
			return;
		}
		
		if (remarkCode != null && !remarkCode.equalsIgnoreCase("NONE")) {
			mBcBean.setLastRemark(remarkCode);
			//registerEventHandlers(mCmdAmountText, F_CMD_AMOUNT);
			//registerEventHandlers(mRcAmountText, F_RC_AMOUNT);
			//mRcAmountSp.setEnabled(true);
			
			makeAeAsDefaultExemptor();
			
			// Remarks selection should make RC amount zero
			mBcBean.mVolatile_rc_collected.setLength(0);
			mBcBean.mVolatile_rc_collected.append("0");
			//populateRcAmountSpinner();
			
			//unHighlightAll();
		} else {
			mBcBean.setLastRemark("");
			//highlightReadonly(mRcAmountText);
			//highlightReadonly(mCmdAmountText);
			mBcBean.exemptor = "";
			mExemptorSp.setSelection(0);
			//resetTextFields();
			//mRcAmountSp.setEnabled(false);
		}
		//updateDependentViews();
	}
	
	private void makeAeAsDefaultExemptor() {
		// Make AE as default exemptor
		if (TextUtils.isEmpty(mBcBean.exemptor)) {
			mBcBean.exemptor = "2"; //AE EXEMPTER code
			populateExempterSpinner();
		}
	}
	
	private void populateExempterSpinner() {
		// Pre-populate remarks selection
		if (mBcBean.exemptor != null) {
			mExemptorCursor.moveToFirst();
			int p = 0;
			do {
				String exemptorCode = mExemptorCursor.getString(1);
				if (exemptorCode != null && exemptorCode.equalsIgnoreCase(
						mBcBean.exemptor)) {
					mExemptorSp.setSelection(p);
					break;
				}
				p++;
			} while (mExemptorCursor.moveToNext());
		}
	}
	
	private void onExemptorSelected(String exemptorCode) {
		if (exemptorCode != null && !exemptorCode.equalsIgnoreCase("NONE")) {
			mBcBean.exemptor = exemptorCode;
		} else {
			mBcBean.exemptor = "";
		}
	}
	
	private void updateExcludeButtonState() {
		
		if (mBcBean.isArrearsSelectedToAdd()) {
			mExcludeArrearsBtn.setText("Don't Add");
			mExcludeArrearsBtn.setBackgroundDrawable(Util.getRedButtonBackground(mViewManager.getActivity()));
		} else {
			mExcludeArrearsBtn.setText("Add");
			mExcludeArrearsBtn.setBackgroundDrawable(Util.getGreenButtonBackground(mViewManager.getActivity()));
		}
		
		int iArrears = (int)Util.asDouble(mBcBean.getArrearsSkipCondition());
		if (iArrears < 0) {
			mExcludeArrearsBtn.setVisibility(View.VISIBLE);
		} else {
			mExcludeArrearsBtn.setVisibility(View.GONE);
		}
	}
	
	private void updateAddDemandButtonState() {
		if (mBcBean.isDemandAdded()) {
			mAddDemandBtn.setVisibility(View.GONE);
		} else {
			mAddDemandBtn.setVisibility(View.VISIBLE);
		}
	}

	public void onKeyClick(char key) {

		switch(key) {
		case KeyPad.KEY_PREVIOUS:
			mViewManager.showPrevious();
			break;
		case KeyPad.KEY_NEXT:
			next();
			break;
		default: break;
		}
	}
	
	private void next() {
		String remarks = mBcBean.getLastRemark();
		Fragment v = new BillCollectionView(mViewManager,mBcBean);
		if (EcollectorDB.REMARKS_EXCLUDING_ARREAR.equals(remarks)) {
			v = new BillCollectionExcludingArrearView(/*mViewManager,*/mBcBean);
		} else if (EcollectorDB.REMARKS_EXCLUDING_ACD.equals(remarks)) {
			//v = new BillCollectionExcludingAcdView(mViewManager, mBcBean);
			mBcBean.excludeAcd(mBcBean.acd+"");
		} else if (EcollectorDB.REMARKS_EXCLUDING_DEMAND.equals(remarks)) {
			mBcBean.excludeDemand();
		} else if (EcollectorDB.REMARKS_ACD_COLLECTED.equals(remarks)) {
			OnlyAcdAction a = new OnlyAcdAction(mBcBean._id,mViewManager);
			a.onClick(null);
			return;
		}
		mViewManager.showView(v);
	}
}