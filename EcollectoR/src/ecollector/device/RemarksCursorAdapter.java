package ecollector.device;

import android.content.Context;
import android.database.Cursor;
import ecollector.device.android.framework.SimpleCursorAdapter;

public class RemarksCursorAdapter extends SimpleCursorAdapter {
	public RemarksCursorAdapter(Context context, int layout, Cursor c,
			String[] from, int[] to) {
		super(context, layout, c, from, to);
	}
	
	public CharSequence convertToString(Cursor cursor) {
		int colCount = cursor.getColumnCount();
		if (colCount >= 2) {
			return cursor.getString(1)+" - "+cursor.getString(2);
		}
		return "N/A";
	}
}