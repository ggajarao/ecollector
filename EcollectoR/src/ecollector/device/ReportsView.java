package ecollector.device;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import ecollector.device.view.FragmentView;
import ecollector.device.view.ViewManager;
import ecollector.device.view.action.ShowEcollector;
import ecollector.device.view.input.KeyPad;
import ecollector.device.view.input.KeyPadListener;
import ecollector.device.view.input.NavigationKeyPad;

public class ReportsView extends FragmentView implements KeyPadListener {
	private static final String TAG = "ReportsView";
	
	private ViewManager mViewManager;
	
	public ReportsView() {
		
	}
	
	/*public ReportsView(ViewManager viewManager) {
		mViewManager = viewManager;
	}*/
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mViewManager = getFragmentViewManager();
		LinearLayout view = (LinearLayout)inflater.inflate(R.layout.reports_view, null);
		
		Button reportBtn = (Button)view.findViewById(R.id.sectionAbstractBtn);
		reportBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				ReportView v = new ReportSectionAbstractView(/*mViewManager*/);
				mViewManager.showView(v);
			}
		});
		
		reportBtn = (Button)view.findViewById(R.id.amountAbstractBtn);
		reportBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				ReportView v = new ReportAmountAbstractView(/*mViewManager*/);
				mViewManager.showView(v);
			}
		});
		
		reportBtn = (Button)view.findViewById(R.id.detailedReportBtn);
		reportBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				ReportView v = new ReportPrHeadsView(/*mViewManager*/);
				mViewManager.showView(v);
			}
		});
		
		NavigationKeyPad n = new NavigationKeyPad("Home","Next");
		n.setNextEnabled(false);
		mViewManager.showKeyPad(n);
		return view;
	}
	
	@Override
	public void onKeyClick(char key) {
		switch(key) {
		case KeyPad.KEY_PREVIOUS:
			ShowEcollector s = new ShowEcollector((Activity)mViewManager.getActivity());
			s.onClick(null);
			break;
		}
	}
}
