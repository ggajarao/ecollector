package ecollector.device;

import android.support.v4.app.Fragment;
import ecollector.device.view.ViewManager;

public class ReportAmountAbstractView extends ReportView {

	public ReportAmountAbstractView() {
		
	}
	
	/*public ReportAmountAbstractView(ViewManager viewManager) {
		super(viewManager);
	}*/

	@Override
	protected Fragment getReportDetailsView(ReportFilterBean reportFilter) {
		return new ReportAmountAbstractDetailsView(reportFilter, mViewManager);
	}
}
