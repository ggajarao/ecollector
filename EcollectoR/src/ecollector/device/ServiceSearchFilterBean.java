package ecollector.device;

public class ServiceSearchFilterBean {
	
	public static ServiceSearchFilterBean i = new ServiceSearchFilterBean();
	
	public String sDistribution = "-1";
	public String sPollAddress = "-1";
	public String sSection = "-1";
	public String sCollectionStatus = "NONE";
	
	public ServiceSearchFilterBean() {
		
	}

	public String getDistribution() {
		return sDistribution;
	}

	public void setDistribution(String distributionCode) {
		this.sDistribution = distributionCode;
	}

	public String getSection() {
		return sSection;
	}

	public void setSection(String pollAddress) {
		this.sSection = pollAddress;
	}

	public String getCollectionStatus() {
		return sCollectionStatus;
	}

	public void setCollectionStatus(String billingStatus) {
		this.sCollectionStatus = billingStatus;
	}

	public String getPollAddress() {
		return sPollAddress;
	}

	public void setPollAddress(String sPollAddress) {
		this.sPollAddress = sPollAddress;
	}
	
	public boolean isFiltering() {
		return !sCollectionStatus.equalsIgnoreCase("NONE") ||
				!sDistribution.equalsIgnoreCase("-1") ||
				!sPollAddress.equalsIgnoreCase("-1") ||
				!sSection.equalsIgnoreCase("-1");
	}

	public boolean isFiltering(String section, String distribution) {
		return sSection.equalsIgnoreCase(section) && sDistribution.equalsIgnoreCase(distribution);
	}
}
