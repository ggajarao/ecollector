package ecollector.device;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;


public class NetworkAvailabilityState {
	
	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager cm =
			(ConnectivityManager)context.getSystemService(
					Context.CONNECTIVITY_SERVICE);

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		if (activeNetwork == null) return false;
		return activeNetwork.isConnected();
	}
	
	private static BroadcastReceiver mConnectivityChangeReceiver;
	private static IntentFilter filter;
	static {
		mConnectivityChangeReceiver = new BroadcastReceiver() {
	        @Override
	        public void onReceive(Context context, Intent intent) {
	            Log.i("ebilly.networkState", "Storage: " + intent.getData());
	            updateNetworkAvailablityState(intent);
	        }
	    };
	    
	    filter = new IntentFilter();
	    filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
	}
	public static boolean isInternetAvailable = false;
	
	private static List<NetworkAvailabilityListener> networkAvailablityListeners =
		new ArrayList<NetworkAvailabilityListener>();
	private static void updateNetworkAvailablityState(Intent intent) {
		 //TODO: ITERATE OVER LISTENERS AND INVOKE METHODS IN IT.
	}

	public static void startWatchingNetworkState(Activity activity) {
	    activity.registerReceiver(mConnectivityChangeReceiver, filter);
	}

	public static void stopWatchingNetworkState(Activity activity) {
	    activity.unregisterReceiver(mConnectivityChangeReceiver);
	}
	
	public interface NetworkAvailabilityListener {
		void networkAvailable();
	}
}