package ecollector.device;

import java.util.Calendar;

import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.TextView;
import ecollector.device.view.FragmentView;
import ecollector.device.view.ViewManager;
import ecollector.device.view.input.KeyPad;
import ecollector.device.view.input.KeyPadListener;
import ecollector.device.view.input.NumericNavKeyPad;

public abstract class BillCollectionBasePRFormView extends FragmentView implements KeyPadListener {
	protected ViewManager mViewManager;
	private TextView mMessageText;
	private TextView mOldPrNumberTv;
	private TextView mOldPrAmountTv;
	protected DatePicker mOldPrDateDp;
	
	protected StringBuffer mOldPrNumber = new StringBuffer();
	protected StringBuffer mOldPrAmount = new StringBuffer();
	
	private static final int F_OLD_PR_NUMBER = 0;
	private static final int F_OLD_PR_AMOUNT = 1;
	private static final int F_OLD_PR_DATE = 2;
	
	private StringBuffer mCurrentFieldValue;
	private int mCurrentFieldLenght;
	private TextView mCurrentTextView;
	private int mCurrentFocusField;
	
	private NumericNavKeyPad mNumericNavKp;
	
	protected BillCollectionBean mBcBean;
	public BillCollectionBasePRFormView() {
		
	}
	
	public BillCollectionBasePRFormView(/*ViewManager viewManager, */BillCollectionBean bcBean) {
		//mViewManager = viewManager;
		mBcBean = bcBean;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mViewManager = getFragmentViewManager();
		return prepareView(inflater);
	}

	private View prepareView(LayoutInflater inflater) {
		View view = inflater.inflate(R.layout.bill_collection_exclude_arrear, null);
		
		mMessageText = (TextView)view.findViewById(R.id.messageText);
		mMessageText.setText(getTitle());
		mNumericNavKp = new NumericNavKeyPad();
		mViewManager.showKeyPad(mNumericNavKp);
		
		mOldPrNumberTv = (TextView)view.findViewById(R.id.oldPrNumberTv);
		mOldPrAmountTv = (TextView)view.findViewById(R.id.oldPrAmountTv);
		mOldPrDateDp = (DatePicker)view.findViewById(R.id.oldPrDateDp);
		mOldPrDateDp.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					unHighlightAll();
					setFocus(F_OLD_PR_DATE);
				}
			}
		});
		
		Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		int monthOfYear = c.get(Calendar.MONTH);
		int dayOfMonth = c.get(Calendar.DAY_OF_MONTH);
		mOldPrDateDp.init(year, monthOfYear, dayOfMonth, 
			new OnDateChangedListener() {
				@Override
				public void onDateChanged(DatePicker datepicker, int i, int j, int k) {
					updateDependentView();
				}
			}
		);
		mOldPrDateDp.setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);
		
		updateDependentView();
		
		//highlightReadonly(mOldPrNumberTv);
		registerEventHandlers(mOldPrNumberTv,F_OLD_PR_NUMBER);
		registerEventHandlers(mOldPrAmountTv,F_OLD_PR_AMOUNT);
		
		// default focus
		final Handler h = new Handler();
		(new Thread(
			new Runnable() {
				public void run() {
					h.postDelayed(new Runnable(){
						@Override
						public void run() {
							setFocus(F_OLD_PR_NUMBER);
						}
					}, 10);
				}
			}
		)).start();
		
		updateNextButtonState();
		return view;
	}
	
	protected CharSequence getTitle() {
		return "Enter Old PR Details";
	}

	private void updateNextButtonState() {
		if (isFormValid()) {
			mNumericNavKp.setNextEnabled(true);
		} else {
			mNumericNavKp.setNextEnabled(false);
		}
	}
	
	protected abstract boolean isFormValid();
	
	private void updateDependentView() {
		setFocus(mCurrentFocusField);
		int i = Util.asInteger(mOldPrAmount.toString());
		mOldPrAmount.setLength(0);
		mOldPrAmount.append(i);
		
		i = Util.asInteger(mOldPrNumber.toString());
		mOldPrNumber.setLength(0);
		mOldPrNumber.append(i);
		
		mOldPrAmountTv.setText(mOldPrAmount.toString());
		mOldPrNumberTv.setText(mOldPrNumber.toString());
		updateNextButtonState();
	}
	
	private void registerEventHandlers(TextView tv, final int focusField) {
		tv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setFocus(focusField);
			}
		});
	}

	private void setFocus(int focusField) {
		unHighlightAll();
		mCurrentFocusField = focusField;
		switch (focusField) {
		case F_OLD_PR_NUMBER:
			focusBackground(mOldPrNumberTv);
			mCurrentFieldValue = mOldPrNumber;
			mCurrentFieldLenght = 7;
			mCurrentTextView = mOldPrNumberTv;
			break;
		case F_OLD_PR_AMOUNT:
			focusBackground(mOldPrAmountTv);
			mCurrentFieldValue = mOldPrAmount;
			mCurrentFieldLenght = 7;
			mCurrentTextView = mOldPrAmountTv;
			break;
		default:
			mCurrentFieldValue = null;
			mCurrentFieldLenght = 0;
			mCurrentTextView = null;
		}
	}
	
	private void focusBackground(View tv) {
		//tv.setBackgroundDrawable(Util.getInputFieldFocusStyle(mViewManager));
		AnimationDrawable background = 
			Util.getInputFieldFocusStyleAnimated(mViewManager);
		tv.setBackgroundDrawable(background);
		((AnimationDrawable)tv.getBackground()).start();
	}
	
	private void highlightReadonly(View tv) {
		tv.setBackgroundDrawable(Util.getItemHighlighter(mViewManager.getActivity()));
		tv.setOnClickListener(null);
	}

	private void unHighlightAll() {
		Drawable background = 
			Util.getInputFieldStyle(mViewManager);
		mOldPrNumberTv.setBackgroundDrawable(background);
		mOldPrAmountTv.setBackgroundDrawable(background);
	}
	
	protected abstract void showNext();
	
	protected abstract void showPrevious();
	
	public void onKeyClick(char key) {
		if (mCurrentFieldValue == null) return;
		switch (key) {
		case KeyPad.KEY_DEL:
			mCurrentFieldValue.setLength(0);
			break;
		case KeyPad.KEY_BACKSPACE:
			if (mCurrentFieldValue.length() > 1)
				mCurrentFieldValue.setLength(mCurrentFieldValue.length()-1);
			else {
				mCurrentFieldValue.setLength(0);
			}
			break;
		case KeyPad.KEY_PREVIOUS:
			showPrevious();
			break;
		case KeyPad.KEY_NEXT:
			showNext();
			break;
		default:
			if (mCurrentFieldValue.length() < mCurrentFieldLenght){
				mCurrentFieldValue.append(key);
			}
		}
		if (mCurrentTextView != null) {
			int v = (int)Util.asDouble(mCurrentFieldValue.toString());
			mCurrentFieldValue.setLength(0);
			mCurrentFieldValue.append(v+"");
			mCurrentTextView.setText(v+"");
		}
		
		updateDependentView();
	}	
}