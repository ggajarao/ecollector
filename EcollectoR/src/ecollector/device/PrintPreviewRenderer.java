package ecollector.device;

public interface PrintPreviewRenderer {
	Object addPreviewItem(String label, String value, String defaultValue);
}
