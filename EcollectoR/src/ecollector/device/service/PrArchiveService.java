package ecollector.device.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;
import ecollector.device.ExternalStorageState;
import ecollector.device.NetworkAvailabilityState;
import ecollector.device.db.EcollectorDB;
import ecollector.device.net.CloudServices;
import ecollector.device.view.action.Messinger;
import ecollector.device.view.action.PrPushMessageHandler;

public class PrArchiveService extends IntentService {

	private static final String TAG = "PrArchiveService";
	
	public static final int EXECUTION_INTERVAL_SECS = 10;

	public PrArchiveService() {
		super("PrArchiveService");
	}

	/**
	 * The IntentService calls this method from the default worker thread with
	 * the intent that started the service. When this method returns, IntentService
	 * stops the service, as appropriate.
	 */
	@Override
	protected void onHandleIntent(Intent intent) {
		
		//WARNING! DO NOT HAVE THIS UNCOMMENT IN PRODUCTION
		// THIS WOULD HANG THE SERVICE
		//android.os.Debug.waitForDebugger(); 
		
		try {
			boolean canReadWrite = ExternalStorageState.isExternalStorageAvailable();
			if (!canReadWrite) {
				Log.i(TAG, "External media not available");
				return;
			}
			
			EcollectorDB.clearArchiveFiles(this);
			
			EcollectorDB.archivePrs(this);
		} catch (Throwable t) {
			Log.i(TAG, "Error",t);
		}
	}
}