package ecollector.device.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;
import ecollector.device.NetworkAvailabilityState;
import ecollector.device.db.EcollectorDB;
import ecollector.device.net.CloudServices;
import ecollector.device.view.action.Messinger;
import ecollector.device.view.action.PrPushMessageHandler;

public class PrPushService extends IntentService {

	private static final String TAG = "PrPushService";
	
	public static final int PUSH_INTERVAL_SECS = 60;

	public PrPushService() {
		super("ServicePushService");
	}

	/**
	 * The IntentService calls this method from the default worker thread with
	 * the intent that started the service. When this method returns, IntentService
	 * stops the service, as appropriate.
	 */
	@Override
	protected void onHandleIntent(Intent intent) {
		
		//WARNING! DO NOT HAVE THIS UNCOMMENT IN PRODUCTION
		// THIS WOULD HANG THE SERVICE
		//android.os.Debug.waitForDebugger(); 
		
		try {
			CloudServices.init(this);
			
			boolean canConnect = NetworkAvailabilityState.isNetworkAvailable(this);
			if (!canConnect) {
				Log.i(TAG, "Network not available");
				return;
			}
			
			CloudServices.syncState();
			//Toast.makeText(this, "Sync State", Toast.LENGTH_SHORT).show();
			
			final PrPushMessageHandler h = new PrPushMessageHandler() {
				@Override
				protected void onMessage(String text) {
					Log.i(TAG, text);
				}
			};
			EcollectorDB.pushPrsToCloud(this, new Messinger() {
				public void sendMessage(Message msg) {
					h.handleMessage(msg);
				}
			});
			
			EcollectorDB.pushDeviceCommandStatesToCloud(this.getApplicationContext());
			
		} catch (Throwable t) {
			Log.i(TAG, "Error",t);
		}
	}
}