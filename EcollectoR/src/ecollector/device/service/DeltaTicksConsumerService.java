package ecollector.device.service;

import android.app.IntentService;
import android.content.Intent;
import ecollector.device.db.EcollectorDB;

public class DeltaTicksConsumerService extends IntentService {

	private static final String TAG = "DeltaTicksConsumerService";

	public static final long DELTA_TICKS_BEAT_INTERVAL_SECS = 30;
	
	public DeltaTicksConsumerService() {
		super("DeltaTicksConsumerService");
	}

	/**
	 * The IntentService calls this method from the default worker thread with
	 * the intent that started the service. When this method returns, IntentService
	 * stops the service, as appropriate.
	 */
	@Override
	protected void onHandleIntent(Intent intent) {
		//WARNING! DO NOT HAVE THIS UNCOMMENT IN PRODUCTION
		// THIS WOULD HANG THE SERVICE
		//android.os.Debug.waitForDebugger();
		EcollectorDB.attemptResettingTsl(this);
		EcollectorDB.consumeExpirationDelta(
				this, 
				DeltaTicksConsumerService.DELTA_TICKS_BEAT_INTERVAL_SECS);
		EcollectorDB.getInstance(this.getApplicationContext()).closeWritableDb();
		EcollectorDB.getInstance(this.getApplicationContext()).close();
	}
}