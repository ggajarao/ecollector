package ecollector.device.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import ecollector.device.db.EcollectorDB;
import ecollector.device.net.CloudServices;

public class CommandExecutorService extends IntentService {

	private static final String TAG = "ExecutorSvc";
	
	public static final int EXECUTION_INTERVAL_SECS = 33;

	public CommandExecutorService() {
		super("CommandExecutorService");
	}

	/**
	 * The IntentService calls this method from the default worker thread with
	 * the intent that started the service. When this method returns, IntentService
	 * stops the service, as appropriate.
	 */
	@Override
	protected void onHandleIntent(Intent intent) {
		//WARNING! DO NOT HAVE THIS UNCOMMENT IN PRODUCTION
		// THIS WOULD HANG THE SERVICE
		//android.os.Debug.waitForDebugger(); 
		
		try {
			CloudServices.init(this.getApplicationContext());
			EcollectorDB.executeDeviceCommands(this.getApplicationContext());
		} catch (Throwable t) {
			Log.i(TAG, "error",t);
		}
	}
}