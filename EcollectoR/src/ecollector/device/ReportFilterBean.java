package ecollector.device;

import java.util.Date;

public class ReportFilterBean {
	private Date mFromDate;
	private Date mToDate;
	private String mSection;
	private String mDistribution;
	
	public ReportFilterBean(Date fromDate, Date toDate, String section, String distribution) {
		mFromDate = fromDate;
		mToDate = toDate;
		mSection = section;
		mDistribution = distribution;
	}

	public Date getFromDate() {
		return mFromDate;
	}

	public void setFromDate(Date mFromDate) {
		this.mFromDate = mFromDate;
	}

	public Date getToDate() {
		return mToDate;
	}

	public void setToDate(Date mToDate) {
		this.mToDate = mToDate;
	}

	public String getSection() {
		return mSection;
	}

	public void setSection(String mSection) {
		this.mSection = mSection;
	}

	public String getDistribution() {
		return mDistribution;
	}

	public void setDistribution(String mDistribution) {
		this.mDistribution = mDistribution;
	}
	
}
