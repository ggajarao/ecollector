package ecollector.device;

import java.util.Calendar;
import java.util.Date;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import ecollector.device.view.FragmentView;
import ecollector.device.view.ViewManager;
import ecollector.device.view.input.KeyPad;
import ecollector.device.view.input.KeyPadListener;
import ecollector.device.view.input.NavigationKeyPad;

public abstract class ReportView extends FragmentView implements KeyPadListener {
	protected ViewManager mViewManager;
	private DatePicker mFromDateDp;
	private DatePicker mToDateDp;
	private TextView mMessageTv;
	
	public ReportView() {
		
	}
	
	/*public ReportView(ViewManager viewManager) {
		mViewManager = viewManager;
	}*/
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mViewManager = getFragmentViewManager();
		LinearLayout view = (LinearLayout)inflater.inflate(R.layout.report_amount_abstract_filter_view, null);
		
		mFromDateDp = (DatePicker)view.findViewById(R.id.fromDateDp);
		mToDateDp = (DatePicker)view.findViewById(R.id.toDateDp);
		mMessageTv = (TextView)view.findViewById(R.id.messageTv);
		
		Calendar c = Calendar.getInstance();
		c.add(Calendar.YEAR, -2);
		Date fromDate = c.getTime();
		c.add(Calendar.YEAR, 2);
		Date toDate = c.getTime();
		BoundedDateChangeListener mDateChangeListener = new BoundedDateChangeListener(fromDate, toDate);
		
		c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		int monthOfYear = c.get(Calendar.MONTH);
		int dayOfMonth = c.get(Calendar.DAY_OF_MONTH);
		mToDateDp.init(year, monthOfYear, dayOfMonth, mDateChangeListener);
		
		//c.add(Calendar.DAY_OF_MONTH, -1);
		year = c.get(Calendar.YEAR);
		monthOfYear = c.get(Calendar.MONTH);
		dayOfMonth = c.get(Calendar.DAY_OF_MONTH);
		mFromDateDp.init(year, monthOfYear, dayOfMonth, mDateChangeListener);
		
		NavigationKeyPad n = new NavigationKeyPad("Back","Next");
		mViewManager.showKeyPad(n);
		return view;
	}
	
	private void showMessage(String msg) {
		mMessageTv.setText(msg);
	}
	
	private void showReport() {
		Date fromDate = Util.getDateFromDp(mFromDateDp);
		Date toDate = Util.getDateFromDp(mToDateDp);
		if (fromDate.after(toDate)) {
			showMessage("From Date should be older than To Date");
			return;
		} else {
			ReportFilterBean reportFilter = new ReportFilterBean(
					fromDate, toDate, null, null);
			Fragment v = getReportDetailsView(reportFilter);
			mViewManager.showView(v);
		}
	}
	
	protected abstract Fragment getReportDetailsView(ReportFilterBean reportFilter);
	
	@Override
	public void onKeyClick(char key) {
		switch(key) {
		case KeyPad.KEY_PREVIOUS:
			mViewManager.showHome();
			break;
		case KeyPad.KEY_NEXT:
			showReport();
			break;
		} 
	}
}
