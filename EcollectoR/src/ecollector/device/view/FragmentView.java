package ecollector.device.view;

import android.support.v4.app.Fragment;
import ecollector.device.view.input.KeyPadListener;

public abstract class FragmentView extends Fragment implements KeyPadListener {
	
	protected FragmentViewManager getFragmentViewManager() {
		return ((FragmentViewManager)getActivity());
	}
}
