package ecollector.device.view.input;

public interface KeyPadListener {

	void onKeyClick(char key);

}
