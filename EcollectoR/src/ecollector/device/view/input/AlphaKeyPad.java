package ecollector.device.view.input;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import ecollector.device.R;

public class AlphaKeyPad extends KeyPad {
	
	private TextView mKeyNumericKeyPadTv;
	private boolean mNumericKeyPadVisibility = true;
	private TextView mLeftTabTv;
	private String mLeftTabLabel;
	private TextView mRightTabTv;
	private String mRightTabLabel;
	public AlphaKeyPad() {
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.alpha_key_pad, null);
		
		registerKey(R.id.keyA,NumericKeyPad.KEY_A,view);
		registerKey(R.id.keyB,NumericKeyPad.KEY_B,view);
		registerKey(R.id.keyC,NumericKeyPad.KEY_C,view);
		registerKey(R.id.keyD,NumericKeyPad.KEY_D,view);
		registerKey(R.id.keyE,NumericKeyPad.KEY_E,view);
		registerKey(R.id.keyF,NumericKeyPad.KEY_F,view);
		registerKey(R.id.keyG,NumericKeyPad.KEY_G,view);
		registerKey(R.id.keyH,NumericKeyPad.KEY_H,view);
		registerKey(R.id.keyI,NumericKeyPad.KEY_I,view);
		registerKey(R.id.keyJ,NumericKeyPad.KEY_J,view);
		registerKey(R.id.keyK,NumericKeyPad.KEY_K,view);
		registerKey(R.id.keyL,NumericKeyPad.KEY_L,view);
		registerKey(R.id.keyM,NumericKeyPad.KEY_M,view);
		registerKey(R.id.keyN,NumericKeyPad.KEY_N,view);
		registerKey(R.id.keyO,NumericKeyPad.KEY_O,view);
		registerKey(R.id.keyP,NumericKeyPad.KEY_P,view);
		registerKey(R.id.keyQ,NumericKeyPad.KEY_Q,view);
		registerKey(R.id.keyR,NumericKeyPad.KEY_R,view);
		registerKey(R.id.keyS,NumericKeyPad.KEY_S,view);
		registerKey(R.id.keyT,NumericKeyPad.KEY_T,view);
		registerKey(R.id.keyU,NumericKeyPad.KEY_U,view);
		registerKey(R.id.keyV,NumericKeyPad.KEY_V,view);
		registerKey(R.id.keyW,NumericKeyPad.KEY_W,view);
		registerKey(R.id.keyX,NumericKeyPad.KEY_X,view);
		registerKey(R.id.keyY,NumericKeyPad.KEY_Y,view);
		registerKey(R.id.keyZ,NumericKeyPad.KEY_Z,view);
		registerKey(R.id.keyAsterisk,NumericKeyPad.KEY_ASTERISK,view);
		registerKey(R.id.keyHyphen,NumericKeyPad.KEY_HYPHEN,view);
		
		
		registerKey(R.id.keySpace,NumericKeyPad.KEY_SPACE,view);
		Button b = registerKey(R.id.clearButton,NumericKeyPad.KEY_DEL,view);
		b.setTextColor(Color.RED);
		b = registerKey(R.id.deleteButton,NumericKeyPad.KEY_BACKSPACE,view);
		b.setTextColor(Color.RED);
		
		mLeftTabTv = registerKey(R.id.keyTabLeft, KeyPad.KEY_TAB_LEFT, view);
		mRightTabTv = registerKey(R.id.keyTabRight, KeyPad.KEY_TAB_RIGHT, view);
		mKeyNumericKeyPadTv = registerKey(R.id.keyNumericKeyPad, KeyPad.KEY_NUMERIC_KEY_PAD, view);
		
		//mKeyNumericKeyPadTv = (TextView)view.findViewById(R.id.keyNumericKeyPad);
		if (mNumericKeyPadVisibility) {
			mKeyNumericKeyPadTv.setVisibility(View.VISIBLE);
		} else {
			mKeyNumericKeyPadTv.setVisibility(View.INVISIBLE);
		}
		
		if (mLeftTabLabel != null) {
			mLeftTabTv.setText(mLeftTabLabel);
		}
		
		if (mRightTabLabel != null) {
			mRightTabTv.setText(mRightTabLabel);
		}
		
		return view;
	}
	
	public void setNumericKeyPadVisibility(boolean flag) {
		mNumericKeyPadVisibility = flag; 
	}
	
	public void setLeftTabLabel(String label) {
		mLeftTabLabel = label;
	}
	
	public void setRightTabLabel(String label) {
		mRightTabLabel = label;
	}
}
