package ecollector.device.view.input;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import ecollector.device.R;

public class NumericNavKeyPad extends KeyPad {
	private String mB1Name;
	private String mB2Name;
	private Button mNextButton;
	private Boolean mNextButtonEnableFlag = true;
	public NumericNavKeyPad() {
		super();
	}
	
	public NumericNavKeyPad(String b1Name, String b2Name) {
		super();
		mB1Name = b1Name;
		mB2Name = b2Name;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.numeric_nav_key_pad, null);
		
		registerKey(R.id.key0, KeyPad.KEY_0, view);
		registerKey(R.id.key1, KeyPad.KEY_1, view);
		registerKey(R.id.key2, KeyPad.KEY_2, view);
		registerKey(R.id.key3, KeyPad.KEY_3, view);
		registerKey(R.id.key4, KeyPad.KEY_4, view);
		registerKey(R.id.key5, KeyPad.KEY_5, view);
		registerKey(R.id.key6, KeyPad.KEY_6, view);
		registerKey(R.id.key7, KeyPad.KEY_7, view);
		registerKey(R.id.key8, KeyPad.KEY_8, view);
		registerKey(R.id.key9, KeyPad.KEY_9, view);
		Button b = registerKey(R.id.clearButton, KeyPad.KEY_DEL, view);
		b.setTextColor(Color.RED);
		b = registerKey(R.id.deleteButton, KeyPad.KEY_BACKSPACE, view);
		b.setTextColor(Color.RED);
		
		
		/*Navigation key pad related initializations*/
		Button mPreviousButton = super.registerKey(R.id.keyPrevious, KeyPad.KEY_PREVIOUS, view);
		if (mB1Name != null) {
			mPreviousButton.setText(mB1Name);
		}
		//this.setPreviousEnabled(mPreviousButtonEnableFlag);
		mNextButton = super.registerKey(R.id.keyNext, KeyPad.KEY_NEXT, view);
		this.setNextEnabled(mNextButtonEnableFlag);
		if (mB2Name != null) {
			mNextButton.setText(mB2Name);
		}
		/*Button previousButton = (Button)view.findViewById(R.id.keyPrevious);
		previousButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				onKeyClick(KeyPad.KEY_PREVIOUS);
			}
		});
		if (mB1Name != null) {
			previousButton.setText(mB1Name);
		}
		
		mNextButton = (Button)view.findViewById(R.id.keyNext);
		mNextButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				onKeyClick(KeyPad.KEY_NEXT);
			}
		});
		this.setNextEnabled(mNextButtonEnableFlag);
		
		if (mB2Name != null) {
			mNextButton.setText(mB2Name);
		}*/
		return view;
	}	

	public void setNextEnabled(boolean b) {
		if (mNextButton != null) {
			mNextButton.setEnabled(b);
		} else {
			mNextButtonEnableFlag = b;
		}
	}
}
