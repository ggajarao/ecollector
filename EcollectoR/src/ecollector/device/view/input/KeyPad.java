package ecollector.device.view.input;

import java.util.List;

import ecollector.device.R;
import ecollector.device.Util;

import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public abstract class KeyPad extends Fragment {
	
	public static final char KEY_DEL = 127;
	public static final char KEY_BACKSPACE = 8;
	public static final char KEY_0 = 48;
	public static final char KEY_1 = 49;
	public static final char KEY_2 = 50;
	public static final char KEY_3 = 51;
	public static final char KEY_4 = 52;
	public static final char KEY_5 = 53;
	public static final char KEY_6 = 54;
	public static final char KEY_7 = 55;
	public static final char KEY_8 = 56;
	public static final char KEY_9 = 57;
	
	protected static final char KEY_A = 65;
	protected static final char KEY_B = 66;
	protected static final char KEY_C = 67;
	protected static final char KEY_D = 68;
	protected static final char KEY_E = 69;
	protected static final char KEY_F = 70;
	protected static final char KEY_G = 71;
	protected static final char KEY_H = 72;
	protected static final char KEY_I = 73;
	protected static final char KEY_J = 74;
	protected static final char KEY_K = 75;
	protected static final char KEY_L = 76;
	protected static final char KEY_M = 77;
	protected static final char KEY_N = 78;
	protected static final char KEY_O = 79;
	protected static final char KEY_P = 80;
	protected static final char KEY_Q = 81;
	protected static final char KEY_R = 82;
	protected static final char KEY_S = 83;
	protected static final char KEY_T = 84;
	protected static final char KEY_U = 85;
	protected static final char KEY_V = 86;
	protected static final char KEY_W = 87;
	protected static final char KEY_X = 88;
	protected static final char KEY_Y = 89;
	protected static final char KEY_Z = 90;
	
	protected static final char KEY_ASTERISK = '*';
	protected static final char KEY_HYPHEN = '-';
	
	public static final char KEY_PREVIOUS = 300;
	public static final char KEY_NEXT = 301;
	public static final char KEY_ALPHABET_KEY_PAD = 302;
	public static final char KEY_NUMERIC_KEY_PAD = 303;
	
	public static final char KEY_SPACE = ' ';
	public static final char KEY_TAB_LEFT = '<';
	public static final char KEY_TAB_RIGHT = '>';
	

	protected List<KeyPadListener> mKeyPadListeners;
	public void setKeyPadListeners(List<KeyPadListener> keyPadListeners) {
		mKeyPadListeners = keyPadListeners;
	}
	
	protected void fireOnKeyClickEvent(char key) {
		for (KeyPadListener listener : mKeyPadListeners) {
			listener.onKeyClick(key);
		}
	}
	
	protected void onKeyClick(char key) {
		fireOnKeyClickEvent(key);
	}
	
	protected Button registerKey(int resourceId, final char keyValue, View view) {
		final Button b = (Button)view.findViewById(resourceId);
		b.setBackgroundDrawable(Util.getKeyPadButtonBackground(this.getActivity()));
		//b.setTextSize(TypedValue.COMPLEX_UNIT_SP, 40);
		b.setTextColor(Color.WHITE);
		b.setShadowLayer(3, 0, 3, Color.BLACK);
		b.setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				//Util.playResponseSound(v.getContext());
				
				onKeyClick(keyValue);
				
				/*Key press animation*/
				TransitionDrawable transitionBkg = 
					Util.getKeypadButtonPressedAnimated(KeyPad.this.getActivity());
				
				// set the bounds in which this transition background
				// should render
				Rect rect = new Rect();
				b.getDrawingRect(rect);
				transitionBkg.setBounds(rect);
				
				int left = b.getPaddingLeft();
				int top = b.getPaddingTop();
				int right = b.getPaddingRight();
				int bottom = b.getPaddingBottom();
				b.setBackgroundDrawable(transitionBkg); 
				// setting background overwrites padding 
				// so re-setting paddings to original values. 
				b.setPadding(left, top, right, bottom);
				
				// Start animation
				((TransitionDrawable)b.getBackground()).startTransition(300);
			}
		});
		return b;
	}
}
