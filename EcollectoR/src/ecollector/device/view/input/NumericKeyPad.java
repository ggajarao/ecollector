package ecollector.device.view.input;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import ecollector.device.R;

public class NumericKeyPad extends KeyPad {

	public NumericKeyPad() {
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.numeric_key_pad, null);
		registerKey(R.id.key0, KeyPad.KEY_0, view);
		registerKey(R.id.key1, KeyPad.KEY_1, view);
		registerKey(R.id.key2, KeyPad.KEY_2, view);
		registerKey(R.id.key3, KeyPad.KEY_3, view);
		registerKey(R.id.key4, KeyPad.KEY_4, view);
		registerKey(R.id.key5, KeyPad.KEY_5, view);
		registerKey(R.id.key6, KeyPad.KEY_6, view);
		registerKey(R.id.key7, KeyPad.KEY_7, view);
		registerKey(R.id.key8, KeyPad.KEY_8, view);
		registerKey(R.id.key9, KeyPad.KEY_9, view);
		Button b = registerKey(R.id.clearButton, KeyPad.KEY_DEL, view);
		b.setTextColor(Color.RED);
		b = registerKey(R.id.deleteButton, KeyPad.KEY_BACKSPACE, view);
		b.setTextColor(Color.RED);
		return view;
	}
}
