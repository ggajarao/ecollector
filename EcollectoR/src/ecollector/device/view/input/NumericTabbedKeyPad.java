package ecollector.device.view.input;

import ecollector.device.R;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class NumericTabbedKeyPad extends KeyPad {
	
	private TextView mKeyAlphabetKeyPadTv;
	private boolean mAlphabetKeyPadVisibility = true;
	public NumericTabbedKeyPad() {
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.numeric_tabbed_key_pad, null);
		
		registerKey(R.id.key0, KeyPad.KEY_0, view);
		registerKey(R.id.key1, KeyPad.KEY_1, view);
		registerKey(R.id.key2, KeyPad.KEY_2, view);
		registerKey(R.id.key3, KeyPad.KEY_3, view);
		registerKey(R.id.key4, KeyPad.KEY_4, view);
		registerKey(R.id.key5, KeyPad.KEY_5, view);
		registerKey(R.id.key6, KeyPad.KEY_6, view);
		registerKey(R.id.key7, KeyPad.KEY_7, view);
		registerKey(R.id.key8, KeyPad.KEY_8, view);
		registerKey(R.id.key9, KeyPad.KEY_9, view);
		registerKey(R.id.clearButton, KeyPad.KEY_DEL, view);
		registerKey(R.id.deleteButton, KeyPad.KEY_BACKSPACE, view);
		
		registerKey(R.id.keyTabLeft, KeyPad.KEY_TAB_LEFT, view);
		registerKey(R.id.keyTabRight, KeyPad.KEY_TAB_RIGHT, view);
		registerKey(R.id.keyAlphabetKeyPad,KeyPad.KEY_ALPHABET_KEY_PAD, view);
		
		mKeyAlphabetKeyPadTv = (TextView)view.findViewById(R.id.keyAlphabetKeyPad);
		updateKeyPadStatus();
		return view;
	}
	
	private void updateKeyPadStatus() {
		
		if (mKeyAlphabetKeyPadTv == null) return;
		
		if (mAlphabetKeyPadVisibility) {
			mKeyAlphabetKeyPadTv.setEnabled(true);
		} else {
			mKeyAlphabetKeyPadTv.setEnabled(false);
		}
	}
	
	public void setAlphabetKeyPadVisibility(boolean flag) {
		mAlphabetKeyPadVisibility = flag;
		updateKeyPadStatus();
	}
}
