package ecollector.device.view.input;

import ecollector.device.R;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class NavigationKeyPad extends KeyPad {
	
	private String mB1Name;
	private String mB2Name;
	private Button mPreviousButton;
	private Boolean mPreviousButtonEnableFlag = true;
	private Button mNextButton;
	private Boolean mNextButtonEnableFlag = true;
	public NavigationKeyPad() {
		super();
	}
	
	public NavigationKeyPad(String b1Name, String b2Name) {
		super();
		mB1Name = b1Name;
		mB2Name = b2Name;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.navigation_key_pad, null);
		
		mPreviousButton = super.registerKey(R.id.keyPrevious, KeyPad.KEY_PREVIOUS, view);
		if (mB1Name != null) {
			mPreviousButton.setText(mB1Name);
		}
		this.setPreviousEnabled(mPreviousButtonEnableFlag);
		mNextButton = super.registerKey(R.id.keyNext, KeyPad.KEY_NEXT, view);
		this.setNextEnabled(mNextButtonEnableFlag);
		if (mB2Name != null) {
			mNextButton.setText(mB2Name);
		}
		return view;
	}
	
	public void setNextEnabled(boolean b) {
		if (mNextButton != null) {
			mNextButton.setEnabled(b);
			if (!b) mNextButton.setTextColor(Color.GRAY);
		} else {
			mNextButtonEnableFlag = b;
		}
	}
	
	public void setPreviousEnabled(boolean b) {
		if (mPreviousButton != null) {
			mPreviousButton.setEnabled(b);
			if (!b) mPreviousButton.setTextColor(Color.GRAY);
		} else {
			mPreviousButtonEnableFlag = b;
		}
	}
}