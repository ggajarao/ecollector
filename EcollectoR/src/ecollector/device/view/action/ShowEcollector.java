package ecollector.device.view.action;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import ecollector.device.Ecollector;

public class ShowEcollector extends AbstractAction {
	private Activity activity;
	public ShowEcollector(Activity activity) {
		this.activity = activity;
	}
	@Override
	protected void perform(View view) {
		Intent intent = new Intent(this.activity, Ecollector.class);
		this.activity.startActivity(intent);
	}
}
