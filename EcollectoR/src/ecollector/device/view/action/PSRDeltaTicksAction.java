package ecollector.device.view.action;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import ecollector.device.Ecollector;
import ecollector.device.db.EcollectorDB;
import ecollector.device.service.DeltaTicksConsumerService;

public class PSRDeltaTicksAction extends AbstractAction {
	private Activity activity;
	public PSRDeltaTicksAction(Activity activity) {
		this.activity = activity;
	}
	@Override
	protected void perform(View view) {
		
		Thread t = new Thread(new Runnable(){
			@Override
			public void run() {
				for (int i = 0; i < 4000; i++) {
					Log.i("PSRDeltaTicksAction", "iteration "+i);
					/*Intent deltaTicksConsumerService = 
						new Intent(activity, DeltaTicksConsumerService.class);
					activity.startService(deltaTicksConsumerService);*/
					EcollectorDB.consumeExpirationDelta(activity, 
							10);
					try{Thread.sleep(500);}catch(Exception e){};
				}
				Log.i("PSRDeltaTicksAction", "Completed");
			}
		});
		t.start();
	}
}
