package ecollector.device.view.action;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import ecollector.device.service.PrPushService;

public class ServicePushAction extends AbstractAction {
	private Activity activity;
	public ServicePushAction(Activity activity) {
		this.activity = activity;
	}
	@Override
	protected void perform(View view) {
		Intent intent = new Intent(this.activity, PrPushService.class);
		activity.startService(intent);
	}
}
