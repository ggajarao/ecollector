package ecollector.device.view.action;

import android.view.View;
import ecollector.device.BillCollectionBean;
import ecollector.device.BillCollectionRemarksView;
import ecollector.device.BillDetailsView;
import ecollector.device.view.ViewManager;

public class OnlyRcAction extends AbstractAction {
	
	private ViewManager mViewManager;
	private long mServiceId;
	public OnlyRcAction(long serviceId, ViewManager viewManager) {
		mViewManager = viewManager;
		mServiceId = serviceId;
	}
	
	@Override
	protected void perform(View view) {
		BillCollectionBean bcBean = new BillCollectionBean(mServiceId,mViewManager);
		bcBean.prepareForOnlyRcCollection();
		
		BillDetailsView v = new BillDetailsView(/*mViewManager,*/ bcBean);
		//BillCollectionRemarksView v = new BillCollectionRemarksView(mViewManager,bcBean);
		mViewManager.showView(v);
	}
}
