package ecollector.device.view.action;

import java.io.File;

import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.DeviceFileUi;
import ecollector.device.db.EcollectorDB;
import ecollector.device.net.DeviceFileDownloadedInDevice;

import android.content.ContentResolver;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;

public class DownloadAndImportDataAction extends AbstractAction {
	
	private DeviceFileUi mDeviceFileUi;
	private File mDownloadDir;
	private Handler mHandler;
	private ContentResolver mContentResolver;
	private Context mContext;
	private Thread mWorker;
	private DownloadFileAction mDownloadFileAction;
	private DeviceFileDownloadedInDevice mDeviceFileDownloadedInDeviceCommand;
	private ImportDataAction mImportDataAction;
	public DownloadAndImportDataAction(DeviceFileUi deviceFileUi, File downloadDir, 
			Handler handler, ContentResolver contentResolver, Context context) {
		mDeviceFileUi = deviceFileUi;
		mDownloadDir = downloadDir;
		mHandler = handler;
		mContentResolver = contentResolver;
		mContext = context;
	}
	
	@Override
	protected void perform(View view) {
		if (this.mWorker == null || !this.mWorker.isAlive()) {
			this.mWorker = new Thread() {
				public void run() {
					actionFlow();
				}
			};
			this.mWorker.start();
		}
	}
	
	private void actionFlow() {
		/*Handler downloadFileHandler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				mHandler.dispatchMessage(msg);
			}
		};*/
		mDownloadFileAction = 
			new DownloadFileAction(mContext, mDeviceFileUi.getDeviceServicesFileKey(), 
					mDownloadDir, mHandler);
		mDownloadFileAction.perform(null);
		try {
			Thread.sleep(100);
		} catch (Exception e) {}
		try {
			mDownloadFileAction.waitForDownload();
		} catch (Exception e) {}
		
		String downloadedFilePath = mDownloadFileAction.getDownloadedFilePath();
		if (TextUtils.isEmpty(downloadedFilePath)) {
			Message msg = Message.obtain();
			msg.arg1 = HandlerActions.DOWNLOADING_FAILED;
			mHandler.sendMessage(msg);
			return;
		}
		Messinger messinger = new SimpleMessinger(mHandler);
		String deviceIdentifier = EcollectorDB.fetchDeviceIdentifier(mContentResolver);
		mDeviceFileDownloadedInDeviceCommand = 
			new DeviceFileDownloadedInDevice(
					mContext, messinger, deviceIdentifier,mDeviceFileUi.getId());
		mDeviceFileDownloadedInDeviceCommand.execute();
		
		String inputMethod = mDeviceFileUi.getRemoteDeviceImportMethod();
		if (AppConfig.REMOTE_DEVICE_IMPORT_METHOD.CLEAR_EXISTING_DATA.toString()
				.equalsIgnoreCase(inputMethod)) {
			Message msg = Message.obtain();
			msg.arg1 = HandlerActions.DELETING_SERVICES_STARTED;
			mHandler.sendMessage(msg);
			EcollectorDB.deleteAllServices(mContext);
			msg = Message.obtain();
			msg.arg1 = HandlerActions.DELETING_SERVICES_ENDED;
			mHandler.sendMessage(msg);
		}
		
		mImportDataAction = new ImportDataAction(mContext, mHandler, downloadedFilePath);
		mImportDataAction.perform(null);
		try {
			Thread.sleep(100);
		} catch (Exception e) {}
		
		try {
			mImportDataAction.waitForImport();
		} catch (Exception e) {}
	}
	
	public void prepareUi(Handler handler) {
		mHandler = handler;
		
		if (mDownloadFileAction != null) {
			mDownloadFileAction.prepareUi(mHandler);
		}
		if (mDeviceFileDownloadedInDeviceCommand != null) {
			mDeviceFileDownloadedInDeviceCommand.prepareUi(mHandler);
		}
		if (mImportDataAction != null) {
			mImportDataAction.prepareUi(mHandler);
		}
	}
}
