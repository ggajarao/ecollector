package ecollector.device.view.action;

import android.view.View;
import ecollector.device.BillCollectionAddDemandView;
import ecollector.device.BillCollectionBean;
import ecollector.device.view.ViewManager;

public class AddDemandAction extends AbstractAction {
	
	private ViewManager mViewManager;
	private BillCollectionBean mBcBean;
	public AddDemandAction(BillCollectionBean bcBean, ViewManager viewManager) {
		mViewManager = viewManager;
		mBcBean = bcBean;
	}
	
	@Override
	protected void perform(View view) {
		BillCollectionAddDemandView v = new BillCollectionAddDemandView(/*mViewManager, */mBcBean);
		mViewManager.showView(v);
	}
}
