package ecollector.device.view.action;

import android.os.Message;

public class PrPushMessageHandler extends MessageHandler {
	
	public Integer mTotalPrsToPush;
	public boolean mPrepareUi;
	public PrPushMessageHandler() {
		super();
	}
	
	protected String onMessage(Message msg) {
		String notifyText = "";
    	mPrepareUi = false;
    	switch (msg.arg1) {
    	case HandlerActions.PUSH_PR_STARTED:
    		notifyText = "Please wait...";
    		break;
    	case HandlerActions.DEVICE_PAIRING_REQUIRED:
    		notifyText = "Device is not paired !";
    		break;
    	case HandlerActions.PUSH_PR_NO_SERVICES_TO_PUSH:
    		notifyText = "Nothing to push";
    		break;
    	case HandlerActions.PUSH_PR_PREPARING_TO_PUSH:
    		mTotalPrsToPush = (Integer)msg.obj;
    		notifyText = "Preparing to push..."+mTotalPrsToPush+" PRs";
    		break;
    	case HandlerActions.PUSH_PR_PROGRESSING:
    		notifyText = ((Integer)msg.obj) + " PRs pushed out of "+mTotalPrsToPush;
    		break;
    	case HandlerActions.PUSH_PR_COMPLETED:
    		//notifyText = "Completed Pushing PRs";
    		mPrepareUi = true;
    		break;
    	case HandlerActions.NETWORK_UNAVAILABLE:
    		notifyText = "Internet not available";
    		mPrepareUi = true;
    		break;
    	case HandlerActions.NETWORK_ERROR:
    		notifyText = "Network error, "+(msg.obj==null?"":msg.obj.toString());
    		break;
    	case HandlerActions.OTHER_ERROR:
    		notifyText = msg.obj+". Failed, please try again";
    		mPrepareUi = true;
    		break;
    	}
    	return notifyText;
	}
	
	protected void onMessage(String text) {
		
	}
}
