package ecollector.device.view.action;

import android.view.View;
import android.view.View.OnClickListener;

public abstract class AbstractAction implements OnClickListener {

	public void onClick(View view) {
		perform(view);
	}
	
	protected abstract void perform(View view);
}
