package ecollector.device.view.action;

import android.os.Handler;
import android.os.Message;

public abstract class MessageHandler extends Handler {
	
	public String mNotifyText;
	public MessageHandler() {
		super();
	}
	
	public void handleMessage(Message msg) {
		mNotifyText = onMessage(msg); 
		onMessage(mNotifyText);
	}
	
	protected abstract String onMessage(Message msg);
	
	protected abstract void onMessage(String text);
}
