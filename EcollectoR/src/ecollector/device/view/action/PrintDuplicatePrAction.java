package ecollector.device.view.action;

import android.view.View;
import ecollector.device.BillCollectionBean;
import ecollector.device.BillCollectionRemarksView;
import ecollector.device.BillDetailsView;
import ecollector.device.view.ViewManager;

public class PrintDuplicatePrAction extends AbstractAction {
	
	private ViewManager mViewManager;
	private long mServiceId;
	private String mPrNumber;
	public PrintDuplicatePrAction(long serviceId, ViewManager viewManager, String prNumber) {
		mViewManager = viewManager;
		mServiceId = serviceId;
		mPrNumber = prNumber;
	}
	
	@Override
	protected void perform(View view) {
		BillCollectionBean bcBean = new BillCollectionBean(mServiceId,mViewManager);
		bcBean.prepareForPrintDuplicatePr(mPrNumber);
		
		BillDetailsView v = new BillDetailsView(/*mViewManager,*/ bcBean);
		mViewManager.showView(v);
	}
}
