package ecollector.device.view.action;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import ecollector.device.ServiceSearchActivity;

public class ServiceSearchAction extends AbstractAction {
	private Activity activity;
	public ServiceSearchAction(Activity activity) {
		this.activity = activity;
	}
	@Override
	protected void perform(View view) {
		Intent intent = new Intent(this.activity, ServiceSearchActivity.class);
		this.activity.startActivity(intent);
	}
}
