package ecollector.device.view.action;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import ecollector.common.DeviceFileReader;
import ecollector.common.ServiceColumns;
import ecollector.device.ExternalStorageState;
import ecollector.device.Util;
import ecollector.device.db.EcollectorContentProvider;
import ecollector.device.db.EcollectorDB;
import ecollector.device.db.Record;

public class ImportDataAction extends AbstractAction {
	private static final String TAG = "ecollector.ImportDataAction";
	
	private static final String FILE_NAME = "er.txt";
	
	//private View mView;
	private Context mContext;
	private Handler mHandler;
	private Thread worker;
	private String mImportFilePath;
	
	public ImportDataAction(Context context,
			Handler handler, String importFilePath) {
		this.mContext = context;
		this.mImportFilePath = importFilePath;
		this.mHandler = handler;
	}
	@Override
	protected void perform(View view) {
		//this.mView = view;
		//this.mActivity.showDialog(this.mDialogId);
		if (this.worker == null || !this.worker.isAlive()) {
			this.worker = new Thread() {
				public void run() {
					importData();
				}
			};
			this.worker.start();
		}
	}
	
	public void waitForImport() throws InterruptedException {
		if (this.worker == null || this.worker.isAlive()) {
			worker.join();
		} else {
			return;
		}
	}
	
	private void importData() {
		Message msg = Message.obtain();
		msg.arg1 = HandlerActions.IMPORTING_STARTED; msg.arg2 = 0;
		mHandler.sendMessage(msg);
		// Check if external storage is mounted
		if (!ExternalStorageState.isExternalStorageAvailable) {
			showToast("Memory Card is unplugged");
			msg = Message.obtain();
			msg.arg1 = HandlerActions.IMPORTING_MEDIA_UNAVAILABLE;
			mHandler.sendMessage(msg);
			return;
		}
		File directory = Util.getDownloadDir();
		showToast("Data Directory: "+directory.getAbsolutePath());
		//showToast("Parent of Data Directory: "+directory.getParentFile().getAbsolutePath());
		
		DeviceFileReader fileReader = null;
		boolean success = false;
		try {
			/*fileReader = 
				new DeviceFileReader(directory,FILE_NAME);*/
			File file = new File(mImportFilePath);
			fileReader = 
				new DeviceFileReader(mImportFilePath);
			msg = Message.obtain();
			msg.arg1 = HandlerActions.DTERMINING_TOTAL_RECORDS; msg.arg2 = fileReader.getNumberOfLines();
			mHandler.sendMessage(msg);
			
			String[] recordValues = null; 
			List<ContentValues> contentValuesList = new ArrayList<ContentValues>();
			ContentValues cv = null;
			Record record = new Record(ServiceColumns.IMPORT_COLUMN_ORDER.toArray(new String[]{}));
			int recCounter = 0;
			long beginTime = System.currentTimeMillis();
			while ((recordValues = fileReader.nextRecord()) != null) {
				showToast(Arrays.toString(recordValues));
				cv = record.asContentValues(recordValues);
				if (cv != null) {
					// Set the record id as the ServiceNumber
					Long nUscNo = cv.getAsLong(ServiceColumns.USC_NO);
					Long nServiceNumber = cv.getAsLong(ServiceColumns.SC_NO);
					if (nUscNo == null || nServiceNumber == null) {
						Log.i(TAG,"Invalid record either USC_NO or SC_NO are invalid, USC_NO["+
								nUscNo+"], SC_NO:["+nServiceNumber+"]");
					}
					cv.put(ServiceColumns._ID, nUscNo);
					cv.put(ServiceColumns._N_USC_NO, nUscNo);
					cv.put(ServiceColumns._SC_NO, nServiceNumber);
					contentValuesList.add(cv);
				}
				
				recCounter++;
				msg = Message.obtain();
				msg.arg1 = HandlerActions.IMPORTING_RECORDS; msg.arg2 = recCounter;
				mHandler.sendMessage(msg);
				
				if ((recCounter % 50) == 0) {
					insertIntoDb(contentValuesList);
					contentValuesList.clear();
				}
			}
			insertIntoDb(contentValuesList);
			long endTime = System.currentTimeMillis();
			Log.i(TAG,"Elapsed time to complete import(milliSecs): "+(endTime-beginTime));
			success = true;
		} catch (IOException e) {
			String message = "Error while importing data, exception: "+e.getMessage();
			Log.i(TAG, message, e);
			showToast(message);
			msg = Message.obtain();
			msg.arg1 = HandlerActions.PROBLEM_READING_IMPORT_FILE;
			mHandler.sendMessage(msg);
		} finally {
			if (fileReader != null) {
				fileReader.close();
			}
			if (success) {
				msg = Message.obtain();
				msg.arg1 = HandlerActions.IMPORTING_COMPUTING_STATS;
				mHandler.sendMessage(msg);
				EcollectorDB.prepareSummaryReport(mContext);
				
				msg = Message.obtain();
				msg.arg1 = HandlerActions.IMPORTING_COMPLETED;
				mHandler.sendMessage(msg);
			}
		}
	}
	
	private void insertIntoDb(List<ContentValues> contentValuesList) {
		mContext.getContentResolver().bulkInsert(
				EcollectorContentProvider.CONTENT_URI, 
				contentValuesList.toArray(new ContentValues[]{}));
	}
	
	private void showToast(String message) {
		// Toasts in threads does not work !
		// http://stackoverflow.com/questions/3875184/cant-create-handler-inside-thread-that-has-not-called-looper-prepare
		/*Toast.makeText(
				ImportDataAction.this.view.getContext(),
				message, 
				Toast.LENGTH_LONG).show();*/
		Log.i("ebilly.ImportDataAction",message);
	}
	
	public void prepareUi(Handler handler) {
		mHandler = handler;
	}
}



/*

Batch size 100
Elapsed time to complete import(milliSecs): 38865


Batch size 50
Elapsed time to complete import(milliSecs): 38783


Batch size 10
Elapsed time to complete import(milliSecs): 42058


Batch size 1
Elapsed time to complete import(milliSecs): 46320
 
*/