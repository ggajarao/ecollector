package ecollector.device.view.action;

import ecollector.device.SetupActivity;
import android.app.Activity;
import android.content.Intent;
import android.view.View;

public class SetupAction extends AbstractAction {
	private Activity activity;
	public SetupAction(Activity activity) {
		this.activity = activity;
	}
	@Override
	protected void perform(View view) {
		Intent intent = new Intent(this.activity, SetupActivity.class);
		this.activity.startActivity(intent);
	}
}
