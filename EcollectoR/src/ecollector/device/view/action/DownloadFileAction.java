package ecollector.device.view.action;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import ebilly.net.HttpConnector;
import ecollector.common.NetConstants;
import ecollector.device.net.DownloadFileCommand;

public class DownloadFileAction extends AbstractAction {

	private Thread mWorker;
	private String mFileKey;
	private File mDownloadDir;
	private Handler mHandler;
	private String mDownloadedFilePath;
	private Context mContext;
	private DownloadFileCommand mDownloadCommand;
	public DownloadFileAction(Context context, String fileKey, File downloadDir, Handler handler) {
		mContext = context;
		mFileKey = fileKey;
		mDownloadDir = downloadDir;
		mHandler = handler;
	}
	
	protected void perform(View view) {
		if (this.mWorker == null || !this.mWorker.isAlive()) {
			this.mWorker = new Thread() {
				public void run() {
					downloadFile();
				}
			};
			this.mWorker.start();
		}
	}
	
	public void waitForDownload() throws InterruptedException {
		if (this.mWorker == null || this.mWorker.isAlive()) {
			this.mWorker.join();
		} else {
			return;
		}
	}
	
	public String getDownloadedFilePath() {
		return mDownloadedFilePath;
	}
	
	private void downloadFile() {
		mDownloadCommand = 
			new DownloadFileCommand(mContext, new SimpleMessinger(mHandler), mFileKey, mDownloadDir);
		mDownloadCommand.execute();
		mDownloadedFilePath = (String)mDownloadCommand.getResults();
	}

	public void prepareUi(Handler handler) {
		mHandler = handler;
		if (mDownloadCommand != null) {
			mDownloadCommand.prepareUi(mHandler);
		}
	}
}
