package ecollector.device.view.action;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import ecollector.device.ReportsActivity;

public class ReportsAction extends AbstractAction {
	private Activity activity;
	public ReportsAction(Activity activity) {
		this.activity = activity;
	}
	@Override
	protected void perform(View view) {
		Intent intent = new Intent(this.activity, ReportsActivity.class);
		this.activity.startActivity(intent);
	}
}
