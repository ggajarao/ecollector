package ecollector.device.view.action;

import java.util.HashMap;
import java.util.Map;

public class Workers {
	
	private static Map<String, Thread> sWorkerMap = new HashMap<String, Thread>();

	public synchronized static Thread registerWorker(String name, Thread worker) {
		if (worker == null) {
			worker = sWorkerMap.get(name);
		} else {
			sWorkerMap.put(name, worker);
		}
		return worker;
	}
}
