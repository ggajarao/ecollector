package ecollector.device.view.action;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import ecollector.device.DevicePairingActivity;

public class ShowDevicePairingActivity extends AbstractAction {
	private Activity activity;
	public ShowDevicePairingActivity(Activity activity) {
		this.activity = activity;
	}
	@Override
	protected void perform(View view) {
		Intent intent = new Intent(this.activity, DevicePairingActivity.class);
		this.activity.startActivity(intent);
	}
}
