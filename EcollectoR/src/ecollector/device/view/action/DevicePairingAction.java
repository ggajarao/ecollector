package ecollector.device.view.action;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import ebilly.net.HttpConnector;
import ecollector.common.NetConstants;
import ecollector.device.NetworkAvailabilityState;
import ecollector.device.db.EcollectorDB;
import ecollector.device.net.HttpPostStore;
import ecollector.device.net.JavaCallProxy;

public class DevicePairingAction extends AbstractAction {
	
	public static final int STARTED = 0;

	public static final int NO_NETWORK = 1;

	public static final int ALREADY_PAIRED = 2;

	public static final int PAIRING = 3;

	public static final int ALREADY_PAIRED_WITH_THIS_NAME = 4;

	public static final int FAILED_AT_SERVER = 5;

	public static final int ACKNOWLEDGE_PAIRING = 6;

	public static final int PAIRED = 7;

	public static final int FAILED = 8;

	public static final int NETWORK_ERROR = 9;
	
	public static final int INVALID_SOFT_VERSION = 10;
	
	private View mView;
	private Activity mActivity;
	private String mDeviceName;
	private Handler handler;
	private Thread worker;
	
	public DevicePairingAction(Activity activity, String deviceName, Handler handler) {
		this.mActivity = activity;
		this.handler = handler;
		this.mDeviceName = deviceName;
	}
	@Override
	protected void perform(View view) {
		this.mView = view;
		if (this.worker == null || !this.worker.isAlive()) {
			this.worker = new Thread() {
				public void run() {
					pairDevice();
				}
			};
			this.worker.start();
		}
	}
	
	private void pairDevice() {
		Message msg = Message.obtain();
		msg.arg1 = DevicePairingAction.STARTED;
		handler.sendMessage(msg);
		
		if (EcollectorDB.isDevicePaired(mActivity.getContentResolver())) {
			msg = Message.obtain();
			msg.arg1 = DevicePairingAction.ALREADY_PAIRED;
			handler.sendMessage(msg);
			return;
		}
		
		boolean canConnect = NetworkAvailabilityState.isNetworkAvailable(mActivity);
		if (!canConnect) {
			msg = Message.obtain();
			msg.arg1 = DevicePairingAction.NO_NETWORK;
			handler.sendMessage(msg);
			return;
		}
		
		try {
			msg = Message.obtain();
			msg.arg1 = DevicePairingAction.PAIRING;
			handler.sendMessage(msg);
			
			// Pair device call
			Map<String,String> postParams = new HashMap<String,String>();
			postParams.put("a", "P");
			postParams.put("1", mDeviceName);
			String response;
			try {
				HttpConnector hc = new HttpConnector();
				response = hc.connect(NetConstants.PARING_SERVICE,postParams).message;
			} catch (IOException e) {
				msg = Message.obtain();
				msg.arg1 = DevicePairingAction.NETWORK_ERROR;
				msg.obj = e.getMessage();
				handler.sendMessage(msg);
				return;
			}
			 
			if ("-2".equals(response)) {
				msg = Message.obtain();
				msg.arg1 = DevicePairingAction.ALREADY_PAIRED_WITH_THIS_NAME;
				handler.sendMessage(msg);
				return;
			} else if ("-1".equals(response)) {
				msg = Message.obtain();
				msg.arg1 = DevicePairingAction.FAILED_AT_SERVER;
				handler.sendMessage(msg);
				return;
			} else if (response != null &&
					   response.length() > 10) {
				String[] respElements = response.split("\\|");
				if (respElements.length != 3) {
					msg = Message.obtain();
					msg.arg1 = DevicePairingAction.INVALID_SOFT_VERSION;
					handler.sendMessage(msg);
					return;
				}
				String deviceIdentifier = respElements[0];
				String cloudUserName = respElements[1];
				byte[] cloudPassword = respElements[2].getBytes();
				
				msg = Message.obtain();
				msg.arg1 = DevicePairingAction.ACKNOWLEDGE_PAIRING;
				handler.sendMessage(msg);
				
				// Acknowledge Paring
				postParams = new HashMap<String,String>();
				postParams.put("a", "O");
				postParams.put("1", mDeviceName);
				postParams.put("2", deviceIdentifier);
				/*response = JavaCallProxy.call(NetConstants.PARING_SERVICE, 
								null, postParams);*/
				HttpConnector hc = new HttpConnector();
				response = hc.connect(NetConstants.PARING_SERVICE, postParams).message;
				if ("Ok".equals(response)) {
					EcollectorDB.pairDevice(
							mActivity.getApplicationContext(),
							mDeviceName,deviceIdentifier,
							cloudUserName, cloudPassword);
					msg = Message.obtain();
					msg.arg1 = DevicePairingAction.PAIRED;
					handler.sendMessage(msg);
				}
			} else {
				msg = Message.obtain();
				msg.arg1 = DevicePairingAction.FAILED;
				handler.sendMessage(msg);
			}
			
		} catch (Throwable e) {
			String message = "Error while pairing, exception: "+e.getMessage();
			Log.i("ebilly.ImportDataAction", message, e);
			showToast(message);
			msg = Message.obtain();
			msg.arg1 = DevicePairingAction.FAILED;
			handler.sendMessage(msg);
		}
	}
	
	private void showToast(String message) {
		// Toasts in threads does not work !
		// http://stackoverflow.com/questions/3875184/cant-create-handler-inside-thread-that-has-not-called-looper-prepare
		/*Toast.makeText(
				ImportDataAction.this.view.getContext(),
				message, 
				Toast.LENGTH_LONG).show();*/
		Log.i("ebilly.ImportDataAction",message);
	}
}