package ecollector.device.view.action;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

class PrinterQueue /*implements Executor*/ {
	@SuppressWarnings("rawtypes")
	//final Queue tasks = new ArrayDeque();
	//final Executor executor;
	//Runnable active;
	byte[] currentPrintBuffer;
	
	private final ScheduledExecutorService scheduler =
	     Executors.newScheduledThreadPool(1);
	
	private long lastRunTime = 0;
	
	public synchronized void print(byte[] printBuffer) {
		Runnable printCommand = createPrintCommand(printBuffer);
		int waitFor = 0;
		scheduler.schedule(printCommand, waitFor, TimeUnit.SECONDS);
	}

	private Runnable createPrintCommand(final byte[] printBuffer) {
		return new Runnable() {
			@Override
			public void run() {
				
			}
		};
	}
}