package ecollector.device.view.action;

import android.os.Handler;
import android.os.Message;

public class SimpleMessinger implements Messinger {
	private Handler mHandler;
	public SimpleMessinger(Handler handler) {
		mHandler = handler;
	}
	@Override
	public void sendMessage(Message msg) {
		mHandler.sendMessage(msg);
	}
}
