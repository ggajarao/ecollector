package ecollector.device.view.action;

public interface HandlerActions {
	
	public static final int OTHER_ERROR = -1;
	
	public static final int DTERMINING_TOTAL_RECORDS = 0;
	public static final int IMPORTING_RECORDS = 1;
	public static final int IMPORTING_COMPLETED = 2;
	public static final int IMPORTING_MEDIA_UNAVAILABLE = 3;
	public static final int PROBLEM_READING_IMPORT_FILE = 4;
	public static final int IMPORTING_COMPUTING_STATS = 5;
	
	public static final int DOWNLOADING_FILE = 6;
	public static final int DOWNLOADING_COMPLETE = 7;
	public static final int DOWNLOADING_FAILED = 8;
	
	public static final int NETWORK_UNAVAILABLE = 9;
	public static final int NETWORK_ERROR = 10;
	
	public static final int CLOUD_CHK_FOR_UPDATES = 11;
	public static final int CLOUD_CHK_FOR_UPDATES_COMPLETED = 12;
	public static final int CLOUD_CHK_FOR_UPDATES_NO_FILES = 13;
	
	public static final int IMPORTING_STARTED = 14;

	public static final int DELETING_SERVICES_STARTED = 15;
	public static final int DELETING_SERVICES_ENDED = 16;

	public static final int BUSSY = 17;

	public static final int DOWNLOADING_STARTED = 18;
	public static final int DOWNLOADING_PROGRESS = 19;

	public static final int PUSH_PR_STARTED = 20;
	public static final int DEVICE_PAIRING_REQUIRED = 21;
	public static final int PUSH_PR_NO_SERVICES_TO_PUSH = 22;
	public static final int PUSH_PR_PREPARING_TO_PUSH = 23;
	public static final int PUSH_PR_PROGRESSING = 24;
	public static final int PUSH_PR_COMPLETED = 25;
}
