package ecollector.device.view.command;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import ecollector.device.db.EcollectorDB;

public class ResetCollectionCommand extends CommandAction {
	
	public ResetCollectionCommand(Context context, ContentValues commandInfo) {
		super(context,commandInfo);
	}
	
	@Override
	protected void onBeforeExecute() {
		
	}
	
	@Override
	protected void onAfterExecute() {
		
	}

	@Override
	protected void perform() {
		Cursor pushablePrs = EcollectorDB.findPuashablePrs(mContext);
		if (pushablePrs != null && pushablePrs.getCount() == 0) {
			//super.onBeforeExecute();
			EcollectorDB.deleteAllPrs(mContext);
			EcollectorDB.resetReciptNumber(mContext);
			EcollectorDB.prepareSummaryReport(mContext);
			super.onAfterExecute();
		}
	}
}
