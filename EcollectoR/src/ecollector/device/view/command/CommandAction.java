package ecollector.device.view.command;

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;
import ebilly.admin.shared.AppConfig;
import ecollector.common.DeviceCommandColumns;
import ecollector.device.db.EcollectorDB;

public abstract class CommandAction {
	protected String TAG = "CommandAction";
	
	protected Context mContext;
	protected ContentValues mCommandInfo;
	protected String mCommandId;
	CommandAction(Context context, ContentValues commandInfo) {
		mContext = context;
		mCommandInfo = commandInfo;
		mCommandId = mCommandInfo.getAsString(DeviceCommandColumns._ID);
	}
	
	protected String getCommandValue(String key) {
		return mCommandInfo.getAsString(key);
	}
	
	public void execute() {
		String executionNotes = "";
		boolean execSuccess = false;
		try {
			onBeforeExecute();
			perform();
			onAfterExecute();
			execSuccess = true;
		} catch (Throwable t) {
			Log.e(TAG,"Error executing command action, commandCode: "+
					getCommandValue(DeviceCommandColumns.COMMANDCODE),
					t);
		} finally {
			try {
				if (!execSuccess) {
					EcollectorDB.updateDeviceCommandState(mContext, mCommandId,
							AppConfig.DEVICE_COMMAND_STATES.EXECUTION_FAILED,
							executionNotes);
				}
			} catch (Throwable t) {
				Log.e(TAG,"Error updating command state to failure, commandCode: "+
						getCommandValue(DeviceCommandColumns.COMMANDCODE),
						t);
			}
		}
	}
	
	protected void onBeforeExecute() {
		EcollectorDB.updateDeviceCommandState(mContext, mCommandId,
				AppConfig.DEVICE_COMMAND_STATES.EXECUTING,
				"");
	}
	
	protected void onAfterExecute() {
		EcollectorDB.updateDeviceCommandState(mContext, mCommandId,
				AppConfig.DEVICE_COMMAND_STATES.EXECUTED,
				"");
	}

	protected abstract void perform();
}
