package ecollector.device.view.command;

import android.content.ContentValues;
import android.content.Context;
import ebilly.admin.shared.AppConfig;
import ecollector.common.DeviceCommandColumns;

public class CommandFactory {
	public static CommandAction createCommandFor(Context context, ContentValues commandInfo) {
		CommandAction commandAction = null;
		String commandCode = commandInfo.getAsString(DeviceCommandColumns.COMMANDCODE);
		if (AppConfig.DEVICE_COMMAND.RESET_COLLECTION.toString().equals(commandCode)) {
			commandAction = new ResetCollectionCommand(context, commandInfo);
		}
		return commandAction;
	}
}
