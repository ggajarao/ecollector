package ecollector.device.view;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.ViewGroup;
import ecollector.device.R;
import ecollector.device.view.input.AlphaKeyPad;
import ecollector.device.view.input.KeyPad;
import ecollector.device.view.input.KeyPadListener;
import ecollector.device.view.input.NavigationKeyPad;
import ecollector.device.view.input.NumericKeyPad;
import ecollector.device.view.input.NumericNavKeyPad;

abstract public class FragmentViewManager extends FragmentActivity implements ViewManager {
	
	private ViewGroup mContentView;
	private ViewGroup mInputView;
	
	private Fragment mHomeView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		/*requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
		
		setContentView(R.layout.servicesearch);
		mContentView = (ViewGroup)findViewById(R.id.searchResultsView);
		mInputView = (ViewGroup)findViewById(R.id.inputView);
		this.showHome();
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}
	
	public void showHome() {
		// found in PSR testing that mBackStack is accumulating and
		// causing memory leak.
		mBackStack.clear(); 
		if (this.mHomeView == null) {
			// default
			mHomeView = getHomeView();
		}
		this.showView(mHomeView);
	}
	
	abstract protected Fragment getHomeView();
	
	public void setHome(Fragment view) {
		mHomeView = view;
	}
	
	/**
	 * Request view manager to
	 * show content exclusively by 
	 * hiding key pad.
	 */
	@Override
	public void exclusiveContent() {
		android.widget.LinearLayout.LayoutParams params = 
			(android.widget.LinearLayout.LayoutParams)mContentView.getLayoutParams();
		params.weight = 100;
		mContentView.setLayoutParams(params);
		
		params = 
			(android.widget.LinearLayout.LayoutParams)mInputView.getLayoutParams();
		params.weight = 0;
		mInputView.setLayoutParams(params);
	}
	
	public void showKeyPad(KeyPad keyPad) {
		if (keyPad == null) return;
		keyPad.setKeyPadListeners(this.keyPadListeners);
		if (keyPad instanceof NavigationKeyPad) {
			android.widget.LinearLayout.LayoutParams params = 
				(android.widget.LinearLayout.LayoutParams)mContentView.getLayoutParams();
			params.weight = 85;
			mContentView.setLayoutParams(params);
			
			params = 
				(android.widget.LinearLayout.LayoutParams)mInputView.getLayoutParams();
			params.weight = 15;
			mInputView.setLayoutParams(params);
		} else if (keyPad instanceof NumericNavKeyPad ||
				keyPad instanceof NumericKeyPad ||
				keyPad instanceof AlphaKeyPad) {
			android.widget.LinearLayout.LayoutParams params = 
				(android.widget.LinearLayout.LayoutParams)mContentView.getLayoutParams();
			params.weight = 40;
			mContentView.setLayoutParams(params);
			
			params = 
				(android.widget.LinearLayout.LayoutParams)mInputView.getLayoutParams();
			params.weight = 60;
			mInputView.setLayoutParams(params);
		} else {
			android.widget.LinearLayout.LayoutParams params = 
				(android.widget.LinearLayout.LayoutParams)mContentView.getLayoutParams();
			params.weight = 60;
			mContentView.setLayoutParams(params);
			
			params = 
				(android.widget.LinearLayout.LayoutParams)mInputView.getLayoutParams();
			params.weight = 40;
			mInputView.setLayoutParams(params);
		}
		FragmentTransaction fm = this.getSupportFragmentManager().beginTransaction();
		fm.replace(R.id.inputView, keyPad);
		fm.commit();
	}
	
	private KeyPadListener mCurrentKeyPadListener;
	private java.util.Stack<Fragment> mBackStack = new java.util.Stack<Fragment>();
	public void showView(Fragment fragment) {
		if (fragment == null) return;
		FragmentTransaction fm = this.getSupportFragmentManager().beginTransaction();
		fm.replace(R.id.searchResultsView, fragment);
		mBackStack.push(fragment);
		fm.commit();
		
		if (fragment instanceof KeyPadListener) {
			KeyPadListener kpl = (KeyPadListener)fragment;
			this.addKeyPadListener(kpl);
			if (mCurrentKeyPadListener != null) {
				this.removeKeyPadListener(mCurrentKeyPadListener);
			}
			mCurrentKeyPadListener = kpl;
		}
	}
	
	public void removeScreenFromNavigation() {
		if (!mBackStack.isEmpty()) {
			mBackStack.pop();
		}
	}
	
	public void showPrevious() {
		//Pop the current Fragment
		mBackStack.pop();
		
		if (mBackStack.isEmpty()) {
			onBackPressed();
			return;
		}
		
		Fragment fragment = mBackStack.pop();
		if (fragment != null) {
			this.showView(fragment);
		}
	}

	List<KeyPadListener> keyPadListeners = new ArrayList<KeyPadListener>();
	private void addKeyPadListener(KeyPadListener kpl) {
		this.keyPadListeners.add(kpl);
	}
	
	private void removeKeyPadListener(KeyPadListener kpl) {
		this.keyPadListeners.remove(kpl);
	}

	public FragmentActivity getActivity() {
		return this;
	}	
}