package ecollector.device.view;

import ecollector.device.view.input.KeyPad;
import android.content.Context;
import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentActivity;

public interface ViewManager {

	void showView(Fragment mrv);
	
	void removeScreenFromNavigation();
	
	void showPrevious();
	
	void showKeyPad(KeyPad kp);
	
	/**
	 * Request view manager to
	 * show content exclusively by 
	 * hiding key pad.
	 */
	void exclusiveContent();
	
	Context getActivity();
	
	void setHome(Fragment homeView);
	
	void showHome();
}
