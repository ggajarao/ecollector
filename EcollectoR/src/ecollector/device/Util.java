package ecollector.device;

import java.io.File;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Environment;
import android.os.SystemClock;
import android.util.Log;
import android.widget.DatePicker;
import ecollector.common.ServiceColumns;
import ecollector.device.autobot.AutoPrGeneratorService;
import ecollector.device.db.EcollectorDB;
import ecollector.device.service.CommandExecutorService;
import ecollector.device.service.DeltaTicksConsumerService;
import ecollector.device.service.PrArchiveService;
import ecollector.device.service.PrPushService;
import ecollector.device.view.ViewManager;

public class Util {
	
	private static final String MACHINE_TIMESTAMP = "yyMMddssSSS";
	private static final String MACHINE_DATE_FORMAT = ServiceColumns.MACHINE_DATE_FORMAT;
	private static final String READABLE_DATE_FORMAT = ServiceColumns.READABLE_DATE_FORMAT;
	private static final String MACHINE_TIME_FORMAT = ServiceColumns.MACHINE_TIME_FORMAT;
	private static final String MONTH_FORMAT = "MMM";
	private static final String YEAR_FORMAT = "yy";
	public static final String MACHINE_DATE_TIME_FORMAT = "dd/MM/yyyy hh:mm:ss a z";
	
	public static String getCurrentDate() {
		SimpleDateFormat sdf = new SimpleDateFormat(MACHINE_DATE_FORMAT);
		return sdf.format(new Date(System.currentTimeMillis()));
	}
	
	public static String getCurrentTimestamp() {
		SimpleDateFormat sdf = new SimpleDateFormat(MACHINE_TIMESTAMP);
		return sdf.format(new Date(System.currentTimeMillis()));
	}
	
	private static final SimpleDateFormat sMachineDateFormat = new SimpleDateFormat(MACHINE_DATE_FORMAT);
	public static Date getDate(String date) {
		if (date == null) return null;
		try {
			return sMachineDateFormat.parse(date);
		} catch (ParseException e) {
			Log.i("Util", "Exception converting string to date, dateString: "+date, e);
		}
		return null;
	}

	public static String getCurrentTime() {
		SimpleDateFormat sdf = new SimpleDateFormat(MACHINE_TIME_FORMAT);
		return sdf.format(new Date(System.currentTimeMillis()));
	}
	
	public static String getTodaysArchiveFileName() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
		return sdf.format(new Date(System.currentTimeMillis()));
	}
	
	public static String getArchiveFileName(int olderByDays) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_MONTH, -1*olderByDays);
		return sdf.format(c.getTime());
	}
	
	public static String getReadableDate(String date) {
		/*SimpleDateFormat sdf = new SimpleDateFormat(MACHINE_DATE_FORMAT);
		SimpleDateFormat rsdf = new SimpleDateFormat(READABLE_DATE_FORMAT);
		try {
			return rsdf.format(sdf.parse(date));
		} catch (ParseException e) {}
		return "N/A";*/
		if (date == null || date.length() != 8) {
			return date;
		}
		
		return   date.substring(0, 2) + "/" 
		       + date.substring(2, 4) + "/" 
		       + date.substring(4, date.length());
	}
	
	public static String getReadableDate(Date date) {
		if (date == null) return "N/A";
		SimpleDateFormat rsdf = new SimpleDateFormat(READABLE_DATE_FORMAT);
		return rsdf.format(date);
	}
	
	public static String getTimeIn24HourFormat(String timeString) {
		SimpleDateFormat sdf = new SimpleDateFormat(MACHINE_TIME_FORMAT);
		SimpleDateFormat rsdf = new SimpleDateFormat("HH:mm");
		try {
			return rsdf.format(sdf.parse(timeString));
		} catch (ParseException e) {}
		return "00:00";
	}
	
	public static String getReadableMonth(String date) {
		SimpleDateFormat sdf = new SimpleDateFormat(MACHINE_DATE_FORMAT);
		SimpleDateFormat monthFormat = new SimpleDateFormat(MONTH_FORMAT);
		
		try {
			return monthFormat.format(sdf.parse(date));
		} catch (Exception e) {}
		return "";
	}
	
	public static String getReadableYear(String date) {
		SimpleDateFormat sdf = new SimpleDateFormat(MACHINE_DATE_FORMAT);
		SimpleDateFormat yearFormat = new SimpleDateFormat(YEAR_FORMAT);
		
		try {
			return yearFormat.format(sdf.parse(date));
		} catch (Exception e) {}
		return "";
	}

	public static double sigma(String[] items) {
		double sigma = 0;
		for(String i : items) {
			try {
				//sigma += Long.parseLong(i);
				sigma += Double.parseDouble(i);
			}catch(Exception e) {}
		}
		return sigma;
	}

	public static String addDaysAndFormatDaysMonth(String dateString, int days) {
		SimpleDateFormat sdf = new SimpleDateFormat(MACHINE_DATE_FORMAT);
		try {
			Date date = sdf.parse(dateString);
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			cal.add(Calendar.DATE, days);
			Date newDate = cal.getTime();
			SimpleDateFormat daysMonthSdf = new SimpleDateFormat(READABLE_DATE_FORMAT);
			return daysMonthSdf.format(newDate);
		} catch (Exception e) {
			throw new IllegalArgumentException(
					"Unable to add days to date: "+dateString, e);
		}
	}
	
	public static int deltaMonths(String dateString1, String dateString2) {
		SimpleDateFormat sdf = new SimpleDateFormat(MACHINE_DATE_FORMAT);
		try {
			Date date1 = sdf.parse(dateString1);
			Date date2 = sdf.parse(dateString2);
			long deltaMillies = Math.abs(date1.getTime() - date2.getTime());
			double deltaMonths = ((double)deltaMillies)/(30*24*60*60*1000.0);
			int returnValue = (int)Math.round(deltaMonths);
			return returnValue;
		} catch (Exception e) {
			throw new IllegalArgumentException(
					"Unable to find deltaMonths for date1: "+dateString1+
					", date2: "+dateString2, e);
		}
	}

	public static int asInteger(String intString) {
		try {
			return Integer.parseInt(intString);
		} catch (NumberFormatException e) {
			Log.i("Util","NumberFormatException parsing string: "+intString, e);
			return 0;
		}
	}
	
	public static String getFormattedDate(Date date, String pattern) {
		if (date == null) return null;
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(date);
	}
	
	public static String getFormattedDate(Date date) {
		if (date == null) return null;
		SimpleDateFormat sdf = new SimpleDateFormat(MACHINE_DATE_FORMAT);
		return sdf.format(date);
	}
	
	public static double asDouble(String doubleString) {
		try {
			return Double.parseDouble(doubleString);
		} catch (NumberFormatException e) {
			Log.i("Util","NumberFormatException parsing string: "+doubleString, e);
			return 0;
		}
	}
	
	public static Date asDate(String dateString) {
		SimpleDateFormat sdf = new SimpleDateFormat(MACHINE_DATE_FORMAT);
		try {
			return sdf.parse(dateString);
		} catch (Exception e) {
			Log.i("Util","Exception parsing for date string: "+dateString, e);
		}
		return null;
	}
	
	//private static GradientDrawable mCurrentFocusFieldGradient;
	//private static GradientDrawable mInputFieldGradient;
	public static GradientDrawable getInputFieldFocusStyle(ViewManager viewManager) {
		return (GradientDrawable) viewManager.getActivity().getResources().getDrawable(R.drawable.current_focus_field);
	}
	public static GradientDrawable getInputFieldStyle(ViewManager viewManager) {
		return (GradientDrawable) viewManager.getActivity().getResources().getDrawable(R.drawable.input_field);
	}
	
	public static GradientDrawable getInputFieldErrorStyle(ViewManager viewManager) {
		return (GradientDrawable) viewManager.getActivity().getResources().getDrawable(R.drawable.input_field_error);
	}
	public static AnimationDrawable getInputFieldFocusStyleAnimated(ViewManager viewManager) {
		return (AnimationDrawable)viewManager.getActivity().getResources().getDrawable(R.drawable.curren_focus_field_animated);
	}
	public static AnimationDrawable getInputFieldFocusErrorStyleAnimated(ViewManager viewManager) {
		return (AnimationDrawable)viewManager.getActivity().getResources().getDrawable(R.drawable.curren_focus_field_error_animated);
	}
	public static TransitionDrawable getKeypadButtonPressedAnimated(Activity activity) {
		return (TransitionDrawable)activity.getResources().getDrawable(R.drawable.keypad_button_pressed_animation);
	}
	public static Drawable getItemHighlighter(Context activity) {
		return (GradientDrawable) activity.getResources().getDrawable(R.drawable.item_highlighter);
	}
	public static Drawable getFilterOffBackground(Context activity) {
		return activity.getResources().getDrawable(R.drawable.keypad_button);
	}
	public static Drawable getFilterOnBackground(Context activity) {
		return activity.getResources().getDrawable(R.drawable.red_button);
	}
	public static Drawable getRedButtonBackground(Context activity) {
		return activity.getResources().getDrawable(R.drawable.red_button);
	}
	public static Drawable getGreenButtonBackground(Context activity) {
		return activity.getResources().getDrawable(R.drawable.green_button);
	}
	public static Drawable getKeyPadButtonBackground(Context activity) {
		return activity.getResources().getDrawable(R.drawable.keypad_button);
	}
	
	/**
	 * Rounds the decimal part at 2nd decimal place
	 * eg: 12.39999999999 returns 12.40
	 * eg: 12.37888888888 returns 12.38
	 */
	public static double roundDec2(double r) {
		int sign = 1;
		if (r < 0) sign = -1*sign;
		return ((int)((r + sign*0.005)*100))/100.0;
	}

	public static String inWords(String value) {
		try {
			double d = Double.parseDouble(value);
			String s = ((int)Math.floor(d))+"";
			StringBuffer b = new StringBuffer();
			for (int i = 0; i < s.length(); i++) {
				char c = s.charAt(i);
				switch (c) {
				case '1':
					b.append(" ONE");
					break;
				case '2':
					b.append(" TWO");
					break;
				case '3':
					b.append(" THREE");
					break;
				case '4':
					b.append(" FOUR");
					break;
				case '5':
					b.append(" FIVE");
					break;
				case '6':
					b.append(" SIX");
					break;
				case '7':
					b.append(" SEVEN");
					break;
				case '8':
					b.append(" EIGHT");
					break;
				case '9':
					b.append(" NINE");
					break;
				case '0':
					b.append(" ZERO");
					break;
				}
			}
			return b.toString().trim();
		} catch (Exception e) {}
		return "";
	}
	
	// input date range must be in the form of
	// MMYYYYMMYYYY
	public static String getReadableDateRange(String dateRange) {
		if (dateRange == null) {
			return "";
		}
		
		dateRange = dateRange.trim();
		if (dateRange.length() == 0 ||
			dateRange.length() != 12) {
			return "";
		}
		
		String dateFrom = dateRange.substring(0,6);
		String dateTo = dateRange.substring(6,dateRange.length());
		
		return dateFrom.substring(0,2) + "/" + dateFrom.substring(2,dateFrom.length())
			   + "-" +
			   dateTo.substring(0,2) + "/" + dateTo.substring(2,dateTo.length());
	}

	public static long trimTime(long time) {
		long dayMillies = 1 * 24 * 60 * 60 * 1000;
		return time - ( time % dayMillies );
	}
	
	public static Date trimTime(Date date) {
		if (date == null) return null;
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c.getTime();
	}
	
	public static List<String> getTrimedAglServices(List<String> aglServicesList) {
		List<String> l = new ArrayList<String>();
		if (aglServicesList == null || aglServicesList.isEmpty()) {
			return l;
		}
		for (String aglSvc : aglServicesList) {
			if (aglSvc.length() == 13) {
				l.add(aglSvc.substring(7,aglSvc.length()));
			} else {
				l.add(aglSvc);
			}
		}
		return l;
	}

	public static File getDownloadDir() {
		/*return Environment.getExternalStoragePublicDirectory(
				Environment.DIRECTORY_DOWNLOADS).getParentFile();*/
		File file = new File(getEcollectorBaseDir(),"downloads");
		file.mkdirs();
		return file;
		
	}
	
	public static File getArchiveDir() {
		File file = new File(getEcollectorBaseDir(),"archive");
		file.mkdirs();
		return file;
		
	}
	
	private static File getEcollectorBaseDir() {
		File file = new File(Environment.getExternalStorageDirectory(),"ecollector");
		file.mkdirs();
		return file;
	}
	
	public static Date getDateFromDp(DatePicker dp) {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, dp.getYear());
		c.set(Calendar.MONTH, dp.getMonth());
		c.set(Calendar.DAY_OF_MONTH, dp.getDayOfMonth());
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c.getTime();
	}
	
	// NOTE: DO NOT DELETE THIS CODE !! USEFUL FOR A FEATURE
	/*public static void printInetAddress() throws Exception {
		// Ref: http://stackoverflow.com/questions/2845279/how-to-get-the-lan-ip-of-a-client-using-java
		for (final Enumeration<NetworkInterface> interfaces = NetworkInterface
				.getNetworkInterfaces(); interfaces.hasMoreElements();) {
			final NetworkInterface cur = interfaces.nextElement();

			if (cur.isLoopback()) {
				continue;
			}

			Log.i("Util","interface " + cur.getName());

			for (final InterfaceAddress addr : cur.getInterfaceAddresses()) {
				final InetAddress inet_addr = addr.getAddress();

				if (!(inet_addr instanceof Inet4Address)) {
					continue;
				}

				Log.i("Util","  address: " + inet_addr.getHostAddress()
						+ "/" + addr.getNetworkPrefixLength());

				Log.i("Util","  broadcast address: "
						+ addr.getBroadcast().getHostAddress());
			}
		}
	}*/

	public static long computeDeltaSecs(Date fromDate, Date toDate) {
		if (fromDate == null || toDate == null) return 0l;
		long dayMillies = 1 * 24 * 60 * 60 * 1000;
		// we add dayMillies to include the complete toDate day.
		long delta = (toDate.getTime()+dayMillies) - fromDate.getTime();
		return delta/1000;
	}

	/**
	 * Schedule recurring service that consumes
	 * deltaTicks.
	 * 
	 */
	public static void scheduleDeltaTickConsumer(Context context) {
		Intent serviceIntent = 
			new Intent(context, DeltaTicksConsumerService.class);
        PendingIntent pendingIntent = PendingIntent.getService(
				context, 
				0, serviceIntent, 
				0);
        AlarmManager alarmManager = 
			(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		
		alarmManager.cancel(pendingIntent);
		
		alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, 
				SystemClock.elapsedRealtime() + 
				  (DeltaTicksConsumerService.DELTA_TICKS_BEAT_INTERVAL_SECS*1000),
				  DeltaTicksConsumerService.DELTA_TICKS_BEAT_INTERVAL_SECS*1000, 
				pendingIntent);
	}
	
	/**
	 * Schedule recurring service that consumes
	 * deltaTicks.
	 * 
	 */
	public static void schedulePrPushService(Context context) {
		Intent serviceIntent = 
			new Intent(context, PrPushService.class);
        PendingIntent pendingIntent = PendingIntent.getService(
				context, 
				0, serviceIntent, 
				0);
        AlarmManager alarmManager = 
			(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		
		alarmManager.cancel(pendingIntent);
		
		alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, 
				SystemClock.elapsedRealtime() + 
				  (PrPushService.PUSH_INTERVAL_SECS*1000),
				  PrPushService.PUSH_INTERVAL_SECS*1000, 
				pendingIntent);
	}
	
	/**
	 * Schedule recurring service that execute commands.
	 * 
	 */
	public static void scheduleCommandExecutorService(Context context) {
		Intent serviceIntent = 
			new Intent(context, CommandExecutorService.class);
        PendingIntent pendingIntent = PendingIntent.getService(
				context, 
				0, serviceIntent, 
				0);
        AlarmManager alarmManager = 
			(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		
		alarmManager.cancel(pendingIntent);
		
		alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, 
				SystemClock.elapsedRealtime() + 
				  (CommandExecutorService.EXECUTION_INTERVAL_SECS*1000),
				  CommandExecutorService.EXECUTION_INTERVAL_SECS*1000, 
				pendingIntent);
	}
	
	/**
	 * Schedule recurring service that archives PRs.
	 * 
	 */
	public static void schedulePrArchiveService(Context context) {
		Intent serviceIntent = 
			new Intent(context, PrArchiveService.class);
        PendingIntent pendingIntent = PendingIntent.getService(
				context, 
				0, serviceIntent, 
				0);
        AlarmManager alarmManager = 
			(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		
		alarmManager.cancel(pendingIntent);
		
		alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, 
				SystemClock.elapsedRealtime() + 
				  (PrArchiveService.EXECUTION_INTERVAL_SECS*1000),
				  PrArchiveService.EXECUTION_INTERVAL_SECS*1000, 
				pendingIntent);
	}
	
	//private static SoundPool sSoundPool = new SoundPool(2,AudioManager.STREAM_MUSIC,0); 
	public static void playResponseSound(final Context context) {
		Thread t = new Thread(new Runnable(){
			public void run() {
				MediaPlayer mp = MediaPlayer.create(context, R.raw.small_bell_ring_01);  
				mp.start();
				mp.release();
				/*int soundId = sSoundPool.load(context, R.raw.small_bell_ring_01, 1);
				sSoundPool.play(soundId, 1, 1, 1, 0, 1);*/
			}
		});
		t.start();
	}

	public static void schedulePrGenerator(Context context) {
		Intent serviceIntent = 
			new Intent(context, AutoPrGeneratorService.class);
        PendingIntent pendingIntent = PendingIntent.getService(
				context, 
				0, serviceIntent, 
				0);
        AlarmManager alarmManager = 
			(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		
		alarmManager.cancel(pendingIntent);
		
		alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, 
				SystemClock.elapsedRealtime() /*+ 
				  (AutoPrGeneratorService.EXECUTION_INTERVAL_SECS*1000)*/,
				  AutoPrGeneratorService.EXECUTION_INTERVAL_SECS*1000, 
				pendingIntent);
		
	}
}
