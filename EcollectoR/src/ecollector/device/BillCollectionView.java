package ecollector.device;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import ecollector.device.view.FragmentView;
import ecollector.device.view.ViewManager;
import ecollector.device.view.input.KeyPad;
import ecollector.device.view.input.KeyPadListener;
import ecollector.device.view.input.NumericNavKeyPad;

public class BillCollectionView extends FragmentView implements KeyPadListener {
	private ViewManager mViewManager;
	private TextView mMessageText;
	private TextView mArrearsAmountTv;
	private TextView mAcdAmountTv;
	private TextView mRcAmountTv;
	//private TextView mOthersTv;
	private TextView mAglAmountTv;
	private TextView mAmountCollectedTv;
	
	private static final int F_ARREARS_AMOUNT = 0;
	private static final int F_ACD_AMOUNT = 2;
	private static final int F_OTHERS = 4;
	private static final int F_AGL_AMOUNT = 5;
	private static final int F_AMOUNT_COLLECTED = 6;
	
	private StringBuffer mCurrentFieldValue;
	private int mCurrentFieldLenght;
	private TextView mCurrentTextView;
	private int mCurrentFocusField;
	
	private NumericNavKeyPad mNumericNavKp;
	
	private BillCollectionBean mBcBean;
	public BillCollectionView() {
		
	}
	public BillCollectionView(ViewManager viewManager, BillCollectionBean serviceCollectionBean) {
		//mViewManager = viewManager;
		mBcBean = serviceCollectionBean;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mViewManager = getFragmentViewManager();
		if (mBcBean.mIsCursorEmpty) {
			//Toast.makeText((Context) mViewManager, "Service Record not found with id: "+mServiceId, Toast.LENGTH_SHORT).show();
			TextView tv = new TextView((Context)mViewManager);
			tv.setText("Service Number "+ mBcBean._id +" not found");
			return tv;
		}
		
		// Prepare initialitzations for bill collection
		mBcBean.prepareForCollection();
		return prepareView(inflater);
	}

	private View prepareView(LayoutInflater inflater) {
		View view = inflater.inflate(R.layout.bill_collection_view, null);
		
		mMessageText = (TextView)view.findViewById(R.id.messageText);
		mMessageText.setText("Enter Amount");
		mNumericNavKp = new NumericNavKeyPad();
		mViewManager.showKeyPad(mNumericNavKp);
		
		
		mArrearsAmountTv = (TextView)view.findViewById(R.id.arrearsNcmdAmtTv);
		mAcdAmountTv = (TextView)view.findViewById(R.id.acdAmountTv);
		mRcAmountTv = (TextView)view.findViewById(R.id.rcAmountTv);
		//mOthersTv = (TextView)view.findViewById(R.id.othersTv);
		mAglAmountTv = (TextView)view.findViewById(R.id.aglAmount1Tv);
		mAmountCollectedTv = (TextView)view.findViewById(R.id.amountCollectedTv);
		
		updateDependentView();
		
		highlightReadonly(mArrearsAmountTv);
		// ACD is non editable
		highlightReadonly(mAcdAmountTv);
		//registerEventHandlers(mAcdAmountTv,F_ACD_AMOUNT);
		highlightReadonly(mRcAmountTv);
		//registerEventHandlers(mOthersTv,F_OTHERS);
		highlightReadonly(mAglAmountTv);
		registerEventHandlers(mAmountCollectedTv,F_AMOUNT_COLLECTED);
		
		// default focus
		final Handler h = new Handler();
		(new Thread(
			new Runnable() {
				public void run() {
					h.postDelayed(new Runnable(){
						@Override
						public void run() {
							setFocus(F_AMOUNT_COLLECTED);
						}
					}, 10);
				}
			}
		)).start();
		
		updateNextButtonState();
		
		/*Button selectRemarksBtn = (Button)view.findViewById(R.id.selectRemarksBtn);
		selectRemarksBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				selectRemarks();
			}
		});*/
		
		return view;
	}
	
	private void updateNextButtonState() {
		if (mBcBean.isCollectedAmountValid()) {
			mNumericNavKp.setNextEnabled(true);
		} else {
			mNumericNavKp.setNextEnabled(false);
		}
	}
	
	private void updateDependentView() {
		mArrearsAmountTv.setText(mBcBean.arrears_n_demand);
		mAcdAmountTv.setText(mBcBean.acd_collected);
		mRcAmountTv.setText(mBcBean.rc_collected);
		//mOthersTv.setText(mBcBean.others_collected);
		mAglAmountTv.setText(mBcBean.agl_amount);
		mAmountCollectedTv.setText(mBcBean.mVolatileAmountCollected);
		mMessageText.setText("Enter Collectable Amount: "+mBcBean.mVolatileCollectableAmount);
		//mMessageText.setText("Collectable Amount: "+mBcBean.mVolatileCollectableAmount);
		
		setFocus(mCurrentFocusField);
		updateNextButtonState();
	}
	
	private void selectRemarks() {
		BillCollectionRemarksView v = new BillCollectionRemarksView(mBcBean);
		mViewManager.showView(v);
	}
	private void registerEventHandlers(TextView tv, final int focusField) {
		tv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setFocus(focusField);
				
				if (focusField == F_AMOUNT_COLLECTED) {
					onKeyClick(KeyPad.KEY_DEL);
				}
			}
		});
	}

	private void setFocus(int focusField) {
		unHighlightAll();
		mCurrentFocusField = focusField;
		switch (focusField) {
		case F_ACD_AMOUNT:
			focusBackground(mAcdAmountTv);
			mCurrentFieldValue = mBcBean.acd_collected;
			mCurrentFieldLenght = 7;
			mCurrentTextView = mAcdAmountTv;
			break;
		case F_AGL_AMOUNT:
			focusBackground(mAglAmountTv);
			break;
		case F_ARREARS_AMOUNT:
			/*focusBackground(mArrearsAmountTv);
			mCurrentFieldValue = mBcBean.arriers;
			mCurrentFieldLenght = 10;
			mCurrentTextView = mArrearsAmountTv;*/
			break;
		/*case F_OTHERS:
			focusBackground(mOthersTv);
			mCurrentFieldValue = mBcBean.others_collected;
			mCurrentFieldLenght = 7;
			mCurrentTextView = mOthersTv;
			break;*/
		case F_AMOUNT_COLLECTED:
			focusBackground(mAmountCollectedTv);
			mCurrentFieldValue = mBcBean.mVolatileAmountCollected;
			mCurrentFieldLenght = 8;
			mCurrentTextView = mAmountCollectedTv;
			break;
		default:
			mCurrentFieldValue = null;
			mCurrentFieldLenght = 0;
			mCurrentTextView = null;
		}
	}
	
	private void focusBackground(View tv) {
		//tv.setBackgroundDrawable(Util.getInputFieldFocusStyle(mViewManager));
		AnimationDrawable background = 
			Util.getInputFieldFocusStyleAnimated(mViewManager);
		if (tv.equals(mAmountCollectedTv)) {
			if (!mBcBean.isCollectedAmountValid()) {
				background = Util.getInputFieldFocusErrorStyleAnimated(
						mViewManager);
			}
		}
		tv.setBackgroundDrawable(background);
		((AnimationDrawable)tv.getBackground()).start();
	}
	
	private void highlightReadonly(View tv) {
		tv.setBackgroundDrawable(Util.getItemHighlighter(mViewManager.getActivity()));
		tv.setOnClickListener(null);
	}

	private void unHighlightAll() {
		//mAcdAmountTv.setBackgroundDrawable(Util.getInputFieldStyle(mViewManager));
		//mAglAmount.setBackgroundDrawable(Util.getInputFieldStyle(mViewManager));
		//mArrearsAmountTv.setBackgroundDrawable(Util.getInputFieldStyle(mViewManager));
		//mOthersTv.setBackgroundDrawable(Util.getInputFieldStyle(mViewManager));
		//mRcAmountTv.setBackgroundDrawable(Util.getInputFieldStyle(mViewManager));
		
		Drawable background = 
			Util.getInputFieldStyle(mViewManager);
		if (!mBcBean.isCollectedAmountValid()) {
			background = Util.getInputFieldErrorStyle(mViewManager);
		}mAmountCollectedTv.setBackgroundDrawable(background);
	}
	
	private void showNext() {
		mBcBean.computeBill();
		if ((int)Util.asDouble(mBcBean.mVolatileCollectionDelta.toString()) <= 0) {
			BillDetailsView v = 
				new BillDetailsView(/*mViewManager,*/mBcBean);
			mViewManager.showView(v);
		} else {
			BillCollectionConfirmationView v = 
				new BillCollectionConfirmationView(mBcBean);
			mViewManager.showView(v);
			mViewManager.removeScreenFromNavigation();
		}
	}
	
	private void showPrevious() {
		mViewManager.showPrevious();
	}
	
	public void onKeyClick(char key) {
		if (mCurrentFieldValue == null) return;
		switch (key) {
		case KeyPad.KEY_DEL:
			mCurrentFieldValue.setLength(0);
			break;
		case KeyPad.KEY_BACKSPACE:
			if (mCurrentFieldValue.length() > 1)
				mCurrentFieldValue.setLength(mCurrentFieldValue.length()-1);
			else {
				mCurrentFieldValue.setLength(0);
			}
			break;
		case KeyPad.KEY_PREVIOUS:
			showPrevious();
			break;
		case KeyPad.KEY_NEXT:
			showNext();
			break;
		default:
			if (mCurrentFieldValue.length() < mCurrentFieldLenght){
				mCurrentFieldValue.append(key);
			}
		}
		if (mCurrentTextView != null) {
			int v = (int)Util.asDouble(mCurrentFieldValue.toString());
			mCurrentFieldValue.setLength(0);
			mCurrentFieldValue.append(v+"");
			mCurrentTextView.setText(v+"");
		}
		
		if (!mAmountCollectedTv.equals(mCurrentTextView)) {
			mBcBean.computeCollectableAmount();
			
			// Set collectable amount to amount collected for convineance
			mBcBean.mVolatileAmountCollected.setLength(0);
			mBcBean.mVolatileAmountCollected.append(mBcBean.mVolatileCollectableAmount);
			updateDependentView();
		} else {
			updateCollectedAmountDependentView();
		}
	}

	private void updateCollectedAmountDependentView() {
		// warns if wrong amount entered, by 
		// changing background
		setFocus(F_AMOUNT_COLLECTED);
		updateNextButtonState();
	}
}