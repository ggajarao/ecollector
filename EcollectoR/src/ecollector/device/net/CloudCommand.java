package ecollector.device.net;

import java.io.IOException;
import java.lang.reflect.UndeclaredThrowableException;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import ecollector.device.NetworkAvailabilityState;
import ecollector.device.view.action.HandlerActions;
import ecollector.device.view.action.Messinger;

public abstract class CloudCommand {
	
	private Messinger mMessinger;
	protected Context mContext;
	public CloudCommand(Context context, Messinger handler) {
		mMessinger = handler;
		mContext = context;
	}
	
	public void execute() {
		Message msg;
		if (!NetworkAvailabilityState.isNetworkAvailable(mContext)) {
			msg = Message.obtain();
			msg.arg1 = HandlerActions.NETWORK_UNAVAILABLE;
			sendMessage(msg);
			return;
		}
		
		try {
			CloudServices.ensureLoggedIn();
			
			this.perform();
			
		} catch (UndeclaredThrowableException e) {
			Log.i(this.getClass().getName(),"Error executing command",e);
			Throwable cause = e.getCause();
			msg = Message.obtain();
			
			if (cause == null) {
				msg.arg1 = HandlerActions.OTHER_ERROR;
			} else if (cause instanceof IOException) {
				msg.arg1 = HandlerActions.NETWORK_ERROR;
				String causeMsg = cause.getMessage();
				if (causeMsg != null) {
					msg.obj = new StringBuffer(causeMsg);
				}
			} else {
				msg.arg1 = HandlerActions.OTHER_ERROR;
				String causeMsg = cause.getMessage();
				if (causeMsg != null) {
					msg.obj = new StringBuffer(causeMsg);
				}
			}
			sendMessage(msg);
		} catch (Exception e) {
			Log.i(this.getClass().getName(),"Error executing command",e);
			msg = Message.obtain();
			msg.arg1 = HandlerActions.OTHER_ERROR;
			
			StringBuffer sb = new StringBuffer((e.getMessage()!=null)?e.getMessage():"Error ");
			if (e.getCause() != null) {
				sb.append("Cause: "+e.getCause().getMessage());
			}
			msg.obj = sb.toString();
			sendMessage(msg);
		}
	}
	
	protected final void sendMessage(Message msg) {
		if (mMessinger != null) mMessinger.sendMessage(msg);
	}

	abstract protected void perform() throws Exception;
	abstract public Object getResults();
	
	/**
	 * When ui is ready, ui threads can call this
	 * method to recieve notifications via handler
	 * 
	 * @param handler
	 */
	public void prepareUi(Handler handler) {
		//mMessinger = handler;
	}
}
