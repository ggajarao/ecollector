package ecollector.device.net;

import java.util.List;

import android.content.ContentResolver;
import android.content.Context;
import android.os.Message;
import ebilly.admin.shared.DeviceFileUi;
import ecollector.device.db.EcollectorDB;
import ecollector.device.view.action.HandlerActions;
import ecollector.device.view.action.Messinger;

public class CheckUpdatesCommand extends CloudCommand {
	
	private List<DeviceFileUi> mDeviceFiles;
	private ContentResolver mContentResolver;
	public CheckUpdatesCommand(ContentResolver contentResolver, Context context, Messinger messinger) {
		super(context, messinger);
		mContentResolver = contentResolver;
	}
	
	protected void perform() throws Exception {
		String deviceIdentifier = EcollectorDB.fetchDeviceIdentifier(mContentResolver);
		Message msg;
		msg = Message.obtain();
		msg.arg1 = HandlerActions.CLOUD_CHK_FOR_UPDATES;
		sendMessage(msg);		
		mDeviceFiles = CloudServices.getDownloadableDeviceFiles(deviceIdentifier);
	}
	
	public Object getResults() {
		return mDeviceFiles;
	}
}
