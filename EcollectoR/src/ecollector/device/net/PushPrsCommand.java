package ecollector.device.net;

import android.content.Context;
import ebilly.admin.shared.PushPrRequest;
import ebilly.admin.shared.viewdef.CrudResponse;
import ecollector.device.view.action.Messinger;

public class PushPrsCommand extends CloudCommand {

	private PushPrRequest mPushPrRequest;
	private CrudResponse mCrudResponse;
	public PushPrsCommand(Context context, Messinger messinger, PushPrRequest pushPrRequest) {
		super(context, messinger);
		mPushPrRequest = pushPrRequest;
	}

	@Override
	protected void perform() throws Exception {
		mCrudResponse = CloudServices.pushPrs(mPushPrRequest);
	}

	@Override
	public Object getResults() {
		return mCrudResponse;
	}
}
