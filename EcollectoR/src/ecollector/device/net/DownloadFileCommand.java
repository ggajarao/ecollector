package ecollector.device.net;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.os.Message;
import android.util.Log;
import ebilly.net.DataTransferProgress;
import ebilly.net.HttpConnector;
import ecollector.common.NetConstants;
import ecollector.device.view.action.HandlerActions;
import ecollector.device.view.action.Messinger;

public class DownloadFileCommand extends CloudCommand {
	
	class MyDataTransferProgress implements DataTransferProgress {
		private CloudCommand mCloudCommand; 
		public MyDataTransferProgress(CloudCommand cloudCommand) {
			mCloudCommand = cloudCommand;
		}
		@Override
		public void progressBytes(int progressBytes) {
			Message msg = Message.obtain();
			msg.arg1 = HandlerActions.DOWNLOADING_PROGRESS;
			msg.arg2 = progressBytes;
			mCloudCommand.sendMessage(msg);
		}
		@Override
		public void transferBegan(long contentLength) {
			Message msg = Message.obtain();
			msg.arg1 = HandlerActions.DOWNLOADING_STARTED;
			msg.obj = Long.valueOf(contentLength);
			mCloudCommand.sendMessage(msg);
		}
		@Override
		public void transferCompleted(int totalBytes) {
			Message msg = Message.obtain();
			msg.arg1 = HandlerActions.DOWNLOADING_COMPLETE;
			msg.arg2 = totalBytes;
			mCloudCommand.sendMessage(msg);
		}
	}
	
	private String mDownloadedFilePath; 
	private String mFileKey;
	private File mDownloadDir;
	public DownloadFileCommand(Context context, Messinger messinger, 
			String fileKey, File downloadDir) {
		super(context,messinger);
		mFileKey = fileKey;
		mDownloadDir = downloadDir;
	}

	@Override
	protected void perform() throws Exception {
		Message msg = Message.obtain();
		msg.arg1 = HandlerActions.DOWNLOADING_FILE;
		sendMessage(msg);
		MyDataTransferProgress dtp = new MyDataTransferProgress(this);
		HttpConnector hc = CloudServices.getHttpConnector();
		try {
			Map<String,String> params = new HashMap<String,String>();
			params.put("a", "r");
			params.put("bk", mFileKey);
			mDownloadedFilePath = 
				hc.downloadFile(NetConstants.DOWNLOAD_SERVICE, params, 
						mDownloadDir.getPath(),dtp);
			msg = Message.obtain();
			msg.arg1 = HandlerActions.DOWNLOADING_COMPLETE; 
			sendMessage(msg);
		} catch(IOException e) {
			msg = Message.obtain();
			msg.arg1 = HandlerActions.DOWNLOADING_FAILED; 
			sendMessage(msg);
			Log.i("ecollector.downloadFile", "Failed to download file", e);
		}
	}

	@Override
	public Object getResults() {
		return mDownloadedFilePath;
	}
}
