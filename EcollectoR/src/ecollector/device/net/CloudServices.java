package ecollector.device.net;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import ebilly.admin.client.service.LoginService;
import ebilly.admin.client.service.SecureService;
import ebilly.admin.shared.DeviceFileUi;
import ebilly.admin.shared.DevicePayload;
import ebilly.admin.shared.DeviceSummaryReport;
import ebilly.admin.shared.PushPrRequest;
import ebilly.admin.shared.UserContext;
import ebilly.admin.shared.viewdef.CrudResponse;
import ebilly.gwtsvcproxy.SyncProxy;
import ebilly.net.HttpConnector;
import ecollector.common.NetConstants;
import ecollector.device.db.EcollectorDB;

public class CloudServices {
	
	private static String uName;
	private static byte[] pwd;
	
	private static final String TAG = "ecollector.CloudServices";
	private static SecureService sSecureService;
	private static LoginService sLoginService;
	private static Context mContext;
	public static synchronized SecureService getSecureService() {
		if (sSecureService == null) {
			sSecureService = 
				(SecureService)SyncProxy.newProxyInstance(SecureService.class, 
						NetConstants.CLOUD_URL,NetConstants.CLOUD_SERVICE_SECURE);
		}
		return sSecureService;
	}
	public static synchronized LoginService getLoginService() {
		if (sLoginService == null) {
			sLoginService = 
				(LoginService)SyncProxy.newProxyInstance(LoginService.class, 
						NetConstants.CLOUD_URL,NetConstants.CLOUD_SERVICE_LOGIN);
		}
		return sLoginService;
	}
	
	public static void init(Context context) {
		mContext = context;
		String[] credentials = EcollectorDB.fetchCredentials(mContext);
		if (credentials == null) return;
		uName = credentials[0];
		pwd = credentials[1]!=null?credentials[1].getBytes():new byte[]{};
	}
	
	public static HttpConnector getHttpConnector() {
		return SyncProxy.getHttpConnector();
	}
	
	//public static boolean isLoggedIn = false;
	public static UserContext login(String userName, byte[] pwrd) {
		LoginService s = getLoginService();
		UserContext userContext = s.login(userName, new String(pwrd));
		if (userContext != null) {
			//isLoggedIn = true;
			Log.i(TAG, "Device successfully signed into cloud");
		} else {
			//isLoggedIn = false;
			Log.i(TAG, "Device cloud signin failed, wrong credentials?");
		}
		return userContext;
	}
	
	static void ensureLoggedIn() {
		LoginService s = getLoginService();
		
		DevicePayload payload = createPayload();
		UserContext uc = s.isLoggedIn(payload);
		if (uc == null) {
			uc = login(uName,pwd);
		}
		
		if (uc != null) {
			EcollectorDB.updateDeviceState(uc, mContext);
		}
		/*if (!isLoggedIn) {
			login(uName,pwd);
		}*/
	}
	
	private static DevicePayload createPayload() {
		DeviceSummaryReport dsr = EcollectorDB.fetchSummaryReport(
			mContext.getContentResolver()).asDeviceSummaryReport(mContext.getContentResolver());
		DevicePayload payload = new DevicePayload();
		payload.deviceSummaryReport = dsr;
		return payload;
	}
	public static List<DeviceFileUi> getDownloadableDeviceFiles(String deviceIdentifer) 
	throws Exception {
		if (TextUtils.isEmpty(deviceIdentifer)) {
			return Collections.emptyList();
		}
		ensureLoggedIn();
		SecureService s = getSecureService();
		DeviceFileUi[] aDfs = 
			s.fetchDownloadableDevicefiles(deviceIdentifer);
		if (aDfs != null) {
			return Arrays.asList(aDfs);
		} else {
			return Collections.emptyList();
		}
	}
	public static String deviceFileDownloadedInDevice(String deviceIdentifier,
			String deviceFileId) {
		if (TextUtils.isEmpty(deviceIdentifier) || TextUtils.isEmpty(deviceFileId)) {
			return "";
		}
		ensureLoggedIn();
		SecureService s = getSecureService();
		CrudResponse crudResponse = s.deviceFileDownloadedInDevice(deviceIdentifier,deviceFileId);
		if (crudResponse.isSuccess()) {
			return "OK";
		} else {
			Log.i("CloudServices","Failed to mark device file downloaded in cloud, message: "+crudResponse.errorMessage);
			return "ERROR";
		}
	}
	
	public static CrudResponse pushPrs(PushPrRequest pushPrRequest) {
		if (pushPrRequest == null) {
			return null;
		}
		//ensureLoggedIn();
		SecureService s = getSecureService();
		return s.pushPrs(pushPrRequest);
	}
	
	public static CrudResponse updateDeviceCommandState(String commandId,
			String commandState, String executionNotes) {
		if (commandId == null || commandState == null) {
			return null;
		}
		
		ensureLoggedIn();
		SecureService s = getSecureService();
		return s.updateDeviceCommand(commandId, commandState, executionNotes);
	}
	
	public static void release() {
		mContext = null;
	}
	
	public static void syncState() {
		CloudServices.ensureLoggedIn();
	}
}
