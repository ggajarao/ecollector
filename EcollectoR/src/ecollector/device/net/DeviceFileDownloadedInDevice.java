package ecollector.device.net;

import android.content.Context;
import ecollector.device.view.action.Messinger;

public class DeviceFileDownloadedInDevice extends CloudCommand {
	
	private String mDeviceIdentifier;
	private String mDeviceFileId;
	private String mResult;
	public DeviceFileDownloadedInDevice(Context context, Messinger messinger,
			String deviceIdentifier, String deviceFileId) {
		super(context,messinger);
		mDeviceIdentifier = deviceIdentifier;
		mDeviceFileId = deviceFileId;
	}

	@Override
	protected void perform() throws Exception {
		mResult = CloudServices.deviceFileDownloadedInDevice(mDeviceIdentifier, mDeviceFileId);
	}

	@Override
	public Object getResults() {
		return mResult;
	}
}
