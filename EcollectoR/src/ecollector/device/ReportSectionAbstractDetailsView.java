package ecollector.device;

import java.util.Date;

import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import ecollector.common.ReportAbstractColumns;
import ecollector.device.db.EcollectorDB;
import ecollector.device.db.SectionAbstractReport;
import ecollector.device.view.FragmentView;
import ecollector.device.view.ViewManager;
import ecollector.device.view.input.KeyPad;
import ecollector.device.view.input.KeyPadListener;
import ecollector.device.view.input.NavigationKeyPad;

public class ReportSectionAbstractDetailsView extends FragmentView implements KeyPadListener {
	private static final String TAG = "ReportSectionAbstractView";
	
	private ViewManager mViewManager;
	private ReportFilterBean mReportFilter;
	private ViewGroup mReportPanel;
	private SectionAbstractReport mReport;
	
	public ReportSectionAbstractDetailsView() {
		
	}
	public ReportSectionAbstractDetailsView(ReportFilterBean reportFilter/*,
			ViewManager viewManager*/) {
		mReportFilter = reportFilter;
		//mViewManager = viewManager;
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mViewManager = getFragmentViewManager();
		LinearLayout view = (LinearLayout)inflater.inflate(R.layout.report_section_abstract_view, null);
		
		mReportPanel = (ViewGroup)view.findViewById(R.id.reportContentPane);
		
		/*View hLine = inflater.inflate(R.layout.horizontal_line, null);
		mReportPanel.addView(hLine);*/
		
		mReport = new SectionAbstractReport(
				mReportFilter,
				mViewManager.getActivity().getContentResolver());
		while (mReport.hasNext()) {
			Cursor c = mReport.next();
			String distribution = 
				c.getString(c.getColumnIndex(ReportAbstractColumns.DISTRIBUTION));
			if (distribution == null || distribution.trim().length() == 0) {
				// wola, its a section.
				View hSpacer = inflater.inflate(R.layout.horizontal_spacer, null);
				mReportPanel.addView(hSpacer);
			}
			prepareSectionDistributionUi(c,mReportPanel, inflater);
		}
		
		NavigationKeyPad n = new NavigationKeyPad("Back","Print");
		mViewManager.showKeyPad(n);
		return view;
	}
	
	private void prepareSectionDistributionUi(Cursor c, 
			ViewGroup reportContentPane, LayoutInflater inflater) {
		Date fromDate = mReportFilter.getFromDate();
		Date toDate = mReportFilter.getToDate();
		
		ViewGroup secPane = (ViewGroup)inflater.inflate(R.layout.report_amount_abstract_section_or_distribution_item, null);
		
		TextView tv = (TextView)secPane.findViewById(R.id.rcCodeTv);
		String deviceName = "SCM Name: "+EcollectorDB.fetchDeviceName(mViewManager.getActivity().getContentResolver());
		tv.setText(deviceName);
		
		tv = (TextView)secPane.findViewById(R.id.dateRangeTv);
		String dateRange = "Between: "+Util.getReadableDate(fromDate)+" - "+Util.getReadableDate(toDate);
		tv.setText(dateRange);
		
		String sectionName = c.getString(c.getColumnIndex(ReportAbstractColumns.SECTION));
		String dist = c.getString(c.getColumnIndex(ReportAbstractColumns.DISTRIBUTION));
		
		tv = (TextView)secPane.findViewById(R.id.sectionTv);
		String secDistTitle = 
			"Sec: " + (sectionName==null?"N/A":sectionName) +
			((dist == null || dist.trim().length() == 0) ? "" : "  Dist: "+dist);
		
		tv.setText(secDistTitle);
		
		ViewGroup secReportItemsPane = (ViewGroup)secPane.findViewById(R.id.sectionDistributionContentPane);
		
		View reportItem = createReportItem(inflater, "CC", ReportAbstractColumns.ARREARS_N_DEMAND_COUNT, ReportAbstractColumns.ARREARS_N_DEMAND, R.layout.report_amount_abstract_item,c); 
		secReportItemsPane.addView(reportItem);	
		
		reportItem = createReportItem(inflater, "RC", ReportAbstractColumns.RC_COLLECTED_COUNT, ReportAbstractColumns.RC_COLLECTED, R.layout.report_amount_abstract_item,c); 
		secReportItemsPane.addView(reportItem);
		
		reportItem = createReportItem(inflater, "ACD", ReportAbstractColumns.ACD_COLLECTED_COUNT, ReportAbstractColumns.ACD_COLLECTED, R.layout.report_amount_abstract_item,c); 
		secReportItemsPane.addView(reportItem);
		
		reportItem = createReportItem(inflater, "AGL", ReportAbstractColumns.AGL_AMOUNT_COUNT, ReportAbstractColumns.AGL_AMOUNT, R.layout.report_amount_abstract_item,c); 
		secReportItemsPane.addView(reportItem);
		
		View hLine = inflater.inflate(R.layout.horizontal_line, null);
		secReportItemsPane.addView(hLine);
		
		reportItem = createReportItem(inflater, "Grand Total", ReportAbstractColumns.TOTAL_PRS, ReportAbstractColumns.GRAND_TOTAL, R.layout.report_amount_abstract_footer_item,c); 
		secReportItemsPane.addView(reportItem);
		
		hLine = inflater.inflate(R.layout.horizontal_line, null);
		secReportItemsPane.addView(hLine);
		
		reportContentPane.addView(secPane);
	}

	private ViewGroup createReportItem(LayoutInflater inflater, String title, 
			String countFieldName, String totalFieldName, int layoutResource, Cursor cursor) {
		String itemCount = cursor.getString(cursor.getColumnIndex(countFieldName));
		String itemTotal = cursor.getString(cursor.getColumnIndex(totalFieldName));
		ViewGroup reportItem = (ViewGroup)inflater.inflate(layoutResource, null);
		TextView reportItemTv = (TextView)reportItem.findViewById(R.id.itemNameTv);
		reportItemTv.setText(title);
		reportItemTv = (TextView)reportItem.findViewById(R.id.itemCountTv);
		reportItemTv.setText(itemCount);
		reportItemTv = (TextView)reportItem.findViewById(R.id.itemTotalTv);
		reportItemTv.setText(itemTotal);
		return reportItem;
	}
	
	private void printReport() {
		PrintBillAction a = 
			new PrintBillAction(mViewManager.getActivity(),
					mReport.getPrintBuffer().toString().getBytes());
		a.perform(null);
	}
	
	@Override
	public void onKeyClick(char key) {
		switch(key) {
		case KeyPad.KEY_PREVIOUS:
			mViewManager.showPrevious();
			break;
		case KeyPad.KEY_NEXT:
			printReport();
			break;
		} 
	}
}
