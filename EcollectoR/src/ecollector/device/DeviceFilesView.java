package ecollector.device;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import ecollector.device.view.FragmentView;
import ecollector.device.view.ViewManager;
import ecollector.device.view.action.HandlerActions;
import ecollector.device.view.input.KeyPad;
import ecollector.device.view.input.KeyPadListener;
import ecollector.device.view.input.NavigationKeyPad;

public class DeviceFilesView extends FragmentView implements KeyPadListener {
	
	private ViewManager mViewManager;
	private LayoutInflater mInflater;
	private TextView mMessageTextTv;
	private LinearLayout mView;
	private ViewGroup mFileListPnl;
	private Thread mWorker;
	private PowerManager.WakeLock mWakeLock;
	private NavigationKeyPad mNavikationKeyPad;
	/*public DeviceFilesView() {
		
	}*/
	public DeviceFilesView(/*ViewManager viewManager*/) {
		//mViewManager = viewManager;
	}
	
	private Handler myHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.arg1) {
			case HandlerActions.NETWORK_UNAVAILABLE:
				mMessageTextTv.setText("Internet unavailable !");
				break;
			case HandlerActions.CLOUD_CHK_FOR_UPDATES:
				mMessageTextTv.setText("Checking for updates...");
				break;
			case HandlerActions.CLOUD_CHK_FOR_UPDATES_COMPLETED:
				mMessageTextTv.setText("Select file to import");
				showDeviceFiles(this.getLooper());
				break;
			case HandlerActions.NETWORK_ERROR:
				mMessageTextTv.setText("Network error, "+(msg.obj==null?"":msg.obj.toString()));
				break;
			case HandlerActions.OTHER_ERROR:
				mMessageTextTv.setText(((msg.obj!=null)?msg.obj.toString():"Unknown Error"));
				break;
			case HandlerActions.CLOUD_CHK_FOR_UPDATES_NO_FILES:
				mMessageTextTv.setText("No updates available");
				break;
			case HandlerActions.IMPORTING_MEDIA_UNAVAILABLE:
				mMessageTextTv.setText("Memory card/storage unavailable");
				break;
			}
		};
	};
	
	private void showDeviceFiles(Looper looper) {
		mFileListPnl.removeAllViews();
		DownloadableDeviceFiles
		.getInstance(mViewManager.getActivity().getContentResolver())
		.prepareUi(mInflater, mView, looper, mNavikationKeyPad);
	}
	
	private void checkForUpdates() {
		Message msg;
		
		if (!ExternalStorageState.isExternalStorageAvailable) {
			msg = Message.obtain();
			msg.arg1 = HandlerActions.IMPORTING_MEDIA_UNAVAILABLE;
			myHandler.sendMessage(msg);
			return;
		}
		
		msg = Message.obtain();
		msg.arg1 = HandlerActions.CLOUD_CHK_FOR_UPDATES;
		myHandler.sendMessage(msg);
		
		mWorker = new Thread() {
			public void run() {
				DownloadableDeviceFiles
					.getInstance(mViewManager.getActivity().getContentResolver())
					.checkForUpdates(mViewManager.getActivity(), myHandler);
			}
		};
		mWorker.start();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mViewManager = getFragmentViewManager();
		mInflater = inflater;
		mView = (LinearLayout)inflater.inflate(R.layout.device_files_view, null);
		mMessageTextTv = (TextView)mView.findViewById(R.id.messageText);
		mFileListPnl = (ViewGroup)mView.findViewById(R.id.deviceFilesPnl);
		checkForUpdates();
		
		mNavikationKeyPad = new NavigationKeyPad();
		mNavikationKeyPad.setNextEnabled(false);
		mViewManager.showKeyPad(mNavikationKeyPad);
		return mView;
	}
	
	@Override
	public void onStart() {
		super.onStart();
		PowerManager pm = 
			(PowerManager)mViewManager.getActivity()
				.getSystemService(Context.POWER_SERVICE);
		
		mWakeLock =
			pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, 
					DeviceFilesView.class.getName());
		mWakeLock.acquire();
	}
	
	@Override
	public void onStop() {
		super.onStop();
		if (mWakeLock != null) {
			try {
				mWakeLock.release();
			} catch (RuntimeException e) {
				Log.i("ecollector.DeviceFilesView","error releasing wake lock",e);
			}
		}
	}
	
	@Override
	public void onKeyClick(char key) {
		switch(key) {
		case KeyPad.KEY_PREVIOUS:
			mViewManager.showPrevious();
			break;
		}
	}
}
