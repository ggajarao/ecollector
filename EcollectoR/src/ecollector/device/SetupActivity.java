package ecollector.device;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import ebilly.gwtsvcproxy.SyncProxy;
import ecollector.device.net.CloudServices;
import ecollector.device.view.FragmentViewManager;

public class SetupActivity extends FragmentViewManager {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		SyncProxy.releaseResources();
		CloudServices.init(this);
	}
	
	@Override
	protected Fragment getHomeView() {
		return new SetupView(/*this*/);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		SyncProxy.releaseResources();
	}
	
	@Override
	public void onBackPressed() {
		
	}
}
