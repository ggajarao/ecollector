package ecollector.device;

import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import ecollector.device.view.FragmentView;
import ecollector.device.view.ViewManager;
import ecollector.device.view.input.KeyPad;
import ecollector.device.view.input.KeyPadListener;
import ecollector.device.view.input.NumericNavKeyPad;

public class BillCollectionConfirmationView extends FragmentView implements KeyPadListener {
	private ViewManager mViewManager;
	private TextView mMessageText;
	private TextView mConfirmAmountTv;
	private StringBuffer mConfirmedAmount = new StringBuffer();
	private int mExcessAmountToConfirm;
	
	private static final int F_CONFIRM_AMOUNT = 0;
	
	private StringBuffer mCurrentFieldValue;
	private int mCurrentFieldLenght;
	private TextView mCurrentTextView;
	private int mCurrentFocusField;
	
	private NumericNavKeyPad mNumericNavKp;
	
	private BillCollectionBean mBcBean;
	public BillCollectionConfirmationView() {
		
	}
	
	public BillCollectionConfirmationView(/*ViewManager viewManager, */BillCollectionBean serviceCollectionBean) {
		//mViewManager = viewManager;
		mBcBean = serviceCollectionBean;
		mExcessAmountToConfirm = (int)Util.asDouble(mBcBean.mVolatileCollectionDelta.toString());
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mViewManager = getFragmentViewManager();
		mBcBean.computeBill();
		return prepareView(inflater);
	}

	private View prepareView(LayoutInflater inflater) {
		View view = inflater.inflate(R.layout.bill_collection_confirmation_view, null);
		
		mMessageText = (TextView)view.findViewById(R.id.messageText);
		mMessageText.setText("Confirm Excess Amount");
		mNumericNavKp = new NumericNavKeyPad();
		mViewManager.showKeyPad(mNumericNavKp);
		
		TextView tv = null;
		tv = (TextView)view.findViewById(R.id.collectableAmountTv);
		tv.setText(((int)Util.asDouble(mBcBean.mVolatileCollectableAmount))+"");
		highlightReadonly(tv);
		
		tv = (TextView)view.findViewById(R.id.amountCollectedTv);
		tv.setText(mBcBean.mVolatileAmountCollected);
		highlightReadonly(tv);
		
		tv = (TextView)view.findViewById(R.id.excessAmountTv);
		tv.setText(mExcessAmountToConfirm+"");
		highlightReadonly(tv);
		
		mConfirmAmountTv = (TextView)view.findViewById(R.id.confirmCollectedTv);
		
		updateDependentView();
		
		highlightReadonly(mConfirmAmountTv);
		registerEventHandlers(mConfirmAmountTv,F_CONFIRM_AMOUNT);
		
		
		// default focus
		final Handler h = new Handler();
		(new Thread(
			new Runnable() {
				public void run() {
					h.postDelayed(new Runnable(){
						@Override
						public void run() {
							setFocus(F_CONFIRM_AMOUNT);
						}
					}, 10);
				}
			}
		)).start();
		
		updateNextButtonState();
		return view;
	}
	
	private void updateNextButtonState() {
		if (isConfirmationAmountValid()) {
			mNumericNavKp.setNextEnabled(true);
		} else {
			mNumericNavKp.setNextEnabled(false);
		}
	}
	
	private void updateDependentView() {
		int confirmedAmount = Util.asInteger(mConfirmedAmount.toString());
		mConfirmedAmount.setLength(0);
		mConfirmedAmount.append(confirmedAmount);
		mConfirmAmountTv.setText(mConfirmedAmount.toString());
		setFocus(mCurrentFocusField);
		updateNextButtonState();
		
		// warns if wrong amount entered, by 
		// changing background
		setFocus(F_CONFIRM_AMOUNT);
		updateNextButtonState();
	}
	
	private void registerEventHandlers(TextView tv, final int focusField) {
		tv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setFocus(focusField);
			}
		});
	}

	private void setFocus(int focusField) {
		unHighlightAll();
		mCurrentFocusField = focusField;
		switch (focusField) {
		case F_CONFIRM_AMOUNT:
			focusBackground(mConfirmAmountTv);
			mCurrentFieldValue = mConfirmedAmount;
			mCurrentFieldLenght = 7;
			mCurrentTextView = mConfirmAmountTv;
			break;
		default:
			mCurrentFieldValue = null;
			mCurrentFieldLenght = 0;
			mCurrentTextView = null;
		}
	}
	
	private void focusBackground(View tv) {
		//tv.setBackgroundDrawable(Util.getInputFieldFocusStyle(mViewManager));
		AnimationDrawable background = 
			Util.getInputFieldFocusStyleAnimated(mViewManager);
		if (tv.equals(mConfirmAmountTv)) {
			if (!isConfirmationAmountValid()) {
				background = Util.getInputFieldFocusErrorStyleAnimated(
						mViewManager);
			}
		}
		tv.setBackgroundDrawable(background);
		((AnimationDrawable)tv.getBackground()).start();
	}
	
	private boolean isConfirmationAmountValid() {
		return mExcessAmountToConfirm == (int)Util.asInteger(mConfirmedAmount.toString()); 
	}

	private void highlightReadonly(View tv) {
		tv.setBackgroundDrawable(Util.getItemHighlighter(mViewManager.getActivity()));
		tv.setOnClickListener(null);
	}

	private void unHighlightAll() {
		Drawable background = 
			Util.getInputFieldStyle(mViewManager);
		if (!isConfirmationAmountValid()) {
			background = Util.getInputFieldErrorStyle(mViewManager);
		}
		mConfirmAmountTv.setBackgroundDrawable(background);
	}
	
	private void showNext() {
		BillDetailsView v = new BillDetailsView(/*mViewManager, */mBcBean);
		mViewManager.showView(v);
	}
	
	private void showPrevious() {
		mViewManager.showPrevious();
	}
	
	public void onKeyClick(char key) {
		if (mCurrentFieldValue == null) return;
		switch (key) {
		case KeyPad.KEY_DEL:
			mCurrentFieldValue.setLength(0);
			break;
		case KeyPad.KEY_BACKSPACE:
			if (mCurrentFieldValue.length() > 1)
				mCurrentFieldValue.setLength(mCurrentFieldValue.length()-1);
			else {
				mCurrentFieldValue.setLength(0);
			}
			break;
		case KeyPad.KEY_PREVIOUS:
			showPrevious();
			break;
		case KeyPad.KEY_NEXT:
			showNext();
			break;
		default:
			if (mCurrentFieldValue.length() < mCurrentFieldLenght){
				mCurrentFieldValue.append(key);
			}
		}
		if (mCurrentTextView != null) {
			int v = (int)Util.asDouble(mCurrentFieldValue.toString());
			mCurrentFieldValue.setLength(0);
			mCurrentFieldValue.append(v+"");
			mCurrentTextView.setText(v+"");
		}
		
		updateDependentView();
	}	
}