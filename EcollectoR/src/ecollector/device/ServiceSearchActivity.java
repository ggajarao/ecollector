package ecollector.device;

import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.WindowManager;
import ecollector.device.view.FragmentViewManager;

public class ServiceSearchActivity extends FragmentViewManager {

	@Override
	protected Fragment getHomeView() {
		return new ServiceSearchFilters(this);
	}

	@Override
	public void onAttachedToWindow() {
		// Disables home button and back button
		// Ref: http://stackoverflow.com/questions/3898876/how-to-disable-the-home-key
		super.onAttachedToWindow();
		//this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD);
		// allowing home button as it is showing title bar when 
		// an activity is resumed
		// Ref: http://stackoverflow.com/questions/7485219/set-window-full-screen-atribute-after-resume-on-android
	}

	int backButtonCounter = 0;
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// Disables home button and back button
		// Ref: http://stackoverflow.com/questions/3898876/how-to-disable-the-home-key
		if(keyCode == KeyEvent.KEYCODE_HOME)
		{
			Log.i("Home Button","Home button disabled");
		} else if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			Log.i("Home Button","back button disabled");
			backButtonCounter++;
			if (backButtonCounter > 2) {
				super.finish();
			}
		}
		return false;
	}
}