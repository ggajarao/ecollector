package ecollector.device;

import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import ecollector.device.db.EcollectorDB;
import ecollector.device.view.FragmentView;
import ecollector.device.view.ViewManager;
import ecollector.device.view.input.KeyPad;
import ecollector.device.view.input.KeyPadListener;
import ecollector.device.view.input.NumericNavKeyPad;

public class PrNumberSetupView extends FragmentView implements KeyPadListener {
	
	private ViewManager mViewManager;
	private NumericNavKeyPad mNumericNavKeyPad;
	private TextView mNextPrNumberTv;
	private long mDbNextPrNumber;
	private StringBuffer mNextPrNumber = new StringBuffer();
	
	private static final int F_NEXT_PR_NUMBER = 0;
	
	private StringBuffer mCurrentFieldValue;
	private int mCurrentFieldLenght;
	private TextView mCurrentTextView;
	private int mCurrentFocusField;
	
	private LinearLayout mView;
	private TextView mMessageTextTv;
	/*public PrNumberSetupView() {
		
	}*/
	public PrNumberSetupView(/*ViewManager viewManager*/) {
		//mViewManager = viewManager;
		//initNextPrNumber();
	}
	
	private void initNextPrNumber() {
		mDbNextPrNumber = EcollectorDB.getNextRecieptNo(mViewManager.getActivity());
		mNextPrNumber = 
			new StringBuffer(""+mDbNextPrNumber);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mViewManager = getFragmentViewManager();
		initNextPrNumber();
		
		mView = (LinearLayout)inflater.inflate(R.layout.pr_number_setup_view, null);
		mMessageTextTv = (TextView)mView.findViewById(R.id.messageText);
		mMessageTextTv.setText("PR Number Setup");
		
		mNextPrNumberTv = (TextView)mView.findViewById(R.id.nextPrNumberTv);
		
		registerEventHandlers(mNextPrNumberTv,F_NEXT_PR_NUMBER);
		
		mNumericNavKeyPad = new NumericNavKeyPad("Cancel","Ok");
		mNumericNavKeyPad.setNextEnabled(false);
		mViewManager.showKeyPad(mNumericNavKeyPad);
		
		
		// default focus
		final Handler h = new Handler();
		(new Thread(
			new Runnable() {
				public void run() {
					h.postDelayed(new Runnable(){
						@Override
						public void run() {
							setFocus(F_NEXT_PR_NUMBER);
						}
					}, 10);
				}
			}
		)).start();
		
		updateDependentView();
		
		return mView;
	}
	
	private void registerEventHandlers(TextView tv, final int focusField) {
		tv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setFocus(focusField);
			}
		});
	}
	
	private void updateDependentView() {
		mNextPrNumberTv.setText(mNextPrNumber+"");
		setFocus(mCurrentFocusField);
		updateNextButtonState();
	}
	
	private void setFocus(int focusField) {
		unHighlightAll();
		mCurrentFocusField = focusField;
		switch (focusField) {
		case F_NEXT_PR_NUMBER:
			focusBackground(mNextPrNumberTv);
			mCurrentFieldValue = mNextPrNumber;
			mCurrentFieldLenght = 5;
			mCurrentTextView = mNextPrNumberTv;
			break;
		default:
			mCurrentFieldValue = null;
			mCurrentFieldLenght = 0;
			mCurrentTextView = null;
		}
	}
	
	private void focusBackground(View tv) {
		AnimationDrawable background = 
			Util.getInputFieldFocusStyleAnimated(mViewManager);
		if (tv.equals(mNextPrNumberTv)) {
			if (!isNextPrNumberValid()) {
				background = Util.getInputFieldFocusErrorStyleAnimated(
						mViewManager);
			}
		}
		tv.setBackgroundDrawable(background);
		((AnimationDrawable)tv.getBackground()).start();
	}
	
	private boolean isNextPrNumberValid() {
		int newPrNumber = Util.asInteger(mNextPrNumber.toString()); 
		return mDbNextPrNumber < newPrNumber && newPrNumber > 0;
	}

	private void unHighlightAll() {
		Drawable background = 
			Util.getInputFieldStyle(mViewManager);
		if (!isNextPrNumberValid()) {
			background = Util.getInputFieldErrorStyle(mViewManager);
		}
		mNextPrNumberTv.setBackgroundDrawable(background);
	}
	
	private void updateNextButtonState() {
		if (isNextPrNumberValid()) {
			mNumericNavKeyPad.setNextEnabled(true);
		} else {
			mNumericNavKeyPad.setNextEnabled(false);
		}
	}
	
	private void showPrevious() {
		mViewManager.showPrevious();
	}
	
	private void showNext() {
		if (!isNextPrNumberValid()) {
			return;
		}
		int value = Util.asInteger(mNextPrNumber.toString()); 
		EcollectorDB.setNextRecieptNo(value, mViewManager.getActivity());
		showPrevious();
	}

	public void onKeyClick(char key) {
		if (mCurrentFieldValue == null) return;
		switch (key) {
		case KeyPad.KEY_DEL:
			mCurrentFieldValue.setLength(0);
			break;
		case KeyPad.KEY_BACKSPACE:
			if (mCurrentFieldValue.length() > 1)
				mCurrentFieldValue.setLength(mCurrentFieldValue.length()-1);
			else {
				mCurrentFieldValue.setLength(0);
			}
			break;
		case KeyPad.KEY_PREVIOUS:
			showPrevious();
			break;
		case KeyPad.KEY_NEXT:
			showNext();
			break;
		default:
			if (mCurrentFieldValue.length() < mCurrentFieldLenght){
				mCurrentFieldValue.append(key);
			}
		}
		if (mCurrentTextView != null) {
			int v = (int)Util.asInteger(mCurrentFieldValue.toString());
			mCurrentFieldValue.setLength(0);
			mCurrentFieldValue.append(v+"");
			mCurrentTextView.setText(v+"");
		}
		
		updateDependentView();
	}
}
