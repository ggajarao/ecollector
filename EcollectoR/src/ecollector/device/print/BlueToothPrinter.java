package ecollector.device.print;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

import org.apache.http.entity.mime.MinimalField;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.ContentResolver;
import android.util.Log;
import ecollector.device.PrintBillAction;
import ecollector.device.db.EcollectorDB;

public class BlueToothPrinter {

	private static final String TAG = "BT-Printer";

	private static BlueToothPrinter sPrinter;
	
	private BluetoothAdapter mBluetoothAdapter;
	private BluetoothSocket mBtSocket = null;
	private OutputStream mOutStream = null;
	private InputStream mInputStream = null;
	private boolean mIsInitialized = false;
	private String mPrinterAddress;
	private long mLastPrintTs = 0;
	private BlueToothPrinter(String printerAddress) {
		mPrinterAddress = printerAddress;
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	}
	
	public boolean isPrintSupported() {
		if (mBluetoothAdapter == null) {
			return false;
		} else
		{
			return true;
		}
	}
	
	public static BlueToothPrinter getInstance(String printerAddress) {
		if (sPrinter == null) {
			synchronized (BlueToothPrinter.class) {
				if (sPrinter != null) return sPrinter;
				sPrinter = new BlueToothPrinter(printerAddress);
			}
		}
		return sPrinter;
	}
	
	public static void releasePrinter() {
		if (sPrinter != null) {
			sPrinter.releaseConnections();
		}
		sPrinter = null;
	}
	
	private void releaseConnections() {
		if (mBtSocket != null) {
			try {
				mBtSocket.close();
			} catch (IOException e) {
				Log.i(TAG, "Error while closing printer soc",e);
			}
		}
		
		if (mOutStream != null) {
			try {
				mOutStream.close();
			} catch (IOException e) {
				Log.i(TAG, "Error while closing bt os",e);
			}
		}
		
		if (mInputStream != null) {
			try {
				mInputStream.close();
			} catch (IOException e) {
				Log.i(TAG, "Error while closing bt is",e);
			}
		}
			
	}
	
	public static void initPrinter(ContentResolver contentResolver) {
		String[] cfg = EcollectorDB.fetchPrinterConfig(contentResolver);
		if (cfg != null && cfg.length == 2) {
			BlueToothPrinter.getInstance(cfg[1]);
		}
	}
	
	private int initialize() {
		mIsInitialized = false;
		
		if (!mBluetoothAdapter.isEnabled()) {
			return PrintBillAction.PRINT_TURN_ON_BLUETOOTH;
		}
		
		//String mPrinterAddress = null;
		//Set<BluetoothDevice> bondedDevices = mBluetoothAdapter.getBondedDevices();
		/*for (BluetoothDevice bondedDevice : bondedDevices) {
			Log.i(TAG, "VVV: "+bondedDevice.getName() +" @: "+bondedDevice.getAddress());
			if (bondedDevice.getName().equalsIgnoreCase("ANALOGICS")) {
				address = bondedDevice.getAddress();
			}
		}*/
		
		if (mPrinterAddress == null) {
			return PrintBillAction.PRINT_DEVICE_NOT_PAIRED;
		}
		
		BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mPrinterAddress);
		try {
			mBtSocket = device.createRfcommSocketToServiceRecord(
					UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
		} catch (IOException e) {
			return PrintBillAction.PRINT_PRINTER_NOT_FOUND;
		}
		
		mBluetoothAdapter.cancelDiscovery();
		try {
			mBtSocket.connect();
			Log.i(TAG, "BT Device Connected");
		} catch (IOException e) {
			try {
				mIsInitialized = false;
				mBtSocket.close();
			} catch (IOException e2) {
				Log.e(TAG, 
					"Unable to close socket during connection failure", e2);
			}
			return PrintBillAction.PRINT_PRINTER_NOT_FOUND;
		}
		
		try {
			mOutStream = mBtSocket.getOutputStream();
		} catch (IOException e) {
			Log.e(TAG, "Output stream creation failed.", e);
			return PrintBillAction.PRINT_PRINTER_NOT_FOUND;
		}
		
		hookInputStream();
		
		mIsInitialized = true;
		return -1;
	}
	
	public int print(byte[] buffer) {
		waitForPrintingToComplete();
		if (!mIsInitialized) {
			int code = initialize();
			if (code != -1) {
				return code;
			}
		}
		try {
			mOutStream.write(buffer);
		} catch (IOException e) {
			mIsInitialized = false;
			Log.e(TAG, "Printing failed", e);
			return PrintBillAction.PRINT_COMPLETED;
		}
		return PrintBillAction.PRINT_COMPLETED;
	}
	
	public int printBill(final byte[] data) {
		Thread t = new Thread(new Runnable(){
			@Override
			public void run() {
				sPrinter.print(data);
			}
		});
		t.start();
		return -1;
		//return sPrinter.print(data);
	}
	
	private static Thread sInputStreamSucker;
	
	public void waitForPrintingToComplete() {
		Thread t = new Thread(new Runnable(){
			public void run() {
				while (true) {
					long currentMillies = System.currentTimeMillis();
					long delta = currentMillies - mLastPrintTs;
					if (delta > 1000) {
						return;
					}
				}
			}
		});
		t.start();
		try {
			t.join();
			Log.i(TAG, "Waiting completed... Printing next job");
		} catch (InterruptedException e) {
			Log.i(TAG,"Error witing while print completion",e);
		}
	}
	
	private void hookInputStream() {
		if (sInputStreamSucker == null || 
			 (sInputStreamSucker != null && !sInputStreamSucker.isAlive())) {
			sInputStreamSucker = new Thread(new Runnable(){
				public void run() {
					try {
						mInputStream = mBtSocket.getInputStream();
						byte[] b = new byte[1024];
						int r = 0;
						while ( (r = mInputStream.read(b)) != -1 ) {
							Log.i(TAG,"Printer IS >>>("+r+")" + asHex(b,r));
							mLastPrintTs = System.currentTimeMillis();
						}
					} catch (IOException e) {
						Log.i(TAG, "Failed to get inputstream from printer",e);
					}
				}

				private String asHex(byte[] b, int r) {
					final char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
				    char[] hexChars = new char[r * 2];
				    int v;
				    for ( int j = 0; j < r; j++ ) {
				        v = b[j] & 0xFF;
				        hexChars[j * 2] = hexArray[v >>> 4];
				        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
				    }
				    return new String(hexChars);
				}
			});
			sInputStreamSucker.start();
		}
	}
}