package ecollector.device;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.util.Log;
import ecollector.common.FormatUtils;
import ecollector.common.ServiceColumns;
import ecollector.common.SummaryReportColumns;
import ecollector.device.db.EcollectorDB;
import ecollector.device.view.ViewManager;
import ecollector.device.view.action.ServicePushAction;

public class BillCollectionBean {
	//public Cursor mCursor;
	
	public final long _id;
	public String usc_No;
	public String sc_No;
	public String name;   
	public String address;
	public String category;
	public String group_x; 
	public String ero;
	public String section;
	public String distribution;
	private String arrears;
	public String mVolatilve_arrears;
	public String cmd;
	public String mVolatile_cmd_decided; // this is derived based on mIsExcludeArriearsSelected
	public StringBuffer mVolatile_cmd_collected = new StringBuffer();
	public String acd;
	public String sd_available;
	public String billed_months;  
	public String dc_date;
	public String rc_code;
	public String machine_code;
	public String pefctdt;
	public String agl_amount;
	public String agl_services;
	//private List<String> aglServicesList;
	public String sub_category;
	
	//public String receipt_number;
	public String collection_date;
	public String collection_time;
	public String arrears_n_demand;
	public String rc_collected;
	public StringBuffer mVolatile_rc_collected = new StringBuffer();
	public StringBuffer acd_collected = new StringBuffer();
	public StringBuffer others_collected = new StringBuffer();
	public String last_paid_receipt_no;
	public String last_paid_date;
	public String last_paid_amount;
	public String tc_seal_no;
	private String remarks;
	public String remarks_amount;
	private String generatedRemarks;
	public String amount_collected;
	
	public StringBuffer mVolatileCollectionDelta = new StringBuffer();
	public StringBuffer mVolatileNew_arrears = new StringBuffer();
	
	/*This is not used to store in db, volatile !!*/
	public String exemptor;
	
	public String _PR_IDS;
	public String _SC_NO;
	public String _N_USC_NO;
	//public final String _PUSH_STATE;
	
	private List<PrBean> prList = new ArrayList<PrBean>();
	/*public PrBean mAglPr = null;
	public PrBean mNonAglPr = null;*/
	
	public String mVolatileCollectableAmount;
	public StringBuffer mVolatileAmountCollected = new StringBuffer();
	
	private boolean mIsCollectionFrozen = false;
	public boolean mIsCursorEmpty = false;
	
	private boolean mIsArrearsSelectedToAdd = true;
	
	private boolean mIsForOnlyRc = false;
	
	private boolean mIsForOnlyAcd = false; 
	
	private boolean mIsDemandAdded = false;
	
	private ViewManager mViewManager;
	public String mVolatile_bill_months;
	
	private boolean mIsForPrintDuplicatePr;
	
	private String mPrintDuplicatePrNumber;

	public BillCollectionBean(long id, ViewManager viewManager) {
		mViewManager = viewManager;
		_id = id;
		init();
	}
	
	public void startOver() {
		if (!isDemandAdded()) {
			init();
		}
	}
	
	private void init() {
		initData();
		resetArrearsSelectedToAddFlag();
		
		// AGL Services: Observed in some services that AGL demand exists without 
		// AGL Service Numbers. In such cases we should discard AGL amount and 
		// should not include in collectable amount. Also AGL PR should not 
		// be genrated
		if (this.agl_services == null || this.agl_services.trim().length() == 0) {
			this.agl_amount = "0";
		}
	}
	
	private void initData() {
		Cursor cursor = null;
		try {
			cursor = EcollectorDB.findServiceById(mViewManager.getActivity(), _id);
			if (!cursor.moveToFirst()) {
				mIsCursorEmpty = true;
			}
			_N_USC_NO = cursor.getString(cursor.getColumnIndex(ServiceColumns._N_USC_NO));
			//_PUSH_STATE = mCursor.getString(mCursor.getColumnIndex(ServiceColumns._PUSH_STATE));
			_SC_NO = cursor.getString(cursor.getColumnIndex(ServiceColumns._SC_NO));
			_PR_IDS = cursor.getString(cursor.getColumnIndex(ServiceColumns._PR_IDS));
			
			/* Init field members */
			//_id = mCursor.getLong(mCursor.getColumnIndex(ServiceColumns._ID));
			usc_No = cursor.getString(cursor.getColumnIndex(ServiceColumns.USC_NO));
			sc_No = cursor.getString(cursor.getColumnIndex(ServiceColumns.SC_NO));
			name = cursor.getString(cursor.getColumnIndex(ServiceColumns.NAME));   
			address = cursor.getString(cursor.getColumnIndex(ServiceColumns.ADDRESS));
			category = cursor.getString(cursor.getColumnIndex(ServiceColumns.CATEGORY));
			group_x = cursor.getString(cursor.getColumnIndex(ServiceColumns.GROUP_X)); 
			ero = cursor.getString(cursor.getColumnIndex(ServiceColumns.ERO));
			section = cursor.getString(cursor.getColumnIndex(ServiceColumns.SECTION));
			distribution = cursor.getString(cursor.getColumnIndex(ServiceColumns.DISTRIBUTION));
			acd = cursor.getString(cursor.getColumnIndex(ServiceColumns.ACD));
			sd_available = cursor.getString(cursor.getColumnIndex(ServiceColumns.SD_AVAILABLE));
			billed_months = cursor.getString(cursor.getColumnIndex(ServiceColumns.BILLED_MONTHS));  
			dc_date = cursor.getString(cursor.getColumnIndex(ServiceColumns.DC_DATE));
			rc_code = cursor.getString(cursor.getColumnIndex(ServiceColumns.RC_CODE));
			machine_code = cursor.getString(cursor.getColumnIndex(ServiceColumns.MACHINE_CODE));
			pefctdt = cursor.getString(cursor.getColumnIndex(ServiceColumns.PEFCTDT));
			agl_amount = cursor.getString(cursor.getColumnIndex(ServiceColumns.AGL_AMOUNT));
			agl_services = cursor.getString(cursor.getColumnIndex(ServiceColumns.AGL_SERVICES));
			sub_category = cursor.getString(cursor.getColumnIndex(ServiceColumns.SUB_CATEGORY));
			/*receipt_number = mCursor.getString(mCursor.getColumnIndex(ServiceColumns.RECEIPT_NUMBER));
			collection_date = mCursor.getString(mCursor.getColumnIndex(ServiceColumns.COLLECTION_DATE));
			collection_time = mCursor.getString(mCursor.getColumnIndex(ServiceColumns.COLLECTION_TIME));
			arrears_n_demand = mCursor.getString(mCursor.getColumnIndex(ServiceColumns.ARREARS_N_DEMAND));
			acd_collected = mCursor.getString(mCursor.getColumnIndex(ServiceColumns.ACD_COLLECTED));
			last_paid_receipt_no = mCursor.getString(mCursor.getColumnIndex(ServiceColumns.LAST_PAID_RECEIPT_NO));
			last_paid_date = mCursor.getString(mCursor.getColumnIndex(ServiceColumns.LAST_PAID_DATE));
			last_paid_amount = mCursor.getString(mCursor.getColumnIndex(ServiceColumns.LAST_PAID_AMOUNT));
			tc_seal_no = mCursor.getString(mCursor.getColumnIndex(ServiceColumns.TC_SEAL_NO));
			remarks = mCursor.getString(mCursor.getColumnIndex(ServiceColumns.REMARKS));
			remarks_amount = mCursor.getString(mCursor.getColumnIndex(ServiceColumns.REMARKS_AMOUNT));*/
			
			arrears = cursor.getString(cursor.getColumnIndex(ServiceColumns.ARRIERS));
			cmd = cursor.getString(cursor.getColumnIndex(ServiceColumns.CMD));
			resetCmdCollected();
			
			//aglServicesList = Util.parseAglServices(agl_services);
			
			//TODO: IF SERVICES ARE LOADED AFRESH, THEN
			// THIS LOGIC MUST BE BASED ON WHETHER THE
			// EXISTING PRS ARE GENERATED BASED ON 
			// CURRENT SERVICE DATA OR OLDER SERVICE DATA
			//mIsCollectionFrozen = this.getPrIds().size() > 0;
			mIsCollectionFrozen = false;
			
			if (isAmountCollected()) {
				prList = fetchPrBeans(this.getPrIds());
			}
		} finally {
			if (cursor != null) cursor.close();
		}
	}
	
	private String mNegativeArrearsAdded;
	public String getNegativeArrearsAdded() {
		return mNegativeArrearsAdded;
	}
	
	
	public String getArrears() {
		int iArrears = (int)Util.asDouble(arrears);
		if (iArrears > 0) {
			mNegativeArrearsAdded = "0";
			return arrears;
		}
		
		if (mIsArrearsSelectedToAdd) {
			mNegativeArrearsAdded = arrears;
			return arrears;
		} else {
			return "0";
		}
	}
	public String getArrearsSkipCondition() {
		return arrears;
	}
	
	String getFinalRemarks() {
		if (this.isDemandAdded()) {
			return EcollectorDB.REMARKS_DEMAND_ADDED+this.remarks;
		} else {
			return remarks;
		}
	}
	
	public String getLastRemark() {
		return this.remarks;
	}
	
	public void setLastRemark(String remark) {
		this.remarks = remark;
	}
	
	public List<PrBean> fetchPrBeans(List<String> prNumbers) {
		List<PrBean> prBeans = new ArrayList<PrBean>();
		Cursor c = null;
		try {
			c = EcollectorDB.findPrsByIds(mViewManager.getActivity(),
					prNumbers);
			if (c == null || c.getCount() == 0) {
				Log.i("BillCollectionBean","No Prs to initialize for uscNo: "+this.usc_No);
				return Collections.emptyList();
			}
			c.moveToFirst();
			do {
				prBeans.add(new PrBean(this,c));
			} while (c.moveToNext());
		} finally {
			if (c != null) c.close();
		}
		return prBeans;
	}

	public boolean isAmountCollected() {
		return mIsCollectionFrozen;
	}
	
	public void save() {
		List<String> prNumbers = this.savePrs(mViewManager.getActivity());
		if (prNumbers.isEmpty()) {
			Log.i("BillCollectionBean","No prs for this service. Could not proceed saving, uscNo: "+this.usc_No);
			return;
		}
		
		// store recipt id in service record
		List<String> prIdList = this.getPrIds();
		prIdList.addAll(prNumbers);
		String prIds = FormatUtils.listAsCsv(prIdList);
		
		ContentValues serviceValues = new ContentValues();
		serviceValues.put(ServiceColumns._PR_IDS, prIds);
		
		EcollectorDB.saveServiceDetails(this._id+"", mViewManager.getActivity(), 
				serviceValues);
		
		ContentValues values = new ContentValues();
		values.put(SummaryReportColumns.TOTAL_PR_COUNT, prNumbers.size());
		values.put(SummaryReportColumns.TOTAL_PR_TO_PUSH_COUNT, prNumbers.size());
		values.put(SummaryReportColumns.TOTAL_COLLECTION, this.amount_collected);
		EcollectorDB.bumpSummaryReportPrStats(mViewManager.getActivity().getContentResolver(), values);
		
		initData();
		
		/*ServicePushAction a = new ServicePushAction(mViewManager.getActivity());
		a.onClick(null);*/
	}
	
	public List<String> getPrIds() {
		return FormatUtils.csvAsList(this._PR_IDS);
	}

	private List<String> savePrs(Context context) {
		List<String> prNumbers = new ArrayList<String>();
		if (!this.isAmountCollected()) {
			this.computeBill();
			for (PrBean prBean : prList) {
				prNumbers.add(prBean.save(context));
			}
		}
		return prNumbers;
	}
	
	public byte[] getPrintBuffer() {
		StringBuffer s = new StringBuffer();
		boolean firstPr = false;
		for (PrBean prBean : prList) {
			if (firstPr) {
				// When printing multiple prs, 
				// we should print a blank line 
				// as it would when printed multiple times 
				// ( printer is adding a blank like 
				//   before each print request )
				s.append(PrintUtil.BLANK_LINE);
			}
			s.append(prBean.getPrintBuffer(mViewManager.getActivity().getApplicationContext()));
			firstPr = true;
			
		}
		return s.toString().getBytes();
	}
	
	public boolean isCollectedAmountValid() {
		int collectableAmount = Util.asInteger(mVolatileCollectableAmount.toString());
		int collectedAmount = Util.asInteger(mVolatileAmountCollected.toString());
		boolean withRemarks = !TextUtils.isEmpty(this.remarks);
		//int arrears = (int)Util.asDouble(this.getArrears());
		//int cmd = (int)Util.asDouble(this.cmd);
		int sigmaArrearsNCmd = (int)Util.asDouble(arrears_n_demand);//arrears + cmd;
		
		// If areas+cmd is negative or zero, then allow to 
		// enter any collected amount
		/*if (sigmaArrearsNCmd <= 0) {
			if (collectedAmount > 0) {
				return true;
			} else {
				return false;
			}
		} else */
		if (withRemarks) {
			if (EcollectorDB.REMARKS_PART_PAYMENT.equalsIgnoreCase(this.getLastRemark())) {
				if (collectedAmount > 0) {
					int aglAmount = Util.asInteger(this.agl_amount);
					if (!this.isAglService()) {
						return true;
					} else if (collectedAmount >= aglAmount ){
						return true;
					} else {
						return false;
					}
				} else {
					return false;
				}
			}
			if (this.isDueDateCrossed()) {
				// Past Due date
				if (collectedAmount >= collectableAmount) {
					return true;
				} else {
					return false;
				}
			} else {
				if ((collectableAmount - 50) <=  collectedAmount) {
					return true;
				} else {
					return false;
				}
			}
		} if (this.isAglService()) {
			int aglAmount = Util.asInteger(this.agl_amount);
			if (collectedAmount >= aglAmount ){
				return true;
			} else {
				return false;
			}
		} else if (sigmaArrearsNCmd <= 0) {
			if (collectedAmount > 0) {
				return true;
			} else {
				return false;
			}
		} else {
			/*no remarks*/
			if (((collectableAmount - 10) <=  collectedAmount) && collectedAmount != 0) {
				return true;
			} else {
				return false;
			}
		}
	}

	private boolean isDueDateCrossed() {
		long dueDate = Util.trimTime(Util.asDate(this.dc_date).getTime());
		long dateNow = Util.trimTime(Calendar.getInstance().getTime().getTime());
		return dateNow > dueDate;
	}
	
	
	public void prepareForCollection() {
		this.resetCollectionDelta();
		
		int iCmd = (int)Util.asDouble(this.cmd);
		int iArrears = (int)Util.asDouble(this.getArrearsSkipCondition());
		if (mIsArrearsSelectedToAdd) {
			this.mVolatile_cmd_decided = this.cmd;
			this.resetCmdCollected();
		} else if ((iCmd + iArrears) == 0) {
			this.mVolatile_cmd_decided = "0";
			this.resetCmdCollected();
		} else {
			this.mVolatile_cmd_decided = this.cmd;
			this.resetCmdCollected();
		}
		
		/*if ((iCmd + iArrears) == 0 && mIsArrearsSelectedToAdd) {
			this.mVolatile_cmd_decided = this.cmd;
			this.resetCmdCollected();
		} else {
			this.mVolatile_cmd_decided = "0";
			this.resetCmdCollected();
		}*/
		
		this.computeArrearsNDemand();

		// set acd_collected from acd.
		if (this.acd_collected.length() == 0) {
			this.acd_collected.append(this.acd);
		}
		computeRCBasedOnDependents();
		computeCollectableAmount() ;
		mVolatileAmountCollected.setLength(0);
		// To prevent unconscious navigation away from bill collection
		// screen, we set the value to zeor to make the user enter
		// the collectable amount manually.
		//mVolatileAmountCollected.append(mVolatileCollectableAmount);
		mVolatileAmountCollected.append("0");
	}
	
	public void computeCollectableAmount() {
		int collectableAmount = 
			(int)Math.ceil(
					Util.sigma(new String[]{
						arrears_n_demand,
						rc_collected,
						others_collected.toString(),
						acd_collected.toString(),
						agl_amount
				}));
		mVolatileCollectableAmount = collectableAmount + "";
	}
	
	public void computeBill() {
		collection_date = Util.getCurrentDate();
		collection_time = Util.getCurrentTime();
		
		if (this.isForOnlyRc() || this.isForOnlyAcd()) {
			amount_collected = mVolatileAmountCollected.toString();
			createPrs();
		} else if (isAmountCollected()) {
			return;
		} else {
			computeBillAndGeneratePrs();
		}
	}
	
	private void computeBillAndGeneratePrs() {
		resetCollectionDelta();
		
		computeArrearsNDemand();
		computeRCBasedOnDependents();
		computeCollectableAmount();
		
		computeCollectionDelta();
		
		amount_collected = mVolatileAmountCollected.toString();
		
		createPrs();
	}

	private void createPrs() {
		this.prList.clear();
		PrBean nonAglPr = new PrBean(this);
		double dAglAmount = Util.asDouble(agl_amount);
		if (dAglAmount > 0) {
			
			nonAglPr.agl_amount = "0";
			nonAglPr.agl_services = "";
			double newCollectedAmount = Util.asDouble(nonAglPr.amount_collected);
			newCollectedAmount = newCollectedAmount - dAglAmount;
			nonAglPr.amount_collected = newCollectedAmount + "";
			
			PrBean aglPr = new PrBean(this);
			aglPr.acd_collected = "0";
			aglPr.arrears_n_demand = "0";
			aglPr.amount_collected = dAglAmount + "";
			aglPr.cmd_collected = "0";
			aglPr.new_arrears = "0";
			aglPr.others_collected = "0";
			aglPr.rc_collected = "0";
			aglPr.remarks_amount = "0";
			aglPr.collection_delta = "0";
			aglPr.remarks = EcollectorDB.REMARKS_AGL_COLLECTION;
			prList.add(aglPr);
		}
		
		double amountCollected = Util.asDouble(nonAglPr.amount_collected);
		if (amountCollected > 0) {
			prList.add(nonAglPr);
		}
	}
	
	private void computeCollectionDelta() {
		double dCollectableAmount = 
			Util.roundDec2(Util.asDouble(mVolatileCollectableAmount.toString()));
		double dCollectedAmount = 
			Util.roundDec2(Util.asDouble(mVolatileAmountCollected.toString()));
		double delta = dCollectedAmount - dCollectableAmount;
		
		mVolatileCollectionDelta.setLength(0);
		mVolatileCollectionDelta.append(
				delta);
		
		mVolatileNew_arrears.setLength(0);
		mVolatileNew_arrears.append(
			Util.roundDec2(
				Util.sigma(new String[]{
						mVolatileCollectionDelta.toString(),
						mVolatilve_arrears
			})));
		computeArrearsNDemand();
	}

	public void computeArrearsNDemand() {
		//if (TextUtils.isEmpty(mVolatilve_arrears)) {
			mVolatilve_arrears = this.getArrears();
		//}
		
		int iCmd = (int)Util.asDouble(this.cmd);
		int iArrears = (int)Util.asDouble(mVolatilve_arrears);
		if ((iCmd + iArrears) <= 0) {
			mVolatilve_arrears = "0";
		}
		
		/*if (!TextUtils.isEmpty(this.remarks)) {
			if (EcollectorDB.REMARKS_EXCLUDING_ARREAR.equalsIgnoreCase(this.remarks) ||
				EcollectorDB.REMARKS_EXCLUDING_DEMAND.equalsIgnoreCase(this.remarks) ||
				EcollectorDB.REMARKS_PART_PAYMENT.equalsIgnoreCase(this.remarks) ||
				EcollectorDB.REMARKS_RC_EXCLUDED.equalsIgnoreCase(this.remarks)) {
				mVolatilve_arrears = "0";
			}
		} else {
			///
		}*/
		
		arrears_n_demand = 
			Util.roundDec2(
			  Math.ceil(  // :ROUND: 		
				Util.sigma(new String[]{
						mVolatilve_arrears,
						mVolatile_cmd_collected.toString(),
						mVolatileCollectionDelta.toString()
					}))) + "";
	}
	
	public void computeRCBasedOnDependents() {
		if (!TextUtils.isEmpty(this.getLastRemark()) && 
			 (this.getLastRemark().equals(EcollectorDB.REMARKS_PART_PAYMENT) ||
			  this.getLastRemark().equals(EcollectorDB.REMARKS_RC_EXCLUDED) ||
			  this.getLastRemark().equals(EcollectorDB.REMARKS_ACD_COLLECTED) )) {
			rc_collected = "0";
			return;
		}
		
		//boolean isExcludeArrears = EcollectorDB.REMARKS_EXCLUDING_ARREAR.equals(this.getLastRemark());
		double iArrears = Util.asDouble(this.getArrears().toString());
		double arrearsNDemand = Util.asDouble(arrears_n_demand);
		
		boolean computeRc = false;
		if (iArrears >= 50) {
			computeRc = true;
		} else if (arrearsNDemand >= 50 && this.isDueDateCrossed()) {
			computeRc = true;
		}
		if (computeRc) {
			computeRcBasedOnCategories();
		} else {
			rc_collected = "0";
		}
	}

	private void computeRcBasedOnCategories() {
		if (this.isForOnlyAcd()) {
			rc_collected = "0";
			return;
		}
		if (category != null && category.equals("1") &&
				sub_category != null && sub_category.equals("01")) {
			rc_collected = "25";
		} else {
			rc_collected = "75";
		}
	}	

	public void resetCmdCollected() {
		this.mVolatile_cmd_collected.setLength(0);
		this.mVolatile_cmd_collected.append(this.mVolatile_cmd_decided);
	}

	public void resetRcCollected() {
		this.mVolatile_rc_collected.setLength(0);
		this.mVolatile_rc_collected.append(this.rc_collected);
	}
	
	public void resetCollectionDelta() {
		this.mVolatileCollectionDelta.setLength(0);
	}
	
	public boolean isCollectionDeltaExcess() {
		return (int)Util.asDouble(mVolatileCollectionDelta.toString()) > 0;
	}
	
	public boolean isCollectionDeltaLess() {
		return (int)Util.asDouble(mVolatileCollectionDelta.toString()) < 0;
	}
	
	public List<PrBean> getPrList() {
		return this.prList;
	}
	
	public boolean isArrearsSelectedToAdd() {
		return mIsArrearsSelectedToAdd;
	}
	
	public void toggleArrearsSelectedToAdd() {
		mIsArrearsSelectedToAdd = !mIsArrearsSelectedToAdd;
	}
	
	public void resetArrearsSelectedToAddFlag() {
		mIsArrearsSelectedToAdd = true;
	}

	public void prepareForOnlyRcCollection() {
		arrears = "0";
		cmd = "0";
		acd = "0";
		mVolatile_cmd_decided = "0";
		mVolatile_cmd_collected.setLength(0);
		mVolatile_cmd_collected.append("0");
		agl_amount = "0";
		others_collected.setLength(0);
		others_collected.append("0");
		mIsForOnlyRc = true;
		computeRcBasedOnCategories();
		mVolatileAmountCollected.setLength(0);
		mVolatileAmountCollected.append(this.rc_collected);
	}
	
	public void prepareForOnlyAcdCollection() {
		arrears = "0";
		cmd = "0";
		acd = this.acd; // :))  we need only ACD to move forward
		mVolatile_cmd_decided = "0";
		mVolatile_cmd_collected.setLength(0);
		mVolatile_cmd_collected.append("0");
		agl_amount = "0";
		others_collected.setLength(0);
		others_collected.append("0");
		mIsForOnlyAcd = true;
		mIsForOnlyRc = false;
		computeRcBasedOnCategories();
		mVolatileAmountCollected.setLength(0);
		mVolatileAmountCollected.append(this.rc_collected);
		this.setLastRemark(EcollectorDB.REMARKS_ACD_COLLECTED);
		
		this.prepareForCollection();
	}
	
	public void prepareForPrintDuplicatePr(String prNumber) {
		mIsForPrintDuplicatePr = true;
		mPrintDuplicatePrNumber = prNumber;
		
		List<String> prNumbers = new ArrayList<String>();
		prNumbers.add(mPrintDuplicatePrNumber);
		List<PrBean> prBeans = this.fetchPrBeans(prNumbers);
		if (prBeans.isEmpty()) return;
		
		PrBean prBean = prBeans.get(0);
		
		prBean.isForPrintDuplicatePr = true;
		
		mVolatileAmountCollected.setLength(0);
		mVolatileAmountCollected.append(prBean.amount_collected);
		
		this.prList = prBeans;
		this.arrears_n_demand = prBean.arrears_n_demand;
		this.rc_collected = prBean.rc_collected;
		this.acd = prBean.acd_collected;
	}
	
	public boolean isForPrintDuplicatePr() {
		return mIsForPrintDuplicatePr;
	}
	
	public boolean isForOnlyRc() {
		return mIsForOnlyRc;
	}
	
	private boolean isForOnlyAcd() {
		return mIsForOnlyAcd;
	}

	public void addNewCmd(String newCmd) {
		// New demand is moved into cmd field
		// before that old cmd is added to arrears.
		mIsDemandAdded = true;
		int iCmd = (int)Util.asDouble(this.cmd);
		int iArrears = (int)Util.asDouble(getArrears());
		this.arrears = (iCmd + iArrears) + "";
		this.cmd = (int)Util.asInteger(newCmd) + "";
	}
	
	public boolean isDemandAdded() {
		return mIsDemandAdded;
	}
	
	public boolean isAglService() {
		return Util.asInteger(this.agl_amount) > 0;
	}
	
	private int mExcludeArrearsCollectedAmmount = 0;
	private boolean mIsArrearsExcluded = false;
	public void excludeArrears(String excludeArrearsCollectedAmmount) {
		// Old collection amount is subtracted from Arrears
		// if delta is >= 50 then RC is calculated.
		int iArrears = (int)Util.asDouble(getArrears());
		int iExcludeArrearsCollectedAmmount = (int)Util.asDouble(excludeArrearsCollectedAmmount);
		
		// Idempotency. if this method is
		// called multiple times, we should not 
		// end up excluding arrears multiple times
		// unless oldCollectedAmount is different
		// than previous call. In that case (if oldCollectedAmount 
		// is different than that of previous call) we must
		// revert the exclusion by adding the previous
		// oldCollectedAmount back to Arrears, and exclude
		// current oldCollectionAmount from Arrears.
		if (mIsArrearsExcluded) {
			if (iExcludeArrearsCollectedAmmount != mExcludeArrearsCollectedAmmount) {
				iArrears = iArrears + mExcludeArrearsCollectedAmmount;
			} else {
				// Redundant call, so we should
				// not exclude arrears again.
				return;
			}
		}
		
		int newArrears = iArrears - iExcludeArrearsCollectedAmmount;
		this.arrears = newArrears + "";
		this.setLastRemark(EcollectorDB.REMARKS_EXCLUDING_ARREAR);
		this.remarks_amount = iExcludeArrearsCollectedAmmount+"";
		mIsArrearsExcluded = true;
		mExcludeArrearsCollectedAmmount = iExcludeArrearsCollectedAmmount;
	}

	public void excludeAcd(String excludeAcdCollectedAmount) {
		int iAcd = (int)Util.asDouble(this.acd);
		int iExcludeAcdCollectedAmount = Util.asInteger(excludeAcdCollectedAmount);
		if (iAcd == iExcludeAcdCollectedAmount) {
			this.remarks_amount = this.acd;
			this.acd = "0";
			this.acd_collected.setLength(0);
		}
		this.setLastRemark(EcollectorDB.REMARKS_EXCLUDING_ACD);
	}

	public void excludeDemand() {
		this.remarks_amount = this.cmd;
		this.cmd = "0";
		this.setLastRemark(EcollectorDB.REMARKS_EXCLUDING_DEMAND);
	}
}