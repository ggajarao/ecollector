package ecollector.device;

import java.util.List;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import ecollector.device.view.FragmentView;
import ecollector.device.view.ViewManager;
import ecollector.device.view.input.KeyPad;
import ecollector.device.view.input.KeyPadListener;
import ecollector.device.view.input.NavigationKeyPad;

public class BillDetailsView extends FragmentView implements KeyPadListener, PrintPreviewRenderer{
	private TextView mMessageText;
	private ViewManager mViewManager;
	private BillCollectionBean mBcBean;
	
	private LayoutInflater mInflater;
	private ViewGroup mPreviewItemParent;
	public BillDetailsView() {
		
	}
	public BillDetailsView(/*ViewManager viewManager, */BillCollectionBean bcBean) {
		//mViewManager = viewManager;
		mBcBean = bcBean;
		if (!mBcBean.isForPrintDuplicatePr()) {
			mBcBean.computeBill();
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mViewManager = getFragmentViewManager();
		
		View v = null;
		mInflater = inflater;
		
		View view = mInflater.inflate(R.layout.bill_details_view, null);
		
		TextView tv = (TextView)view.findViewById(R.id.arrearsNcmdAmtTv);
		tv.setText(mBcBean.arrears_n_demand);
		tv = (TextView)view.findViewById(R.id.acdAmountTv);
		tv.setText(mBcBean.acd);
		tv = (TextView)view.findViewById(R.id.rcAmountTv);
		tv.setText(mBcBean.rc_collected);
		tv = (TextView)view.findViewById(R.id.aglAmount1Tv);
		tv.setText(mBcBean.agl_amount);
		
		tv = (TextView)view.findViewById(R.id.sectionName);
		tv.setText(mBcBean.section);
		tv = (TextView)view.findViewById(R.id.distributionName);
		tv.setText(mBcBean.distribution);
		tv = (TextView)view.findViewById(R.id.uscNumber);
		tv.setText(mBcBean.usc_No);
		
		tv = (TextView)view.findViewById(R.id.messageText);
		tv.setText("Amount Collected: "+mBcBean.mVolatileAmountCollected);
		
		/*final ScrollView sv = (ScrollView)view.findViewById(R.id.scrollView);
		sv.postDelayed(new Runnable() { 
	        public void run() { 
	        	sv.fullScroll(sv.FOCUS_DOWN);
	        } 
		}, 0);*/
		
		ViewGroup prListView = (LinearLayout)view.findViewById(R.id.prList);
		
		List<PrBean> prBeans = mBcBean.getPrList();
		for (PrBean prBean : prBeans) {
			mPreviewItemParent = (ViewGroup) inflater.inflate(R.layout.pr_details_view, null);
			TextView titleTv = (TextView)mPreviewItemParent.findViewById(R.id.prTitle);
			titleTv.setText(prBean.isAglPr()?"AGL PR":"NON AGL PR");
			prBean.previewPr((PrintPreviewRenderer)this);
			prListView.addView(mPreviewItemParent);
		}
		
		String printLabel = "Save & Print";
		if (mBcBean.isForPrintDuplicatePr()) {
			printLabel = "Print";
		}
		NavigationKeyPad n = new NavigationKeyPad("Previous",printLabel);
		if (prBeans.size() == 0) {
			n.setNextEnabled(false);
		}
		mViewManager.showKeyPad(n);
		
		return view;
	}
	
	public Object addPreviewItem(String label, String value, String defaultValue) {
		View view = mInflater.inflate(R.layout.bill_item, null);
		mPreviewItemParent.addView(view);
		TextView tv = (TextView)view.findViewById(R.id.textView1);
		tv.setText(label);
		tv = (TextView)view.findViewById(R.id.textView2);
		if (value == null || value.trim().length()==0) {
			tv.setText(defaultValue);
		} else {
			tv.setText(value);
		}
		View separater = mInflater.inflate(R.layout.horizontal_line, null);
		mPreviewItemParent.addView(separater);
		
		if ("AMOUNT COLLD".equalsIgnoreCase(label) || 
			"NAME".equalsIgnoreCase(label) ||
			"SCNO".equalsIgnoreCase(label)) {
			Drawable itemHighlighter = (GradientDrawable) mViewManager.getActivity().getResources().getDrawable(R.drawable.item_highlighter);
			view.setBackgroundDrawable(itemHighlighter);
		}
		
		return view;
	}
	
	public void onKeyClick(char key) {
		switch (key) {
		case KeyPad.KEY_PREVIOUS:
			mViewManager.showPrevious();
			break;
		case KeyPad.KEY_NEXT:
			if (mBcBean.isAmountCollected() || mBcBean.isForPrintDuplicatePr()) {
				print();
			} else {
				onSaveAndPrint();
			}
			mViewManager.showHome();
			break;
		}
	}
	
	private void print() {
		//BlueToothPrinter.printBill(mMrBean);
		PrintBillAction pba = new PrintBillAction(mViewManager.getActivity(), mBcBean.getPrintBuffer());
		pba.onClick(null);
		
		/*PrintAction pa = new PrintAction(mViewManager.getActivity(), mBcBean.getPrintBuffer());
		pa.onClick(null);*/
	}
	
	private void onSaveAndPrint() {
		mBcBean.save();
		print();
	}
}
