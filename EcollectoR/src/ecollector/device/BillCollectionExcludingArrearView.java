package ecollector.device;

import java.util.Date;

import ecollector.device.view.ViewManager;

public class BillCollectionExcludingArrearView extends BillCollectionBasePRFormView {
	public BillCollectionExcludingArrearView() {
		
	}
	
	public BillCollectionExcludingArrearView(/*ViewManager viewManager, */BillCollectionBean bcBean) {
		super(/*viewManager,*/bcBean);
	}
	
	protected CharSequence getTitle() {
		return "Enter Old PR Details";
	}

	protected boolean isFormValid() {
		int prNumber = Util.asInteger(mOldPrNumber.toString());
		int prAmount = Util.asInteger(mOldPrAmount.toString());
		Date prDate = Util.getDateFromDp(mOldPrDateDp);
		Date currentDate = new Date();
		return prNumber > 0 && prAmount > 0 && prDate.before(currentDate);
	}
	
	protected void showNext() {
		mBcBean.excludeArrears(mOldPrAmount.toString());
		mViewManager.showView(new BillCollectionView(mViewManager,mBcBean));
	}
	
	protected void showPrevious() {
		mViewManager.showPrevious();
	}	
}