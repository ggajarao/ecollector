package ecollector.device;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import ecollector.device.db.EcollectorDB;
import ecollector.device.view.FragmentView;
import ecollector.device.view.ViewManager;
import ecollector.device.view.action.ImportDataAction;
import ecollector.device.view.action.ShowEcollector;
import ecollector.device.view.input.KeyPad;
import ecollector.device.view.input.KeyPadListener;
import ecollector.device.view.input.NavigationKeyPad;

public class SetupView extends FragmentView implements KeyPadListener {
	private static final String TAG = "SetupActivity";
	
	private ImportDataAction importDataAction = null;
	
	private ViewManager mViewManager;
	
	public SetupView() {
		
	}
	
	/*public SetupView(ViewManager viewManager) {
		mViewManager = viewManager;
	}*/
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mViewManager = getFragmentViewManager();
		LinearLayout view = (LinearLayout)inflater.inflate(R.layout.setup, null);
		
		Button checkForUpdatesBtn = (Button)view.findViewById(R.id.checkDataUpdatesBtn);
		checkForUpdatesBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				DeviceFilesView v = new DeviceFilesView(/*mViewManager*/);
				mViewManager.showView(v);
			}
		});
		/*this.importDataAction = new ImportDataAction(
				SetupView.this.getActivity().getContentResolver(),
				SetupView.DIALOG_IMPORT_PROGRESS,
				SetupView.this.importProgressHandler);
		importButton.setOnClickListener(this.importDataAction);*/
		
		Button printerSetupBtn = (Button)view.findViewById(R.id.printerSetupBtn);
		printerSetupBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				PrinterSetupView v = new PrinterSetupView(/*mViewManager*/);
				mViewManager.showView(v);
			}
		});
		
//		Button prepareStatsBtn = (Button)view.findViewById(R.id.prepareStatsBtn);
//		prepareStatsBtn.setOnClickListener(new OnClickListener() {
//			public void onClick(View view) {
//				prepareStats();
//				
//				/*PSRDeltaTicksAction p = new PSRDeltaTicksAction(mViewManager.getActivity());
//				p.onClick(null);*/
//				
//				// Only for Test purpose
//				/*try {
//					Util.printInetAddress();
//				} catch (Exception e) {
//					Log.i("SetupView", "error printing inet address",e);
//				}*/
//			}
//		});
		
		Button nextPrNumberBtn = (Button)view.findViewById(R.id.nextPrNumberBtn);
		nextPrNumberBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				PrNumberSetupView vf = new PrNumberSetupView(/*mViewManager*/);
				mViewManager.showView(vf);
			}
		});
		
		NavigationKeyPad n = new NavigationKeyPad("Home","Next");
		n.setNextEnabled(false);
		mViewManager.showKeyPad(n);
		return view;
	}
	
	private void prepareStats() {
		EcollectorDB.tuneDbForFasterInserts(mViewManager.getActivity());
		Toast.makeText(mViewManager.getActivity(), "Preparing Stats... Please wait.", Toast.LENGTH_LONG).show();
		EcollectorDB.prepareSummaryReport(mViewManager.getActivity());
		Toast.makeText(mViewManager.getActivity(), "Preparing Stats... Completed.", Toast.LENGTH_SHORT).show();
	}
	
	@Override
	public void onKeyClick(char key) {
		switch(key) {
		case KeyPad.KEY_PREVIOUS:
			ShowEcollector s = new ShowEcollector((Activity)mViewManager.getActivity());
			s.onClick(null);
			break;
		}
	}
}