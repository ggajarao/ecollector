package ecollector.device;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import ecollector.common.FormatUtils;
import ecollector.device.db.EcollectorDB;
import ecollector.device.db.SummaryReport;
import ecollector.device.view.action.HandlerActions;
import ecollector.device.view.action.Messinger;
import ecollector.device.view.action.PrPushMessageHandler;
import ecollector.device.view.action.Workers;

public class DashboardView {
	private static final String PUSH_PR_WORKER_NAME = "PUSH_PR_WORKER";
	
	private Activity mActivity;
	private ViewGroup mHostPanel;
	private TextView mDeviceNameTxt;
	private TextView mTotalCollectionTxt;
	private TextView mTotalServicesTxt;
	private TextView mTotalPrsTxt;
	private TextView mTotalReadyToPushTxt;
	private TextView mNotifyText;
	private TextView mExpiresOnText;
	private TextView mMarginText;
	
	private Button mOnDemandPushBtn;
	//private ViewGroup mNotifyPanel;
	private PushPrWorker mPushPrWorker;
	private int mTotalPrsToPush;
	private Handler mHandler = new PrPushMessageHandler() {
		protected void onMessage(String text) {
			updateUi(this);
		};
	};
	
	/*new Handler() {
		public void handleMessage(Message msg) {
			updateUi(msg);
		};
	};*/
	
	public DashboardView(Activity activity, ViewGroup hostPanel) {
		mActivity = activity;
		mHostPanel = hostPanel;
	}
	
	public void onCreate() {
		LayoutInflater layoutInflater = mActivity.getLayoutInflater();
		ViewGroup dashboardPnl = (ViewGroup)layoutInflater.inflate(R.layout.dashboard, null);
		mHostPanel.removeAllViews();
		mHostPanel.addView(dashboardPnl);
		
		mDeviceNameTxt = (TextView)dashboardPnl.findViewById(R.id.deviceNameTxt);
		mTotalCollectionTxt = (TextView)dashboardPnl.findViewById(R.id.collectionTxt);
		mTotalServicesTxt = (TextView)dashboardPnl.findViewById(R.id.totalServicesTxt);
        mTotalPrsTxt = (TextView)dashboardPnl.findViewById(R.id.totalPrsTxt);
        mTotalReadyToPushTxt = (TextView)dashboardPnl.findViewById(R.id.readyCount);
        mOnDemandPushBtn = (Button)dashboardPnl.findViewById(R.id.pushOnDemandBtn);
        //mNotifyPanel = (ViewGroup)dashboardPnl.findViewById(R.id.notifyPnl);
        mNotifyText = (TextView)dashboardPnl.findViewById(R.id.notifyText);
        mExpiresOnText = (TextView)dashboardPnl.findViewById(R.id.expiresOnTxt);
        mMarginText = (TextView)dashboardPnl.findViewById(R.id.marginTxt);
        
        mOnDemandPushBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				onDemandPush();
			}
		});
        
	}
	
	public void onResume() {
		mPushPrWorker = (PushPrWorker)Workers.registerWorker(PUSH_PR_WORKER_NAME, null);
        if (mPushPrWorker != null) {
        	mPushPrWorker.registerHandler(mHandler);
        }
        startUpdatingDashboard();
	}
	
	private void updatePushBtnState() {
    	if (mTotalReadyToPushCount > 0) {
    		if (mPushPrWorker != null && mPushPrWorker.isAlive()) {
    			mOnDemandPushBtn.setVisibility(View.GONE);
    		} else {
    			mOnDemandPushBtn.setVisibility(View.VISIBLE);
    		}
    	} else {
    		mOnDemandPushBtn.setVisibility(View.GONE);
    	}
    }
	
	private int mTotalReadyToPushCount;
    
    private boolean mStopUpdatingDashboard = false;
    private void stopUpdatingDashboard() {
    	mStopUpdatingDashboard = true;
    }
    
    private void startUpdatingDashboard() {
    	mStopUpdatingDashboard = false;
    	updateDashboard();
    }
    
    private void updateDashboard() {
    	if (mExpiresOnText == null || mTotalServicesTxt == null) return;
    	
    	if (mStopUpdatingDashboard) return;
    	
    	try {
    		prepareUi();
    	} finally {
    		mHandler.postDelayed(new Runnable(){
        		@Override
        		public void run() {
        			updateDashboard();
        		}
        	}, 1000*10);
    	}
    }
    
    private void prepareUi() {
    	SummaryReport summaryReport = 
    		EcollectorDB.fetchSummaryReport(mActivity.getContentResolver());
    	mTotalServicesTxt.setText(summaryReport.getmTotalServicesCount()+"");
    	mTotalPrsTxt.setText(summaryReport.getmTotalPrCount()+"");
    	mTotalReadyToPushCount = summaryReport.getmTotalReadyToPushServicesCount();
    	mTotalReadyToPushTxt.setText(mTotalReadyToPushCount+"");
    	
    	String marginText = "0";
    	Double marginAmount = summaryReport.getCollectionMargin(mActivity.getContentResolver());
    	if (marginAmount > 0) {
    		marginText = FormatUtils.amountFormat(marginAmount +"");
    	}
    	String collectionAmount = summaryReport.getTotalCollection();
    	collectionAmount = FormatUtils.amountFormat(collectionAmount);
    	mTotalCollectionTxt.setText(collectionAmount);
    	mMarginText.setText(marginText);
    	
    	mDeviceNameTxt.setText(EcollectorDB.fetchDeviceName(mActivity.getContentResolver()));
    	updatePushBtnState();
    	
    	Log.i("DashboardView","Fetching expiration details");
    	mExpiresOnText.setText(""+EcollectorDB.fetchExpirationDeltaTicksReadable(mActivity.getContentResolver()));
    }
    
    private void onDemandPush() {
    	if (mPushPrWorker == null || !mPushPrWorker.isAlive()) {
    		mPushPrWorker = new PushPrWorker(mActivity);
    		mPushPrWorker.registerHandler(mHandler);
    		mPushPrWorker.start();
    		updatePushBtnState();
    	}
    }
    
    public void onPause() {
    	if (mPushPrWorker != null) {
    		mPushPrWorker.unRegisterHandler();
    	}
    	stopUpdatingDashboard();
    }
    
    public void onDestroy() {
    	mActivity = null;
    	stopUpdatingDashboard();
    }
    
    private void updateUi(PrPushMessageHandler h) {
    	/*String notifyText = "";
    	boolean prepareUi = false;
    	switch (msg.arg1) {
    	case HandlerActions.PUSH_PR_STARTED:
    		//mNotifyPanel.removeAllViews();
    		notifyText = "Please wait...";
    		break;
    	case HandlerActions.DEVICE_PAIRING_REQUIRED:
    		notifyText = "Device is not paired !";
    		break;
    	case HandlerActions.PUSH_PR_NO_SERVICES_TO_PUSH:
    		notifyText = "Nothing to push";
    		break;
    	case HandlerActions.PUSH_PR_PREPARING_TO_PUSH:
    		mTotalPrsToPush = (Integer)msg.obj;
    		notifyText = "Preparing to push..."+mTotalPrsToPush+" PRs";
    		break;
    	case HandlerActions.PUSH_PR_PROGRESSING:
    		notifyText = ((Integer)msg.obj) + " PRs pushed out of "+mTotalPrsToPush;
    		break;
    	case HandlerActions.PUSH_PR_COMPLETED:
    		//notifyText = "Completed Pushing PRs";
    		prepareUi = true;
    		break;
    	case HandlerActions.NETWORK_UNAVAILABLE:
    		notifyText = "Internet not available";
    		prepareUi = true;
    		break;
    	case HandlerActions.NETWORK_ERROR:
    		notifyText = "Network error, "+(msg.obj==null?"":msg.obj.toString());
    		break;
    	case HandlerActions.OTHER_ERROR:
    		notifyText = msg.obj+". Failed, please try again";
    		prepareUi = true;
    		break;
    	}*/
    	
    	mNotifyText.setText(h.mNotifyText);
    	if (h.mPrepareUi) {
    		mHandler.postDelayed(new Runnable() {
        		public void run() {
        			mNotifyText.setText("");
        			prepareUi();
        		}
        	}, 1000);
    	}
    }
}

class PushPrWorker extends Thread implements Messinger {
	
	private final Integer LOCK = new Integer("0"); 
	private Handler mHandler;
	private Context mContext;
	public PushPrWorker(Context context) {
		mContext = context;
	}
	
	public void run() {
		EcollectorDB.pushPrsToCloud(mContext, (Messinger)this);
	}
	
	public void registerHandler(Handler h) {
		synchronized (LOCK) {
			mHandler = h;
		}
	}
	
	public void unRegisterHandler() {
		synchronized (LOCK) {
			mHandler = null;
		}
	}
	
	public void sendMessage(Message msg) {
		synchronized (LOCK) {
			if (mHandler != null) {
				mHandler.sendMessage(msg);
			}
		}
	}
}