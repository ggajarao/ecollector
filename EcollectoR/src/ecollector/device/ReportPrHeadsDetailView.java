package ecollector.device;

import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import ecollector.common.FormatUtils;
import ecollector.common.PrColumns;
import ecollector.common.ReportAbstractColumns;
import ecollector.device.db.EcollectorDB;
import ecollector.device.db.PrHeadsReport;
import ecollector.device.view.FragmentView;
import ecollector.device.view.ViewManager;
import ecollector.device.view.input.KeyPad;
import ecollector.device.view.input.KeyPadListener;
import ecollector.device.view.input.NavigationKeyPad;

public class ReportPrHeadsDetailView extends FragmentView implements KeyPadListener {
	private static final String TAG = "ReportAmountAbstractView";
	
	private ViewManager mViewManager;
	private ReportFilterBean mReportFilter;
	private ViewGroup mReportPanel;
	private PrHeadsReport mReport;
	
	public ReportPrHeadsDetailView() {
		
	}
	
	public ReportPrHeadsDetailView(ReportFilterBean reportFilter,
			ViewManager viewManager) {
		mReportFilter = reportFilter;
		//mViewManager = viewManager;
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mViewManager = getFragmentViewManager();
		LinearLayout view = (LinearLayout)inflater.inflate(R.layout.report_pr_heads_view, null);
		
		mReportPanel = (ViewGroup)view.findViewById(R.id.reportContentPane);
		
		/*View hLine = inflater.inflate(R.layout.horizontal_line, null);
		mReportPanel.addView(hLine);*/
		
		mReport = new PrHeadsReport(
				mReportFilter,
				mViewManager.getActivity().getContentResolver());
		
		String lastSection = "";
		String lastDistribution = "";
		ViewGroup secDistPanel = null;
		while (mReport.hasNext()) {
			Cursor c = mReport.next();
			String distribution = 
				c.getString(c.getColumnIndex(PrColumns.DISTRIBUTION));
			String section = 
				c.getString(c.getColumnIndex(PrColumns.SECTION));
			
			if (!lastSection.equalsIgnoreCase(section) ||
				!lastDistribution.equalsIgnoreCase(distribution)) {
				
				lastSection = section;
				lastDistribution = distribution;
				
				// AddSection, Distribution Item
				View secDistPane = 
					inflater.inflate(R.layout.report_pr_heads_section_distribution_item, null);
				mReportPanel.addView(secDistPane);
				
				TextView tv = (TextView)secDistPane.findViewById(R.id.rcCodeTv);
				String deviceName = "SCM Name: "+EcollectorDB.fetchDeviceName(mViewManager.getActivity().getContentResolver());
				tv.setText(deviceName);
				
				tv = (TextView)secDistPane.findViewById(R.id.dateRangeTv);
				String dateRange = "Between: "+Util.getReadableDate(mReportFilter.getFromDate())+" - "+Util.getReadableDate(mReportFilter.getToDate());
				tv.setText(dateRange);
				
				String sectionName = c.getString(c.getColumnIndex(ReportAbstractColumns.SECTION));
				String dist = c.getString(c.getColumnIndex(ReportAbstractColumns.DISTRIBUTION));
				
				tv = (TextView)secDistPane.findViewById(R.id.sectionTv);
				String secDistTitle = 
					"Sec: " + (sectionName==null?"N/A":sectionName) +
					((dist == null || dist.trim().length() == 0) ? "" : "  Dist: "+dist);
				
				tv.setText(secDistTitle);
				
				View hSpacer = inflater.inflate(R.layout.horizontal_line, null);
				mReportPanel.addView(hSpacer);
				
				secDistPanel = (ViewGroup)secDistPane.findViewById(R.id.sectionDistributionContentPane);
				
				preparePrHeadUi(secDistPanel,inflater);
			}
			preparePrHeadItemUi(c,secDistPanel, inflater);
		}
		
		NavigationKeyPad n = new NavigationKeyPad("Back","Print");
		n.setNextEnabled(false);
		mViewManager.showKeyPad(n);
		return view;
	}
	
	private void preparePrHeadUi(ViewGroup reportContentPane, 
			LayoutInflater inflater) {
		ViewGroup reportItem = (ViewGroup)inflater.inflate(R.layout.report_pr_heads_item, null);
		
		String scNo = "SC NO";
		String cc = "CC";
		String rc = "RC";
		String acd = "ACD";
		String agl = "AGL";
		
		TextView tv = (TextView)reportItem.findViewById(R.id.scNoTv);
		tv.setText(scNo);
		tv = (TextView)reportItem.findViewById(R.id.ccTv);
		tv.setText(cc);
		tv = (TextView)reportItem.findViewById(R.id.rcTv);
		tv.setText(rc);
		tv = (TextView)reportItem.findViewById(R.id.acdTv);
		tv.setText(acd);
		tv = (TextView)reportItem.findViewById(R.id.aglTv);
		tv.setText(agl);
		reportContentPane.addView(reportItem);
		
		View hSpacer = inflater.inflate(R.layout.horizontal_line, null);
		reportContentPane.addView(hSpacer);
	}
	
	private void preparePrHeadItemUi(Cursor c, 
			ViewGroup reportContentPane, LayoutInflater inflater) {
		ViewGroup reportItem = (ViewGroup)inflater.inflate(R.layout.report_pr_heads_item, null);
		
		String scNo = c.getString(c.getColumnIndex(PrColumns.SC_NO));
		String cc = c.getString(c.getColumnIndex(PrColumns.ARREARS_N_DEMAND));
		String rc = c.getString(c.getColumnIndex(PrColumns.RC_COLLECTED));
		String acd = c.getString(c.getColumnIndex(PrColumns.ACD_COLLECTED));
		String agl = c.getString(c.getColumnIndex(PrColumns.AGL_AMOUNT));
		
		cc = FormatUtils.amountFormat(cc);
		rc = FormatUtils.amountFormat(rc);
		acd = FormatUtils.amountFormat(acd);
		agl = FormatUtils.amountFormat(agl);
		
		TextView tv = (TextView)reportItem.findViewById(R.id.scNoTv);
		tv.setText(scNo);
		tv = (TextView)reportItem.findViewById(R.id.ccTv);
		tv.setText(cc);
		tv = (TextView)reportItem.findViewById(R.id.rcTv);
		tv.setText(rc);
		tv = (TextView)reportItem.findViewById(R.id.acdTv);
		tv.setText(acd);
		tv = (TextView)reportItem.findViewById(R.id.aglTv);
		tv.setText(agl);
		
		reportContentPane.addView(reportItem);
	}

	private void printReport() {
		PrintBillAction a = 
			new PrintBillAction(mViewManager.getActivity(),
					mReport.getPrintBuffer().toString().getBytes());
		a.perform(null);
	}
	
	@Override
	public void onKeyClick(char key) {
		switch(key) {
		case KeyPad.KEY_PREVIOUS:
			mViewManager.showPrevious();
			break;
		case KeyPad.KEY_NEXT:
			printReport();
			break;
		} 
	}
}