package ecollector.device;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.util.Log;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;

public class BoundedDateChangeListener implements OnDateChangedListener {
	
	private Date mFromDate;
	private Date mToDate;
	public BoundedDateChangeListener(Date fromDate, Date toDate) {
		mFromDate = truncTime(fromDate);
		mToDate = truncTime(toDate);
	}
	
	private Date truncTime(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.HOUR, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c.getTime();
	}

	public void onDateChanged(DatePicker view, int year, 
			int monthOfYear, int dayOfMonth)
	{
		SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
		try {
			Date setDate = 
				df.parse((new StringBuffer(year+"")).append("/").append(monthOfYear).append("/").append(dayOfMonth).toString());
			if (mFromDate.after(setDate)) {
				Calendar c = Calendar.getInstance();
				c.setTime(mFromDate);
				view.updateDate(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
			} else if (mToDate.before(setDate)) {
				Calendar c = Calendar.getInstance();
				c.setTime(mToDate);
				view.updateDate(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
			}
		} catch (ParseException e) {
			Log.i("DateChangeListener","Exception: ",e);
		}
	}

}
