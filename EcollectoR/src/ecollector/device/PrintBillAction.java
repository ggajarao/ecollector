package ecollector.device;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import ecollector.device.db.EcollectorDB;
import ecollector.device.print.BlueToothPrinter;
import ecollector.device.view.action.AbstractAction;

public class PrintBillAction extends AbstractAction {
	private static final String TAG = PrintBillAction.class.getName();

	public static final int PRINT_STARTED = 0;
	public static final int PRINT_PRINTER_NOT_FOUND = 1;
	public static final int PRINT_TURN_ON_BLUETOOTH = 2;
	public static final int PRINT_COMPLETED = 3;
	public static final int PRINT_DEVICE_NOT_PAIRED = 4;

	class PrintBillActionHandler extends Handler {
		public void handleMessage(Message msg) {
			switch(msg.arg1) {
			case PRINT_STARTED:
				Toast.makeText(mContext, "Printing Sarted", Toast.LENGTH_SHORT).show();
				break;
			case PRINT_COMPLETED:
				Toast.makeText(mContext, "Printing Completed", Toast.LENGTH_SHORT).show();
				break;
			case PRINT_PRINTER_NOT_FOUND:
			case PRINT_DEVICE_NOT_PAIRED:
				Toast.makeText(mContext, "Printing Device not found", Toast.LENGTH_SHORT).show();
				break;
			case PRINT_TURN_ON_BLUETOOTH:
				Toast.makeText(mContext, "Turn on Bluetooth", Toast.LENGTH_SHORT).show();
				break;
			}
		}
	}

	private PrintBillActionHandler mHandler = new PrintBillActionHandler();
	private BlueToothPrinter mBtPrinter;
	private Context mContext;
	private byte[] mPrintBuffer;
	
	public PrintBillAction(Context context, 
			byte[] printBuffer) {
		this.mContext = context;
		mPrintBuffer = printBuffer;
	}

	private void initPrinter() {
		String[] printerCfg = EcollectorDB.fetchPrinterConfig(mContext.getContentResolver());
		if (printerCfg != null && printerCfg.length == 2) {
			mBtPrinter = BlueToothPrinter.getInstance(printerCfg[1]);
		} else {
			Message msg = Message.obtain();
			msg.arg1 = PRINT_PRINTER_NOT_FOUND;
			mHandler.sendMessage(msg);
		}
	}

	private static final ScheduledExecutorService scheduler =
		Executors.newScheduledThreadPool(1);

	
	private static long nextPossiblePrintTime = 0;
	
	@Override
	protected void perform(View view) {
		long currentTime = SystemClock.elapsedRealtime();
		long milliesToWaitBeforePrint = nextPossiblePrintTime - currentTime;
		if (milliesToWaitBeforePrint < 0) {
			milliesToWaitBeforePrint = 0;
		}
		nextPossiblePrintTime = 
			SystemClock.elapsedRealtime() +
			PrintUtil.estimatePrintingTime(mPrintBuffer) +
			milliesToWaitBeforePrint;
		Log.i(TAG,"Print Queue, nextPossiblePrintTime: "+nextPossiblePrintTime+
				", currentTime: "+currentTime+", milliesToWaitBeforePrint: "+milliesToWaitBeforePrint);
		scheduler.schedule(
				new Runnable() {
					public void run() {
						printBill();
					}
				},
				milliesToWaitBeforePrint, 
				TimeUnit.MILLISECONDS);
	}

	private void printBill() {
		Log.i(TAG,"Printing Pr...");
		initPrinter();
		Message msg = Message.obtain();
		msg.arg1 = mBtPrinter.printBill(mPrintBuffer);/*mMrBean.getPrintBuffer()*/
		mHandler.sendMessage(msg);
		msg = Message.obtain();
		msg.arg1 = PrintBillAction.PRINT_COMPLETED;
		mHandler.sendMessage(msg);
	}
}