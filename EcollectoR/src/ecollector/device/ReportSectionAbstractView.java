package ecollector.device;

import android.support.v4.app.Fragment;
import ecollector.device.view.ViewManager;

public class ReportSectionAbstractView extends ReportView {

	public ReportSectionAbstractView() {
		
	}
	/*public ReportSectionAbstractView(ViewManager viewManager) {
		super(viewManager);
	}*/

	@Override
	protected Fragment getReportDetailsView(ReportFilterBean reportFilter) {
		return new ReportSectionAbstractDetailsView(reportFilter/*, mViewManager*/);
	}
}
