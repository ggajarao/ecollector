package ecollector.device;

import java.util.List;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import ecollector.common.FormatUtils;
import ecollector.common.PrColumns;
import ecollector.device.db.EcollectorDB;

public class PrBean {
	public String usc_No;
	public String sc_No;
	public String ero;
	public String section;
	public String distribution;
	public String receipt_number;
	public String collection_date;
	public String collection_time;
	public String arrears_n_demand;
	public String rc_collected;
	public String acd_collected;
	public String others_collected;
	public String rc_code;
	public String machine_code;
	public String last_paid_receipt_no;
	public String last_paid_date;
	public String last_paid_amount;
	public String tc_seal_no;
	public String remarks;
	public String remarks_amount;
	public String agl_amount;
	public String agl_services;
	public String new_arrears;
	public String cmd_collected;
	public String collection_delta;
	public String amount_collected;
	//private List<String> aglServicesList;
	
	public boolean isForPrintDuplicatePr = false;
	
	public final BillCollectionBean mBcBean;
	
	public PrBean(BillCollectionBean bcBean) {
		mBcBean = bcBean;
		initData(mBcBean);
		applyFieldFormatting();
	}
	
	public PrBean(BillCollectionBean bcBean, Cursor c) {
		mBcBean = bcBean;
		initData(c);
		applyFieldFormatting();
	}
	
	private void initData(Cursor c) {
		usc_No = c.getString(c.getColumnIndex(PrColumns.USC_NO));
		sc_No = c.getString(c.getColumnIndex(PrColumns.SC_NO));
		ero = c.getString(c.getColumnIndex(PrColumns.ERO));
		section = c.getString(c.getColumnIndex(PrColumns.SECTION));
		distribution = c.getString(c.getColumnIndex(PrColumns.DISTRIBUTION));
		receipt_number = c.getString(c.getColumnIndex(PrColumns.RECEIPT_NUMBER));
		collection_date = c.getString(c.getColumnIndex(PrColumns.COLLECTION_DATE));
		collection_time = c.getString(c.getColumnIndex(PrColumns.COLLECTION_TIME));
		arrears_n_demand = c.getString(c.getColumnIndex(PrColumns.ARREARS_N_DEMAND));
		rc_collected = c.getString(c.getColumnIndex(PrColumns.RC_COLLECTED));
		acd_collected = c.getString(c.getColumnIndex(PrColumns.ACD_COLLECTED));
		others_collected = c.getString(c.getColumnIndex(PrColumns.OTHERS_COLLECTED));
		rc_code = c.getString(c.getColumnIndex(PrColumns.RC_CODE));
		machine_code = c.getString(c.getColumnIndex(PrColumns.MACHINE_CODE));
		last_paid_receipt_no = c.getString(c.getColumnIndex(PrColumns.LAST_PAID_RECEIPT_NO));
		last_paid_date = c.getString(c.getColumnIndex(PrColumns.LAST_PAID_DATE));
		last_paid_amount = c.getString(c.getColumnIndex(PrColumns.LAST_PAID_AMOUNT));
		tc_seal_no = c.getString(c.getColumnIndex(PrColumns.TC_SEAL_NO));
		remarks = c.getString(c.getColumnIndex(PrColumns.REMARKS));
		remarks_amount = c.getString(c.getColumnIndex(PrColumns.REMARKS_AMOUNT));
		agl_amount = c.getString(c.getColumnIndex(PrColumns.AGL_AMOUNT));
		agl_services = c.getString(c.getColumnIndex(PrColumns.AGL_SERVICES));
		new_arrears = c.getString(c.getColumnIndex(PrColumns.NEW_ARREARS));
		cmd_collected = c.getString(c.getColumnIndex(PrColumns.CMD_COLLECTED));
		collection_delta = c.getString(c.getColumnIndex(PrColumns.COLLECTION_DELTA));
		amount_collected = c.getString(c.getColumnIndex(PrColumns.AMOUNT_COLLECTED));
	}
	
	private void applyFieldFormatting() {
		arrears_n_demand = FormatUtils.decimalPadding(arrears_n_demand);
		new_arrears = FormatUtils.decimalPadding(new_arrears);
		cmd_collected = FormatUtils.decimalPadding(cmd_collected);
		acd_collected = FormatUtils.decimalPadding(acd_collected);
		rc_collected = FormatUtils.decimalPadding(rc_collected);
		others_collected = FormatUtils.decimalPadding(others_collected);
		agl_amount = FormatUtils.decimalPadding(agl_amount);
		last_paid_amount = FormatUtils.decimalPadding(last_paid_amount);
		amount_collected = FormatUtils.decimalPadding(amount_collected);
	}

	private void initData(BillCollectionBean mBcBean) {
		usc_No = mBcBean.usc_No;
		sc_No = mBcBean.sc_No;
		ero = mBcBean.ero;
		section = mBcBean.section;
		distribution = mBcBean.distribution;
		collection_date = mBcBean.collection_date;
		collection_time = mBcBean.collection_time;
		/*computation fields*/
		  arrears_n_demand = mBcBean.arrears_n_demand;
		  rc_collected = mBcBean.rc_collected;
		  acd_collected = mBcBean.acd_collected.toString();
		  others_collected = mBcBean.others_collected.toString();
		/*----*/
		rc_code = mBcBean.rc_code;
		machine_code = mBcBean.machine_code;
		last_paid_receipt_no = mBcBean.last_paid_receipt_no;
		last_paid_date = mBcBean.last_paid_date;
		last_paid_amount = mBcBean.last_paid_amount;
		tc_seal_no = mBcBean.tc_seal_no;
		/*computation fields*/
		  remarks = mBcBean.getFinalRemarks();
		  remarks_amount = mBcBean.remarks_amount;
		  agl_amount = mBcBean.agl_amount;
		/*----*/
		agl_services = mBcBean.agl_services;
		
		new_arrears = mBcBean.mVolatileNew_arrears.toString();
		cmd_collected = mBcBean.mVolatile_cmd_collected.toString();
		collection_delta = mBcBean.mVolatileCollectionDelta.toString();
		amount_collected = mBcBean.amount_collected;
	}
	
	private String getArrearsLessCollectionDelta() {
		double newArrears = Util.asDouble(new_arrears);
		double collectionDelta = Util.asDouble(collection_delta);
		return Util.roundDec2(newArrears - collectionDelta)+"";
	}
	
	public boolean isCollectionDeltaExcess() {
		return (int)Util.asDouble(collection_delta) > 0;
	}
	
	public boolean isCollectionDeltaLess() {
		return (int)Util.asDouble(collection_delta) < 0;
	}
	
	public StringBuffer getPrintBuffer(Context context) {
		//WARNING! DO NOT HAVE THIS UNCOMMENT IN PRODUCTION
		// THIS WOULD HANG THE THREAD
		//android.os.Debug.waitForDebugger();
		
		if (PrintUtil.isPrePrintedStationaryFormat(context)) {
			return getPrintBufferNoCaptions(); 
		} else {
			return getPrintBufferWithCaptions();
		}
	}

	private StringBuffer getPrintBufferWithCaptions() {
		StringBuffer s = new StringBuffer();
		PrBean m = this;
		BillCollectionBean b = this.mBcBean;
		
		if (isForPrintDuplicatePr) {
			s.append(PrintUtil.prepareLineRpad("DUPLICATE",PrintUtil.MAX_CHARS_PER_LINE));
		} else {
			s.append(PrintUtil.prepareLineRpad("ORIGINAL",PrintUtil.MAX_CHARS_PER_LINE));
		}
		
		s.append(PrintUtil.prepareLineCalign("APSPDCL",PrintUtil.MAX_CHARS_PER_LINE));
		
		s.append(PrintUtil.preparePrintLineItem(
				"P.R NO: ",7,
				m.receipt_number+"",1,
				PrintUtil.MAX_CHARS_PER_LINE));
		
		s.append(PrintUtil.preparePrintLineItem(
				"TSL NO: ",7,
				m.tc_seal_no,1,
				PrintUtil.MAX_CHARS_PER_LINE));
		
		// DATE AND TIME LINE
		s.append(PrintUtil.preparePrintLineItem(
				"DT:",3,
				Util.getReadableDate(m.collection_date),1,
				14));
		s.append(PrintUtil.preparePrintLineItem(
				"TIME ",5,
				Util.getTimeIn24HourFormat(m.collection_time),1,
				PrintUtil.MAX_CHARS_PER_LINE-14));
		
		s.append(PrintUtil.preparePrintLineItem(
				"ERO NAME:",9,
				m.ero,1,
				PrintUtil.MAX_CHARS_PER_LINE));
		
		s.append(PrintUtil.preparePrintLineItem(
				"SEC NAME:",9,
				m.section,1,
				PrintUtil.MAX_CHARS_PER_LINE));
		
		s.append(PrintUtil.preparePrintLineItem(
				"DIST NAME:",10,
				m.distribution,1,
				PrintUtil.MAX_CHARS_PER_LINE));
		
		s.append(PrintUtil.preparePrintLineItem(
				"USCNO:",6,
				m.usc_No,3,
				PrintUtil.MAX_CHARS_PER_LINE));
		
		/*SCNO GRPU CAT Line*/
		s.append(PrintUtil.preparePrintLineItem(
				"SCNO:",5,
				m.sc_No,1,
				12));
		s.append(PrintUtil.preparePrintLineItem(
				"GRP:",4,
				b.group_x,1,
				7));
		s.append(PrintUtil.preparePrintLineItem(
				"CAT:",4,
				b.category,1,
				5));
		
		s.append(PrintUtil.preparePrintLineItem(
				"MONS:",5,
				Util.getReadableDateRange(b.billed_months),1,
				PrintUtil.MAX_CHARS_PER_LINE));
		
		s.append(PrintUtil.preparePrintLineItem(
				"NAME:",5,
				b.name,1,
				PrintUtil.MAX_CHARS_PER_LINE));
		
		/*----------------------------*/
		s.append(PrintUtil.DASHED_LINE);
		/*----------------------------*/
		
		s.append(PrintUtil.preparePrintLineItem(
				"ARREARS AMT:",14,
				//FormatUtils.decimalPadding(m.new_arrears),
				FormatUtils.decimalPadding(m.getArrearsLessCollectionDelta()),
				3,
				PrintUtil.MAX_CHARS_PER_LINE));
		
		s.append(PrintUtil.preparePrintLineItem(
				"CURR MON DEMD:",14,
				FormatUtils.decimalPadding(m.cmd_collected),3,
				PrintUtil.MAX_CHARS_PER_LINE));
		
		s.append(PrintUtil.preparePrintLineItem(
				"A.C.D AMOUNT:",14,
				FormatUtils.decimalPadding(m.acd_collected),3,
				PrintUtil.MAX_CHARS_PER_LINE));
		
		s.append(PrintUtil.preparePrintLineItem(
				"RC FEE:",14,
				FormatUtils.decimalPadding(m.rc_collected.toString()),3,
				PrintUtil.MAX_CHARS_PER_LINE));
		
		s.append(PrintUtil.preparePrintLineItem(
				"OTHERS :",14,
				FormatUtils.decimalPadding(m.others_collected.toString()),3,
				PrintUtil.MAX_CHARS_PER_LINE));
		
		s.append(PrintUtil.preparePrintLineItem(
				"AGL AMOUNT:",14,
				FormatUtils.decimalPadding(m.agl_amount.toString()),3,
				PrintUtil.MAX_CHARS_PER_LINE));
		
		if (!isAglPr()) {
			if (this.isCollectionDeltaExcess()) {
				s.append(PrintUtil.preparePrintLineItem(
						"Excess paid:",14,
						"+"+FormatUtils.decimalPadding(m.collection_delta),3,
						PrintUtil.MAX_CHARS_PER_LINE));
				/*s.append(PrintUtil.prepareLineRpad(
						"Excess paid Rs."+FormatUtils.decimalPadding(m.collection_delta),
						PrintUtil.MAX_CHARS_PER_LINE));*/
			} else if (this.isCollectionDeltaLess()) {
				s.append(PrintUtil.preparePrintLineItem(
						"Less paid:",14,
						FormatUtils.decimalPadding(m.collection_delta),3,
						PrintUtil.MAX_CHARS_PER_LINE));
				/*s.append(PrintUtil.prepareLineRpad(
						"Less paid Rs."+FormatUtils.decimalPadding(m.collection_delta),
						PrintUtil.MAX_CHARS_PER_LINE));*/
			}
		}
		
		s.append(PrintUtil.prepareLineLpad("__________", PrintUtil.MAX_CHARS_PER_LINE));
		
		s.append(PrintUtil.preparePrintLineItem(
				"AMMOUNT COLLD:",14,
				FormatUtils.decimalPadding(m.amount_collected.toString()),3,
				PrintUtil.MAX_CHARS_PER_LINE));
		
		s.append(PrintUtil.prepareLineLpad("__________", PrintUtil.MAX_CHARS_PER_LINE));
		
		s.append(PrintUtil.prepareLineRpad("Rs."+Util.inWords(m.amount_collected),PrintUtil.MAX_CHARS_PER_LINE));
		
		/*                     */
		s.append(PrintUtil.BLANK_LINE);
		/*                     */
		
		
		if (isAglPr()) {
			s.append(PrintUtil.prepareLineRpadMultiLine(
					"AGL SVCS:"+getAglServicesAsString()));
		} else {
			s.append(PrintUtil.prepareLineRpad(
					"Remarks: "+EcollectorDB.getRemarksPrintable(m.remarks), 
					PrintUtil.MAX_CHARS_PER_LINE));
		}
		
		/*                     */
		s.append(PrintUtil.BLANK_LINE);
		s.append(PrintUtil.BLANK_LINE);
		/*                     */
		
		s.append(PrintUtil.prepareLineLpad(
				"AUTHORISED SIGNATORY", 
				PrintUtil.MAX_CHARS_PER_LINE));
		
		/*                     */
		s.append(PrintUtil.BLANK_LINE);
		/*                     */
		/*                     */
		s.append(PrintUtil.BLANK_LINE);
		/*                     */
		return s;
	}

	private StringBuffer getPrintBufferNoCaptions() {
		StringBuffer s = new StringBuffer();
		PrBean m = this;
		BillCollectionBean b = this.mBcBean;
		
		if (isForPrintDuplicatePr) {
			s.append(PrintUtil.prepareLineRpad("DUPLICATE",PrintUtil.MAX_CHARS_PER_LINE));
		} else {
			s.append(PrintUtil.prepareLineRpad("ORIGINAL",PrintUtil.MAX_CHARS_PER_LINE));
		}
		
		s.append(PrintUtil.BLANK_LINE);
		s.append(PrintUtil.BLANK_LINE);
		s.append(PrintUtil.BLANK_LINE);
		
		/*P.R NO: */
		s.append(PrintUtil.preparePrintLineItem(
				"",7,
				m.receipt_number+"",1,
				PrintUtil.MAX_CHARS_PER_LINE));
		/*TSL NO: */
		s.append(PrintUtil.preparePrintLineItem(
				"",7,
				m.tc_seal_no,1,
				PrintUtil.MAX_CHARS_PER_LINE));
		
		// DATE AND TIME LINE
		s.append(PrintUtil.preparePrintLineItem(
				"   ",3,
				Util.getReadableDate(m.collection_date),1,
				14));
		s.append(PrintUtil.preparePrintLineItem(
				"     ",5,
				Util.getTimeIn24HourFormat(m.collection_time),1,
				PrintUtil.MAX_CHARS_PER_LINE-14));
		
		/*ERO NAME:*/
		s.append(PrintUtil.preparePrintLineItem(
				"",9,
				m.ero,1,
				PrintUtil.MAX_CHARS_PER_LINE));
		
		/*SEC NAME:*/
		s.append(PrintUtil.preparePrintLineItem(
				"",9,
				m.section,1,
				PrintUtil.MAX_CHARS_PER_LINE));
		
		/*DIST NAME:*/
		s.append(PrintUtil.preparePrintLineItem(
				"",10,
				m.distribution,1,
				PrintUtil.MAX_CHARS_PER_LINE));
		
		/*USCNO:*/
		s.append(PrintUtil.preparePrintLineItem(
				"",6,
				m.usc_No,3,
				PrintUtil.MAX_CHARS_PER_LINE));
		
		/*SCNO GRPU CAT Line*/
		s.append(PrintUtil.preparePrintLineItem(
				"     ",5,
				m.sc_No,1,
				12));
		s.append(PrintUtil.preparePrintLineItem(
				"    ",4,
				b.group_x,1,
				7));
		s.append(PrintUtil.preparePrintLineItem(
				"    ",4,
				b.category,1,
				5));
		
		/*MONS:*/
		s.append(PrintUtil.preparePrintLineItem(
				"     ",5,
				Util.getReadableDateRange(b.billed_months),1,
				PrintUtil.MAX_CHARS_PER_LINE));
		/*NAME:*/
		s.append(PrintUtil.preparePrintLineItem(
				"     ",5,
				b.name,1,
				PrintUtil.MAX_CHARS_PER_LINE));
		
		/*----------------------------*/
		/*s.append(PrintUtil.DASHED_LINE);*/
		/*----------------------------*/
		
		/*                     */
		s.append(PrintUtil.BLANK_LINE);
		/*                     */
		
		/*ARREARS AMT:*/
		s.append(PrintUtil.preparePrintLineItem(
				"",14,
				//FormatUtils.decimalPadding(m.new_arrears),
				FormatUtils.decimalPadding(m.getArrearsLessCollectionDelta()),
				3,
				PrintUtil.MAX_CHARS_PER_LINE));
		
		/*CURR MON DEMD:*/
		s.append(PrintUtil.preparePrintLineItem(
				"",14,
				FormatUtils.decimalPadding(m.cmd_collected),3,
				PrintUtil.MAX_CHARS_PER_LINE));
		/*A.C.D AMOUNT:*/
		s.append(PrintUtil.preparePrintLineItem(
				"",14,
				FormatUtils.decimalPadding(m.acd_collected),3,
				PrintUtil.MAX_CHARS_PER_LINE));
		/*RC FEE:*/
		s.append(PrintUtil.preparePrintLineItem(
				"",14,
				FormatUtils.decimalPadding(m.rc_collected.toString()),3,
				PrintUtil.MAX_CHARS_PER_LINE));
		/*OTHERS :*/
		/*s.append(PrintUtil.preparePrintLineItem(
				"",14,
				FormatUtils.decimalPadding(m.others_collected.toString()),3,
				PrintUtil.MAX_CHARS_PER_LINE));*/
		
		if (isAglPr()) {
			/*AGL AMOUNT:*/
			s.append(PrintUtil.preparePrintLineItem(
					"AGL AMOUNT:",14,
					FormatUtils.decimalPadding(m.agl_amount.toString()),3,
					PrintUtil.MAX_CHARS_PER_LINE));
		} else {
			if (this.isCollectionDeltaExcess()) {
				s.append(PrintUtil.preparePrintLineItem(
						"Excess paid:",14,
						"+"+FormatUtils.decimalPadding(m.collection_delta),3,
						PrintUtil.MAX_CHARS_PER_LINE));
				/*s.append(PrintUtil.prepareLineRpad(
						"Excess paid Rs."+FormatUtils.decimalPadding(m.collection_delta),
						PrintUtil.MAX_CHARS_PER_LINE));*/
			} else if (this.isCollectionDeltaLess()) {
				s.append(PrintUtil.preparePrintLineItem(
						"Less paid:",14,
						FormatUtils.decimalPadding(m.collection_delta),3,
						PrintUtil.MAX_CHARS_PER_LINE));
				/*s.append(PrintUtil.prepareLineRpad(
						"Less paid Rs."+FormatUtils.decimalPadding(m.collection_delta),
						PrintUtil.MAX_CHARS_PER_LINE));*/
			} else {
				/*                     */
				s.append(PrintUtil.BLANK_LINE);
				/*                     */
			}
		}
		
		/*if (!isAglPr()) {
			if (this.isCollectionDeltaExcess()) {
				s.append(PrintUtil.preparePrintLineItem(
						"Excess paid:",14,
						"+"+FormatUtils.decimalPadding(m.collection_delta),3,
						PrintUtil.MAX_CHARS_PER_LINE));
			} else if (this.isCollectionDeltaLess()) {
				s.append(PrintUtil.preparePrintLineItem(
						"Less paid:",14,
						FormatUtils.decimalPadding(m.collection_delta),3,
						PrintUtil.MAX_CHARS_PER_LINE));
			}
		}*/
		
		s.append(PrintUtil.prepareLineLpad("__________", PrintUtil.MAX_CHARS_PER_LINE));
		
		/*AMMOUNT COLLD:*/
		s.append(PrintUtil.preparePrintLineItem(
				"",14,
				FormatUtils.decimalPadding(m.amount_collected.toString()),3,
				PrintUtil.MAX_CHARS_PER_LINE));
		
		s.append(PrintUtil.prepareLineLpad("__________", PrintUtil.MAX_CHARS_PER_LINE));
		
		s.append(PrintUtil.prepareLineRpad("Rs."+Util.inWords(m.amount_collected),PrintUtil.MAX_CHARS_PER_LINE));
		
		/*                     */
		s.append(PrintUtil.BLANK_LINE);
		/*                     */
		/*                     */
		s.append(PrintUtil.BLANK_LINE);
		/*                     */
		
		if (isAglPr()) {
			s.append(PrintUtil.prepareLineRpadMultiLine(
					"AGL SVCS:"+getAglServicesAsString()));
		} else {
			/*Remarks:*/ 
			String printableRemarks = EcollectorDB.getRemarksPrintable(m.remarks);
			if (printableRemarks != null && printableRemarks.trim().length() != 0) {
				s.append(PrintUtil.prepareLineRpad(
						""+printableRemarks, 
						PrintUtil.MAX_CHARS_PER_LINE));
			} else {
				/*                     */
				s.append(PrintUtil.BLANK_LINE);
				/*                     */
			}
		}
		
		/*                     */
		s.append(PrintUtil.BLANK_LINE);
		/*                     */
		
		/*s.append(PrintUtil.prepareLineLpad(
				"AUTHORISED SIGNATORY", 
				PrintUtil.MAX_CHARS_PER_LINE));*/
		
		/*                     */
		s.append(PrintUtil.BLANK_LINE);
		/*                     */
		/*                     */
		s.append(PrintUtil.BLANK_LINE);
		/*                     */
		/*                     */
		s.append(PrintUtil.BLANK_LINE);
		/*                     */
		/*                     */
		s.append(PrintUtil.BLANK_LINE);
		/*                     */
		/*                     */
		s.append(PrintUtil.BLANK_LINE);
		/*                     */
		
		return s;
	}

	public void previewPr(PrintPreviewRenderer printPreviewRenderer) {
		PrintPreviewRenderer r = printPreviewRenderer;
		r.addPreviewItem("P.R. No", receipt_number, "");
		r.addPreviewItem("TSL No", tc_seal_no, "");
		r.addPreviewItem("DATE TIME", Util.getReadableDate(collection_date) + " " +collection_time, "");
		r.addPreviewItem("ERO NAME", ero, "");
		r.addPreviewItem("SEC NAME", section, "");
		r.addPreviewItem("DIST NAME", distribution, "");
		r.addPreviewItem("USCNO", usc_No, "");
		r.addPreviewItem("SCNO", sc_No, "");
		r.addPreviewItem("MONS", Util.getReadableDateRange(mBcBean.billed_months), "");
		r.addPreviewItem("NAME", mBcBean.name, "");
		r.addPreviewItem("ARREARS AMT", FormatUtils.decimalPadding(getArrearsLessCollectionDelta()), ""); 
				//FormatUtils.decimalPadding(new_arrears.toString()), "");
		r.addPreviewItem("CURR MON DEMD", FormatUtils.decimalPadding(cmd_collected.toString()), "");
		r.addPreviewItem("ACD AMOUNT", FormatUtils.decimalPadding(acd_collected.toString()), "");
		r.addPreviewItem("RC FEE", FormatUtils.decimalPadding(rc_collected.toString()), "");
		r.addPreviewItem("OTHERS", FormatUtils.decimalPadding(others_collected.toString()), "");
		r.addPreviewItem("AGL AMOUNT", FormatUtils.decimalPadding(agl_amount), "");
		if (!isAglPr()) {
			if (this.isCollectionDeltaExcess()) {
				r.addPreviewItem("Excess paid Rs.", FormatUtils.decimalPadding(collection_delta), "");						
			} else if (this.isCollectionDeltaLess()) {
				r.addPreviewItem("Less paid Rs.", FormatUtils.decimalPadding(collection_delta), "");
			}
		}
		r.addPreviewItem("AMOUNT COLLD", FormatUtils.decimalPadding(amount_collected), "");
		r.addPreviewItem("Rs.", Util.inWords(amount_collected), "");

		if (isAglPr()) {
			r.addPreviewItem("Remarks", getAglServicesAsString(), "");
		} else {
			r.addPreviewItem("Remarks", EcollectorDB.getRemarksDescription(remarks), "NONE");
		}
		r.addPreviewItem("Rs.", FormatUtils.decimalPadding(remarks_amount), "");
	}
	
	private String getAglServicesAsString() {
		List<String> aglServicesList = FormatUtils.parseAglServices(agl_services);
		aglServicesList = Util.getTrimedAglServices(aglServicesList);
		StringBuffer sb = new StringBuffer();
		for (String svc : aglServicesList) {
			sb.append(" ").append(svc);
		}
		return sb.toString();
	}

	public boolean isAglPr() {
		return Util.asDouble(agl_amount) > 0;
	}
	
	private String preparePrNumber(Context context) {
		return EcollectorDB.getNextRecieptNo(context)+"";
	}
	
	private String prepareTslNumber(Context context) {
		return rc_code+EcollectorDB.getNextTslNo(context);
	}
	
	String deviceName = null;
	private String getDeviceName(Context context) {
		if (deviceName == null) {
			deviceName = EcollectorDB.fetchDeviceName(context);
		}
		return deviceName;
	}

	public String save(Context context) {
		
		/** If remarks is not set manually, 
		 *  then generate remarks based on computation
		 *  Eg: if paid amount is less then remarks should be 'C - Part Payment'
		 *      if agl service remarks should be 'K - AGL Collection'
		 *      etc.
		 */
		String remarks = this.remarks;
		if (remarks == null || remarks.trim().length() == 0) {
			if (this.isCollectionDeltaLess()) {
				remarks = EcollectorDB.REMARKS_PART_PAYMENT;
			} else if (mBcBean.isArrearsSelectedToAdd()) {
				double iNegativeArrears = Util.asDouble(mBcBean.getNegativeArrearsAdded());
				if (iNegativeArrears < 0) {
					remarks = EcollectorDB.REMARKS_NEGATIVE_BALANCE_EXCLUDED;
					remarks_amount = mBcBean.getNegativeArrearsAdded();
				}
			}
			this.remarks = remarks;
		}
		
		ContentValues v = new ContentValues();
		
		receipt_number = preparePrNumber(context);
		tc_seal_no = prepareTslNumber(context);
		v.put(PrColumns.RECEIPT_NUMBER, receipt_number);
		v.put(PrColumns.USC_NO,this.usc_No);
		v.put(PrColumns.SC_NO,this.sc_No);
		v.put(PrColumns.ERO,this.ero);
		v.put(PrColumns.SECTION,this.section);
		v.put(PrColumns.DISTRIBUTION,this.distribution);
		v.put(PrColumns.RECEIPT_NUMBER,this.receipt_number);
		v.put(PrColumns.COLLECTION_DATE,this.collection_date);
		v.put(PrColumns.COLLECTION_TIME,this.collection_time);
		v.put(PrColumns.ARREARS_N_DEMAND,this.arrears_n_demand);
		v.put(PrColumns.RC_COLLECTED,this.rc_collected);
		v.put(PrColumns.ACD_COLLECTED,this.acd_collected.toString());
		v.put(PrColumns.OTHERS_COLLECTED,this.others_collected.toString());
		v.put(PrColumns.RC_CODE,this.rc_code);
		v.put(PrColumns.MACHINE_CODE,this.machine_code);
		v.put(PrColumns.LAST_PAID_RECEIPT_NO,this.last_paid_receipt_no);
		v.put(PrColumns.LAST_PAID_DATE,this.last_paid_date);
		v.put(PrColumns.LAST_PAID_AMOUNT,this.last_paid_amount);
		v.put(PrColumns.TC_SEAL_NO,this.tc_seal_no);
		v.put(PrColumns.REMARKS,this.remarks);
		v.put(PrColumns.REMARKS_AMOUNT,this.remarks_amount);
		v.put(PrColumns.AGL_AMOUNT,this.agl_amount);
		v.put(PrColumns.AGL_SERVICES,this.agl_services);
		v.put(PrColumns.NEW_ARREARS, this.new_arrears);
		v.put(PrColumns.CMD_COLLECTED, this.cmd_collected);
		v.put(PrColumns.COLLECTION_DELTA, this.collection_delta);
		v.put(PrColumns.AMOUNT_COLLECTED, this.amount_collected);
		
		String devicePrCode = 
			getDeviceName(context) +
			Util.getCurrentTimestamp()+this.receipt_number;
		v.put(PrColumns._DEVICE_PR_CODE, devicePrCode);
		
		EcollectorDB.savePrDetails(context,v);
		return receipt_number;
	}
}