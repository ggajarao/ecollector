package ecollector.device;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import ecollector.device.db.EcollectorDB;
import ecollector.device.view.action.DevicePairingAction;
import ecollector.device.view.action.ShowEcollector;

public class DevicePairingActivity extends Activity {
	
	private EditText mDeviceNameEditText;
	private TextView mMessageText;
	private TextView mHeaderTitle;
	private Button mPairBtn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.d_p_a_view);
        mDeviceNameEditText = (EditText)findViewById(R.id.deviceNameEditText);
        
        mMessageText = (TextView)findViewById(R.id.messageText);
        mHeaderTitle = (TextView)findViewById(R.id.headerTitle);
        
        mPairBtn = (Button)findViewById(R.id.pairBtn);
        mPairBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				DevicePairingAction dpa = 
					new DevicePairingAction(DevicePairingActivity.this, 
							mDeviceNameEditText.getText().toString(), 
							importProgressHandler);
				dpa.onClick(v);
			}
		});
	}
	
	@Override
    protected void onResume() {
		super.onResume();
		if (EcollectorDB.isDevicePaired(this.getContentResolver())) {
			mHeaderTitle.setText("This device is already paired");
			mHeaderTitle.setTextColor(Color.GREEN);
			mMessageText.setVisibility(View.GONE);
			mDeviceNameEditText.setVisibility(View.GONE);
			
			mPairBtn.setText(" Home ");
			mPairBtn.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					ShowEcollector s = new ShowEcollector(DevicePairingActivity.this);
					s.onClick(v);
				}
			});
		}
	}
	
	private final Handler importProgressHandler = new Handler() {
		public void handleMessage(Message msg) {
			TextView tv = mMessageText; 
			if (tv != null) {
				int action = msg.arg1;
				int value = msg.arg2;
				switch (action) {
				case DevicePairingAction.ACKNOWLEDGE_PAIRING:
					tv.setText("Acknowledge...");
					break;
				case DevicePairingAction.ALREADY_PAIRED:
					tv.setText("This device is already paired");
					break;
				case DevicePairingAction.ALREADY_PAIRED_WITH_THIS_NAME:
					tv.setText("This name is already used by another device, please use another name");
					break;
				case DevicePairingAction.FAILED:
					tv.setText("Pairing failed, please try again");
					break;
				case DevicePairingAction.FAILED_AT_SERVER:
					tv.setText("Pairing failed at host, please try again");
					break;
				case DevicePairingAction.PAIRED:
					tv.setText("Device Paired successfully");
					ShowEcollector s = new ShowEcollector(DevicePairingActivity.this);
					s.onClick(null);
					break;
				case DevicePairingAction.PAIRING:
					tv.setText("Pairing...");
					break;
				case DevicePairingAction.NO_NETWORK:
					tv.setText("No Internet Connection !");
					break;
				case DevicePairingAction.STARTED:
					tv.setText("Connecting...");
					break;
				case DevicePairingAction.NETWORK_ERROR:
					tv.setText("Network error: "+msg.obj);
					break;
				case DevicePairingAction.INVALID_SOFT_VERSION:
					tv.setText("Incompatible software version, please upgrade to latest version");
					break;	
				default:
					tv.setText("...");
				}
			}
		}
	};
	
	@Override
	public void onBackPressed() {
		
	}
}
