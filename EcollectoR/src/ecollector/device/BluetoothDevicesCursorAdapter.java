package ecollector.device;

import android.content.Context;
import android.database.Cursor;
import ecollector.device.android.framework.SimpleCursorAdapter;

public class BluetoothDevicesCursorAdapter extends SimpleCursorAdapter {
	public BluetoothDevicesCursorAdapter(Context context, int layout, Cursor c,
			String[] from, int[] to) {
		super(context, layout, c, from, to);
	}
	
	public CharSequence convertToString(Cursor cursor) {
		int colCount = cursor.getColumnCount();
		if (colCount >= 2) {
			return cursor.getString(0)+" - "+cursor.getString(1);
		}
		return "N/A";
	}
}