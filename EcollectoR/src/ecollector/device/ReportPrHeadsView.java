package ecollector.device;

import android.support.v4.app.Fragment;
import ecollector.device.view.ViewManager;

public class ReportPrHeadsView extends ReportView {

	public ReportPrHeadsView() {
		
	}
	
	/*public ReportPrHeadsView(ViewManager viewManager) {
		super(viewManager);
	}*/

	@Override
	protected Fragment getReportDetailsView(ReportFilterBean reportFilter) {
		return new ReportPrHeadsDetailView(reportFilter, mViewManager);
	}
}
