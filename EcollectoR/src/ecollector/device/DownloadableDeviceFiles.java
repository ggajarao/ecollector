package ecollector.device;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.content.ContentResolver;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import ebilly.admin.shared.DeviceFileUi;
import ecollector.device.net.CheckUpdatesCommand;
import ecollector.device.net.CloudCommand;
import ecollector.device.view.action.HandlerActions;
import ecollector.device.view.action.SimpleMessinger;
import ecollector.device.view.input.NavigationKeyPad;

public class DownloadableDeviceFiles {
	private Map<String, DownloadableDeviceFileItem> deviceFileItemMap = 
		new HashMap<String, DownloadableDeviceFileItem>();
	private ContentResolver mContentResolver;
	private NavigationKeyPad mNavKeyPad;
	
	
	private static DownloadableDeviceFiles i;
	public synchronized static DownloadableDeviceFiles getInstance(ContentResolver contentResolver) {
		if (i == null) {
			i = new DownloadableDeviceFiles(contentResolver);
		}
		return i;
	}
	
	private DownloadableDeviceFiles(ContentResolver contentResolver) {
		mContentResolver = contentResolver;
		Log.d("DownloadableDeviceFiles","Initialized");
	}
	
	@SuppressWarnings("unchecked")
	public synchronized void checkForUpdates(Context context, Handler h) {
		CloudCommand c = new CheckUpdatesCommand(mContentResolver,context,new SimpleMessinger(h));
		c.execute();
		List<DeviceFileUi> deviceFiles = (List<DeviceFileUi>)c.getResults();
		
		prepareDeviceFileItems(deviceFiles);

		if (deviceFileItemMap.isEmpty()) {
			Message msg = Message.obtain();
			msg.arg1 = HandlerActions.CLOUD_CHK_FOR_UPDATES_NO_FILES;
			h.sendMessage(msg);
			return;
		} else {
			Message msg = Message.obtain();
			msg.arg1 = HandlerActions.CLOUD_CHK_FOR_UPDATES_COMPLETED;
			h.sendMessage(msg);
		}	
	}
	
	private synchronized void prepareDeviceFileItems(List<DeviceFileUi> deviceFiles) {
		//if (deviceFiles == null) return;
		if (deviceFiles != null) {
			for (DeviceFileUi df : deviceFiles) {
				String fileKey = df.getDeviceServicesFileKey();
				if (!deviceFileItemMap.containsKey(fileKey)) {
					deviceFileItemMap.put(fileKey, 
							new DownloadableDeviceFileItem(df,
									mContentResolver,this));
				}
			}
		}
		
		List<DownloadableDeviceFileItem> removeList = new ArrayList<DownloadableDeviceFileItem>();
		for (Entry<String,DownloadableDeviceFileItem> e : deviceFileItemMap.entrySet()) {
			boolean found = false;
			if (deviceFiles != null) {
				for (DeviceFileUi df : deviceFiles) {
					if (df.getDeviceServicesFileKey().equalsIgnoreCase(e.getKey())) {
						found = true;
					}
				}	
			}
			
			if (!found && !e.getValue().isImportInProgress()) {
				removeList.add(e.getValue());
			}
		}
		
		for (DownloadableDeviceFileItem dfi : removeList) {
			removeDeviceFileItem(dfi);
		}
	}
	
	private synchronized void removeDeviceFileItem(DownloadableDeviceFileItem dfi) {
		deviceFileItemMap.remove(dfi.getFileKey());
	}
	
	public void prepareUi(LayoutInflater inflater, ViewGroup parent, Looper looper, NavigationKeyPad navKeyPad) {
		mNavKeyPad = navKeyPad;
		for (Entry<String,DownloadableDeviceFileItem> e : deviceFileItemMap.entrySet()) {
			final String deviceFileKey = e.getKey();
			e.getValue().prepareUi(inflater,parent, looper);
		}
	}
	
	void freezeActionControls(boolean freezeActionControls) {
		mNavKeyPad.setPreviousEnabled(!freezeActionControls);
		for (Entry<String,DownloadableDeviceFileItem> e : deviceFileItemMap.entrySet()) {
			e.getValue().enableImportButton(!freezeActionControls);
		}
	}
}
