package ebilly.admin.server.bo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ecollector.common.ServiceColumns;

public class PrSetupData {
	public int NUM_COMPANY = 0;
	public int NUM_CIRCLE = 0;
	public int NUM_DIVISION = 0;
	public int NUM_ERO = 0;
	public int NUM_SUB_DIV = 0;
	public int NUM_SECTION = 0;
	public int NUM_DIST = 0; // 2 digit
	public int NUM_SC = 0;
	
	public double and = 0;
	public double rc = 0;
	public double acd = 0;
	public double agl = 0;
	
	public String deviceName = "DeviceNameX";
	public String collectionDate = getCurrentDateInDevicePrFormat();
	public String collectionTime = getCurrentTimestampInDevicePrFormat();
	
	public PrSetupData() {init();}
	
	public void init() {
		NUM_COMPANY = 0;
		NUM_CIRCLE = 0;
		NUM_DIVISION = 0;
		NUM_ERO = 0;
		NUM_SUB_DIV = 0;
		NUM_SECTION = 0;
		NUM_DIST = 0; // 2 digit
		NUM_SC = 0;
		
		and = 0;
		rc = 0;
		acd = 0;
		agl = 0;
	}
	
	public PrHead asPrHead() {
		PrHead ph = new PrHead();
		ph.and = this.and;
		ph.rc = this.rc;
		ph.acd = this.acd;
		ph.agl = this.agl;
		return ph;
	}
	
	public PrHead getCompanySum() {
		PrHead ph = this.asPrHead();
		int multiplier = this.getCompanysPrCount(); 
			//(NUM_CIRCLE+1)*(NUM_DIVISION+1)*(NUM_ERO+1)*(NUM_SUB_DIV+1)*(NUM_SECTION+1)*(NUM_DIST+1)*(NUM_SC+1);
		return ph.multiply(multiplier);
	}
	
	public PrHead getCircleSum() {
		PrHead ph = this.asPrHead();
		int multiplier = this.getCirclesPrCount(); 
			//(NUM_DIVISION+1)*(NUM_ERO+1)*(NUM_SUB_DIV+1)*(NUM_SECTION+1)*(NUM_DIST+1)*(NUM_SC+1);
		return ph.multiply(multiplier);
	}
	
	public PrHead getDivisionSum() {
		PrHead ph = this.asPrHead();
		int multiplier = this.getDivisionsPrCount(); 
		   //(NUM_ERO+1)*(NUM_SUB_DIV+1)*(NUM_SECTION+1)*(NUM_DIST+1)*(NUM_SC+1);
		return ph.multiply(multiplier);
	}
	
	public PrHead getEroSum() {
		PrHead ph = this.asPrHead();
		int multiplier =
			this.getErosPrCount();
			//(NUM_SUB_DIV+1)*(NUM_SECTION+1)*(NUM_DIST+1)*(NUM_SC+1);
		return ph.multiply(multiplier);
	}
	
	public PrHead getSubdivisionSum() {
		PrHead ph = this.asPrHead();
		int multiplier = this.getSubDivsPrCount(); 
			//(NUM_SECTION+1)*(NUM_DIST+1)*(NUM_SC+1);
		ph.prCount = this.getSubDivsPrCount();
		return ph.multiply(multiplier);
	}
	
	public PrHead getSectionSum() {
		PrHead ph = this.asPrHead();
		int multiplier = this.getSectionsPrCount(); 
			//(NUM_DIST+1)*(NUM_SC+1);
		ph.prCount = this.getSectionsPrCount();
		return ph.multiply(multiplier);
	}
	
	public PrHead getDistributionSum() {
		PrHead ph = this.asPrHead();
		int multiplier = this.getDistributionsPrCount(); 
			//(NUM_SC+1);
		ph.prCount = this.getDistributionsPrCount();
		return ph.multiply(multiplier);
	}
	
	public int getCompanyCount() {
		return (NUM_COMPANY+1);
	}
	
	public int getCompanysPrCount() {
		return (NUM_CIRCLE+1)*this.getCirclesPrCount();
	}
	
	public int getCirclesCount() {
		return (NUM_COMPANY+1)*(NUM_CIRCLE+1);
	}
	
	public int getCirclesPrCount() {
		return (NUM_DIVISION+1)*this.getDivisionsPrCount();
	}
	
	public int getDivisionsCount() {
		return (NUM_COMPANY+1)*(NUM_CIRCLE+1)*
		   (NUM_DIVISION+1);
	}
	
	public int getDivisionsPrCount() {
		return (NUM_ERO+1)*this.getErosPrCount();
	}
	
	public int getEroCount() {
		return (NUM_COMPANY+1)*(NUM_CIRCLE+1)*
		   (NUM_DIVISION+1)*(NUM_ERO+1);
	}
	
	public int getErosPrCount() {
		return (NUM_SUB_DIV+1)*this.getSubDivsPrCount();
	}
	
	public int getSubDivCount() {
		return (NUM_COMPANY+1)*(NUM_CIRCLE+1)*
		   (NUM_DIVISION+1)*(NUM_ERO+1)*(NUM_SUB_DIV+1);
	}
	
	public int getSubDivsPrCount() {
		return (NUM_SECTION+1)*this.getSectionsPrCount();
	}
	
	public int getSectionsCount() {
		return (NUM_COMPANY+1)*(NUM_CIRCLE+1)*
		   (NUM_DIVISION+1)*(NUM_ERO+1)*
		   (NUM_SUB_DIV+1)*(NUM_SECTION+1);
	}
	
	public int getSectionsPrCount() {
		return (NUM_DIST+1)*this.getDistributionsPrCount();
	}
	
	public int getDistributionsCount() {
		return (NUM_COMPANY+1)*(NUM_CIRCLE+1)*
			   (NUM_DIVISION+1)*(NUM_ERO+1)*
			   (NUM_SUB_DIV+1)*(NUM_SECTION+1)*
			   (NUM_DIST+1);
	}
	
	public int getDistributionsPrCount() {
		return (NUM_SC+1);
	}
	
	public List<String> getCompanies() {
		List<String> list = new ArrayList<String>();
		list.add("0");
		return list;
	}
	
	public List<String> getCircles() {
		List<String> list = new ArrayList<String>();
		int circle = 0;
		long uscNo = 0;
		for (; circle <= NUM_CIRCLE; circle++) {
			uscNo = 
				(1+circle)  *1L ;
			list.add(uscNo+"");
		}
		return list;
	}
	
	public List<String> getDivisions() {
		List<String> list = new ArrayList<String>();
		int circle = 0;
		int division = 0;
		long uscNo = 0;
		for (; circle <= NUM_CIRCLE; circle++) { division = 0;
		for (; division <= NUM_DIVISION; division++) {
			uscNo = 
				(1+circle)  *10L +
			    (1+division)* 1L ;
			list.add(uscNo+"");
		}}
		return list;
	}
	
	public List<String> getEros() {
		List<String> list = new ArrayList<String>();
		int circle = 0;
		int division = 0;
		int ero = 0;
		long uscNo = 0;
		for (; circle <= NUM_CIRCLE; circle++) { division = 0;
		for (; division <= NUM_DIVISION; division++) { ero = 0;
		for (; ero <= NUM_ERO; ero++) {
			uscNo = 
				(1+circle)  *100L +
			    (1+division)* 10L +
			    (1+ero)     *  1L ;
			list.add(uscNo+"");
		}}}
		return list;
	}
	
	public List<String> getSubdivisions() {
		List<String> list = new ArrayList<String>();
		int circle = 0;
		int division = 0;
		int ero = 0;
		int sub_div = 0;
		long uscNo = 0;
		for (; circle <= NUM_CIRCLE; circle++) { division = 0;
		for (; division <= NUM_DIVISION; division++) { ero = 0;
		for (; ero <= NUM_ERO; ero++) { sub_div = 0;
		for (; sub_div <= NUM_SUB_DIV; sub_div++) {
			uscNo = 
				(1+circle)  *1000L +
			    (1+division)* 100L +
			    (1+ero)     *  10L +
			    (1+sub_div) *   1L ;
			list.add(uscNo+"");
		}}}}
		return list;
	}
	
	public List<String> getSections() {
		List<String> list = new ArrayList<String>();
		int circle = 0;
		int division = 0;
		int ero = 0;
		int sub_div = 0;
		int section = 0;
		long uscNo = 0;
		for (; circle <= NUM_CIRCLE; circle++) { division = 0;
		for (; division <= NUM_DIVISION; division++) { ero = 0;
		for (; ero <= NUM_ERO; ero++) { sub_div = 0;
		for (; sub_div <= NUM_SUB_DIV; sub_div++) { section = 0;
		for (; section <= NUM_SECTION; section++) {
			uscNo = 
				(1+circle)  *10000L +
			    (1+division)* 1000L +
			    (1+ero)     *  100L +
			    (1+sub_div) *   10L +
			    (1+section) *    1L ;
			list.add(uscNo+"");
		}}}}} 
		return list;
	}
	
	public List<String> getDistributions() {
		List<String> list = new ArrayList<String>();
		int circle = 0;
		int division = 0;
		int ero = 0;
		int sub_div = 0;
		int section = 0;
		int dist = 0;
		long uscNo = 0;
		for (; circle <= NUM_CIRCLE; circle++) { division = 0;
		for (; division <= NUM_DIVISION; division++) { ero = 0;
		for (; ero <= NUM_ERO; ero++) { sub_div = 0;
		for (; sub_div <= NUM_SUB_DIV; sub_div++) { section = 0;
		for (; section <= NUM_SECTION; section++) { dist = 0;
		for (; dist <= NUM_DIST; dist++) {
			uscNo = 
				(1+circle)  *1000000L +
			    (1+division)* 100000L +
			    (1+ero)     *  10000L +
			    (1+sub_div) *   1000L +
			    (1+section) *    100L +
			       (1+dist) *      1L ;
			list.add(uscNo+"");
		}}}}}} 
		return list;
	}

	public PrSetupData add(PrSetupData psd2) {
		PrSetupData psd = new PrSetupData();
		psd.NUM_CIRCLE = this.NUM_CIRCLE + psd2.NUM_CIRCLE;
		psd.NUM_COMPANY = this.NUM_COMPANY + psd2.NUM_COMPANY;
		psd.NUM_DIST = this.NUM_DIST + psd2.NUM_DIST;
		psd.NUM_DIVISION = this.NUM_DIVISION + psd2.NUM_DIVISION;
		psd.NUM_ERO = this.NUM_ERO + psd2.NUM_ERO;
		psd.NUM_SECTION = this.NUM_SECTION + psd2.NUM_SECTION;
		psd.NUM_SUB_DIV = this.NUM_SUB_DIV + psd2.NUM_SUB_DIV;
		psd.NUM_SC = 1 + this.NUM_SC + psd2.NUM_SC;
		
		psd.acd = this.acd;
		psd.agl = this.agl;
		psd.and = this.and;
		psd.rc = this.rc;
		
		return psd;
	}
	
	public static String getCurrentDateInDevicePrFormat() {
		SimpleDateFormat sdf = new SimpleDateFormat(ServiceColumns.MACHINE_DATE_FORMAT);
		return sdf.format(new Date(System.currentTimeMillis()));
	}
	
	public static String getDateInDevicePrFormat(Calendar cal) {
		SimpleDateFormat sdf = new SimpleDateFormat(ServiceColumns.MACHINE_DATE_FORMAT);
		return sdf.format(cal.getTime().getTime());
	}
	
	public static String getCurrentTimestampInDevicePrFormat() {
		SimpleDateFormat sdf = new SimpleDateFormat(ServiceColumns.MACHINE_TIME_FORMAT);
		return sdf.format(new Date(System.currentTimeMillis()));
	}	
}

class PrHead {
	public double and = 0;
	public double rc = 0;
	public double acd = 0;
	public double agl = 0;
	public double grandTotal = 0;
	public int prCount = 0;
	
	public PrHead() {
		init();
	}
	
	public void init() {
		and = 0;
		rc = 0;
		acd = 0;
		agl = 0;
	}
	
	public PrHead multiply(int multiplier) {
		PrHead ph = new PrHead();
		ph.and = this.and * multiplier;
		ph.rc = this.rc * multiplier;
		ph.acd = this.acd * multiplier;
		ph.agl = this.agl * multiplier;
		ph.grandTotal = ph.and + ph.rc + ph.acd + ph.agl;
		ph.prCount = multiplier;
		return ph;
	}
	
	public PrHead divide(int divisor) {
		PrHead ph = new PrHead();
		ph.and = this.and / divisor;
		ph.rc = this.rc / divisor;
		ph.acd = this.acd / divisor;
		ph.agl = this.agl / divisor;
		ph.prCount = this.prCount / divisor;
		ph.grandTotal = ph.and + ph.rc + ph.acd + ph.agl;
		return ph;
	}
	
}