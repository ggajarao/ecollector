package ebilly.admin.server.bo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

//import com.google.appengine.tools.development.testing.LocalBlobstoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

import ebilly.admin.server.core.reports.ResultAggregator;
import ebilly.admin.server.db.DevicePr;
import ebilly.admin.server.db.PrSummaryData;
import ebilly.admin.shared.viewdef.ResultRecord;
import ebilly.admin.shared.viewdef.SearchRequest;
import ebilly.admin.shared.viewdef.SearchResult;
import ecollector.common.PrColumns;
import ecollector.common.ReportAbstractColumns;
import ecollector.common.ServiceColumns;


public class PrBoSummerizerTest extends TestCase {
	
	private final LocalServiceTestHelper helper =
        new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());
	/*private final LocalServiceTestHelper blobStoreHelper =
        new LocalServiceTestHelper(new LocalBlobstoreServiceTestConfig());*/
	static final int BATCH_SIZE = 1000;
	
	@Before
	protected void setUp() throws Exception {
		helper.setUp();
		//blobStoreHelper.setUp();
		PrSummaryTestUtils.clearExistingAggs();
	}
	
	@After
	protected void tearDown() throws Exception {
		try {
			//blobStoreHelper.tearDown();
			helper.tearDown();
		} catch (Throwable t) {
			t.printStackTrace();
		}
		
	}
	
	public void testCreateOrFetchPrSummaryByMonth() {
		PrSummaryData oldMonthPrSummary = 
			PrSummaryData.createOrFetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		
		PrSummaryData newMonthPrSummary = 
			PrSummaryData.createOrFetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		
		assertTrue("Refetching months pr data returned different record",
				oldMonthPrSummary.getId().equals(newMonthPrSummary.getId()));
	}
	
	@Test
	public void testSummarizePrsWithOneSC() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 0;
				NUM_DIVISION = 0;
				NUM_ERO = 0;
				NUM_SUB_DIV = 0;
				NUM_SECTION = 0;
				NUM_DIST = 0; // 2 digit
				NUM_SC = 0;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		assertMonthDimensions(monthPrSummary, psd1);
	}
	
	@Test
	public void testSummarizePrsWithTwoSC() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 0;
				NUM_DIVISION = 0;
				NUM_ERO = 0;
				NUM_SUB_DIV = 0;
				NUM_SECTION = 0;
				NUM_DIST = 0; // 2 digit
				NUM_SC = 1;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		assertMonthDimensions(monthPrSummary, psd1);
	}
	
	@Test
	public void testSummarizePrsWithNSC() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 0;
				NUM_DIVISION = 0;
				NUM_ERO = 0;
				NUM_SUB_DIV = 0;
				NUM_SECTION = 0;
				NUM_DIST = 0; // 2 digit
				NUM_SC = 27;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		assertMonthDimensions(monthPrSummary, psd1);
	}
	
	@Test
	public void testSummarizePrsWithN1SC() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 0;
				NUM_DIVISION = 0;
				NUM_ERO = 0;
				NUM_SUB_DIV = 0;
				NUM_SECTION = 0;
				NUM_DIST = 0; // 2 digit
				NUM_SC = 28;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		assertMonthDimensions(monthPrSummary, psd1);
	}
	
	@Test
	public void testSummarizePrsWithOneDist() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 0;
				NUM_DIVISION = 0;
				NUM_ERO = 0;
				NUM_SUB_DIV = 0;
				NUM_SECTION = 0;
				NUM_DIST = 0; // 2 digit
				NUM_SC = 3;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		assertMonthDimensions(monthPrSummary, psd1);
	}
	
	@Test
	public void testSummarizePrsWithTwoDist() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 0;
				NUM_DIVISION = 0;
				NUM_ERO = 0;
				NUM_SUB_DIV = 0;
				NUM_SECTION = 0;
				NUM_DIST = 1; // 2 digit
				NUM_SC = 3;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		assertMonthDimensions(monthPrSummary, psd1);
	}
	
	@Test
	public void testSummarizePrsWithNDist() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 0;
				NUM_DIVISION = 0;
				NUM_ERO = 0;
				NUM_SUB_DIV = 0;
				NUM_SECTION = 0;
				NUM_DIST = 13; // 2 digit
				NUM_SC = 2;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		assertMonthDimensions(monthPrSummary, psd1);
	}
	
	@Test
	public void testSummarizePrsWithN1Dist() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 0;
				NUM_DIVISION = 0;
				NUM_ERO = 0;
				NUM_SUB_DIV = 0;
				NUM_SECTION = 0;
				NUM_DIST = 14; // 2 digit
				NUM_SC = 1;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		assertMonthDimensions(monthPrSummary, psd1);
	}
	
	@Test
	public void testSummarizePrsWithOneSection() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 0;
				NUM_DIVISION = 0;
				NUM_ERO = 0;
				NUM_SUB_DIV = 0;
				NUM_SECTION = 0;
				NUM_DIST = 0; // 2 digit
				NUM_SC = 4;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		assertMonthDimensions(monthPrSummary, psd1);
	}
	
	@Test
	public void testSummarizePrsWithTwoSections() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 0;
				NUM_DIVISION = 0;
				NUM_ERO = 0;
				NUM_SUB_DIV = 0;
				NUM_SECTION = 1;
				NUM_DIST = 0; // 2 digit
				NUM_SC = 4;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		assertMonthDimensions(monthPrSummary, psd1);
	}
	
	@Test
	public void testSummarizePrsWithNSections() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 0;
				NUM_DIVISION = 0;
				NUM_ERO = 0;
				NUM_SUB_DIV = 0;
				NUM_SECTION = 5;
				NUM_DIST = 0; // 2 digit
				NUM_SC = 4;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		assertMonthDimensions(monthPrSummary, psd1);
	}
	
	@Test
	public void testSummarizePrsWithN1Sections() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 0;
				NUM_DIVISION = 0;
				NUM_ERO = 0;
				NUM_SUB_DIV = 0;
				NUM_SECTION = 6;
				NUM_DIST = 0; // 2 digit
				NUM_SC = 4;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		assertMonthDimensions(monthPrSummary, psd1);
	}
	
	@Test
	public void testSummarizePrsWithOneSubdivision() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 0;
				NUM_DIVISION = 0;
				NUM_ERO = 0;
				NUM_SUB_DIV = 0;
				NUM_SECTION = 3;
				NUM_DIST = 2; // 2 digit
				NUM_SC = 0;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		assertMonthDimensions(monthPrSummary, psd1);
	}
	
	@Test
	public void testSummarizePrsWithTwoSubdivisions() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 0;
				NUM_DIVISION = 0;
				NUM_ERO = 0;
				NUM_SUB_DIV = 1;
				NUM_SECTION = 3;
				NUM_DIST = 2; // 2 digit
				NUM_SC = 0;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		assertMonthDimensions(monthPrSummary, psd1);
	}
	
	@Test
	public void testSummarizePrsWithNSubdivisions() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 0;
				NUM_DIVISION = 0;
				NUM_ERO = 0;
				NUM_SUB_DIV = 7;
				NUM_SECTION = 3;
				NUM_DIST = 2; // 2 digit
				NUM_SC = 0;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		assertMonthDimensions(monthPrSummary, psd1);
	}
	
	@Test
	public void testSummarizePrsWithN1Subdivision() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 0;
				NUM_DIVISION = 0;
				NUM_ERO = 0;
				NUM_SUB_DIV = 8;
				NUM_SECTION = 3;
				NUM_DIST = 2; // 2 digit
				NUM_SC = 0;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		assertMonthDimensions(monthPrSummary, psd1);
	}
	
	@Test
	public void testSummarizePrsWithOneEro() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 0;
				NUM_DIVISION = 0;
				NUM_ERO = 0;
				NUM_SUB_DIV = 1;
				NUM_SECTION = 1;
				NUM_DIST = 1; // 2 digit
				NUM_SC = 2;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		assertMonthDimensions(monthPrSummary, psd1);
	}
	
	@Test
	public void testSummarizePrsWithTwoEros() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 0;
				NUM_DIVISION = 0;
				NUM_ERO = 1;
				NUM_SUB_DIV = 1;
				NUM_SECTION = 3;
				NUM_DIST = 2; // 2 digit
				NUM_SC = 0;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		assertMonthDimensions(monthPrSummary, psd1);
	}
	
	@Test
	public void testSummarizePrsWithNEros() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 0;
				NUM_DIVISION = 0;
				NUM_ERO = 4;
				NUM_SUB_DIV = 7;
				NUM_SECTION = 3;
				NUM_DIST = 2; // 2 digit
				NUM_SC = 0;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		assertMonthDimensions(monthPrSummary, psd1);
	}
	
	@Test
	public void testSummarizePrsWithN1Eros() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 0;
				NUM_DIVISION = 0;
				NUM_ERO = 5;
				NUM_SUB_DIV = 8;
				NUM_SECTION = 3;
				NUM_DIST = 2; // 2 digit
				NUM_SC = 0;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		assertMonthDimensions(monthPrSummary, psd1);
	}
	
	@Test
	public void testSummarizePrsWithOneDivision() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 0;
				NUM_DIVISION = 0;
				NUM_ERO = 2;
				NUM_SUB_DIV = 4;
				NUM_SECTION = 3;
				NUM_DIST = 2; // 2 digit
				NUM_SC = 0;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		assertMonthDimensions(monthPrSummary, psd1);
	}
	
	@Test
	public void testSummarizePrsWithTwoDivisions() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 0;
				NUM_DIVISION = 1;
				NUM_ERO = 2;
				NUM_SUB_DIV = 4;
				NUM_SECTION = 3;
				NUM_DIST = 2; // 2 digit
				NUM_SC = 0;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		assertMonthDimensions(monthPrSummary, psd1);
	}
	
	@Test
	public void testSummarizePrsWithNDivisions() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 0;
				NUM_DIVISION = 7;
				NUM_ERO = 2;
				NUM_SUB_DIV = 4;
				NUM_SECTION = 3;
				NUM_DIST = 2; // 2 digit
				NUM_SC = 0;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		assertMonthDimensions(monthPrSummary, psd1);
	}
	
	@Test
	public void testSummarizePrsWithN1Divisions() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 0;
				NUM_DIVISION = 8;
				NUM_ERO = 2;
				NUM_SUB_DIV = 4;
				NUM_SECTION = 3;
				NUM_DIST = 2; // 2 digit
				NUM_SC = 0;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		assertMonthDimensions(monthPrSummary, psd1);
	}
	
	@Test
	public void testSummarizePrsWithOneCircle() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 0;
				NUM_DIVISION = 2;
				NUM_ERO = 1;
				NUM_SUB_DIV = 4;
				NUM_SECTION = 3;
				NUM_DIST = 2; // 2 digit
				NUM_SC = 1;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		assertMonthDimensions(monthPrSummary, psd1);
	}
	
	@Test
	public void testSummarizePrsWithTwoCircles() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 1;
				NUM_DIVISION = 2;
				NUM_ERO = 2;
				NUM_SUB_DIV = 4;
				NUM_SECTION = 3;
				NUM_DIST = 2; // 2 digit
				NUM_SC = 2;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		assertMonthDimensions(monthPrSummary, psd1);
	}
	
	@Test
	public void testSummarizePrsWithNCircles() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 4;
				NUM_DIVISION = 2;
				NUM_ERO = 3;
				NUM_SUB_DIV = 4;
				NUM_SECTION = 1;
				NUM_DIST = 2; // 2 digit
				NUM_SC = 0;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		assertMonthDimensions(monthPrSummary, psd1);
	}
	
	@Test
	public void testSummarizePrsWithN1Circles() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 5;
				NUM_DIVISION = 2;
				NUM_ERO = 4;
				NUM_SUB_DIV = 4;
				NUM_SECTION = 1;
				NUM_DIST = 2; // 2 digit
				NUM_SC = 0;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		assertMonthDimensions(monthPrSummary, psd1);
	}
	
	// USC Number does not support company, so assumption
	// is that one cloud instance will be dedicated one company.
	/*@Test
	public void testSummarizePrsWithTwoCompanies() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 1;
				NUM_CIRCLE = 3;
				NUM_DIVISION = 1;
				NUM_ERO = 0;
				NUM_SUB_DIV = 2;
				NUM_SECTION = 1;
				NUM_DIST = 1; // 2 digit
				NUM_SC = 3;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		assertMonthDimensions(monthPrSummary, psd1);
	}*/
	
	
	@Test
	public void testSummarizePrsWithDuplicatePush() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		DevicePr pr = PrSummaryTestUtils.createPr(psd1, "1111100900001");
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		
		ResultAggregator dimDistribution = 
			monthPrSummary.dimensions().getDimDistribution();
		int expectedResultsSize = 1;
		PrHead prHead = new PrHead(){
			public void init() {
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
				grandTotal = and+rc+acd+agl;
				prCount = 1;
			}
		};
		assertIncrementalPrHeads("Distribution",dimDistribution,expectedResultsSize, monthPrSummary, prHead);		
		
		ResultAggregator dimSection = 
			monthPrSummary.dimensions().getDimSection();
		assertIncrementalPrHeads("Section",dimSection,expectedResultsSize, monthPrSummary, prHead);
		
		ResultAggregator dimSubdivision = 
			monthPrSummary.dimensions().getDimSubdivision();
		assertIncrementalPrHeads("Subdivision",dimSubdivision,expectedResultsSize, monthPrSummary, prHead);
		
		ResultAggregator dimEro =
			monthPrSummary.dimensions().getDimEro();
		assertIncrementalPrHeads("ERO",dimEro,expectedResultsSize, monthPrSummary, prHead);
		
		ResultAggregator dimDivision = 
			monthPrSummary.dimensions().getDimDivision();
		assertIncrementalPrHeads("Division",dimDivision,expectedResultsSize, monthPrSummary, prHead);
		
		ResultAggregator dimCircle = 
			monthPrSummary.dimensions().getDimCircle();
		assertIncrementalPrHeads("Circle",dimCircle,expectedResultsSize, monthPrSummary, prHead);
		
		ResultAggregator dimCompany = 
			monthPrSummary.dimensions().getDimCompany();
		assertIncrementalPrHeads("Company",dimCompany,expectedResultsSize, monthPrSummary, prHead);
		
		PrSummaryTestUtils.createPr(psd1, "1111100900001");
		//pr.save();
		
		// Add another pr in exsting distribution
		PrSummaryTestUtils.createPr(psd1, "1111100900002");
		PrBo.summarizePrs();
		monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		PrHead prHeadDist = prHead.multiply(2); // Only distribution dimension should get updated
		dimDistribution = monthPrSummary.dimensions().getDimDistribution();
		assertIncrementalPrHeads("Distribution",dimDistribution,expectedResultsSize, monthPrSummary, prHeadDist);
		
		dimSection = monthPrSummary.dimensions().getDimSection();
		assertIncrementalPrHeads("Section",dimSection,expectedResultsSize, monthPrSummary, prHeadDist);
		
		dimSubdivision = monthPrSummary.dimensions().getDimSubdivision();
		assertIncrementalPrHeads("Subdivision",dimSubdivision,expectedResultsSize, monthPrSummary, prHeadDist);
		
		dimEro= monthPrSummary.dimensions().getDimEro();
		assertIncrementalPrHeads("ERO",dimEro,expectedResultsSize, monthPrSummary, prHeadDist);
		
		dimDivision = monthPrSummary.dimensions().getDimDivision();
		assertIncrementalPrHeads("Division",dimDivision,expectedResultsSize, monthPrSummary, prHeadDist);
		
		dimCircle = monthPrSummary.dimensions().getDimCircle();
		assertIncrementalPrHeads("Circle",dimCircle,expectedResultsSize, monthPrSummary, prHeadDist);
		
		dimCompany = monthPrSummary.dimensions().getDimCompany();
		assertIncrementalPrHeads("Company",dimCompany,expectedResultsSize, monthPrSummary, prHeadDist);
		
		
	}
	
	@Test
	public void testSummarizePrsIncremental() throws Exception {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		DevicePr pr = PrSummaryTestUtils.createPr(psd1, "1111100900001");
		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		
		ResultAggregator dimDistribution = 
			monthPrSummary.dimensions().getDimDistribution();
		int expectedResultsSize = 1;
		PrHead prHead = new PrHead(){
			public void init() {
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
				grandTotal = and+rc+acd+agl;
				prCount = 1;
			}
		};
		assertIncrementalPrHeads("Distribution",dimDistribution,expectedResultsSize, monthPrSummary, prHead);		
		
		ResultAggregator dimSection = 
			monthPrSummary.dimensions().getDimSection();
		assertIncrementalPrHeads("Section",dimSection,expectedResultsSize, monthPrSummary, prHead);
		
		ResultAggregator dimSubdivision = 
			monthPrSummary.dimensions().getDimSubdivision();
		assertIncrementalPrHeads("Subdivision",dimSubdivision,expectedResultsSize, monthPrSummary, prHead);
		
		ResultAggregator dimEro =
			monthPrSummary.dimensions().getDimEro();
		assertIncrementalPrHeads("ERO",dimEro,expectedResultsSize, monthPrSummary, prHead);
		
		ResultAggregator dimDivision = 
			monthPrSummary.dimensions().getDimDivision();
		assertIncrementalPrHeads("Division",dimDivision,expectedResultsSize, monthPrSummary, prHead);
		
		ResultAggregator dimCircle = 
			monthPrSummary.dimensions().getDimCircle();
		assertIncrementalPrHeads("Circle",dimCircle,expectedResultsSize, monthPrSummary, prHead);
		
		ResultAggregator dimCompany = 
			monthPrSummary.dimensions().getDimCompany();
		assertIncrementalPrHeads("Company",dimCompany,expectedResultsSize, monthPrSummary, prHead);
		
		// Add another pr in exsting distribution
		PrSummaryTestUtils.createPr(psd1, "1111100900002");
		PrBo.summarizePrs();
		monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		PrHead prHeadDist = prHead.multiply(2); // Only distribution dimension should get updated
		dimDistribution = monthPrSummary.dimensions().getDimDistribution();
		assertIncrementalPrHeads("Distribution",dimDistribution,expectedResultsSize, monthPrSummary, prHeadDist);
		
		dimSection = monthPrSummary.dimensions().getDimSection();
		assertIncrementalPrHeads("Section",dimSection,expectedResultsSize, monthPrSummary, prHeadDist);
		
		dimSubdivision = monthPrSummary.dimensions().getDimSubdivision();
		assertIncrementalPrHeads("Subdivision",dimSubdivision,expectedResultsSize, monthPrSummary, prHeadDist);
		
		dimEro= monthPrSummary.dimensions().getDimEro();
		assertIncrementalPrHeads("ERO",dimEro,expectedResultsSize, monthPrSummary, prHeadDist);
		
		dimDivision = monthPrSummary.dimensions().getDimDivision();
		assertIncrementalPrHeads("Division",dimDivision,expectedResultsSize, monthPrSummary, prHeadDist);
		
		dimCircle = monthPrSummary.dimensions().getDimCircle();
		assertIncrementalPrHeads("Circle",dimCircle,expectedResultsSize, monthPrSummary, prHeadDist);
		
		dimCompany = monthPrSummary.dimensions().getDimCompany();
		assertIncrementalPrHeads("Company",dimCompany,expectedResultsSize, monthPrSummary, prHeadDist);
		
		
		// Add more prs in exsting Subdivisions across two sections
		// Create three prs in section 11112"
		String sectionName_2 = "11112";
		PrSummaryTestUtils.createPr(psd1, sectionName_2+"00900010");
		PrSummaryTestUtils.createPr(psd1, sectionName_2+"00900011");
		PrSummaryTestUtils.createPr(psd1, sectionName_2+"00900012");
		
		// Create two prs in section 11113"
		String sectionName_3 = "11113";
		PrSummaryTestUtils.createPr(psd1, sectionName_3+"00900010");
		PrSummaryTestUtils.createPr(psd1, sectionName_3+"00900011");
		
		PrBo.summarizePrs();
		monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		
		ResultAggregator dimSection3 = monthPrSummary.dimensions().getDimSection();
		assertTrue("Section dimension is missing", dimSection3 != null);
		SearchResult sectionResults = dimSection3.getAggregateSearchResult();
		
		assertTrue("Section dimension results are missing", sectionResults != null);
		assertTrue("Section Results size is missmatching, expecting: "+3+", got: "+sectionResults.getResults().size(),
				sectionResults.getResults().size() == 3);
		SearchRequest sr = dimSection3.getSearchRequest();
		
		// Assert Section 11112 head values 
	 	List<ResultRecord> section2Records = sr.findRecords(PrColumns._SECTION_CODE, sectionName_2, sectionResults.getResults());
		assertTrue("Found more than one or zero records for section: "+sectionName_2, 
				section2Records.size() == 1);
		ResultRecord sectionRecord = section2Records.get(0);
		PrHead sectionPrHead = prHead.multiply(3);// since this is a new section and we added three Prs to it.
		PrSummaryTestUtils.assertRecordHeads(sr, "Section "+sectionName_2, sectionPrHead, sectionRecord);
		//assertRecordValues("Section "+sectionName_2,sectionRecord,sr,sectionPrHead);
		
		// Assert Section 11113 head values
		List<ResultRecord> section3Records = sr.findRecords(PrColumns._SECTION_CODE, sectionName_3, sectionResults.getResults());
		assertTrue("Found more than one or zero records for section: "+sectionName_3, 
				section3Records.size() == 1);
		ResultRecord section3Record = section3Records.get(0);
		PrHead section3PrHead = prHead.multiply(2);// since this is a new section and we added two Prs to it.
		PrSummaryTestUtils.assertRecordHeads(sr, "Section "+sectionName_3, section3PrHead, section3Record);
		//assertRecordValues("Section "+sectionName_3,section3Record,sr,section3PrHead);
		
		// Assert Section 11111 head values
		String sectionName_1 = "11111"; 
		List<ResultRecord> section1Records = sr.findRecords(PrColumns._SECTION_CODE, sectionName_1, sectionResults.getResults());
		assertTrue("Found more than one or zero records for section: "+sectionName_3, 
				section1Records.size() == 1);
		ResultRecord section1Record = section1Records.get(0);
		PrHead section1PrHead = prHead.multiply(2);// this should remain same, as no corresponding PRs are added, existing two PRs sum should match
		PrSummaryTestUtils.assertRecordHeads(sr, "Section "+sectionName_1, section1PrHead, section1Record);
		//assertRecordValues("Section "+sectionName_1,section1Record,sr,section1PrHead);
		
		// Now assert Subdivision Pr Heads, it should tally to 7 PRs sum.
		ResultAggregator dimSubdivision2 = monthPrSummary.dimensions().getDimSubdivision();
		assertTrue("Subdivision dimension is missing", dimSubdivision2 != null);
		SearchResult subdivisionResults = dimSubdivision2.getAggregateSearchResult();
		
		assertTrue("Subdivision dimension results are missing", subdivisionResults != null);
		assertTrue("Subdivision Results size is missmatching, expecting: "+1+", got: "+subdivisionResults.getResults().size(),
				subdivisionResults.getResults().size() == 1);
		sr = dimSubdivision2.getSearchRequest();
		ResultRecord subdivisionRecord = subdivisionResults.getResults().get(0);
		PrHead subdivisionPrHead = prHead.multiply(7);
		PrSummaryTestUtils.assertRecordHeads(sr, "Subdivision", subdivisionPrHead, subdivisionRecord);
		//assertRecordValues("Subdivision",subdivisionRecord,sr,subdivisionPrHead);		
	}	
	
	/**
	 * This test case intended to test if the summarization fails
	 * in the middle of processing pr's.
	 * Use cases covered in this test are:
	 * 1. If summarization failed at month pr's processing.
	 * 2. If summarization failed at batch level processing.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSummarizePrsFailuresInBatches() throws Exception {
		final int numberOfPrs = 4000;
		final SimpleDateFormat sdf = new SimpleDateFormat(ServiceColumns.MACHINE_DATE_FORMAT);
		int j=0;
		Calendar cal = null;
		PrSetupData psd1 = null;
		final int numberOfMonths = 5;
		for(int i = 0; i < numberOfMonths; i++) {
			cal = Calendar.getInstance();
			final int k = i;
			final int numberOfServices = numberOfPrs/numberOfMonths;
			cal.add(Calendar.MONTH, (j-i));
			final Calendar calendar = cal;
			psd1 = new PrSetupData() {
				public void init() {
					and = 230;
					rc = 75;
					acd = 150;
					agl = 30;
					NUM_SC = numberOfServices;
					collectionDate = sdf.format(calendar.getTime());
					deviceName = "DeviceNameX"+k;
				}
			};
			
			PrSummaryTestUtils.setupPrs(psd1);
		}
		
		int failAtBatchNumber = 2;
		int failAtMonthNumber = 3;
		PrBo.JUnitBatchProcessContext.setFailAtBacthNumber(failAtBatchNumber);
		PrBo.JUnitBatchProcessContext.setFailAtMonthNumber(failAtMonthNumber);

		int expectedResultsSize = 1;
		int resultsPerBatch = (numberOfPrs/numberOfMonths)+1;
		int resultPrsSize = resultsPerBatch;

		PrBo.summarizePrs();
		MonthsPrSummary monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		
		assertMonthData(monthPrSummary, resultPrsSize, expectedResultsSize);
		
		cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		
		monthPrSummary = 
				PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(cal));

		resultPrsSize = BATCH_SIZE - resultsPerBatch;
		assertMonthData(monthPrSummary, resultPrsSize, expectedResultsSize);

		failAtBatchNumber = 1;
		PrBo.JUnitBatchProcessContext.setFailAtBacthNumber(failAtBatchNumber);
		
		PrBo.summarizePrs();
		monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		
		resultPrsSize = resultsPerBatch;
		assertMonthData(monthPrSummary, resultPrsSize, expectedResultsSize);
		
		cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		
		monthPrSummary = 
				PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(cal));

		resultPrsSize = BATCH_SIZE - resultsPerBatch;
		assertMonthData(monthPrSummary, resultPrsSize, expectedResultsSize);
		
		failAtBatchNumber = 3;
		PrBo.JUnitBatchProcessContext.setFailAtBacthNumber(failAtBatchNumber);
		
		PrBo.summarizePrs();
		monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		
		expectedResultsSize = 1;
		resultsPerBatch = (numberOfPrs/numberOfMonths)+1;
		resultPrsSize = resultsPerBatch;
		
		assertMonthData(monthPrSummary, resultPrsSize, expectedResultsSize);
		
		cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		
		monthPrSummary = 
				PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(cal));

		assertMonthData(monthPrSummary, resultPrsSize, expectedResultsSize);
		
		cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -2);
		monthPrSummary = 
				PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(cal));

		assertMonthData(monthPrSummary, resultPrsSize, expectedResultsSize);
		
		cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -3);
		monthPrSummary = 
				PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(cal));
		resultPrsSize = ((3 * BATCH_SIZE) - (resultsPerBatch * 3));
		
		assertMonthData(monthPrSummary, resultPrsSize, expectedResultsSize);		
		
		PrBo.summarizePrs();
		monthPrSummary = 
			PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		
		resultPrsSize = resultsPerBatch;
		assertMonthData(monthPrSummary, resultPrsSize, expectedResultsSize);
		
		cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		
		monthPrSummary = 
				PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(cal));

		assertMonthData(monthPrSummary, resultPrsSize, expectedResultsSize);

		cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -2);
		monthPrSummary = 
				PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(cal));
		
		assertMonthData(monthPrSummary, resultPrsSize, expectedResultsSize);
		
		cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -3);
		monthPrSummary = 
				PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(cal));
		
		assertMonthData(monthPrSummary, resultPrsSize, expectedResultsSize);
		
		cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -4);
		monthPrSummary = 
				PrBo.fetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(cal));
		
		assertMonthData(monthPrSummary, resultPrsSize, expectedResultsSize);

	}
	
	private void assertMonthData(MonthsPrSummary monthPrSummary, int resultPrsSize,
			int expectedDimensionsCount) {
		
		ResultAggregator dimDistribution = monthPrSummary.dimensions().getDimDistribution();
		
		SearchResult searchResult = dimDistribution.getAggregateSearchResult();
		List<ResultRecord> results = searchResult.getResults();
		//int expectedResultsSize = 1;
		String dimName = "Distribution";
		assertTrue(dimName+" dimension results are missing", !results.isEmpty());
		assertTrue(dimName+" dimension record count mismatch. Expected: "+
				expectedDimensionsCount+", got: "+results.size(), 
				expectedDimensionsCount == results.size());
		SearchRequest searchRequest = monthPrSummary.dimensions().getSearchRequest();
		
		PrHead prHead = new PrHead(){
			public void init() {
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
				grandTotal = and+rc+acd+agl;
				prCount = 1;
			}
		};
		prHead = prHead.multiply(resultPrsSize);
		assertNotNull("Distribution record got null", results.get(0));
		PrSummaryTestUtils.assertRecordHeads(searchRequest, dimName, prHead, results.get(0));

	}
	
	private void assertIncrementalPrHeads(String dimName, ResultAggregator dim, int expectedResultsSize,
			MonthsPrSummary monthPrSummary, PrHead prHead) {
		assertTrue(dimName+" dimension is missing", dim != null);
		SearchResult searchResult = dim.getAggregateSearchResult();
		List<ResultRecord> results = searchResult.getResults();
		assertTrue(dimName+" dimension results are missing", !results.isEmpty());
		assertTrue(dimName+" dimension record count mismatch. Expected: "+
				expectedResultsSize+", got: "+results.size(), 
				expectedResultsSize == results.size());
		SearchRequest searchRequest = monthPrSummary.dimensions().getSearchRequest();
		for (ResultRecord record : results ) {
			//assertRecordValues(dimName,record,searchRequest,prHead);
			PrSummaryTestUtils.assertRecordHeads(searchRequest, dimName, prHead, record);
		}
	}
	
	private void del_assertRecordValues(String dimName, ResultRecord record, 
			SearchRequest searchRequest, PrHead prHead) {
		String and = searchRequest.getValue(ReportAbstractColumns.ARREARS_N_DEMAND, record);
		assertTrue(dimName+" dimension, value 'Arrears and Demand' missmatch. Expected: "+prHead.and+", got: "+and,
				and.equals(prHead.and+""));
		String acd = searchRequest.getValue(ReportAbstractColumns.ACD_COLLECTED, record);
		assertTrue(dimName+" dimension, value ACD missmatch. Expected: "+prHead.acd+", got: "+acd,
				acd.equals(prHead.acd+""));
		String agl = searchRequest.getValue(ReportAbstractColumns.AGL_AMOUNT, record);
		assertTrue(dimName+" dimension, value AGL missmatch. Expected: "+prHead.agl+", got: "+agl,
				agl.equals(prHead.agl+""));
		String rc = searchRequest.getValue(ReportAbstractColumns.RC_COLLECTED, record);
		assertTrue(dimName+" dimension, value RC missmatch. Expected: "+prHead.rc+", got: "+rc,
				rc.equals(prHead.rc+""));
	}

	private void assertMonthDimensions(MonthsPrSummary monthPrSummary, PrSetupData psd1) {
		ResultAggregator dimDistribution = 
			monthPrSummary.dimensions().getDimDistribution();
		PrHead distributionSum = psd1.getDistributionSum();
		assertHeads(distributionSum,dimDistribution, psd1, monthPrSummary, "Distribution", psd1.getDistributionsCount());
		
		ResultAggregator dimSection = 
			monthPrSummary.dimensions().getDimSection();
		PrHead sectionSum = psd1.getSectionSum();
		assertHeads(sectionSum,dimSection, psd1, monthPrSummary, "Section", psd1.getSectionsCount());
		
		ResultAggregator dimSubdivision = 
			monthPrSummary.dimensions().getDimSubdivision();
		PrHead subdivisionSum = psd1.getSubdivisionSum();
		assertHeads(subdivisionSum,dimSubdivision, psd1, monthPrSummary, "Subdivision", psd1.getSubDivCount());
		
		ResultAggregator dimDivision = 
			monthPrSummary.dimensions().getDimDivision();
		PrHead divisionSum = psd1.getDivisionSum();
		assertHeads(divisionSum,dimDivision, psd1, monthPrSummary, "Division", psd1.getDivisionsCount());
		
		ResultAggregator dimEro = 
			monthPrSummary.dimensions().getDimEro();
		PrHead eroSum = psd1.getEroSum();
		assertHeads(eroSum,dimEro, psd1, monthPrSummary, "ERO", psd1.getEroCount());
		
		ResultAggregator dimCircle = 
			monthPrSummary.dimensions().getDimCircle();
		PrHead circleSum = psd1.getCircleSum();
		assertHeads(circleSum,dimCircle, psd1, monthPrSummary, "Circle", psd1.getCirclesCount());
		
		ResultAggregator dimCompany = 
			monthPrSummary.dimensions().getDimCompany();
		PrHead companySum = psd1.getCompanySum();
		assertHeads(companySum,dimCompany, psd1, monthPrSummary, "Company", psd1.getCompanyCount());
	}

	private void assertHeads(PrHead prHead,
			ResultAggregator dim, PrSetupData psd, 
			MonthsPrSummary monthPrSummary, String dimName,
			int expectedResultsSize) {
		assertTrue(dimName+" dimension is missing", dim != null);
		SearchResult searchResult = dim.getAggregateSearchResult();
		List<ResultRecord> results = searchResult.getResults();
		assertTrue(dimName+" dimension results are missing", !results.isEmpty());
		assertTrue(dimName+" dimension record count mismatch. Expected: "+
				expectedResultsSize+", got: "+results.size(), 
				expectedResultsSize == results.size());
		SearchRequest searchRequest = monthPrSummary.dimensions().getSearchRequest();
		for (ResultRecord record : results ) {
			PrSummaryTestUtils.assertRecordHeads(searchRequest, dimName, prHead, record);
		}
	}	
}
