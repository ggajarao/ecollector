package ebilly.admin.server.bo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import junit.framework.TestCase;

import org.junit.AfterClass;
import org.junit.BeforeClass;

import com.google.appengine.tools.development.testing.LocalBlobstoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.SummarySearchRequestFactory;
import ebilly.admin.shared.viewdef.ResultField;
import ebilly.admin.shared.viewdef.ResultRecord;
import ebilly.admin.shared.viewdef.SearchRequest;
import ebilly.admin.shared.viewdef.SearchResult;
import ecollector.common.PrColumns;
import ecollector.common.ReportAbstractColumns;

public class MonthPrSummarySearchHandlerTest extends TestCase {
	private final LocalServiceTestHelper helper =
        new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());
	private final LocalServiceTestHelper blobStoreHelper =
        new LocalServiceTestHelper(new LocalBlobstoreServiceTestConfig());
	
	private PrSetupData psd = new PrSetupData();
	
	// On this month the PRs are created
	private static final String monthFilterValue = "2014-4";
	
	public MonthPrSummarySearchHandlerTest() {
		
	}
	
	@BeforeClass
	protected void setUp() throws Exception {
		helper.setUp();
		blobStoreHelper.setUp();
		PrSummaryTestUtils.clearExistingAggs();
		setupPrData();
		summarizePrData();
	}
	
	@AfterClass
	protected void tearDown() throws Exception {
		try {
			blobStoreHelper.tearDown();
			//helper.tearDown();
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}
	
	private void summarizePrData() {
		PrBo.summarizePrs();
	}
	
	private void setupPrData() {
		psd.acd = 130.0;
		psd.agl = 60.0;
		psd.and = 117.0;
		psd.rc = 25.0;
		
		psd.NUM_CIRCLE = 3;
		psd.NUM_DIVISION = 3;
		psd.NUM_ERO = 2;
		psd.NUM_SUB_DIV = 2;
		psd.NUM_SECTION = 1;
		psd.NUM_DIST = 1;
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MONTH, 3);
		cal.set(Calendar.DAY_OF_MONTH, 17);
		cal.set(Calendar.YEAR, 2014);
		
		psd.deviceName = "A_";
		psd.collectionDate = PrSetupData.getDateInDevicePrFormat(cal);
		PrSummaryTestUtils.setupPrs(psd);
		
		psd.deviceName = "B_";
		cal.add(Calendar.DAY_OF_MONTH, -1);
		psd.collectionDate = PrSetupData.getDateInDevicePrFormat(cal);
		PrSummaryTestUtils.setupPrs(psd);
	}
	
	public void testDistributionSummarySearch() throws Exception {
		PrHead prHead = psd.asPrHead();
		// Since we created two prs for each distribution across two devices.
		//
		prHead = prHead.multiply(2 * ( psd.NUM_SC+1 ));
		assertSearchResults("Distribution", psd.getDistributions(), SummarySearchRequestFactory.distributionSearchRequestFactory,prHead);
	}
	
	public void testSectionSummarySearch() throws Exception {
		PrHead prHead = psd.asPrHead();
		// Since we created two prs across two devices.
		//
		prHead = prHead.multiply(2*( (psd.NUM_DIST+1) * ( psd.NUM_SC+1 )));
		assertSearchResults("Section", psd.getSections(), SummarySearchRequestFactory.sectionSearchRequestFactory,prHead);
	}
	
	public void testSubdivisionSummarySearch() throws Exception {
		PrHead prHead = psd.asPrHead();
		// Since we created two prs across two devices.
		//
		prHead = prHead.multiply(2*( (psd.NUM_SECTION+1) * (psd.NUM_DIST+1) * (psd.NUM_SC+1)));
		assertSearchResults("Subdivision", psd.getSubdivisions(), SummarySearchRequestFactory.subdivisionSearchRequestFactory,prHead);
	}
	
	public void testEroSummarySearch() throws Exception {
		PrHead prHead = psd.asPrHead();
		// Since we created two prs across two devices.
		//
		prHead = prHead.multiply(2*((psd.NUM_SUB_DIV+1) * (psd.NUM_SECTION+1) * (psd.NUM_DIST+1) * (psd.NUM_SC+1)));
		assertSearchResults("Ero", psd.getEros(), SummarySearchRequestFactory.eroSearchRequestFactory,prHead);
	}
	
	public void testDivisionSummarySearch() throws Exception {
		PrHead prHead = psd.asPrHead();
		// Since we created two prs across two devices.
		//
		prHead = prHead.multiply(2*((psd.NUM_ERO+1) * (psd.NUM_SUB_DIV+1) * (psd.NUM_SECTION+1) * (psd.NUM_DIST+1) * (psd.NUM_SC+1)));
		assertSearchResults("Division", psd.getDivisions(), SummarySearchRequestFactory.divisionSearchRequestFactory,prHead);
	}
	
	public void testCircleSummarySearch() throws Exception {
		PrHead prHead = psd.asPrHead();
		// Since we created two prs across two devices.
		//
		prHead = prHead.multiply(2*((psd.NUM_DIVISION+1) *(psd.NUM_ERO+1) * (psd.NUM_SUB_DIV+1) * (psd.NUM_SECTION+1) * (psd.NUM_DIST+1) * (psd.NUM_SC+1)));
		assertSearchResults("Circle", psd.getCircles(), SummarySearchRequestFactory.circleSearchRequestFactory,prHead);
	}
	
	public void testCompanySummarySearch() throws Exception {
		PrHead prHead = psd.asPrHead();
		// Since we created two prs across two devices.
		//
		prHead = prHead.multiply(2*((psd.NUM_CIRCLE+1) *(psd.NUM_DIVISION+1) *(psd.NUM_ERO+1) * (psd.NUM_SUB_DIV+1) * (psd.NUM_SECTION+1) * (psd.NUM_DIST+1) * (psd.NUM_SC+1)));
		assertSearchResults("Company", psd.getCompanies(), SummarySearchRequestFactory.companySearchRequestFactory,prHead);
	}
	
	private static void assertSearchResults(String dimName, List<String> dimValues, 
			SummarySearchRequestFactory srFactory, PrHead prHead) throws Exception {
		for (String dimValue : dimValues) {
			SearchRequest searchRequest = srFactory.createSearchRequest(dimValue, monthFilterValue);
			MonthPrSummarySearchHandler sh = new MonthPrSummarySearchHandler();
			SearchResult searchResult = 
				sh.fetchResults(searchRequest);
			List<ResultRecord> results = searchResult.getResults();
			assertTrue("Search for "+dimName+": "+dimValue+
					" resulted in wrong result count, expected: 3, got: "+results.size()
					, results.size() == 3);
			//First record should be month aggregate
			ResultRecord record = results.get(0);
			String monthAggregateLabel = searchRequest.getValue(PrColumns.COLLECTION_DATE, record);
			assertTrue("Aggregate month value is mismatching, expected: "
					+AppConfig.PR_SUMMARY_MONTH_AGGREGATE_LABEL
					+", got: "+monthAggregateLabel,
					AppConfig.PR_SUMMARY_MONTH_AGGREGATE_LABEL.equals(monthAggregateLabel));
			
			PrSummaryTestUtils.assertRecordHeads(searchRequest, 
					dimName+" Month Aggregate", 
					prHead, record);
			
			record = results.get(1);
			PrSummaryTestUtils.assertRecordHeads(searchRequest, 
					dimName+" Date Aggregate", 
					prHead.divide(2), record);
			
			record = results.get(2);
			PrSummaryTestUtils.assertRecordHeads(searchRequest, 
					dimName+" Date Aggregate", 
					prHead.divide(2), record);
			
		}
	}	
}