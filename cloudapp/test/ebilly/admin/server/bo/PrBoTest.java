package ebilly.admin.server.bo;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import junit.framework.TestCase;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

import ebilly.admin.server.db.DevicePr;
import ebilly.admin.server.db.OrgCompany;
import ebilly.admin.server.db.PrSummaryData;
import ebilly.admin.shared.orgstruct.OrgCompanyUi;

public class PrBoTest extends TestCase {

	private final LocalServiceTestHelper helper =
        new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());
	
	public PrBoTest() {
	}
	
	@Override
	protected void setUp() throws Exception {
		helper.setUp();
	}
	
	@Override
	protected void tearDown() throws Exception {
		helper.tearDown();
	}
	
	public void testRecordOrgStructure() {
		// Clear Org Structure if exists
		OrgCompany orgCompany = OrgCompany.fetchOrgCompany();
		if (orgCompany != null) {
			OrgCompany.delete(orgCompany.getId());
		}
		
		orgCompany = OrgCompany.fetchOrgCompany();
		assertTrue("Could not delete existing org structure", orgCompany == null);
		
		DevicePr devicePr = new DevicePr();
		String circleCode = "1";
		String divisionCode = "12";
		String eroCode = "123";
		String subdivisionCode = "1234";
		String sectionCode = "12345";
		String distributionCode = "1234567";
		String scNumber = "000001";
		
		String distributionName = "ABC";
		String sectionName = "DEF";
		String eroName = "GHIJK";
		String uscNo = 
			circleCode.substring(0, 1)+
			divisionCode.substring(1, 2)+
			eroCode.substring(2, 3)+
			subdivisionCode.substring(3, 4)+
			sectionCode.substring(4, 5)+
			distributionCode.substring(5, 7)+
			scNumber;
		devicePr.setUSC_NO(uscNo);
		devicePr.setDISTRIBUTION(distributionName);
		devicePr.setSECTION(sectionName);
		devicePr.setERO(eroName);
		devicePr.save();
		
		OrgCompanyUi companyUi = OrgCompany.cFetchOrgCompany();
		assertTrue("Org Structure is not recorded during PR save", companyUi != null);
		
		assertTrue("Circle is not recorded during PR save", companyUi.doesCircleExists(circleCode));
		assertTrue("Division is not recorded during PR save", companyUi.doesDivisionExists(divisionCode));
		assertTrue("ERO is not recorded during PR save", companyUi.doesEroExists(eroCode));
		assertTrue("Subdivision is not recorded during PR save", companyUi.doesSubdivisionExists(subdivisionCode));
		assertTrue("Section is not recorded during PR save", companyUi.doesSectionExists(sectionCode));
		assertTrue("Distribution is not recorded during PR save", companyUi.doesDistributionExists(distributionCode));
		
		circleCode = "2";
		divisionCode = "22";
		eroCode = "223";
		subdivisionCode = "2234";
		sectionCode = "22345";
		distributionCode = "2234567";
		scNumber = "000002";
		
		distributionName = "ABC1";
		sectionName = "DEF1";
		eroName = "GHIJK1";
		uscNo = 
			circleCode.substring(0, 1)+
			divisionCode.substring(1, 2)+
			eroCode.substring(2, 3)+
			subdivisionCode.substring(3, 4)+
			sectionCode.substring(4, 5)+
			distributionCode.substring(5, 7)+
			scNumber;
		devicePr.setUSC_NO(uscNo);
		devicePr.setDISTRIBUTION(distributionName);
		devicePr.setSECTION(sectionName);
		devicePr.setERO(eroName);
		devicePr.save();
		
		companyUi = OrgCompany.cFetchOrgCompany();
		assertTrue("Circle is not recorded during PR save", companyUi.doesCircleExists(circleCode));
		assertTrue("Division is not recorded during PR save", companyUi.doesDivisionExists(divisionCode));
		assertTrue("ERO is not recorded during PR save", companyUi.doesEroExists(eroCode));
		assertTrue("Subdivision is not recorded during PR save", companyUi.doesSubdivisionExists(subdivisionCode));
		assertTrue("Section is not recorded during PR save", companyUi.doesSectionExists(sectionCode));
		assertTrue("Distribution is not recorded during PR save", companyUi.doesDistributionExists(distributionCode));
	}
	
	public void testCFetchSummaryDataList() {
		// Create future month summary and fetch the keys
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, 1);
		String futureMonth = PrSummaryData.createMonthKey(cal);
		PrSummaryData.createOrFetchPrSummaryByMonth(futureMonth);
		
		List<String> refreshedKeys = PrBo.cFetchSummaryDataList();
		assertTrue("Failed to fetch summary data keys", !refreshedKeys.isEmpty());
		assertTrue("Created month summary data key is missing in summary data list", refreshedKeys.contains(futureMonth));
		
		// Incremental test by creating Month Key
		cal.add(Calendar.YEAR, 1);
		String futureMonth2 = PrSummaryData.createMonthKey(cal);
		PrSummaryData.createOrFetchPrSummaryByMonth(futureMonth2);
		
		List<String> refreshedKeys2 = PrBo.cFetchSummaryDataList();
		assertTrue("Failed to fetch summary data keys", !refreshedKeys2.isEmpty());
		assertTrue("Created month summary data key is missing in summary data list", refreshedKeys2.contains(futureMonth2));
		
		
		// Incremental test by creating Year Key
		String futureYear = PrSummaryData.createYearKey(cal);
		PrSummaryData.createOrFetchPrSummaryByYear(futureYear);
		
		List<String> refreshedKeys3 = PrBo.cFetchSummaryDataList();
		assertTrue("Failed to fetch summary data keys", !refreshedKeys3.isEmpty());
		assertTrue("Created year summary data key is missing in summary data list", refreshedKeys3.contains(futureYear));
		System.out.println("Summary Keys: "+Arrays.toString(refreshedKeys3.toArray()));
	}
}
