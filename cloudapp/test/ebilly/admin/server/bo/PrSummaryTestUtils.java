package ebilly.admin.server.bo;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import ebilly.admin.server.db.DevicePr;
import ebilly.admin.server.db.PrSummaryData;
import ebilly.admin.shared.viewdef.ResultRecord;
import ebilly.admin.shared.viewdef.SearchRequest;
import ecollector.common.ReportAbstractColumns;

public class PrSummaryTestUtils {

	public static void clearExistingAggs() {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 0;
				NUM_DIVISION = 0;
				NUM_ERO = 0;
				NUM_SUB_DIV = 0;
				NUM_SECTION = 0;
				NUM_DIST = 0; // 2 digit
				NUM_SC = 0;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		//PrSummaryTestUtils.setupPrs(psd1);
		PrSummaryTestUtils.createPr(psd1, "9999999999999");
		PrBo.summarizePrs();
		PrSummaryData oldMonthPrSummary = 
			PrSummaryData.createOrFetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		oldMonthPrSummary.setActive(0);
		oldMonthPrSummary.save();
		// ensure that we are starting afresh instead of summarizing on exiting data.
		PrSummaryData newMonthPrSummary = 
			PrSummaryData.createOrFetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		TestCase.assertTrue("Could not clear existing summary data", 
				!oldMonthPrSummary.getId().equals(newMonthPrSummary.getId()));
	}
	
	public static void setupPrs(PrSetupData psd) {
		//int NUM_COMPANY = psd.NUM_COMPANY;
		int NUM_CIRCLE = psd.NUM_CIRCLE;
		int NUM_DIVISION = psd.NUM_DIVISION;
		int NUM_ERO = psd.NUM_ERO;
		int NUM_SUB_DIV = psd.NUM_SUB_DIV;
		int NUM_SECTION = psd.NUM_SECTION;
		int NUM_DIST = psd.NUM_DIST; // 2 digit
		int NUM_SC = psd.NUM_SC;
		
		long estimatedPrs = 
			(1+NUM_CIRCLE)*(1+NUM_DIVISION)*
			(1+NUM_ERO)*(1+NUM_SUB_DIV)*
			(1+NUM_SECTION)*(1+NUM_DIST)*(1+NUM_SC);
		log("Summarizing "+estimatedPrs+" PRs");
		
		//int company = 0;
		int circle = 0;
		int division = 0;
		int ero = 0;
		int sub_div = 0;
		int section = 0;
		int dist = 0;
		int sc = 0;
		long uscNo = 0;
		
		int insertedCount = 0;
		
		Map<String,String> idUniquenessMap = new HashMap<String,String>();
		//for (; company <= NUM_COMPANY; company++) { circle = 0;
		for (; circle <= NUM_CIRCLE; circle++) { division = 0;
		for (; division <= NUM_DIVISION; division++) { ero = 0;
		for (; ero <= NUM_ERO; ero++) { sub_div = 0;
		for (; sub_div <= NUM_SUB_DIV; sub_div++) { section = 0;
		for (; section <= NUM_SECTION; section++) { dist = 0;
		for (; dist <= NUM_DIST; dist++) { sc = 0;
		for (; sc <= NUM_SC; sc++) {
			// COMPUTE USC_NO
			uscNo = 
				(1+circle)  *1000000000000L +
			    (1+division)* 100000000000L +
			    (1+ero)     *  10000000000L +
			    (1+sub_div) *   1000000000L +
			    (1+section) *    100000000L +
			       (1+dist) *      1000000L +
			            sc;
			DevicePr devicePr = createPr(psd, uscNo+"");
			insertedCount++;
			idUniquenessMap.put(devicePr.getId(), devicePr.getId());
			log("Completed "+insertedCount+" of "+estimatedPrs+", new Id: "+devicePr.getId()+", uscNo: "+uscNo);
		}}}}}}}
		
		log("Number of ids uniquely found: "+idUniquenessMap.size());
	}
	
	public static DevicePr createPr(PrSetupData psd, String uscNo) {
		String collectionDate = psd.collectionDate;
		String collectionTime = psd.collectionTime;
		DevicePr devicePr = new DevicePr();
		devicePr.setARREARS_N_DEMAND(psd.and+"");
		devicePr.setRC_COLLECTED(psd.rc+"");
		devicePr.setACD_COLLECTED(psd.acd+"");
		devicePr.setAGL_AMOUNT(psd.agl+"");
		
		devicePr.setCOLLECTION_DATE(collectionDate);
		devicePr.setCOLLECTION_TIME(collectionTime);
		//log("UscNum["+uscNo+"]");
		devicePr.setUSC_NO(uscNo);
		devicePr.set_DEVICE_PR_CODE(psd.deviceName+"_"+uscNo);
		devicePr.setDeviceName(psd.deviceName);
		devicePr.save();
		return devicePr;
	}
	
	public static void log(String msg) {
		System.out.println(msg);
	}

	public static void assertRecordHeads(SearchRequest searchRequest, String dimName, PrHead prHead, ResultRecord record) {
		String and = searchRequest.getValue(ReportAbstractColumns.ARREARS_N_DEMAND, record);
		TestCase.assertTrue(dimName+" dimension, value 'Arrears and Demand' missmatch. Expected: "+prHead.and+", got: "+and,
				and.equals(prHead.and+""));
		String acd = searchRequest.getValue(ReportAbstractColumns.ACD_COLLECTED, record);
		TestCase.assertTrue(dimName+" dimension, value ACD missmatch. Expected: "+prHead.acd+", got: "+acd,
				acd.equals(prHead.acd+""));
		String agl = searchRequest.getValue(ReportAbstractColumns.AGL_AMOUNT, record);
		TestCase.assertTrue(dimName+" dimension, value AGL missmatch. Expected: "+prHead.agl+", got: "+agl,
				agl.equals(prHead.agl+""));
		String rc = searchRequest.getValue(ReportAbstractColumns.RC_COLLECTED, record);
		TestCase.assertTrue(dimName+" dimension, value RC missmatch. Expected: "+prHead.rc+", got: "+rc,
				rc.equals(prHead.rc+""));
		String grandTotal = searchRequest.getValue(ReportAbstractColumns.GRAND_TOTAL, record);
		TestCase.assertTrue(dimName+" dimension, value Grand total mismatch. Expected: "+prHead.grandTotal+", got: "+grandTotal,
				grandTotal.equals(prHead.grandTotal+""));
		String prCount = searchRequest.getValue(ReportAbstractColumns.TOTAL_PRS, record);
		TestCase.assertTrue(dimName+" dimension, value Total PRs mismatch. Expected: "+prHead.prCount+", got: "+prCount,
				prCount.equals(prHead.prCount+""));
	}
}
