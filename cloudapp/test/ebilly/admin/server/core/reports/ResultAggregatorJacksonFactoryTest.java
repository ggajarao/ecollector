package ebilly.admin.server.core.reports;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

import junit.framework.TestCase;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ebilly.admin.shared.viewdef.SearchResult;

public class ResultAggregatorJacksonFactoryTest extends TestCase {
	
	private ResultAggregatorTestHelper testHelper;
	
	@Before
	public void setUp() throws Exception {
		testHelper = new ResultAggregatorTestHelper(); 
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testAggregatorSerialization() {
		ResultAggregator ra1 = new ResultAggregator(
				testHelper.results, testHelper.uniqueFields, 
				testHelper.pivotCriteria, testHelper.searchRequest);
		
		SearchResult aggResults = ra1.applyAggregation();
		testHelper.assertAggResults(aggResults);
		
		OutputStream os = new ByteArrayOutputStream();
		try {
			JsonFactory f = new JsonFactory();
			JsonGenerator g = f.createJsonGenerator(os);
			ResultAggregatorJacksonFactory.asJsonStream(ra1, g);
			g.close();
		} catch(IOException ioe) {
			ioe.printStackTrace();
			assertFalse("Exception streaming json. Exception: "+ioe.getMessage(), true);
		}
		
		String raJson = "";
		try {
			raJson = ((ByteArrayOutputStream)os).toString("utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			assertFalse("Exception preparing json as string. Exception: "+e.getMessage(), true);
		}
		assertTrue("Json generation failed", 
				raJson != null && raJson.trim().length() != 0);
		
		InputStream in = new ByteArrayInputStream(raJson.getBytes());
		JsonFactory f = new JsonFactory();
		org.codehaus.jackson.JsonParser jp = null;
		try {
			jp = f.createJsonParser(in);
		} catch (Exception e) {
			e.printStackTrace();
			assertFalse("Exception preparing string as json. Exception: "+e.getMessage(), true);
		}
		ResultAggregator ra2 = null;
		try {
			ra2 = ResultAggregatorJacksonFactory.newResultAggregator(
					jp, aggResults, testHelper.uniqueFields, 
					testHelper.pivotCriteria, testHelper.searchRequest);
		} catch (Exception e) {
			e.printStackTrace();
			assertFalse("Exception preparing json as ResultAggregator. Exception: "+e.getMessage(), true);
		}
		
			//ResultAggregatorFactory.newResultAggregator(raJson);
		assertTrue("ResultAggregator Serialization failed with jackson", ra1.equals(ra2));
		SearchResult deserializedAggSearchResults = ra2.getAggregateSearchResult();
		testHelper.assertAggResults(deserializedAggSearchResults);
	}	
}