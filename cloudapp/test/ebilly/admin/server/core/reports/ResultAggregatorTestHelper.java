package ebilly.admin.server.core.reports;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import junit.framework.TestCase;

import ebilly.admin.shared.viewdef.PivotCriteria;
import ebilly.admin.shared.viewdef.ResultField;
import ebilly.admin.shared.viewdef.ResultRecord;
import ebilly.admin.shared.viewdef.SearchRequest;
import ebilly.admin.shared.viewdef.SearchResult;

public class ResultAggregatorTestHelper {
	public final SearchRequest searchRequest;
	public final SearchResult results;
	public final String[] uniqueFields;
	public final List<PivotCriteria> pivotCriteria;
	
	public final long totAsiaPopulation;
	public final long totAsiaHospitals;
	public final long totAsiaSchools;
	public final double totAsiaPercapitaIncome;
	public final long totEuropePopulation;
	public final long totEuropeHospitals;
	public final long totEuropeSchools;
	public final double totEuropePercapitaIncome;
	
	public ResultAggregatorTestHelper() {
		List<ResultField> resultFields = new ArrayList<ResultField>();
		resultFields.add(new ResultField("Planet", java.lang.String.class.getName()));
		resultFields.add(new ResultField("Continent", java.lang.String.class.getName()));
		resultFields.add(new ResultField("Country", java.lang.String.class.getName()));
		resultFields.add(new ResultField("State", java.lang.String.class.getName()));
		resultFields.add(new ResultField("City", java.lang.String.class.getName()));
		resultFields.add(new ResultField("Population", java.lang.Integer.class.getName()));
		resultFields.add(new ResultField("Hospitals", java.lang.Integer.class.getName()));
		resultFields.add(new ResultField("Schools", java.lang.Integer.class.getName()));
		resultFields.add(new ResultField("PerCapitaIncome", java.lang.Double.class.getName()));
		searchRequest = 
			new SearchRequest("World Geographies",resultFields);
		
		uniqueFields = new String[]{"Continent"};
		
		pivotCriteria = new ArrayList<PivotCriteria>();
		pivotCriteria.add(searchRequest.pivotCriteriaFor("Population", "SUM"));
		pivotCriteria.add(searchRequest.pivotCriteriaFor("Hospitals", "SUM"));
		pivotCriteria.add(searchRequest.pivotCriteriaFor("Schools", "SUM"));
		pivotCriteria.add(searchRequest.pivotCriteriaFor("PerCapitaIncome", "SUM"));
		
		
		List<ResultRecord> resultRecords = new ArrayList<ResultRecord>();
		resultRecords.add(new ResultRecord(Arrays.asList("Earth","Asia","India","Andhra Pradesh","Vijayawada","40000000","242","321","2315.52")));
		resultRecords.add(new ResultRecord(Arrays.asList("Earth","Asia","India","Andhra Pradesh","Hyderabad","120000000","842","821","3815.21")));
		resultRecords.add(new ResultRecord(Arrays.asList("Earth","Asia","India","Andhra Pradesh","Guntur",    "38710000","142","121","2015.39")));
		resultRecords.add(new ResultRecord(Arrays.asList("Earth","Asia","India","Andhra Pradesh","Visakhapattanam", "48710000","342","421","2415.22")));
		resultRecords.add(new ResultRecord(Arrays.asList("Earth","Europe","Ireland","Some State","Amsterdam", "381624","83","90","12415.21")));
		resultRecords.add(new ResultRecord(Arrays.asList("Earth","Europe","Spain","State In Spain","CityInSpain", "4303522","143","160","9415.11")));
		results = new SearchResult(resultRecords);
		
		totAsiaPopulation = 247420000;
		totAsiaHospitals = 1568;
		totAsiaSchools = 1684;
		totAsiaPercapitaIncome = 10561.34;
		
		totEuropePopulation = 4685146;
		totEuropeHospitals = 226;
		totEuropeSchools = 250;
		totEuropePercapitaIncome = 21830.32;
	}
	
	
	public void assertAggResults(SearchResult aggResults) {
		List<ResultRecord> asiaRecords = searchRequest.findRecords("Continent", "Asia", aggResults.getResults());
		TestCase.assertTrue("Expected Asia records 1, got "+asiaRecords.size(), asiaRecords.size() == 1);
		ResultRecord asiaRecord = asiaRecords.get(0);
		assertAggRecord(asiaRecord,"Asia",totAsiaPopulation,totAsiaHospitals,totAsiaSchools,totAsiaPercapitaIncome);
		
		List<ResultRecord> europeRecords = searchRequest.findRecords("Continent", "Europe", aggResults.getResults());
		TestCase.assertTrue("Expected Europe records 1, got "+europeRecords.size(), europeRecords.size() == 1);
		ResultRecord europeRecord = europeRecords.get(0);
		assertAggRecord(europeRecord,"Europe",totEuropePopulation,totEuropeHospitals,totEuropeSchools,totEuropePercapitaIncome);
	}

	public void assertAggRecord(ResultRecord aggRecord, String continent,
			long totAsiaPopulation2, long totAsiaHospitals2,
			long totAsiaSchools2, double totAsiaPercapitaIncome2) {
		String continentPopulation = searchRequest.getValue("Population",aggRecord);
		TestCase.assertTrue("Expected "+continent+" population is incorrect, expected: "+totAsiaPopulation2+", got: "+continentPopulation,
				continentPopulation.equals(totAsiaPopulation2+""));
		
		String continentHospitals = searchRequest.getValue("Hospitals",aggRecord);
		TestCase.assertTrue("Expected "+continent+" hospitals is incorrect, expected: "+totAsiaHospitals2+", got: "+continentHospitals,
				continentHospitals.equals(totAsiaHospitals2+""));
		
		String continentSchools = searchRequest.getValue("Schools",aggRecord);
		TestCase.assertTrue("Expected "+continent+" schools is incorrect, expected: "+totAsiaSchools2+", got: "+continentSchools,
				continentSchools.equals(totAsiaSchools2+""));
		
		String continentPercapitaIncome = searchRequest.getValue("PerCapitaIncome",aggRecord);
		TestCase.assertTrue("Expected "+continent+" percapita income is incorrect, expected: "+totAsiaPercapitaIncome2+", got: "+continentPercapitaIncome,
				continentPercapitaIncome.equals(totAsiaPercapitaIncome2+""));
	}
}
