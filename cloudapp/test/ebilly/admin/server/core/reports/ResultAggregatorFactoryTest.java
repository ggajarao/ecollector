package ebilly.admin.server.core.reports;


import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ebilly.admin.shared.viewdef.SearchResult;

public class ResultAggregatorFactoryTest extends TestCase {
	
	private ResultAggregatorTestHelper testHelper;
	
	@Before
	public void setUp() throws Exception {
		testHelper = new ResultAggregatorTestHelper();
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testAggregatorSerialization() {
		ResultAggregator ra1 = new ResultAggregator(
				testHelper.results, testHelper.uniqueFields, 
				testHelper.pivotCriteria, testHelper.searchRequest);
		
		SearchResult aggResults = ra1.applyAggregation();
		testHelper.assertAggResults(aggResults);
		
		String raJson = ResultAggregatorFactory.asJsonObject(ra1).toString();
		ResultAggregator ra2 = ResultAggregatorFactory.newResultAggregator(raJson);
		assertTrue("ResultAggregator Serialization failed", ra1.equals(ra2));
		SearchResult deserializedAggSearchResults = ra2.getAggregateSearchResult();
		testHelper.assertAggResults(deserializedAggSearchResults);
	}	
}