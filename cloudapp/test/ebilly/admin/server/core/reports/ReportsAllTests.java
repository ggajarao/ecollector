package ebilly.admin.server.core.reports;

import junit.framework.Test;
import junit.framework.TestSuite;

public class ReportsAllTests extends TestSuite {

	public static Test suite() {
		TestSuite suite = new TestSuite(ReportsAllTests.class.getName());
		//$JUnit-BEGIN$
		suite.addTest(new ResultAggregatorFactoryTest());

		//$JUnit-END$
		return suite;
	}

}
