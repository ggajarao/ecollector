package ebilly.admin.server;

import ebilly.admin.server.bo.PrBoSummerizerTest;
import ebilly.admin.server.core.reports.ReportsAllTests;
import ebilly.admin.server.core.reports.ResultAggregatorFactoryTest;
import ebilly.admin.server.db.DeviceDeleteTestCase;
import junit.framework.Test;
import junit.framework.TestSuite;

public class ServerAllTests extends TestSuite {

	public static Test suite() {
		TestSuite suite = new TestSuite(ServerAllTests.class.getName());
		//$JUnit-BEGIN$
		suite.addTestSuite(UtilTest.class);
		suite.addTestSuite(DeviceDeleteTestCase.class);
		suite.addTestSuite(ResultAggregatorFactoryTest.class);
		suite.addTestSuite(PrBoSummerizerTest.class);

		//$JUnit-END$
		return suite;
	}

}