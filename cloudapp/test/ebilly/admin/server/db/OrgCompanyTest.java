package ebilly.admin.server.db;

import java.util.List;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

import junit.framework.TestCase;

public class OrgCompanyTest extends TestCase {
	private final LocalServiceTestHelper helper =
        new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());
	
	public OrgCompanyTest() {
	}
	
	@Override
	protected void setUp() throws Exception {
		helper.setUp();
	}
	
	@Override
	protected void tearDown() throws Exception {
		helper.tearDown();
	}
	
	public void testOrgStructure() {
		OrgCompany company = new OrgCompany();
		company.setOcmpCode("companyCode1");
		company.setOcmpName("CompanyName");
		
		OrgCircle circle = new OrgCircle();
		circle.setOcrcCode("circleCode1");
		circle.setOcrcName("circleName");
		company.addOcmpCircle(circle);
		
		OrgDivision division = new OrgDivision();
		division.setOdvsCode("divisionCode1");
		division.setOdvsName("divisionName");
		circle.addOrgDivision(division);
		
		OrgEro ero = new OrgEro();
		ero.setOeroCode("eroCode1");
		ero.setOeroName("EroName");
		division.addOrgEro(ero);
		
		OrgSubdivision subdivision = new OrgSubdivision();
		subdivision.setOsbdCode("subdivisionCode1");
		subdivision.setOsbdName("subdivisionName");
		ero.addOrgSubdivision(subdivision);
		
		OrgSection section = new OrgSection();
		section.setOsctCode("sectionCode1");
		section.setOsctName("sectionName");
		subdivision.addOrgSection(section);
		
		OrgDistribution distribution = new OrgDistribution();
		distribution.setOdstCode("distributionCode1");
		distribution.setOdstName("distributionName");
		section.addOrgDistribution(distribution);
		
		company.save();
		
		OrgCompany dbCompany = OrgCompany.fetchCompanyByIdAndFetchAllChildren(company.getId());
		assertNotNull("Failed to fetch saved company",dbCompany);
		
		List<OrgCircle> dbCircles = dbCompany.getOcmpCircles();
		assertNotNull("Failed to fetch saved circles", dbCircles);
		assertTrue("Fetched empty circles", !dbCircles.isEmpty());
		assertTrue("Cicle count mismatch in Company", dbCircles.size() == 1);
		OrgCircle dbCircle = dbCircles.get(0);
		assertTrue("Circle saved is different than fetched", 
				dbCircle.getOcrcCode().equals(circle.getOcrcCode())
				&&
				dbCircle.getOcrcName().equals(circle.getOcrcName()));
		
		List<OrgDivision> dbOrgDivisions = dbCircle.getOrgDivisions();
		assertNotNull("Failed to fetch saved Divisions", dbOrgDivisions);
		assertTrue("Fetched empty Divisions", !dbOrgDivisions.isEmpty());
		assertTrue("Division count mismatch in Circle", dbOrgDivisions.size() == 1);
		OrgDivision dbDivision = dbOrgDivisions.get(0);
		assertTrue("Division saved is different than fetched",
				dbDivision.getOdvsCode().equals(division.getOdvsCode())
				&&
				dbDivision.getOdvsName().equals(division.getOdvsName()));
		
		List<OrgEro> dbEros = dbDivision.getOrgEros();
		assertNotNull("Failed to fetch saved Eros", dbEros);
		assertTrue("Fetched empty Eros", !dbEros.isEmpty());
		assertTrue("Ero count mismatch in Division", dbEros.size() == 1);
		OrgEro dbEro = dbEros.get(0);
		assertTrue("Ero saved is different than fetched",
				dbEro.getOeroCode().equals(ero.getOeroCode())
				&&
				dbEro.getOeroName().equals(ero.getOeroName()));
		
		List<OrgSubdivision> dbSubdivisions = dbEro.getOrgSubdivisions();
		assertNotNull("Failed to fetch saved Subdivisions", dbSubdivisions);
		assertTrue("Fetched empty subdivisions",!dbSubdivisions.isEmpty());
		assertTrue("Subdivision count mismatch in Division", dbSubdivisions.size() == 1);
		OrgSubdivision dbSubdivision = dbSubdivisions.get(0);
		assertTrue("Subdivision saved is dfferent than fetched",
				dbSubdivision.getOsbdCode().equals(subdivision.getOsbdCode())
				&&
				dbSubdivision.getOsbdName().equals(subdivision.getOsbdName()));
		
		List<OrgSection> dbSections = dbSubdivision.getOrgSections();
		assertNotNull("Failed to fetch saved Sections", dbSections);
		assertTrue("Fetched empty Sections", !dbSections.isEmpty());
		assertTrue("Section count mismatch in Subdivision", dbSections.size() == 1);
		OrgSection dbSection = dbSections.get(0);
		assertTrue("Section saved is different than fetched",
				dbSection.getOsctCode().equals(section.getOsctCode())
				&&
				dbSection.getOsctName().equals(section.getOsctName()));
		
		List<OrgDistribution> dbDistributions = dbSection.getOrgDistributions();
		assertNotNull("Failed to fetch saved Distributions", dbDistributions);
		assertTrue("Fetched empty Distributions", !dbDistributions.isEmpty());
		assertTrue("Distribution count mismatch in Section", dbDistributions.size() == 1);
		OrgDistribution dbDistribution = dbDistributions.get(0);
		assertTrue("Distribution saved is different than fetched",
				dbDistribution.getOdstCode().equals(distribution.getOdstCode())
				&&
				dbDistribution.getOdstName().equals(distribution.getOdstName()));
	}
}
