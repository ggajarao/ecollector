package ebilly.admin.server.db;

import junit.framework.TestCase;
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

import ebilly.admin.server.bo.DeviceBo;
import ebilly.admin.shared.DeviceUi;
import ebilly.admin.shared.viewdef.CrudRequest;
import ebilly.admin.shared.viewdef.CrudResponse;

public class DeviceDeleteTestCase extends TestCase {

	private final LocalServiceTestHelper helper =
        new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());
	
	public DeviceDeleteTestCase() {
	}
	
	@Override
	protected void setUp() throws Exception {
		helper.setUp();
		
		Role.seedRoles();
	}
	
	@Override
	protected void tearDown() throws Exception {
		helper.tearDown();
	}
	
	public void testDeviceDelete() {
		String deviceName = "JunitDevice_"+System.currentTimeMillis();
		deviceDeleteAssertions(deviceName);
		
		// Now try to create a new device with the same name.
		deviceDeleteAssertions(deviceName);
	}

	private void deviceDeleteAssertions(String deviceName) {
		CrudRequest request = new CrudRequest();
		request.addFieldValue("id", "");
		request.addFieldValue("deviceName", deviceName);
		CrudResponse response = DeviceBo.createDevice(request);
		assertTrue("Device creation failed, reason: "+response.errorMessage,
				response.isSuccess());
		DeviceUi fetchedDevice = DeviceBo.fetchDevice(deviceName);
		assertTrue("Failed to retrieve created device",fetchedDevice != null);
		String deviceId = fetchedDevice.getId();
		DeviceBo.deleteDevice(deviceId);
		
		DeviceUi deletedDevice = DeviceBo.fetchDevice(deviceName);
		assertTrue("Device is not deleted !",deletedDevice == null);
		
		Device cachedDeviceByName = Device.cFetchDeviceByName(deviceName);
		assertTrue("Device from cache by Name, is not cleared", cachedDeviceByName == null);
		
		Device cachedDeviceById = Device.cFetchDeviceById(deviceId);
		assertTrue("Device from cache by Id, is not cleared", cachedDeviceById == null);
		
		User deviceUser = DeviceBo.fetchDeviceUser(deviceId);
		assertTrue("Device User is not deleted", deviceUser == null);
	}
}
