package ebilly.admin.server.db;

import java.util.Calendar;
import java.util.List;

import junit.framework.TestCase;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

public class PrSummaryDataTest extends TestCase {
	private final LocalServiceTestHelper helper =
        new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());
	
	public PrSummaryDataTest() {
	}
	
	@Override
	protected void setUp() throws Exception {
		helper.setUp();
	}
	
	@Override
	protected void tearDown() throws Exception {
		helper.tearDown();
	}
	
	public void testPrSummaryCreation() {
		Calendar cal = Calendar.getInstance();
		String summaryMonth = PrSummaryData.createMonthKey(cal);
		PrSummaryData psd = PrSummaryData.createOrFetchPrSummaryByMonth(summaryMonth);
		String dataFileKey = "123";
		psd.setDataFileKey(dataFileKey);
		psd.save();
		
		PrSummaryData psd2 = PrSummaryData.createOrFetchPrSummaryByMonth(summaryMonth);
		assertTrue("Summary data saved is not same as fetched",
				psd.getId().equals(psd2.getId())
				&&
				psd.getDataFileKey().equals(psd2.getDataFileKey()));
	}
	
	public void testPrSummaryVersionTrack(){
		Calendar cal = Calendar.getInstance();
		String summaryMonth = PrSummaryData.createMonthKey(cal);
		PrSummaryData psd = PrSummaryData.createOrFetchPrSummaryByMonth(summaryMonth);
		String dataFileKey = "123";
		psd.setDataFileKey(dataFileKey);
		psd.save();
		
		String oldVersionNumber = "1.0";
		String newVersionNumber = "2.0";
		PrSummaryVersionTrack versionTrack = psd.createNewSummaryVersion(oldVersionNumber, newVersionNumber);
		assertTrue("Version track failed to create", 
				versionTrack != null);
		assertTrue("Version track saved without id",
				versionTrack.getId() != null);
		assertTrue("Version track saved without PrSummaryData id",
				psd.getId().equals(versionTrack.getPrSummaryDataId()));
		
		List<PrSummaryVersionTrack> versionTrackHistory = 
			PrSummaryVersionTrack.fetchPrSummaryVersionsByPrSummaryDataId(psd.getId());
		assertTrue("Verison track history failed to fetch for PrSummaryData",
				versionTrackHistory != null 
				&&
				!versionTrackHistory.isEmpty());
		assertTrue("Verions track recrod count mismatch, expected: 1, got: "
				+versionTrackHistory.size(),
				versionTrackHistory.size() == 1);
		
		PrSummaryVersionTrack versionTrack2 = versionTrackHistory.get(0);
		assertTrue("Version track history failed to fetch created version track",
				versionTrack.getId().equals(versionTrack2.getId())
				&&
				versionTrack.getOldVersionDataFileKey().equals(versionTrack2.getOldVersionDataFileKey())
				&&
				versionTrack.getNewVersionNumber().equals(versionTrack2.getNewVersionNumber())
				&&
				versionTrack.getOldVersionNumber().equals(versionTrack2.getOldVersionNumber()));
	}
}
