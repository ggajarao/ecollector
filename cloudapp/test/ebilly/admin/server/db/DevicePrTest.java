package ebilly.admin.server.db;

import java.util.Date;
import java.util.List;

import junit.framework.TestCase;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

public class DevicePrTest extends TestCase {

	private final LocalServiceTestHelper helper =
        new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());
	
	public DevicePrTest() {
	}
	
	@Override
	protected void setUp() throws Exception {
		helper.setUp();
		
		Role.seedRoles();
	}
	
	@Override
	protected void tearDown() throws Exception {
		helper.tearDown();
	}
	
	public void testDevicePrCreateFetch() {
		DevicePr pr = new DevicePr();
		pr.setACD_COLLECTED("aCD_COLLECTED");
		//pr.setActive();
		pr.setAGL_AMOUNT("aGL_AMOUNT");
		pr.setAGL_SERVICES("aGL_SERVICES");
		pr.setAMOUNT_COLLECTED("aMOUNT_COLLECTED");
		pr.setARREARS_N_DEMAND("aRREARS_N_DEMAND");
		//pr.setChangeNumber("changeNumber");
		pr.setCircleCode("circleCode");
		pr.setCMD_COLLECTED("cMD_COLLECTED");
		pr.setCOLLECTION_DATE("cOLLECTION_DATE");
		pr.setCOLLECTION_DELTA("cOLLECTION_DELTA");
		pr.setCOLLECTION_TIME("cOLLECTION_TIME");
		//pr.setCollectionDate("collectionDate");
		pr.setCompanyCode("companyCode");
		//pr.setCreatedDate("createdDate");
		pr.setDeviceName("deviceName");
		pr.setDistCode("distCode");
		pr.setDISTRIBUTION("dISTRIBUTION");
		pr.setDivisionCode("divisionCode");
		pr.setERO("eRO");
		pr.setLAST_PAID_AMOUNT("lAST_PAID_AMOUNT");
		pr.setLAST_PAID_DATE("lAST_PAID_DATE");
		pr.setLAST_PAID_RECEIPT_NO("lAST_PAID_RECEIPT_NO");
		//pr.setLastModifiedDate("lastModifiedDate");
		pr.setMACHINE_CODE("mACHINE_CODE");
		pr.setMACHINE_NUMBER("mACHINE_NUMBER");
		pr.setNEW_ARREARS("nEW_ARREARS");
		pr.setOTHERS_COLLECTED("oTHERS_COLLECTED");
		pr.setRC_CODE("rC_CODE");
		pr.setRC_COLLECTED("rC_COLLECTED");
		pr.setRECEIPT_NUMBER("rECEIPT_NUMBER");
		pr.setREMARKS("rEMARKS");
		pr.setREMARKS_AMOUNT("rEMARKS_AMOUNT");
		pr.setSC_NO("sC_NO");
		pr.setSECTION("sECTION");
		pr.setSectionCode("sectionCode");
		pr.setSubDivisionCode("subDivisionCode");
		pr.setTC_SEAL_NO("tC_SEAL_NO");
		pr.setUSC_NO("uSC_NO");
		pr.set_DEVICE_PR_CODE("devicename_123123");
		
		pr.save();
		String id = pr.getId();
		
		DevicePr pr2 = DevicePr.fetchDevicePrById(id);
		assertEquals("Mismatching value get_DEVICE_PR_CODE",pr.get_DEVICE_PR_CODE(),pr2.get_DEVICE_PR_CODE());
		assertEquals("Mismatching value getACD_COLLECTED", pr.getACD_COLLECTED(),pr2.getACD_COLLECTED());
		//assertEquals("Mismatching value ", pr.getActive());
		assertEquals("Mismatching value getAGL_AMOUNT", pr.getAGL_AMOUNT(),pr2.getAGL_AMOUNT());
		assertEquals("Mismatching value getAGL_SERVICES", pr.getAGL_SERVICES(),pr2.getAGL_SERVICES());
		assertEquals("Mismatching value getAMOUNT_COLLECTED", pr.getAMOUNT_COLLECTED(),pr2.getAMOUNT_COLLECTED());
		assertEquals("Mismatching value getARREARS_N_DEMAND", pr.getARREARS_N_DEMAND(),pr2.getARREARS_N_DEMAND());
		//assertEquals("Mismatching value ", pr.getChangeNumber("changeNumber"));
		//assertEquals("Mismatching value getCircleCode", pr.getCircleCode(),pr2.getCircleCode());
		assertEquals("Mismatching value getCMD_COLLECTED", pr.getCMD_COLLECTED(),pr2.getCMD_COLLECTED());
		assertEquals("Mismatching value getCOLLECTION_DATE", pr.getCOLLECTION_DATE(),pr2.getCOLLECTION_DATE());
		assertEquals("Mismatching value getCOLLECTION_DELTA", pr.getCOLLECTION_DELTA(),pr2.getCOLLECTION_DELTA());
		assertEquals("Mismatching value getCOLLECTION_TIME", pr.getCOLLECTION_TIME(),pr2.getCOLLECTION_TIME());
		//assertEquals("Mismatching value ", pr.getCollectionDate("collectionDate"));
		//assertEquals("Mismatching value getCompanyCode", pr.getCompanyCode(),pr2.getCompanyCode());
		//assertEquals("Mismatching value ", pr.getCreatedDate("createdDate"));
		assertEquals("Mismatching value getDeviceName", pr.getDeviceName(),pr2.getDeviceName());
		//assertEquals("Mismatching value getDistCode", pr.getDistCode(),pr2.getDistCode());
		assertEquals("Mismatching value getDISTRIBUTION", pr.getDISTRIBUTION(),pr2.getDISTRIBUTION());
		//assertEquals("Mismatching value getDivisionCode", pr.getDivisionCode(),pr2.getDivisionCode());
		assertEquals("Mismatching value getERO", pr.getERO(),pr2.getERO());
		assertEquals("Mismatching value getLAST_PAID_AMOUNT", pr.getLAST_PAID_AMOUNT(),pr2.getLAST_PAID_AMOUNT());
		assertEquals("Mismatching value getLAST_PAID_DATE", pr.getLAST_PAID_DATE(),pr2.getLAST_PAID_DATE());
		assertEquals("Mismatching value getLAST_PAID_RECEIPT_NO", pr.getLAST_PAID_RECEIPT_NO(),pr2.getLAST_PAID_RECEIPT_NO());
		//assertEquals("Mismatching value ", pr.getLastModifiedDate("lastModifiedDate"));
		assertEquals("Mismatching value getMACHINE_CODE", pr.getMACHINE_CODE(),pr2.getMACHINE_CODE());
		assertEquals("Mismatching value getMACHINE_NUMBER", pr.getMACHINE_NUMBER(),pr2.getMACHINE_NUMBER());
		assertEquals("Mismatching value getNEW_ARREARS", pr.getNEW_ARREARS(),pr2.getNEW_ARREARS());
		assertEquals("Mismatching value getOTHERS_COLLECTED", pr.getOTHERS_COLLECTED(),pr2.getOTHERS_COLLECTED());
		assertEquals("Mismatching value getRC_CODE", pr.getRC_CODE(),pr2.getRC_CODE());
		assertEquals("Mismatching value getRC_COLLECTED", pr.getRC_COLLECTED(),pr2.getRC_COLLECTED());
		assertEquals("Mismatching value getRECEIPT_NUMBER", pr.getRECEIPT_NUMBER(),pr2.getRECEIPT_NUMBER());
		assertEquals("Mismatching value getREMARKS", pr.getREMARKS(),pr2.getREMARKS());
		assertEquals("Mismatching value getREMARKS_AMOUNT", pr.getREMARKS_AMOUNT(),pr2.getREMARKS_AMOUNT());
		assertEquals("Mismatching value getSC_NO", pr.getSC_NO(),pr2.getSC_NO());
		assertEquals("Mismatching value getSECTION", pr.getSECTION(),pr2.getSECTION());
		//assertEquals("Mismatching value getSectionCode", pr.getSectionCode(),pr2.getSectionCode());
		//assertEquals("Mismatching value getSubDivisionCode", pr.getSubDivisionCode(),pr2.getSubDivisionCode());
		assertEquals("Mismatching value getTC_SEAL_NO", pr.getTC_SEAL_NO(),pr2.getTC_SEAL_NO());
		assertEquals("Mismatching value getUSC_NO", pr.getUSC_NO(),pr2.getUSC_NO());
		
	}
	
	public void testFetchDevicePrsCreatedAfter() {
		Date createdAfter = new Date(System.currentTimeMillis());
		BatchResult<List<DevicePr>> prResults = 
			DevicePr.fetchDevicePrsCreatedAfter(createdAfter, null);
		List<DevicePr> results = prResults.getResults();
		assertTrue("Unexpected result set, expecting empty !", results.isEmpty());
		
		// Insert a PrRecord and retrieve
		String devicePrCode1 = ""+(System.currentTimeMillis()+1);
		DevicePr dpr1 = new DevicePr();
		dpr1.set_DEVICE_PR_CODE(devicePrCode1);
		dpr1.setAMOUNT_COLLECTED("100");
		dpr1.save();
		
		prResults = DevicePr.fetchDevicePrsCreatedAfter(
				createdAfter, prResults.getEncodedCursor());
		results = prResults.getResults();
		assertTrue("Expected one pr record, got empty", !results.isEmpty());
		assertTrue("Expected one pr record, got many : "+results.size(), 
				results.size() == 1);
		dpr1 = results.get(0);
		assertTrue("fetch returned wrong results !", dpr1.get_DEVICE_PR_CODE().equals(devicePrCode1));
		
		// Insert two PrRecords and retrieve
		String devicePrCode2 = ""+(System.currentTimeMillis()+1);
		DevicePr dpr = new DevicePr();
		dpr.set_DEVICE_PR_CODE(devicePrCode2);
		dpr.setAMOUNT_COLLECTED("200");
		dpr.save();
		
		String devicePrCode3 = ""+(System.currentTimeMillis()+1);
		dpr = new DevicePr();
		dpr.set_DEVICE_PR_CODE(devicePrCode3);
		dpr.setAMOUNT_COLLECTED("300");
		dpr.save();
		
		prResults = DevicePr.fetchDevicePrsCreatedAfter(
				createdAfter, prResults.getEncodedCursor());
		results = prResults.getResults();
		
		assertTrue("Expecting two pr records, got empty", !results.isEmpty());
		assertTrue("Expecting two pr records, got: "+results.size(), 
				results.size() == 2);
	}
	
	public void testFetchDevicePrsCreatedAfterByReExecuteCursor() {
		Date createdAfter = new Date(System.currentTimeMillis());
		BatchResult<List<DevicePr>> prResults = 
			DevicePr.fetchDevicePrsCreatedAfter(createdAfter, null);
		List<DevicePr> results = prResults.getResults();
		assertTrue("Unexpected result set, expecting empty !", results.isEmpty());
		
		// Insert a PrRecord and retrieve
		String devicePrCode1 = ""+(System.currentTimeMillis()+1);
		DevicePr dpr1 = new DevicePr();
		dpr1.set_DEVICE_PR_CODE(devicePrCode1);
		dpr1.setAMOUNT_COLLECTED("100");
		dpr1.save();
		
		prResults = DevicePr.fetchDevicePrsCreatedAfter(
				createdAfter, prResults.getEncodedCursor());
		results = prResults.getResults();
		assertTrue("Expected one pr record, got empty", !results.isEmpty());
		assertTrue("Expected one pr record, got many : "+results.size(), 
				results.size() == 1);
		dpr1 = results.get(0);
		assertTrue("fetch returned wrong results !", dpr1.get_DEVICE_PR_CODE().equals(devicePrCode1));
		
		// Insert two PrRecords and retrieve
		String devicePrCode2 = ""+(System.currentTimeMillis()+1);
		DevicePr dpr = new DevicePr();
		dpr.set_DEVICE_PR_CODE(devicePrCode2);
		dpr.setAMOUNT_COLLECTED("200");
		dpr.save();
		
		String devicePrCode3 = ""+(System.currentTimeMillis()+1);
		dpr = new DevicePr();
		dpr.set_DEVICE_PR_CODE(devicePrCode3);
		dpr.setAMOUNT_COLLECTED("300");
		dpr.save();
		
		String cursorState = prResults.getEncodedCursor();
		prResults = DevicePr.fetchDevicePrsCreatedAfter(
				createdAfter, cursorState);
		results = prResults.getResults();
		
		assertTrue("Expecting two pr records, got empty", !results.isEmpty());
		assertTrue("Expecting two pr records, got: "+results.size(), 
				results.size() == 2);
		
		dpr = results.get(0);
		assertTrue("fetch returned wrong results !", dpr.get_DEVICE_PR_CODE().equals(devicePrCode2));
		dpr = results.get(1);
		assertTrue("fetch returned wrong results !", dpr.get_DEVICE_PR_CODE().equals(devicePrCode3));
		
		// Use the same cursor to get the results twice
		// to test if the cursor returning the results of
		// the previous batch or failing.
		prResults = DevicePr.fetchDevicePrsCreatedAfter(
				createdAfter, cursorState);
		results = prResults.getResults();
		
		assertTrue("Expecting two pr records, got empty", !results.isEmpty());
		assertTrue("Expecting two pr records, got: "+results.size(), 
				results.size() == 2);
		
		dpr = results.get(0);
		assertTrue("fetch returned wrong results !", dpr.get_DEVICE_PR_CODE().equals(devicePrCode2));
		dpr = results.get(1);
		assertTrue("fetch returned wrong results !", dpr.get_DEVICE_PR_CODE().equals(devicePrCode3));

		
		// Insert three PrRecords and retrieve
		String devicePrCode4 = ""+(System.currentTimeMillis()+1);
		dpr = new DevicePr();
		dpr.set_DEVICE_PR_CODE(devicePrCode4);
		dpr.setAMOUNT_COLLECTED("200");
		dpr.save();
		
		String devicePrCode5 = ""+(System.currentTimeMillis()+1);
		dpr = new DevicePr();
		dpr.set_DEVICE_PR_CODE(devicePrCode5);
		dpr.setAMOUNT_COLLECTED("300");
		dpr.save();
		
		String devicePrCode6 = ""+(System.currentTimeMillis()+1);
		dpr = new DevicePr();
		dpr.set_DEVICE_PR_CODE(devicePrCode6);
		dpr.setAMOUNT_COLLECTED("300");
		dpr.save();

		prResults = DevicePr.fetchDevicePrsCreatedAfter(
				createdAfter, prResults.getEncodedCursor());
		results = prResults.getResults();
		
		assertTrue("Expecting two pr records, got empty", !results.isEmpty());
		assertTrue("Expecting two pr records, got: "+results.size(), 
				results.size() == 3);
		
		dpr = results.get(0);
		assertTrue("fetch returned wrong results !", dpr.get_DEVICE_PR_CODE().equals(devicePrCode4));
		dpr = results.get(1);
		assertTrue("fetch returned wrong results !", dpr.get_DEVICE_PR_CODE().equals(devicePrCode5));
		dpr = results.get(2);
		assertTrue("fetch returned wrong results !", dpr.get_DEVICE_PR_CODE().equals(devicePrCode6));

	}
}
