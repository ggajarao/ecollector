package ebilly.admin.server;

import org.junit.Test;

import junit.framework.TestCase;

public class UtilTest extends TestCase {
	
	@Test
	public void testThrowableAsString() {
		try {
			throwableAsStringLevel1();
		} catch (Throwable t) {
			String throwableAsString = Util.throwableAsString(t);
			assertTrue("failed to generate string from throwable",
					throwableAsString != null && throwableAsString.trim().length() != 0);
		}
	}

	private void throwableAsStringLevel1() {
		try {
			throwableAsStringLevel2();
		} catch(NullPointerException n) {
			throw new IllegalArgumentException("Level traped exception",n);
		}
	}

	private void throwableAsStringLevel2() {
		String s = null;
		s.toString();
	}
}
