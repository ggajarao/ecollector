package ebilly.admin.server;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;

import com.google.appengine.tools.development.testing.LocalBlobstoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

import ebilly.admin.server.bo.PrSummaryTestUtils;

public class BlobFileReadWriteTest extends TestCase {
	
	private final LocalServiceTestHelper helper =
        new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());
	private final LocalServiceTestHelper blobStoreHelper =
        new LocalServiceTestHelper(new LocalBlobstoreServiceTestConfig());
	
	@Before
	protected void setUp() throws Exception {
		helper.setUp();
		blobStoreHelper.setUp();
		//PrSummaryTestUtils.clearExistingAggs();
	}
	
	@After
	protected void tearDown() throws Exception {
		try {
			blobStoreHelper.tearDown();
			//helper.tearDown();
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}
	
	public void testWriteIntoBlobFile() throws IOException {
		final String content = "Sample json content";
		String blobKey = Util.createBlobFile(
				"application/json", "jsonFileWriteTest.json",
				new Util.BlobFileWriteListener() {
					@Override
					public void readyToWrite(OutputStream os) throws IOException {
						os.write(content.getBytes());
					}
				});
		InputStream is = Util.openBlobFileStream(blobKey);
		BufferedReader bis = new BufferedReader(new InputStreamReader(is));
		StringBuffer readBlobFile = new StringBuffer();
		String line = null;
		while ( (line = bis.readLine()) != null ) {
			readBlobFile.append(line);
		}
		
		assertTrue("Got empty content when read from blob file", 
				readBlobFile != null && 
				readBlobFile.toString().trim().length() != 0);
		assertTrue("stored content is not what retrieved", content.equals(readBlobFile.toString()));
	}
}
