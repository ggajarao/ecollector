package ebilly.admin;

import junit.framework.Test;
import junit.framework.TestSuite;
import ebilly.admin.server.BlobFileReadWriteTest;
import ebilly.admin.server.UtilTest;
import ebilly.admin.server.bo.MonthPrSummarySearchHandlerTest;
import ebilly.admin.server.bo.PrBoSummerizerTest;
import ebilly.admin.server.bo.PrBoTest;
import ebilly.admin.server.core.reports.ResultAggregatorFactoryTest;
import ebilly.admin.server.core.reports.ResultAggregatorJacksonFactoryTest;
import ebilly.admin.server.db.DeviceDeleteTestCase;
import ebilly.admin.server.db.DevicePrTest;
import ebilly.admin.server.db.OrgCompanyTest;
import ebilly.admin.server.db.PrSummaryDataTest;

public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite(AllTests.class.getName());
		//$JUnit-BEGIN$
		suite.addTestSuite(DeviceDeleteTestCase.class);
		suite.addTestSuite(UtilTest.class);
		suite.addTestSuite(DeviceDeleteTestCase.class);
		suite.addTestSuite(ResultAggregatorFactoryTest.class);
		suite.addTestSuite(ResultAggregatorJacksonFactoryTest.class);
		suite.addTestSuite(PrBoSummerizerTest.class);
		suite.addTestSuite(DevicePrTest.class);
		suite.addTestSuite(OrgCompanyTest.class);
		suite.addTestSuite(PrSummaryDataTest.class);
		suite.addTestSuite(PrBoTest.class);
		suite.addTestSuite(MonthPrSummarySearchHandlerTest.class);
		suite.addTestSuite(BlobFileReadWriteTest.class);
		//$JUnit-END$
		return suite;
	}

}
