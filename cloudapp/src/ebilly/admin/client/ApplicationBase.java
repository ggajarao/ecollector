package ebilly.admin.client;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.FlexTable.FlexCellFormatter;

import ebilly.admin.client.menu.CollectionDashboardAction;
import ebilly.admin.client.menu.XAppMenu;
import ebilly.admin.client.service.LoginService;
import ebilly.admin.client.service.LoginServiceAsync;
import ebilly.admin.client.service.SecureService;
import ebilly.admin.client.service.SecureServiceAsync;
import ebilly.admin.client.view.FlexHtmlCell;
import ebilly.admin.client.view.SearchView;
import ebilly.admin.client.view.UserPasswordResetView;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.MyDeque;
import ebilly.admin.shared.RoleUi;
import ebilly.admin.shared.UserContext;

abstract public class ApplicationBase {
	
	public static final LoginServiceAsync loginService = 
		GWT.create(LoginService.class);

	public static final SecureServiceAsync secureService = 
		GWT.create(SecureService.class);

	private UserContext userContext = null;
	
	public static final ApplicationBase instance = Application.createInstance();

	private static final int USER_NAME_MAX_LENGHT = 100;
	private static final int PASSWORD_MAX_LENGTH = 100;
	
	public ApplicationBase() {
		
	}
	
	private void setUserContext(UserContext result) {
		this.userContext = result;
	}
    
    public String getUserRoleCode() {
    	if (this.userContext != null) {
    		return this.userContext.getRoleCode();
    	}
    	return "";
    }
    
    public String getLoginId() {
    	if (this.userContext != null) {
    		return this.userContext.getLoginId();
    	}
    	return "";
    }
    
    public String getUserId() {
    	if (this.userContext != null) {
    		return this.userContext.getUserId();
    	}
    	return "";
    }
    
    private void setApplicationState(UserContext result) {
    	if (result != null) {
    		setUserContext(result);
			ApplicationBase.instance.showLogoutLink(result);
			loadCache();
			showHomeScreen();
		} else {
			ApplicationBase.instance.showLoginLink();
		}
    	setMenueState(result);
    	
	}
    
    private void showHomeScreen() {
		CollectionDashboardAction a = new CollectionDashboardAction();
		a.perform();
	}

	public boolean canUserRepairDevice() {
    	return AppConfig.ROLE_CODE_SUPER_ADMIN.equals(this.getUserRoleCode());
    }
    
    public boolean canUserDeleteDevice() {
    	return AppConfig.ROLE_CODE_SUPER_ADMIN.equals(this.getUserRoleCode());
    }
    
    public boolean canUserCreateDevice() {
    	return AppConfig.ROLE_CODE_SUPER_ADMIN.equals(this.getUserRoleCode());
    }
    
    abstract protected void setMenueState(UserContext userContext);
    
    public void clearCache() {
		this.roles = null;
		// TODO: INSTEAD, LOAD CAHCE WHEN A CACHE MISS
		loadCache();
	}
	
	public void loadCache() {
		loadRoles();
	}
	
	private RoleUi[] roles = null;
	public RoleUi[] fetchSuperAdminRoles() {
		return fetchAllRoles();
	}
	public RoleUi[] fetchAppAdminRoles() {
		if (this.roles == null) return null;
		ArrayList<RoleUi> r = new ArrayList<RoleUi>();
		for (RoleUi role : this.roles) {
			if (role.getRoleCode().equals(AppConfig.ROLE_CODE_SUPER_ADMIN) ||
				role.getRoleCode().equals(AppConfig.ROLE_CODE_DEVICE_USER)) {
				continue;
			}
			r.add(role);
		}
		return r.toArray(new RoleUi[]{});
	}
	
	public RoleUi[] fetchAllRoles() {
		return this.roles;
	}
	
	public void loadRoles() {
		// Application data initialization one time
		ApplicationBase.secureService.fetchAllRoles(new AsyncCallback<RoleUi[]>() {
			@Override
			public void onSuccess(RoleUi[] roles) {
				ApplicationBase.instance.roles = roles;
			}
			
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
				Window.alert("Failed to load role data, cause: "
						+caught.getMessage());
			}
		});
	}

	Label messageLabel = new Label();
	public static void showMessage(String string) {
		if (CommonUtil.isEmpty(string)) {
			instance.messageLabel.addStyleName("work-done");
			//instance.messageLabel.getElement().setInnerHTML(string);
		} else {
			instance.messageLabel.setStyleName("working");
			instance.messageLabel.addStyleName("message-normal");
			instance.messageLabel.getElement().setInnerHTML(string);
		}
		
	}
	
	public static void showErrorMessage(String string) {
		if (CommonUtil.isEmpty(string)) {
			instance.messageLabel.addStyleName("work-done");
			//instance.messageLabel.getElement().setInnerHTML(string);
		} else {
			instance.messageLabel.setStyleName("working");
			instance.messageLabel.addStyleName("message-error");
			instance.messageLabel.getElement().setInnerHTML(string);
		}
	}
	
	protected void showLoginPanel() {
		
		final DialogBox dialog = new DialogBox();
		
		HTMLPanel ft = new HTMLPanel("");
		ft.setStyleName("login-panel");
		final Label message = new Label();
		ft.add(message);
		
		Label unCaption = new Label("User Name");
		unCaption.setStyleName("input-label");
		ft.add(unCaption);
		final TextBox userNameTextBox = new TextBox();
		userNameTextBox.setMaxLength(USER_NAME_MAX_LENGHT);
		ft.add(userNameTextBox);
		
		HTMLPanel pwdCaption = new HTMLPanel("Password");
		pwdCaption.setStyleName("input-label");
		ft.add(pwdCaption);
		final PasswordTextBox passwordTextBox = new PasswordTextBox();
		passwordTextBox.setMaxLength(PASSWORD_MAX_LENGTH);
		ft.add(passwordTextBox);
		
		HTMLPanel hp = new HTMLPanel("");
		hp.addStyleName("action-bar");
		Button loginButton = new Button("Login");
		loginButton.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				String uname = userNameTextBox.getText().trim();
				String pwd = passwordTextBox.getText().trim();
				
				if (uname.length() == 0 || pwd.length() == 0) {
					message.setText("Please enter User Name, Password");
					return;
				}
				
				message.setText("Please wait...");
				Application.loginService.login(uname, pwd, new AsyncCallback<UserContext>() {
					
					@Override
					public void onSuccess(UserContext result) {
						if (result != null) {
							dialog.hide();
							setApplicationState(result);
						} else {
							message.setText("Login failed, please try again");
						}
					}

					@Override
					public void onFailure(Throwable caught) {
						message.setText("Error while Login");
						System.out.println("Login result");
						caught.printStackTrace();
					}
				});
			}
		});
		hp.add(loginButton);
		
		
		Button cancelButton = new Button("Cancel");
		cancelButton.addStyleName("left-space");
		cancelButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				dialog.hide();
			}
		});
		hp.add(cancelButton);
		
		ft.add(hp);
		
		dialog.setAnimationEnabled(true);
		dialog.setText("Sign in");
		dialog.setWidget(ft);
		dialog.setGlassEnabled(true);
		dialog.setAutoHideEnabled(false);
		dialog.center();
		dialog.show();		
	}
	
	/*protected void showLoginPanel() {
		
		final DialogBox dialog = new DialogBox();
		
		FlexTable ft = new FlexTable();
		ft.setCellSpacing(18);
		ft.setStyleName("login-panel");
		
		FlexCellFormatter cellFormatter = ft.getFlexCellFormatter();
		cellFormatter.setColSpan(0,0,2);
		final Label message = new Label();
		ft.setWidget(0, 0, message);
		ft.setWidget(1, 0, new Label("User Name"));
		
		final TextBox userNameTextBox = new TextBox();
		userNameTextBox.setMaxLength(USER_NAME_MAX_LENGHT);
		ft.setWidget(1, 1, userNameTextBox);
		
		ft.setWidget(2,0,new Label("Password "));
		final PasswordTextBox passwordTextBox = new PasswordTextBox();
		passwordTextBox.setMaxLength(PASSWORD_MAX_LENGTH);
		ft.setWidget(2, 1, passwordTextBox);
		
		HorizontalPanel hp = new HorizontalPanel();
		hp.setHorizontalAlignment(HorizontalPanel.ALIGN_CENTER);
		hp.setSpacing(3);
		Button loginButton = new Button("Login");
		loginButton.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				String uname = userNameTextBox.getText().trim();
				String pwd = passwordTextBox.getText().trim();
				
				if (uname.length() == 0 || pwd.length() == 0) {
					message.setText("Please enter User Name, Password");
					return;
				}
				
				message.setText("Please wait...");
				Application.loginService.login(uname, pwd, new AsyncCallback<UserContext>() {
					
					@Override
					public void onSuccess(UserContext result) {
						if (result != null) {
							dialog.hide();
							setApplicationState(result);
						} else {
							message.setText("Login failed, please try again");
						}
					}

					@Override
					public void onFailure(Throwable caught) {
						message.setText("Error while Login");
						System.out.println("Login result");
						caught.printStackTrace();
					}
				});
			}
		});
		hp.add(loginButton);
		
		
		Button cancelButton = new Button("Cancel");
		cancelButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				dialog.hide();
			}
		});
		hp.add(cancelButton);
		
		cellFormatter.setColSpan(3, 0, 2);
		ft.setWidget(3, 0, hp);
		cellFormatter.setHorizontalAlignment(3, 0, HorizontalPanel.ALIGN_RIGHT);
		
		dialog.setAnimationEnabled(true);
		dialog.setText("Sign in");
		dialog.setWidget(ft);
		dialog.setGlassEnabled(true);
		dialog.setAutoHideEnabled(false);
		dialog.center();
		dialog.show();		
	}*/
	
	abstract protected void showLoginLink();

	abstract protected void showLogoutLink(UserContext userContext);
	
	abstract public void freezApplication();
	
	public void showExpiredSessionUi() {
		Application.instance.showScreen(null);
		Application.instance.freezApplication();
		Application.instance.showLoginLink();
		Application.showMessage("");
		Application.instance.showLoginPanel();
	}
	
	private MyDeque<AppScreen> frames = new MyDeque<AppScreen>();
	public void showScreen(AppScreen p) {
		// Clear messages
		Application.showMessage("");
		// Notify current screen that its about to close
		AppScreen currentScreen = frames.peek();
		if (currentScreen != null) {
			currentScreen.onBeforeClose();
		}
		Application.instance.clearContentPanel();
		if (p == null) return;
		frames.push(p);
		
		p.onBeforeOpen();
		Widget w = p.getUiObject();
		if (w != null) {
			Application.instance.setContent(p);
		}
	}
	
	abstract protected void setContent(AppScreen p);
	
	abstract protected void clearContentPanel();
	
	public void showPrevious() {
		// Dispose current screen.
		frames.pop();
		// Now, previous screen
		AppScreen p = frames.pop();
		showScreen(p);
		if (p instanceof SearchView) {
			((SearchView)p).search();
		}
	}
	
	public void showUserHome() {
		RootPanel rp = RootPanel.get("main");
		if (rp == null) return;
		
		RootPanel.get("main").clear();
		RootPanel.get().add(getAppPanel());
		
		Application.loginService.isLoggedIn(new AsyncCallback<UserContext>(){
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("login service failure");
				caught.printStackTrace();
			}

			@Override
			public void onSuccess(UserContext result) {
				Application.instance.setApplicationState(result);
			}
		});
	}
	
	abstract protected Panel getAppPanel();
}
