package ebilly.admin.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;

import ebilly.admin.shared.DevicePayload;
import ebilly.admin.shared.UserContext;

public interface LoginServiceAsync {
	void login(String uname, String pwd, AsyncCallback<UserContext> callback)
			throws IllegalArgumentException;

	void isLoggedIn(AsyncCallback<UserContext> callback) 
		throws IllegalArgumentException;
	
	void isLoggedIn(DevicePayload devicePayload, AsyncCallback<UserContext> callback) 
	throws IllegalArgumentException;
	
	void logout(AsyncCallback<Boolean> callback) throws IllegalArgumentException;
}

