package ebilly.admin.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;

import ebilly.admin.shared.DeleteItemRequest;
import ebilly.admin.shared.DeviceFileUi;
import ebilly.admin.shared.DeviceUi;
import ebilly.admin.shared.EntitySelection;
import ebilly.admin.shared.PushPrRequest;
import ebilly.admin.shared.RoleUi;
import ebilly.admin.shared.UserUi;
import ebilly.admin.shared.viewdef.CrudRequest;
import ebilly.admin.shared.viewdef.CrudResponse;
import ebilly.admin.shared.viewdef.SearchRequest;
import ebilly.admin.shared.viewdef.SearchResult;

/**
 * The async counterpart of <code>SecureService</code>.
 */
public interface SecureServiceAsync {
	void searchView(SearchRequest request, 
			AsyncCallback<SearchResult> callback)
	throws IllegalArgumentException;
	
	void processCrud(CrudRequest crudRequest,
			AsyncCallback<CrudResponse> asyncCallback) 
	throws IllegalArgumentException;
	
	void getCurrentDate(AsyncCallback<String> asyncCallback) 
	throws IllegalArgumentException;
	
	void fetchAdminRoles(AsyncCallback<RoleUi[]> asyncCallback) 
	throws IllegalArgumentException;
	
	void fetchAllRoles(AsyncCallback<RoleUi[]> asyncCallback) 
	throws IllegalArgumentException;
	
	void deleteUser(String userId, AsyncCallback<CrudResponse> asyncCallback) 
	throws IllegalArgumentException;
	
	void fetchUser(String userId, AsyncCallback<UserUi> asyncCallback)
	throws IllegalArgumentException;
	
	void resetUserPassword(String userId, String newPassword,
			AsyncCallback<CrudResponse> asyncCallback)
	throws IllegalArgumentException;
	
	void processDeleteRequest(EntitySelection request, 
			AsyncCallback<CrudResponse> asyncCallback)
	throws IllegalArgumentException;
	
	void prepareBlobStoreUrl(AsyncCallback<String> asyncCallback) 
	throws IllegalArgumentException;
	
	void prepareBlobStoreUrl(String successUrl, AsyncCallback<String> asyncCallback) 
	throws IllegalArgumentException;
	
	void sendPasswordResetEmail(String userId, String emailAddress,
			AsyncCallback<CrudResponse> asyncCallback)
	throws IllegalArgumentException;
	
	void fetchDevice(String deviceName, AsyncCallback<DeviceUi> asyncCallback)
	throws IllegalArgumentException;
	
	void removeDeviceFiles(String[] deviceFileIds,
			AsyncCallback<CrudResponse> asyncCallback)
	throws IllegalArgumentException;
	
	void fetchDownloadableDevicefiles(String deviceIdentifier, 
			AsyncCallback<DeviceFileUi[]> asyncCallback)
	throws IllegalArgumentException;
	
	void deviceFileDownloadedInDevice(String deviceIdentifier,
			String deviceFileId, AsyncCallback<CrudResponse> asynCallback) 
	throws IllegalArgumentException;
	
	void pushPrs(PushPrRequest request, 
				 AsyncCallback<CrudResponse> asyncCallback) 
	throws IllegalArgumentException;
	
	void acceptDeviceServiceFile(CrudRequest crudRequest, 
			AsyncCallback<CrudResponse> asyncCallback)
	throws IllegalArgumentException;
	
	void createExportRequest(SearchRequest searchRequest,
			AsyncCallback<CrudResponse> asyncCallback)
	throws IllegalArgumentException;
	
	void resetDeviceCollection(String deviceId, 
			AsyncCallback<CrudResponse> asyncCallback)
	throws IllegalArgumentException;
	
	void updateDeviceCommand(String deviceCommandId,
			String commandState, String executionNotes,
			AsyncCallback<CrudResponse> asyncCallback)
	throws IllegalArgumentException;
	
	void resetDevicePairing(String deviceId, 
			AsyncCallback<CrudResponse> asyncCallback)
	throws IllegalArgumentException;
	
	void fetchDeviceSummaryReport(String deviceId, 
			AsyncCallback<CrudResponse> asyncCallback)
	throws IllegalArgumentException;
}