package ebilly.admin.client.service;

import ebilly.admin.shared.DevicePayload;
import ebilly.admin.shared.UserContext;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("login")
public interface LoginService extends RemoteService {
	UserContext login(String uname, String pwd) throws IllegalArgumentException;
	UserContext isLoggedIn() throws IllegalArgumentException;
	UserContext isLoggedIn(DevicePayload devicePayload) throws IllegalArgumentException;
	Boolean logout() throws IllegalArgumentException;
}

