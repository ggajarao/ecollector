package ebilly.admin.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import ebilly.admin.shared.DeleteItemRequest;
import ebilly.admin.shared.DeviceFileUi;
import ebilly.admin.shared.DeviceUi;
import ebilly.admin.shared.EntitySelection;
import ebilly.admin.shared.PushPrRequest;
import ebilly.admin.shared.RoleUi;
import ebilly.admin.shared.UserUi;
import ebilly.admin.shared.viewdef.CrudRequest;
import ebilly.admin.shared.viewdef.CrudResponse;
import ebilly.admin.shared.viewdef.SearchRequest;
import ebilly.admin.shared.viewdef.SearchResult;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("secure")
public interface SecureService extends RemoteService {
	SearchResult searchView(SearchRequest request)
	throws IllegalArgumentException;
	
	CrudResponse processCrud(CrudRequest request)
	throws IllegalArgumentException;
	
	String getCurrentDate() throws IllegalArgumentException;
	
	RoleUi[] fetchAdminRoles() 
	throws IllegalArgumentException;
	
	RoleUi[] fetchAllRoles() 
	throws IllegalArgumentException;
	
	CrudResponse deleteUser(String userId) 
	throws IllegalArgumentException;
	
	UserUi fetchUser(String userId)
	throws IllegalArgumentException;
	
	CrudResponse resetUserPassword(String userId, String newPassword) 
	throws IllegalArgumentException;
	
	CrudResponse processDeleteRequest(EntitySelection request)
	throws IllegalArgumentException;
	
	String prepareBlobStoreUrl() 
	throws IllegalArgumentException;
	
	String prepareBlobStoreUrl(String successUrl) 
	throws IllegalArgumentException;
	
	CrudResponse sendPasswordResetEmail(String userId, String emailAddress)
	throws IllegalArgumentException;
	
	DeviceUi fetchDevice(String deviceName) 
	throws IllegalArgumentException;
	
	CrudResponse removeDeviceFiles(String[] deviceFileIds)
	throws IllegalArgumentException;
	
	DeviceFileUi[] fetchDownloadableDevicefiles(String deviceIdentifier)
	throws IllegalArgumentException;
	
	CrudResponse deviceFileDownloadedInDevice(String deviceIdentifier, 
			String deviceFileId)
	throws IllegalArgumentException;
	
	CrudResponse pushPrs(PushPrRequest request) 
	throws IllegalArgumentException;
	
	CrudResponse acceptDeviceServiceFile(CrudRequest crudRequest)
	throws IllegalArgumentException;
	
	CrudResponse createExportRequest(SearchRequest searchRequest)
	throws IllegalArgumentException;
	
	CrudResponse resetDeviceCollection(String deviceId)
	throws IllegalArgumentException;
	
	CrudResponse updateDeviceCommand(String deviceCommandId,
			String commandState, String executionNotes)
	throws IllegalArgumentException;
	
	CrudResponse resetDevicePairing(String deviceId)
	throws IllegalArgumentException;
	
	CrudResponse fetchDeviceSummaryReport(String deviceId) 
	throws IllegalArgumentException;
}