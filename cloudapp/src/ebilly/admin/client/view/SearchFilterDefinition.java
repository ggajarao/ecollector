package ebilly.admin.client.view;

import java.util.ArrayList;
import java.util.List;

import ebilly.admin.shared.CommonUtil;

public class SearchFilterDefinition {
	private List<ViewField> searchFilterDef = new ArrayList<ViewField>();
	public SearchFilterDefinition() {
		
	}
	
	public List<ViewField> getFilterFields() {
		return this.searchFilterDef;
	}
	
	public void add(ViewField viewField) {
		if (viewField == null) return;
		this.searchFilterDef.add(viewField);
	}
	
	public ViewField findFilterField(String fieldName) {
		if (CommonUtil.isEmpty(fieldName)) return null;
		for (ViewField vf : this.searchFilterDef) {
			if (vf.fieldName.equalsIgnoreCase(fieldName)) {
				return vf;
			}
		}
		return null;
	}
	
	public String getFieldValue(String fieldName) {
		ViewField vf = this.findFilterField(fieldName);
		if (vf == null) return null;
		return vf.getValue(new ErrorMessage());
	}
}