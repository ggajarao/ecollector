package ebilly.admin.client.view;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;

import ebilly.admin.client.AppScreen;
import ebilly.admin.client.Application;
import ebilly.admin.client.menu.ActionListener;
import ebilly.admin.client.menu.DeviceDashboardViewAction;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.EntitySelection;
import ebilly.admin.shared.viewdef.CrudResponse;
import ebilly.admin.shared.viewdef.ResultRecord;

public class DeviceSearchView extends SearchView implements AppScreen {

	public DeviceSearchView(String caption) {
		super(caption,false/*do not prefix caption*/,true/*show results action bar*/);
		buildUi();
	}
	
	private Panel resultsActionPanel;
	private void buildUi() {
		ActionBarWidget resultsActionBar = super.getResultsActionBarWidget();
		resultsActionPanel = new HorizontalPanel();
		resultsActionBar.add(resultsActionPanel);
		final ActionListener selectionListener = new ActionListener(){
			public void onBeforeAction(Object data) {
			}
			public void onAfterAction(Object data) {
				updateDeleteAction(viewDef.getEntitySelection());
			}
		};
		super.viewDef.setEntitySelectedListener(selectionListener);
		super.viewDef.setEntityUnselectedListener(selectionListener);
	}
	
	private void updateDeleteAction(EntitySelection selection) {
		if (selection.isEmpty()) {
			resultsActionPanel.clear();
		} else {
			resultsActionPanel.clear();
			String label = "Delete " + selection.getDeleteEntityList().size() +" Devices";
			Button delete = new Button(label);
			delete.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent event) {
					ConfirmationDialog cd = 
						new ConfirmationDialog(
								"Delete Devices",
								"Are you sure you want to delete the selected Devices ?");
					cd.addYesActionListener(new ActionListener() {
						public void onBeforeAction(Object data) {
						}
						public void onAfterAction(Object data) {
							Application.showMessage("Please wait...");
							Application.instance.secureService.processDeleteRequest(
								viewDef.getEntitySelection(), 
								new BaseServiceResponseHandler<CrudResponse>() {
									public void onSuccess(CrudResponse result) {
										DeviceSearchView.this.search();
										viewDef.getEntitySelection().clear();
										updateDeleteAction(viewDef.getEntitySelection());
									}
								});
						}
					});
				}
			});
			resultsActionPanel.add(delete);
		}
	}
	
	@Override
	protected SearchViewDef getSearchViewDefinition() {
		return new DeviceSearchViewDef();
	}
}

class DeviceSearchViewDef extends SearchViewDef {
	public DeviceSearchViewDef() {
		super();
	}
	
	@Override
	protected void initSearchFilterDef(SearchFilterDefinition searchFilterDef) {
		searchFilterDef.add(new ViewField("deviceNameSearchable",java.lang.String.class.getName(),"Device Name"));
	}
	
	@Override
	protected void initSearchResultDef(SearchResultDefinition searchResultDef) {
		ResultColumn c = new ResultColumn("id",java.lang.String.class.getName(),"id");
		c.isHidden = true;
		searchResultDef.add(c);
		
		c = new ResultColumn("deviceName",java.lang.String.class.getName(),"Device Name");
		c.setCellRenderer(new DeviceEditCellRenderer("id","deviceName", null,searchResultDef));
		searchResultDef.add(c);
		
		searchResultDef.add(new ResultColumn("deviceIdentifier",java.lang.String.class.getName(),"UUID"));
		searchResultDef.add(new ResultColumn("pairingStatus",java.lang.String.class.getName(),"Pairing Status"));
		
		c = new ResultColumn("Dashboard",java.lang.String.class.getName(),"Dashboard");
		c.setCellRenderer(new DeviceEditCellRenderer("id",null,"Manage Device",searchResultDef));
		searchResultDef.add(c);
		
		if (Application.instance.canUserDeleteDevice()) {
			c = new ResultColumn("Actions",java.lang.String.class.getName()," ");
			c.setCellRenderer(new DeviceActionsCellRenderer("id", searchResultDef, DeviceSearchViewDef.this));
			searchResultDef.add(c);
		}
	}

	@Override
	public String getSearchObjectName() {
		return AppConfig.OBJ_DEVICE;
	}
}

class DeviceEditCellRenderer implements CellRenderer {

	SearchResultDefinition searchResultDef;
	private String deviceIdField;
	private String labelField;
	private String label;
	public DeviceEditCellRenderer(String deviceIdField, String labelField,
			String label,
			SearchResultDefinition searchResultDef) {
		super();
		this.labelField = labelField;
		this.label = label;
		this.searchResultDef = searchResultDef;
		this.deviceIdField = deviceIdField;
	}
	
	@Override
	public Cell renderIntoCell(FlexTable resultGrid, int row,
			ResultRecord record, CrudViewDefinition parentCrudViewDef,
			CrudViewDefinition sectionCrudViewDef) {
		final Cell c = new Cell();
		final String deviceId = searchResultDef.getValue(deviceIdField, record);
		Anchor anchor = null;
		if (labelField != null) {
			anchor = new Anchor(searchResultDef.getValue(labelField, record));
		} else {
			anchor = new Anchor(label);
		}
		anchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				DeviceDashboardViewAction action = 
					new DeviceDashboardViewAction(deviceId);
				action.perform();
			}
		});
		c.addWidget(anchor);
		return c;
	}
}

class DeviceActionsCellRenderer implements CellRenderer {
	SearchResultDefinition searchResultDef;
	private String deviceIdField;
	private SearchViewDef serchViewDef;
	private final String ENTITY_NAME = "ebilly.admin.server.db.Device";
	public DeviceActionsCellRenderer(String deviceIdField,
			SearchResultDefinition searchResultDef,
			SearchViewDef searchViewDef) {
		super();
		this.searchResultDef = searchResultDef;
		this.deviceIdField = deviceIdField;
		this.serchViewDef = searchViewDef;
	}
	
	public Cell renderIntoCell(final FlexTable resultGrid, final int row, 
			final ResultRecord record, CrudViewDefinition parentCrudViewDef, 
			CrudViewDefinition sectionCrudViewDef) 
{
		final Cell c = new Cell();
		final String deviceId = searchResultDef.getValue(deviceIdField, record);
		final boolean selected = serchViewDef.getEntitySelection().isSelected(deviceId);
		CheckBox cb = new CheckBox();
		cb.setValue(selected);
		cb.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				CheckBox source = (CheckBox)event.getSource();
				if (source.getValue()) {
					serchViewDef.addSelectedEntity(ENTITY_NAME, deviceId);
				} else {
					serchViewDef.removeSelectedEntity(deviceId);
				}
			}
		});
		c.addWidget(cb);
		
		return c;
	}
}

/*class DeviceDeleteConfirmationDialog {
	
	private DialogBox dialog;
	private String deviceId;
	private Cell cell;
	private Widget widget;
	public DeviceDeleteConfirmationDialog(String DeviceId,
			Cell cell) {
		this.deviceId = DeviceId;
		this.widget = widget;
		this.cell = cell;
		buildUi();
	}
	
	protected void buildUi() {
		dialog = new DialogBox();
		
		FlexTable ft = new FlexTable();
		ft.setCellSpacing(6);
		
		FlexCellFormatter cellFormatter = ft.getFlexCellFormatter();
		cellFormatter.setColSpan(0,0,2);
		ft.setWidget(0, 0, new Label("Are you sure you want to delete the Device ?"));
		
		Button noButton = new Button("No");
		Button yesButton = new Button("Yes");
		ActionBarWidget actionBar = new ActionBarWidget();
		actionBar.add(noButton);
		actionBar.add(yesButton);
		
		noButton.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				dialog.hide();
			}
		});
		
		yesButton.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				Window.alert("Implementation pending...");
				Application.instance.secureService.deleteDevice(deviceId, 
					new AsyncCallback<CrudResponse>(){
						@Override
						public void onFailure(Throwable caught) {
							Application.instance.showMessage("Error while deleting Device");
						}
						public void onSuccess(CrudResponse result) {
							dialog.hide();
							cell.clear();
							cell.addWidget(new Label("Deleted"));
						};
					}
				);
			}
		});
		
		cellFormatter.setColSpan(1, 0, 2);
		cellFormatter.setAlignment(1, 0, HorizontalPanel.ALIGN_CENTER, VerticalPanel.ALIGN_MIDDLE);
		ft.setWidget(1, 0, actionBar);
		
		dialog.setAnimationEnabled(true);
		dialog.setText("Confirm");
		dialog.setWidget(ft);
		dialog.center();
		dialog.setAutoHideEnabled(true);
		dialog.show();		
	}
}
*/