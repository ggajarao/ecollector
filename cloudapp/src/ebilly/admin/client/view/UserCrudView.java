package ebilly.admin.client.view;

import java.util.HashMap;

import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.CommonUtil;

public class UserCrudView extends CrudView {

	
	
	public UserCrudView(String crudCaption) {
		super(crudCaption);
	}
	
	public UserCrudView(int crudAction, String crudCaption, String userId) {
		super(crudAction, crudCaption, userId);
	}

	@Override
	protected void initViewFieldDefinition(CrudViewDefinition viewDef) {
		ViewField vf = new ViewField("id",java.lang.String.class.getName(),"Id");
		vf.isMandatory = false;
		vf.isHidden = true;
		viewDef.addViewField(vf);

		vf = new ViewField("loginId",java.lang.String.class.getName(),"User Login");
		vf.setMandatory(true);
		vf.setReadOnlyOnEdit(true);
		viewDef.addViewField(vf);
		
		vf = new ViewField("email",java.lang.String.class.getName(),"Email Address");
		vf.setMandatory(true);
		viewDef.addViewField(vf);
		
		vf = new ViewField("password",AppConfig.FIELD_TYPE_PASSWORD,"Password");
		vf.setMandatory(true);
		vf.setReadOnlyOnEdit(true);
		viewDef.addViewField(vf);
		
		vf = new ViewField("roleId",RoleSelectionPanel.class.getName(),"User Role");
		vf.setMandatory(true);
		vf.setReadOnlyOnEdit(true);
		viewDef.addViewField(vf);
		
		vf = new ViewField("confPassword",AppConfig.FIELD_TYPE_PASSWORD,"Confirm Password");
		vf.setReadOnlyOnEdit(true);
		viewDef.addViewField(vf);
		
		viewDef.setViewValidator(new ViewValidator() {
			@Override
			public void validate(CrudViewDefinition crudViewDefinition,
					ErrorMessage errorMsg) {
				String password = crudViewDefinition.getCrudValue("password");
				String confPassword = crudViewDefinition.getCrudValue("confPassword");
				if (crudViewDefinition.isCreateAction() &&
					!CommonUtil.isEmpty(password) && 
					!password.equals(confPassword)) {
					errorMsg.addItem("Password and Confirmation Password are mismatching");
					HashMap<String, String> crudValueMap = new HashMap<String,String>();
					crudValueMap.put("password", "");
					crudValueMap.put("confPassword", "");
					crudViewDefinition.setCrudValues(crudValueMap);
				}
				
				String email = crudViewDefinition.getCrudValue("email");

				//if (!rfc2822.matcher(email).matches()) {
				if (!UiUtil.isValidEmail(email)) {
				    errorMsg.addItem(AppConfig.MESSAGE_INVALID_EMAIL);
				}
			}
		});
	}

	@Override
	protected String getCrudObjectName() {
		return AppConfig.CRUD_USER;
	}
}

