package ebilly.admin.client.view;

public interface ViewValidator {
	void validate(CrudViewDefinition crudViewDefinition, ErrorMessage errorMsg);
}

