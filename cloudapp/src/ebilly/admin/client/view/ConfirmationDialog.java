package ebilly.admin.client.view;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlexTable.FlexCellFormatter;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

import ebilly.admin.client.menu.ActionListener;

public class ConfirmationDialog {
	
	private DialogBox dialog;
	private String caption;
	private String confirmMessage;
	public ConfirmationDialog(String caption, String confirmMessage) {
		this.caption = caption;
		this.confirmMessage = confirmMessage;
		buildUi();
	}
	
	protected void buildUi() {
		dialog = new DialogBox();
		
		FlexTable ft = new FlexTable();
		ft.setCellSpacing(6);
		
		FlexCellFormatter cellFormatter = ft.getFlexCellFormatter();
		cellFormatter.setColSpan(0,0,2);
		ft.setWidget(0, 0, new Label(this.confirmMessage));
		
		Button noButton = new Button("No");
		Button yesButton = new Button("Yes");
		ActionBarWidget actionBar = new ActionBarWidget();
		actionBar.add(noButton);
		actionBar.add(yesButton);
		
		noButton.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				dialog.hide();
				noListener.onAfterAction(null);
			}
		});
		
		yesButton.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				dialog.hide();
				yesListener.onAfterAction(null);
			}
		});
		
		cellFormatter.setColSpan(1, 0, 2);
		cellFormatter.setAlignment(1, 0, HorizontalPanel.ALIGN_CENTER, VerticalPanel.ALIGN_MIDDLE);
		ft.setWidget(1, 0, actionBar);
		
		dialog.setAnimationEnabled(true);
		dialog.setText(this.caption);
		dialog.setWidget(ft);
		dialog.center();
		dialog.setAutoHideEnabled(true);
		dialog.show();		
	}
	
	private ActionListener yesListener;
	public void addYesActionListener(ActionListener yesListener) {
		this.yesListener = yesListener;
	}
	
	private ActionListener noListener;
	public void addNoActionListener(ActionListener noListener) {
		this.noListener = noListener;
	}	
}