package ebilly.admin.client.view;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Widget;

import ebilly.admin.shared.viewdef.ResultRecord;

public interface CellRenderer {
	//Widget renderIntoCell(FlexTable resultGrid, int row, final ResultRecord record);
	Cell renderIntoCell(FlexTable resultGrid, int row, final ResultRecord record, CrudViewDefinition parentCrudViewDef, CrudViewDefinition sectionCrudViewDef);
}

