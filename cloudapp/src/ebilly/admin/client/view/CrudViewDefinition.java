package ebilly.admin.client.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gwt.user.client.ui.Widget;

import ebilly.admin.client.menu.ActionListener;
import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.EntityUi;
import ebilly.admin.shared.viewdef.CrudResponse;
import ebilly.admin.shared.viewdef.SectionData;

public class CrudViewDefinition {
	
	public static final int CRUD_ACTION_CREATE = 1;
	public static final int CRUD_ACTION_UPDATE = 2;
	public static final int CRUD_ACTION_VIEW = 3;
	
	
	private List<ViewField> viewFields = new ArrayList<ViewField>();
	
	//private Map<ViewField, Widget> fieldInput = new HashMap<ViewField, Widget>();
	
	public Map<String, ViewSectionDefinition> viewSectionDefinitions = 
		new HashMap<String, ViewSectionDefinition>();
	
	private int crudAction = CRUD_ACTION_VIEW;
	
	public boolean hideCrudCommands = false;
	
	private ViewValidator viewValidator = null;
	
	/* Stores spot field edits, the values collected in this collection
	 * is used for crud operations, and the values should be validated before
	 * adding. */
	Map<String, String> hostedFieldValueChanges = null;
	
	/*Crud object class name Ref AppConfig.OBJ_* constant values */
	private String objectName;
	
	private EntityUi crudEntity;
	
	public CrudViewDefinition() {
		
	}
	
	public void setCrudAction(int crudAction) {
		this.crudAction = crudAction;
	}
	
	public boolean isCreateAction() {
		return this.crudAction == CRUD_ACTION_CREATE;
	}
	
	public boolean isViewAction() {
		return this.crudAction == CRUD_ACTION_VIEW;
	}
	
	public void addViewSectionDefinition(ViewSectionDefinition viewSectionDef) {
		this.viewSectionDefinitions.put(viewSectionDef.sectionName, viewSectionDef);
	}
	
	public ViewSectionDefinition getViewSectionDefinition(String sectionName) {
		return this.viewSectionDefinitions.get(sectionName);
	}
	
	public void addViewField(ViewField viewField) {
		viewField.setCrudAction(this.crudAction);
		this.viewFields.add(viewField);
	}
	
	/**
	 * Fills this view definition with values of an entity.
	 * 
	 * @param crudValueMap
	 */
	public void setCrudValues(Map<String, String> crudValueMap) {
		for (Entry<String, String> fieldValue : crudValueMap.entrySet()) {
			String field = fieldValue.getKey();
			String value = fieldValue.getValue();

			/*for (ViewField viewField : fieldInput.keySet()) {
				if (viewField.fieldName.equals(field)) {
					viewField.setValue(value);
					break;
				}
			}*/
			for (ViewField viewField : viewFields) {
				if (viewField.fieldName.equals(field)) {
					viewField.setValue(value);
					break;
				}
			}
		}
	}
	
	private List<SectionData> sectionDataList;
	public void setSectionDataList(List<SectionData> sectionDataList) {
		this.sectionDataList = sectionDataList;
	}
	
	public List<SectionData> getSectionDataList() {
		return sectionDataList;
	}
	
	public String getCrudValue(String fieldName) {
		for (ViewField f : this.viewFields) {
			if (f.fieldName.equals(fieldName)) {
				return f.getValue(new ErrorMessage());				
			}
		}
		return "";
	}
	
	public Widget createInputWidget(String fieldName) {
		ViewField vf = findViewField(fieldName);
		if (vf != null) {
			Widget v = vf.createInputWidget();
			vf.setValue(getCrudValue(fieldName),v);
			return v;
		}
		return null;
	}
	
	/*public void setSectionValues(Map<String, SearchResult> sectionResults) {
		for (Entry<String, SearchResult> entry : sectionResults.entrySet()) {
			
		}
	}*/

	/**
	 * Returns a Map of fieldCode and its value.
	 * 
	 * It also validates number fields and throws IllegalArgumentException
	 * if any number field validation fails.
	 * 
	 * Also reports mandatory fields missing validation.
	 * 
	 * @return
	 */
	public Map<String, String> validateAndGetFieldValueMap() {
		resetErrors();
		
		ErrorMessage errorMsg = new ErrorMessage();
		Map<String, String> fvMap = new HashMap<String, String>();
		/*for (Entry<ViewField, Widget> entry : this.fieldInput.entrySet()) {
			ViewField vf = entry.getKey();
			Widget input = entry.getValue();
			if (input == null) continue;
			String value = vf.getValue(errorMsg);
			if (vf.isMandatory() && CommonUtil.isEmpty(value)) {
				//errorMsg.addItem("Please provide value for " + vf.displayName);
			} else {
				fvMap.put(vf.fieldName, value);
			}
		}*/
		for (ViewField vf : this.viewFields) {
			if (vf.getInputWidget() == null) continue;
			String value = vf.getValue(errorMsg);
			if (vf.isMandatory() && CommonUtil.isEmpty(value)) {
				//errorMsg.addItem("Please provide value for " + vf.displayName);
			} else {
				fvMap.put(vf.fieldName, value);
			}
		}
		
		this.validateWholeView(errorMsg);
		
		if (errorMsg.hasErrors()) {
			throw new IllegalArgumentException(errorMsg.getMessage());
		}
		return fvMap;
	}
	
	public String validateAndGetFieldValue(String fieldName,
			Widget inputWidget, ErrorMessage errorMsg) {
		ViewField vf = findViewField(fieldName);
		vf.resetError();
		return vf.getValue(errorMsg,inputWidget);
	}
	
	public void setViewValidator(ViewValidator viewValidator) {
		this.viewValidator = viewValidator;
	}
	
	private ViewValidator getViewValidator() {
		return this.viewValidator;
	}

	private void validateWholeView(ErrorMessage errorMsg) {
		ViewValidator v = this.getViewValidator();
		if (v != null) {
			v.validate(this, errorMsg);
		}
	}

	private void resetErrors() {
		/*for (Entry<ViewField, Widget> entry : this.fieldInput.entrySet()) {
			entry.getKey().resetError();
		}*/
		for (ViewField field : this.viewFields) {
			field.resetError();
		}
	}
	
	public List<ViewField> getViewFields() {
		return this.viewFields;
	}
	
	public ViewField findViewField(String fieldName) {
		List<ViewField> fields = getViewFields(); 
		for (ViewField field : fields) {
			if (field.fieldName.equalsIgnoreCase(fieldName)) {
				return field;
			}
		}
		return null;
	}
	
	public void setHtmlTuple(String fieldName, FlexHtmlTupleCell tupleCell) {
		ViewField field = findViewField(fieldName);
		if (field == null) return;
		if (field.helpText != null) {
			tupleCell.getCaptionCell().setHelpText(field.helpText);
		}
		field.setHtmlTuple(tupleCell);
	}

	/**
	 * Hosts field value changes, the values set into this should
	 * be validated before this method call. No further validations
	 * are evaluated before any crud requests.
	 * 
	 * Hosting updates is used in spot edits of a field value.
	 * 
	 * requestXXX methods use the hosted values for the crud operations.
	 * 
	 * @param fieldName
	 * @param fieldValue
	 */
	public void hostUpdate(String fieldName, String fieldValue) {
		getHostedFieldValueChanges().put(fieldName, fieldValue);
		requestToCreate();
	}
	
	private Map<String, String> getHostedFieldValueChanges() {
		if (hostedFieldValueChanges == null) {
			hostedFieldValueChanges = new HashMap<String, String>(); 
		}
		return hostedFieldValueChanges;
	}
	
	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}
	
	public String getObjectName() {
		return this.objectName;
	}
	
	public void requestToCreate() {
		String id = this.findViewField("id").getValue(null);
		Map<String, String> fieldValues = this.getHostedFieldValueChanges();
		fieldValues.put("id", id);
		UiUtil.requestToCreate(this.getObjectName(),
				fieldValues, new ActionListener() {
					public void onBeforeAction(Object data) {
					}
					public void onAfterAction(Object data) {
						CrudResponse crudResponse = (CrudResponse)data;
						setCrudValues(crudResponse.fieldValueMap);
					}
				});
	}

	public EntityUi getCrudEntity() {
		return crudEntity;
	}

	public void setCrudEntity(EntityUi crudEntity) {
		this.crudEntity = crudEntity;
	}
}