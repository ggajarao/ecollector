package ebilly.admin.client.view;

import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import ebilly.admin.shared.AppConfig.IMPORT_STATUS;
import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.viewdef.ResultRecord;

public class ImportDataStatusCellRenderer implements CellRenderer {

	SearchResultDefinition searchResultDef;
	private String statusFieldName;
	//private String nextInQueueField;
	private String importRequestIdField;
	public ImportDataStatusCellRenderer(String statusFieldName,
			String importRequestIdField, /*String nextInQueueField,*/
			SearchResultDefinition searchResultDef) {
		super();
		this.searchResultDef = searchResultDef;
		this.statusFieldName = statusFieldName;
		//this.nextInQueueField = nextInQueueField;
		this.importRequestIdField = importRequestIdField; 
	}
	
	@Override
	public Cell renderIntoCell(FlexTable resultGrid, int row,
			ResultRecord record, CrudViewDefinition parentCrudViewDef,
			CrudViewDefinition sectionCrudViewDef) {
		Cell c = new Cell();
		String displayStatus = searchResultDef.getValue(statusFieldName, record);
		Label label = new Label(displayStatus);
		
		ActionBarWidget w = new ActionBarWidget();
		w.setCellPadding(2);
		w.add(label);
		String sNextInQueue = "true";//searchResultDef.getValue(this.nextInQueueField, record);
		if ("false".equalsIgnoreCase(sNextInQueue)) {
			Anchor a = new Anchor("Push to Queue"); 
			String irId = searchResultDef.getValue(this.importRequestIdField, record);
			a.setHref(CommonUtil.getPushToImportRequestQueueUrl(irId));
			a.setTarget("-");
			w.add(a);
		} else if ("true".equalsIgnoreCase(sNextInQueue)) {
			if (IMPORT_STATUS.NEW.toString().equalsIgnoreCase(displayStatus)) {
				w.add(new Label("(Queued)"));
			}
		}
		
		c.addWidget(w);
		return c;
	}
}

