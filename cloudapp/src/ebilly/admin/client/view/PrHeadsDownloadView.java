package ebilly.admin.client.view;

import ebilly.admin.shared.AppConfig;


public class PrHeadsDownloadView extends BasePrDownloadView {
	private SearchView mSearchView = null;
	public PrHeadsDownloadView() {
		super("Recent PR Heads Downloads");
	}

	@Override
	protected String getObjectName() {
		return AppConfig.OBJ_PR_HEADS_REPORT;
	}

	@Override
	protected SearchView getExportRequestSearchView() {
		if (mSearchView != null) return mSearchView;
		mSearchView = new SearchView(""){
			@Override
			protected SearchViewDef getSearchViewDefinition() {
				return new PrHeadsReportSearchViewDef();
			}
		};
		return mSearchView;
	}
}