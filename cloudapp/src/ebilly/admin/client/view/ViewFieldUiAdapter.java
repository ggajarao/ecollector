package ebilly.admin.client.view;

import java.util.Date;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.datepicker.client.DateBox.DefaultFormat;

import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.CommonUtil;

public class ViewFieldUiAdapter {
	
	private Widget inputWidget = null;
	private Widget inputCaption = null;
	private Widget viewCaption = null;
	
	private ViewField viewField = null;
	public ViewFieldUiAdapter(ViewField viewField) {
		this.viewField = viewField;
	}
	
	public void setInputWidget(Widget inputWidget) {
		this.inputWidget = inputWidget;
	}
	
	public Widget getInputCaption() {
		if (inputCaption == null) {
			this.inputCaption = new Label(viewField.displayName);// + (viewField.isMandatory?"*":""));
			this.inputCaption.addStyleName("field-caption");
			if (viewField.isMandatory) {
				this.inputCaption.addStyleName("field-caption-mandatory");
			} else {
				this.inputCaption.removeStyleName("field-caption-mandatory");
			}			
		}
		return this.inputCaption;
	}

	public Widget getInputWidget() {
		if (this.inputWidget == null) {
			this.inputWidget = createInputWidget();
		}
		return this.inputWidget;
	}
	
	public Widget getViewCaption() {
		if (viewCaption == null) {
			this.viewCaption = new Label(viewField.displayName);
			this.viewCaption.setStyleName("field-caption");
		}
		return this.viewCaption;
	}
	
	public Widget createInputWidget() {
		if (viewField.isHidden) return new Hidden();
		
		if (viewField.isHiddenOnEdit && 
			(CrudViewDefinition.CRUD_ACTION_CREATE == viewField.crudAction ||
			 CrudViewDefinition.CRUD_ACTION_UPDATE == viewField.crudAction )) {
			return new Hidden();
		}
		
		boolean enabled = true;
		if (viewField.crudAction == CrudViewDefinition.CRUD_ACTION_VIEW) {
			enabled = false;
		}
		
		if (viewField.isReadonlyOnEdit() && viewField.crudAction == CrudViewDefinition.CRUD_ACTION_UPDATE) {
			enabled = false;
		}
		
		if (viewField.isReadonly) {
			enabled = false;
		}
		if (!enabled && 
			  (viewField.getFieldType().equals(java.lang.String.class.getName()) ||
			   viewField.getFieldType().equals(java.lang.Double.class.getName())  ||
			   viewField.getFieldType().equals(java.util.Date.class.getName())) ||
			   viewField.getFieldType().equals(java.lang.StringBuffer.class.getName())) {
			return new DataLabel();
		} else if (viewField.getFieldType().equals(java.lang.String.class.getName())) {
			TextBox textBox = new TextBox();
			textBox.setMaxLength(viewField.getMaxLenght());
			textBox.setEnabled(enabled);
			return textBox;
		} else if (viewField.getFieldType().equals(java.lang.StringBuffer.class.getName())) {
			TextArea textArea = new TextArea();
			textArea.setReadOnly(!enabled);
			//textArea.setEnabled(enabled);
			return textArea;
		} else if (viewField.getFieldType().equalsIgnoreCase("Email")) {
			TextBox textBox = new TextBox();
			textBox.setMaxLength(100);
			textBox.setEnabled(enabled);
			return textBox;
		} else if (viewField.getFieldType().equals(java.lang.Double.class.getName())) {
			TextBox textBox = new TextBox();
			textBox.setMaxLength(10);
			textBox.setEnabled(enabled);
			return textBox;
		} else if (viewField.getFieldType().equals(java.lang.Integer.class.getName())) {
			TextBox textBox = new TextBox();
			textBox.setMaxLength(10);
			textBox.setEnabled(enabled);
			return textBox;
		} else if (viewField.getFieldType().equals(java.util.Date.class.getName())) {
			DateBox dateBox = new DateBox();
			dateBox.setFormat(
				new DefaultFormat(
					DateTimeFormat.getFormat(
							CommonUtil.DEFAULT_DATE_FORMAT)));
			dateBox.setEnabled(enabled);
			return dateBox;
		} else if (viewField.getFieldType().equals(RoleSelectionPanel.class.getName())) {
			RoleSelectionPanel rsp = new RoleSelectionPanel(null);
			rsp.setEnabled(enabled);
			return rsp;
		} else if (viewField.getFieldType().equals(AppConfig.FIELD_TYPE_PASSWORD)) {
			PasswordTextBox p = new PasswordTextBox();
			p.setEnabled(enabled);
			return p;
		} else if (viewField.getFieldType().equals(DateOrDateRangeSelectionPanel.class.getName())) {
			DateOrDateRangeSelectionPanel p = new DateOrDateRangeSelectionPanel(viewField.displayName,viewField.fieldName);
			p.setEnabled(enabled);
			return p;
		} else {
			return new Label("Unknown filter type");
		}
	}

	public void resetError() {
		if (this.inputCaption != null) {
			this.inputCaption.setStyleName("field-caption");
		}
	}

	public void setError() {
		this.inputCaption.setStyleName("field-caption-error");
	}
	
	public void setValue(String value, Widget inputWidget) {
		if (inputWidget instanceof Hidden) {
			((Hidden)inputWidget).setValue(value);
		} else if (inputWidget instanceof Label) {
			((Label)inputWidget).setText(CommonUtil.isEmpty(value)?"N/A":value);
		} else if (inputWidget instanceof TextBox) {
			((TextBox)inputWidget).setValue(value);
		} else if (inputWidget instanceof DateBox) {
			Date date = UiUtil.getDateFromString(value);
			((DateBox)inputWidget).setValue(date);
		} else if (inputWidget instanceof RoleSelectionPanel) {
			((RoleSelectionPanel)inputWidget).setValue(value);
		} else if (inputWidget instanceof Label) {
			((Label)inputWidget).setText(value);
		} else if (inputWidget instanceof FlexHtmlDataCell) { 
			((FlexHtmlDataCell)inputWidget).setValue(CommonUtil.isEmpty(value)?"N/A":value);
		} else {
			throw new IllegalArgumentException("Unknown input type while setting value: "+value);
		}
	}

	public void setValue(String value) {
		Widget inputWidget = this.getInputWidget();
		setValue(value,inputWidget);
	}
	
	public String getValue(ErrorMessage errorMsg, Widget inputWidget) {
		Widget input = inputWidget;
		ViewField vf = this.viewField;
		String value = "";
		if (input instanceof TextBox) {
			value = ((TextBox)input).getValue().trim();
			if (CommonUtil.isEmpty((String)value) && vf.isMandatory()) {
				errorMsg.addItem(vf.displayName + " is mandatory, please provide value");
				vf.setError();
			} else if ( vf.getFieldType().equals(java.lang.Double.class.getName())) {
				try {
					Double dv = Double.valueOf(value);
				} catch (NumberFormatException e) {
					errorMsg.addItem(
							vf.displayName + " invalid value entered, please provide only number, eg: 200 or 200.50 etc.");
					vf.setError();
				}
			} else if ( vf.getFieldType().equals(java.lang.Integer.class.getName())) {
				try {
					Integer iv = Integer.valueOf(value);
				} catch (NumberFormatException e) {
					errorMsg.addItem(
							vf.displayName + " invalid value entered, please provide only number, eg: 200 etc.");
					vf.setError();
				}
			} else if ( vf.getFieldType().equals(java.lang.Long.class.getName())) {
				try {
					Long lv = Long.valueOf(value);
				} catch (NumberFormatException e) {
					errorMsg.addItem(
							vf.displayName + " invalid value entered, please provide only number, eg: 200 etc.");
					vf.setError();
				}
			} else if ( vf.getFieldType().equalsIgnoreCase("Email")) {
				
				if (!UiUtil.isValidEmail(value)) {
					errorMsg.addItem(
							vf.displayName + ", invalid value entered." + AppConfig.MESSAGE_INVALID_EMAIL);
					vf.setError();
				}
			}
		} else if (input instanceof TextArea) {
			value = ((TextArea)input).getValue().trim();
			if (CommonUtil.isEmpty((String)value) && vf.isMandatory()) {
				errorMsg.addItem(vf.displayName + " is mandatory, please provide value");
				vf.setError();
			}
		} else if (input instanceof FlexHtmlDataCell) { 
			value = ((FlexHtmlDataCell)input).getValue().trim();
			if ("N/A".equalsIgnoreCase(value)) {
				value = "";
			}
			if (CommonUtil.isEmpty((String)value) && vf.isMandatory()) {
				errorMsg.addItem(vf.displayName + " is mandatory, please provide value");
				vf.setError();
			}
		} else if (input instanceof Hidden) {
			value = ((Hidden)input).getValue();
		} else { 
			if (input instanceof DateBox) {
				Date date = ((DateBox)input).getValue();
				if (date != null) {
					String stringDate = DateTimeFormat.getFormat(
							CommonUtil.DEFAULT_DATE_FORMAT).format(date);
					value = stringDate;
				}
			} else if (input instanceof PasswordTextBox) {
				value = ((PasswordTextBox)input).getValue();
			} else if (input instanceof RoleSelectionPanel) {
				value = ((RoleSelectionPanel)input).getRoleId();
			} else if (input instanceof Hidden) {
				value = ((Hidden)input).getValue();
			} else if (input instanceof DataLabel) {
				value = ((DataLabel)input).getText();
			}
			
			if (CommonUtil.isEmpty((String)value) && vf.isMandatory()) {
				errorMsg.addItem(vf.displayName + " is mandatory, please provide value");
				vf.setError();
			}
		}
		return value;
	}

	public String getValue(ErrorMessage errorMsg) {
		return getValue(errorMsg, this.inputWidget);
	}

	public void setInputCaptionWidget(Widget inputCaptionWidget) {
		this.inputCaption = inputCaptionWidget;
	}
}