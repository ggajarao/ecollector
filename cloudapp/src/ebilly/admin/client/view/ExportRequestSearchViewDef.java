package ebilly.admin.client.view;

import ebilly.admin.shared.AppConfig;


public abstract class ExportRequestSearchViewDef extends SearchViewDef {
	public ExportRequestSearchViewDef() {
		super();
		super.hideFilter = true;
	}
	
	@Override
	protected void initSearchFilterDef(SearchFilterDefinition searchFilterDef) {
		ViewField viewField = new ViewField("objectName",java.lang.String.class.getName(),"Object Name");
		viewField.isHidden = true;
		viewField.setValue(getObjectName());
		searchFilterDef.add(viewField);
	}
	
	protected abstract String getObjectName();

	@Override
	protected void initSearchResultDef(SearchResultDefinition searchResultDef) {
		ResultColumn c = new ResultColumn("id",java.lang.String.class.getName(),"id");
		c.isHidden = true;
		searchResultDef.add(c);
		
		c = new ResultColumn("fileName",java.lang.String.class.getName(),"fileName");
		c.isHidden = true;
		searchResultDef.add(c);
		
		c = new ResultColumn("resultFileKey",java.lang.String.class.getName(),"resultFileKey");
		c.isHidden = true;
		searchResultDef.add(c);
		
		c = new ResultColumn("errorMessage",java.lang.String.class.getName(),"errorMessage");
		c.isHidden = true;
		searchResultDef.add(c);
		
		c = new ResultColumn("filters",java.lang.String.class.getName(),"PRs Matching");
		c.setCellRenderer(new HtmlCellRenderer("filters", searchResultDef));
		searchResultDef.add(c);
		
		c = new ResultColumn("summaryMessage",java.lang.String.class.getName(),"Summary");
		//c.isHidden = true;
		searchResultDef.add(c);
		
		/*c = new ResultColumn("objectName",java.lang.String.class.getName(),"Type");
		searchResultDef.add(c);*/
		
		c = new ResultColumn("status",java.lang.String.class.getName(),"Download");
		c.setCellRenderer(new ExportResultCellRenderer("status", "resultFileKey", "errorMessage", "summaryMessage", searchResultDef));
		searchResultDef.add(c);
	}

	@Override
	public String getSearchObjectName() {
		return AppConfig.OBJ_EXPORT_REQUEST;
	}
}
