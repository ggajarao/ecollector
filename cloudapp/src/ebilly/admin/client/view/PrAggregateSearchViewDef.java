package ebilly.admin.client.view;

import ebilly.admin.client.view.DateOrDateRangeSelectionPanel;
import ebilly.admin.shared.AppConfig;

public class PrAggregateSearchViewDef extends SearchViewDef {
	
	@Override
	protected void initSearchFilterDef(SearchFilterDefinition searchFilterDef) {
		searchFilterDef.add(new ViewField("deviceName",java.lang.String.class.getName(),"Device Name"));
		searchFilterDef.add(new ViewField("_collectionDate", DateOrDateRangeSelectionPanel.class.getName(),"Collection Date"));
	}
	
	@Override
	protected void initSearchResultDef(SearchResultDefinition searchResultDef) {
		ResultColumn c = new ResultColumn("id",java.lang.String.class.getName(),"id");
		c.isHidden = true;
		searchResultDef.add(c);
		
		c = new ResultColumn("dISTRIBUTION",java.lang.String.class.getName(),"Distribution");
		searchResultDef.add(c);
		
		c = new ResultColumn("_DEVICE_PR_CODE",java.lang.String.class.getName(),"PRs");
		c.setCellRenderer(new NumberRenderer("_DEVICE_PR_CODE",searchResultDef));
		searchResultDef.add(c);
		
		c = new ResultColumn("aMOUNT_COLLECTED",java.lang.Double.class.getName(),"Amount");
		c.setCellRenderer(new NumberRenderer("aMOUNT_COLLECTED",searchResultDef));
		searchResultDef.add(c);
	}
	
	@Override
	public String getSearchObjectName() {
		return AppConfig.OBJ_PR_AGGREGATE_REPORT;
	}
}
