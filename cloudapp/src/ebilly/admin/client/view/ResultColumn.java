package ebilly.admin.client.view;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Widget;

import ebilly.admin.shared.viewdef.ResultRecord;

public class ResultColumn extends ViewField implements Serializable, IsSerializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private SearchResultDefinition searchResultDefinition;
	
	private CellRenderer cellRenderer;
	
	public ResultColumn(String fieldName, String fieldType, String displayName) {
		super(fieldName,fieldType,displayName);
	}

	public void setSearchResultDefinition(
			SearchResultDefinition searchResultDefinition) {
		this.searchResultDefinition = searchResultDefinition;
	}
	
	public SearchResultDefinition getSearchResultDefinition() {
		return this.searchResultDefinition;
	}
	
	public void setCellRenderer(CellRenderer cellRenderer) {
		this.cellRenderer = cellRenderer;
	}
	
	/*public Widget getCellWidget(FlexTable resultGrid, int row, ResultRecord record) {
		if (this.cellRenderer == null) {
			this.cellRenderer = 
				new SimpleCellRenderer(fieldName, this.searchResultDefinition);
		}
		return this.cellRenderer.renderIntoCell(resultGrid, row, record);
		
	}*/
	
	public Widget getCellWidget(FlexTable resultGrid, int row, ResultRecord record, CrudViewDefinition parentCrudViewDef, CrudViewDefinition sectionCrudViewDef) {
		if (this.cellRenderer == null) {
			this.cellRenderer = 
				new SimpleCellRenderer(fieldName, this.searchResultDefinition);
		}
		return this.cellRenderer.renderIntoCell(resultGrid, row, record, parentCrudViewDef, sectionCrudViewDef);
	}
}
