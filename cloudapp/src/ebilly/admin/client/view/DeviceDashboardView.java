package ebilly.admin.client.view;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import ebilly.admin.client.Application;
import ebilly.admin.client.DataUploadPanel;
import ebilly.admin.client.menu.ActionListener;
import ebilly.admin.client.menu.DeviceDashboardViewAction;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.DeviceFileUi;
import ebilly.admin.shared.DeviceUi;
import ebilly.admin.shared.viewdef.CrudResponse;

public class DeviceDashboardView extends AppViewPanel {

	private String mDeviceId;
	VerticalPanel mDataUploadPanelHolder;
	private VerticalPanel contentPanel;
	public DeviceDashboardView(String caption, String deviceId) {
		super(caption);
		mDeviceId = deviceId;
		buildUi();
	}
	
	private void buildUi() {
		contentPanel = new VerticalPanel();
		super.contentPanel.add(contentPanel);
		
		/* Collection Panel */
		buildCommandPanel();
		/* End of Collection Panel */
		
		VerticalPanel deviceDetailsPanel = new VerticalPanel();
		contentPanel.add(deviceDetailsPanel);
		
		DeviceCrudView deviceDetailsView = 
			new DeviceCrudView(CrudViewDefinition.CRUD_ACTION_UPDATE, 
					"", mDeviceId);
		deviceDetailsView.setContentPanelBorderSpace(0);
		deviceDetailsPanel.add(deviceDetailsView.getUiObject());
	}
	
	private void buildCommandPanel() {
		HorizontalPanel commandPanel = new HorizontalPanel();
		contentPanel.add(commandPanel);
		commandPanel.setWidth("100%");
		commandPanel.setStyleName("debug");
		commandPanel.setHorizontalAlignment(HorizontalPanel.ALIGN_RIGHT);
		ActionBarWidget collectionActionBar = new ActionBarWidget();
		commandPanel.add(collectionActionBar);
		/*Reset collection*/
		final Button resetCollectionBtn = new Button("Reset Collection");
		final Widget resetCollectionBtnWidget = UiUtil.decorateWithHelpText(resetCollectionBtn, 
				"A command will be sent to device, that resets Collection Limit amount. Resetting will unlock Collection if Device is locked");
		collectionActionBar.add(resetCollectionBtnWidget);
		resetCollectionBtn.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				resetCollectionBtn.setEnabled(false);
				ConfirmationDialog cnfDlg = 
					new ConfirmationDialog("Confirm", 
						"Are you sure you want to Reset Collection Amount in this device ?");
				cnfDlg.addNoActionListener(new ActionListener() {
					public void onBeforeAction(Object data) {}
					public void onAfterAction(Object data) {
						resetCollectionBtn.setEnabled(true);
						return;
					}
				});
				cnfDlg.addYesActionListener(new ActionListener() {
					public void onBeforeAction(Object data) {}
					public void onAfterAction(Object data) {
						Application.showMessage("Please wait...");
						Application.instance.secureService.resetDeviceCollection(mDeviceId, 
								new AsyncCallback<CrudResponse>() {
									public void onSuccess(CrudResponse result) {
										if (result.isSuccess()) {
											Application.showMessage("Successfully scheduled reset collection command");
											resetCollectionBtnWidget.setVisible(false);
										} else {
											Application.showErrorMessage(result.errorMessage);
											resetCollectionBtn.setEnabled(true);
										}
									}
									public void onFailure(Throwable caught) {
										Application.showErrorMessage(caught.getMessage());
										resetCollectionBtn.setEnabled(true);
									}
								});
					}
				});
			}
		});
		/*End Reset collection*/
		
		/*Device Unpairing*/
		if (Application.instance.canUserRepairDevice()) {
			final Button unpairBtn = new Button("Unpair");
			unpairBtn.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent event) {
					unpairBtn.setEnabled(false);
					Application.showMessage("Please wait...");
					Application.instance.secureService.resetDevicePairing(
							mDeviceId, new AsyncCallback<CrudResponse>() {
								public void onSuccess(CrudResponse result) {
									if (result.isSuccess()) {
										Application.showMessage(result.successMessage);
										unpairBtn.setVisible(false);
										// Reload dashboard
										DeviceDashboardViewAction a = new DeviceDashboardViewAction(mDeviceId);
										a.perform();
									} else {
										Application.showErrorMessage(result.errorMessage);
										unpairBtn.setEnabled(true);
									}
								}
								public void onFailure(Throwable caught) {
									unpairBtn.setEnabled(true);
								}
							});
				}
			});
			collectionActionBar.add(unpairBtn);
		}
		/*End Device Unpairing*/
	}
	
	private void showDataUploadPanel() {
		mDataUploadPanelHolder.clear();

		DataUploadPanel dataUploadPanel = 
			new DataUploadPanel(AppConfig.OBJ_DEVICE_SERVICE_FILE,mDeviceId);
		mDataUploadPanelHolder.add(dataUploadPanel.getUiObject());
		dataUploadPanel.setCancelHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				mDataUploadPanelHolder.clear();
			}
		});
		dataUploadPanel.setImportCompleteActionListener(new ActionListener() {
			public void onBeforeAction(Object data) {
			}
			public void onAfterAction(Object data) {
				DeviceDashboardViewAction a = new DeviceDashboardViewAction(mDeviceId);
				a.perform();
			}
		});
	}
	
	private void updateDeviceFileUi(DeviceUi deviceUi) {
		/*DeviceUi deviceUi = null;
		if (deviceDetailsView.getCrudEntity() != null) {
			deviceUi = (DeviceUi)deviceDetailsView.getCrudEntity();
		}*/
		
		List<DeviceFileUi> deviceFileUis = new ArrayList<DeviceFileUi>();
		if (deviceUi != null) {
			for (DeviceFileUi dUi :  deviceUi.getDeviceFiles()) {
				if (AppConfig.REMOTE_DEVICE_UPLOAD_STATES.READY_TO_UPLOAD_TO_REMOTE_DEVICE.toString()
						.equalsIgnoreCase(dUi.getRemoteDeviceFileUploadState())) {
					deviceFileUis.add(dUi);
				}
			}
		}
		
		FlowPanel deviceFileListPanel = new FlowPanel();
		contentPanel.add(deviceFileListPanel);
		if (!deviceFileUis.isEmpty()) {
			for (DeviceFileUi dUi : deviceFileUis) {
				HorizontalPanel hp = new HorizontalPanel();
				hp.add(new Label(dUi.getDeviceServicesFileName()));
				hp.add(new Label("("+UiUtil.getFormattedDate(dUi.getDeviceFileStagingDate())+")"));
				deviceFileListPanel.add(hp);
			}
		} else {
			deviceFileListPanel.add(new Label("-No-Files-"));
		}
	}
}