package ebilly.admin.client.view;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;

import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.viewdef.ResultRecord;

public class PrCollectionDateCellRenderer implements CellRenderer {

	SearchResultDefinition searchResultDef;
	private String dateField;
	public PrCollectionDateCellRenderer(String dateField,
			SearchResultDefinition searchResultDef) {
		super();
		this.searchResultDef = searchResultDef;
		this.dateField = dateField;
	}
	
	@Override
	public Cell renderIntoCell(FlexTable resultGrid, int row,
			ResultRecord record, CrudViewDefinition parentCrudViewDef,
			CrudViewDefinition sectionCrudViewDef) {
		Cell c = new Cell();
		String dateValue = CommonUtil.getReadableDate(searchResultDef.getValue(dateField, record));
		Label label = new Label(dateValue);
		label.setStyleName("pr-date-column");
		c.addWidget(label);
		return c;
	}
}
