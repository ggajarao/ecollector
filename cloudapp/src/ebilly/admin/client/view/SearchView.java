package ebilly.admin.client.view;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import ebilly.admin.client.Application;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.viewdef.ResultField;
import ebilly.admin.shared.viewdef.ResultRecord;
import ebilly.admin.shared.viewdef.SearchRequest;
import ebilly.admin.shared.viewdef.SearchResult;

public abstract class SearchView extends AppViewPanel {
	//private Panel contentPanel;
	private Button resetButton;
	private Button searchButton;
	
	private int numHidden = 0;
	

	protected SearchViewDef viewDef;
	
	/**
	 * Useful in cases where this search view is
	 * used as a section and its records should
	 * be editable.
	 */
	private CrudViewDefinition parentCrudViewDef;
	private CrudViewDefinition sectionCrudViewDef;
	
	private ActionBarWidget resultActionsPane;
	private boolean showResultsActionBar = false;
	
	private HTMLPanel resultsPanel;
	//private FlexTable resultGrid;
	
	public SearchView(String caption) {
		super(CommonUtil.isEmpty(caption)?"":"Search "+caption);
		this.viewDef = getSearchViewDefinition();
		buildUi();
	}
	
	public SearchView(String caption, boolean doNotPrefixCaption) {
		super(caption);
		this.viewDef = getSearchViewDefinition();
		buildUi();
	}
	
	public SearchView(String caption, boolean doNotPrefixCaption, boolean showResultsActionBar) {
		super(caption);
		this.viewDef = getSearchViewDefinition();
		this.showResultsActionBar = showResultsActionBar;
		buildUi();
	}
	
	
	protected abstract SearchViewDef getSearchViewDefinition();
	
	/**
	 * @wbp.parser.entryPoint
	 */
	private void buildUi() {
		ActionBarWidget actionBar = new ActionBarWidget();
		Button resetButton = null;
		Button searchButton = null;
		//if (!viewDef.hideFilter) {
			resetButton = new Button("Reset");
			resetButton.addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					viewDef.resetFilterInput();
				}
			});
			actionBar.add(resetButton);
			
			searchButton = new Button("Search");
			searchButton.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					//searchActionListener.onAfterAction(null);
					search();
				}
			});
			actionBar.add(searchButton);
		//}
		
		Widget flexTable = UiUtil.createSearchFilterWidget(this.viewDef,actionBar);
		
		contentPanel.add(flexTable);
		flexTable.setSize("100%", "");
				
		
		//this.numHidden = 0;
		/*for (ResultColumn resultColumn : this.viewDef.searchResultDef) {
			if (resultColumn.isHidden) this.numHidden++;
		}*/
		
		this.resultActionsPane = new ActionBarWidget();
		contentPanel.add(resultActionsPane);
		this.resultActionsPane.setVisible(this.showResultsActionBar);
		
		resultsPanel = new HTMLPanel("");
		resultsPanel.setWidth("100%");
		contentPanel.add(resultsPanel);
		
		resultsPanel.add(UiUtil.createResultsGrid(null,this.viewDef, null, null));
	}
	
	private void buildUiOld() {
		FlexTable flexTable = new FlexTable();
		contentPanel.add(flexTable);
		flexTable.setSize("100%", "");
		
		int rowCounter = 0;
		int colCounter = 0;
		int colOffset = 0;
		if (viewDef.searchFilterDef != null && !viewDef.hideFilter) {
			for (ViewField filter : viewDef.searchFilterDef.getFilterFields()) {
				boolean hideCaption = false;
				Widget widget = filter.getInputWidget();
				if (filter.helpText != null) {
					widget = UiUtil.decorateWithHelpText(widget,filter.helpText);
				}
				
				if (filter.getFieldType().equals(RoleSelectionPanel.class.getName()) ||
					filter.getFieldType().equals(DateOrDateRangeSelectionPanel.class.getName())) {
				/*if (widget instanceof RoleSelectionPanel ||
					widget instanceof DateOrDateRangeSelectionPanel) {*/
					hideCaption = true;
				}
				
				if (!hideCaption) {
					Label fieldLabel = new Label(filter.displayName);
					fieldLabel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
					flexTable.setWidget(rowCounter, colCounter+colOffset, fieldLabel);
					colOffset++;
					
				} else {
					flexTable.getFlexCellFormatter().setColSpan(
							rowCounter, colCounter+colOffset, 2);
				}
				
				flexTable.setWidget(rowCounter, colCounter+colOffset, widget);
				this.viewDef.filterInput.put(filter,widget);
				colOffset++;
				
				colCounter++;
				if (colCounter % 2 == 0) {
					colCounter = 0;
					colOffset = 0;
					rowCounter++;
				}
			}
		} else if (this.viewDef.searchFilterDef != null) {
			// In case of hide filter, initialize filter widgets
			for (ViewField filter : this.viewDef.searchFilterDef.getFilterFields()) {
				filter.getInputWidget();
			}
		}
		rowCounter++;
		
		ActionBarWidget horizontalPanel = new ActionBarWidget();
		flexTable.setWidget(rowCounter, 0, horizontalPanel);
		flexTable.getFlexCellFormatter().setHorizontalAlignment(rowCounter, 0, HorizontalPanel.ALIGN_CENTER);
		flexTable.getFlexCellFormatter().setColSpan(rowCounter, 0, 6);
		
		if (!this.viewDef.hideFilter) {
			resetButton = new Button("Reset");
			resetButton.addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					viewDef.resetFilterInput();
				}
			});
			horizontalPanel.add(resetButton);
			
			searchButton = new Button("Search");
			searchButton.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					search();
				}
			});
			horizontalPanel.add(searchButton);
			flexTable.getFlexCellFormatter().setColSpan(2, 0, 4);
		}		
		
		/*this.numHidden = 0;
		for (ResultColumn resultColumn : this.viewDef.searchResultDef) {
			if (resultColumn.isHidden) this.numHidden++;
		}*/
		
		this.resultActionsPane = new ActionBarWidget();
		contentPanel.add(resultActionsPane);
		this.resultActionsPane.setVisible(this.showResultsActionBar);
		//TODO: PLANNED RESULTS EXPORT
		/*Anchor downloadLink = new Anchor("Download");
		downloadLink.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				downloadResults();
			}
		});
		this.resultActionsPane.add(downloadLink);*/
		
		//Grid grid = new Grid(1, this.viewDef.searchResultDef.size()-numHidden);
		FlexTable grid = new FlexTable();
		//this.resultGrid = grid;
		//grid.setCellPadding(5);
		contentPanel.add(grid);
		grid.setWidth("100%");
		
		grid.getColumnFormatter().addStyleName(0, "search-result-column-first");
		
		int column = 0;
		for (ResultColumn resultColumn : this.viewDef.searchResultDef) {
			if (resultColumn.isHidden) {
				continue;
			}
			Label resultColumnLabel = new Label(resultColumn.displayName);
			grid.setWidget(0, column, resultColumnLabel);
			column++;
		}
		grid.getRowFormatter().addStyleName(0, "search-results-head");
	}
	
	public void downloadResults() {
		
	}
	
	public void search() {
		List<ResultField> resultColumns = new ArrayList<ResultField>();
		for (ResultColumn column : this.viewDef.searchResultDef) {
			resultColumns.add(new ResultField(column.fieldName,column.getFieldType()));
		}
		
		final SearchRequest request =
			new SearchRequest(this.viewDef.getSearchObjectName(),resultColumns);
		
		for (ViewField viewField : this.viewDef.searchFilterDef.getFilterFields()) {
			Widget inputWidget = viewField.getInputWidget();
			if (inputWidget == null) continue;
			if (inputWidget instanceof TextBox) {
				request.addSearchCriteria(viewField.fieldName, 
						((TextBox)inputWidget).getText().trim());
			} else if (inputWidget instanceof Hidden) {
				request.addSearchCriteria(viewField.fieldName, 
						((Hidden)inputWidget).getValue().trim());
			} else if (inputWidget instanceof RoleSelectionPanel) {
				request.addSearchCriteria(viewField.fieldName,
						((RoleSelectionPanel)inputWidget).getRoleId());
			} else if (inputWidget instanceof DateOrDateRangeSelectionPanel) {
				DateOrDateRangeSelectionPanel dodr = 
					(DateOrDateRangeSelectionPanel)inputWidget;
				if (dodr.isRangeSelection()) {
					request.addSearchCriteria(dodr.getFieldName(),dodr.getFromDateAsString(CommonUtil.DEFAULT_DATE_FORMAT),AppConfig.FILTER_CONJUNCTION_AND, AppConfig.FILTER_OPERATOR_GREATER_EQUALS);
					request.addSearchCriteria(dodr.getFieldName(),dodr.getToDateAsString(CommonUtil.DEFAULT_DATE_FORMAT),AppConfig.FILTER_CONJUNCTION_AND, AppConfig.FILTER_OPERATOR_LESS_EQUALS);					
				} else {
					request.addSearchCriteria(dodr.getFieldName(),dodr.getDateAsString(CommonUtil.DEFAULT_DATE_FORMAT),AppConfig.FILTER_CONJUNCTION_AND, AppConfig.FILTER_OPERATOR_GREATER_EQUALS);;
				}
			}
		}
		Application.showMessage("Loading...");
		Application.secureService.searchView(request, new BaseServiceResponseHandler<SearchResult>() {
			@Override
			public void onSuccess(SearchResult result) {
				
				if (result.hasErrors())
					Application.showErrorMessage(result.getErrorMessage());
				else
					showMessage("");
				
				//List<ResultRecord> results = result.getResults();
				showResults(result,request);
			}
		});
	}
	
	private void clearResults() {
		/*int rowCount = this.resultGrid.getRowCount();
		for (int i = 1; i < rowCount; i++) {
			this.resultGrid.removeRow(i);
		}*/
		this.resultsPanel.clear();
	}
	
	public void showResults(SearchResult result, SearchRequest request) {
		onBeforeResultsRender(result,request);
		Widget resultsWidget = UiUtil.createResultsGrid(
				result,this.viewDef,
				this.parentCrudViewDef,
				this.sectionCrudViewDef);
		this.resultsPanel.clear();
		this.resultsPanel.add(resultsWidget);
	}
	
	/*public void showResults(SearchResult result, SearchRequest request) {
		List<ResultRecord> results = result.getResults();
		
		if (result.hasErrors()) {
			Application.showErrorMessage(result.getErrorMessage());
			return;
		}
		
		clearResults();
		Application.showMessage("Preparing results...");
		onBeforeResultsRender(result,request);
		
		//this.resultGrid.resizeRows(1);
		//this.resultGrid.resizeRows(results.size()+1);
		
		if (results == null || results.isEmpty()) {
			int firstRow = 1, firstCol = 0;
			this.resultGrid.setWidget(firstRow, firstCol, new DataLabel("No Records"));
			this.resultGrid.getFlexCellFormatter().setColSpan(firstRow, firstCol, 
					this.viewDef.searchResultDef.size() - this.numHidden);
			this.resultGrid.getFlexCellFormatter().setHorizontalAlignment(
					firstRow, firstCol, HorizontalPanel.ALIGN_CENTER);
		}
		
		int rowNumber = 1;
		String styleName = "search-results-row-even";
		for (ResultRecord record : results) {			
			// check if result record field count is incompatible 
			// with search result definition
			if (record.fieldValues.isEmpty() || 
					record.fieldValues.size() != viewDef.searchResultDef.size()) {
				continue;
			}
			int numHidden = 0;
			for (int colCounter = 0; colCounter < viewDef.searchResultDef.size(); colCounter++) {
				ResultColumn resultColumn = viewDef.searchResultDef.get(colCounter);
				if (resultColumn.isHidden) {
					numHidden++;
					continue;
				}
				this.resultGrid.setWidget(rowNumber, colCounter - numHidden, 
						resultColumn.getCellWidget(this.resultGrid, rowNumber, record));
			}
			if (rowNumber%2 == 0) {
				styleName = "search-results-row-even";
			} else {
				styleName = "search-results-row-odd";
			}
			
			this.resultGrid.getRowFormatter().addStyleName(rowNumber, styleName);
			rowNumber++;
		}
		
		// Clear the message that we began showing at the method start.
		Application.showMessage("");
	}*/

	/**
	 * Override this method to get a notification after the results
	 * are recieved from service call and just before rendering the
	 * results. Common usage is the sort the results.
	 * @param result
	 * @param request
	 */
	protected void onBeforeResultsRender(SearchResult result,
			SearchRequest request) {
		return;
	}
	
	@Override
	public void onBeforeOpen() {
		super.onBeforeOpen();
		search();
	}
	
	public ActionBarWidget getResultsActionBarWidget() {
		return this.resultActionsPane;
	}
}