package ebilly.admin.client.view;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import ebilly.admin.client.Application;
import ebilly.admin.client.DataUploadPanel;
import ebilly.admin.client.NewDataUploadPanel;
import ebilly.admin.client.menu.ActionListener;
import ebilly.admin.client.menu.DeviceDashboardViewAction;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.DeviceFileUi;
import ebilly.admin.shared.DeviceSummaryReport;
import ebilly.admin.shared.DeviceUi;
import ebilly.admin.shared.viewdef.CrudResponse;
import ebilly.admin.shared.viewdef.ResultRecord;
import ebilly.admin.shared.viewdef.SectionData;

public class XDeviceDashboardView extends AppViewPanel {

	private String mDeviceId;
	VerticalPanel mDataUploadPanelHolder;
	private VerticalPanel mainPanel;
	HTMLPanel deviceContentPanel;
	private final CrudViewDefinition deviceViewDef = new CrudViewDefinition();
	
	private FlexHtmlDataCell collectionDataCell = null;
	private FlexHtmlDataCell collectionMarginDataCell = null;
	private FlexHtmlDataCell prCountDataCell = null;
	private FlexHtmlDataCell totalServicesDataCell = null;
	//private FlexHtmlDataCell totalPushedServicesDataCell = null;
	private FlexHtmlDataCell totalReadyToPushServicesDataCell = null;
	private FlexHtmlDataCell reportDateDataCell = null;
	
	
	private boolean isPollingCancelled = false;
	private int numberOfPolls = 0;
	private int maxPolls = 120;

	public XDeviceDashboardView(String caption, String deviceId) {
		super(caption);
		this.setContentPanelBorderSpace(0);
		deviceViewDef.setCrudAction(CrudViewDefinition.CRUD_ACTION_UPDATE);
		deviceViewDef.setObjectName(AppConfig.OBJ_DEVICE);
		initViewDefinition(deviceViewDef);
		mDeviceId = deviceId;
		buildUi();
		Scheduler.get().scheduleDeferred(new ScheduledCommand() {
			@Override
			public void execute() {
				loadRecord(mDeviceId);
			}
		});
		
		if (!isPollingCancelled) {
			Scheduler.get().scheduleFixedDelay(
					new RepeatingCommand() {
						public boolean execute() {
							if (isPollingCancelled) return false;
							numberOfPolls++;
							loadSummaryReport(mDeviceId);
							return numberOfPolls  < maxPolls;
						}
					}, 15*1000);
		}
	}
	
	@Override
	public void onBeforeClose() {
		isPollingCancelled = true;
	}
	
	private void loadSummaryReport(String deviceId) {
		Application.showMessage("Loading...");
		Application.secureService.fetchDeviceSummaryReport(deviceId, 
				new BaseServiceResponseHandler<CrudResponse>() {
			/*@Override
			public void onFailure(Throwable caught) {
				Application.showErrorMessage(
						"Error fetching summary, reason: "+caught.getMessage());
			}*/
			@Override
			public void onSuccess(CrudResponse result) {
				updateSummaryUi((DeviceSummaryReport)result.crudEntity);
				Application.showMessage("");
			}
		});
	}

	private void loadRecord(String deviceId) {
		UiUtil.loadRecord(deviceId, AppConfig.OBJ_DEVICE, deviceViewDef,
				new ActionListener() {
					@Override
					public void onBeforeAction(Object data) {
					}
					@Override
					public void onAfterAction(Object data) {
						showDeviceFiles();
						updateSummaryUi(((DeviceUi)deviceViewDef.getCrudEntity()).deviceSummaryReport());
					}
				});
	}

	private void updateSummaryUi(DeviceSummaryReport deviceSummaryReport) {
		if (deviceSummaryReport == null) {
			showNoSummaryContent();
		} else {
			showSummaryContent();
			collectionMarginDataCell.setValue(deviceSummaryReport.getmCollectionMargin());
			collectionDataCell.setValue(deviceSummaryReport.getmTotalCollection());
			prCountDataCell.setValue(deviceSummaryReport.getmTotalPrCount()+"");
			totalServicesDataCell.setValue(deviceSummaryReport.getmTotalServicesCount()+"");
			//totalPushedServicesDataCell.setValue(deviceSummaryReport.getmTotalPushedServicesCount());
			totalReadyToPushServicesDataCell.setValue(deviceSummaryReport.getmTotalReadyToPushServicesCount()+"");
			reportDateDataCell.setValue(UiUtil.getFormattedDateTime(deviceSummaryReport.getmReportDate()));
		}
	}

	private void initViewDefinition(CrudViewDefinition vd) {
		ViewField vf = new ViewField("id", java.lang.String.class.getName(),
				"Id");
		vf.isMandatory = false;
		vf.isHidden = true;
		vd.addViewField(vf);

		vf = new ViewField("deviceServicesFileKey",
				java.lang.String.class.getName(), "deviceServicesFileKey");
		vf.isMandatory = false;
		vf.isHidden = true;
		vd.addViewField(vf);

		vf = new ViewField("deviceName", java.lang.String.class.getName(),
				"Device Name");
		vf.setMaxLenght(10);
		vf.setMandatory(true);
		vf.setReadOnlyOnEdit(true);
		vd.addViewField(vf);

		vf = new ViewField("pairingStatus", java.lang.String.class.getName(),
				"Pairing Status");
		vf.setReadOnlyOnEdit(true);
		vd.addViewField(vf);

		vf = new ViewField("collectionExpirationDate",
				java.util.Date.class.getName(), "Collection Expires on");
		vf.helpText = "Set this date value to automatically lock this Device from generating PRs";
		vd.addViewField(vf);

		vf = new ViewField("collectionLimit", java.lang.String.class.getName(),
				"Collection Limit (Rs)");
		vf.helpText = "Set this amount value to automatically lock this Device from generating PRs once the collected amount exceeds this value";
		vd.addViewField(vf);
		
		vf = new ViewField("deviceIdentifier", java.lang.String.class.getName(),
							"UUID");
		vd.addViewField(vf);
		

		SearchViewDef searchViewDef = new SearchViewDef() {

			@Override
			protected void initSearchFilterDef(
					SearchFilterDefinition searchFilterDef) {

			}

			@Override
			protected void initSearchResultDef(
					SearchResultDefinition searchResultDef) {
				ResultColumn rc = new ResultColumn("id",
						java.lang.String.class.getName(), "id");
				rc.isHidden = true;
				searchResultDef.add(rc);

				rc = new ResultColumn("deviceServicesFileName",
						java.lang.String.class.getName(), "");
				rc.setCellRenderer(new DeviceFileCellRenerer(
						"id",
						"deviceServicesFileName",
						"remoteDeviceImportMethod",
						"remoteDeviceFileUploadState",
						"deviceFileStagingDate",
						searchResultDef));
				searchResultDef.add(rc);

				rc = new ResultColumn("deviceFileStagingDate",
						java.lang.String.class.getName(), "Uploaded On");
				rc.isHidden = true;
				searchResultDef.add(rc);

				rc = new ResultColumn("remoteDeviceFileUploadState",
						java.lang.String.class.getName(), "State");
				rc.isHidden = true;
				rc.setCellRenderer(new DeviceFileUploadStateRenderer(
						"remoteDeviceFileUploadState", searchResultDef));
				searchResultDef.add(rc);

				rc = new ResultColumn("remoteDeviceImportMethod",
						java.lang.String.class.getName(), "Import Method");
				rc.isHidden = true;
				// rc.setCellRenderer(new
				// DeviceFileUploadStateRenderer("remoteDeviceFileUploadState",searchResultDef));
				searchResultDef.add(rc);
				

				/*rc = new ResultColumn("action",
						java.lang.String.class.getName(), "Action");
				rc.setCellRenderer(new DeviceFileActionsRenderer(
						"remoteDeviceFileUploadState", searchResultDef));
				searchResultDef.add(rc);*/
			}

			@Override
			public String getSearchObjectName() {
				return "";
			}
		};
		searchViewDef.hideFilter = true;
		ViewSectionDefinition viewSectionDef = new ViewSectionDefinition(
				AppConfig.SECTION_DEVICE_FILES, searchViewDef);

		vd.addViewSectionDefinition(viewSectionDef);
	}

	private void buildUi() {
		mainPanel = new VerticalPanel();
		super.contentPanel.add(mainPanel);

		HTMLPanel dd = new HTMLPanel("");
		dd.addStyleName("device-dashboard");

		HTMLPanel deviceSummary = createDeviceSummaryPanel();
		dd.add(deviceSummary);

		HTMLPanel dashboardBody = new HTMLPanel("");
		dashboardBody.setStyleName("dashboardBody");
		dd.add(dashboardBody);
		
		HTMLPanel deviceInfo = createDeviceInfoPanel();
		dashboardBody.add(deviceInfo);

		HTMLPanel deviceActions = createDeviceActionsPanel();
		dashboardBody.add(deviceActions);
		
		HTMLPanel deviceContent = createDeviceContentPanel();
		dashboardBody.add(deviceContent);

		mainPanel.setWidth("100%");
		mainPanel.add(dd);
	}
	
	private HTMLPanel createDeviceContentPanel() {
		
		HTMLPanel deviceContentRoot = new HTMLPanel("");
		deviceContentRoot.addStyleName("deviceContent");
		
		HTMLPanel infoOutset = new HTMLPanel("");
		infoOutset.addStyleName("tile-outset");
		deviceContentRoot.add(infoOutset);
		HTMLPanel infoInset = new HTMLPanel("");
		infoInset.addStyleName("tile-inset");
		infoOutset.add(infoInset);
		
		deviceContentPanel = new HTMLPanel("");
		infoInset.add(deviceContentPanel);
		
		return deviceContentRoot;
	}

	private HTMLPanel createDeviceActionsPanel() {
		HTMLPanel deviceActions = new HTMLPanel("");
		deviceActions.addStyleName("deviceActions");
		
		HTMLPanel infoOutset = new HTMLPanel("");
		infoOutset.addStyleName("tile-outset");
		deviceActions.add(infoOutset);
		HTMLPanel infoInset = new HTMLPanel("");
		infoInset.addStyleName("tile-inset");
		infoOutset.add(infoInset);
		
		HTMLPanel col1 = createCellColumn();
		
		col1.setWidth("100%");
		infoInset.add(col1);
		
		Button addFile = null;
		addFile = new Button("Send Data File");
		addFile.setStyleName("command-button");
		addFile.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				showDeviceFileUploadPanel(); // upload without zip
				//showDeviceFileUploadPanelAdvanced();
			}
		});
		FlexHtmlCell cell = createCommandCell();
		col1.add(cell);
		cell.add(addFile);
		
		Widget resetCollection = creatResetCollectionButton();
		col1.add(resetCollection);
		cell = createCommandCell();
		col1.add(cell);
		cell.add(resetCollection);
		
		Widget unpairBtn = createUnpairButton();
		if (unpairBtn != null) {
			cell = createCommandCell();
			col1.add(cell);
			cell.add(unpairBtn);
		}
		
		return deviceActions;
	}
	
	private FlexHtmlCell createCommandCell() {
		FlexHtmlCell cell = new FlexHtmlCell();
		cell.setStyleName("cell-command");
		return cell;
	}

	private void showDeviceFileUploadPanelAdvanced() {
		final DialogBox dialog = new DialogBox();
		dialog.addStyleName("uplad-panel-dialog");
		dialog.setAnimationEnabled(true);
		dialog.setText("Upload Device Services File( Zips Automatically )");
		dialog.setGlassEnabled(true);
		dialog.setAutoHideEnabled(false);
		
		NewDataUploadPanel dataUploadPanel = 
			new NewDataUploadPanel(mDeviceId);
		dataUploadPanel.setCancelHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				dialog.hide();
			}
		});
		dataUploadPanel.setImportCompleteActionListener(new ActionListener() {
			public void onBeforeAction(Object data) {
			}
			public void onAfterAction(Object data) {
				dialog.hide();
				DeviceDashboardViewAction a = new DeviceDashboardViewAction(mDeviceId);
				a.perform();
			}
		});
		
		Anchor simpleMode = new Anchor("Switch to Simple");
		simpleMode.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				dialog.hide();
				XDeviceDashboardView.this.showDeviceFileUploadPanel();
			}
		});
		dataUploadPanel.addActionWidget(simpleMode);
		
		dialog.add(dataUploadPanel.getUiObject());
		dialog.center();
		dialog.show();
	}
	
	/**
	 * This is for regular uploads, which does not
	 * use applet, and thus does not zip the
	 * selected file before uploading.
	 * 
	 * Use of this is disabled, code is vaulted
	 * so that in emergencies we can default 
	 * to this method of upload.
	 */
	private void showDeviceFileUploadPanel() {
		final DialogBox dialog = new DialogBox();
		dialog.addStyleName("uplad-panel-dialog");
		dialog.setAnimationEnabled(true);
		dialog.setText("Upload Device Services File");
		dialog.setGlassEnabled(true);
		dialog.setAutoHideEnabled(false);
		
		DataUploadPanel dataUploadPanel = 
			new DataUploadPanel(AppConfig.OBJ_DEVICE_SERVICE_FILE, mDeviceId);
		dataUploadPanel.setCancelHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				dialog.hide();
			}
		});
		dataUploadPanel.setImportCompleteActionListener(new ActionListener() {
			public void onBeforeAction(Object data) {
			}
			public void onAfterAction(Object data) {
				dialog.hide();
				DeviceDashboardViewAction a = new DeviceDashboardViewAction(mDeviceId);
				a.perform();
			}
		});
		
		Anchor zipUploader = new Anchor("Switch to Advanced");
		zipUploader.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				dialog.hide();
				XDeviceDashboardView.this.showDeviceFileUploadPanelAdvanced();
			}
		});
		dataUploadPanel.addActionWidget(zipUploader);
		
		
		dialog.add(dataUploadPanel.getUiObject());
		dialog.center();
		dialog.show();
	}
	
	private void showDeviceFiles() {
		deviceContentPanel.clear();
		List<SectionData> sections = this.deviceViewDef.getSectionDataList();
		for (ViewSectionDefinition sectionDef : this.deviceViewDef.viewSectionDefinitions.values()) {
			for (SectionData section : sections) {
				Widget sectionView = 
					sectionDef.getSectionViewFor(section);
				sectionView.setStyleName("");
				deviceContentPanel.add(sectionView);
			}
		}
	}

	private HTMLPanel createDeviceInfoPanel() {
		HTMLPanel infoPanel = new HTMLPanel("");
		infoPanel.addStyleName("deviceInfo");
		
		HTMLPanel infoOutset = new HTMLPanel("");
		infoOutset.addStyleName("tile-outset");
		infoPanel.add(infoOutset);
		HTMLPanel infoInset = new HTMLPanel("");
		infoInset.addStyleName("tile-inset");
		infoOutset.add(infoInset);
		
		
		FlexHtmlTupleCell tuple = new FlexHtmlTupleCell();
		deviceViewDef.setHtmlTuple("deviceName", tuple);
		infoInset.add(tuple);
		
		tuple = new FlexHtmlTupleCell();
		deviceViewDef.setHtmlTuple("collectionExpirationDate", tuple);
		infoInset.add(tuple);
		final FlexHtmlDataCell editableCell = tuple.getDataCell();
		editableCell.setEditable(true);
		editableCell.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				editableCell.acceptInput("collectionExpirationDate", deviceViewDef);
			}
		});
		tuple = new FlexHtmlTupleCell();
		deviceViewDef.setHtmlTuple("collectionLimit", tuple);
		final FlexHtmlDataCell collectionLimitEditor = tuple.getDataCell();
		collectionLimitEditor.setEditable(true);
		collectionLimitEditor.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				collectionLimitEditor.acceptInput("collectionLimit", deviceViewDef);
			}
		});
		infoInset.add(tuple);
		
		tuple = new FlexHtmlTupleCell();
		deviceViewDef.setHtmlTuple("pairingStatus", tuple);
		infoInset.add(tuple);
		
		tuple = new FlexHtmlTupleCell();
		deviceViewDef.setHtmlTuple("deviceIdentifier", tuple);
		infoInset.add(tuple);
		
		return infoPanel;
	}

	private HTMLPanel deviceSummaryPanel;
	private HTMLPanel summaryContent;
	private HTMLPanel noSummaryContent;
	private HTMLPanel createDeviceSummaryPanel() {
		HTMLPanel deviceSummaryRoot = new HTMLPanel("");
		deviceSummaryRoot.setStyleName("db-outset");
		deviceSummaryPanel = new HTMLPanel("");
		deviceSummaryRoot.add(deviceSummaryPanel);
		deviceSummaryPanel.setStyleName("db-inset");
		
		summaryContent = new HTMLPanel("");
		deviceSummaryPanel.add(summaryContent);
		
		HTMLPanel col1 = createCellColumn();
		summaryContent.add(col1);
		
		HTMLPanel col2 = createCellColumn();
		summaryContent.add(col2);
		
		HTMLPanel col3 = createCellColumn();
		summaryContent.add(col3);
		
		FlexHtmlTupleCell tuple = new FlexHtmlTupleCell();
		tuple.getCaptionCell().setValue("Collection");
		collectionDataCell = tuple.getDataCell();
		col1.add(tuple);
		
		tuple = new FlexHtmlTupleCell();
		tuple.getCaptionCell().setValue("Margin");
		collectionMarginDataCell = tuple.getDataCell();
		col1.add(tuple);
		
		tuple = new FlexHtmlTupleCell();
		tuple.getCaptionCell().setValue("PRs");
		prCountDataCell = tuple.getDataCell();
		col2.add(tuple);
		
		tuple = new FlexHtmlTupleCell();
		tuple.getCaptionCell().setValue("Loaded Services");
		totalServicesDataCell = tuple.getDataCell();
		col2.add(tuple);
		
		tuple = new FlexHtmlTupleCell();
		tuple.getCaptionCell().setValue("Ready To Push PRs");
		totalReadyToPushServicesDataCell = tuple.getDataCell();
		col3.add(tuple);
		
		tuple = new FlexHtmlTupleCell();
		tuple.getCaptionCell().setValue("Reported Time");
		reportDateDataCell = tuple.getDataCell();
		col3.add(tuple);
		
		createNoSummaryContent();
		
		return deviceSummaryRoot;
	}
	
	private void createNoSummaryContent() {
		noSummaryContent = new HTMLPanel("");
		noSummaryContent.setVisible(false);
		deviceSummaryPanel.add(noSummaryContent);
		FlexHtmlCell noSummary = new FlexHtmlCell();
		noSummary.setStyleName("data-cell");
		noSummary.setValue("Device Disconnected, Summary not available !");
		noSummaryContent.add(noSummary);
	}
	
	private void showSummaryContent() {
		summaryContent.setVisible(true);
		noSummaryContent.setVisible(false);
	}
	
	private void showNoSummaryContent() {
		summaryContent.setVisible(false);
		noSummaryContent.setVisible(true);
	}

	private Widget createCell() {
		HTMLPanel cellPanel = new HTMLPanel("");
		cellPanel.addStyleName("cell");

		HTMLPanel labelComp = new HTMLPanel("XXXXXXXXXX");
		labelComp.addStyleName("caption-cell");
		HTMLPanel dataComp = new HTMLPanel("XXXXXXXXXXXXXXXXXXXXXX");
		dataComp.addStyleName("data-cell");

		cellPanel.add(labelComp);
		cellPanel.add(dataComp);

		return cellPanel;
	}

	private HTMLPanel createCellColumn() {
		HTMLPanel cellColumn = new HTMLPanel("");
		cellColumn.addStyleName("cell-column");
		return cellColumn;
	}
	
	private Widget creatResetCollectionButton() {
		final Button resetCollectionBtn = new Button("Reset Collection");
		resetCollectionBtn.setStyleName("command-button");
		final Widget resetCollectionBtnWidget = UiUtil
				.decorateWithHelpText(
						resetCollectionBtn,
						"A command will be sent to device, that resets Collection Limit amount. Resetting will unlock Collection if Device is locked");
		//collectionActionBar.add(resetCollectionBtnWidget);
		resetCollectionBtn.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				resetCollectionBtn.setEnabled(false);
				ConfirmationDialog cnfDlg = new ConfirmationDialog("Confirm",
						"Are you sure you want to Reset Collection Amount in this device ?");
				cnfDlg.addNoActionListener(new ActionListener() {
					public void onBeforeAction(Object data) {
					}

					public void onAfterAction(Object data) {
						resetCollectionBtn.setEnabled(true);
						return;
					}
				});
				cnfDlg.addYesActionListener(new ActionListener() {
					public void onBeforeAction(Object data) {
					}

					public void onAfterAction(Object data) {
						Application.showMessage("Please wait...");
						Application.instance.secureService
								.resetDeviceCollection(mDeviceId,
										new BaseServiceResponseHandler<CrudResponse>() {
											public void onSuccess(
													CrudResponse result) {
												if (result.isSuccess()) {
													Application
															.showMessage("Successfully scheduled reset collection command");
													resetCollectionBtnWidget
															.setVisible(false);
												} else {
													Application
															.showErrorMessage(result.errorMessage);
													resetCollectionBtn
															.setEnabled(true);
												}
											}

											public void onFailure(
													Throwable caught) {
												/*Application
														.showErrorMessage(caught
																.getMessage());*/
												resetCollectionBtn.setEnabled(true);
												super.onFailure(caught);
											}
										});
					}
				});
			}
		});
		return resetCollectionBtnWidget;
	}
	
	private Widget createUnpairButton() {
		if (!Application.instance.canUserRepairDevice()) {
			return null;
		}
		final Button unpairBtn = new Button("Unpair");
		unpairBtn.setStyleName("command-button");
		unpairBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				unpairBtn.setEnabled(false);
				Application.showMessage("Please wait...");
				Application.instance.secureService.resetDevicePairing(
						mDeviceId, new BaseServiceResponseHandler<CrudResponse>() {
							public void onSuccess(CrudResponse result) {
								if (result.isSuccess()) {
									Application
									.showMessage(result.successMessage);
									unpairBtn.setVisible(false);
									// Reload dashboard
									DeviceDashboardViewAction a = new DeviceDashboardViewAction(
											mDeviceId);
									a.perform();
								} else {
									Application
									.showErrorMessage(result.errorMessage);
									unpairBtn.setEnabled(true);
								}
							}

							public void onFailure(Throwable caught) {
								unpairBtn.setEnabled(true);
								super.onFailure(caught);
							}
						});
			}
		});
		return unpairBtn;
		//collectionActionBar.add(unpairBtn);
	}

	private void buildCommandPanel() {
		HorizontalPanel commandPanel = new HorizontalPanel();
		mainPanel.add(commandPanel);
		commandPanel.setWidth("100%");
		commandPanel.setStyleName("debug");
		commandPanel.setHorizontalAlignment(HorizontalPanel.ALIGN_RIGHT);
		ActionBarWidget collectionActionBar = new ActionBarWidget();
		commandPanel.add(collectionActionBar);
		/* Reset collection */
		
		/* End Reset collection */

		/* Device Unpairing */
		/* End Device Unpairing */
	}

	private void showDataUploadPanel() {
		mDataUploadPanelHolder.clear();

		DataUploadPanel dataUploadPanel = new DataUploadPanel(
				AppConfig.OBJ_DEVICE_SERVICE_FILE, mDeviceId);
		mDataUploadPanelHolder.add(dataUploadPanel.getUiObject());
		dataUploadPanel.setCancelHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				mDataUploadPanelHolder.clear();
			}
		});
		dataUploadPanel.setImportCompleteActionListener(new ActionListener() {
			public void onBeforeAction(Object data) {
			}

			public void onAfterAction(Object data) {
				DeviceDashboardViewAction a = new DeviceDashboardViewAction(
						mDeviceId);
				a.perform();
			}
		});
	}

	private void updateDeviceFileUi(DeviceUi deviceUi) {
		/*
		 * DeviceUi deviceUi = null; if (deviceDetailsView.getCrudEntity() !=
		 * null) { deviceUi = (DeviceUi)deviceDetailsView.getCrudEntity(); }
		 */

		List<DeviceFileUi> deviceFileUis = new ArrayList<DeviceFileUi>();
		if (deviceUi != null) {
			for (DeviceFileUi dUi : deviceUi.getDeviceFiles()) {
				if (AppConfig.REMOTE_DEVICE_UPLOAD_STATES.READY_TO_UPLOAD_TO_REMOTE_DEVICE
						.toString().equalsIgnoreCase(
								dUi.getRemoteDeviceFileUploadState())) {
					deviceFileUis.add(dUi);
				}
			}
		}

		FlowPanel deviceFileListPanel = new FlowPanel();
		mainPanel.add(deviceFileListPanel);
		if (!deviceFileUis.isEmpty()) {
			for (DeviceFileUi dUi : deviceFileUis) {
				HorizontalPanel hp = new HorizontalPanel();
				hp.add(new Label(dUi.getDeviceServicesFileName()));
				hp.add(new Label("("
						+ UiUtil.getFormattedDate(dUi
								.getDeviceFileStagingDate()) + ")"));
				deviceFileListPanel.add(hp);
			}
		} else {
			deviceFileListPanel.add(new Label("-No-Files-"));
		}
	}
}

class DeviceFileCellRenerer implements CellRenderer {
	SearchResultDefinition searchResultDef;
	private String idField;
	private String fileNameField;
	private String importMethodField;
	private String statusField; 
	private String deviceFileStagingDateField;
	private static int rowOffsetWhileRemovingRow = 0;
	public DeviceFileCellRenerer(
			String idField,
			String fileNameField,
			String importMethodField,
			String statusField,
			String deviceFileStagingDateField,
			SearchResultDefinition searchResultDef) {
		super();
		this.idField = idField;
		this.fileNameField = fileNameField;
		this.importMethodField = importMethodField;
		this.statusField = statusField;
		this.deviceFileStagingDateField = deviceFileStagingDateField;
		this.searchResultDef = searchResultDef;
		rowOffsetWhileRemovingRow = 0;
	}

	@Override
	public Cell renderIntoCell(FlexTable resultGrid, int row,
			ResultRecord record, CrudViewDefinition parentCrudViewDef,
			CrudViewDefinition sectionCrudViewDef) {
		final String deviceFileId = this.searchResultDef.getValue(this.idField, record);
		String status = this.searchResultDef.getValue(this.statusField, record);
		String fileName = this.searchResultDef.getValue(this.fileNameField, record);
		String importMethod = this.searchResultDef.getValue(this.importMethodField, record);
		String stagingDate = this.searchResultDef.getValue(this.deviceFileStagingDateField, record);
		
		
		final VerticalPanel vp = new VerticalPanel();
		vp.setWidth("100%");
		HorizontalPanel dash = new HorizontalPanel();
		dash.setWidth("100%");
		vp.add(dash);
		vp.setCellHorizontalAlignment(dash, HorizontalPanel.ALIGN_LEFT);
		
		FlexHtmlCaptionCell stagingDateLabel = new FlexHtmlCaptionCell();
		stagingDateLabel.setValue(stagingDate);
		dash.add(stagingDateLabel);
		dash.setCellHorizontalAlignment(stagingDateLabel, HorizontalPanel.ALIGN_LEFT);
		final Anchor a = new Anchor("Delete");
		dash.add(a);
		dash.setCellHorizontalAlignment(a, HorizontalPanel.ALIGN_RIGHT);
		
		a.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Application.showMessage("Removing Device File...");
				Application.instance.secureService.removeDeviceFiles(new String[]{deviceFileId},
						new BaseServiceResponseHandler<CrudResponse>() {
							public void onSuccess(CrudResponse result) {
								if (result.isSuccess()) {
									Application.showMessage(result.successMessage);
									//int rowCount = resultGrid.getRowCount();
									//resultGrid.removeRow(row-rowOffsetWhileRemovingRow);
									//rowOffsetWhileRemovingRow++;
									vp.setVisible(false);
								} else {
									Application.showErrorMessage(result.errorMessage);
								}
							}
							/*public void onFailure(Throwable caught) {
								Application.showErrorMessage("Fatal error: "+caught.getMessage());
							}*/
						}
				);
			}
		});
		
		FlexHtmlDataCell fileNameLabel = new FlexHtmlDataCell();
		fileNameLabel.setValue(fileName);
		vp.add(fileNameLabel);
		
		HorizontalPanel dash2 = new HorizontalPanel();
		vp.add(dash2);
		vp.setCellHorizontalAlignment(dash2, HorizontalPanel.ALIGN_LEFT);
		dash2.setSpacing(2);
		
		String statusReadable = "N/A";
		String statusStyle = "";
		if (AppConfig.REMOTE_DEVICE_UPLOAD_STATES.READY_TO_UPLOAD_TO_REMOTE_DEVICE.toString()
			.equalsIgnoreCase(status)) {
			statusReadable = "Waiting";
			statusStyle = "waiting-status-cell";
		} else if (AppConfig.REMOTE_DEVICE_UPLOAD_STATES.UPLOADED_TO_REMOTE_DEVICE.toString()
				.equalsIgnoreCase(status)) {
			statusReadable = "File Sent";
			statusStyle = "sent-status-cell";
		}
		FlexHtmlCell statusLabel = new FlexHtmlCell();
		statusLabel.addStyleName(statusStyle);
		statusLabel.setValue(statusReadable);
		dash2.add(statusLabel);
		
		String importMethodReadable = "N/A";
		String importMethodStyle = "";
		if (AppConfig.REMOTE_DEVICE_IMPORT_METHOD.CLEAR_EXISTING_DATA.toString()
			.equalsIgnoreCase(importMethod)) {
			importMethodReadable = "Overwrite";
			importMethodStyle = "overwrite-import-method-cell";
		} else if (AppConfig.REMOTE_DEVICE_IMPORT_METHOD.ADD_TO_EXISTING_DATA.toString()
				.equalsIgnoreCase(importMethod)) {
			importMethodReadable = "Append";
			importMethodStyle = "append-import-method-cell";
		}
		FlexHtmlCell importMethodLabel = new FlexHtmlCell();
		importMethodLabel.setValue(importMethodReadable);
		importMethodLabel.addStyleName(importMethodStyle);
		dash2.add(importMethodLabel);
		
		Cell c = new Cell();
		c.addWidget(vp);
		return c;
	}
}