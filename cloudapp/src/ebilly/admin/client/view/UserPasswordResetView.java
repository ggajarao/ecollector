package ebilly.admin.client.view;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import ebilly.admin.client.Application;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.UserUi;
import ebilly.admin.shared.viewdef.CrudResponse;

public class UserPasswordResetView extends AppViewPanel {

	private String userId;
	
	private FlexTable flexTable;
	private VerticalPanel passwordResetPanel;
	private VerticalPanel userDetailsPanel;
	private UserUi userUi;
	private ActionBarWidget actionBarWidget;
	private Button resetPassword;
	private Button sendPwdResetEmail;
	
	public UserPasswordResetView(String userId) {
		super("Reset User Password");
		this.userId = userId;
		Scheduler.get().scheduleDeferred(new ScheduledCommand() {
			@Override
			public void execute() {
				showMessage("Loading...");
				Application.instance.secureService.fetchUser(
						UserPasswordResetView.this.userId,
						new BaseServiceResponseHandler<UserUi>() {
							/*@Override
							public void onFailure(Throwable caught) {
								showError(caught.getMessage());
							}*/
							@Override
							public void onSuccess(UserUi userUi) {
								showMessage("");
								populateData(userUi);
							}
						});
			}
		});
		buildUi();
	}
	
	public void populateData(UserUi userUi) {
		this.userUi = userUi;
		buildUserPanel();
		buildUserResetPanel();
	}
	
	private void buildUserPanel() {
		this.userDetailsPanel.clear();
		String loginId = this.userUi.getLoginId();
		FlexTable p = new FlexTable();
		this.userDetailsPanel.add(p);
		p.setWidget(0, 0, new Label("Login Id"));
		p.setWidget(0, 1, new DataLabel(loginId));
	}

	private void buildUserResetPanel() {
		this.passwordResetPanel.clear();
		HorizontalPanel hp = new HorizontalPanel();
		hp.setSize("100%", "100%");
		hp.setSpacing(5);
		this.passwordResetPanel.add(hp);
		
		FlexTable manualResetPanel = new FlexTable();
		FlexTable automaticResetPanel = new FlexTable();
		
		VerticalPanel vp1 = new VerticalPanel();
		vp1.setSize("100%", "100%");
		vp1.setSpacing(5);
		vp1.add(manualResetPanel);
		hp.add(vp1);
		
		VerticalPanel vp2 = new VerticalPanel();
		vp2.setSize("100%", "100%");
		vp2.setSpacing(5);
		vp2.setStyleName("left-vertical-line");
		vp2.add(automaticResetPanel);
		hp.add(vp2);
		
		final AsyncCallback<CrudResponse> asyncCallback = 
			new BaseServiceResponseHandler<CrudResponse>() {
			public void onSuccess(CrudResponse result) {
				if (result.isSuccess()) {
					showMessage(result.successMessage);
					if (sendPwdResetEmail != null) {
						sendPwdResetEmail.setEnabled(false);
					}
					if (resetPassword != null) {
						resetPassword.setEnabled(false);
					}
				} else {
					showError(result.errorMessage);
				}
			};
			/*@Override
			public void onFailure(Throwable caught) {
				showError("Failed to reset user password, reason: "+
						caught.getMessage());
			}*/
		};
		
		// Manual Reset password panel construction
		buildManualResetPasswordPanel(manualResetPanel, asyncCallback);
		
		// Automatic password reset panel construction
		buildAutomaticResetPasswordPanel(automaticResetPanel, asyncCallback);
		
	}

	private void buildAutomaticResetPasswordPanel(
			FlexTable automaticResetPanel,
			final AsyncCallback<CrudResponse> asyncCallback) {
		int row = 0;
		int column = 0;
		automaticResetPanel.setSize("100%", "100%");
		Label title = new Label("Autogenerate password and send an email to user");
		title.setStyleName("section-title-panel");
		automaticResetPanel.getFlexCellFormatter().setColSpan(row, column, 2);
		automaticResetPanel.setWidget(row,column,title);
		
		final String userEmail = this.userUi.getEmail();
		
		if (CommonUtil.isEmpty(userEmail)) {
			
			row++;
			column = 0;
			final TextBox emailTextbox = new TextBox();
			automaticResetPanel.setWidget(row,column++, new Label("User Email Address"));
			automaticResetPanel.setWidget(row, column++, emailTextbox);
			
			row++;
			column = 0;
			sendPwdResetEmail = new Button("Send Password Reset Email");
			automaticResetPanel.getFlexCellFormatter().setColSpan(row, column, 2);
			automaticResetPanel.getFlexCellFormatter().setAlignment(row, column, HorizontalPanel.ALIGN_CENTER, VerticalPanel.ALIGN_MIDDLE);
			automaticResetPanel.setWidget(row, column, sendPwdResetEmail);
			sendPwdResetEmail.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					showMessage("Loading...");
					String email = emailTextbox.getValue();
					if (CommonUtil.isEmpty(email)) {
						showError("Please provide User Email Address");
						return;
					}
					if (!UiUtil.isValidEmail(email)) {
						showError(AppConfig.MESSAGE_INVALID_EMAIL);
						return;
					}
					Application.instance.secureService.sendPasswordResetEmail(
							userId, 
							email, 
							asyncCallback);
				}
			});
		} else {
			row++;
			column = 0;
			automaticResetPanel.getFlexCellFormatter().setColSpan(row, column, 2);
			automaticResetPanel.setWidget(row,column++, 
					new HTML("An email message will be sent to <b>"+userEmail+"</b> with the resetted password"));
			
			row++;
			column = 0;
			automaticResetPanel.getFlexCellFormatter().setColSpan(row, column, 2);
			automaticResetPanel.getFlexCellFormatter().setAlignment(row, column, HorizontalPanel.ALIGN_CENTER, VerticalPanel.ALIGN_MIDDLE);
			sendPwdResetEmail = new Button("Send Password Reset Email");
			automaticResetPanel.setWidget(row,column++,sendPwdResetEmail);
			sendPwdResetEmail.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					showMessage("");
					Application.instance.secureService.sendPasswordResetEmail(
							userId, 
							userEmail, 
							asyncCallback);
				}
			});
		}
	}

	private void buildManualResetPasswordPanel(FlexTable manualResetPanel, final AsyncCallback<CrudResponse> asyncCallback) {
		int row = 0;
		int column = 0;
		manualResetPanel.setSize("100%", "100%");
		
		Label title = new Label("Set the new password below");
		title.setStyleName("section-title-panel");
		manualResetPanel.getFlexCellFormatter().setColSpan(row, column, 2);
		manualResetPanel.setWidget(row,column,title);
		
		row++;
		column = 0;
		final PasswordTextBox passwordBox = new PasswordTextBox();
		manualResetPanel.setWidget(row,column++, new Label("New Password"));
		manualResetPanel.setWidget(row, column++, passwordBox);
		
		row++;
		column = 0;
		final PasswordTextBox confPasswordBox = new PasswordTextBox();
		manualResetPanel.setWidget(row,column++, new Label("Confirm New Password"));
		manualResetPanel.setWidget(row, column++, confPasswordBox);
		
		row++;
		column = 0;
		resetPassword = new Button("Reset Password");
		manualResetPanel.getFlexCellFormatter().setColSpan(row, column, 2);
		manualResetPanel.getFlexCellFormatter().setAlignment(row, column, HorizontalPanel.ALIGN_CENTER, VerticalPanel.ALIGN_MIDDLE);
		manualResetPanel.setWidget(row, column, resetPassword);
		resetPassword.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				showMessage("");
				String p1 = passwordBox.getValue();
				String p2 = confPasswordBox.getValue();
				if (CommonUtil.isEmpty(p1) || CommonUtil.isEmpty(p2)) {
					showError("Please provide password and confirmation password");
					return;
				}
				
				if (!p1.equals(p2)) {
					showError("New Password and Confirm New Password values are mismatching, please provide same values for the both fields");
					passwordBox.setValue("");
					confPasswordBox.setValue("");
					return;
				}
				Application.instance.secureService.resetUserPassword(
						userId,p1,asyncCallback);
			}
		});
	}

	private void buildUi() {
		flexTable = new FlexTable();
		contentPanel.add(flexTable);
		flexTable.setWidth("100%");
		userDetailsPanel = new VerticalPanel();
		passwordResetPanel = new VerticalPanel();
		int row = 0, column = 0;
		flexTable.setWidget(row, column, userDetailsPanel);
		row++;
		flexTable.setWidget(row, column, passwordResetPanel);
		
		actionBarWidget = new ActionBarWidget();
		contentPanel.add(actionBarWidget);
	}
	
	public void addActionButton(Widget widget) {
		actionBarWidget.add(widget);
	}
}
