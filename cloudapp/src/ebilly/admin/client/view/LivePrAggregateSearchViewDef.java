package ebilly.admin.client.view;

import ebilly.admin.shared.AppConfig;


public class LivePrAggregateSearchViewDef extends PrAggregateSearchViewDef {
	public LivePrAggregateSearchViewDef() {
		super();
	}
	
	@Override
	protected void initSearchFilterDef(SearchFilterDefinition searchFilterDef) {
		searchFilterDef.add(new ViewField("deviceName",java.lang.String.class.getName(),"Device Name"));
	}

	@Override
	public String getSearchObjectName() {
		return AppConfig.OBJ_PR_AGGREGATE_LIVE_REPORT;
	}
}