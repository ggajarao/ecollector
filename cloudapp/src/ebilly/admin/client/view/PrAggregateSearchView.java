package ebilly.admin.client.view;

import ebilly.admin.client.AppScreen;

public class PrAggregateSearchView extends SearchView implements AppScreen {

	public PrAggregateSearchView(String caption) {
		super(caption,true);
	}
	
	@Override
	protected SearchViewDef getSearchViewDefinition() {
		return new PrAggregateSearchViewDef();
	}
}
