package ebilly.admin.client.view;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;

import ebilly.admin.shared.viewdef.ResultRecord;

public class HtmlCellRenderer implements CellRenderer {
	private String fieldName;
	private SearchResultDefinition searchResultDef;
	public HtmlCellRenderer(String fieldName,
			SearchResultDefinition searchResultDef) {
		this.fieldName = fieldName;
		this.searchResultDef = searchResultDef;
	}
	
	@Override
	public Cell renderIntoCell(FlexTable resultGrid, int row,
			ResultRecord record, CrudViewDefinition parentCrudViewDef,
			CrudViewDefinition sectionCrudViewDef) {
		Cell c = new Cell();
		Label l = new Label();
		l.getElement().setInnerHTML(searchResultDef.getValue(this.fieldName, record));
		c.addWidget(l);
		return c;
	}
}