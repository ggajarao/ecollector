package ebilly.admin.client.view;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Widget;

public class Cell extends Composite {
	HorizontalPanel panel;
	public Cell() {
		initUi();
		initWidget(panel);
	}
	
	private void initUi() {
		this.panel = new HorizontalPanel();
		this.panel.setWidth("100%");
		this.panel.setSpacing(5);
	}
	
	public void addWidget(Widget widget) {
		this.panel.add(widget);		
	}
	
	public void clear() {
		this.panel.clear();
	}

	public void remove(Widget widget) {
		this.panel.remove(widget);
	}
}