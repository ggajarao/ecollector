package ebilly.admin.client.view;

import java.util.Date;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.datepicker.client.DateBox.DefaultFormat;

import ebilly.admin.shared.CommonUtil;

public class DateOrDateRangeSelectionPanel extends Composite {
	
	private FlexTable panel;
	private String caption = "Choose";
	private boolean isEnabled = true;
	DateBox dateBox;
	DateBox fromDate;
	DateBox toDate;
	private String selectionMode = "d"; // d - date, r - range 
	private String fieldName;
	private RadioButton rangeOption;
	private RadioButton yearOption;
	
	public DateOrDateRangeSelectionPanel(String caption, String fieldName) {
		if (!CommonUtil.isEmpty(caption)) {
			this.caption = caption;
		}
		this.fieldName = fieldName;
		buildUi();
		initWidget(panel);
	}
	
	private void buildUi() {
		int rowCounter = 0;
		int colCounter = 0;
		
		panel = new FlexTable();
		panel.setWidth("100%");
		
		Label captionLabel = new Label(this.caption);
		captionLabel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LEFT);
		panel.setWidget(rowCounter, colCounter, captionLabel);
		panel.getFlexCellFormatter().setColSpan(rowCounter, colCounter, 5);
		rowCounter++;
		colCounter = 0;
		
		// Add Date radio and DateBox
		yearOption = new RadioButton("g","Date");
		yearOption.setValue(true);
		yearOption.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				dateSelected();
			}
		});
		panel.setWidget(rowCounter, colCounter, yearOption); 
		colCounter++;
		
		dateBox = new DateBox();
		dateBox.setFormat(
			new DefaultFormat(
				DateTimeFormat.getFormat(
						CommonUtil.DEFAULT_DATE_FORMAT)));
		dateBox.setValue(new Date());
		panel.setWidget(rowCounter, colCounter, dateBox);
		panel.getFlexCellFormatter().setColSpan(rowCounter, colCounter, 3);
		colCounter++;
		
		rowCounter++;
		colCounter = 0;
		
		// Add Date between radio and two Date boxes
		rangeOption = new RadioButton("g","Date between ");
		rangeOption.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				rangeSelected();
			}
		});
		panel.setWidget(rowCounter, colCounter, rangeOption);
		colCounter++;
		
		fromDate = new DateBox();
		fromDate.setFormat(
			new DefaultFormat(
				DateTimeFormat.getFormat(
						CommonUtil.DEFAULT_DATE_FORMAT)));
		fromDate.setValue(new Date());
		panel.setWidget(rowCounter, colCounter, fromDate);
		colCounter++;
		
		Label andLabel = new Label("and");
		panel.setWidget(rowCounter, colCounter, andLabel);
		colCounter++;
		
		toDate = new DateBox();
		toDate.setFormat(
			new DefaultFormat(
				DateTimeFormat.getFormat(
						CommonUtil.DEFAULT_DATE_FORMAT)));
		toDate.setValue(new Date());
		panel.setWidget(rowCounter, colCounter, toDate);
		colCounter++;
		updateState();
	}
	
	private void updateState() {
		if (this.isRangeSelection()) {
			yearOption.setValue(false);
			rangeOption.setValue(true);
			rangeSelected();
		} else {
			yearOption.setValue(true);
			rangeOption.setValue(false);
			dateSelected();
		}
	}
	
	private void dateSelected() {
		this.dateBox.setEnabled(true);
		this.fromDate.setEnabled(false);
		this.toDate.setEnabled(false);
		this.selectionMode = "d";
	}
	
	private void rangeSelected() {
		this.dateBox.setEnabled(false);
		this.fromDate.setEnabled(true);
		this.toDate.setEnabled(true);
		this.selectionMode = "r";
	}

	public void reset() {
		dateSelected();
		updateState();
		dateBox.setValue(new Date());
		this.fromDate.setValue(new Date());
		this.toDate.setValue(new Date());
	}
	
	public boolean isRangeSelection() {
		return this.selectionMode.equals("r");
	}
	
	public Date getDate() {
		return this.dateBox.getValue();
	}
	
	public String getDateAsString(String format) {
		Date d = this.getDate();
		if (d != null) {
			return DateTimeFormat.getFormat(format).format(d);
		} else {
			return null;
		}
	}
	
	public Date getFromDate() {
		return this.fromDate.getValue();
	}
	
	public String getFromDateAsString(String format) {
		Date d = this.getFromDate();
		if (d != null) {
			return DateTimeFormat.getFormat(format).format(d);
		} else {
			return null;
		}
	}
	
	public Date getToDate() {
		Date d = this.toDate.getValue();
		long l = d.getTime() + (24*60*60*1000);
		return new Date(l);
	}
	
	public String getToDateAsString(String format) {
		Date d = this.getToDate();
		if (d != null) {
			return DateTimeFormat.getFormat(format).format(d);
		} else {
			return null;
		}
	}
	
	public void setValue(String value) {
		
	}
	
	public String getFieldName() {
		return this.fieldName;
	}
	
	public void setEnabled(boolean enabled) {
		this.isEnabled = enabled;
	}
}
