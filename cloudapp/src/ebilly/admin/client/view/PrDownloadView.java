package ebilly.admin.client.view;

import ebilly.admin.shared.AppConfig;


public class PrDownloadView extends BasePrDownloadView {
	private SearchView mSearchView = null;
	public PrDownloadView() {
		super("Recent PR Downloads");
	}

	@Override
	protected String getObjectName() {
		return AppConfig.OBJ_PR_REPORT;
	}

	@Override
	protected SearchView getExportRequestSearchView() {
		if (mSearchView != null) return mSearchView; 
		mSearchView = new SearchView(""){
			@Override
			protected SearchViewDef getSearchViewDefinition() {
				return new PrReportSearchViewDef();
			}
		};
		return mSearchView;
	}
}