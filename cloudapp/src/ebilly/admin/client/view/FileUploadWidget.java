package ebilly.admin.client.view;

import com.google.gwt.dom.client.Document;
import com.google.gwt.user.client.ui.Label;

import ebilly.admin.client.Application;
import ebilly.admin.client.menu.ActionListener;

public class FileUploadWidget extends Label {
	private String mElementId;
	private boolean isEnabled;
	private boolean isLoaded;
	private String mFileUploadUrl;
	public FileUploadWidget(String name) {
		super(Document.get().createDivElement());
		mElementId = (name==null)?"fileUploadWidget":name;
		getElement().setId(mElementId);
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		initApplet();
	}
	
	private native void initApplet() /*-{
		//alert(this.@ebilly.admin.client.view.FileUploadWidget::mElementId);
		var fuwThis = this;
		$wnd.dtjava.embed(
                {         id: "jfileupload",
                         url: "applet/jfileupload.jnlp",
                       width: 100,
                      height: 35,
                 placeholder: this.@ebilly.admin.client.view.FileUploadWidget::mElementId
                },
                {},
                {}
        );
        // This function definition is needed
        // for jfileupload applet, when applet is
        // loaded this function will get called.
        $wnd.jfileuploadLoaded = function() {
        	// set js callback function in applet.
        	$wnd.jfileupload.setJsNotifyListener("fileUploadedToServer");
        	fuwThis.@ebilly.admin.client.view.FileUploadWidget::onAppletLoaded()();
		}
		
		$wnd.fileUploadedToServer = function() {
			fuwThis.@ebilly.admin.client.view.FileUploadWidget::fireFileUploadedListener(Ljava/lang/String;)($wnd.jfileuploadResponse);
		}
		
		// Applet callback when a file is selected 
		// in applet.
		$wnd.jfileuploadFileSelected = function() {
			//alert($wnd.jfileuploadSelectedFile);
			fuwThis.@ebilly.admin.client.view.FileUploadWidget::fireFileSelectedEvent(Ljava/lang/String;)($wnd.jfileuploadSelectedFile);
		}
		
		// Applet callback when applets has some
		// message to notify.
		$wnd.jfileuploadRecieveMessages = function() {
			fuwThis.@ebilly.admin.client.view.FileUploadWidget::notifyMessage(Ljava/lang/String;)($wnd.jfileuploadMessage);
			//alert($wnd.jfileuploadMessage);
		}
	}-*/;
	
	public void setFileUploadUrl(String url) {
		mFileUploadUrl = url;
		if (isLoaded) {
			appletSetFileUploadUrl(mFileUploadUrl);
		}
	};
	
	private native void appletSetFileUploadUrl(String url) /*-{
		$wnd.jfileupload.setUploadUrl(url);
	}-*/; 
	
	public void setEnabled(boolean flag) {
		isEnabled = flag;
		if (isLoaded) {
			appletSetEnabled(isEnabled);
		}
	}
	private native void appletSetEnabled(boolean flag)/*-{
		$wnd.jfileupload.setSelectFileEnabled(flag);
	}-*/;
	
	void onAppletLoaded() {
		isLoaded = true;
		appletSetEnabled(isEnabled);
		appletSetFileUploadUrl(mFileUploadUrl);
	}
	
	private ActionListener mFileSelectionListener;
	private void fireFileSelectedEvent(String filePath) {
		if (mFileSelectionListener != null) {
			mFileSelectionListener.onAfterAction(filePath);
		}
	}
	
	public void setFileSelectionListener(ActionListener listener) {
		mFileSelectionListener = listener;
	}
	
	private ActionListener mFileUploadedListener;
	private void fireFileUploadedListener(String serverResponse) {
		if (mFileUploadedListener != null) {
			mFileUploadedListener.onAfterAction(serverResponse);
		}
	}
	
	public void setFileUploadedListener(ActionListener listener) {
		mFileUploadedListener = listener;
	}
	
	public void notifyMessage(String message) {
		Application.showMessage(message);
	}	
}
