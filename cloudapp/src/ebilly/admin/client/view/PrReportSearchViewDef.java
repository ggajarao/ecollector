package ebilly.admin.client.view;

import ebilly.admin.shared.AppConfig;

public class PrReportSearchViewDef extends ExportRequestSearchViewDef {
	public PrReportSearchViewDef() {
		super();
	}
	
	protected String getObjectName() {
		return AppConfig.OBJ_PR_REPORT;
	}
}
