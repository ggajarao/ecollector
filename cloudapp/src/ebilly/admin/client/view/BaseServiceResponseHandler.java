package ebilly.admin.client.view;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.StatusCodeException;

import ebilly.admin.client.Application;

public abstract class BaseServiceResponseHandler<T> implements AsyncCallback<T> {
	@Override
	public void onFailure(Throwable caught) {
		if (caught instanceof com.google.gwt.user.client.rpc.StatusCodeException) {
			StatusCodeException sce = (com.google.gwt.user.client.rpc.StatusCodeException)caught;
			if (401 == sce.getStatusCode()) {
				Application.instance.showExpiredSessionUi();
				return;
			}
		}
		Application.showErrorMessage("Error: "+
					caught.getMessage());
		caught.printStackTrace();			
	}

	public abstract void onSuccess(T result);
}
