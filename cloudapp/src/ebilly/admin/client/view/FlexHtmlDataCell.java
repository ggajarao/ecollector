package ebilly.admin.client.view;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasKeyDownHandlers;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.FocusWidget;
import com.google.gwt.user.client.ui.Widget;

import ebilly.admin.client.Application;

public class FlexHtmlDataCell extends FlexHtmlCell implements HasKeyDownHandlers {
	public FlexHtmlDataCell() {
		this.addStyleName("data-cell");
	}
	
	private boolean editableFlag = false;
	public void setEditable(boolean flag) {
		editableFlag = flag;
		updateEditableStyle();
	}
	
	private void updateEditableStyle() {
		if (this.editableFlag) {
			this.addStyleName("data-cell");
			this.addStyleName("data-cell-editable");
			this.removeStyleName("data-cell-on-edit");
		} else {
			this.removeStyleName("data-cell-editable");
		}
	}
	
	public void acceptInput(final String fieldName, final CrudViewDefinition deviceViewDef) {
		super.clickHandler.removeHandler();
		this.clear();
		this.setStyleName("data-cell-on-edit");
		
		final String oldValue = this.getValue();
		final Widget inputWidget = 
			deviceViewDef.createInputWidget(fieldName);
		inputWidget.setWidth("100pt");
		if ( inputWidget instanceof FocusWidget ) {
			Scheduler.get().scheduleDeferred(new ScheduledCommand() {
				@Override
				public void execute() {
					((FocusWidget)inputWidget).setFocus(true);
				}
			});
			
		}
		Widget w = UiUtil.createEditPanel(inputWidget,
				new ClickHandler(){ // Save Handler
					public void onClick(ClickEvent event) {
						saveDataCell(deviceViewDef,fieldName,inputWidget);
						event.stopPropagation();
						updateEditableStyle();
						FlexHtmlDataCell.this.reRegisterClickHandler();
					}}, 
				new ClickHandler() { // Cancel Handler
					public void onClick(ClickEvent event) {
						FlexHtmlDataCell.this.setValue(oldValue);
						event.stopPropagation();
						updateEditableStyle();
						FlexHtmlDataCell.this.reRegisterClickHandler();
					}
				}
		);
		this.addKeyDownHandler(new KeyDownHandler() {
			public void onKeyDown(KeyDownEvent event) {
				System.out.println(event.getNativeKeyCode());
				if (event.getNativeKeyCode() == 13 /*return key*/) {
					saveDataCell(deviceViewDef,fieldName,inputWidget);
					event.stopPropagation();
					updateEditableStyle();
					FlexHtmlDataCell.this.reRegisterClickHandler();
				} else if (event.getNativeKeyCode() == 27 /*esc key*/) {
					FlexHtmlDataCell.this.setValue(oldValue);
					event.stopPropagation();
					updateEditableStyle();
					FlexHtmlDataCell.this.reRegisterClickHandler();
				}
			}			
		});
		this.add(w);
	}
	
	private void saveDataCell(CrudViewDefinition crudViewDef, String fieldName, 
			Widget inputWidget) {
		Application.showMessage("");
		ErrorMessage errorMsg = new ErrorMessage();
		String fieldValue = 
			crudViewDef.validateAndGetFieldValue(fieldName, 
					inputWidget, errorMsg);
		if (errorMsg.hasErrors()) {
			Application.showErrorMessage(errorMsg.getMessage());
			return;
		}
		
		crudViewDef.hostUpdate(fieldName,fieldValue);
		FlexHtmlDataCell.this.setValue(fieldValue);
	}

	@Override
	public HandlerRegistration addKeyDownHandler(KeyDownHandler handler) {
		return this.addDomHandler(handler, KeyDownEvent.getType());
	}
}