package ebilly.admin.client.view;

import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;

import ebilly.admin.shared.AppConfig.EXPORT_STATUS;
import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.viewdef.ResultRecord;

public class ExportResultCellRenderer implements CellRenderer {

	SearchResultDefinition searchResultDef;
	private String statusFieldName;
	private String resultFileKeyField;
	private String errorMessageField;
	private String summaryMessageField;
	public ExportResultCellRenderer(String statusFieldName,
			String resultFileKeyField, String errorMessageField,
			String summaryMessageField,
			SearchResultDefinition searchResultDef) {
		super();
		this.searchResultDef = searchResultDef;
		this.statusFieldName = statusFieldName;
		this.resultFileKeyField = resultFileKeyField;
		this.errorMessageField = errorMessageField;
		this.summaryMessageField = summaryMessageField;
	}

	@Override
	public Cell renderIntoCell(FlexTable resultGrid, int row,
			ResultRecord record, CrudViewDefinition parentCrudViewDef,
			CrudViewDefinition sectionCrudViewDef) {
		Cell c = new Cell();
		String displayStatus = searchResultDef.getValue(statusFieldName, record);
		Widget w = null;
		if (displayStatus == null) {
			w = new Label("");
		} else if (displayStatus.equalsIgnoreCase(
				EXPORT_STATUS.COMPLETED.toString()) ||
				displayStatus.equalsIgnoreCase(
						EXPORT_STATUS.FAILED.toString())) {
			w = new ActionBarWidget();
			((ActionBarWidget)w).setCellPadding(2);

			// Error message popup panel
			final String errorMessage = 
				searchResultDef.getValue(this.errorMessageField,record);
			if (! CommonUtil.isEmpty(errorMessage)) {
				final Anchor msgAnchor = new Anchor("Error");
				final PopupPanel pp = new PopupPanel(/*auto hide*/true);
				pp.setWidget(new Label(errorMessage));
				msgAnchor.addMouseOverHandler(new MouseOverHandler(){
					public void onMouseOver(MouseOverEvent event) {
						pp.showRelativeTo(msgAnchor);
					}
				});
				msgAnchor.addMouseOutHandler(new MouseOutHandler(){
					public void onMouseOut(MouseOutEvent event) {
						pp.hide();
					}
				});
				((ActionBarWidget)w).add(msgAnchor);
			} else {
				/*// show summary in a popup panel
				final String importSummary = 
					searchResultDef.getValue(this.summaryMessageField,record);
				if (!CommonUtil.isEmpty(importSummary)) {
					final Anchor msgAnchor = new Anchor("Summary");
					final PopupPanel pp = new PopupPanel(auto hidetrue);
					
					HorizontalPanel hp = new HorizontalPanel();
					hp.add(new DataLabel(importSummary));
					hp.setSpacing(10);
					pp.setWidget(hp);
					pp.setAnimationEnabled(true);
					msgAnchor.addMouseOverHandler(new MouseOverHandler(){
						public void onMouseOver(MouseOverEvent event) {
							pp.showRelativeTo(msgAnchor);
						}});
					msgAnchor.addMouseOutHandler(new MouseOutHandler(){
						public void onMouseOut(MouseOutEvent event) {
							pp.hide();
						}
					});
					((ActionBarWidget)w).add(msgAnchor);
				}*/
			}
			// Result file download link
			String resultFile = 
				searchResultDef.getValue(this.resultFileKeyField,record);
			if (! CommonUtil.isEmpty(resultFile)) {
				Anchor resultFileAnchor = new Anchor("Download");
				resultFileAnchor.setHref(CommonUtil.getDownloadUrl(resultFile));
				resultFileAnchor.setTarget("-");
				((ActionBarWidget)w).add(resultFileAnchor);
			}
		} else if (displayStatus.equalsIgnoreCase(
				EXPORT_STATUS.INPROGRESS.toString())) {
			w = new Label("");
			w.setHeight("10px"); w.setWidth("150px");
			w.setStyleName("progress-bar");
		} else {
			w = new Label(displayStatus);
		}

		c.addWidget(w);
		return c;
	}
}
