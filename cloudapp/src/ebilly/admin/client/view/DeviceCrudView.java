package ebilly.admin.client.view;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Widget;

import ebilly.admin.client.Application;
import ebilly.admin.client.DataUploadPanel;
import ebilly.admin.client.NewDataUploadPanel;
import ebilly.admin.client.menu.ActionListener;
import ebilly.admin.client.menu.DeviceDashboardViewAction;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.viewdef.CrudResponse;
import ebilly.admin.shared.viewdef.ResultRecord;

public class DeviceCrudView extends CrudView {
	
	public DeviceCrudView(String crudCaption) {
		super(crudCaption);
	}
	
	private String mDeviceId;
	public DeviceCrudView(int crudAction, String crudCaption, String deviceId) {
		super(crudAction, crudCaption, deviceId);
		mDeviceId = deviceId;
	}

	@Override
	protected void initViewFieldDefinition(CrudViewDefinition viewDef) {
		initViewDefinition(viewDef);
		
		ActionBarWidget actionBar = new ActionBarWidget();
		Anchor addFile = null;
		/*addFile = new Anchor("Upload File");
		addFile.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				showDeviceFileUploadPanel();
			}
		});
		actionBar.add(addFile);*/
		
		addFile = new Anchor("Upload File");
		addFile.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				showDeviceFileUploadPanelAdvanced();
			}
		});
		actionBar.add(addFile);
		ViewSectionDefinition viewSectionDef = viewDef.getViewSectionDefinition(AppConfig.SECTION_DEVICE_FILES);
		viewSectionDef.setActionBar(actionBar);
	}

	@Override
	protected String getCrudObjectName() {
		return AppConfig.CRUD_DEVICE;
	}
	
	public static void initViewDefinition(CrudViewDefinition viewDef) {
		ViewField vf = new ViewField("id",java.lang.String.class.getName(),"Id");
		vf.isMandatory = false;
		vf.isHidden = true;
		viewDef.addViewField(vf);
		
		vf = new ViewField("deviceServicesFileKey",java.lang.String.class.getName(),"deviceServicesFileKey");
		vf.isMandatory = false;
		vf.isHidden = true;
		viewDef.addViewField(vf);

		vf = new ViewField("deviceName",java.lang.String.class.getName(),"Device Name");
		vf.setMaxLenght(10);
		vf.setMandatory(true);
		vf.setReadOnlyOnEdit(true);
		vf.helpText = "Set this value as Device name";
		viewDef.addViewField(vf);
		
		vf = new ViewField("pairingStatus",java.lang.String.class.getName(),"Pairing Status");
		vf.setReadOnlyOnEdit(true);
		vf.isReadonly = true;
		vf.helpText = "Shows Device pairing status";
		viewDef.addViewField(vf);
		
		vf = new ViewField("collectionExpirationDate",java.util.Date.class.getName(),"Collection Expires on");
		vf.helpText = "Set this date value to automatically lock this Device from generating PRs";
		viewDef.addViewField(vf);
		
		vf = new ViewField("collectionLimit",java.lang.String.class.getName(),"Collection Limit (Rs)");
		vf.helpText = "Set this amount value to automatically lock this Device from generating PRs once the collected amount exceeds this value";
		viewDef.addViewField(vf);
		
		SearchViewDef searchViewDef = new SearchViewDef() {

			@Override
			protected void initSearchFilterDef(SearchFilterDefinition searchFilterDef) {
				
			}
			@Override
			protected void initSearchResultDef(
					SearchResultDefinition searchResultDef) {
				ResultColumn rc = new ResultColumn("id",java.lang.String.class.getName(),"id");
				rc.isHidden = true;
				searchResultDef.add(rc);
				
				rc = new ResultColumn("deviceServicesFileName",java.lang.String.class.getName(),"File Name");
				searchResultDef.add(rc);
				
				rc = new ResultColumn("deviceFileStagingDate",java.lang.String.class.getName(),"Uploaded On");
				searchResultDef.add(rc);
				
				rc = new ResultColumn("remoteDeviceFileUploadState",java.lang.String.class.getName(),"State");
				rc.setCellRenderer(new DeviceFileUploadStateRenderer("remoteDeviceFileUploadState",searchResultDef));
				searchResultDef.add(rc);
				
				rc = new ResultColumn("remoteDeviceImportMethod",java.lang.String.class.getName(),"Import Method");
				//rc.setCellRenderer(new DeviceFileUploadStateRenderer("remoteDeviceFileUploadState",searchResultDef));
				searchResultDef.add(rc);
				
				rc = new ResultColumn("action",java.lang.String.class.getName(),"Action");
				rc.setCellRenderer(new DeviceFileActionsRenderer("remoteDeviceFileUploadState",searchResultDef));
				searchResultDef.add(rc);
			}
			@Override
			public String getSearchObjectName() {
				return "";
			}
		};
		searchViewDef.hideFilter = true;
		ViewSectionDefinition viewSectionDef = 
			new ViewSectionDefinition(AppConfig.SECTION_DEVICE_FILES,
				searchViewDef);
		
		viewDef.addViewSectionDefinition(viewSectionDef);
	}
	
	private void showDeviceFileUploadPanelAdvanced() {
		final DialogBox dialog = new DialogBox();
		dialog.setAnimationEnabled(true);
		dialog.setText("Upload Device Services File( Zips Automatically )");
		dialog.setGlassEnabled(true);
		dialog.setAutoHideEnabled(false);
		
		NewDataUploadPanel dataUploadPanel = 
			new NewDataUploadPanel(mDeviceId);
		dataUploadPanel.setCancelHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				dialog.hide();
			}
		});
		dataUploadPanel.setImportCompleteActionListener(new ActionListener() {
			public void onBeforeAction(Object data) {
			}
			public void onAfterAction(Object data) {
				dialog.hide();
				DeviceDashboardViewAction a = new DeviceDashboardViewAction(mDeviceId);
				a.perform();
			}
		});
		
		dialog.add(dataUploadPanel.getUiObject());
		dialog.center();
		dialog.show();
	}
	
	/**
	 * This is for regular uploads, which does not
	 * use applet, and thus does not zip the
	 * selected file before uploading.
	 * 
	 * Use of this is disabled, code is vaulted
	 * so that in emergencies we can default 
	 * to this method of upload.
	 */
	private void showDeviceFileUploadPanel() {
		final DialogBox dialog = new DialogBox();
		dialog.setAnimationEnabled(true);
		dialog.setText("Upload Device Services File");
		dialog.setGlassEnabled(true);
		dialog.setAutoHideEnabled(false);
		
		DataUploadPanel dataUploadPanel = 
			new DataUploadPanel(AppConfig.OBJ_DEVICE_SERVICE_FILE, mDeviceId);
		dataUploadPanel.setCancelHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				dialog.hide();
			}
		});
		dataUploadPanel.setImportCompleteActionListener(new ActionListener() {
			public void onBeforeAction(Object data) {
			}
			public void onAfterAction(Object data) {
				dialog.hide();
				DeviceDashboardViewAction a = new DeviceDashboardViewAction(mDeviceId);
				a.perform();
			}
		});
		
		dialog.add(dataUploadPanel.getUiObject());
		dialog.center();
		dialog.show();
	}
}

class DeviceFileUploadStateRenderer implements CellRenderer {
	SearchResultDefinition searchResultDef;
	private String statusField;
	public DeviceFileUploadStateRenderer(String statusField, 
			SearchResultDefinition searchResultDef) {
		super();
		this.searchResultDef = searchResultDef;
		this.statusField = statusField;
	}

	@Override
	public Cell renderIntoCell(FlexTable resultGrid, int row,
			ResultRecord record, CrudViewDefinition parentCrudViewDef,
			CrudViewDefinition sectionCrudViewDef) {
		String s = this.searchResultDef.getValue(this.statusField, record);
		String statusReadable = "N/A";
		if (AppConfig.REMOTE_DEVICE_UPLOAD_STATES.READY_TO_UPLOAD_TO_REMOTE_DEVICE.toString()
			.equalsIgnoreCase(s)) {
			statusReadable = "Waiting";
		} else if (AppConfig.REMOTE_DEVICE_UPLOAD_STATES.UPLOADED_TO_REMOTE_DEVICE.toString()
				.equalsIgnoreCase(s)) {
			statusReadable = "In Device";
		}
		Cell c = new Cell();
		c.addWidget(new DataLabel(statusReadable));
		return c;
	}	
}

class DeviceFileActionsRenderer implements CellRenderer {
	private static int rowOffsetWhileRemovingRow = 0;
	SearchResultDefinition searchResultDef;
	private String statusField;
	public DeviceFileActionsRenderer(String statusField, 
			SearchResultDefinition searchResultDef) {
		super();
		this.searchResultDef = searchResultDef;
		this.statusField = statusField;
		rowOffsetWhileRemovingRow = 0;
	}

	@Override
	public Cell renderIntoCell(final FlexTable resultGrid, final int row,
			ResultRecord record, CrudViewDefinition parentCrudViewDef,
			CrudViewDefinition sectionCrudViewDef) {
		String s = this.searchResultDef.getValue(this.statusField, record);
		final String deviceFileId = this.searchResultDef.getValue("id", record);
		final Anchor a = new Anchor("Delete");
		a.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Application.showMessage("Removing Device File...");
				Application.instance.secureService.removeDeviceFiles(new String[]{deviceFileId},
						new BaseServiceResponseHandler<CrudResponse>() {
							public void onSuccess(CrudResponse result) {
								if (result.isSuccess()) {
									Application.showMessage(result.successMessage);
									//int rowCount = resultGrid.getRowCount();
									resultGrid.removeRow(row-rowOffsetWhileRemovingRow);
									rowOffsetWhileRemovingRow++;
								} else {
									Application.showErrorMessage(result.errorMessage);
								}
							}
							/*public void onFailure(Throwable caught) {
								Application.showErrorMessage("Fatal error: "+caught.getMessage());
							}*/
						}
				);
			}
		});
		Cell c = new Cell();
		c.addWidget(a);
		return c;
	}	
}

