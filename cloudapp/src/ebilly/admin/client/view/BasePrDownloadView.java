package ebilly.admin.client.view;

import java.util.ArrayList;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import ebilly.admin.client.Application;
import ebilly.admin.client.menu.ActionListener;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.viewdef.CrudResponse;
import ebilly.admin.shared.viewdef.ResultField;
import ebilly.admin.shared.viewdef.SearchRequest;

public abstract class BasePrDownloadView extends AppViewPanel {
	private boolean isPollingCancelled = false; 
	private int numberOfPolls = 0;
	private int maxPolls = 60;
	private SearchView mExportRequestSearchView;
	private PrSearchFilterPanel mPrSearchFilterPanel;
	public BasePrDownloadView(String viewName) {
		super(viewName);
		buildUi();
		
		Scheduler.get().scheduleDeferred(new ScheduledCommand() {
			@Override
			public void execute() {
				mExportRequestSearchView.search();
			}
		});
		
		if (!isPollingCancelled) {
			Scheduler.get().scheduleFixedDelay(
					new RepeatingCommand() {
						public boolean execute() {
							if (isPollingCancelled) return false;
							numberOfPolls++;
							mExportRequestSearchView.search();
							return numberOfPolls  < maxPolls;
						}
					}, 15*1000);
		}
	}
	
	private void buildUi() {
		Panel main = super.contentPanel;
		
		mExportRequestSearchView = getExportRequestSearchView();
		//mExportRequestSearchView.setContentPanelBorderSpace(10);
		main.add(mExportRequestSearchView.getUiObject());
		
		super.setContentPanelBorderSpace(0);
		
		mPrSearchFilterPanel = new PrSearchFilterPanel(getObjectName());
		Button downloadAction = new Button("New Download");
		super.actionBarWidget.add(downloadAction);
		downloadAction.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				mPrSearchFilterPanel.showDialog();
			}
		});
		mPrSearchFilterPanel.setOnFilterSelectedAction(new ActionListener() {
			public void onBeforeAction(Object data) {
			}
			
			public void onAfterAction(Object data) {
				createExportRequest((SearchRequest)data);
			}			
		});
	}
	
	protected abstract SearchView getExportRequestSearchView();

	protected abstract String getObjectName();

	private void createExportRequest(SearchRequest searchRequest) {
		Application.showMessage("Please Wait...");
		Application.instance.secureService.createExportRequest(searchRequest, 
				new BaseServiceResponseHandler<CrudResponse>() {
					public void onSuccess(CrudResponse result) {
						if (result.isSuccess()) {
							Application.showMessage("Preparing to Download...");
							mPrSearchFilterPanel.hideDialog();
							mExportRequestSearchView.search();
						} else {
							Application.showErrorMessage(result.errorMessage);
						}
					}
					
					/*public void onFailure(Throwable caught) {
						Application.showErrorMessage(caught.getMessage());
					}*/
				});
	}
	
	@Override
	public void onBeforeClose() {
		isPollingCancelled = true;
	}
}

class PrSearchFilterPanel extends AppViewPanel {
	private VerticalPanel mContentPanel;
	private DialogBox prSearchFilgerDialog;
	private TextBox mDeviceNameTb;
	private DateOrDateRangeSelectionPanel mDodr;
	private String mObjectName;
	public PrSearchFilterPanel(String objectName) {
		super("");
		super.setContentPanelBorderSpace(0);
		mObjectName = objectName;
		buildUi();
	}
	
	private ActionListener onFilterSelectedAction = null;
	public void setOnFilterSelectedAction(ActionListener onPrepareDownloadAction) {
		this.onFilterSelectedAction = onPrepareDownloadAction;
	}
	
	private void onPrepareDownload() {
		if (this.onFilterSelectedAction != null) {
			SearchRequest request = new SearchRequest(mObjectName, new ArrayList<ResultField>());
			String deviceName = mDeviceNameTb.getText().trim();
			if (deviceName.length() != 0) {
				request.addSearchCriteria("deviceName",deviceName.toUpperCase());
			}
			if (mDodr.isRangeSelection()) {
				request.addSearchCriteria(mDodr.getFieldName(),mDodr.getFromDateAsString(CommonUtil.DEFAULT_DATE_FORMAT),AppConfig.FILTER_CONJUNCTION_AND, AppConfig.FILTER_OPERATOR_GREATER_EQUALS);
				request.addSearchCriteria(mDodr.getFieldName(),mDodr.getToDateAsString(CommonUtil.DEFAULT_DATE_FORMAT),AppConfig.FILTER_CONJUNCTION_AND, AppConfig.FILTER_OPERATOR_LESS_EQUALS);					
			} else {
				request.addSearchCriteria(mDodr.getFieldName(),mDodr.getDateAsString(CommonUtil.DEFAULT_DATE_FORMAT),AppConfig.FILTER_CONJUNCTION_AND, AppConfig.FILTER_OPERATOR_GREATER_EQUALS);;
			}
			
			onFilterSelectedAction.onAfterAction(request);
		}
	}
	
	private void buildUi() {
		mContentPanel = new VerticalPanel();
		mContentPanel.setSpacing(10);
		super.contentPanel.add(mContentPanel);
		
		mDodr = 
			new DateOrDateRangeSelectionPanel("Collection between","_collectionDate");
		mContentPanel.add(mDodr);
		
		HorizontalPanel deviceNamePanel = new HorizontalPanel();
		deviceNamePanel.setVerticalAlignment(VerticalPanel.ALIGN_MIDDLE);
		Label deviceNameLabel = new Label("Device Name");
		mDeviceNameTb = new TextBox();
		mDeviceNameTb.setMaxLength(50);
		
		deviceNamePanel.add(deviceNameLabel);
		deviceNamePanel.add(mDeviceNameTb);
		mContentPanel.add(deviceNamePanel);
		
		ActionBarWidget actionBar = new ActionBarWidget();
		mContentPanel.add(actionBar);
		actionBar.setCellPadding(10);
		mContentPanel.setCellHorizontalAlignment(actionBar, HorizontalPanel.ALIGN_RIGHT);
		
		Button cancelBtn = new Button("Cancel");
		actionBar.add(cancelBtn);
		cancelBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				hideDialog();
			}
		});
		Button prepareDownloadButton = new Button("Prepare Download");
		actionBar.add(prepareDownloadButton);
		prepareDownloadButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				onPrepareDownload();
			}
		});
		
		prSearchFilgerDialog = super.prepareContentAsDialog("Select PRs matching...");
	}
	
	private void reset() {
		mDodr.reset();
		mDeviceNameTb.setText("");
	}
	
	public void showDialog() {
		// Reset field values
		reset();
		
		this.prSearchFilgerDialog.center();
		this.prSearchFilgerDialog.show();
	}
	
	public void hideDialog() {
		this.prSearchFilgerDialog.hide();
	}
}
