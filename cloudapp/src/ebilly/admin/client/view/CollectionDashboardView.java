package ebilly.admin.client.view;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

import ebilly.admin.client.Application;
import ebilly.admin.client.menu.Action;
import ebilly.admin.client.menu.ActionListener;
import ebilly.admin.client.menu.XMenu;
import ebilly.admin.client.menu.XMenuGroup;
import ebilly.admin.client.menu.XMenuItem;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.CollectionDashboardFilterSelection;
import ebilly.admin.shared.CollectionDashboardPiggyBack;
import ebilly.admin.shared.SummarySearchRequestFactory;
import ebilly.admin.shared.orgstruct.OrgCircleUi;
import ebilly.admin.shared.orgstruct.OrgDistributionUi;
import ebilly.admin.shared.orgstruct.OrgDivisionUi;
import ebilly.admin.shared.orgstruct.OrgEroUi;
import ebilly.admin.shared.orgstruct.OrgSectionUi;
import ebilly.admin.shared.orgstruct.OrgSubdivisionUi;
import ebilly.admin.shared.viewdef.CrudRequest;
import ebilly.admin.shared.viewdef.CrudResponse;
import ebilly.admin.shared.viewdef.ResultRecord;
import ebilly.admin.shared.viewdef.SearchRequest;
import ebilly.admin.shared.viewdef.SearchResult;

public class CollectionDashboardView extends AppViewPanel {

	private CollectionDashboardPiggyBack dashboardData = null;
	private CollectionDashboardDataSource dataSource = new CollectionDashboardDataSource();
	
	private CollectionDashboardFilterSelection filterSelection = new CollectionDashboardFilterSelection();
	private HTMLPanel filterPanel = null;
	private HTMLPanel filterMaskPanel = null;
	private HTMLPanel dimHeadPanel = null;
	private HTMLPanel dimSubHeadPanel = null;
	
	private SearchViewDef aggHeadSearchViewDef = new SearchViewDef() {
		@Override
		protected void initSearchResultDef(
				SearchResultDefinition searchResultDef) {
			ResultColumn rc = new ResultColumn("GRAND_TOTAL", java.lang.Double.class.getName(), "Total Amount");
			rc.setCellRenderer(new PrAmountCellRenderer("GRAND_TOTAL",searchResultDef));
			searchResultDef.add(rc);
			
			searchResultDef.add(new ResultColumn("TOTAL_PRS", java.lang.Long.class.getName(), "PRs"));
			
			rc = new ResultColumn("ARREARS_N_DEMAND", java.lang.Double.class.getName(), "CC+Arrears");
			rc.setCellRenderer(new PrAmountCellRenderer("ARREARS_N_DEMAND",searchResultDef));
			searchResultDef.add(rc);
			
			rc = new ResultColumn("CMD_COLLECTED", java.lang.Double.class.getName(), "CMD");
			rc.isHidden = true;
			rc.setCellRenderer(new PrAmountCellRenderer("CMD_COLLECTED",searchResultDef));
			searchResultDef.add(rc);
			
			rc = new ResultColumn("RC_COLLECTED", java.lang.Double.class.getName(), "RC"); 
			rc.setCellRenderer(new PrAmountCellRenderer("RC_COLLECTED",searchResultDef));
			searchResultDef.add(rc);
			
			rc = new ResultColumn("ACD_COLLECTED", java.lang.Double.class.getName(), "ACD"); 
			rc.setCellRenderer(new PrAmountCellRenderer("ACD_COLLECTED",searchResultDef));
			searchResultDef.add(rc);
			
			rc = new ResultColumn("AGL_AMOUNT", java.lang.Double.class.getName(), "AGL");
			rc.setCellRenderer(new PrAmountCellRenderer("AGL_AMOUNT",searchResultDef));
			searchResultDef.add(rc);
			
			rc = new ResultColumn("COLLECTION_DATE", java.lang.Double.class.getName(), "Arrears");
			rc.isHidden = true;
			searchResultDef.add(rc);
		}

		@Override
		protected void initSearchFilterDef(
				SearchFilterDefinition searchFilterDef) {
		}

		@Override
		public String getSearchObjectName() {
			return "";
		}
	};
	
	private SearchViewDef aggSubHeadSearchViewDef = new SearchViewDef() {
		@Override
		protected void initSearchResultDef(
				SearchResultDefinition searchResultDef) {
			
			ResultColumn rc = new ResultColumn("COLLECTION_DATE", java.lang.String.class.getName(), "Date");
			rc.setCellRenderer(new PrCollectionDateCellRenderer("COLLECTION_DATE",searchResultDef));
			searchResultDef.add(rc);
			
			rc = new ResultColumn("GRAND_TOTAL", java.lang.Double.class.getName(), "Total Amount");
			rc.setCellRenderer(new PrAmountCellRenderer("GRAND_TOTAL",searchResultDef));
			searchResultDef.add(rc);
			
			searchResultDef.add(new ResultColumn("TOTAL_PRS", java.lang.Long.class.getName(), "PRs"));
			
			rc = new ResultColumn("ARREARS_N_DEMAND", java.lang.Double.class.getName(), "CC+Arrears"); 
			rc.setCellRenderer(new PrAmountCellRenderer("ARREARS_N_DEMAND",searchResultDef));
			searchResultDef.add(rc);
			
			
			rc = new ResultColumn("RC_COLLECTED", java.lang.Double.class.getName(), "RC"); 
			rc.setCellRenderer(new PrAmountCellRenderer("RC_COLLECTED",searchResultDef));
			searchResultDef.add(rc);
			
			rc = new ResultColumn("ACD_COLLECTED", java.lang.Double.class.getName(), "ACD"); 
			rc.setCellRenderer(new PrAmountCellRenderer("ACD_COLLECTED",searchResultDef));
			searchResultDef.add(rc);
			
			rc = new ResultColumn("AGL_AMOUNT", java.lang.Double.class.getName(), "AGL");
			rc.setCellRenderer(new PrAmountCellRenderer("AGL_AMOUNT",searchResultDef));
			searchResultDef.add(rc);
			
			rc = new ResultColumn("CMD_COLLECTED", java.lang.Double.class.getName(), "Arrears");
			rc.isHidden = true;
			rc.setCellRenderer(new PrAmountCellRenderer("CMD_COLLECTED",searchResultDef));
			searchResultDef.add(rc);
		}

		@Override
		protected void initSearchFilterDef(
				SearchFilterDefinition searchFilterDef) {
		}

		@Override
		public String getSearchObjectName() {
			return "";
		}
	};

	public CollectionDashboardView() {
		Scheduler.get().scheduleDeferred(new ScheduledCommand() {
			@Override
			public void execute() {
				loadView();
			}
		});
	}
	
	private void loadView() {
		CrudRequest crudRequest = new CrudRequest();
		crudRequest.crudAction = AppConfig.CRUD_ACTION_RETRIEVE;
		crudRequest.crudObject = AppConfig.CRUD_COLLECTION_DASHBOARD_DATA;
		Application.instance.secureService.processCrud(crudRequest, 
				new BaseServiceResponseHandler<CrudResponse>(){
					@Override
					public void onSuccess(CrudResponse result) {
						dashboardData = (CollectionDashboardPiggyBack)result.crudEntity;
						initSelection();
						buildUi();
					}
		});
	}
	
	private void initSelection() {
		List<String> summaryDataKeys = dashboardData.getSummaryDataKeys();
		if (!summaryDataKeys.isEmpty()) {
			filterSelection.setSelectedSummaryDataKey(summaryDataKeys.get(0));
		}
	}
	
	private void buildUi() {
		super.panel.setStyleName("dashboardPanel");
		filterPanel = new HTMLPanel("");
		filterPanel.setStyleName("dashboardBlock");
		
		super.panel.add(filterPanel);
		
		dimHeadPanel = new HTMLPanel("");
		dimHeadPanel.setStyleName("dashboardBlock dashboardBlockSpacer summaryHead");
		super.panel.add(dimHeadPanel);		
		
		dimSubHeadPanel = new HTMLPanel("");
		dimSubHeadPanel.setStyleName("dashboardBlock dashboardBlockSpacer");
		super.panel.add(dimSubHeadPanel);
		
		updateFilterPanel();
		
		hideDimPanels();
		filterReady();
	}

	private void hideDimPanels() {
		dimHeadPanel.setVisible(false);
		dimSubHeadPanel.setVisible(false);
	}
	
	private void showDimPanels() {
		dimHeadPanel.setVisible(true);
		dimSubHeadPanel.setVisible(true);
	}
	
	private void filterBussy() {
		filterMaskPanel.setVisible(true);
	}
	
	private void filterReady() {
		filterMaskPanel.setVisible(false);
	}

	private void updateFilterPanel() {
		HTMLPanel container = this.filterPanel;
		container.clear();
		
		filterMaskPanel = new HTMLPanel("");
		filterMaskPanel.setStyleName("dashboard-filter-mask");
		container.add(filterMaskPanel);
		
		XMenu driverMenu = new XMenu();
		driverMenu.setStyleName("flatMenuDriverPanel");
		HTMLPanel subselectionBar = new HTMLPanel("");
		subselectionBar.setStyleName("flatMenuSubselectionPanel");
		
		container.add(driverMenu);
		container.add(subselectionBar);
		
		XMenuGroup menuGroup = new XMenuGroup(driverMenu,"");
		menuGroup.setStyleName("flatMenuGroup");
		driverMenu.add(menuGroup);
		
		ListBox listBox = buildMonthFilter();
		listBox.addStyleName("flatMenuItem");
		listBox.addStyleName("flatMenuItemListInput");
		listBox.addStyleName("flatMenuHeadItem");
		menuGroup.add(listBox);
		
		XMenuItem separator = new XMenuItem("",null);
		separator.setStyleName("flatMenuItemSeparator");
		separator.addStyleName("flatMenuHeadItem");
		menuGroup.add(separator);
		
		updateMonthSelection(listBox);
		
		// show in ui what org selection is made,
		// this can be driven by user selection in order
		// to the data entitlement rules.
		updateOrgSelection(menuGroup, subselectionBar);
	}
	
	private void updateOrgSelection(XMenuGroup menuGroup,
			HTMLPanel subselectionBar) {
		subselectionBar.clear();
		int orgType = this.filterSelection.getSelectedOrgType();
		String itemName = null;
		String orgValue = null;
		
		OrgDistributionUi dist = null;
		OrgSectionUi section = null;
		OrgSubdivisionUi subdivision = null;
		OrgEroUi ero = null;
		OrgDivisionUi division = null;
		OrgCircleUi circle = null;
		
		XMenu ssMenu = new XMenu();
		XMenuGroup ssMenuGroup = new XMenuGroup(ssMenu,"");
		ssMenuGroup.setStyleName("flatMenuGroup");
		subselectionBar.add(ssMenu);
		List<XMenuItem> menuItems = new ArrayList<XMenuItem>();
		switch(orgType) {
		case CollectionDashboardFilterSelection.DISTRIBUTION:
			String distributionCode = this.filterSelection.getSelectedDistribution();
			dist = dashboardData.getOrgCompanyUi().findDistribution(distributionCode);
			if (dist != null) {
				itemName = dist.getOdstName();
				orgValue = dist.getOdstCode();
				menuItems.add(createMenuItem(itemName, 
						newSelectAction(orgType,orgValue),
						orgType == CollectionDashboardFilterSelection.DISTRIBUTION));
				section = dist.getOdstSection();
			}
		case CollectionDashboardFilterSelection.SECTION:
			if (section == null && orgType == CollectionDashboardFilterSelection.SECTION) {
				String sectionCode = this.filterSelection.getSelectedSection();
				section = dashboardData.getOrgCompanyUi().findSection(sectionCode);
				List<OrgDistributionUi> orgDistributions = section.getOrgDistributions();
				for (OrgDistributionUi distribution : orgDistributions) {
					ssMenuGroup.add(createSubselectionMenuItem(
							distribution.getOdstName(),
							CollectionDashboardFilterSelection.DISTRIBUTION,
							distribution.getOdstCode()));
				}
			}
			if (section != null) {
				itemName = section.getOsctName();
				orgValue = section.getOsctCode();
				menuItems.add(createMenuItem(itemName, 
						newSelectAction(CollectionDashboardFilterSelection.SECTION,orgValue),
						orgType == CollectionDashboardFilterSelection.SECTION));
				subdivision = section.getOsctSubdivision();				
			}
		case CollectionDashboardFilterSelection.SUBDIVISION:
			if (subdivision == null && orgType == CollectionDashboardFilterSelection.SUBDIVISION) {
				String subdivisionCode = this.filterSelection.getSelectedSubdivision();
				subdivision = dashboardData.getOrgCompanyUi().findSubdivision(subdivisionCode);
				List<OrgSectionUi> orgSections = subdivision.getOrgSections();
				for (OrgSectionUi oSection : orgSections) {
					ssMenuGroup.add(createSubselectionMenuItem(
							oSection.getOsctName(),
							CollectionDashboardFilterSelection.SECTION,
							oSection.getOsctCode()));
				}
			}
			if (subdivision != null) {
				itemName = subdivision.getOsbdName();
				orgValue = subdivision.getOsbdCode();
				menuItems.add(createMenuItem(itemName, 
						newSelectAction(CollectionDashboardFilterSelection.SUBDIVISION,orgValue),
						orgType == CollectionDashboardFilterSelection.SUBDIVISION));
				ero = subdivision.getOsbdEro();
			}
		case CollectionDashboardFilterSelection.ERO:
			if (ero == null && orgType == CollectionDashboardFilterSelection.ERO) {
				String eroCode = this.filterSelection.getSelectedEro();
				ero = dashboardData.getOrgCompanyUi().findEro(eroCode);
				List<OrgSubdivisionUi> orgSubdivisions = ero.getOrgSubdivisions();
				for (OrgSubdivisionUi oSubdiv : orgSubdivisions) {
					ssMenuGroup.add(createSubselectionMenuItem(
							oSubdiv.getOsbdName(),
							CollectionDashboardFilterSelection.SUBDIVISION,
							oSubdiv.getOsbdCode()));
				}
			}
			if (ero != null) {
				itemName = ero.getOeroName();
				orgValue = ero.getOeroCode();
				menuItems.add(createMenuItem(itemName, 
						newSelectAction(CollectionDashboardFilterSelection.ERO,orgValue),
						orgType == CollectionDashboardFilterSelection.ERO));
				division = ero.getOeroDivision();
			}
		case CollectionDashboardFilterSelection.DIVISION:
			if (division == null && orgType == CollectionDashboardFilterSelection.DIVISION) {
				String divisionCode = this.filterSelection.getSelectedDivision();
				division = dashboardData.getOrgCompanyUi().findDivision(divisionCode);
				List<OrgEroUi> orgEros = division.getOrgEros();
				for (OrgEroUi oEro : orgEros) {
					ssMenuGroup.add(createSubselectionMenuItem(
							oEro.getOeroName(),
							CollectionDashboardFilterSelection.ERO,
							oEro.getOeroCode()));
				}
			}
			if (division != null) {
				itemName = division.getOdvsName();
				orgValue = division.getOdvsCode();
				menuItems.add(createMenuItem(itemName, 
						newSelectAction(CollectionDashboardFilterSelection.DIVISION,orgValue),
						orgType == CollectionDashboardFilterSelection.DIVISION));
				circle = division.getOdvsCircle();
			}
		case CollectionDashboardFilterSelection.CIRCLE:
			if (circle == null && orgType == CollectionDashboardFilterSelection.CIRCLE) {
				String circleCode = this.filterSelection.getSelectedCircle();
				circle = dashboardData.getOrgCompanyUi().findCircle(circleCode);
				List<OrgDivisionUi> orgDivision = circle.getOrgDivisions();
				for (OrgDivisionUi oDivision : orgDivision) {
					ssMenuGroup.add(createSubselectionMenuItem(
							oDivision.getOdvsName(),
							CollectionDashboardFilterSelection.DIVISION,
							oDivision.getOdvsCode()));
				}
			}
			if (circle != null) {
				itemName = circle.getOcrcName();
				orgValue = circle.getOcrcCode();
				menuItems.add(createMenuItem(itemName, 
						newSelectAction(CollectionDashboardFilterSelection.CIRCLE,orgValue),
						orgType == CollectionDashboardFilterSelection.CIRCLE));
			}
		case CollectionDashboardFilterSelection.COMPANY:
			itemName = dashboardData.getOrgCompanyUi().getOcmpName();
			orgValue = dashboardData.getOrgCompanyUi().getOcmpCode();
			menuItems.add(createMenuItem(itemName, 
					newSelectAction(CollectionDashboardFilterSelection.COMPANY,orgValue),
					orgType == CollectionDashboardFilterSelection.COMPANY));
			List<OrgCircleUi> orgCircles = dashboardData.getOrgCompanyUi().getOrgCircles();
			if (orgType == CollectionDashboardFilterSelection.COMPANY) {
				for (OrgCircleUi oCircle : orgCircles) {
					ssMenuGroup.add(createSubselectionMenuItem(
							oCircle.getOcrcName(),
							CollectionDashboardFilterSelection.CIRCLE,
							oCircle.getOcrcCode()));
				}
			}
		}
		for (int i = menuItems.size()-1; i >= 0; i--) {
			XMenuItem mi = menuItems.get(i);
			menuGroup.add(mi);
		}
	}
	
	private XMenuItem createSubselectionMenuItem(String itemName, 
			int orgType, String orgValue) {
		XMenuItem mi = new XMenuItem(itemName, newSelectAction(orgType, orgValue));
		mi.setStyleName("flatMenuItem");
		return mi;
	}

	private Action newSelectAction(final int orgType, final String orgValue) {
		return new Action() {
			@Override
			protected void execute() {
				setSelectionModel(orgType, orgValue);
			}
		};
	}

	private void setSelectionModel(int orgType, String orgValue) {
		filterSelection.setSelection(orgType,orgValue);
		updateFilterPanel();
		// display agg results corresponding to filter selection
		showAggResults();
	}
	
	private XMenuItem createMenuItem(String itemName, Action menuAction, boolean isSelected) {
		XMenuItem mi = new XMenuItem(itemName, menuAction);
		mi.setStyleName("flatMenuItem");
		mi.addStyleName("flatMenuHeadItem");
		if (isSelected) mi.addStyleName("flatMenuItemSelected");
		return mi;
	}

	private ListBox buildMonthFilter() {
		final ListBox listBox = new ListBox();
		List<String> summaryDataKeys = this.dashboardData.getSummaryDataKeys();
		for (String summaryDataKey : summaryDataKeys) {
			listBox.addItem(summaryDataKey, summaryDataKey);
		}
		
		listBox.addChangeHandler(new ChangeHandler() {
			public void onChange(ChangeEvent event) {
				fireDashboardFilterChangedEvent(listBox.getValue(listBox.getSelectedIndex()));
			}
		});
		return listBox;
	}
	
	private void updateMonthSelection(ListBox listBox) {
		for (int i=0; i<listBox.getItemCount(); i++) {
			String itemValue = listBox.getValue(i);
			if (itemValue.equals(this.filterSelection.getSelectedSummaryDataKey())) {
				listBox.setSelectedIndex(i);
				return;
			}
		}
	}
	
	private void fireDashboardFilterChangedEvent(String selectedSummaryDataKey) {
		this.filterSelection.setSelectedSummaryDataKey(selectedSummaryDataKey);
		showAggResults();
	}

	private void showAggResults() {
		hideDimPanels();
		Application.showMessage("Loading...");
		filterBussy();
		this.dataSource.fetchOrgDimResult(this.filterSelection, new ActionListener(){
			public void onBeforeAction(Object data) {}
			public void onAfterAction(Object data) {
				renderSummary((SearchResult)data);
			}
		});
	}
	
	private void renderSummary(SearchResult result) {
		Application.showMessage("");
		filterReady();
		renderAggHeadSummary(result);
		renderAggSubSummary(result);
		showDimPanels();
	}
	
	private void renderAggSubSummary(SearchResult result) {
		List<ResultRecord> results = result.getResults();
		SearchResult subHeadResult = new SearchResult();
		// Only interested in the result fields.
		SearchRequest sourceRequest = 
			SummarySearchRequestFactory.companySearchRequestFactory.createSearchRequest("", "");
		if (!results.isEmpty()) {
			int i = 0;
			for (ResultRecord r : results) {
				r = r.copy();
				i++;
				if (i==1) continue;// first record is dim summary record
				//r.orderValuesPerResultDef(sourceRequest, aggSubHeadSearchViewDef.searchResultDef);
				aggSubHeadSearchViewDef.searchResultDef.orderValuesPerResultDef(sourceRequest, r);
				subHeadResult.getResults().add(r);
			}
		}
		
		Widget resultsWidget = UiUtil.createResultsGrid(subHeadResult, aggSubHeadSearchViewDef, null, null);
		dimSubHeadPanel.clear();
		dimSubHeadPanel.add(resultsWidget);
	}
	private void renderAggHeadSummary(SearchResult result) {
		List<ResultRecord> results = result.getResults();
		SearchResult headResult = new SearchResult();
		// Only interested in the result fields.
		SearchRequest sourceRequest = 
			SummarySearchRequestFactory.companySearchRequestFactory.createSearchRequest("", "");
		if (!results.isEmpty()) {
			ResultRecord r = results.get(0);
			r = r.copy();
			//r.orderValuesPerResultDef(sourceRequest, aggHeadSearchViewDef.searchResultDef);
			aggHeadSearchViewDef.searchResultDef.orderValuesPerResultDef(sourceRequest, r);
			headResult.getResults().add(r);
		}
		
		Widget resultsWidget = UiUtil.createResultsGrid(headResult, aggHeadSearchViewDef, null, null);
		dimHeadPanel.clear();
		dimHeadPanel.add(resultsWidget);
	}
}