package ebilly.admin.client.view;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;

import ebilly.admin.shared.viewdef.ResultRecord;

public class PrAmountCellRenderer implements CellRenderer {

	SearchResultDefinition searchResultDef;
	private String amountField;
	public PrAmountCellRenderer(String amountField,
			SearchResultDefinition searchResultDef) {
		super();
		this.searchResultDef = searchResultDef;
		this.amountField = amountField;
	}
	
	@Override
	public Cell renderIntoCell(FlexTable resultGrid, int row,
			ResultRecord record, CrudViewDefinition parentCrudViewDef,
			CrudViewDefinition sectionCrudViewDef) {
		Cell c = new Cell();
		String amountValue = searchResultDef.getValue(amountField, record);
		amountValue = UiUtil.decimalPadding(amountValue);
		amountValue = UiUtil.amountFormat(amountValue);
		Label label = new Label(amountValue);
		label.setStyleName("search-view-data-label");
		c.addWidget(label);
		return c;
	}
}
