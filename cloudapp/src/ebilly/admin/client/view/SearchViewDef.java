package ebilly.admin.client.view;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import ebilly.admin.client.menu.ActionListener;
import ebilly.admin.shared.EntitySelection;
import ebilly.admin.shared.EntityUi;
import ebilly.admin.shared.viewdef.ResultField;

public abstract class SearchViewDef {
	
	//public List<ViewField> searchFilterDef = new ArrayList<ViewField>();
	public SearchFilterDefinition searchFilterDef = new SearchFilterDefinition();
	
	public SearchResultDefinition searchResultDef = new SearchResultDefinition();
	
	public Map<ViewField, Widget> filterInput = new HashMap<ViewField, Widget>();

	public boolean hideFilter = false;
	public boolean hideHeader = false;
	
	private EntitySelection entitySelection = new EntitySelection();
	private ActionListener entitySelectedListener;
	private ActionListener entityUnselectedListener;
	
	public SearchViewDef() {
		initSearchFilterDef(this.searchFilterDef);
		initSearchResultDef(this.searchResultDef);
	}
	
	protected abstract void initSearchResultDef(SearchResultDefinition searchResultDef);

	protected abstract void initSearchFilterDef(SearchFilterDefinition searchFilterDef);

	public Map<String, String> getFilterTypeMap() {
		Map<String, String> filterTypeMap = new HashMap<String, String>();
		for (ViewField searchFilter : this.searchFilterDef.getFilterFields()) {
			filterTypeMap.put(searchFilter.fieldName, searchFilter.getFieldType());
		}
		return filterTypeMap;
	}
	
	public ViewField getViewField(String fieldName) {
		/*for (ViewField viewField : this.searchFilterDef) {
			if (fieldName.equals(viewField.fieldName)) {
				return viewField;
			}
		}
		return null;*/
		return this.searchFilterDef.findFilterField(fieldName);
	}

	public void resetFilterInput() {
		for (Entry<ViewField, Widget> filterInput : this.filterInput.entrySet()) {
			Widget inputWidget = filterInput.getValue();
			if (inputWidget == null) continue;
			if (inputWidget instanceof TextBox) {
				((TextBox)inputWidget).setText("");
			}
		}
	}

	public abstract String getSearchObjectName();

	public List<ResultField> getOrderByFields() {
		return Collections.emptyList();
	}
	
	public void addSelectedEntity(String entityName, String entityId) {
		this.entitySelection.add(entityName, entityId);
		if (this.entitySelectedListener != null) {
			this.entitySelectedListener.onAfterAction(new EntityUi(entityName, entityId));
		}
	}

	public void removeSelectedEntity(String entityId) {
		this.entitySelection.remove(entityId);
		if (this.entityUnselectedListener != null) {
			this.entityUnselectedListener.onAfterAction(new EntityUi(null, entityId));
		}
	}
	
	public void clearEntitySelection() {
		this.entitySelection.clear();
		if (this.entityUnselectedListener != null) {
			this.entityUnselectedListener.onAfterAction(null);
		}
	}
	
	public void setEntitySelectedListener(ActionListener actionListener) {
		this.entitySelectedListener = actionListener;
	}
	public void setEntityUnselectedListener(ActionListener actionListener) {
		this.entityUnselectedListener = actionListener;
	}

	public EntitySelection getEntitySelection() {
		return entitySelection;		
	}
	
	public int getHiddenFieldCount() {
		int numHidden = 0;
		for (ResultColumn resultColumn : this.searchResultDef) {
			if (resultColumn.isHidden) numHidden++;
		}
		return numHidden;
	}
}

