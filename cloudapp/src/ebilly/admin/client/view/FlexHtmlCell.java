package ebilly.admin.client.view;

import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

import ebilly.admin.client.view.animation.TextHighlightAnimation;

public class FlexHtmlCell extends HTMLPanel implements HasClickHandlers {
	private String value;
	private Widget helpWidget;
	public FlexHtmlCell() {
		super("");
	}
	
	public void setValue(String value) {
		String oldValue = this.value;
		String newValue = value;
		
		this.value = value;
		this.clear();
		HTMLPanel valuePanel = new HTMLPanel(value);
		this.add(valuePanel);
		//this.getElement().setInnerHTML(value);
		addHelpWidget();
		animateValueChange(oldValue,newValue, valuePanel);
	}
	
	private void animateValueChange(String oldValue, String newValue,
			HTMLPanel valuePanel) {
		if (oldValue != null && !oldValue.equals(newValue) ||
			newValue != null && !newValue.equals(oldValue)) {
			Element element = valuePanel.getElement();
			try {
				(new TextHighlightAnimation(element)).run(1000);
			} catch (Exception e) {
				// IE Does not support animation and
				// exception is thrown
			}
		}
	}

	private void addHelpWidget() {
		if (helpWidget != null) this.add(helpWidget);
	}
	
	public String getValue() {
		return this.value;
	}
	
	public void setHelpText(String helpText) {
		helpWidget = UiUtil.createHelpWidget(helpText);
		addHelpWidget();
	}

	protected HandlerRegistration clickHandler;
	private ClickHandler handler;
	@Override
	public HandlerRegistration addClickHandler(ClickHandler handler) {
		this.handler = handler;
		reRegisterClickHandler();
		return clickHandler;
	}
	
	protected final void reRegisterClickHandler() {
		clickHandler = super.addDomHandler(handler, ClickEvent.getType());
	}
}