package ebilly.admin.client.view;

import ebilly.admin.client.AppScreen;

public class LivePrSearchView extends SearchView implements AppScreen {

	public LivePrSearchView(String caption) {
		super(caption,true);
	}
	
	@Override
	protected SearchViewDef getSearchViewDefinition() {
		return new LivePrSearchViewDef();
	}
}
