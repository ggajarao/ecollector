package ebilly.admin.client.view.animation;

import com.google.gwt.animation.client.Animation;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style;

public class TextHighlightAnimation extends Animation {
	
	private Element subject;
	
	private String oldBackgroundColor;
	
	private double alpha = 1;
	public TextHighlightAnimation(Element element) {
		this.subject = element;
	}
	
	@Override
	protected void onStart() {
		Style style = subject.getStyle();
		oldBackgroundColor = style.getBackgroundColor();
		String backgroundColor = "rgba(235, 255, 0, "+alpha+")";
		style.setBackgroundColor(backgroundColor);
	}
	
	@Override
	protected void onComplete() {
		Style style = subject.getStyle();
		style.setBackgroundColor(oldBackgroundColor);
	}
	
	@Override
	protected void onUpdate(double progress) {
		alpha = 1 - progress;
		subject.getStyle().setBackgroundColor("rgba(235, 255, 0, "+alpha+")");
	}
}
