package ebilly.admin.client.view;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import ebilly.admin.client.Application;
import ebilly.admin.client.menu.ActionListener;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.viewdef.CrudRequest;
import ebilly.admin.shared.viewdef.CrudResponse;
import ebilly.admin.shared.viewdef.ResultRecord;
import ebilly.admin.shared.viewdef.SearchResult;

public class UiUtil {
	
	public static Widget decorateWithHelpText(final Widget widget, String helpText) {
		Widget helpWidget = createHelpWidget(helpText);
		
		HorizontalPanel hp = new HorizontalPanel();
		hp.setVerticalAlignment(VerticalPanel.ALIGN_TOP);
		hp.add(widget);
		hp.add(helpWidget);
		
		return hp;
	}
	
	public static Widget createEditPanel(Widget inputWidget, ClickHandler saveHandler,
			ClickHandler cancelHandler) {
		HorizontalPanel hp = new HorizontalPanel();
		hp.setSpacing(2);
		hp.setWidth("100%");
		Button save = new Button("");
		save.setStyleName("button-small-save");
		save.addClickHandler(saveHandler);
		Button cancel = new Button("");
		cancel.setStyleName("button-small-cancel");
		cancel.addClickHandler(cancelHandler);
		hp.add(inputWidget);
		hp.setCellHorizontalAlignment(inputWidget, HorizontalPanel.ALIGN_LEFT);
		hp.add(save);
		hp.setCellVerticalAlignment(save, VerticalPanel.ALIGN_BOTTOM);
		//hp.setCellHorizontalAlignment(save, HorizontalPanel.ALIGN_RIGHT);
		hp.add(cancel);
		hp.setCellVerticalAlignment(cancel, VerticalPanel.ALIGN_BOTTOM);
		//hp.setCellHorizontalAlignment(cancel, HorizontalPanel.ALIGN_RIGHT);
		
		return hp;
	}
	
	public static Widget createHelpWidget(String helpText) {
		final Label helpWidget = new Label("? ");
		/*helpWidget.setWidth("15px");
		helpWidget.setHeight("15px");*/
		helpWidget.setStyleName("help");
		if (helpText == null || helpText.trim().length() == 0) {
			return helpWidget;
		}
		
		boolean autoHide = true;
		final PopupPanel helpTextPp = new PopupPanel(autoHide);
		Label helpTextLabel = new Label();
		helpTextLabel.getElement().setInnerHTML(helpText);
		helpTextPp.setWidget(helpTextLabel);
		helpTextPp.setWidth("300px");
		
		helpWidget.addMouseOverHandler(new MouseOverHandler() {
			public void onMouseOver(MouseOverEvent event) {
				helpTextPp.showRelativeTo(helpWidget);
			}
		});
		helpWidget.addMouseOutHandler(new MouseOutHandler() {
			public void onMouseOut(MouseOutEvent event) {
				helpTextPp.hide();
			}
		});
		return helpWidget;
	}

	public static void loadRecord(String recordId, 
			final String crudObjectName, final CrudViewDefinition viewDef,
			final ActionListener onLoadListener) {
		Application.showMessage("Loading...");
		CrudRequest crudRequest = new CrudRequest();
		crudRequest.crudObject = crudObjectName;
		crudRequest.crudAction = AppConfig.CRUD_ACTION_RETRIEVE;
		crudRequest.addFieldValue("id", recordId);
		for (ViewSectionDefinition viewSectionDef : viewDef.viewSectionDefinitions.values()) {
			crudRequest.addSectionResultColumns(viewSectionDef.sectionName, 
					viewSectionDef.searchViewDef.searchResultDef.getColumns());
		}
		Application.secureService.processCrud(crudRequest, new BaseServiceResponseHandler<CrudResponse>(){
			/*@Override
			public void onFailure(Throwable caught) {
				Application.showErrorMessage(
						"Error while loading "+crudObjectName+", " +
						"reason: "+caught.getMessage());
			}*/
			
			@Override
			public void onSuccess(CrudResponse result) {
				if (result.isSuccess()) {
					Application.showMessage("");
					viewDef.setCrudEntity(result.crudEntity);
					viewDef.setCrudValues(
							result.fieldValueMap);
					//crudEntity = result.crudEntity;
					viewDef.setSectionDataList(result.sections);
					if (onLoadListener != null) {
						onLoadListener.onAfterAction(viewDef);
					}
				} else {
					Application.showErrorMessage(result.errorMessage);
				}
			}			
		});
	}
	
	public static void requestToCreate(String objectName, 
			Map<String, String> fieldValueMap,
			final ActionListener completeListener) {
		Application.showMessage("Saving....");
		CrudRequest crudRequest = new CrudRequest();
		crudRequest.crudObject = objectName;
		crudRequest.crudAction = AppConfig.CRUD_ACTION_CREATE_UPDATE;
		/*Map<String, String> fieldValueMap = null;
		try {
			fieldValueMap = this.viewDef.validateAndGetFieldValueMap();
		} catch (IllegalArgumentException e) {
			showError(e.getMessage());
			return;
		}*/
		
		crudRequest.setFieldValueMap(fieldValueMap);
		Application.secureService.processCrud(crudRequest,
				new BaseServiceResponseHandler<CrudResponse>() {
					/*@Override
					public void onFailure(Throwable caught) {
						Application.showErrorMessage(
								"Problems while saving, reason: "+caught.getMessage());
					}*/
					@Override
					public void onSuccess(CrudResponse result) {
						if (result == null) {
							Application.showErrorMessage("Unexpected state!");
							return;
						}
						if (!CommonUtil.isEmpty(result.errorMessage)) {
							Application.showErrorMessage(result.errorMessage);
						} else {
							Application.showMessage(result.successMessage);
							completeListener.onAfterAction(result);
							//CrudView.this.onComplete(result);
						}
					}
				});
	}

	public static DialogBox createDialog(Widget uiObject) {
		DialogBox dialog = new DialogBox();
		
		VerticalPanel v = new VerticalPanel();
		v.setSpacing(10);
		v.add(uiObject);
		dialog.setWidget(v);
		
		//Ref: http://stackoverflow.com/questions/3702644/gwt-dialogbox-sizing-problem-when-animation-is-enabled
		//dialog.setAnimationEnabled(true);
		
		//dialog.setText(dialogCaption);
		dialog.setGlassEnabled(true);
		dialog.setAutoHideEnabled(false);

		// this will show the dialog, hence commented. 
		//dialog.center();
		
		return dialog;
	}
	
	private static final RegExp rfc2822 = RegExp.compile(
	    "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$"
	);
	
	public static boolean isValidEmail(String email) {
		return rfc2822.test(email);
	}
	
	public static Date getDateFromString(String value) {
		if (CommonUtil.isEmpty(value)) return null;
		DateTimeFormat dtFormat = DateTimeFormat.getFormat(CommonUtil.DEFAULT_DATE_FORMAT);
		return dtFormat.parse(value.trim());
	}
	
	public static Date getDateFromString(String value, String format) {
		if (CommonUtil.isEmpty(value)) return null;
		DateTimeFormat dtFormat = DateTimeFormat.getFormat(CommonUtil.DEFAULT_DATE_FORMAT);
		return dtFormat.parse(value.trim());
	}
	
	public static String getFormattedDate(Date date) {
		if (date == null) return "";
		DateTimeFormat dtFormat = DateTimeFormat.getFormat(CommonUtil.DEFAULT_DATE_FORMAT);
		return dtFormat.format(date);
	}
	
	public static String getFormattedDateTime(Date date) {
		if (date == null) return "";
		DateTimeFormat dtFormat = DateTimeFormat.getFormat(CommonUtil.DEFAULT_DATE_TIME_FORMAT);
		return dtFormat.format(date);
	}
	
	public static Widget createResultsGrid(SearchResult result,
			SearchViewDef searchViewDef, CrudViewDefinition parentCrudViewDef, 
			CrudViewDefinition sectionCrudViewDef) {
		
		FlexTable resultGrid = new FlexTable();
		resultGrid.setWidth("100%");
		
		// Results header
		if (!searchViewDef.hideHeader) {
			int column = 0;
			for (ResultColumn resultColumn : searchViewDef.searchResultDef) {
				if (resultColumn.isHidden) {
					continue;
				}
				Label resultColumnLabel = new Label(resultColumn.displayName);
				resultColumnLabel.setStyleName("result-col-heading");
				if (column == 0) {
					// Set the style class for first column
					resultColumnLabel.addStyleName("result-col-heading-first");
				}
				resultGrid.setWidget(0, column, resultColumnLabel);
				column++;
			}
			resultGrid.getRowFormatter().addStyleName(0, "search-results-head");
		}
		
		if (result == null) return resultGrid;
		
		// Result records
		List<ResultRecord> results = result.getResults();
		if (results == null || results.isEmpty()) {
			int firstRow = 1, firstCol = 0;
			resultGrid.setWidget(firstRow, firstCol, new DataLabel("No Records"));
			resultGrid.getFlexCellFormatter().setColSpan(firstRow, firstCol, 
					searchViewDef.searchResultDef.size() - searchViewDef.getHiddenFieldCount());
			resultGrid.getFlexCellFormatter().setHorizontalAlignment(
					firstRow, firstCol, HorizontalPanel.ALIGN_CENTER);
		}
		
		int rowNumber = 1;
		String styleName = "search-results-row-even";
		for (ResultRecord record : results) {			
			// check if result record field count is incompatible 
			// with search result definition
			if (record.fieldValues.isEmpty() || 
					record.fieldValues.size() != searchViewDef.searchResultDef.size()) {
				continue;
			}
			int numHidden = 0;
			for (int colCounter = 0; colCounter < searchViewDef.searchResultDef.size(); colCounter++) {
				ResultColumn resultColumn = searchViewDef.searchResultDef.get(colCounter);
				if (resultColumn.isHidden) {
					numHidden++;
					continue;
				}
				Widget columnWidget = resultColumn.getCellWidget(
						resultGrid, 
						rowNumber, 
						record,
						parentCrudViewDef,
						sectionCrudViewDef);
				if (colCounter == 0) {
					// set the style name for the first column
					columnWidget.addStyleName("result-col-first");
				}
				resultGrid.setWidget(rowNumber, colCounter - numHidden, 
						columnWidget);
			}
			if (rowNumber%2 == 0) {
				styleName = "search-results-row-even";
			} else {
				styleName = "search-results-row-odd";
			}
			
			resultGrid.getRowFormatter().addStyleName(rowNumber, styleName);
			rowNumber++;
		}
		return resultGrid;
	}
	
	public static Widget createSearchFilterWidget(final SearchViewDef viewDef,
			ActionBarWidget actionBar) {
		FlexTable flexTable = new FlexTable();
		int rowCounter = 0;
		int colCounter = 0;
		int colOffset = 0;
		if (viewDef.searchFilterDef != null && !viewDef.hideFilter) {
			for (ViewField filter : viewDef.searchFilterDef.getFilterFields()) {
				boolean hideCaption = false;
				Widget widget = filter.getInputWidget();
				if (widget instanceof RoleSelectionPanel ||
					widget instanceof DateOrDateRangeSelectionPanel) {
					hideCaption = true;
				}
				
				if (!hideCaption) {
					Label fieldLabel = new Label(filter.displayName);
					fieldLabel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
					flexTable.setWidget(rowCounter, colCounter+colOffset, fieldLabel);
					colOffset++;
					
				} else {
					flexTable.getFlexCellFormatter().setColSpan(
							rowCounter, colCounter+colOffset, 2);
				}
				
				flexTable.setWidget(rowCounter, colCounter+colOffset, widget);
				viewDef.filterInput.put(filter,widget);
				colOffset++;
				
				colCounter++;
				if (colCounter % 2 == 0) {
					colCounter = 0;
					colOffset = 0;
					rowCounter++;
				}
			}
		} else if (viewDef.searchFilterDef != null) {
			// In case of hide filter, initialize filter widgets
			for (ViewField filter : viewDef.searchFilterDef.getFilterFields()) {
				filter.getInputWidget();
			}
		}
		rowCounter++;
		
		//ActionBarWidget actionBar = new ActionBarWidget();
		if (actionBar != null && !viewDef.hideFilter) {
			flexTable.setWidget(rowCounter, 0, actionBar);
			flexTable.getFlexCellFormatter().setHorizontalAlignment(rowCounter, 0, HorizontalPanel.ALIGN_CENTER);
			flexTable.getFlexCellFormatter().setColSpan(rowCounter, 0, 6);
			flexTable.getFlexCellFormatter().setColSpan(2, 0, 4);
		}
		return flexTable;
	}
	
	/**
	 * Prepends 0's before the given number. Number of 
	 * 0's prepended can be speicified in the param numZeros
	 * 
	 * Eg: n =   12, places=3 return value: 012
	 * Eg: n =  230, places=3 return value: 230
	 * @param n
	 * @param places
	 * @return
	 */
	// copied from FormatUtils
	public static String numberPadding(String n, int places) {
		int number = 0;
		try {
			number = Integer.parseInt(n);
		} catch(NumberFormatException nfe) {
		}
		String s = number+"";
		StringBuffer sb = new StringBuffer();
		for (int i = s.length(); i < places; i++) {
			sb.append("0");
		}
		return sb.append(s).toString();
	}
	
	/**
	 * Pads 0's upto two decimal places. 
	 * Also truncates beyond two decimal places
	 * 
	 * eg: input 100 returns 100.00
	 *     input .21 returns 0.21
	 *     input .7 returns 0.70
	 *     input 12.3212 return 12.32
	 * 
	 * @param d
	 * @return
	 */
	// copied from FormatUtils
	public static String decimalPadding(String d) {
		if (d == null) return "0.00";
		if (d.trim().isEmpty()) return "0.00";
		
		// Padding zeros to 2 places
		StringBuffer s = new StringBuffer(d.trim());
		int decIndex = (s.indexOf("."));
		if (s.length() == 0) {
			s.append("0.00");
			return s.toString();
		}
		
		if (s.toString().startsWith(".")) {
			s = new StringBuffer("0");
			s.append(d.trim());
			decIndex = s.indexOf(".");
		}
		
		if (decIndex == -1) {
			s.append(".00");
		} else {
			String dec = s.substring(decIndex+1);
			if (dec.length() < 2) {
				for (int i = dec.length(); i < 2; i++ ) {
					s.append("0");
				}
			} else if (dec.length() > 2) {
				s = new StringBuffer(s.substring(0,decIndex+1));
				s.append(dec.substring(0, 2));
			}
		}
		return s.toString();
	}
	
	/**
	 * Given 1000.00 returns 1,000.00 
	 * @param d
	 * @return
	 */
	// copied from FormatUtils
	public static String amountFormat(String d) {
		if (d == null) return "";
		d = d.trim();
		StringBuffer sb = new StringBuffer(d);
		int decIndex = sb.indexOf(".");
		if (decIndex == -1) decIndex = sb.length();
		int pos = -1;
		for ( int i = decIndex; i > 0; i--) {
			pos++;
			if (pos % 3 == 0 && pos != 0) {
				sb.insert(i, ",");
			}
		}
		return sb.toString();
	}
}
