package ebilly.admin.client.view;


public class FlexHtmlTupleCell extends FlexHtmlCell {
	private FlexHtmlCaptionCell captionCell;
	private FlexHtmlDataCell dataCell;
	
	public FlexHtmlTupleCell() {
		this.addStyleName("tuple-cell");
		captionCell = new FlexHtmlCaptionCell();
		dataCell = new FlexHtmlDataCell();
		
		this.add(captionCell);
		this.add(dataCell);
	}
	
	public FlexHtmlCaptionCell getCaptionCell() {
		return captionCell;
	}
	
	public FlexHtmlDataCell getDataCell() {
		return dataCell;
	}
	
}