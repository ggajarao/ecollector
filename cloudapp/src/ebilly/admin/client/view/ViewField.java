package ebilly.admin.client.view;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.google.gwt.user.client.ui.Widget;

public class ViewField implements Serializable, IsSerializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public final String fieldName;
	private final String fieldType;
	public final String displayName;
	//private Widget inputWidget = null;
	public boolean isMandatory = false;
	public boolean isHidden = false;
	public int crudAction = 1; // 1 - Create, 2 - Update, 3 - View
	private boolean isReadonlyOnEdit = false;
	public boolean isHiddenOnEdit = false;
	public boolean isReadonly = false;
	public String helpText;
	private int inputMaxLenght = 200; 
	
	private ViewFieldUiAdapter viewAdapter = new ViewFieldUiAdapter(this);
	
	public ViewField(String fieldName, String fieldType, String displayName) {
		this.fieldName = fieldName;
		this.fieldType = fieldType;
		this.displayName = displayName;
		this.getInputCaption();
	}
	
	public ViewField(String fieldName, String fieldType, Widget inputWidget, String displayName) {
		this.fieldName = fieldName;
		this.fieldType = fieldType;
		this.displayName = displayName;
		viewAdapter.setInputWidget(inputWidget);
		this.getInputCaption();
	}
	
	public ViewField(String fieldName, String fieldType, String displayName,
			FlexHtmlTupleCell tupleCell) {
		this.fieldName = fieldName;
		this.fieldType = fieldType;
		this.displayName = displayName;
		setHtmlTuple(tupleCell);
		this.getInputCaption();
	}
	
	public void setHtmlTuple(FlexHtmlTupleCell tupleCell) {
		FlexHtmlCaptionCell captionCell = tupleCell.getCaptionCell();
		captionCell.setValue(this.displayName);
		viewAdapter.setInputCaptionWidget(captionCell);
		viewAdapter.setInputWidget(tupleCell.getDataCell());
	}
	
	public String getFieldType() {
		return this.fieldType;
	}
	
	public void setCrudAction(int crudAction) {
		this.crudAction = crudAction;
	}
	
	public Widget getInputWidget() {
		/*if (this.inputWidget == null) {
			this.inputWidget = createInputWidget();
		}
		return this.inputWidget;*/
		return viewAdapter.getInputWidget();
	}
	
	public Widget getInputCaption() {
		/*if (inputCaption == null) {
			this.inputCaption = new Label(this.displayName + (this.isMandatory?"*":""));
			this.inputCaption.setStyleName("field-caption");
		}
		return this.inputCaption;*/
		return viewAdapter.getInputCaption();
	}
	
	public Widget getViewCaption() {
		/*if (viewCaption == null) {
			this.viewCaption = new Label(this.displayName);
			this.viewCaption.setStyleName("field-caption");
		}
		return this.viewCaption;*/
		return viewAdapter.getViewCaption();
	}

	public void setMandatory(boolean b) {
		this.isMandatory = b;
	}
	
	public boolean isMandatory() {
		return this.isMandatory;
	}

	public void resetError() {
		viewAdapter.resetError();	
	}
	
	public void setError() {
		viewAdapter.setError();
	}

	public void setValue(String value) {
		viewAdapter.setValue(value);
	}
	
	public void setValue(String value, Widget widget) {
		viewAdapter.setValue(value, widget);
	}
	
	public String getValue(ErrorMessage errorMsg) {
		return viewAdapter.getValue(errorMsg);
	}
	
	public String getValue(ErrorMessage errorMsg, Widget inputWidget) {
		return viewAdapter.getValue(errorMsg, inputWidget);
	}

	public void setReadOnlyOnEdit(boolean flag) {
		this.isReadonlyOnEdit  = flag;
	}
	
	public void setMaxLenght(int maxLenght) {
		this.inputMaxLenght = maxLenght;
	}
	
	public int getMaxLenght() {
		return this.inputMaxLenght;
	}

	public boolean isReadonlyOnEdit() {
		return isReadonlyOnEdit;
	}
	
	public Widget createInputWidget() {
		return viewAdapter.createInputWidget();
	}
}