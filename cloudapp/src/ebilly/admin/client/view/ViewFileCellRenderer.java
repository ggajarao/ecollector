package ebilly.admin.client.view;

import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Widget;

import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.viewdef.ResultRecord;

public class ViewFileCellRenderer implements CellRenderer {

	SearchResultDefinition searchResultDef;
	private String blobKeyField;
	private String fileNameField;
	public ViewFileCellRenderer(String blobKeyFiled,
			String fileNameField,
			SearchResultDefinition searchResultDef) {
		super();
		this.fileNameField = fileNameField;
		this.searchResultDef = searchResultDef;
		this.blobKeyField = blobKeyFiled;
	}
	
	@Override
	public Cell renderIntoCell(FlexTable resultGrid, int row,
			ResultRecord record, CrudViewDefinition parentCrudViewDef,
			CrudViewDefinition sectionCrudViewDef) {
		Cell c = new Cell();
		final String blobKey = searchResultDef.getValue(blobKeyField, record);
		String fileName = searchResultDef.getValue(fileNameField, record);
		if (CommonUtil.isEmpty(fileName)) {
			fileName = "Unknown-File-Name";
		}
		Anchor a = new Anchor(fileName);
		a.setHref(CommonUtil.getDownloadUrl(blobKey));
		a.setTarget("-");
		c.addWidget(a);
		return c;
	}
}

