package ebilly.admin.client.view;

public class ErrorMessage {
	String errorMessage = "";
	private boolean hasErrors;
	public ErrorMessage() {
		reset();
	}
	
	public void reset() {
		errorMessage = "<ul>";
		hasErrors = false;
	}
	
	public void addItem(String item) {
		this.hasErrors = true;
		errorMessage += "<li>"+item+"</li>";
	}
	
	public String getMessage() {
		return errorMessage + "<ul>";
	}

	public boolean hasErrors() {
		return this.hasErrors;
	}
}

