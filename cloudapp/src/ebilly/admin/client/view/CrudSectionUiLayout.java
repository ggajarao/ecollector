package ebilly.admin.client.view;

import java.util.List;

import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import ebilly.admin.client.layout.UiLayoutBase;
import ebilly.admin.shared.viewdef.SectionData;

public class CrudSectionUiLayout extends UiLayoutBase {
	
	private CrudViewDefinition crudViewDef;
	private VerticalPanel panel;
	private VerticalPanel sectionsPanel;
	private CrudView crudView;
	public CrudSectionUiLayout(CrudView crudView) {
		this.crudView = crudView;
		this.crudViewDef = crudView.getViewDef();
		buildUi();
	}
	
	private void buildUi() {
		/*VerticalPanel spacer = new VerticalPanel();
		spacer.setWidth("100%");
		spacer.setSpacing(40);
		contentPanel.add(spacer);*/
		panel = new VerticalPanel();
		panel.setHorizontalAlignment(VerticalPanel.ALIGN_LEFT);
		
		sectionsPanel = new VerticalPanel();
		sectionsPanel.setHorizontalAlignment(VerticalPanel.ALIGN_LEFT);
		//contentPanel.add(sectionsPanel);
		
		panel.add(sectionsPanel);
		
		/*if (!this.crudViewDef.hideCrudCommands && 
				!this.crudViewDef.isViewAction()) { 
				//this.viewDef.crudAction != CrudViewDefinition.CRUD_ACTION_VIEW) {
			flexTable.setWidget(rowCounter,0,actionBar);
			flexTable.getFlexCellFormatter().setColSpan(rowCounter, 0, 6);
		}*/
	}
	
	@SuppressWarnings("unchecked")
	public void bindData(Object data) {
		List<SectionData> sections = (List<SectionData>)data;
		sectionsPanel.clear();
		
		for (ViewSectionDefinition sectionDef : this.crudViewDef.viewSectionDefinitions.values()) {
			for (SectionData section : sections) {
				sectionsPanel.add(
						sectionDef.getSectionViewFor(
								section));
			}
		}
	}
	

	@Override
	public Widget getUiObject() {
		return panel;
	}

}
