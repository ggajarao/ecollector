package ebilly.admin.client.view;

import ebilly.admin.client.AppScreen;

public class PrSearchView extends SearchView implements AppScreen {

	public PrSearchView(String caption) {
		super(caption,true);
	}
	
	@Override
	protected SearchViewDef getSearchViewDefinition() {
		return new PrSearchViewDef();
	}
}
