package ebilly.admin.client.view;

import ebilly.admin.client.AppScreen;

@Deprecated
public class ExportRequestSearchView extends SearchView implements AppScreen {

	public ExportRequestSearchView(String caption) {
		super(caption,true);
	}
	
	@Override
	protected SearchViewDef getSearchViewDefinition() {
		return null;
		//return new ExportRequestSearchViewDef();
	}
}
