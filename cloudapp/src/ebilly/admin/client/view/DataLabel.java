package ebilly.admin.client.view;

import com.google.gwt.user.client.ui.Label;

public class DataLabel extends Label {
	
	public DataLabel() {
		super();
		this.setStyleName("data-label");
	}
	
	public DataLabel(String text) {
		super(text, true);
		this.setStyleName("data-label");
	}
}

