package ebilly.admin.client.view;

import ebilly.admin.client.view.ActionBarWidget;
import ebilly.admin.client.view.SearchView;
import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.viewdef.SearchResult;
import ebilly.admin.shared.viewdef.SectionData;

import com.google.gwt.user.client.ui.Widget;

public class ViewSectionDefinition {
	
	public SearchViewDef searchViewDef;
	private ActionBarWidget actionBar;
	class SectionSearchView extends SearchView {
		public SectionSearchView(String caption) {
			super(caption,true);
			buildUi();
		}
		
		@Override
		protected SearchViewDef getSearchViewDefinition() {
			return searchViewDef;
		}
		
		public void buildUi() {
			super.setContentPanelBorderSpace(0);
			this.actionBarWidget.setCellPadding(0);
			this.captionLabel.setStylePrimaryName("section-title-panel");
		}
		
		public void setActionBar(ActionBarWidget actionBar) {
			this.actionBarWidget.copyFrom(actionBar);
		}
	};
	/*private class SectionSearchView extends SearchView {
		public SectionSearchView() {
			super("");
		}
		
		@Override
		protected SearchViewDef getSearchViewDefinition() {
			return searchViewDef;
		}
	};*/
	
	//private SectionSearchView searchView;

	public String sectionName;
	
	public ViewSectionDefinition(String sectionName, SearchViewDef searchViewDef) {
		this.sectionName = sectionName;
		this.searchViewDef = searchViewDef;
		//this.searchView = new SectionSearchView();
	}
	
	public void setActionBar(ActionBarWidget actionBar) {
		this.actionBar = actionBar;
	}
	
	public Widget getSectionViewFor(SectionData section) {
		SectionSearchView sv = new SectionSearchView(
				((CommonUtil.isEmpty(section.sectionSetName)) ? 
						this.sectionName
						:
						section.sectionSetName+" - "+this.sectionName));
		sv.setActionBar(this.actionBar);
		sv.showResults(new SearchResult(section.searchResult.getResults()),null);
		return sv.getUiObject();
	}
	
	/*public void setSectionRecords(List<ResultRecord> resultRecords) {
		this.searchView.showResults(resultRecords);
	}*/

	public SearchViewDef getSearchViewDef() {
		return searchViewDef;
	}

	public void setSearchViewDef(SearchViewDef searchViewDef) {
		this.searchViewDef = searchViewDef;
	}
	
	/*public Widget getUiObject() {
		return searchView.getUiObject();
	}*/
}
