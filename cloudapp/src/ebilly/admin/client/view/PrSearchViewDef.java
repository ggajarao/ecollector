package ebilly.admin.client.view;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.viewdef.ResultRecord;

public class PrSearchViewDef extends SearchViewDef {

	@Override
	protected void initSearchFilterDef(SearchFilterDefinition searchFilterDef) {
		searchFilterDef.add(new ViewField("deviceName",java.lang.String.class.getName(),"Device Name"));
		searchFilterDef.add(new ViewField("_collectionDate", DateOrDateRangeSelectionPanel.class.getName(),"Collection Date"));
	}
	
	@Override
	protected void initSearchResultDef(SearchResultDefinition searchResultDef) {
		ResultColumn c = new ResultColumn("id",java.lang.String.class.getName(),"id");
		c.isHidden = true;
		searchResultDef.add(c);
		
		c = new ResultColumn("deviceName",java.lang.String.class.getName(),"Device Name");
		searchResultDef.add(c);
		
		c = new ResultColumn("dISTRIBUTION",java.lang.String.class.getName(),"Distribution");
		searchResultDef.add(c);
		
		c = new ResultColumn("sC_NO",java.lang.String.class.getName(),"Service Number");
		searchResultDef.add(c);
		
		c = new ResultColumn("cOLLECTION_DATE",java.lang.String.class.getName(),"Prs Reading");
		c.isHidden = true;
		searchResultDef.add(c);
		c = new ResultColumn("cOLLECTION_TIME",java.lang.String.class.getName(),"Prs Reading");
		c.isHidden = true;
		searchResultDef.add(c);
		
		c = new ResultColumn("CollectionDateTime",java.lang.String.class.getName(),"Collection Date/Time");
		c.setCellRenderer(new PrCollectionDateTimeCellRenderer("cOLLECTION_DATE","cOLLECTION_TIME",searchResultDef));
		searchResultDef.add(c);
		
		c = new ResultColumn("aMOUNT_COLLECTED",java.lang.String.class.getName(),"Amount");
		c.setCellRenderer(new NumberRenderer("aMOUNT_COLLECTED",searchResultDef));
		searchResultDef.add(c);
	}
	
	@Override
	public String getSearchObjectName() {
		return AppConfig.OBJ_PR_REPORT;
	}
}

class PrCollectionDateTimeCellRenderer implements CellRenderer {

	SearchResultDefinition searchResultDef;
	private String dateField;
	private String timeField;
	public PrCollectionDateTimeCellRenderer(String dateField,
			String timeField,
			SearchResultDefinition searchResultDef) {
		super();
		this.searchResultDef = searchResultDef;
		this.dateField = dateField;
		this.timeField = timeField; 
	}
	
	@Override
	public Cell renderIntoCell(FlexTable resultGrid, int row,
			ResultRecord record, CrudViewDefinition parentCrudViewDef,
			CrudViewDefinition sectionCrudViewDef) {
		Cell c = new Cell();
		String dateValue = CommonUtil.getReadableDate(searchResultDef.getValue(dateField, record));
		String timeValue = searchResultDef.getValue(timeField, record);
		c.addWidget(new Label(dateValue+" "+timeValue));
		return c;
	}
}