package ebilly.admin.client.view;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import ebilly.admin.client.layout.UiLayoutBase;

public class CrudEntityUiLayout extends UiLayoutBase {
	
	private CrudViewDefinition crudViewDef;
	private FlexTable flexTable;
	public CrudEntityUiLayout(CrudViewDefinition crudViewDef) {
		this.crudViewDef = crudViewDef;
		buildUi();
	}
	
	private void buildUi() {
		flexTable = new FlexTable();
		//contentPanel.add(flexTable);
		int rowCounter = 0;
		int colCounter = 0;
		int colOffset = 0;
		for (ViewField viewField : crudViewDef.getViewFields()) {
			
			if (viewField.isHidden || 
				(viewField.isHiddenOnEdit && !this.crudViewDef.isViewAction())) {
				//this.viewDef.fieldInput.put(viewField,viewField.getInputWidget());
				continue;
			}
			
			Widget widget = viewField.getInputWidget();
			widget = UiUtil.decorateWithHelpText(widget,viewField.helpText);
			boolean showCaption = true;
			if (viewField.getFieldType().equals(RoleSelectionPanel.class.getName())) {
				showCaption = false;
			}
			
			if (showCaption) {
				Label fieldLabel = (Label) viewField.getInputCaption();
				fieldLabel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
				flexTable.setWidget(rowCounter, colCounter+colOffset, fieldLabel);
				colOffset++;
			}
			
			flexTable.setWidget(rowCounter, colCounter+colOffset, widget);
			//this.viewDef.fieldInput.put(viewField,widget);
			if (!showCaption) {
				flexTable.getFlexCellFormatter().setColSpan(rowCounter, colCounter+colOffset, 2);
			}
			colOffset++;
			
			colCounter++;
			if (colCounter % 2 == 0) {
				colCounter = 0;
				colOffset = 0;
				rowCounter++;
			}
		}
		rowCounter++;
		
		/*
		VerticalPanel spacer = new VerticalPanel();
		spacer.setWidth("100%");
		spacer.setSpacing(40);
		contentPanel.add(spacer);
		sectionsPanel = new VerticalPanel();
		sectionsPanel.setHorizontalAlignment(VerticalPanel.ALIGN_LEFT);
		contentPanel.add(sectionsPanel);
		
		HorizontalPanel actionBar = createActionBar();
		if (!this.crudViewDef.hideCrudCommands && 
				!this.crudViewDef.isViewAction()) { 
				//this.viewDef.crudAction != CrudViewDefinition.CRUD_ACTION_VIEW) {
			flexTable.setWidget(rowCounter,0,actionBar);
			flexTable.getFlexCellFormatter().setColSpan(rowCounter, 0, 6);
		}*/
	}
	

	@Override
	public Widget getUiObject() {
		return flexTable;
	}

}
