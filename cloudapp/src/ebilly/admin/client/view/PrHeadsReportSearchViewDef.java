package ebilly.admin.client.view;

import ebilly.admin.shared.AppConfig;


public class PrHeadsReportSearchViewDef extends ExportRequestSearchViewDef {
	public PrHeadsReportSearchViewDef() {
		super();
	}
	
	protected String getObjectName() {
		return AppConfig.OBJ_PR_HEADS_REPORT;
	}
}
