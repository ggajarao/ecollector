package ebilly.admin.client.view;

import ebilly.admin.client.AppScreen;

public class LivePrAggregateSearchView extends SearchView implements AppScreen {

	public LivePrAggregateSearchView(String caption) {
		super(caption,true);
	}
	
	@Override
	protected SearchViewDef getSearchViewDefinition() {
		return new LivePrAggregateSearchViewDef();
	}
}
