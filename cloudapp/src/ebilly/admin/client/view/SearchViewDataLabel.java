package ebilly.admin.client.view;

import com.google.gwt.user.client.ui.Label;

public class SearchViewDataLabel extends Label {
	
	public SearchViewDataLabel() {
		super();
		this.setStyleName("search-view-data-label");
	}
	
	public SearchViewDataLabel(String text) {
		super(text, true);
		this.setStyleName("search-view-data-label");
	}
}

