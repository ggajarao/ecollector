package ebilly.admin.client.view;

import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Widget;

import ebilly.admin.client.Application;
import ebilly.admin.client.layout.UiLayout;
import ebilly.admin.client.menu.ActionListener;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.viewdef.CrudRequest;
import ebilly.admin.shared.viewdef.CrudResponse;
import ebilly.admin.shared.viewdef.SectionData;

public abstract class CrudView extends AppViewPanel {

	//private VerticalPanel sectionsPanel;
	private UiLayout crudSectionUiLayout;
	protected CrudViewDefinition viewDef = new CrudViewDefinition();
	private ActionListener successSaveListener;
	private Object crudEntity;
	private CrudView(String curdCaption, int crudAction) {
		super(curdCaption);
		viewDef.setCrudAction(crudAction);
		initViewFieldDefinition(viewDef);
		buildUi();
	}
	
	public CrudView(String crudCaption) {
		this(crudCaption, CrudViewDefinition.CRUD_ACTION_CREATE);
	}
	
	public CrudView(int crudAction, String crudCaption, final String recordId) {
		this(crudCaption,crudAction);
		Scheduler.get().scheduleDeferred(new ScheduledCommand() {
			@Override
			public void execute() {
				loadRecord(recordId);
			}
		});
	}
	
	public Object getCrudEntity() {
		return crudEntity;
	}
	
	protected abstract void initViewFieldDefinition(CrudViewDefinition viewDef);
	protected abstract String getCrudObjectName();
	
	private void buildUi() {
		//FlexTable flexTable = new FlexTable();
		Widget actionBar = createActionBar();
		contentPanel.add(actionBar);
		contentPanel.add(buildCrudUiLayout().getUiObject());
		contentPanel.setCellHorizontalAlignment(actionBar, HorizontalPanel.ALIGN_RIGHT);
		this.crudSectionUiLayout = buildSectionUiLayout();  
		if (this.crudSectionUiLayout != null) {
			contentPanel.add(this.crudSectionUiLayout.getUiObject());
		}
		
		/*int rowCounter = 0;
		int colCounter = 0;
		int colOffset = 0;
		for (ViewField viewField : viewDef.viewFields) {
			
			if (viewField.isHidden || 
				(viewField.isHiddenOnEdit && !this.viewDef.isViewAction())) {
				//this.viewDef.fieldInput.put(viewField,viewField.getInputWidget());
				continue;
			}
			
			Widget widget = viewField.getInputWidget();
			widget = UiUtil.decorateWithHelpText(widget,viewField.helpText);
			boolean showCaption = true;
			if (viewField.getFieldType().equals(RoleSelectionPanel.class.getName())) {
				showCaption = false;
			}
			
			if (showCaption) {
				Label fieldLabel = (Label) viewField.getInputCaption();
				fieldLabel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
				flexTable.setWidget(rowCounter, colCounter+colOffset, fieldLabel);
				colOffset++;
			}
			
			flexTable.setWidget(rowCounter, colCounter+colOffset, widget);
			//this.viewDef.fieldInput.put(viewField,widget);
			if (!showCaption) {
				flexTable.getFlexCellFormatter().setColSpan(rowCounter, colCounter+colOffset, 2);
			}
			colOffset++;
			
			colCounter++;
			if (colCounter % 2 == 0) {
				colCounter = 0;
				colOffset = 0;
				rowCounter++;
			}
		}
		rowCounter++;
		
		VerticalPanel spacer = new VerticalPanel();
		spacer.setWidth("100%");
		spacer.setSpacing(40);
		contentPanel.add(spacer);
		
		sectionsPanel = new VerticalPanel();
		sectionsPanel.setHorizontalAlignment(VerticalPanel.ALIGN_LEFT);
		contentPanel.add(sectionsPanel);
		
		HorizontalPanel actionBar = createActionBar();
		if (!this.viewDef.hideCrudCommands && 
				!this.viewDef.isViewAction()) { 
				//this.viewDef.crudAction != CrudViewDefinition.CRUD_ACTION_VIEW) {
			flexTable.setWidget(rowCounter,0,actionBar);
			flexTable.getFlexCellFormatter().setColSpan(rowCounter, 0, 6);
		}*/
	}
	
	protected UiLayout buildCrudUiLayout() {
		return new CrudEntityUiLayout(this.viewDef);
	}
	
	protected UiLayout buildSectionUiLayout() {
		return new CrudSectionUiLayout(this);
	}

	
	private HorizontalPanel createActionBar() {
		HorizontalPanel hp = new HorizontalPanel();
		hp.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		
		
		ActionBarWidget actionBar = new ActionBarWidget();
		hp.add(actionBar);
		hp.setSize("100%","100%");
		
		Button cancelButton = new Button("cancel");
		actionBar.add(cancelButton);
		cancelButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				//Application.instance.contentPanel.clear();
				Application.instance.showPrevious();
			}
		});
		
		Button saveButton = new Button("Save");
		actionBar.add(saveButton);
		saveButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				CrudView.this.requestToCreate();
			}
		});
		return hp;
	}

	private void loadRecord(String recordId) {
		UiUtil.loadRecord(recordId, 
				getCrudObjectName(), 
				this.viewDef, 
				new ActionListener() {
					@Override
					public void onBeforeAction(Object data) {
					}
					@Override
					public void onAfterAction(Object data) {
						CrudViewDefinition vd = (CrudViewDefinition)data;
						setSections(vd.getSectionDataList());
					}
				});
		
		/*showMessage("Loading...");
		CrudRequest crudRequest = new CrudRequest();
		crudRequest.crudObject = getCrudObjectName();
		crudRequest.crudAction = AppConfig.CRUD_ACTION_RETRIEVE;
		crudRequest.addFieldValue("id", recordId);
		for (ViewSectionDefinition viewSectionDef : this.viewDef.viewSectionDefinitions.values()) {
			crudRequest.addSectionResultColumns(viewSectionDef.sectionName, 
					viewSectionDef.searchViewDef.searchResultDef.getColumns());
		}
		Application.instance.secureService.processCrud(crudRequest, new AsyncCallback<CrudResponse>(){
			@Override
			public void onFailure(Throwable caught) {
				CrudView.this.showError(
						"Error while loading "+getCrudObjectName()+", " +
						"reason: "+caught.getMessage());
			}
			
			@Override
			public void onSuccess(CrudResponse result) {
				if (result.isSuccess()) {
					showMessage("");
					CrudView.this.viewDef.setCrudValues(
							result.fieldValueMap);
					crudEntity = result.crudEntity;
					setSections(result.sections);
					if (onLoadListener != null) {
						onLoadListener.onAfterAction(CrudView.this);
					}
				} else {
					showMessage(result.errorMessage);
				}
			}			
		});*/
	}
	
	private void setSections(List<SectionData> sections) {
		this.crudSectionUiLayout.bindData(sections);
		/*sectionsPanel.clear();
		
		for (ViewSectionDefinition sectionDef : this.viewDef.viewSectionDefinitions.values()) {
			for (SectionData section : sections) {
				sectionsPanel.add(
						sectionDef.getSectionViewFor(
								section));
			}
		}*/
	}
	
	protected void requestToCreate() {
		showMessage("Saving....");
		CrudRequest crudRequest = new CrudRequest();
		crudRequest.crudObject = getCrudObjectName();
		crudRequest.crudAction = AppConfig.CRUD_ACTION_CREATE_UPDATE;
		Map<String, String> fieldValueMap = null;
		try {
			fieldValueMap = this.viewDef.validateAndGetFieldValueMap();
		} catch (IllegalArgumentException e) {
			showError(e.getMessage());
			return;
		}
		
		crudRequest.setFieldValueMap(fieldValueMap);
		Application.instance.secureService.processCrud(crudRequest,
				new BaseServiceResponseHandler<CrudResponse>() {
					/*@Override
					public void onFailure(Throwable caught) {
						CrudView.this.showError(
								"Problems while saving, reason: "+caught.getMessage());
					}*/
					@Override
					public void onSuccess(CrudResponse result) {
						if (result == null) {
							CrudView.this.showError("Unexpected state!");
							return;
						}
						if (!CommonUtil.isEmpty(result.errorMessage)) {
							CrudView.this.showError(result.errorMessage);
						} else {
							CrudView.this.showMessage(result.successMessage);
							CrudView.this.onComplete(result);
						}
					}
				});
	}
	
	public void setSuccessSaveListener(ActionListener successSaveListener) {
		this.successSaveListener = successSaveListener;
	}
	
	private void onComplete(CrudResponse result) {
		if (this.successSaveListener != null) {
			this.successSaveListener.onAfterAction(result);
		} else {
			Application.instance.showPrevious();
		}
	}
	
	public CrudViewDefinition getViewDef() {
		return this.viewDef;
	}
	
	private ActionListener onLoadListener;
	public void setOnLoadListener(ActionListener actionListener) {
		this.onLoadListener = actionListener;
	}

	@Override
	public Widget getUiObject() {
		return this.panel;
	}
}


