package ebilly.admin.client.view;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Widget;

public class ActionBarWidget extends Composite {
	private FlexTable ft;
	private int column = 0;
	public ActionBarWidget() {
		buildUi();
		initWidget(this.ft);
	}
	
	private void buildUi() {
		this.ft = new FlexTable();
		this.ft.setCellPadding(5);
	}
	
	public void setCellPadding(int padding) {
		this.ft.setCellPadding(padding);
	}
	
	public void add(Widget widget) {
		this.ft.setWidget(0, this.column++, widget);
	}
	
	public void clear() {
		this.column = 0;
		this.ft.clear();
	}
	
	public void copyFrom(ActionBarWidget from) {
		if (from == null) return;
		this.clear();
		for (int column = 0; column < from.column; column++  ) {
			this.add(from.ft.getWidget(0, column));
		}
	}
}