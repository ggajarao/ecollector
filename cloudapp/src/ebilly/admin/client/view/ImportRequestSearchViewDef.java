package ebilly.admin.client.view;

import ebilly.admin.shared.AppConfig;


public class ImportRequestSearchViewDef extends SearchViewDef {
	public ImportRequestSearchViewDef() {
		super();
	}
	
	@Override
	protected void initSearchFilterDef(SearchFilterDefinition searchFilterDef) {
		//searchFilterDef.add(new ViewField("hallticketNo",java.lang.String.class.getName(),"Hall Ticket Number"));
	}
	
	@Override
	protected void initSearchResultDef(SearchResultDefinition searchResultDef) {
		ResultColumn c = new ResultColumn("id",java.lang.String.class.getName(),"id");
		c.isHidden = true;
		searchResultDef.add(c);
		c = new ResultColumn("staticValue1",java.lang.String.class.getName(),"sv1");
		c.isHidden = true;
		searchResultDef.add(c);
		c = new ResultColumn("staticValue2",java.lang.String.class.getName(),"sv2");
		c.isHidden = true;
		searchResultDef.add(c);
		c = new ResultColumn("staticValue3",java.lang.String.class.getName(),"sv3");
		c.isHidden = true;
		searchResultDef.add(c);
		c = new ResultColumn("importResultFile",java.lang.String.class.getName(),"resultFile");
		c.isHidden = true;
		searchResultDef.add(c);
		c = new ResultColumn("importErrorMessage",java.lang.String.class.getName(),"errorMessage");
		c.isHidden = true;
		searchResultDef.add(c);
		c = new ResultColumn("importSummary",java.lang.String.class.getName(),"importSummary");
		c.isHidden = true;
		searchResultDef.add(c);
		c = new ResultColumn("fileName",java.lang.String.class.getName(),"File Name");
		c.isHidden = true;
		searchResultDef.add(c);
		
		c = new ResultColumn("fileContent",java.lang.String.class.getName(),"File");
		c.setCellRenderer(new ViewFileCellRenderer("fileContent", "fileName", this.searchResultDef));
		searchResultDef.add(c);
		
		c = new ResultColumn("objectType",java.lang.String.class.getName(),"Import Into");
		searchResultDef.add(c);
		
		searchResultDef.add(new ResultColumn("createdDate",java.lang.String.class.getName(),"Requested Date"));
		
		c = new ResultColumn("status",java.lang.String.class.getName(),"Status");
		c.setCellRenderer(new ImportDataStatusCellRenderer("status",
				"id",this.searchResultDef));
		searchResultDef.add(c);
		
		c = new ResultColumn("Result",java.lang.String.class.getName(),"Result");
		c.setCellRenderer(new ImportResultCellRenderer("status",
				"importResultFile","importErrorMessage", "importSummary", this.searchResultDef));
		searchResultDef.add(c);
		
		/*// The order of this field should always be at the end.
		c = new ResultColumn("nextInQueue",java.lang.String.class.getName(),"");
		c.isHidden = true;
		searchResultDef.add(c);*/
	}

	@Override
	public String getSearchObjectName() {
		return AppConfig.OBJ_IMPORT_REQUEST;
	}
}
