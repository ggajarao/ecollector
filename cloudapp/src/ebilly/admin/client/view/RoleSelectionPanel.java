package ebilly.admin.client.view;

import ebilly.admin.client.Application;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.RoleUi;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;

public class RoleSelectionPanel extends Composite {
	
	private Grid panel;
	private ListBox roleList;
	private String caption = "User Role";
	private String value;
	private DataLabel valueLabel;
	private boolean isEnabled;
	//private RoleUi[] roles;
	
	public RoleSelectionPanel(String caption) {
		if (!CommonUtil.isEmpty(caption)) {
			this.caption = caption;
		}
		buildUi();
		initWidget(panel);		
	}
	
	private RoleUi[] getRoles() {
		if (Application.instance.getUserRoleCode().equals(AppConfig.ROLE_CODE_APP_ADMIN)) {
			return Application.instance.fetchAppAdminRoles();
		} else if (Application.instance.getUserRoleCode().equals(AppConfig.ROLE_CODE_SUPER_ADMIN)) {
			return Application.instance.fetchSuperAdminRoles();
		}
		return new RoleUi[]{};
	}
		
	private void buildUi() {
		panel = new Grid(1,3);
		panel.setWidth("100%");
		
		int column = 0;
		
		Label monthLabel = new Label(this.caption);
		monthLabel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		panel.setWidget(0, column, monthLabel);
		column++;
		
		this.roleList = new ListBox();
		this.roleList.addItem("Select One","");
		for(RoleUi role : this.getRoles()) {
			this.roleList.addItem(role.getRoleName(), role.getId());
		}
		panel.setWidget(0, column, roleList);
		
		this.valueLabel = new DataLabel();
	}

	public void reset() {
		this.roleList.setSelectedIndex(0);
	}
	
	public void setValue(String value) {
		if (CommonUtil.isEmpty(value)) return;
		this.value = value;
		if (!this.isEnabled) {
			for (RoleUi r : this.getRoles()) {
				if (r.getId().equals(value)) {
					this.valueLabel.setText(r.getRoleName());
				}
			}
		} else {
			for (int i = 0; i < this.roleList.getItemCount(); i++) {
				if (value.equals(this.roleList.getValue(i))) {
					this.roleList.setSelectedIndex(i);
					return;
				}
			}
			this.roleList.setSelectedIndex(0);
		}
	}
	
	public void setEnabled(boolean enabled) {
		this.isEnabled = enabled;
		if (!this.isEnabled) {
			this.panel.clear();
			this.panel.setWidget(0, 0, new Label(this.caption));
			this.panel.setWidget(0, 1, this.valueLabel);
		}
	}
	
	public String getRoleId() {
		if (!this.isEnabled) return this.value;
		return this.roleList.getValue(this.roleList.getSelectedIndex());
	}
}
