package ebilly.admin.client.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.viewdef.ResultRecord;
import ebilly.admin.shared.viewdef.SearchRequest;

public class SearchResultDefinition extends ArrayList<ResultColumn> 
implements Serializable, IsSerializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SearchResultDefinition() {
		super();
	}
	
	public boolean add(ResultColumn resultColumn) {
		resultColumn.setSearchResultDefinition(this);
		return super.add(resultColumn);
	}
	
	public List<String> getColumns() {
		List<String> results = new ArrayList<String>();
		for (ResultColumn resultColum : this) {
			results.add(resultColum.fieldName);
		}
		return results;
	}
	
	public String getValue(String fieldName, ResultRecord record) {
		
		if (CommonUtil.isEmpty(fieldName)) return "";
		if (record.fieldValues == null || record.fieldValues.isEmpty() 
				|| record.fieldValues.size() != this.size() ) {
			return "";
		}
		
		for (int i = 0; i < this.size(); i++) {
			ResultColumn column = this.get(i);
			if (fieldName.equals(column.fieldName)) {
				return record.fieldValues.get(i);
			}
		}
		return "";
	}
	
	/**
	 * Orders the fieldValues in the order of the columns defined in resultDef.
	 * 
	 * This method is helpful to access fieldValues in the field order specified
	 * in resultDef, otherwise the fieldValues are in the order specified in
	 * sourceRequest. 
	 * 
	 * @param sourceRequest
	 * @param resultDef
	 */
	public void orderValuesPerResultDef(SearchRequest sourceRequest, ResultRecord record) {
		SearchResultDefinition resultDef = this;
		List<String> orderedValues = new ArrayList<String>();
		for (ResultColumn resultColName : resultDef) {
			orderedValues.add(sourceRequest.getValue(resultColName.fieldName,record));
		}
		record.fieldValues = orderedValues;
	}
}
