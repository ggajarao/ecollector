package ebilly.admin.client.view;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlexTable.FlexCellFormatter;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import ebilly.admin.client.AppScreen;
import ebilly.admin.client.Application;
import ebilly.admin.client.menu.UserEditAction;
import ebilly.admin.client.menu.UserResetPasswordAction;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.viewdef.CrudResponse;
import ebilly.admin.shared.viewdef.ResultRecord;

public class UserSearchView extends SearchView implements AppScreen {

	public UserSearchView(String caption) {
		super(caption);
	}
	
	@Override
	protected SearchViewDef getSearchViewDefinition() {
		return new UserSearchViewDef();
	}
}

class UserSearchViewDef extends SearchViewDef {
	public UserSearchViewDef() {
		super();
	}
	
	@Override
	protected void initSearchFilterDef(SearchFilterDefinition searchFilterDef) {
		searchFilterDef.add(new ViewField("loginId",java.lang.String.class.getName(),"User Login (case sensitive eg: 'Ramu' is not same as 'ramu')"));
		searchFilterDef.add(new ViewField("roleId",RoleSelectionPanel.class.getName(),"User Role"));
	}
	
	@Override
	protected void initSearchResultDef(SearchResultDefinition searchResultDef) {
		ResultColumn c = new ResultColumn("id",java.lang.String.class.getName(),"id");
		c.isHidden = true;
		searchResultDef.add(c);
		
		c = new ResultColumn("studentId",java.lang.String.class.getName(),"studentId");
		c.isHidden = true;
		searchResultDef.add(c);
		
		c = new ResultColumn("collegeId",java.lang.String.class.getName(),"collegeId");
		c.isHidden = true;
		searchResultDef.add(c);
		
		c = new ResultColumn("loginId",java.lang.String.class.getName(),"User Login");
		c.setCellRenderer(new UserEditCellRenderer("id","loginId",searchResultDef));
		searchResultDef.add(c);
		
		searchResultDef.add(new ResultColumn("email",java.lang.String.class.getName(),"Email Address"));
		
		c = new ResultColumn("Role",java.lang.String.class.getName(),"Role");
		searchResultDef.add(c);
		
		c = new ResultColumn("Action",java.lang.String.class.getName(),"Action");
		c.setCellRenderer(new UserActionsCellRenderer("id",searchResultDef));
		searchResultDef.add(c);
	}

	@Override
	public String getSearchObjectName() {
		return AppConfig.OBJ_USER;
	}
}

class UserEditCellRenderer implements CellRenderer {

	SearchResultDefinition searchResultDef;
	private String userIdField;
	private String labelField;
	public UserEditCellRenderer(String userIdField, String labelField, 
			SearchResultDefinition searchResultDef) {
		super();
		this.labelField = labelField;
		this.searchResultDef = searchResultDef;
		this.userIdField = userIdField;
	}
	
	@Override
	public Cell renderIntoCell(final FlexTable resultGrid, int row,
			ResultRecord record, CrudViewDefinition parentCrudViewDef,
			CrudViewDefinition sectionCrudViewDef) {
		final Cell c = new Cell();
		final String userId = searchResultDef.getValue(userIdField, record);
		Anchor anchor = new Anchor(searchResultDef.getValue(labelField, record));
		anchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				UserEditAction action = 
					new UserEditAction(userId);
				action.perform();
			}
		});
		c.addWidget(anchor);
		return c;
	}
}

class UserActionsCellRenderer implements CellRenderer {

	SearchResultDefinition searchResultDef;
	private String userIdField;
	private String labelField;
	public UserActionsCellRenderer(String userIdField, 
			SearchResultDefinition searchResultDef) {
		super();
		this.searchResultDef = searchResultDef;
		this.userIdField = userIdField;
	}
	
	@Override
	public Cell renderIntoCell(FlexTable resultGrid, int row,
			ResultRecord record, CrudViewDefinition parentCrudViewDef,
			CrudViewDefinition sectionCrudViewDef) {
		final Cell c = new Cell();
		final String userId = searchResultDef.getValue(userIdField, record);
		
		Anchor anchor = null;
		
		String loggedInUserId = Application.instance.getUserId();
		if (loggedInUserId != null && !loggedInUserId.equals(userId)) {
			anchor = new Anchor("Delete");
			anchor.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					UserDeleteConfirmationDialog d = 
						new UserDeleteConfirmationDialog(userId,c);
				}
			});
			c.addWidget(anchor);
		}
		anchor = new Anchor("Reset Password");
		anchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				UserResetPasswordAction a = new UserResetPasswordAction(userId);
				a.perform();
			}
		});
		c.addWidget(anchor);
		
		return c;
	}
	
	private void showResetPasswordDialog(String userId) {
		UserPasswordResetView v = new UserPasswordResetView(userId);
		final DialogBox dialog = new DialogBox();
		
		Button closeButton = new Button("Close");
		v.addActionButton(closeButton);
		closeButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				dialog.hide();
			}
		});
		
		dialog.setSize("800px", "100%");
		dialog.setGlassEnabled(true);
		dialog.setAnimationEnabled(true);
		dialog.setText("Confirm");
		dialog.setWidget(v.getUiObject());
		dialog.center();
		dialog.setAutoHideEnabled(true);
		dialog.show();
	}
}

class UserDeleteConfirmationDialog {
	
	private DialogBox dialog;
	private String userId;
	private Cell cell;
	private Widget widget;
	public UserDeleteConfirmationDialog(String userId,
			Cell cell) {
		this.userId = userId;
		this.widget = widget;
		this.cell = cell;
		buildUi();
	}
	
	protected void buildUi() {
		dialog = new DialogBox();
		
		FlexTable ft = new FlexTable();
		ft.setCellSpacing(6);
		
		FlexCellFormatter cellFormatter = ft.getFlexCellFormatter();
		cellFormatter.setColSpan(0,0,2);
		ft.setWidget(0, 0, new Label("Are you sure you want to delete the user ?"));
		
		Button noButton = new Button("No");
		Button yesButton = new Button("Yes");
		ActionBarWidget actionBar = new ActionBarWidget();
		actionBar.add(noButton);
		actionBar.add(yesButton);
		
		noButton.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				dialog.hide();
			}
		});
		
		yesButton.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				Application.instance.secureService.deleteUser(userId, 
					new BaseServiceResponseHandler<CrudResponse>(){
						/*@Override
						public void onFailure(Throwable caught) {
							Application.instance.showMessage("Error while deleting user");
						}*/
						public void onSuccess(CrudResponse result) {
							dialog.hide();
							cell.clear();
							cell.addWidget(new Label("Deleted"));
						};
					}
				);
			}
		});
		
		cellFormatter.setColSpan(1, 0, 2);
		cellFormatter.setAlignment(1, 0, HorizontalPanel.ALIGN_CENTER, VerticalPanel.ALIGN_MIDDLE);
		ft.setWidget(1, 0, actionBar);
		
		dialog.setAnimationEnabled(true);
		dialog.setText("Confirm");
		dialog.setWidget(ft);
		dialog.center();
		dialog.setAutoHideEnabled(true);
		dialog.show();		
	}
}
