package ebilly.admin.client.view;

import java.util.HashMap;
import java.util.Map;

import ebilly.admin.client.Application;
import ebilly.admin.client.menu.ActionListener;
import ebilly.admin.shared.CollectionDashboardFilterSelection;
import ebilly.admin.shared.SummarySearchRequestFactory;
import ebilly.admin.shared.viewdef.SearchRequest;
import ebilly.admin.shared.viewdef.SearchResult;

public class CollectionDashboardDataSource {
	private Map<String, SearchResult> orgDimResultMap = 
		new HashMap<String, SearchResult>();
	public CollectionDashboardDataSource() {
		
	}
	
	public void fetchOrgDimResult(CollectionDashboardFilterSelection selection, 
			ActionListener onResultsAvailable) {
		String selectedFilter = selection.getSelectedFilter();
		SearchResult result = orgDimResultMap.get(selectedFilter);
		if (result != null) {
			onResultsAvailable.onAfterAction(result);
		} else {
			loadDimResult(selection, onResultsAvailable);
		}
	}
	
	private void loadDimResult(
			final CollectionDashboardFilterSelection selection, 
			final ActionListener onResultsAvailable) {
		SearchRequest request = null;
		SummarySearchRequestFactory requestFactory = null;
		switch(selection.getSelectedOrgType()) {
		case CollectionDashboardFilterSelection.COMPANY:
			requestFactory = SummarySearchRequestFactory.companySearchRequestFactory;
			break;
		case CollectionDashboardFilterSelection.CIRCLE:
			requestFactory = SummarySearchRequestFactory.circleSearchRequestFactory;
			break;
		case CollectionDashboardFilterSelection.DIVISION:
			requestFactory = SummarySearchRequestFactory.divisionSearchRequestFactory;
			break;
		case CollectionDashboardFilterSelection.ERO:
			requestFactory = SummarySearchRequestFactory.eroSearchRequestFactory;
			break;
		case CollectionDashboardFilterSelection.SUBDIVISION:
			requestFactory = SummarySearchRequestFactory.subdivisionSearchRequestFactory;
			break;
		case CollectionDashboardFilterSelection.SECTION:
			requestFactory = SummarySearchRequestFactory.sectionSearchRequestFactory;
			break;
		case CollectionDashboardFilterSelection.DISTRIBUTION:
			requestFactory = SummarySearchRequestFactory.distributionSearchRequestFactory;
			break;		
		}
		request = requestFactory.createSearchRequest(selection.getSelectedOrgValue(), 
				selection.getSelectedSummaryDataKey());
		
		Application.secureService.searchView(request, new BaseServiceResponseHandler<SearchResult>() {
			@Override
			public void onSuccess(SearchResult result) {
				orgDimResultMap.put(selection.getSelectedFilter(), result);
				onResultsAvailable.onAfterAction(result);
			}
		} );
	}
}
