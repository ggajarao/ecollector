package ebilly.admin.client.view;

import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;

import ebilly.admin.shared.AppConfig.IMPORT_STATUS;
import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.viewdef.ResultRecord;

public class ImportResultCellRenderer implements CellRenderer {

	SearchResultDefinition searchResultDef;
	private String statusFieldName;
	private String importResultFile;
	private String importErrorMessage;
	private String importSummary;
	public ImportResultCellRenderer(String statusFieldName,
			String importResultFile, String importErrorMessage,
			String importSummaryField,
			SearchResultDefinition searchResultDef) {
		super();
		this.searchResultDef = searchResultDef;
		this.statusFieldName = statusFieldName;
		this.importResultFile = importResultFile;
		this.importErrorMessage = importErrorMessage;
		this.importSummary = importSummaryField;
	}

	@Override
	public Cell renderIntoCell(FlexTable resultGrid, int row,
			ResultRecord record, CrudViewDefinition parentCrudViewDef,
			CrudViewDefinition sectionCrudViewDef) {
		Cell c = new Cell();
		String displayStatus = searchResultDef.getValue(statusFieldName, record);
		Widget w = null;
		if (displayStatus == null) {
			w = new Label("");
		} else if (displayStatus.equalsIgnoreCase(
				IMPORT_STATUS.COMPLETED.toString()) ||
				displayStatus.equalsIgnoreCase(
						IMPORT_STATUS.FAILED.toString())) {
			w = new ActionBarWidget();
			((ActionBarWidget)w).setCellPadding(2);

			// Error message popup panel
			final String errorMessage = 
				searchResultDef.getValue(this.importErrorMessage,record);
			if (! CommonUtil.isEmpty(errorMessage)) {
				final Anchor msgAnchor = new Anchor("Error");
				final PopupPanel pp = new PopupPanel(/*auto hide*/true);
				pp.setWidget(new Label(errorMessage));
				msgAnchor.addMouseOverHandler(new MouseOverHandler(){
					public void onMouseOver(MouseOverEvent event) {
						pp.showRelativeTo(msgAnchor);
					}
				});
				msgAnchor.addMouseOutHandler(new MouseOutHandler(){
					public void onMouseOut(MouseOutEvent event) {
						pp.hide();
					}
				});
				((ActionBarWidget)w).add(msgAnchor);
			} else {
				// show summary in a popup panel
				final String importSummary = 
					searchResultDef.getValue(this.importSummary,record);
				if (!CommonUtil.isEmpty(importSummary)) {
					final Anchor msgAnchor = new Anchor("Summary");
					final PopupPanel pp = new PopupPanel(/*auto hide*/true);
					
					HorizontalPanel hp = new HorizontalPanel();
					hp.add(new DataLabel(importSummary));
					hp.setSpacing(10);
					pp.setWidget(hp);
					pp.setAnimationEnabled(true);
					msgAnchor.addMouseOverHandler(new MouseOverHandler(){
						public void onMouseOver(MouseOverEvent event) {
							pp.showRelativeTo(msgAnchor);
						}});
					msgAnchor.addMouseOutHandler(new MouseOutHandler(){
						public void onMouseOut(MouseOutEvent event) {
							pp.hide();
						}
					});
					((ActionBarWidget)w).add(msgAnchor);
				}
			}
			// Result file download link
			String resultFile = 
				searchResultDef.getValue(this.importResultFile,record);
			if (! CommonUtil.isEmpty(resultFile)) {
				Anchor resultFileAnchor = new Anchor("Result File");
				resultFileAnchor.setHref(CommonUtil.getDownloadUrl(resultFile));
				resultFileAnchor.setTarget("-");
				((ActionBarWidget)w).add(resultFileAnchor);
			}
		} else {
			w = new Label("--");
		}

		c.addWidget(w);
		return c;
	}
}
