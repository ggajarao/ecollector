package ebilly.admin.client.view;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import ebilly.admin.shared.viewdef.ResultRecord;

public class SimpleCellRenderer implements CellRenderer {
	private String fieldName;
	private SearchResultDefinition searchResultDef;
	public SimpleCellRenderer(String fieldName,
			SearchResultDefinition searchResultDef) {
		this.fieldName = fieldName;
		this.searchResultDef = searchResultDef;
	}
	
	@Override
	public Cell renderIntoCell(FlexTable resultGrid, int row,
			ResultRecord record, CrudViewDefinition parentCrudViewDef,
			CrudViewDefinition sectionCrudViewDef) {
		Cell c = new Cell();
		Label l = new SearchViewDataLabel(searchResultDef.getValue(this.fieldName, record));
		c.addWidget(l);
		return c;
	}
}