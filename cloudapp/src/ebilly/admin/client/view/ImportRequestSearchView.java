package ebilly.admin.client.view;

import ebilly.admin.client.AppScreen;

public class ImportRequestSearchView extends SearchView implements AppScreen {

	public ImportRequestSearchView(String caption) {
		super(caption);
	}
	
	@Override
	protected SearchViewDef getSearchViewDefinition() {
		return new ImportRequestSearchViewDef();
	}
}
