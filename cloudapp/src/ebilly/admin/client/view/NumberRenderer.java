package ebilly.admin.client.view;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;

import ebilly.admin.shared.viewdef.ResultRecord;

public class NumberRenderer implements CellRenderer {
	SearchResultDefinition searchResultDef;
	private String amountField;
	public NumberRenderer(String amountField,
			SearchResultDefinition searchResultDef) {
		super();
		this.searchResultDef = searchResultDef;
		this.amountField = amountField; 
	}
	
	@Override
	public Cell renderIntoCell(FlexTable resultGrid, int row,
			ResultRecord record, CrudViewDefinition parentCrudViewDef,
			CrudViewDefinition sectionCrudViewDef) {
		Cell c = new Cell();
		Label amountValue = new Label(searchResultDef.getValue(amountField, record));
		amountValue.setHorizontalAlignment(HorizontalPanel.ALIGN_RIGHT);
		c.addWidget(amountValue);
		return c;
	}
}
