package ebilly.admin.client.view;

import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import ebilly.admin.client.AppScreen;
import ebilly.admin.client.Application;

public abstract class AppViewPanel implements AppScreen {
	protected final Panel panel;
	protected Label messageLabel;
	protected Label errorMessageLabel;
	protected CrudViewDefinition viewDef = new CrudViewDefinition();	
	protected Label captionLabel;
	private String caption;
	protected VerticalPanel contentPanel;
	protected ActionBarWidget actionBarWidget;
	private VerticalPanel contentIndenter;
	
	protected AppViewPanel() {
		this.panel = new HTMLPanel("");
		this.panel.setStyleName("AppViewPanel");
	}
	
	public AppViewPanel(String caption) {
		this.caption = caption;
		this.panel = new VerticalPanel();
		buildUi();
	}
	
	private void buildUi() {
		//this.panel = new VerticalPanel();
		panel.setSize("100%", "");
		panel.setStyleName("AppViewPanel");
		
		VerticalPanel headerPanel = new VerticalPanel();
		panel.add(headerPanel);
		headerPanel.setWidth("100%");
		
		VerticalPanel captionPanel = new VerticalPanel();
		headerPanel.add(captionPanel);
		captionPanel.setWidth("100%");
		
		captionLabel = new Label(this.caption);
		captionLabel.setStyleName("title-panel");
		captionPanel.add(captionLabel);
		captionLabel.setWidth("100%");
		
		VerticalPanel messagePanel = new VerticalPanel();
		headerPanel.add(messagePanel);
		messagePanel.setWidth("100%");
		
		messageLabel = new Label("");
		messagePanel.add(messageLabel);
		messageLabel.setWidth("100%");
		
		errorMessageLabel = new Label("");
		errorMessageLabel.setStyleName("error-label");
		messagePanel.add(errorMessageLabel);
		errorMessageLabel.setWidth("100%");
		
		contentPanel = new VerticalPanel();
		contentPanel.setWidth("100%");
		
		actionBarWidget = new ActionBarWidget();
		
		VerticalPanel contentWrapper = new VerticalPanel();
		contentWrapper.setWidth("100%");
		contentWrapper.add(actionBarWidget);
		contentWrapper.setCellHorizontalAlignment(actionBarWidget, HorizontalPanel.ALIGN_RIGHT);
		contentWrapper.add(contentPanel);
		
		// This is the indenter to the content panel
		contentIndenter = new VerticalPanel();
		contentIndenter.setSpacing(20);
		contentIndenter.setWidth("100%");
		contentIndenter.add(contentWrapper);
		panel.add(contentIndenter);
		contentPanel.setSize("100%", "100%");
	}
	
	protected void setContentPanelBorderSpace(int space) {
		this.contentIndenter.setSpacing(space);
	}

	protected void showError(String message) {
		/*this.errorMessageLabel.getElement().setInnerHTML(message);
		this.messageLabel.getElement().setInnerHTML("");*/
		Application.showErrorMessage(message);
	}
	
	protected void showMessage(String message) {
		Application.showMessage(message);
		/*this.errorMessageLabel.getElement().setInnerHTML("");
		this.messageLabel.getElement().setInnerHTML(message);*/
	}

	@Override
	public Widget getUiObject() {
		return this.panel;
	}
	
	
	public DialogBox prepareContentAsDialog(String dialogCaption) {
		DialogBox d = UiUtil.createDialog(this.getUiObject());
		d.setText(dialogCaption);
		return d;
	}
	
	public void onBeforeClose() {
		
	}
	
	public void onBeforeOpen() {
		
	}
}


