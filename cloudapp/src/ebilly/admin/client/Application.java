package ebilly.admin.client;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import ebilly.admin.client.menu.CollectionDashboardAction;
import ebilly.admin.client.menu.DeviceCreateAction;
import ebilly.admin.client.menu.DeviceSearchAction;
import ebilly.admin.client.menu.LivePrAggregateSearchAction;
import ebilly.admin.client.menu.LivePrSearchAction;
import ebilly.admin.client.menu.PrAggregateSearchAction;
import ebilly.admin.client.menu.PrDownloadAction;
import ebilly.admin.client.menu.PrHeadsDownloadAction;
import ebilly.admin.client.menu.PrSearchAction;
import ebilly.admin.client.menu.UserCreateAction;
import ebilly.admin.client.menu.UserSearchAction;
import ebilly.admin.client.menu.XAppMenu;
import ebilly.admin.client.menu.XMenu;
import ebilly.admin.client.menu.XMenuGroup;
import ebilly.admin.client.menu.XMenuItem;
import ebilly.admin.client.view.UserPasswordResetView;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.UserContext;


public class Application extends ApplicationBase {
	
	protected final DockPanel appPanel = new DockPanel();
	protected HorizontalPanel headerPanel = null;
	protected final VerticalPanel menuePanel = new VerticalPanel();
	protected final HorizontalPanel contentPanel = new HorizontalPanel();
	protected HorizontalPanel userContextPanel = null;
	protected Panel headerCenter = null;
	
	private Application() {
		super();
		buildLayoutPanel();
	}
	
	static Application createInstance() {
		return new Application();
	}
	
	private void buildLayoutPanel() {
		appPanel.setSize("100%", "100%");

		headerPanel = new HorizontalPanel();
		headerPanel.setStyleName("application-header");
		headerPanel.setWidth("100%");
		//headerPanel.setHeight("100px");
		headerPanel.setHorizontalAlignment(HorizontalPanel.ALIGN_LEFT);
		
		
		headerCenter = new VerticalPanel();
		headerPanel.add(headerCenter);
		headerPanel.setCellWidth(headerCenter, "50%");
		
		userContextPanel = new HorizontalPanel();
		//userContextPanel.setSize("100%", "100%");
		userContextPanel.setSpacing(2);
		headerPanel.add(userContextPanel);
		headerPanel.setCellHorizontalAlignment(userContextPanel, HorizontalPanel.ALIGN_RIGHT);
		
		appPanel.add(headerPanel,DockPanel.NORTH);
		//appPanel.setCellHeight(headerPanel, "20px");
		appPanel.add(contentPanel,DockPanel.CENTER);
		appPanel.setCellHeight(contentPanel, "100%");
		appPanel.setCellWidth(contentPanel, "100%");
		contentPanel.setSize("100%", "100%");
		contentPanel.setStyleName("panel");
		menuePanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		
		appPanel.add(menuePanel, DockPanel.WEST);
		menuePanel.setSize("100%", "");
		appPanel.setCellWidth(menuePanel, "15%");
		
		headerPanel.add(messageLabel);
	}
	
	@Override
	protected Panel getAppPanel() {
		return appPanel;
	}
	
	@Override
	protected void setMenueState(UserContext userContext) {
    	menuePanel.clear();
    	/*AppMenue appMenue = new AppMenue();
    	menuePanel.add(appMenue);
    	appMenue.applyUserContext(userContext);*/
    	XAppMenu appMenu = new XAppMenu();
    	menuePanel.add(appMenu);
    	applyUserContext(userContext, appMenu);
    }
	
	private void applyUserContext(UserContext userContext, XMenu appMenu) {
		appMenu.clear();
		if (userContext == null) return;
		List<XMenuGroup> menuGroups = new ArrayList<XMenuGroup>();
		
		XMenuGroup g = null;
		
		g = new XMenuGroup(appMenu, "");
		g.add(new XMenuItem("Home",new CollectionDashboardAction()));
		menuGroups.add(g);
		
		if (AppConfig.ROLE_CODE_SUPER_ADMIN.equals(userContext.getRoleCode())) {
			/*g = new XMenuGroup("Import");
			menuGroups.add(g);
			g.add(new XMenuItem("Import Data",new DataUploadAction()));
			g.add(new XMenuItem("Import Requests",new ImportRequestSearchAction()));*/
			
			g = new XMenuGroup(appMenu, "Devices");
			g.add(new XMenuItem("Device Search",new DeviceSearchAction()));
			g.add(new XMenuItem("New Device",new DeviceCreateAction()));
			menuGroups.add(g);
			
			g = new XMenuGroup(appMenu,"Live");
			g.add(new XMenuItem("Abstract",new LivePrAggregateSearchAction()));
			g.add(new XMenuItem("Collections",new LivePrSearchAction()));
			
			menuGroups.add(g);
			
			g = new XMenuGroup(appMenu,"Reports");
			g.add(new XMenuItem("Abstract",new PrAggregateSearchAction()));
			g.add(new XMenuItem("Collections",new PrSearchAction()));
			g.add(new XMenuItem("PR Download",new PrDownloadAction()));
			g.add(new XMenuItem("PR Heads Download",new PrHeadsDownloadAction()));
			
			menuGroups.add(g);
			
			g = new XMenuGroup(appMenu,"Manage Users");
			menuGroups.add(g);
			g.add(new XMenuItem("User Search",new UserSearchAction()));
			g.add(new XMenuItem("Create User",new UserCreateAction()));
			
		} else if (AppConfig.ROLE_CODE_APP_ADMIN.equals(userContext.getRoleCode())) {
			
			//XMenuGroup g = null;
			
			g = new XMenuGroup(appMenu,"Devices");
			g.add(new XMenuItem("Device Search",new DeviceSearchAction()));
			/*g.add(new XMenuItem("New Device",new DeviceCreateAction()));*/
			menuGroups.add(g);
			
			g = new XMenuGroup(appMenu,"Live");
			g.add(new XMenuItem("Abstract",new LivePrAggregateSearchAction()));
			g.add(new XMenuItem("Collections",new LivePrSearchAction()));
			
			menuGroups.add(g);
			
			g = new XMenuGroup(appMenu,"Reports");
			g.add(new XMenuItem("Abstract",new PrAggregateSearchAction()));
			g.add(new XMenuItem("Collections",new PrSearchAction()));
			g.add(new XMenuItem("PR Download",new PrDownloadAction()));
			g.add(new XMenuItem("PR Heads Download",new PrHeadsDownloadAction()));
			menuGroups.add(g);
			
			g = new XMenuGroup(appMenu,"Manage Users");
			menuGroups.add(g);
			g.add(new XMenuItem("User Search",new UserSearchAction()));
			g.add(new XMenuItem("Create User",new UserCreateAction()));
			
		} else if (AppConfig.ROLE_CODE_AAO_USER.equals(userContext.getRoleCode())) {
			
			//XMenuGroup g = null;
			
			g = new XMenuGroup(appMenu,"Devices");
			g.add(new XMenuItem("Device Search",new DeviceSearchAction()));
			/*g.add(new XMenuItem("New Device",new DeviceCreateAction()));*/
			menuGroups.add(g);
			
			g = new XMenuGroup(appMenu,"Live");
			g.add(new XMenuItem("Abstract",new LivePrAggregateSearchAction()));
			g.add(new XMenuItem("Collections",new LivePrSearchAction()));
			
			menuGroups.add(g);
			
			g = new XMenuGroup(appMenu,"Reports");
			g.add(new XMenuItem("Abstract",new PrAggregateSearchAction()));
			g.add(new XMenuItem("Collections",new PrSearchAction()));
			g.add(new XMenuItem("PR Download",new PrDownloadAction()));
			g.add(new XMenuItem("PR Heads Download",new PrHeadsDownloadAction()));			
			menuGroups.add(g);
			
		} else if (AppConfig.ROLE_CODE_CASHIER_USER.equals(userContext.getRoleCode())) {
			
			//XMenuGroup g = null;
			
			g = new XMenuGroup(appMenu,"Reports");
			g.add(new XMenuItem("Abstract",new PrAggregateSearchAction()));
			g.add(new XMenuItem("Collections",new PrSearchAction()));
			g.add(new XMenuItem("PR Heads Download",new PrHeadsDownloadAction()));
			//g.add(new XMenuItem("PR Download",new PrDownloadAction()));
			
			menuGroups.add(g);
		}
		/*for(XMenuGroup a : menuGroups) {
			appMenu.add(a);    		
    	}*/
	}
	
	@Override
	public void freezApplication() {
		Application.instance.setMenueState(null);
		Application.instance.clearContentPanel();
		CommonUtil.clearAppCache();
	}
	
	@Override
	protected void clearContentPanel() {
		contentPanel.clear();
	}
	
	@Override
	protected void setContent(AppScreen p) {
		contentPanel.add(p.getUiObject());
	}
	
	@Override
	protected void showLogoutLink(UserContext userContext) {
		userContextPanel.clear();
		Anchor hlink = new Anchor("Sign out");
		
		hlink.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Application.loginService.logout(new AsyncCallback<Boolean>(){

					@Override
					public void onFailure(Throwable caught) {
						System.out.println("Error while logout");
						caught.printStackTrace();
					}

					@Override
					public void onSuccess(Boolean result) {
						showExpiredSessionUi();
					}
				});
			}
		});
		Label welcomeNote = new Label();
		welcomeNote.setText(""+userContext.getLoginId());
		
		userContextPanel.add(welcomeNote);
		userContextPanel.add(new Label("|"));
		userContextPanel.add(createPreferencesLink());
		userContextPanel.add(new Label("|"));
		userContextPanel.add(hlink);
	}
	
	private Widget createPreferencesLink() {
		Anchor preferences = new Anchor("Change Password");
		preferences.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Application.instance.showScreen(
						new UserPasswordResetView(
								Application.instance.getUserId()));
								//Application.instance.userContext.getUserId()));
			}
		});
		return preferences;
	}
	
	@Override
	protected void showLoginLink() {
		userContextPanel.clear();
		Anchor hlink = new Anchor("Login");
		hlink.setStyleName("link");
		hlink.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Application.instance.showLoginPanel();
			}
		});
		userContextPanel.add(hlink);
	}
}
