package ebilly.admin.client.menu;

import ebilly.admin.client.Application;
import ebilly.admin.client.view.UserPasswordResetView;

public class UserResetPasswordAction extends Action {
	private String userId;
	public UserResetPasswordAction(String userId) {
		super();
		this.userId = userId;
	}
	
	protected void execute() {
		UserPasswordResetView p = new UserPasswordResetView(this.userId);
		Application.instance.showScreen(p);
	}
}

