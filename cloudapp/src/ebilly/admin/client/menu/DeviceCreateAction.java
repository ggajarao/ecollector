package ebilly.admin.client.menu;

import ebilly.admin.client.Application;
import ebilly.admin.client.view.DeviceCrudView;

public class DeviceCreateAction extends Action {
	
	public DeviceCreateAction() {
		super();
	}

	@Override
	protected void execute() {
		DeviceCrudView p = new DeviceCrudView("New Device");
		Application.instance.showScreen(p);
	}
}

