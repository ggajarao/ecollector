package ebilly.admin.client.menu;

import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;

public class XAppMenu extends XMenu {
	//private XLeftMenu leftMenu;
	//List<XMenuItem> actions = new ArrayList<XMenuItem>();
	public XAppMenu() {
		/*buildUi();
		initWidget(leftMenu);*/
	}
	
	public Panel buildUi() {
		HTMLPanel p = new HTMLPanel("");
		p.setStyleName("menu-left");
		p.setWidth("100%");
		return p;
		/*XLeftMenu leftMenu = new XLeftMenu();
		leftMenu.setWidth("100%");
		return leftMenu;*/
	}
	
	public void add(Widget w) {
		XMenuGroup menuGroup = (XMenuGroup)w;
		HTMLPanel menuGroupHeading = new HTMLPanel(menuGroup.getCaption());
		menuGroupHeading.setStyleName("menu-left-group-head");
		/*this.panel.add(menuGroupHeading);
		this.panel.add(menuGroup);*/
		super.add(menuGroupHeading);
		super.add(menuGroup);
	}
}

