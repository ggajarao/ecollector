package ebilly.admin.client.menu;

import java.util.ArrayList;
import java.util.List;

public abstract class Action {
	
	public Action() {
	}
	
	private List<ActionListener> listeners = new ArrayList<ActionListener>();
	public void addActionListener(ActionListener listener) {
		listeners.add(listener);
	}
	
	public void perform() {
		onBeforeAction();
		execute();
		onAfterAction();
	}
	
	private void onBeforeAction() {
		for (ActionListener listener : listeners) {
			listener.onBeforeAction(null);
		}
	}
	
	private void onAfterAction() {
		for (ActionListener listener : listeners) {
			listener.onAfterAction(null);
		}
	}
	
	protected abstract void execute();
}
