package ebilly.admin.client.menu;

import ebilly.admin.client.Application;
import ebilly.admin.client.view.LivePrAggregateSearchView;

public class LivePrAggregateSearchAction extends Action {
	public LivePrAggregateSearchAction() {
		super();
	}
	
	protected void execute() {
		LivePrAggregateSearchView p = new LivePrAggregateSearchView(
				"Todays Amount Collected in distributions");
		Application.instance.showScreen(p);
		p.search();
	}
}
