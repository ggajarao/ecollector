package ebilly.admin.client.menu;

import ebilly.admin.client.Application;
import ebilly.admin.client.view.ImportRequestSearchView;
import ebilly.admin.shared.AppConfig;

public class ImportRequestSearchAction extends Action {
	public ImportRequestSearchAction() {
		super();
	}
	
	protected void execute() {
		ImportRequestSearchView p = new ImportRequestSearchView(AppConfig.OBJ_IMPORT_REQUEST);
		Application.instance.showScreen(p);
		p.search();
	}
}
