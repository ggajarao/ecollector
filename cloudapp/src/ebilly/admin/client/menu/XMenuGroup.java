package ebilly.admin.client.menu;

import com.google.gwt.dom.client.Document;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.Widget;

public class XMenuGroup extends ComplexPanel {

	private String caption;
	private XMenu mMenu;
	public XMenuGroup(XMenu menu, String caption) {
		this.caption = caption;
		this.mMenu = menu;
		setElement(Document.get().createULElement());
		this.mMenu.add(this);
	}
	
	public String getCaption() {
		return this.caption;
	}
	
	public void add(Widget widget) {
		if (widget instanceof XMenuItem) {
			((XMenuItem)widget).setMenuGroup(this);
		}
		add(widget, getElement());
	}

	public void setActiveItem(XMenuItem menuItem) {
		this.mMenu.setActive(menuItem);
	}
}