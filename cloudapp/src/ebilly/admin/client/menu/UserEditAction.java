package ebilly.admin.client.menu;

import ebilly.admin.client.Application;
import ebilly.admin.client.view.CrudViewDefinition;
import ebilly.admin.client.view.UserCrudView;
import ebilly.admin.shared.AppConfig;

public class UserEditAction extends Action {
	private String userId;
	public UserEditAction(String userId) {
		super();
		this.userId = userId;
	}
	
	protected void execute() {
		UserCrudView p = new UserCrudView(
				CrudViewDefinition.CRUD_ACTION_UPDATE,
				AppConfig.CRUD_USER+" - Edit",this.userId);
		Application.instance.showScreen(p);
	}
}

