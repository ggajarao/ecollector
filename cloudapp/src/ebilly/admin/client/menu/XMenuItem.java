package ebilly.admin.client.menu;

import com.google.gwt.dom.client.Document;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.ui.FocusWidget;

public class XMenuItem extends FocusWidget implements ClickHandler, KeyPressHandler, ActionListener {
	private Action action;
	//private XMenu appMenu;
	private XMenuGroup menuGroup;
	private boolean isHighlighted = false;
	public XMenuItem(String caption, Action action) {
		setElement(Document.get().createLIElement());
		getElement().setInnerHTML(caption);
		
		this.action = action;
		//this.appMenu = appMenu;
		this.addClickHandler(this);
		this.addKeyPressHandler(this);
		if (action != null) this.action.addActionListener(this);
		this.addMouseOverHandler(new MouseOverHandler() {
			public void onMouseOver(MouseOverEvent event) {
				if (!isHighlighted) addStyleName("hover");				
			}
		});
		this.addMouseOutHandler(new MouseOutHandler() {
			public void onMouseOut(MouseOutEvent event) {
				removeStyleName("hover");
			}
		});
	}
	
	void setMenuGroup(XMenuGroup menuGroup) {
		this.menuGroup = menuGroup;
	}
	
	@Override
	public void onBeforeAction(Object data) {
		//this.appMenu.setActive(this);
		menuGroup.setActiveItem(this);
	}
	@Override
	public void onAfterAction(Object data) {
		
	}
	@Override
	public void onClick(ClickEvent event) {
		this.action.perform();
	}
	public void unhighlight() {
		this.isHighlighted = false;
		this.removeStyleName("selected");
	}
	public void highlight() {
		this.isHighlighted = true;
		this.addStyleName("selected");
	}
	@Override
	public void onKeyPress(KeyPressEvent event) {
		if (event.getUnicodeCharCode() == 13/*Return key*/) {
			this.action.perform();
		}
	}
}

