package ebilly.admin.client.menu;

import ebilly.admin.client.Application;
import ebilly.admin.client.view.XDeviceDashboardView;
import ebilly.admin.shared.AppConfig;

public class DeviceDashboardViewAction extends Action {
	private String mDeviceId;
	public DeviceDashboardViewAction(String deviceId) {
		super();
		this.mDeviceId = deviceId;
	}
	
	protected void execute() {
		XDeviceDashboardView p = new XDeviceDashboardView(
				AppConfig.OBJ_DEVICE+" Dashboard",this.mDeviceId);
		Application.instance.showScreen(p);
		/*DeviceCrudView p = 
			new DeviceCrudView(CrudViewDefinition.CRUD_ACTION_UPDATE, 
				"Device Dashboard", mDeviceId);
		Application.instance.showScreen(p);*/
	}
}

