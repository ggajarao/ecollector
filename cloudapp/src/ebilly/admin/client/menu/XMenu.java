package ebilly.admin.client.menu;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;

public class XMenu extends Composite {
	List<XMenuItem> actions = new ArrayList<XMenuItem>();
	private Panel containerPanel = null;
	public XMenu() {
		containerPanel = buildUi();
		initWidget(containerPanel);
	}
	
	public Panel buildUi() {
		return new HTMLPanel("");
	}
	
	public void clear() {
		this.containerPanel.clear();
	}
	
	public void add(Widget w) {
		this.containerPanel.add(w);
	}

	private XMenuItem activeMenuItem;
	public void setActive(XMenuItem menuItem) {
		if (this.activeMenuItem != null) {
			this.activeMenuItem.unhighlight();
		}
		this.activeMenuItem = menuItem;
		if (this.activeMenuItem != null) {
			this.activeMenuItem.highlight();
		}
	}
}

