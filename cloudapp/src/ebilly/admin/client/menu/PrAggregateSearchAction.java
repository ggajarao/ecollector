package ebilly.admin.client.menu;

import ebilly.admin.client.Application;
import ebilly.admin.client.view.PrAggregateSearchView;

public class PrAggregateSearchAction extends Action {
	public PrAggregateSearchAction() {
		super();
	}
	
	protected void execute() {
		PrAggregateSearchView p = new PrAggregateSearchView(
				"Amount Collected in distributions");
		Application.instance.showScreen(p);
		p.search();
	}
}
