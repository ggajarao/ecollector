package ebilly.admin.client.menu;

public interface ActionListener {

	void onBeforeAction(Object data);

	void onAfterAction(Object data);
	
}

