package ebilly.admin.client.menu;

import ebilly.admin.client.Application;
import ebilly.admin.client.view.PrSearchView;

public class PrSearchAction extends Action {
	public PrSearchAction() {
		super();
	}
	
	protected void execute() {
		PrSearchView p = new PrSearchView("Collections");
		Application.instance.showScreen(p);
		p.search();
	}
}
