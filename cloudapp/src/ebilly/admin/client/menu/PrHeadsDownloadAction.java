package ebilly.admin.client.menu;

import ebilly.admin.client.Application;
import ebilly.admin.client.view.PrHeadsDownloadView;

public class PrHeadsDownloadAction extends Action {
	public PrHeadsDownloadAction() {
		super();
	}
	
	protected void execute() {
		PrHeadsDownloadView p = new PrHeadsDownloadView();
		Application.instance.showScreen(p);
	}
}
