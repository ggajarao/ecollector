package ebilly.admin.client.menu;

import ebilly.admin.client.Application;
import ebilly.admin.client.DataUploadPanel;
import ebilly.admin.shared.AppConfig;

public class DataUploadAction extends Action {
	private String mDeviceId;
	public DataUploadAction(String deviceId) {
		super();
		mDeviceId = deviceId;
	}
	
	protected void execute() {
		DataUploadPanel p = new DataUploadPanel(AppConfig.OBJ_DEVICE_SERVICE_FILE, mDeviceId);
		Application.instance.showScreen(p);
	}
}

