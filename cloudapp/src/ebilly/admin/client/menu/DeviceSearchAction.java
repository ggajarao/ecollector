package ebilly.admin.client.menu;

import ebilly.admin.client.Application;
import ebilly.admin.client.view.DeviceSearchView;
import ebilly.admin.shared.AppConfig;

public class DeviceSearchAction extends Action {
	public DeviceSearchAction() {
		super();
	}
	
	protected void execute() {
		DeviceSearchView p = new DeviceSearchView(AppConfig.OBJ_DEVICE);
		Application.instance.showScreen(p);
	}
}

