package ebilly.admin.client.menu;

import ebilly.admin.client.Application;
import ebilly.admin.client.view.UserSearchView;
import ebilly.admin.shared.AppConfig;

public class UserSearchAction extends Action {
	public UserSearchAction() {
		super();
	}
	
	protected void execute() {
		UserSearchView p = new UserSearchView(AppConfig.OBJ_USER);
		Application.instance.showScreen(p);
	}
}
