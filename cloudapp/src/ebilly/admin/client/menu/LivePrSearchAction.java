package ebilly.admin.client.menu;

import ebilly.admin.client.Application;
import ebilly.admin.client.view.LivePrSearchView;

public class LivePrSearchAction extends Action {
	public LivePrSearchAction() {
		super();
	}
	
	protected void execute() {
		LivePrSearchView p = new LivePrSearchView("Todays Collections");
		Application.instance.showScreen(p);
		p.search();
	}
}
