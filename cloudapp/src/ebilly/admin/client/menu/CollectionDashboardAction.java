package ebilly.admin.client.menu;

import ebilly.admin.client.Application;
import ebilly.admin.client.view.CollectionDashboardView;

public class CollectionDashboardAction extends Action {
	public CollectionDashboardAction() {
		super();
	}
	
	protected void execute() {
		CollectionDashboardView p = new CollectionDashboardView();
		Application.instance.showScreen(p);
	}
}

