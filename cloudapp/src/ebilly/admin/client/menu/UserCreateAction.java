package ebilly.admin.client.menu;

import ebilly.admin.client.Application;
import ebilly.admin.client.view.UserCrudView;

public class UserCreateAction extends Action {
	
	public UserCreateAction() {
		super();
	}

	@Override
	protected void execute() {
		UserCrudView p = new UserCrudView("New User");
		Application.instance.showScreen(p);
	}
}

