package ebilly.admin.client.menu;

import ebilly.admin.client.Application;
import ebilly.admin.client.view.PrDownloadView;

public class PrDownloadAction extends Action {
	public PrDownloadAction() {
		super();
	}
	
	protected void execute() {
		PrDownloadView p = new PrDownloadView();
		Application.instance.showScreen(p);
	}
}
