package ebilly.admin.client.layout;

import com.google.gwt.user.client.ui.Widget;

public interface UiLayout {

	Widget getUiObject();

	void bindData(Object data);

}
