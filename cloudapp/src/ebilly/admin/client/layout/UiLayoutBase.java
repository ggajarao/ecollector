package ebilly.admin.client.layout;

import com.google.gwt.user.client.ui.Widget;

public abstract class UiLayoutBase implements UiLayout {
	
	public abstract Widget getUiObject();
	
	public void bindData(Object data) {
		
	}
}
