package ebilly.admin.client;

import com.google.gwt.user.client.ui.Panel;

import ebilly.admin.shared.UserContext;


public class XApplication extends ApplicationBase {
	private XApplication() {
		super();
		buildLayoutPanel();
	}
	
	static XApplication createInstance() {
		return new XApplication();
	}
	
	private void buildLayoutPanel() {

	}
	
	@Override
	protected void clearContentPanel() {
		
	}

	@Override
	protected void setMenueState(UserContext userContext) {
		
	}

	@Override
	protected void showLoginLink() {
		
	}

	@Override
	protected void showLogoutLink(UserContext userContext) {
		
	}

	@Override
	public void freezApplication() {
		
	}

	@Override
	protected void setContent(AppScreen p) {
		
	}

	@Override
	protected Panel getAppPanel() {
		return null;
	}
}
