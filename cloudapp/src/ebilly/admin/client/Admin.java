package ebilly.admin.client;

import com.google.gwt.core.client.EntryPoint;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Admin implements EntryPoint {
	public void onModuleLoad() {
		Application.instance.showUserHome();
	}
}