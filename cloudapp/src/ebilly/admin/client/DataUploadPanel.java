package ebilly.admin.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteHandler;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import ebilly.admin.client.menu.ActionListener;
import ebilly.admin.client.view.ActionBarWidget;
import ebilly.admin.shared.CommonUtil;

public class DataUploadPanel implements AppScreen {
	
	private FormPanel formPane;
	private FileUpload fileUpload;
	private Button btnStartImporting;
	private HandlerRegistration impBtnHandlerReg;
	private Button btnCancel;
	private HandlerRegistration cancelBtnHandlerReg;
	private CheckBox mSelectImportMethod;
	
	private Hidden objectTypeValue;
	private Hidden staticValue1;
	private Hidden staticValue2;
	private Hidden staticValue3;
	
	private ActionListener actionListener;
	private String mObjectType;
	
	private ActionBarWidget actionBar;
	
	/**
	 * @wbp.parser.entryPoint
	 */
	public DataUploadPanel(String objectType, String value1) {
		mObjectType = objectType;
		buildUi();
		this.setStaticValue1(value1);
	}
	
	private void buildUi() {
		Panel main = new VerticalPanel();
		formPane = new FormPanel();
		formPane.setAction("/cloudapp/secure/dataUploadHandler");
		formPane.setEncoding(FormPanel.ENCODING_MULTIPART);
	    formPane.setMethod(FormPanel.METHOD_POST);
	    formPane.setWidget(main);
		main.setSize("100%", "100%");
		
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		main.add(verticalPanel);
		verticalPanel.setWidth("100%");
				
		Grid grid = new Grid(6, 2);
		grid.setCellSpacing(5);
		main.add(grid);
		int row = 0;

		Label lblNewLabel = new Label("File to import");
		grid.setWidget(++row, 0, lblNewLabel);
		
		fileUpload = new FileUpload();
		fileUpload.setName("dataFile");
		grid.setWidget(row, 1, fileUpload);
		fileUpload.setWidth("311px");
		
		Label lblOnlyFilesOf = new Label("Only files of type .txt or .zip are allowed. Make sure the file size is less than 5MB");
		grid.setWidget(++row, 1, lblOnlyFilesOf);
		lblOnlyFilesOf.setWidth("313px");
		
		mSelectImportMethod = new CheckBox("Select if you want the Device should be cleared before importing this file");
		mSelectImportMethod.setValue(true);
		grid.setWidget(++row, 1, mSelectImportMethod);
		lblOnlyFilesOf.setWidth("313px");
		
		actionBar = new ActionBarWidget();
		grid.setWidget(++row, 1, actionBar);
		
		btnCancel = new Button("Cancel");
		cancelBtnHandlerReg = btnCancel.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Application.instance.clearContentPanel();
			}
		});
		btnCancel.setStyleName("command-button");
		actionBar.add(btnCancel);
		
		btnStartImporting = new Button("Upload");
		actionBar.add(btnStartImporting);
		btnStartImporting.setWidth("");
		btnStartImporting.setStyleName("command-button");
		
		objectTypeValue = new Hidden("objectType");
		main.add(objectTypeValue);
		staticValue1 = new Hidden("staticValue1");
		main.add(staticValue1);
		staticValue2 = new Hidden("staticValue2");
		main.add(staticValue2);
		staticValue3 = new Hidden("staticValue3");
		main.add(staticValue3);
		impBtnHandlerReg = btnStartImporting.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				startImport();
			}			
		});
		
		formPane.addSubmitCompleteHandler(new SubmitCompleteHandler() {
			@Override
			public void onSubmitComplete(SubmitCompleteEvent event) {
				btnCancel.setEnabled(true);
				btnStartImporting.setEnabled(true);
				String results = event.getResults(); 
					//with blob store service, import handler messages are
					// not sent back. So using static message.
					//"Import Job submitted, please check its status";
				//messageLabel.getElement().setInnerHTML(results);
				Application.showMessage(results);
				if (DataUploadPanel.this.actionListener != null) {
					DataUploadPanel.this.actionListener.onAfterAction(null);
				}
				
				if (mImportCompleteActionListener != null) {
					mImportCompleteActionListener.onAfterAction(null);
				}
			}
		});
	}
	
	public void addActionListener(ActionListener actionListener) {
		this.actionListener = actionListener;
	}
	
	public void startImport() {
		if (validateFormAndSetHiddenFields()) {
			Application.showMessage("Importing file please wait...");
			
			btnCancel.setEnabled(false);
			btnStartImporting.setEnabled(false);
			
			Application.instance.secureService.prepareBlobStoreUrl(
					new AsyncCallback<String>() {
						@Override
						public void onSuccess(String result) {
							//messageLabel.setText("Upload Url: "+result);
							formPane.setAction(result);
							formPane.submit();
						}
						
						@Override
						public void onFailure(Throwable caught) {
							Application.showErrorMessage("Error: "+caught.getMessage());
						}
					});
		}
	}
	
	private ActionListener mImportCompleteActionListener;
	public void setImportCompleteActionListener(ActionListener actionListener) {
		mImportCompleteActionListener = actionListener;
	}
	
	public Widget getUiObject() {
		return formPane;
	}
	
	protected boolean validateFormAndSetHiddenFields() {
		String value = mObjectType;//objectTypeList.getValue(objectTypeList.getSelectedIndex());
		if (value == null || value.trim().length() == 0 || value == "-1") {
			Application.showErrorMessage("Please select the type of data you are trying to import !");
			return false;
		}
		
		String selectedFileName = fileUpload.getFilename();
		if (selectedFileName == null || selectedFileName.trim().length() == 0) {
			Application.showErrorMessage("Please choose a file to upload");
			return false;
		}
		
		if (!CommonUtil.isEmpty(selectedFileName) 
			&& !selectedFileName.toUpperCase().endsWith(".TXT")
			&& !selectedFileName.toUpperCase().endsWith(".ZIP")) {
			Application.showErrorMessage("Only files of format .TXT or .ZIP with fixed field lenght are allowed");
			//errorLabel.setText("Only files of format .TXT with fixed field lenght are allowed");
			return false;
		}
		
		this.objectTypeValue.setValue(mObjectType);
		setStaticValue2(this.mSelectImportMethod.getValue() + "");
		
		return true;
	}
	
	public void setStaticValue1(String value) {
		this.staticValue1.setValue(value);
	}
	
	public void setStaticValue2(String value) {
		this.staticValue2.setValue(value);
	}
	
	public void setStaticValue3(String value) {
		this.staticValue3.setValue(value);
	}
	
	public void setCancelHandler(ClickHandler handler) {
		this.cancelBtnHandlerReg.removeHandler();
		this.cancelBtnHandlerReg = this.btnCancel.addClickHandler(handler);
	}
	
	public void setStartImportHandler(ClickHandler handler) {
		this.impBtnHandlerReg.removeHandler();
		this.impBtnHandlerReg = this.btnStartImporting.addClickHandler(handler);
	}
	
	public void addActionWidget(Widget actionWidget) {
		this.actionBar.add(actionWidget);
	}

	@Override
	public void onBeforeClose() {
		
	}
	
	@Override
	public void onBeforeOpen() {
	}
}