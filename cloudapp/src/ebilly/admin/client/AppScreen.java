package ebilly.admin.client;

import com.google.gwt.user.client.ui.Widget;

public interface AppScreen {
	
	Widget getUiObject();
	

	/**
	 * This method notifies that
	 * this screen is about to open
	 */
	void onBeforeOpen();
	
	/**
	 * This method notifies that
	 * this screen is about to close
	 */
	void onBeforeClose();

}

