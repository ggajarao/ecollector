package ebilly.admin.client;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import ebilly.admin.client.menu.ActionListener;
import ebilly.admin.client.view.ActionBarWidget;
import ebilly.admin.client.view.FileUploadWidget;
import ebilly.admin.shared.viewdef.CrudRequest;
import ebilly.admin.shared.viewdef.CrudResponse;

public class NewDataUploadPanel implements AppScreen {
	
	private FormPanel formPane;
	//private FileUpload fileUpload;
	private Button btnStartImporting;
	private HandlerRegistration impBtnHandlerReg;
	private Button btnCancel;
	private HandlerRegistration cancelBtnHandlerReg;
	private CheckBox mSelectImportMethod;
	private Label mLabel;
	
	private String mDeviceId;
	private String mBlobKey;
	private FileUploadWidget mFileUploadWidget;
	
	private ActionBarWidget actionBar;
	
	/**
	 * @wbp.parser.entryPoint
	 */
	public NewDataUploadPanel(String deviceId) {
		mDeviceId = deviceId;
		buildUi();
	}
	
	private void buildUi() {
		Panel main = new VerticalPanel();
		formPane = new FormPanel();
		formPane.setAction("/cloudapp/secure/dataUploadHandler");
		formPane.setEncoding(FormPanel.ENCODING_MULTIPART);
	    formPane.setMethod(FormPanel.METHOD_POST);
	    formPane.setWidget(main);
		main.setSize("100%", "100%");
		
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		main.add(verticalPanel);
		verticalPanel.setWidth("100%");
				
		Grid grid = new Grid(6, 2);
		grid.setCellSpacing(5);
		main.add(grid);
		int row = 0;

		Label lblNewLabel = new Label("File to import");
		grid.setWidget(++row, 0, lblNewLabel);
		
		/*fileUpload = new FileUpload();
		fileUpload.setName("dataFile");
		grid.setWidget(row, 1, fileUpload);
		fileUpload.setWidth("311px");*/
		
		VerticalPanel fup = new VerticalPanel();
		grid.setWidget(row, 1, fup);
		
		mLabel = new Label("No File selected");
		fup.add(mLabel);
		
		mFileUploadWidget = new FileUploadWidget("dataFileUpLoadId");
		fup.add(mFileUploadWidget);
		mFileUploadWidget.setFileSelectionListener(new ActionListener() {
			public void onBeforeAction(Object data) {}
			public void onAfterAction(Object data) {
				fileSelected((String)data);
			}
		});
		mFileUploadWidget.setFileUploadedListener(new ActionListener() {
			public void onBeforeAction(Object data) {}
			public void onAfterAction(Object data) {
				fileUploaded((String)data);
			}
		});
		
		Label lblOnlyFilesOf = new Label("Only files of type .txt are allowed. Make sure the file size is less than 5MB");
		grid.setWidget(++row, 1, lblOnlyFilesOf);
		lblOnlyFilesOf.setWidth("313px");
		
		mSelectImportMethod = new CheckBox("Select if you want the Device should be cleared before importing this file");
		mSelectImportMethod.setValue(true);
		grid.setWidget(++row, 1, mSelectImportMethod);
		lblOnlyFilesOf.setWidth("313px");
		
		actionBar = new ActionBarWidget();
		grid.setWidget(++row, 1, actionBar);
		
		btnCancel = new Button("Cancel");
		cancelBtnHandlerReg = btnCancel.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Application.instance.clearContentPanel();
			}
		});
		btnCancel.setStyleName("command-button");
		actionBar.add(btnCancel);
		
		btnStartImporting = new Button("Upload");
		actionBar.add(btnStartImporting);
		btnStartImporting.setWidth("");
		btnStartImporting.setEnabled(false);
		btnStartImporting.setStyleName("command-button");
		
		impBtnHandlerReg = btnStartImporting.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				startImport();
			}			
		});
		
		/*formPane.addSubmitCompleteHandler(new SubmitCompleteHandler() {
			@Override
			public void onSubmitComplete(SubmitCompleteEvent event) {
				btnCancel.setEnabled(true);
				btnStartImporting.setEnabled(true);
				String results = //event.getResults(); 
					//with blob store service, import handler messages are
					// not sent back. So using static message.
					"Import Job submitted, please check its status";
				//messageLabel.getElement().setInnerHTML(results);
				Application.showMessage(results);
				if (NewDataUploadPanel.this.actionListener != null) {
					NewDataUploadPanel.this.actionListener.onAfterAction(null);
				}
				
				if (mImportCompleteActionListener != null) {
					mImportCompleteActionListener.onAfterAction(null);
				}
			}
		});*/
		
		Scheduler.get().scheduleDeferred(new ScheduledCommand() {
			@Override
			public void execute() {
				prepareUploadUrl();
			}
		});
	}
	
	private void prepareUploadUrl() {
		mFileUploadWidget.setEnabled(false);
		btnStartImporting.setEnabled(false);
		Application.instance.secureService.prepareBlobStoreUrl(
				"/cloudapp/fileUploadHandler",
				new AsyncCallback<String>() {
					@Override
					public void onSuccess(String result) {
						mFileUploadWidget.setFileUploadUrl(result);
						mFileUploadWidget.setEnabled(true);
						btnStartImporting.setEnabled(true);
					}

					@Override
					public void onFailure(Throwable caught) {
						Application.showErrorMessage("Error: "+caught.getMessage());
						mFileUploadWidget.setEnabled(true);
					}
				});
	}
	
	private void fileSelected(String data) {
		mLabel.setText(data);
		mFileUploadWidget.setEnabled(false);
		btnStartImporting.setEnabled(false);
	}
	
	private void fileUploaded(String serverResponse) {
		mBlobKey = serverResponse;
		Application.showErrorMessage("");
		prepareUploadUrl();
	}
	
	public void startImport() {
		if (validateFormAndSetHiddenFields()) {
			Application.showMessage("Please wait...");
			btnCancel.setEnabled(false);
			btnStartImporting.setEnabled(false);
			final CrudRequest request = new CrudRequest();
			request.addFieldValue("deviceId", mDeviceId);
			request.addFieldValue("remoteDeviceImportMethod", this.mSelectImportMethod.getValue()+"");
			request.addFieldValue("deviceServicesFileKey", mBlobKey);
			Application.instance.secureService.acceptDeviceServiceFile(request, new AsyncCallback<CrudResponse>() {
				public void onSuccess(CrudResponse result) {
					if (result.isSuccess()) {
						fireImportCompleteEvent();
					} else {
						Application.showErrorMessage(result.errorMessage);
						btnCancel.setEnabled(true);
						btnStartImporting.setEnabled(true);
					}
				};
				public void onFailure(Throwable caught) {
					btnCancel.setEnabled(true);
					btnStartImporting.setEnabled(true);
				}
			});
		}
	}
	
	private void fireImportCompleteEvent() {
		if (mImportCompleteActionListener != null) {
			mImportCompleteActionListener.onAfterAction(null);
		}
	}
	
	private ActionListener mImportCompleteActionListener;
	public void setImportCompleteActionListener(ActionListener actionListener) {
		mImportCompleteActionListener = actionListener;
	}
	
	public Widget getUiObject() {
		return formPane;
	}
	
	protected boolean validateFormAndSetHiddenFields() {
		return true;
	}
	
	public void setCancelHandler(ClickHandler handler) {
		this.cancelBtnHandlerReg.removeHandler();
		this.cancelBtnHandlerReg = this.btnCancel.addClickHandler(handler);
	}
	
	public void setStartImportHandler(ClickHandler handler) {
		this.impBtnHandlerReg.removeHandler();
		this.impBtnHandlerReg = this.btnStartImporting.addClickHandler(handler);
	}

	@Override
	public void onBeforeClose() {
		
	}
	
	@Override
	public void onBeforeOpen() {
	}
	
	public void addActionWidget(Widget actionWidget) {
		this.actionBar.add(actionWidget);
	}
}