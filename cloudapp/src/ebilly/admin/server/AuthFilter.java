package ebilly.admin.server;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ebilly.admin.shared.UserContext;

/**
 * 
 * @author Vamsi
 *
 */
public final class AuthFilter implements Filter {
   private FilterConfig filterConfig = null;
   public void init(FilterConfig filterConfig) 
      throws ServletException {
      this.filterConfig = filterConfig;      
   }
   public void destroy() {
      this.filterConfig = null;      
   }
   
   public void doFilter(ServletRequest request,
      ServletResponse response, FilterChain chain) 
      throws IOException, ServletException {
	   
	  HttpServletRequest hreq  = (HttpServletRequest)request; 
	  UserContext userContext = 
		  (UserContext)hreq.getSession().getAttribute(
				  Util.SESSION_ATTRIB_AUTH_TOKEN);
	  if (userContext != null) {
		  chain.doFilter(request, response);
	  } else {
		  /*RequestDispatcher requestDispatcher = this.filterConfig.getServletContext().getRequestDispatcher("/College.html");
		  requestDispatcher.forward(request, response);*/
		  ((HttpServletResponse)response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		  //throw new ServletException("Invalid Session");
	  }
   }
}
