package ebilly.admin.server;

import java.util.Date;

import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import ebilly.admin.client.service.SecureService;
import ebilly.admin.server.bo.BaseSearchHandler;
import ebilly.admin.server.bo.CommonBo;
import ebilly.admin.server.bo.CrudHandler;
import ebilly.admin.server.bo.DeviceBo;
import ebilly.admin.server.bo.PrBo;
import ebilly.admin.server.bo.UserBo;
import ebilly.admin.server.core.AppLogger;
import ebilly.admin.shared.DeviceFileUi;
import ebilly.admin.shared.DeviceUi;
import ebilly.admin.shared.EntitySelection;
import ebilly.admin.shared.FieldVerifier;
import ebilly.admin.shared.PushPrRequest;
import ebilly.admin.shared.RoleUi;
import ebilly.admin.shared.UserContext;
import ebilly.admin.shared.UserUi;
import ebilly.admin.shared.viewdef.CrudRequest;
import ebilly.admin.shared.viewdef.CrudResponse;
import ebilly.admin.shared.viewdef.SearchRequest;
import ebilly.admin.shared.viewdef.SearchResult;


/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class SecureServiceImpl extends RemoteServiceServlet implements
		SecureService {
	
	private static AppLogger log = AppLogger.getLogger(SecureServiceImpl.class.getName()); 

	@Override
	protected void onBeforeRequestDeserialized(String serializedRequest) {
		super.onBeforeRequestDeserialized(serializedRequest);
		
		/*
		 Pushes User context into ContextUtils, for later
		 use to refer current logged in user information.
		 */
		Object userContext = getThreadLocalRequest().getSession()
			.getAttribute(Util.SESSION_ATTRIB_AUTH_TOKEN);
		if (userContext != null) {
			ContextUtil.getInstance().pushUserContext((UserContext)userContext);
		} else {
			log.warning("User context not found !");
		}
	}
	
	public String greetServer(String input) throws IllegalArgumentException {
		// Verify that the input is valid. 
		if (!FieldVerifier.isValidName(input)) {
			// If the input is not valid, throw an IllegalArgumentException back to
			// the client.
			throw new IllegalArgumentException(
					"Name must be at least 4 characters long");
		}

		String serverInfo = getServletContext().getServerInfo();
		String userAgent = getThreadLocalRequest().getHeader("User-Agent");

		// Escape data from the client to avoid cross-site script vulnerabilities.
		input = escapeHtml(input);
		userAgent = escapeHtml(userAgent);

		return "Hello, " + input + "!<br><br>I am running " + serverInfo
				+ ".<br><br>It looks like you are using:<br>" + userAgent;
	}

	/**
	 * Escape an html string. Escaping data received from the client helps to
	 * prevent cross-site script vulnerabilities.
	 * 
	 * @param html the html string to escape
	 * @return the escaped string
	 */
	private String escapeHtml(String html) {
		if (html == null) {
			return null;
		}
		return html.replaceAll("&", "&amp;").replaceAll("<", "&lt;")
				.replaceAll(">", "&gt;");
	}

	@Override
	public SearchResult searchView(SearchRequest request) 
	throws IllegalArgumentException {
		String message = "";
		try {
			String objectName = request.getObjectName();
			BaseSearchHandler bsh = BaseSearchHandler.getHandler(objectName);
			if (bsh != null) {
				return bsh.fetchResults(request);
			}
		} catch (Exception e) {
			message = e.getMessage();
			//e.printStackTrace();
			log.info("Exception while Search", e);
		}
		SearchResult results = new SearchResult(request.getObjectName());
		results.setErrorMessage(message);
		return results;
	}

	@Override
	public CrudResponse processCrud(CrudRequest request)
			throws IllegalArgumentException {
		CrudHandler handler = CrudHandler.getHandler(request);
		return handler.handleRequest();
	}
	
	@Override
	public String getCurrentDate() throws IllegalArgumentException {
		return Util.getFormattedDate(new Date());
	}
	
	public RoleUi[] fetchAdminRoles() 
	throws IllegalArgumentException {
		return UserBo.cFetchAdminRoles();
	}
	
	public RoleUi[] fetchAllRoles() 
	throws IllegalArgumentException {
		return UserBo.cFetchAllRoles();
	}
	
	public CrudResponse deleteUser(String userId) 
	throws IllegalArgumentException {
		return UserBo.deleteUser(userId);
	}
	
	public UserUi fetchUser(String userId)
	throws IllegalArgumentException {
		return UserBo.fetchUser(userId);
	}
	
	public CrudResponse resetUserPassword(String userId, String newPassword) 
	throws IllegalArgumentException {
		return UserBo.resetUserPassword(userId, newPassword);
	}
	
	public CrudResponse processDeleteRequest(EntitySelection request)
	throws IllegalArgumentException {
		return CommonBo.processDeleteRequest(request);
	}
	
	public String prepareBlobStoreUrl() throws IllegalArgumentException {
		String uploadUrl = BlobstoreServiceFactory.getBlobstoreService().createUploadUrl("/cloudapp/secure/dataUploadHandler");
		return uploadUrl;
	}
	
	public String prepareBlobStoreUrl(String successUrl) throws IllegalArgumentException {
		String uploadUrl = BlobstoreServiceFactory.getBlobstoreService().createUploadUrl(successUrl);
		return uploadUrl;
	}
	
	public CrudResponse sendPasswordResetEmail(String userId, String emailAddress)
	throws IllegalArgumentException {
		return UserBo.sendPasswordResetEmail(userId, emailAddress);
	}

	@Override
	public DeviceUi fetchDevice(String deviceName)
			throws IllegalArgumentException {
		return DeviceBo.fetchDevice(deviceName);		
	}
	
	public CrudResponse removeDeviceFiles(String[] deviceFileIds)
	throws IllegalArgumentException {
		return DeviceBo.removeDeviceFiles(deviceFileIds);
	}
	
	@Override
	public DeviceFileUi[] fetchDownloadableDevicefiles(String deviceIdentifier)
			throws IllegalArgumentException {
		return DeviceBo.fetchDownloadableDeviceFiles(deviceIdentifier);
	}
	
	@Override
	public CrudResponse deviceFileDownloadedInDevice(String deviceIdentifier,
			String deviceFileId) throws IllegalArgumentException {
		return DeviceBo.deviceFileDownloadedInDevice(deviceIdentifier,
				deviceFileId);
	}
	
	@Override
	public CrudResponse pushPrs(PushPrRequest request) 
	throws IllegalArgumentException {
		return PrBo.importPrs(request);
	}
	
	public CrudResponse acceptDeviceServiceFile(CrudRequest crudRequest)
	throws IllegalArgumentException {
		String deviceId = crudRequest.getFieldValueMap().get("deviceId");
		String importMethod = crudRequest.getFieldValueMap().get("remoteDeviceImportMethod");
		String blobKey = crudRequest.getFieldValueMap().get("deviceServicesFileKey");
		return DeviceBo.acceptDeviceServiceFile(deviceId, importMethod, blobKey);
	}
	
	public CrudResponse createExportRequest(SearchRequest searchRequest)
	throws IllegalArgumentException {
		return CommonBo.createExportRequest(searchRequest);
	}
	
	@Override
	public CrudResponse resetDeviceCollection(String deviceId)
			throws IllegalArgumentException {
		return DeviceBo.resetDeviceCollection(deviceId);
	} 
	
	@Override
	public CrudResponse updateDeviceCommand(String deviceCommandId,
			String commandState, String executionNotes)
			throws IllegalArgumentException {
		return DeviceBo.updateDeviceCommand(deviceCommandId, 
				commandState, executionNotes);
	}
	
	@Override
	public CrudResponse resetDevicePairing(String deviceId)
			throws IllegalArgumentException {
		return DeviceBo.resetDevicePairing(deviceId);
	}
	
	@Override
	public CrudResponse fetchDeviceSummaryReport(String deviceId)
			throws IllegalArgumentException {
		return DeviceBo.fetchDeviceSummaryReport(deviceId);
	}
}

