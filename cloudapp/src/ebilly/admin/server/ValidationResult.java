package ebilly.admin.server;

public class ValidationResult {
	StringBuffer errorMessage = new StringBuffer();
	private boolean hasErrors;
	public ValidationResult() {
		reset();
	}

	public void reset() {
		errorMessage.setLength(0);
		hasErrors = false;
	}

	public void addItem(String item) {
		this.hasErrors = true;
		errorMessage.append(". ").append(item);
	}

	public String getMessage() {
		return errorMessage.toString();
	}

	public boolean hasErrors() {
		return this.hasErrors;
	}
}
