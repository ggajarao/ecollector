package ebilly.admin.server;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ebilly.admin.server.bo.DeviceBo;
import ebilly.admin.server.core.AppLogger;
import ebilly.admin.shared.CommonUtil;

public class DownloadDeviceData extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		process(req,resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		process(req,resp);
	}
	
	private void process(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String deviceName = req.getParameter("dn");
		
		/****AWARE OF THIS CONTENT TYPE****/
		resp.setContentType("text/plain");
		
		if (CommonUtil.isEmpty(deviceName)) {
			resp.getWriter().println("Invalid request");
			return;
		}
		downloadAction(deviceName,req,resp);
	}

	private void downloadAction(String deviceName,
			HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String downloadFileName = deviceName+".txt";
		resp.setHeader("Content-Disposition","inline;filename="+downloadFileName);
		try {
			DeviceBo.exportDeviceServiceStageRecords(deviceName, resp.getWriter());
		} catch (Exception e) {
			AppLogger.getLogger(DownloadDeviceData.class.getName()).info("Error downloading data from deviceName: "+deviceName,e);
			resp.getWriter().println("Error!! don't try again");
		}
	}
}