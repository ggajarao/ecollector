package ebilly.admin.server.core;

import java.util.logging.Level;
import java.util.logging.Logger;

public class AppLogger
{
	private final Logger log;
	AppLogger(String clazz) {
		 log = Logger.getLogger(clazz);
	}
	
	public static AppLogger getLogger(String clazz) {
		return new AppLogger(clazz);
	}
	
	public void info(String message)
	{
		log.info(message);
	}

	public void info(String message, Throwable t)
	{
		log.log(Level.INFO, message, t);
	}

	public void debug(String message) {
		log.log(Level.FINER, message);
	}

	public void error(String message, Throwable e) {
		log.log(Level.SEVERE, message, e);
	}
	
	public void warning(String message)
	{
		log.warning(message);
	}

	public void warning(String message, Throwable t)
	{
		log.log(Level.WARNING, message, t);
	}
}

