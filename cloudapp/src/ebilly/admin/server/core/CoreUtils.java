package ebilly.admin.server.core;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CoreUtils {
	
	public static final String DEFAULT_DATE_FORMAT = "dd/MM/yyyy";
	public static final String MONTH_YEAR = "MMM-yyyy";
	public static final String YEAR = "yyyy";
	
	public static String toBeanProperty(String propertyName)
	{
	    if (!isEmpty(propertyName))
	    {
	        return new StringBuilder().append(
	            Character.toLowerCase(propertyName.charAt(0))).append(
	                propertyName.substring(1)).toString();
	    }
	    return propertyName;
	}
	
	public static Map<String, Object> toFieldTypedValuemap(
			String[] fieldNames, String[] values, 
			Map<String, Class> fieldTypeMap) {
		Map<String, Object> fieldProperties 
			= new HashMap<String, Object>();
		for (int i = 0; i < fieldNames.length; i++) {
			String fieldName = fieldNames[i];
			String value = values[i];
			if (!isEmpty(value)) {
				value = value.trim();
				Class type = fieldTypeMap.get(fieldName);
				fieldProperties.put(
						CoreUtils.toBeanProperty(fieldName), 
						CoreUtils.convertToTypedValue(value,type));
			}
		}
		return fieldProperties;
	}
	
	/**
	 * Fills the given fieldProperties map with the typed values
	 * 
	 * @param fieldNames
	 * @param values
	 * @param fieldTypeMap
	 * @param fieldProperties
	 */
	public static void toFieldTypedValuemap(
			String[] fieldNames, String[] values, 
			Map<String, Class> fieldTypeMap, Map<String, Object> fieldProperties) {
		for (int i = 0; i < fieldNames.length; i++) {
			String fieldName = fieldNames[i];
			String value = values[i];
			if (!isEmpty(value)) {
				value = value.trim();
				Class type = fieldTypeMap.get(fieldName);
				fieldProperties.put(
						CoreUtils.toBeanProperty(fieldName), 
						CoreUtils.convertToTypedValue(value,type));
			}
		}
	}
	
	public static Object convertToTypedValue(String value, Class type) {
		if (type == null) {
			return value;
		}
		value = value.trim();
		
		if ( type.equals(java.lang.String.class)) {
			return value;
		} else if (type.equals(java.lang.Integer.class)||
				type.getName().equals("int")) {
			if (isNull(value)) return 0;
			return new Integer(value);
		} else if (type.equals(java.lang.Double.class) ||
				type.getName().equals("double")) {
			if (isNull(value)) return 0;
			return new Double(value);
		} else if (type.equals(java.lang.Long.class)||
				type.getName().equals("long")) {
			if (isNull(value)) return 0;
			return new Long(value);
		} else if (type.equals(java.lang.Boolean.class)||
				type.getName().equals("boolean")) {
			return new Boolean(value);
		} else if (type.equals(java.util.Date.class)) {
			if (isNull(value)) return null;
			if (!isEmpty(value)) {
				Date dateValue = CoreUtils.getDateFromString(value);
				if (dateValue == null ) {
					throw new IllegalArgumentException("Date value must be in the format dd/MM/yyyy");
				}
				return dateValue;
			} else {
				return null;
			}
		} else {
			throw new IllegalArgumentException(
				"Conversion error. Unsupported type: "+type+", value: "+value);
		}
	}
	
	public static boolean isEmpty(String value) {
		return value == null || value.trim().length() == 0;		
	}
	
	public static boolean isNull(String value) {
		if (isEmpty(value)) return true;
		if ("NULL".equalsIgnoreCase(value.trim())) return true;
		return false;
	}
	
	public static final Date getDateFromString(String dateString,
	        String pattern)
	{
		SimpleDateFormat simpeDateFormat = new SimpleDateFormat(pattern);
		Date date = simpeDateFormat.parse(dateString, new ParsePosition(0));
		return date;
	}
	
	public static final Date getDateFromString(String dateString) {
		return getDateFromString(dateString, DEFAULT_DATE_FORMAT);
	}
	
	public static String getFormattedDate(Date date, String pattern) {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(date);
	}
	
	public static String getFormattedDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
		return sdf.format(date);
	}
}

