package ebilly.admin.server.core.reports;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import ebilly.admin.server.core.AppLogger;
import ebilly.admin.shared.viewdef.Filter;
import ebilly.admin.shared.viewdef.FilterCriteria;
import ebilly.admin.shared.viewdef.PivotCriteria;
import ebilly.admin.shared.viewdef.ResultField;
import ebilly.admin.shared.viewdef.ResultRecord;
import ebilly.admin.shared.viewdef.SearchRequest;
import ebilly.admin.shared.viewdef.SearchResult;

public class ResultAggregatorFactory {
	
	public static JsonPrimitive asJsonObject(String value) {
		if (value == null) 
			return new JsonPrimitive("");
		else 
			return new JsonPrimitive(value);
	}
	
	public static String jsonAsString(JsonPrimitive primitive) {
		if (primitive == null) return null;
		return primitive.getAsString();
	}
	
	public static long jsonAsLong(JsonPrimitive primitive) {
		if (primitive == null) return 0;
		return primitive.getAsLong();
	}

	public static int jsonAsInteger(JsonPrimitive primitive) {
		if (primitive == null) return 0;
		return primitive.getAsInt();
	}

	
	public static JsonElement asJsonObject(long value) {
		return new JsonPrimitive(value);
	}

	public static JsonElement asJsonObject(int value) {
		return new JsonPrimitive(value); 
	}
	
	public static JsonObject asJsonObject(ResultRecord resultRecord) {
		JsonObject resultRecordJson = new JsonObject();
		if (resultRecord == null) return resultRecordJson;
		JsonArray fieldValuesJson = new JsonArray();
		resultRecordJson.add("fieldValues",fieldValuesJson);
		if (resultRecord.fieldValues != null) {
			for (String fieldValue : resultRecord.fieldValues) {
				fieldValuesJson.add(new JsonPrimitive(fieldValue));
			}
		}
		return resultRecordJson;
	}
	
	private static ResultRecord jsonAsResultRecord(JsonObject resultRecordJson) {
		if (resultRecordJson == null) return null;
		return new ResultRecord(ResultAggregatorFactory.jsonAsStringList(resultRecordJson.getAsJsonArray("fieldValues")));
	}
	
	public static JsonObject asJsonObject(SearchResult searchResult) {
		JsonObject resultsJson = new JsonObject();
		if (searchResult == null) return resultsJson;
		resultsJson.add("objectName", ResultAggregatorFactory.asJsonObject(searchResult.getObjectName()));
		resultsJson.add("errorMessage", ResultAggregatorFactory.asJsonObject(searchResult.getErrorMessage()));
		if (searchResult.getResults() != null) {
			JsonArray resultRecordsJson = new JsonArray();
			for (ResultRecord resultRecord : searchResult.getResults()) {
				resultRecordsJson.add(ResultAggregatorFactory.asJsonObject(resultRecord));
			}
			resultsJson.add("results", resultRecordsJson);
		}
		return resultsJson;
	}
	
	private static SearchResult jsonAsSearchResult(JsonObject searchResultJson) {
		if (searchResultJson == null) return new SearchResult();
		String objectName = searchResultJson.getAsJsonPrimitive("objectName").getAsString();
		String errorMessage = searchResultJson.getAsJsonPrimitive("errorMessage").getAsString();
		SearchResult searchResult = new SearchResult(objectName);
		searchResult.setErrorMessage(errorMessage);
		JsonArray resultRecordsJson = searchResultJson.getAsJsonArray("results");
		Iterator<JsonElement> iterator = resultRecordsJson.iterator();
		while (iterator.hasNext()) {
			JsonElement resultRecordJson = iterator.next();
			searchResult.addResultRecord(ResultAggregatorFactory.jsonAsResultRecord((JsonObject)resultRecordJson));
		}
		return searchResult;
	}

	public static JsonObject asJsonObject(ResultField resultField) {
		JsonObject resultFieldJson = new JsonObject();
		if (resultField == null) return resultFieldJson;
		resultFieldJson.add("fieldName", new JsonPrimitive(resultField.getFieldName()));
		resultFieldJson.add("fieldType", new JsonPrimitive(resultField.getFieldType()));
		return resultFieldJson;
	}
	
	public static ResultField jsonAsReultField(JsonObject resultFieldJson) {
		ResultField resultField = new ResultField();
		resultField.setFieldName(resultFieldJson.getAsJsonPrimitive("fieldName").getAsString());
		resultField.setFieldType(resultFieldJson.getAsJsonPrimitive("fieldType").getAsString());
		return resultField;
	}

	public static JsonElement asJsonObject(Filter filter) {
		JsonObject filterJson = new JsonObject();
		if (filter == null) return filterJson;
		filterJson.add("fieldName", ResultAggregatorFactory.asJsonObject(filter.getFieldName()));
		filterJson.add("value", ResultAggregatorFactory.asJsonObject(filter.getValue()));
		filterJson.add("operator", ResultAggregatorFactory.asJsonObject(filter.getOperator()));
		filterJson.add("conjunction", ResultAggregatorFactory.asJsonObject(filter.getConjunction()));
		return filterJson;
	}

	private static Filter jsonAsFilter(JsonObject filterJson) {
		if (filterJson == null) return null;
		String fieldName = filterJson.getAsJsonPrimitive("fieldName").getAsString();
		String value = filterJson.getAsJsonPrimitive("value").getAsString();
		String operator = filterJson.getAsJsonPrimitive("operator").getAsString();
		String conjunction = filterJson.getAsJsonPrimitive("conjunction").getAsString();
		return new Filter(fieldName, value, operator, conjunction);		
	}
	
	public static JsonElement asJsonObject(SearchRequest request) {
		JsonObject searchRequestJson = new JsonObject();
		if (request == null) return searchRequestJson;
		
		searchRequestJson.add("objectName", ResultAggregatorFactory.asJsonObject(request.getObjectName()));
		searchRequestJson.add("fetchSize", ResultAggregatorFactory.asJsonObject(request.getFetchSize()));
		searchRequestJson.add("fetchPageNumber", ResultAggregatorFactory.asJsonObject(request.getPageNumber()));
		
		JsonObject filterCriteriaJson = new JsonObject();
		FilterCriteria fc = request.getFilterCriteria();
		if (fc != null) {
			JsonArray filtersJson = new JsonArray();
			for (Filter filter : fc.getFilters()) {
				filtersJson.add(ResultAggregatorFactory.asJsonObject(filter));
			}
			filterCriteriaJson.add("filterCriteria", filtersJson);
		}
		searchRequestJson.add("filterCriteria", filterCriteriaJson);
		
		JsonArray resultFieldsJson = new JsonArray();
		List<ResultField> resultFields = request.getResultFields();
		if (resultFields != null) {
			for (ResultField resultField : resultFields) {
				resultFieldsJson.add(ResultAggregatorFactory.asJsonObject(resultField));
			}
		}
		searchRequestJson.add("resultFields", resultFieldsJson);
		return searchRequestJson;
	}

	private static SearchRequest jsonAsSearchRequest(JsonObject searchRequestJson) {
		String objectName = searchRequestJson.getAsJsonPrimitive("objectName").getAsString();
		List<ResultField> resultFields = 
			ResultAggregatorFactory.jsonAsResultFieldList(
					searchRequestJson.getAsJsonArray("resultFields"));
		SearchRequest request = new SearchRequest(objectName, resultFields);
		request.setFetchSize(ResultAggregatorFactory.jsonAsInteger(searchRequestJson.getAsJsonPrimitive("fetchSize")));
		request.setPageNumber(ResultAggregatorFactory.jsonAsLong(searchRequestJson.getAsJsonPrimitive("fetchPageNumber")));
		
		JsonObject filterCriteriaJson = searchRequestJson.getAsJsonObject("filterCriteria");
		FilterCriteria fc = new FilterCriteria();
		JsonArray filtersJson = filterCriteriaJson.getAsJsonArray("filterCriteria");
		Iterator<JsonElement> iterator = filtersJson.iterator();
		while (iterator.hasNext()) {
			JsonElement filterJson = iterator.next();
			Filter filter = ResultAggregatorFactory.jsonAsFilter((JsonObject)filterJson);
			if (filter != null) fc.add(filter);
		}
		return request;
	}

	private static List<ResultField> jsonAsResultFieldList(JsonArray resultFieldsJson) {
		List<ResultField> resultFields = new ArrayList<ResultField>();
		if (resultFieldsJson == null) return resultFields;
		Iterator<JsonElement> iterator = resultFieldsJson.iterator();
		while (iterator.hasNext()) {
			JsonElement resultFieldJson = iterator.next();
			resultFields.add(ResultAggregatorFactory.jsonAsReultField((JsonObject)resultFieldJson));
		}
		return resultFields;
	}

	public static JsonObject asJsonObject(PivotCriteria pivotCriteria) {
		JsonObject pcJson = new JsonObject();
		if (pivotCriteria == null) return pcJson;
		pcJson.add("fieldName", ResultAggregatorFactory.asJsonObject(pivotCriteria.getFieldName()));
		pcJson.add("fieldType", ResultAggregatorFactory.asJsonObject(pivotCriteria.getFieldType()));
		pcJson.add("pivotFunction", ResultAggregatorFactory.asJsonObject(pivotCriteria.getPivotFunction()));
		return pcJson;
	}
	
	private static PivotCriteria jsonAsPivotCriteria(JsonObject pivotCriteriaJson) {
		String fieldName = pivotCriteriaJson.getAsJsonPrimitive("fieldName").getAsString();
		String fieldType = pivotCriteriaJson.getAsJsonPrimitive("fieldType").getAsString();
		String pivotFunction = pivotCriteriaJson.getAsJsonPrimitive("pivotFunction").getAsString();
		return new PivotCriteria(fieldName,fieldType,pivotFunction);		
	}
	
	public static String[] jsonAsStringArray(JsonArray jsonArray) {
		if (jsonArray == null) return new String[]{};
		return ResultAggregatorFactory.jsonAsStringList(jsonArray).toArray(new String[]{});
	}
	
	public static List<String> jsonAsStringList(JsonArray jsonArray) {
		if (jsonArray == null) return Collections.emptyList();
		List<String> values = new ArrayList<String>();
		Iterator<JsonElement> iterator = jsonArray.iterator();
		while(iterator.hasNext()) {
			JsonElement valueJson = iterator.next();
			values.add(valueJson.getAsString());
		}
		return values;
	}
	
	public static List<PivotCriteria> jsonAsPivotCriteriaArray(JsonArray jsonArray) {
		if (jsonArray == null) return new ArrayList<PivotCriteria>();
		List<PivotCriteria> pivotCriteriaList = new ArrayList<PivotCriteria>();
		Iterator<JsonElement> iterator = jsonArray.iterator();
		while(iterator.hasNext()) {
			JsonElement pivotCriteriaJson = iterator.next();
			pivotCriteriaList.add(ResultAggregatorFactory.jsonAsPivotCriteria((JsonObject)pivotCriteriaJson));
		}
		return pivotCriteriaList;
	}
	
	public static JsonObject asJsonObject(ResultAggregator resultAggregator) {
		JsonObject raJson = new JsonObject();
		
		if (resultAggregator == null) {
			return raJson;
		}
		
		// This step is needed for serialization into json,
		// it transforms result record values into raw values
		// instead of in typed values.
		resultAggregator.getAggregateSearchResult();
		
		if (resultAggregator.results != null) {
			
			// Json Serialization of SearchResult
			raJson.add("results", ResultAggregatorFactory.asJsonObject(resultAggregator.results));
			
			// Json Serialization of List<PivotCriteria>
			JsonArray pivotListJson = new JsonArray();
			if (resultAggregator.pivotList != null) {
				for (PivotCriteria pc : resultAggregator.pivotList) {
					pivotListJson.add(ResultAggregatorFactory.asJsonObject(pc));
				}
			}
			// End of Json Serialization of List<PivotCriteria>			
			raJson.add("pivotList", pivotListJson);
			
			// Json Serialization of uniqueFields
			JsonArray uniqueFieldsJson = new JsonArray();
			if (resultAggregator.uniqueFields != null) {
				for (String uf : resultAggregator.uniqueFields) {
					uniqueFieldsJson.add(new JsonPrimitive(uf));
				}
			}
			// End of Json Serialization of uniqueFields
			raJson.add("uniqueFields", uniqueFieldsJson);
			
			// Json Serialization of SearchRequest
			raJson.add("request", ResultAggregatorFactory.asJsonObject(resultAggregator.request));
			
			// Json serialization of Map<String, ResultRecord> aggMap
			JsonObject aggMapJson = new JsonObject();
			if (resultAggregator.aggMap != null) {
				JsonArray entriesJson = new JsonArray();
				for(Entry<String,ResultRecord> entry : resultAggregator.aggMap.entrySet()) {
					JsonObject entryJson = new JsonObject();
					entryJson.add("key", ResultAggregatorFactory.asJsonObject(entry.getKey()));
					entryJson.add("value", ResultAggregatorFactory.asJsonObject(entry.getValue()));
					entriesJson.add(entryJson);
				}
				aggMapJson.add("entries", entriesJson);
			}
			// End of Json serialization of Map<String, ResultRecord> aggMap
			raJson.add("aggMap", aggMapJson);			
		}
		
		return raJson;
	}
	
	/**
	 * PSR: Tuned
	 *    Only aggMap from the resultAggregator is serialized into JsonObject
	 *    other state members are not serialized.
	 *    
	 * @param resultAggregator
	 * @return
	 */
	public static JsonObject asJsonObjectOnlyAggMap(ResultAggregator resultAggregator) {
		JsonObject raJson = new JsonObject();
		
		if (resultAggregator == null) {
			return raJson;
		}
		
		// This step is needed for serialization into json,
		// it transforms result record values into raw values
		// instead of in typed values.
		resultAggregator.getAggregateSearchResult();
		
		//if (resultAggregator.results != null) {
			
			// Json Serialization of SearchResult
			//raJson.add("results", ResultAggregatorFactory.asJsonObject(resultAggregator.results));
			
			// Json Serialization of List<PivotCriteria>
//			JsonArray pivotListJson = new JsonArray();
//			if (resultAggregator.pivotList != null) {
//				for (PivotCriteria pc : resultAggregator.pivotList) {
//					pivotListJson.add(ResultAggregatorFactory.asJsonObject(pc));
//				}
//			}
//			// End of Json Serialization of List<PivotCriteria>			
//			raJson.add("pivotList", pivotListJson);
			
			// Json Serialization of uniqueFields
//			JsonArray uniqueFieldsJson = new JsonArray();
//			if (resultAggregator.uniqueFields != null) {
//				for (String uf : resultAggregator.uniqueFields) {
//					uniqueFieldsJson.add(new JsonPrimitive(uf));
//				}
//			}
//			// End of Json Serialization of uniqueFields
//			raJson.add("uniqueFields", uniqueFieldsJson);
			
			// Json Serialization of SearchRequest
			//raJson.add("request", ResultAggregatorFactory.asJsonObject(resultAggregator.request));
			
			// Json serialization of Map<String, ResultRecord> aggMap
			JsonObject aggMapJson = new JsonObject();
			if (resultAggregator.aggMap != null) {
				JsonArray entriesJson = new JsonArray();
				for(Entry<String,ResultRecord> entry : resultAggregator.aggMap.entrySet()) {
					JsonObject entryJson = new JsonObject();
					entryJson.add("key", ResultAggregatorFactory.asJsonObject(entry.getKey()));
					entryJson.add("value", ResultAggregatorFactory.asJsonObject(entry.getValue()));
					entriesJson.add(entryJson);
				}
				aggMapJson.add("entries", entriesJson);
			}
			// End of Json serialization of Map<String, ResultRecord> aggMap
			raJson.add("aggMap", aggMapJson);			
		//}
		
		return raJson;
	}
	
	public static ResultAggregator newResultAggregator(String json) {
		SearchResult results;
		String[] uniqueFields;
		List<PivotCriteria> pivotList;
		SearchRequest request;
		Map<String, ResultRecord> aggMap = new HashMap<String, ResultRecord>();
		
		JsonParser jsonParser = new JsonParser();
		JsonObject raJson = (JsonObject)jsonParser.parse(json);
		
		uniqueFields = ResultAggregatorFactory.jsonAsStringArray(raJson.getAsJsonArray("uniqueFields"));
		pivotList = ResultAggregatorFactory.jsonAsPivotCriteriaArray(raJson.getAsJsonArray("pivotList"));
		request = ResultAggregatorFactory.jsonAsSearchRequest(raJson.getAsJsonObject("request"));
		results = ResultAggregatorFactory.jsonAsSearchResult(raJson.getAsJsonObject("results"));
		JsonObject aggMapJson = raJson.getAsJsonObject("aggMap");
		
		JsonArray entriesJson = aggMapJson.getAsJsonArray("entries");
		Iterator<JsonElement> iterator = entriesJson.iterator();
		while (iterator.hasNext()) {
			JsonObject entryJson = (JsonObject)iterator.next();
			String key = entryJson.getAsJsonPrimitive("key").getAsString();
			ResultRecord resultRecord = ResultAggregatorFactory.jsonAsResultRecord(entryJson.getAsJsonObject("value"));
			// PSR: Tuned
			// Once retrieved from json object, removing it to reclaim memory.
			entryJson.remove("value");
			aggMap.put(key, resultRecord);
		}
		
		ResultAggregator ra = 
			ResultAggregator.instantiate(
					results, uniqueFields, pivotList, request, aggMap);
		
		return ra;
	}
	
	/**
	 * PSR: Tuned this accepts a stripped version of json string with only having
	 *      aggMap, other state members are not part of json.
	 * @param json
	 * @return
	 */
	public static ResultAggregator newResultAggregator(
			JsonObject json, SearchResult results, String[] uniqueFields, 
			List<PivotCriteria> pivotList, SearchRequest request) {
		Map<String, ResultRecord> aggMap = new HashMap<String, ResultRecord>();
		
		//JsonParser jsonParser = new JsonParser();
		//JsonObject raJson = (JsonObject)jsonParser.parse(json);
		JsonObject raJson = json;
		
		//uniqueFields = ResultAggregatorFactory.jsonAsStringArray(raJson.getAsJsonArray("uniqueFields"));
		//pivotList = ResultAggregatorFactory.jsonAsPivotCriteriaArray(raJson.getAsJsonArray("pivotList"));
		//request = ResultAggregatorFactory.jsonAsSearchRequest(raJson.getAsJsonObject("request"));
		//results = ResultAggregatorFactory.jsonAsSearchResult(raJson.getAsJsonObject("results"));
		JsonObject aggMapJson = raJson.getAsJsonObject("aggMap");
		
		JsonArray entriesJson = aggMapJson.getAsJsonArray("entries");
		log("Unmarshalling aggMap of Size: "+entriesJson.size());
		Iterator<JsonElement> iterator = entriesJson.iterator();
		while (iterator.hasNext()) {
			JsonObject entryJson = (JsonObject)iterator.next();
			String key = entryJson.getAsJsonPrimitive("key").getAsString();
			ResultRecord resultRecord = ResultAggregatorFactory.jsonAsResultRecord(entryJson.getAsJsonObject("value"));
			// PSR: Tuned
			// Once retrieved from json object, removing it to reclaim memory.
			entryJson.remove("value");
			aggMap.put(key, resultRecord);
		}
		
		ResultAggregator ra = 
			ResultAggregator.instantiate(
					results, uniqueFields, pivotList, request, aggMap);
		
		return ra;
	}
	
	private static AppLogger log = AppLogger.getLogger("ResultAggregatorFactory");
	private static void log(String msg) {
		log.info(msg);
	}
}
