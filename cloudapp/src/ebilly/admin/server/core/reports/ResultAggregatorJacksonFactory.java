package ebilly.admin.server.core.reports;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonToken;

import ebilly.admin.server.core.AppLogger;
import ebilly.admin.shared.viewdef.PivotCriteria;
import ebilly.admin.shared.viewdef.ResultRecord;
import ebilly.admin.shared.viewdef.SearchRequest;
import ebilly.admin.shared.viewdef.SearchResult;

public class ResultAggregatorJacksonFactory {
	
	private static AppLogger log = AppLogger.getLogger("ResultAggregatorJacksonFactory");
	private static void log(String msg) {
		log.info(msg);
	}
	
	public static void asJsonStream(ResultAggregator resultAggregator, JsonGenerator g) throws IOException {
		
		// This step is needed for serialization into json,
		// it transforms result record values into raw values
		// instead of in typed values.
		resultAggregator.getAggregateSearchResult();
		
		g.writeStartObject();
			g.writeFieldName("aggMap");
			g.writeStartArray();
				if (resultAggregator.aggMap != null) {
					for(Entry<String,ResultRecord> entry : resultAggregator.aggMap.entrySet()) {
						g.writeStartObject();
						g.writeStringField("key", entry.getKey());
						g.writeFieldName("value");
						asJsonStream(entry.getValue(),g);
						//g.writeString("<ResultRecord data here>");
						g.writeEndObject();
					}
				}
			g.writeEndArray();
		g.writeEndObject();
	}
	
	public static ResultAggregator newResultAggregator(
			org.codehaus.jackson.JsonParser jp, SearchResult results, String[] uniqueFields, 
			List<PivotCriteria> pivotList, SearchRequest request) throws JsonParseException, IOException {
		Map<String, ResultRecord> aggMap = new HashMap<String, ResultRecord>();
		
		if (jp.nextToken() != JsonToken.START_OBJECT) throw new IllegalArgumentException("Unexpected json");
		
		if (jp.nextToken() != JsonToken.FIELD_NAME)  throw new IllegalArgumentException("Unexpected json");
		
		JsonToken t = null;
		String fieldName = jp.getCurrentName();
		if (fieldName.equals("aggMap")) {
			t = jp.nextToken(); // begin of array
			while((t = jp.nextToken()) != JsonToken.END_ARRAY) {
				t = jp.nextToken(); // begin key field
				t = jp.nextToken(); // begin key value
				String key = jp.getText();
				t = jp.nextToken(); // begin value field
				
				ResultRecord record = jsonAsResultRecord(jp); 
				t = jp.nextToken(); // end object
				aggMap.put(key, record);
			}
		}
		
		ResultAggregator ra = 
			ResultAggregator.instantiate(
					results, uniqueFields, pivotList, request, aggMap);
		
		t = jp.nextToken(); // end object
		
		return ra;
	}
	
	private static void asJsonStream(ResultRecord resultRecord, JsonGenerator g) throws IOException {
		g.writeStartObject();
			g.writeFieldName("fieldValues");
			g.writeStartArray();
				if (resultRecord.fieldValues != null) {
					for (String fieldValue : resultRecord.fieldValues) {
						g.writeString(fieldValue);
					}
				}
			g.writeEndArray();
		g.writeEndObject();
	}
	
	public static ResultRecord jsonAsResultRecord(org.codehaus.jackson.JsonParser jp) 
	throws JsonParseException, IOException {
		if (jp == null) return null;
		JsonToken t = jp.nextToken(); 
		if (t != JsonToken.START_OBJECT) throw new IllegalArgumentException("Unexpected json"); // begin object
		t = jp.nextToken(); // field name
		String fieldName = jp.getText();
		if (fieldName.equals("fieldValues")) {
			List<String> list = ResultAggregatorJacksonFactory.jsonAsStringList(jp);
			t = jp.nextToken(); // end object;
			return new ResultRecord(list);
		} else {
			throw new IllegalArgumentException("Unexpected json");
		}
	}

	public static List<String> jsonAsStringList(
			org.codehaus.jackson.JsonParser jp) 
			throws JsonParseException, IOException {
		List<String> list = new ArrayList<String>();
		if (jp.nextToken() == JsonToken.START_ARRAY) {
			while(jp.nextToken() != JsonToken.END_ARRAY) {
				list.add(jp.getText());
			}
		}
		return list;
	}
	
	public static void asJsonStream(List<String> list, JsonGenerator g) throws JsonGenerationException, IOException {
		for (String fieldValue : list) {
			g.writeString(fieldValue);
		}
	}

	public static String jsonAsVersionString(
			org.codehaus.jackson.JsonParser jp, String mVersion) 
	throws JsonParseException, IOException {
		JsonToken t = jp.nextToken(); // begin Object;
		if (t != JsonToken.START_OBJECT) throw new IllegalArgumentException("Unexpected json");
		t = jp.nextToken(); // version field name
		t = jp.nextToken(); // version field value
		return jp.getText();
	}
}
