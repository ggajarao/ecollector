package ebilly.admin.server.core.reports;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ebilly.admin.server.core.CoreUtils;
import ebilly.admin.shared.viewdef.PivotCriteria;
import ebilly.admin.shared.viewdef.ResultField;
import ebilly.admin.shared.viewdef.ResultRecord;
import ebilly.admin.shared.viewdef.SearchRequest;
import ebilly.admin.shared.viewdef.SearchResult;

@SuppressWarnings("rawtypes")
public class ResultAggregator {
	
	SearchResult results;
	List<PivotCriteria> pivotList;
	String[] uniqueFields;
	SearchRequest request;
	Map<String, Class> fieldTypeMap;
	Map<String, ResultRecord> aggMap = new HashMap<String, ResultRecord>();
	
	static ResultAggregator instantiate(
			SearchResult results, String[] uniqueFields,
			List<PivotCriteria> pivotList, SearchRequest request,
			Map<String, ResultRecord> aggMap) {
		ResultAggregator ra = new ResultAggregator(results, uniqueFields, pivotList, request);
		ra.aggMap = aggMap;
		return ra;
	}
	
	public ResultAggregator(SearchResult results, String[] uniqueFields, 
			List<PivotCriteria> pivotList,
			SearchRequest request) {
		this.results = results;
		this.uniqueFields = uniqueFields;
		this.pivotList = pivotList;
		this.request = request;
		initFieldTypeMap();
		this.results.clearFieldTypedData();
	}
	
	private void initFieldTypeMap() {
		this.fieldTypeMap = new HashMap<String, Class>();
		List<ResultField> resultFields = request.getResultFields();
		for (ResultField rf : resultFields) {
			Class<?> clazz = java.lang.String.class;
			try {
				clazz = Class.forName(rf.getFieldType());
			} catch(Exception e) {}
			this.fieldTypeMap.put(rf.getFieldName(),clazz);
		}
	}
	
	public SearchResult getAggregateSearchResult() {
		return prepareAggregateSearchResult();
	}
	
	public SearchResult applyAggregation() {
		this.aggregateRecords(this.results);
		return prepareAggregateSearchResult();
	}
	
	public SearchResult getAggregateSearchResultWithTypedData() {
		this.aggregateRecords(this.results);
		return prepareAggregateSearchResultWithTypedData();
	}
	
	/**
	 * PSR Note - results.getResults().clear() is called
	 * to retain memory. Callers should note this.
	 * 
	 * To retain results use aggregateRecordsRetainResults(SearchResult results) 
	 * instead.
	 * 
	 * @param results
	 */
	public void aggregateRecords(SearchResult results) {
		ResultRecord[] records = 
			(ResultRecord[])results.getResults().toArray(
					new ResultRecord[results.getResults().size()]);
		// PSR: Tuned.
		// Once the aggregation is applied,
		// we should not retain the results, 
		// as its not used further.
		results.getResults().clear();
		
		//for (ResultRecord record : records) {
		for (int i = 0; i < records.length; i++) {
			ResultRecord record = records[i];
			aggregateRecord(record);
			// PSR: Tuned.
			// Once the aggregation is applied,
			// we should not retain the record in results, 
			// as its not used further.
			records[i] = null;
		}
	}
	
	/**
	 * This is memory expensive as results state is preserved
	 * during the aggregation process. For memory intensive usage
	 * use aggregateRecords(SearchResult results) instead.
	 * 
	 * @param results
	 */
	public void aggregateRecordsRetainResults(SearchResult results) {
		ResultRecord[] records = 
			(ResultRecord[])results.getResults().toArray(
					new ResultRecord[results.getResults().size()]);
		for (int i = 0; i < records.length; i++) {
			ResultRecord record = records[i];
			aggregateRecord(record);
			// PSR: Tuned.
			// Once the aggregation is applied,
			// we should not retain the record in results, 
			// as its not used further.
			records[i] = null;
		}
	}
	
	public void aggregateRecord(ResultRecord record) {
		initTypedData(record);
		
		// PSR: can avoid Map lookup aggMap.get(*) by one call
		// if this method returns the boolean which is returned
		// in "aggregateExists(*)" method.
		//
		// DONE - AVOIDED BOOLEAN, AND MOVED THE 
		//        METHODS pivot(..), initCounters(..) 
		//        TO aggregateFor(..) METHOD.
		//
		//boolean aggregateExists = this.aggregateExists(record);
		/*ResultRecord aggRecord = */this.aggregateFor(record);
		/*if (aggregateExists) {
			this.pivot(aggRecord, record);
		} else {
			this.initCounters(aggRecord);
		}*/
	}
	
	private SearchResult prepareAggregateSearchResult() {
		List<ResultRecord> aggRecords = new ArrayList<ResultRecord>();
		for (ResultRecord r : this.aggMap.values()) {
			/*this.typedDataToFieldValues(this.request.getResultFields(), r);
			aggRecords.add(r);*/
			aggRecords.add(request.convertToRawRecord(r));
		}
		return new SearchResult(aggRecords);
	}
	
	private SearchResult prepareAggregateSearchResultWithTypedData() {
		List<ResultRecord> aggRecords = new ArrayList<ResultRecord>();
		for (ResultRecord r : this.aggMap.values()) {
			initTypedData(r);
			aggRecords.add(r);
		}
		return new SearchResult(aggRecords);
	}
	
	private ResultRecord aggregateFor(ResultRecord record) {
		//String id = this.request.getValue(this.uniqueField, record);
		String id = this.getUniqueRecordId(record);
		ResultRecord aggRecord = this.aggMap.get(id);
		if (aggRecord == null) {
			aggRecord = new ResultRecord(record.fieldValues);
			aggMap.put(id, aggRecord);
			this.initTypedData(aggRecord);
			
			// Clear fieldValues to save memory
			// these values are redundant as
			// we converted them into typed data.
			aggRecord.fieldValues.clear();
			this.initCounters(aggRecord);
		} else {
			this.pivot(aggRecord, record);
		}
		return aggRecord;
	}
	
	private String getUniqueRecordId(ResultRecord record) {
		StringBuffer sb = new StringBuffer();
		for (String idEl : this.uniqueFields) {
			sb.append(this.request.getValue(idEl, record));
		}
		return sb.toString();
	}

	/*
	 // During PSR tuning, avoided this method usage.
	 private boolean aggregateExists(ResultRecord record) {
		//String id = this.request.getValue(this.uniqueField, record);
		String id = this.getUniqueRecordId(record);
		return this.aggMap.get(id) != null;
	}*/
	
	private void pivot(ResultRecord aggregate, ResultRecord record) {
		initTypedData(aggregate);
		for (PivotCriteria p : pivotList) {
			String fieldName = CoreUtils.toBeanProperty(p.getFieldName());
			if (p.getPivotFunction().equals("SUM")) {
				
				//initTypedData(record);
				
				if (p.getFieldType().equals(java.lang.Long.class.getName())) {
					long aggValue = aggregate.longValue(fieldName);
					long recValue = record.longValue(fieldName); 
					long sumValue = aggValue + recValue;
					aggregate.fieldTypedValueMap.put(fieldName,
							sumValue);
					if (recValue > 0) {
						aggregate.bumpCounter(fieldName);
					}
				} else if (p.getFieldType().equals(java.lang.Double.class.getName())) {
					double aggValue = aggregate.doubleValue(fieldName);
					double recValue = record.doubleValue(fieldName); 
					double sumValue = aggValue + recValue;
					aggregate.fieldTypedValueMap.put(fieldName,
							sumValue);
					if (recValue > 0) {
						aggregate.bumpCounter(fieldName);
					}
				} else if (p.getFieldType().equals(java.lang.Integer.class.getName())) {
					int aggValue = aggregate.intValue(fieldName);
					int recValue = record.intValue(fieldName); 
					int sumValue = aggValue + recValue;
					aggregate.fieldTypedValueMap.put(fieldName,
							sumValue);
					if (recValue > 0) {
						aggregate.bumpCounter(fieldName);
					}
				}				
			} else if (p.getPivotFunction().equals("COUNT")) {
				//if (p.getFieldType().equals(java.lang.Long.class.getName())) {
					
				long count = 0;
				/* For the first access of aggregate.longValue(fieldName) 
				 * if the field type is not Long then we get NPE but we 
				 * are supposed to COUNT these fields we should expect 
				 * field type can be any data type.
				 */
				try {
					count = aggregate.longValue(fieldName);
				} catch (NumberFormatException nfe) {}
				aggregate.fieldTypedValueMap.put(fieldName,++count);
				/*} else {
					throw new IllegalArgumentException("Aggregation Count on field types other than long are not supported");
				}*/
			}
		}
	}
	
	private void initCounters(ResultRecord record) {
		initTypedData(record);
		for (PivotCriteria p : pivotList) {
			String fieldName = CoreUtils.toBeanProperty(p.getFieldName());
			if (p.getPivotFunction().equals("SUM")) {
				if (p.getFieldType().equals(java.lang.Long.class.getName())) {
					long recValue = record.longValue(fieldName); 
					if (recValue > 0) {
						record.bumpCounter(fieldName);
					}
				} else if (p.getFieldType().equals(java.lang.Double.class.getName())) {
					double recValue = record.doubleValue(fieldName); 
					if (recValue > 0) {
						record.bumpCounter(fieldName);
					}
				} else if (p.getFieldType().equals(java.lang.Integer.class.getName())) {
					int recValue = record.intValue(fieldName); 
					if (recValue > 0) {
						record.bumpCounter(fieldName);
					}
				}				
			}
		}
	}

	private void initTypedData(ResultRecord record) {
		if (record.fieldTypedValueMap == null) {
			record.fieldTypedValueMap = 
				this.typedData(this.request.getResultFields(), 
						record.fieldValues, this.fieldTypeMap);
			for (PivotCriteria p : this.pivotList) {
				if (p.getPivotFunction().equals("COUNT")) {
					record.fieldTypedValueMap.put(
							CoreUtils.toBeanProperty(p.getFieldName()), 1L);
				}
			}
		}
	}
	
	private List<String> fieldNameSet = null; 
	private Map<String, Object> typedData(List<ResultField> fields, 
			List<String> fieldValues,
			Map<String, Class> fieldTypeMap) {
		String[] fieldNames = new String[fields.size()];
		String[] values = new String[fields.size()];
		for (int i = 0; i < fields.size(); i++) {
			ResultField rf = fields.get(i);
			fieldNames[i] = rf.getFieldName();
			values[i] = fieldValues.get(i);
		}
		
		if (fieldNameSet == null) {
			fieldNameSet = new ArrayList<String>();//this.request.getResultFieldNames();
		}
		
		//return CoreUtils.toFieldTypedValuemap(fieldNames, values, fieldTypeMap);
		Map<String, Object> typedData = new FixedKeyMap<String, Object>(fieldNameSet);
		CoreUtils.toFieldTypedValuemap(fieldNames, values, fieldTypeMap, typedData);
		return typedData;
	}
	
	// METHOD REFACTORED TO SearchRequest.convertToRawRecord(ResultRecord)
	/*private void typedDataToFieldValues(List<ResultField> fields, ResultRecord record) {
		if (record.fieldTypedValueMap == null || 
			record.fieldTypedValueMap.isEmpty()) {
			// If typed value map is empty or non existing then there is
			// nothing to do.
			return;
		}
		record.fieldValues.clear();
		for (ResultField rf : fields) {
			record.fieldValues.add(""+record.fieldTypedValueMap.get(
					CoreUtils.toBeanProperty(rf.getFieldName())));
		}
		// PSR: Tuned.
		// Saving memory, since we are transforming the typed data
		// into fieldValues. When fieldTypedValueMap is
		// required we should transform again using initTypedData()
		// method.
		record.fieldTypedValueMap.clear();
	}*/
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof ResultAggregator)) {
			return false;
		}
		ResultAggregator ra1 = this;
		ResultAggregator ra2 = (ResultAggregator)obj;
		if (ra1 == ra2) return true;
		
		if (!ra1.aggMap.equals(ra2.aggMap)) return false;
		
		if (!ra1.fieldTypeMap.equals(ra2.fieldTypeMap)) return false;
		if (!ra1.pivotList.equals(ra2.pivotList)) return false;
		if (!ra1.request.equals(ra2.request)) return false;
		
		if (ra1.uniqueFields.length != ra2.uniqueFields.length) return false;
		for (int i = 0; i < ra1.uniqueFields.length; i++) {
			if (!ra1.uniqueFields[i].equals(ra2.uniqueFields[i])) return false;
		}
		return true;
	}

	public SearchRequest getSearchRequest() {
		return this.request;
	}
}
