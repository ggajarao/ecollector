package ebilly.admin.server.core.reports;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.collections.iterators.EntrySetMapIterator;

public class FixedKeyMap<K, V> implements Map<K, V> {
	
	List<K> keys;
	List<V> values;
	
	public FixedKeyMap(List<K> keySet) {
		keys = keySet;
		values = new ArrayList<V>();
		for (int i=0; i < keys.size(); i++) {
			values.add(null);
		}
	}
	
	@Override
	public int size() {
		return keys.size();
	}

	@Override
	public boolean isEmpty() {
		return values.isEmpty();
	}

	@Override
	public boolean containsKey(Object key) {
		return keys.contains(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return values.contains(value);
	}

	@Override
	public V get(Object key) {
		int i = keys.indexOf(key);
		if ( i == -1 ) return null;
		return values.get(i);
	}

	@Override
	public V put(K key, V value) {
		int i = keys.indexOf(key);
		if ( i == -1 ) {
			keys.add(key);
			values.add(value);
		} else {
			values.remove(i);
			values.add(i, value);
		}
		return value;
	}

	@Override
	public V remove(Object key) {
		int i = keys.indexOf(values);
		if ( i == -1 ) {
			return null;
		}
		V value = values.remove(i);
		//keys.remove(i);
		return value;
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> m) {
		throw new IllegalArgumentException("SimpleMap does not support putAll method");
	}

	@Override
	public void clear() {
		//keys.clear();
		values.clear();
	}

	@Override
	public Set<K> keySet() {
		return new HashSet<K>(keys);
	}

	@Override
	public Collection<V> values() {
		return new ArrayList<V>(values);
	}

	@Override
	public Set<java.util.Map.Entry<K, V>> entrySet() {
		throw new IllegalArgumentException("SimpleMap does not support entrySet method");
	}
	
	public static void main(String[] args) {
		List<String> keySet = new ArrayList<String>();
		keySet.add("Integer");
		keySet.add("Double");
		keySet.add("Long");
		Map<String, Object> sm = new FixedKeyMap<String, Object>(keySet);
		sm.put("Integer", new Integer(1));
		sm.put("Integer", new Integer(2));
		sm.put("Double", new Double(2.0));
		sm.put("Long", new Long(12));
		
		System.out.println("Integer: "+(Integer)sm.get("Integer"));
		System.out.println("Long: "+(Long)sm.get("Long"));
		System.out.println("Double: "+(Double)sm.get("Double"));
	}
		
}
