package ebilly.admin.server.core.db;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManagerFactory;

import org.apache.commons.beanutils.BeanUtils;

import ebilly.admin.server.Util;
import ebilly.admin.server.core.AppLogger;
import ebilly.admin.server.core.CoreUtils;

public class DBUtils {
	
	private static final AppLogger log = AppLogger.getLogger(DBUtils.class.getName());
	
    private static final PersistenceManagerFactory pmfInstance =
        JDOHelper.getPersistenceManagerFactory("transactions-optional");

    public static PersistenceManagerFactory getPMF() {
        return pmfInstance;
    }
    
    public static Object copyProperties(Object source, Class<?> targetClass) 
	throws IllegalAccessException, InvocationTargetException
	{
		try {
			Object newCopy = Class.forName(targetClass.getName());
			BeanUtils.copyProperties(newCopy, source);
			return newCopy;
		}
		catch(Exception e)
		{
			log.info("Exception while converting to ui object ",e);
		}
		return null;
	}

	public static void copyProperties(Object source, Object target)
	{
		Map<String, Object> propValues;
		try {
			propValues = toFieldValueMap(source);
		}
		catch(Exception e)
		{
			String msg = "Exception while copyProperties source: "+source+
				", target: "+target+", Error: "+e.getMessage();
			log.info(msg,e);
			throw new RuntimeException(msg,e);
		}
		copyPropertiesFromMap(target,propValues);
	}
	
	/* Use this in case you have bean properties with all uppercase letters */
	public static void copyPropertiesV2(Object source, Object target)
	{
		Map<String, Object> propValues;
		Map<String, Object> propValuesUpperKeys = new HashMap<String, Object>();
		try {
			propValues = toFieldValueMap(source);
			for (Entry<String, Object> e : propValues.entrySet()) {
				propValuesUpperKeys.put(e.getKey().toUpperCase(), e.getValue());
			}
		}
		catch(Exception e)
		{
			String msg = "Exception while copyProperties source: "+source+
				", target: "+target+", Error: "+e.getMessage();
			log.info(msg,e);
			throw new RuntimeException(msg,e);
		}
		copyPropertiesFromMap(target,propValuesUpperKeys);
	}
	
	public static void copyPropertiesFromMap(Object target, 
			Map<String, ?> propValues)
	{
		try {
			filterNullValues(propValues);
			BeanUtils.populate(target, propValues);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			String msg = "Exception while copyProperties into: "+target+
				"from map: " + propValues + ", Error: "+e.getMessage();
			log.info(msg,e);
			throw new RuntimeException(msg,e);
		}
	}
	
	private static void filterNullValues(Map<String, ?> propValues) {
		
		List<String> nullProps = new ArrayList<String>(50);
		Iterator<String> iterator = propValues.keySet().iterator();
		while (iterator.hasNext()) {
			String prop = iterator.next();
			Object propValue = propValues.get(prop);
			if (propValue == null) {
				nullProps.add(prop);
			}/* else {
				if (propValue instanceof String) {
					if (((String)propValue).trim().equals("")) {
						nullProps.add(prop);
					}
				}
			}*/
		}
		
		for (String prop : nullProps) {
			propValues.remove(prop);
		}
	}
	
	/**
	 * returns a Map key = declared field name ( in given clazz )
	 *               value = data type of the field
	 * @param clazz
	 * @return
	 */
	public static Map<String, Class> fieldDataTypeMap(Class clazz) {
		Map<String, Class> fieldTypeMap  = new HashMap<String, Class>();
		Field[] declaredFields = clazz.getDeclaredFields();
		for (Field declaredField : declaredFields ) {
			fieldTypeMap.put(CoreUtils.toBeanProperty(declaredField.getName()),
							declaredField.getType());
		}
		return fieldTypeMap;
	}
	
	/**
	 * returns a Map key = declared field name ( in given clazzes )
	 *               value = data type of the field
	 * @param clazz
	 * @return
	 */
	public static Map<String, Class> fieldDataTypeMap(Class[] clazzes) {
		Map<String, Class> fieldTypeMap  = new HashMap<String, Class>();
		for (Class clazz : clazzes) {
			Map<String, Class> clazzFieldTypeMap =
				DBUtils.fieldDataTypeMap(clazz);
			for (Entry<String,Class> entry : clazzFieldTypeMap.entrySet()) {
				/*if (fieldTypeMap.containsKey(entry.getKey())) {
					throw new IllegalArgumentException("Duplicate field: "+entry.getKey()
							+", this field is appeared more than once in of the given set of classes: "
							+Arrays.toString(clazzes));
				} else {*/
					fieldTypeMap.put(entry.getKey(), entry.getValue());					
				/*}*/
			}
		}
		return fieldTypeMap;
	}

	/**
	 * Return Map will contain key = name of getter and its associated value
	 * 
	 * @param object
	 * @return
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	public static Map<String,Object> toFieldValueMap(Object object)
    {
		return DBUtils.toFieldValueMap(object, new ArrayList<String>());
    }
	
	/**
	 * return Map will only have the given fields
	 *  
	 * @param object
	 * @param fields
	 * @return
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	public static Map<String,Object> toFieldValueMap(Object object, List<String> fields)
    {
        Map<String,Object> fieldValueMap = new HashMap<String,Object>();
        for (Method method : object.getClass().getMethods()) {
            String methodName = method.getName();
            if(methodName.startsWith("get") || methodName.startsWith("is")) 
            {
                try 
                {
                	Object value = method.invoke(object);
                    if (methodName.startsWith("get")) {
                        methodName = methodName.substring(3);
                    }
                    else if(methodName.startsWith("is")){
                        methodName = methodName.substring(2);
                    }
                    fieldValueMap.put(CoreUtils.toBeanProperty(methodName), value);
                }
                catch (Exception e) {
                	log.info("Exception while invoking method: "+method+
                			" which is while setting object data for method: "
                            + methodName+", object: "+object, e);
                }
            }
        }
        return fieldValueMap;
    }

	public static Map<String, String> toFieldStringValueMap(Object dataObject,
			Map<String, Class> fieldDataTypeMap) {
		Map<String, String> fieldProperties = new HashMap<String, String>();
		Map<String, Object> fieldValueMap = DBUtils.toFieldValueMap(dataObject);
		for (Entry<String, Object> entry : fieldValueMap.entrySet()) {
			fieldProperties.put(entry.getKey(), 
					DBUtils.convertToStringValue(
					entry.getValue(), fieldDataTypeMap.get(entry.getKey())));
		}
		return fieldProperties;
	}
	
	public static String convertToStringValue(Object value, Class type) {
		if (type == null) {
			return value + "";
		}
		
		if ( type.equals(java.lang.String.class)) {
			return ((value==null)?"":value+"");
		} else if (type.equals(java.lang.Integer.class)||
				type.getName().equals("int")) {
			return value+"";
		} else if (type.equals(java.lang.Double.class) ||
				type.getName().equals("double")) {
			return value+"";
		} else if (type.equals(java.lang.Long.class)||
				type.getName().equals("long")) {
			return value+"";
		} else if (type.equals(java.lang.Boolean.class)||
				type.getName().equals("boolean")) {
			return value+"";
		} else if (type.equals(java.util.Date.class)) {
			if (value != null) {
				Date dateValue = (Date)value;
				return Util.getFormattedDate(dateValue);
			} else {
				return null;
			}
		} else if (type.equals(java.util.List.class) ||
				   type.equals(java.util.Set.class)){
			return "";
		} else {
			throw new IllegalArgumentException(
				"Conversion error. Unsupported type: "+type+", value: "+value);
		}
	}
}