package ebilly.admin.server.core.db;

import java.io.Serializable;
import java.util.Date;

import javax.jdo.PersistenceManager;
import javax.persistence.PersistenceException;

import ebilly.admin.server.core.AppLogger;

public abstract class BaseEntityNonPersistent implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final AppLogger log = AppLogger.getLogger(BaseEntityNonPersistent.class.getName());
	
	
	public BaseEntityNonPersistent() {
		this.setLastModifiedDate(getCurrentDate());
	}
	
	public abstract Date getCreatedDate();

	public abstract void setCreatedDate(Date createdDate);

	public abstract Date getLastModifiedDate();

	public abstract void setLastModifiedDate(Date lastModifiedDate);
	
	public abstract void setChangeNumber(int changeNumber);
	
	public abstract int getChangeNumber();

	public abstract String getId();
	
	// we should prevent this property from
	// bean population, hence renamed
	// this property from setId to setEntityId
	public abstract void setEntityId(String id);
	
	public abstract int getActive();

	public abstract void setActive(int active);
	
	protected void setLifecyclAttributes() {
		if (this.isInMemory()) {
			this.setCreatedDate(getCurrentDate());
			this.setActive(1);
		}
		this.setChangeNumber(getChangeNumber() + 1);
		this.setLastModifiedDate(getCurrentDate());		
	}
	
	protected boolean isInMemory() {
		return this.getId() == null;
	}
	
	protected static Date getCurrentDate() {
		return new Date(System.currentTimeMillis());
	}
	
	public void save()
	{
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			setLifecyclAttributes();
			onBeforeSave();
			pm.makePersistent(this);
			onAfterSave();
		}
		catch(Exception e)
		{
			log.info("Exception whlie saving entity", e);
			throw new PersistenceException(
					"Exception while saving "+this,e);
		}
		finally {
			pm.close();
		}
	}
	
	/**
	 * Override in specific class, 
	 * to get notified after saving entity
	 */
	protected void onAfterSave() {
		
	}

	// Overrided in specific classes,
	// to get a chance before entity is saved
	protected void onBeforeSave() {
		
	}
	
	protected BaseEntityNonPersistent fetchById() {
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			BaseEntityNonPersistent c = pm.getObjectById(this.getClass(), getUniqueId());
			return pm.detachCopy(c);
		} catch (javax.jdo.JDOObjectNotFoundException e ) {
			/*Logger.log(this.getClass().getName()+" - Could not fetch object with code"
					+getUniqueId(), e);*/
		}
		return null;
	}

	protected abstract Object getUniqueId();
	
	public void simplePersist() {
		BaseEntityNonPersistent dbRec = this.fetchById();
		if (dbRec != null) {
			DBUtils.copyProperties(this, dbRec);
			dbRec.save();
			DBUtils.copyProperties(dbRec, this);
			this.setEntityId(dbRec.getId());
		} else {
			this.save();			
		}	
	}

	@Override
	public String toString() {
		return "id=" + getId() + ", createdDate=" + getCreatedDate()
				+ ", lastModifiedDate=" + getLastModifiedDate() + ", active="
				+ getActive() + ", changeNumber=" + getChangeNumber();
	}
}
