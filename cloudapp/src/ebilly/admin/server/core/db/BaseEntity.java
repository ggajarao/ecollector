package ebilly.admin.server.core.db;

import java.io.Serializable;
import java.util.Date;

import javax.jdo.PersistenceManager;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import javax.persistence.PersistenceException;

import ebilly.admin.server.core.AppLogger;

@PersistenceCapable(identityType = IdentityType.APPLICATION, detachable="true")
@Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
public abstract class BaseEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private static final AppLogger log = AppLogger.getLogger(BaseEntity.class.getName());
	
	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
	private String id;
	// In an attempt to use the entity id in UI and back to data layer
	//http://code.google.com/appengine/docs/java/datastore/jdo/creatinggettinganddeletingdata.html
    //private Key id;
	
	@Persistent
	private Date createdDate;
	
	@Persistent
	private Date lastModifiedDate;
	
	@Persistent
	private int active = 1;
	
	@Persistent
	private int changeNumber;
	
	public BaseEntity() {
		this.setLastModifiedDate(getCurrentDate());
	}
	
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getId() {
		return id;
	}
	
	// we should prevent this property from
	// bean population, hence renamed
	// this property from setId to setEntityId
	public void setEntityId(String id) {
		this.id = id;
	}
	
	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}
	
	private void setLifecyclAttributes() {
		if (this.isInMemory()) {
			this.setCreatedDate(getCurrentDate());
			this.setActive(1);
		}
		this.changeNumber++;
		this.setLastModifiedDate(getCurrentDate());		
	}
	
	protected boolean isInMemory() {
		return this.getId() == null;
	}
	
	protected static Date getCurrentDate() {
		return new Date(System.currentTimeMillis());
	}
	
	public void save()
	{
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			setLifecyclAttributes();
			onBeforeSave();
			pm.makePersistent(this);
			onAfterSave();
		}
		catch(Exception e)
		{
			log.info("Exception whlie saving entity", e);
			e.printStackTrace();
			throw new PersistenceException(
					"Exception while saving "+this,e);
		}
		finally {
			pm.close();
		}
	}
	
	/**
	 * Override in specific classes,
	 * to get a notification after entity save
	 */
	protected void onAfterSave() {
		
	}

	// Overrided in specific classes,
	// to get a chance before entity is saved
	protected void onBeforeSave() {
		
	}
	
	protected BaseEntity fetchById() {
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			BaseEntity c = pm.getObjectById(this.getClass(), getUniqueId());
			return pm.detachCopy(c);
		} catch (javax.jdo.JDOObjectNotFoundException e ) {
			/*Logger.log(this.getClass().getName()+" - Could not fetch object with code"
					+getUniqueId(), e);*/
		}
		return null;
	}

	protected abstract Object getUniqueId();
	
	public void simplePersist() {
		BaseEntity dbRec = this.fetchById();
		if (dbRec != null) {
			DBUtils.copyProperties(this, dbRec);
			dbRec.save();
			DBUtils.copyProperties(dbRec, this);
			this.setEntityId(dbRec.getId());
		} else {
			this.save();			
		}	
	}

	@Override
	public String toString() {
		return "id=" + id + ", createdDate=" + createdDate
				+ ", lastModifiedDate=" + lastModifiedDate + ", active="
				+ active + ", changeNumber=" + changeNumber;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseEntity other = (BaseEntity) obj;
		if (active != other.active)
			return false;
		if (changeNumber != other.changeNumber)
			return false;
		if (createdDate == null) {
			if (other.createdDate != null)
				return false;
		} else if (!createdDate.equals(other.createdDate))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lastModifiedDate == null) {
			if (other.lastModifiedDate != null)
				return false;
		} else if (!lastModifiedDate.equals(other.lastModifiedDate))
			return false;
		return true;
	}
}
