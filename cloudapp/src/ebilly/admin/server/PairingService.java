package ebilly.admin.server;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ebilly.admin.server.bo.DeviceBo;
import ebilly.admin.shared.CommonUtil;

public class PairingService extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		process(req,resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		process(req,resp);
	}
	
	private void process(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String action = req.getParameter("a");
		String param1 = req.getParameter("1");
		String param2 = req.getParameter("2");
		if (CommonUtil.isEmpty(action)) {
			respond(":)",resp);
			return;
		}
		if ("P".equalsIgnoreCase(action)) { // PAIR WITH GIVEN PAIRKEY
			if (CommonUtil.isEmpty(param1)) {
				respond(":|",resp);
				return;
			}
			respond(DeviceBo.pair(param1),resp);
			return;
		} else if ("O".equalsIgnoreCase(action)) { //  PAIRING DONE WITH GIVEN PAIRKEY
			if (CommonUtil.isEmpty(param1)) {
				respond(":|",resp);
				return;
			}
			if (CommonUtil.isEmpty(param2)) {
				respond(":(",resp);
				return;
			}
			respond(DeviceBo.pairingAcknowledge(param1,param2),resp);
			return;
		} else {
			respond(":((",resp);
			return;
		}
	}

	private void respond(String message, HttpServletResponse resp) 
	throws IOException {
		resp.setContentType("text/text");
		resp.getWriter().println(message);
	}
}