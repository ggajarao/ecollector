package ebilly.admin.server;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobInfoFactory;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;

import ebilly.admin.shared.CommonUtil;

public class FileServer extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		process(req,resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		process(req,resp);
	}
	
	private void process(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String action = req.getParameter("a");
		String sBlobKey = req.getParameter("bk");
		if (CommonUtil.isEmpty(sBlobKey)) {
			resp.setContentType("text/html");
			resp.getWriter().println("bk param missing !!!");
			return;
		}
		BlobKey blobKey = new BlobKey(sBlobKey);
		if ("r".equalsIgnoreCase(action)) { // Read
			readAction(blobKey, req, resp);
		} else if ("d".equalsIgnoreCase(action)) { // Delete
			deleteAction(blobKey, req, resp);
		} else {
			defaultAction(req, resp);
		}
	}

	private void readAction(BlobKey blobKey,
			HttpServletRequest req, HttpServletResponse resp) throws IOException {
		BlobstoreService blobstoreService = 
			BlobstoreServiceFactory.getBlobstoreService();
		
		BlobInfoFactory f = new BlobInfoFactory();
		BlobInfo blobInfo = f.loadBlobInfo(blobKey);
		
		blobstoreService.serve(blobKey, resp);
		if (blobInfo != null) {
			String fileName = blobInfo.getFilename();
			resp.setContentType(blobInfo.getContentType());
			resp.setHeader("Content-Disposition","inline;filename="+fileName);
		}
	}

	private void defaultAction(HttpServletRequest req, HttpServletResponse resp) 
	throws IOException {
		resp.setContentType("text/html");
		StringBuffer sb = new StringBuffer();
		resp.getWriter().println(sb.toString());
	}

	private void deleteAction(BlobKey blobKey, HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		BlobstoreService blobstoreService = 
			BlobstoreServiceFactory.getBlobstoreService();
		blobstoreService.delete(blobKey);
		resp.getWriter().println("");		
	}	
}