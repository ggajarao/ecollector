package ebilly.admin.server;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ebilly.admin.server.core.AppLogger;
import ebilly.admin.server.db.DevicePr;

public class UpgradeHandler extends HttpServlet {
	
	private static final AppLogger log = AppLogger.getLogger(UpgradeHandler.class.getName());
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
	throws ServletException, IOException {
		handleRequest(req,resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
	throws ServletException, IOException {
		handleRequest(req,resp);
	}

	private void handleRequest(HttpServletRequest req, HttpServletResponse resp) 
	throws ServletException, IOException {
		log.info("UpgradeHandlerHandler::handleRequest method entry");
		resp.setContentType("text/plain");
		
		try {
			List<DevicePr> devicePrs = DevicePr.fetchAllDevicePrs();
			Exception lastException = null;
			for (DevicePr d : devicePrs) {
				try {
					d.save();
				} catch(Exception e) {
					lastException = e;
				}
			}
			if (lastException != null) {
				respond(lastException.getMessage(),resp);
				lastException.printStackTrace();
				log.error("UpgradeHandler::exception saving device pr", lastException);
			} else {
				respond("OK",resp);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("UpgradeHandler::handleRequest error", e);
			//resp.getOutputStream().print("Error: "+e.getMessage());
		}
		log.info("UpgradeHandlerHandler::handleRequest method exit");
	}
	
	private void respond(String message, HttpServletResponse resp) 
	throws IOException {
		resp.setContentType("text/text");
		resp.getWriter().println(message);
	}
}

