package ebilly.admin.server;

import static com.google.appengine.api.taskqueue.TaskOptions.Builder.withUrl;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import javax.cache.Cache;
import javax.cache.CacheException;
import javax.cache.CacheFactory;
import javax.cache.CacheManager;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import com.google.appengine.api.appidentity.AppIdentityService;
import com.google.appengine.api.appidentity.AppIdentityServiceFactory;
import com.google.appengine.api.backends.BackendService;
import com.google.appengine.api.backends.BackendServiceFactory;
import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobInfoFactory;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.files.AppEngineFile;
import com.google.appengine.api.files.FileReadChannel;
import com.google.appengine.api.files.FileService;
import com.google.appengine.api.files.FileServiceFactory;
import com.google.appengine.api.files.FileWriteChannel;
import com.google.appengine.api.memcache.stdimpl.GCacheFactory;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.tools.cloudstorage.GcsFileOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsInputChannel;
import com.google.appengine.tools.cloudstorage.GcsOutputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.gson.Gson;

import ebilly.admin.server.core.AppLogger;
import ebilly.admin.server.db.DbAppConfig;
import ebilly.admin.server.db.DevicePr;
import ebilly.admin.server.db.EmailSendRequest;
import ebilly.admin.server.db.User;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.UserContext;

public class Util {
	
	private static AppLogger log = AppLogger.getLogger(Util.class.getName()); 
	
	/** Used to set session attribute with this name
	 * after a successful authentication.
	 * If no session token exists with this name, consider
	 * redirecting user to login screen
	 */
	public static final String SESSION_ATTRIB_AUTH_TOKEN = "sessionAuthToken";
	
	private static Cache appCache;
	static {
		try {
			CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
			Map props = new HashMap();
	        props.put(GCacheFactory.EXPIRATION_DELTA, 3600*12);
			appCache = cacheFactory.createCache(props);
		} catch (CacheException e) {
			AppLogger.getLogger(Util.class.getName()).error("Cache initialization exception", e);
		}
	}
	
	public static Object getAppCacheValue(String key) {
		if (appCache == null) return null;
		return appCache.get(key);
	}
	
	public static boolean cacheContainsKey(String key) {
		if (appCache == null) return false;
		return appCache.containsKey(key);
	}
	
	public static void removeAppCacheValue(String key) {
		if (appCache == null) return;
		appCache.remove(key);
	}
	
	@SuppressWarnings("unchecked")
	public static void putAppCacheValue(String key, Object value) {
		if (appCache == null) return;
		appCache.put(key, value);
	}
	
	public static Cache getAppCache() {
		return Util.appCache;
	}
	
	public static String getFormattedDate(Date date) {
		return Util.getFormattedDate(date, CommonUtil.DEFAULT_DATE_FORMAT);
	}
	
	public static String getYearFromDate(Date date) {
		return Util.getFormattedDate(date,"yyyy");
	}
	
	public static String getFormattedDate(Date date, String format)
	{
		if (date == null)
			return null;
		Timestamp timestamp = new Timestamp(date.getTime());
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		//formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
		return formatter.format(timestamp);
	}
	
	public static Date getDateFromString(String date, String format) {
		if (date == null || format == null) {
			return null;
		}
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		try {
			return formatter.parse(date);
		} catch (ParseException e) {
			log.error("Exception parsing date from string: "+date
					+", using format: "+format, e);
		}
		return null;
	}
	
	public static final Date getCurrentDate() {
		return new Date(System.currentTimeMillis());
	}
	
	public static final Date getCurrentDateContextSensitive() {
		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(TimeZone.getTimeZone("IST")); //TODO: FETCH TZ FROM CONTEXT
		return cal.getTime();
	}
	
	public static final Date ripTime(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY,0);
		cal.set(Calendar.MINUTE,0);
		cal.set(Calendar.SECOND,0);
		cal.set(Calendar.MILLISECOND,0);
		return cal.getTime();
	}

	public static String generateRandomPassword() {
		int alphaStart = 97; // asci for character 'a'
		int alphaSize = 26;
		
		int numStart = 48; // asci for character 'a'
		int numSize = 10;
		
		StringBuffer p = new StringBuffer();
		for (int i = 0; i < 8; i++) {
			if ((((int)(Math.random()*1000)) % 3) == 0 ) {
				int alpha = (((int)(Math.random()*1000)) % alphaSize) + alphaStart;
				p.append((char)alpha);
			} else {
				int numeric = (((int)(Math.random()*1000)) % numSize) + numStart;
				p.append((char)numeric);
			}
		}
		return p.toString();
		//return (int)(Math.random()*1000) + "A" + (int)(Math.random()*1000);
	}
	
	public static String generateDeviceUserPassword() {
		int numStart = 48; // asci for character 'a'
		int numSize = 10;
		
		StringBuffer p = new StringBuffer();
		for (int i = 0; i < 6; i++) {
			int numeric = (((int)(Math.random()*1000)) % numSize) + numStart;
			p.append((char)numeric);
			
		}
		return p.toString();
	}
	
	public static void queueImportRequest(String id) {
		Queue queue = QueueFactory.getQueue("dataimporthandler");
		
		BackendService backendService = BackendServiceFactory.getBackendService();
		queue.add(withUrl("/cloudapp/backend/importRequestHandler")
				.param("irId", id)
				.header("Host", backendService.getBackendAddress("importdata")));
	}
	
	public static void queuePrSummary(DevicePr devicePr) {
		Queue queue = QueueFactory.getQueue("prSummarizer");
		String prJson = (new Gson()).toJson(devicePr);
		System.out.println("prJson: "+prJson);
		BackendService backendService = BackendServiceFactory.getBackendService();
		queue.add(withUrl("/cloudapp/backend/prSummarizer")
				.param("prJson",prJson)
				.header("Host", backendService.getBackendAddress("importdata")));
	}
	
	public static void queueExportRequest(String id) {
		Queue queue = QueueFactory.getQueue("dataimporthandler");
		
		// Avoiding to use backend because, 
		// when the ExportRequest is processed in backend and 
		// any modification to ExportRequest record is not
		// reflected in Application !! 
		// This seems to be a bug and i could not resolve and
		// searched in google to find nothing about it !
		//
		//BackendService backendService = BackendServiceFactory.getBackendService();
		//String backendAddress = backendService.getBackendAddress("importdata");
		queue.add(withUrl("/cloudapp/backend/exportRequestHandler")
				.param("erId", id));
				//.header("Host", backendAddress));
	}
	
	/*public static String createBlobFile(String fileContent, 
			String mimeType, String fileName) throws IOException {
		FileService fs = FileServiceFactory.getFileService();
		AppEngineFile writableFile = 
			fs.createNewBlobFile(mimeType,fileName);
		FileWriteChannel writeChannel =
	        fs.openWriteChannel(writableFile, /*lock ?true);
	    PrintWriter writer = new PrintWriter(
	    		Channels.newWriter(writeChannel, "UTF8"));
	    writer.print(fileContent);
	    writer.close();
	    writeChannel.closeFinally();
	    BlobKey resultsFileBlobKey = fs.getBlobKey(writableFile);
	    return resultsFileBlobKey.getKeyString();
	}*/
	
	public static String createBlobFile(String fileContent, 
			String mimeType, String fileName) throws IOException {
		
		GcsService gcsService = GcsServiceFactory.createGcsService();
		System.out.println("GcsFileName: " + getGcsDefaultBucketName() + " - " + fileName + ", mimeType: " + mimeType);
		GcsFilename file = new GcsFilename(getGcsDefaultBucketName(), fileName);
		GcsFileOptions.Builder builder = new GcsFileOptions.Builder();
	    GcsFileOptions options = builder.mimeType(mimeType).build();     
		GcsOutputChannel channel = gcsService.createOrReplace(file, options);

	    PrintWriter writer = new PrintWriter(
	    		Channels.newWriter(channel, "UTF8"));
	    writer.print(fileContent);
	    writer.close();
	    channel.close();

	    return file.getObjectName();
	}
	
	// Use this in hope to avoid this error
	// http://stackoverflow.com/questions/5522804/1mb-quota-limit-for-a-blobstore-object-in-google-app-engine
	/*public static String createBlobFileBuffered(String fileContent, 
			String mimeType, String fileName) throws IOException {
		
		FileService fs = FileServiceFactory.getFileService();
		AppEngineFile writableFile = 
			fs.createNewBlobFile(mimeType,fileName);
		FileWriteChannel writeChannel =
	        fs.openWriteChannel(writableFile, /*lock ?true);
		BufferedInputStream is = 
			new BufferedInputStream(new ByteArrayInputStream(
					fileContent.getBytes("UTF8")));
		byte[] chunk = new byte[524800];// 512kb
		int bytesRead = 0;
		while ( (bytesRead = is.read(chunk)) != -1) {
			ByteBuffer byteBuffer = ByteBuffer.wrap(chunk, 0, bytesRead);
			writeChannel.write(byteBuffer);
		}
	    writeChannel.closeFinally();
	    BlobKey resultsFileBlobKey = fs.getBlobKey(writableFile);
	    return resultsFileBlobKey.getKeyString();
	}*/
	
	public static String createBlobFileBuffered(String fileContent, 
			String mimeType, String fileName) throws IOException {
		
		GcsService gcsService = GcsServiceFactory.createGcsService();
		System.out.println("GcsFileName: " + getGcsDefaultBucketName() + " - " + fileName + ", mimeType: " + mimeType);
		GcsFilename file = new GcsFilename(getGcsDefaultBucketName(), fileName);
		GcsFileOptions.Builder builder = new GcsFileOptions.Builder();
	    GcsFileOptions options = builder.mimeType(mimeType).build();     
		GcsOutputChannel channel = gcsService.createOrReplace(
				file, options);

		BufferedInputStream is = 
			new BufferedInputStream(new ByteArrayInputStream(
					fileContent.getBytes("UTF8")));
		byte[] chunk = new byte[524800];// 512kb
		int bytesRead = 0;
		while ( (bytesRead = is.read(chunk)) != -1) {
			ByteBuffer byteBuffer = ByteBuffer.wrap(chunk, 0, bytesRead);
			channel.write(byteBuffer);
		}
	    channel.close();
	    
	    return file.getObjectName();
	}
	
	public static interface BlobFileWriteListener {
		public void readyToWrite(OutputStream os) throws IOException;
	}
	public static String createBlobFile(String mimeType, String fileName, 
			BlobFileWriteListener writeListener) 
	throws IOException {
		
		GcsService gcsService = GcsServiceFactory.createGcsService();
		System.out.println("GcsFileName: " + getGcsDefaultBucketName() + " - " + fileName + ", mimeType: " + mimeType);
		GcsFilename file = new GcsFilename(getGcsDefaultBucketName(), fileName);
		GcsFileOptions.Builder builder = new GcsFileOptions.Builder();
	    GcsFileOptions options = builder.mimeType(mimeType).build();     
		GcsOutputChannel channel = gcsService.createOrReplace(file, options);
		
		OutputStream outputStream = Channels.newOutputStream(channel);
		try {
			writeListener.readyToWrite(outputStream);
		} finally {
			if(outputStream != null)
				outputStream.close();
			if(channel != null)
				channel.close();;
		}

	    return file.getObjectName();
	}
	
	/*public static String createBlobFile(String mimeType, String fileName, 
			BlobFileWriteListener writeListener) 
	throws IOException {
		
		GcsFileOptions options = GcsFileOptions.getDefaultInstance();
		//GcsFileOptions.Builder.mimeType(mimeType);
		//b.mimeType(mimeType);
		
		GcsService gcsService = GcsServiceFactory.createGcsService();
		GcsFilename gcsFile =  new GcsFilename(getGcsDefaultBucketName(), fileName);
		
		//FileService fs = FileServiceFactory.getFileService();
		//AppEngineFile writableFile = 
		//	fs.createNewBlobFile(mimeType,fileName);
		//FileWriteChannel writeChannel =
	    //    fs.openWriteChannel(writableFile, /*lock ?*//*true);
		OutputStream outputStream = null;
		GcsOutputChannel writableChannel = null;
		try {
			//TODO: mime type is pending.
			writableChannel = gcsService.createOrReplace(
					gcsFile, GcsFileOptions.getDefaultInstance());
			outputStream = Channels.newOutputStream(writableChannel);
			writeListener.readyToWrite(outputStream);
		} finally {
			if(outputStream != null)
				outputStream.close();
			if(writableChannel != null)
				writableChannel.close();;
		}
	    //BlobKey resultsFileBlobKey = fs.getBlobKey(writableFile);
	    //String blobKey = resultsFileBlobKey.getKeyString();
	    //return blobKey;
	}*/

	
	/*public static StringBuffer readBlobFile(String strBlobKey) throws IOException {
		if (strBlobKey == null || CommonUtil.isEmpty(strBlobKey)) {
			return new StringBuffer("");
		}
		BlobKey blobKey = new BlobKey(strBlobKey);
		BlobstoreService blobService = BlobstoreServiceFactory.getBlobstoreService();
		
		StringBuffer sBuffer = new StringBuffer();
		int startIndex = 0;
		int fetchSize = 512;
		int endIndex = fetchSize;
		boolean readCompleted = false;
		byte[] data = null;
		while (!readCompleted) {
			data = blobService.fetchData(blobKey, startIndex, endIndex);
			sBuffer.append(data);
			startIndex = endIndex + 1;
			endIndex += (endIndex +1);
			if(data.length < fetchSize)
				readCompleted = true;
		}
		return sBuffer;
	}*/
	

	public static String getGcsDefaultBucketName() {
		AppIdentityService appIdentityService = AppIdentityServiceFactory.getAppIdentityService();
		return appIdentityService.getDefaultGcsBucketName();
	}
	
	public static StringBuffer readBlobFile(String fileName) throws IOException {
		if (fileName == null || CommonUtil.isEmpty(fileName)) {
			return new StringBuffer("");
		}
		
		GcsService gcsService = GcsServiceFactory.createGcsService();
		System.out.println("GcsFileName: " + getGcsDefaultBucketName() + " - " + fileName);
		GcsFilename gcsFile =  new GcsFilename(getGcsDefaultBucketName(), fileName);
		GcsInputChannel readChannel = null;

		StringBuffer sBuffer = new StringBuffer();
		try {
			readChannel = gcsService.openReadChannel(gcsFile, new Long(0));
			BufferedReader reader = new BufferedReader(Channels.newReader(readChannel, "UTF8")); 
			String line = null; 
			while ((line = reader.readLine()) != null) sBuffer.append(line);
		} finally {
			readChannel.close();
		}
		return sBuffer;
	}

	
/*	public static StringBuffer readBlobFile(String strBlobKey) throws IOException {
		if (strBlobKey == null || CommonUtil.isEmpty(strBlobKey)) {
			return new StringBuffer("");
		}
		BlobKey blobKey = new BlobKey(strBlobKey);
		FileService fileService = FileServiceFactory.getFileService();
		AppEngineFile file = fileService.getBlobFile(blobKey);
		FileReadChannel readChannel = null;
		StringBuffer sBuffer = new StringBuffer();
		try {
			readChannel = fileService.openReadChannel(file, false);
			BufferedReader reader = new BufferedReader(Channels.newReader(readChannel, "UTF8")); 
			String line = null; 
			while ((line = reader.readLine()) != null) sBuffer.append(line);
		} finally {
			readChannel.close();
		}
		return sBuffer;
	}*/

	
/*	public static StringBuffer readBlobFile(String strBlobKey) throws IOException {
		if (strBlobKey == null || CommonUtil.isEmpty(strBlobKey)) {
			return new StringBuffer("");
		}
		BlobKey blobKey = new BlobKey(strBlobKey);
		FileService fileService = FileServiceFactory.getFileService();
		AppEngineFile file = fileService.getBlobFile(blobKey);
		FileReadChannel readChannel = null;
		StringBuffer sBuffer = new StringBuffer();
		try {
			readChannel = fileService.openReadChannel(file, falsedo not lock);
			BufferedReader reader = new BufferedReader(Channels.newReader(readChannel, "UTF8")); 
			String line = null; 
			while ((line = reader.readLine()) != null) sBuffer.append(line);
		} finally {
			readChannel.close();
		}
		return sBuffer;
	}*/
	
	public static InputStream openBlobFileStream(String strBlobKey) throws IOException {
		if (strBlobKey == null || CommonUtil.isEmpty(strBlobKey)) {
			return null;
		}

		GcsService gcsService = GcsServiceFactory.createGcsService();
		System.out.println("GcsFileName: " + getGcsDefaultBucketName() + " - " + strBlobKey);
		GcsFilename gcsFile =  new GcsFilename(getGcsDefaultBucketName(), strBlobKey);
		GcsInputChannel readChannel = null;
		
		readChannel = gcsService.openReadChannel(gcsFile, new Long(0)/*do not lock*/);
		return Channels.newInputStream(readChannel);
		/*try {
			readChannel = fileService.openReadChannel(file, falsedo not lock);
			BufferedReader reader = new BufferedReader(Channels.newReader(readChannel, "UTF8")); 
			String line = null; 
			while ((line = reader.readLine()) != null) sBuffer.append(line);
		} finally {
			readChannel.close();
		}
		return sBuffer;*/
	}
	
	public static String getFileNameFromBlobKey(String blobKey) {
		if (blobKey == null || blobKey.trim().length() == 0) return null;
		BlobKey key = new BlobKey(blobKey);
		BlobInfoFactory blobInfoFactory = new BlobInfoFactory();
		BlobInfo blobInfo = blobInfoFactory.loadBlobInfo(key);
		if (blobInfo != null) {
			return blobInfo.getFilename();
		} else {
			return null;
		}
	}

	public static boolean isImportRequestInQueue(String importRequestId) {
		Object o = Util.getAppCacheValue(AppConfig.CACHE_IMPORT_REQUEST_QUEUE);
		if (o == null) return false;
		ArrayList<String> importRequestQueue = (ArrayList<String>)o;
		return importRequestQueue.contains(importRequestId);
	}
	
	public static String buildPasswordChangeEmailMessage(User u) {
		StringBuffer s = new StringBuffer();
		
		String loginUrl = DbAppConfig.instance().getAppUrl();
		
		s.append("<html><head></head><body>");
		s.append("Your password to login into exam cell has been changed. ")
		 .append("<br/>")
		 .append("User Name: <b>")
		 .append(u.getLoginId())
		 .append("</b><br/>")
		 .append(" New password: <b>")
		 .append(u.getPassword())
		 .append("</b><br/>")
		 .append("Login URL: ")
		 .append("<a href=\"").append(loginUrl).append("\">")
		 .append(loginUrl)
		 .append("</a>");
		s.append("</body></html>");
		return s.toString();
	}
	
	public static void createEmailSendRequest(String emailAddress, String subject, 
			String message) {
		if (CommonUtil.isEmpty(emailAddress) || CommonUtil.isEmpty(subject) ||
		    CommonUtil.isEmpty(message)) {
			throw new IllegalArgumentException("Required params missing, emailAddress: "+emailAddress+
					", subject: "+subject+", message: "+message);
		}
		EmailSendRequest r = new EmailSendRequest();
		r.setToEmailAddress(emailAddress);
		r.setEmailMessage(message);
		r.setSendAttempts(0);
		r.setSent(false);
		r.setSubject(subject);
		r.save();
	}
	
	public static void sendEmail(EmailSendRequest r) throws Exception {
		String comments = "";
		boolean isEmailSentSuccessfully = false;
		try {
			Util.sendEmailNow(r.getToEmailAddress(), 
					r.getSubject(), r.getEmailMessage());
			isEmailSentSuccessfully = true;
			comments = "successfully sent";
		} catch(Exception e) {
			isEmailSentSuccessfully = false;
			comments = "failed, reason: "+e.getMessage();
			throw e;
		} finally {
			r.setSendAttempts(1+r.getSendAttempts());
			r.setAudit(
					((r.getAudit()==null)?"":(r.getAudit()+"\n"))
					+Util.getFormattedDate(new Date(System.currentTimeMillis()))
					+": "+comments);
			r.setSent(isEmailSentSuccessfully);
			r.save();
		}
	}
	
	static void sendEmailNow(String emailAddress, String subject, String message)
	throws Exception {
		Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);
        try {
            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(DbAppConfig.instance().getEmailFromAddress()));
            msg.addRecipient(Message.RecipientType.TO,
                             new InternetAddress(emailAddress));
            msg.setSubject(subject);
            msg.setText(message);
            msg.setContent(message, "text/html");
            Transport.send(msg);
        } catch (Exception e) {
        	e.printStackTrace();
        	String msg = "Unable to send email to "+emailAddress;
        	AppLogger.getLogger("Util.sendEmail").info(msg,e);
            throw e;
        }
	}
	
	public static UserContext getUserContext(HttpServletRequest request) {
		return (UserContext)request.getSession().getAttribute(Util.SESSION_ATTRIB_AUTH_TOKEN);
	}
	
	public static String throwableAsString(Throwable throwable) {
		StringBuffer stackTrace = new StringBuffer();
		Throwable nextLevel = throwable;
		do {
			stackTrace.append(nextLevel.toString()).append("\n");
            StackTraceElement[] traceElements = nextLevel.getStackTrace();
            if (traceElements != null && traceElements.length != 0) {
                for (int i = 0; i < traceElements.length; i++) {
                    stackTrace.append("\tat ")
                        .append(traceElements[i].toString()).append("\n");
                }
            }
			nextLevel = nextLevel.getCause();
			if (nextLevel != null) stackTrace.append("Caused by ");
		} while (nextLevel != null);
		return stackTrace.toString();
	}
	
	public static String getStackTrace(Throwable throwable)
    {
        StringBuffer stackTrace = new StringBuffer();
        if (throwable == null) {
            stackTrace.append("No Throwable Object Provided to log");
        }
        else {
            stackTrace.append(throwable.toString()).append("\n");
            StackTraceElement[] traceElements = throwable.getStackTrace();
            if (traceElements != null && traceElements.length != 0) {
                for (int i = 0; i < traceElements.length; i++) {
                    stackTrace.append("\tat")
                        .append(traceElements[i].toString()).append("\n");
                }
            }

            // append the initial cause of the throwable
            Throwable initCause = throwable.getCause();
            if (initCause != null) {
                stackTrace.append("Caused by: ")
                    .append(getStackTrace(initCause));
            }
        }
        return stackTrace.toString();
    }

	public static void notifyAppAlert(String msg, Throwable e) {
		String alertEmail = DbAppConfig.instance().getAppAlertNotifyEmail();
		if (CommonUtil.isEmpty(alertEmail)) {
			alertEmail = AppConfig.EMAIL_APP_NOTIFY_ADDRESS;
		}
		String message = msg + "\n" + Util.throwableAsString(e);
		Util.createEmailSendRequest(alertEmail, msg, message);
	}
	
	public static void log(List<Object> objects, String prefixMsg, int logBatchSize) {
		int index = 0;
		StringBuilder msg = new StringBuilder(prefixMsg);
		for(Object obj : objects) {
			msg.append(obj + ",");
			index ++;
			if((index%logBatchSize) == 0) {
				log.info(msg.toString());
				msg = new StringBuilder(prefixMsg);
			}
		}
		if((index % logBatchSize) != 0) {
			log.info(msg.toString());
		}
	}
	
    public static String getCurrentTimestamp() {
        String format = "yyyyMMdd_HHmmss_SS";
        Timestamp timestamp = new Timestamp(Calendar.getInstance().getTimeInMillis());
        return getFormattedDate(timestamp, format);
    }

    public static String getFormattedDate(Timestamp ts, String format){
        if(ts == null)
            return null;
        
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        return formatter.format(ts);
    }
}