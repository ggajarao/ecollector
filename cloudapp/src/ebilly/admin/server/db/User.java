package ebilly.admin.server.db;

import javax.jdo.PersistenceManager;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

import com.google.appengine.api.datastore.KeyFactory;

import ebilly.admin.server.ValidationResult;
import ebilly.admin.server.core.AppLogger;
import ebilly.admin.server.core.db.BaseEntity;
import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.shared.CommonUtil;

@PersistenceCapable(identityType = IdentityType.APPLICATION, detachable="true")
public class User extends BaseEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final AppLogger log = AppLogger.getLogger(User.class.getName());

	@Persistent
    @Extension(vendorName="datanucleus", key="gae.pk-name", value="true")
	private String loginIdPk;
	
	@Persistent
    private String loginId;
	
	@Persistent
	private String password;
	
	@Persistent
	private String email;
	
	@Persistent
	private String roleId;
	
	public User() {
		super();		
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
		this.loginIdPk = this.loginId==null?"":this.loginId.toLowerCase();
	}

	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	
	@Override
	protected Object getUniqueId() {
		throw new IllegalArgumentException("Simple persistance is not supported in this class");
	}
	
	public static User fetchUserById(String id) {
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			User c =
				pm.getObjectById(User.class, 
						KeyFactory.stringToKey(id));
			c = pm.detachCopy(c);
			if (c.getActive() == 1) {
				return c;
			}
		} catch (Exception e ) {
			log.info("Could not fetch object", e);
		}
		return null;
	}
	
	public static User fetchUserByLoginId(String loginId) {
		if (CommonUtil.isEmpty(loginId)) return null;
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			log.info("Fetching user with loginId: "+loginId);
			User c = pm.getObjectById(User.class, loginId.toLowerCase());
			c = pm.detachCopy(c);
			if (c.getActive() == 1) {
				return c;
			}
		} catch (javax.jdo.JDOObjectNotFoundException e ) {
			log.info("Could not fetch object", e);
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onBeforeSave() {
		
	} 
	
	@Override
	public String toString() {
		return "User ["+super.toString()+", loginId=" + loginId
				+ ", password=" + password + ", email=" + email 
				+ ", roleId="+ roleId+ "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (loginId == null) {
			if (other.loginId != null)
				return false;
		} else if (!loginId.equals(other.loginId))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (roleId == null) {
			if (other.roleId != null)
				return false;
		} else if (!roleId.equals(other.roleId))
			return false;
		return true;
	}

	public void validate(ValidationResult validationResult) {
		if (CommonUtil.isEmpty(this.loginIdPk)) {
			validationResult.addItem("User login id is empty");
		}
	}
}