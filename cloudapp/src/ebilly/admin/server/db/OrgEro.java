package ebilly.admin.server.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.FetchGroup;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import ebilly.admin.server.Util;
import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.shared.AppConfig;

@PersistenceCapable(identityType = IdentityType.APPLICATION, detachable="true")
@FetchGroup(name = "orgFg", 
		members={@Persistent(name="orgSubdivisions"), @Persistent(name="oeroDivision")})
public class OrgEro implements Serializable {
	private static final long serialVersionUID = 1L;
	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
	private String id;
	
	@Persistent
	@Extension(vendorName="datanucleus", key="gae.pk-name", value="true")
	private String oeroCode;
	
	@Persistent
	private String oeroName;
	
	@Persistent
	private OrgDivision oeroDivision;
	
	/*@Persistent
	private String odvsCircleId;*/
	
	@Persistent(defaultFetchGroup = "true", mappedBy = "osbdEro")
	private List<OrgSubdivision> orgSubdivisions;

	public OrgEro() {
		
	}
	
	public String getId() {
		return id;
	}
	
	/*	public AcademicRegulation getAcbRegulation() {
			return acbRegulation;
		}
	
		public void setAcbRegulation(AcademicRegulation acbRegulation) {
			this.acbRegulation = acbRegulation;
		}
	*/
	
	public List<OrgSubdivision> getOrgSubdivisions() {
		return orgSubdivisions;
	}

	public void setOrgSubdivisions(List<OrgSubdivision> orgSubdivisions) {
		this.orgSubdivisions = orgSubdivisions;
	}

	public void addOrgSubdivision(OrgSubdivision orgSubdivision) {
		if (this.orgSubdivisions == null) {
			 this.orgSubdivisions = new ArrayList<OrgSubdivision>();
		}
		/*if (this.getId() == null) throw new IllegalArgumentException("Save Division first then add Subdivision");
		orgSubdivision.setOsbdDivisionId(this.getId());*/
		this.orgSubdivisions.add(orgSubdivision);
	}
	
	public OrgSubdivision findOrgSubdivision(Key subdivisionId) {
		if (this.orgSubdivisions == null) return null;
		String strSubdivisionId = KeyFactory.keyToString(subdivisionId);
		for (OrgSubdivision sem : this.orgSubdivisions) {
			if (sem.getId().equals(strSubdivisionId)) {
				return sem;
			}
		}
		return null;
	}
	
	public OrgSubdivision findOrgSubdivision(String subdivisionCode) {
		if (this.orgSubdivisions == null) return null;
		for (OrgSubdivision subDiv : this.orgSubdivisions) {
			if (subDiv.getOsbdCode().equals(subdivisionCode)) {
				return subDiv;
			}
		}
		return null;
	}

	public String getOeroCode() {
		return oeroCode;
	}

	public void setOeroCode(String oeroCode) {
		this.oeroCode = oeroCode;
	}

	public String getOeroName() {
		return oeroName;
	}

	public void setOeroName(String oeroName) {
		this.oeroName = oeroName;
	}

	/*public String getOdvsCircleId() {
		return odvsCircleId;
	}
	
	public void setOdvsCircleId(String circleId) {
		this.odvsCircleId = circleId;
	}*/

	public void save()
	{
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			pm.makePersistent(this);
			Util.removeAppCacheValue(AppConfig.CACHE_ORG_STRUCTURE_DATA);
		}
		finally {
			pm.close();
		}
	}
	
	public static OrgEro fetchDivisionById(String divisionId) {
		Key key = KeyFactory.stringToKey(divisionId);
		return OrgEro.fetchDivisionById(key);
	}

	public static OrgEro fetchDivisionById(Key key) {
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			pm.getFetchPlan().addGroup("acbFg");
			OrgEro c = pm.getObjectById(OrgEro.class, key);
			return pm.detachCopy(c);
		} catch (javax.jdo.JDOObjectNotFoundException e ) {
			//log.info("Could not fetch object", e);
		}
		return null;
	}
		
	@Override
	public String toString() {
		return "OrgEro [id=" + id + ", oeroCode="
				+ oeroCode + ", oeroName=" + oeroName
				+ /*", odvsCircleId=" + odvsCircleId +*/ "]";
	}

	public static void delete(String entityId) {
		Key key = KeyFactory.stringToKey(entityId);
		OrgEro r = OrgEro.fetchDivisionById(key);
		if (r != null) {
			PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
			pm.deletePersistent(r);
			Util.removeAppCacheValue(AppConfig.CACHE_ORG_STRUCTURE_DATA);
		}
	}
}
