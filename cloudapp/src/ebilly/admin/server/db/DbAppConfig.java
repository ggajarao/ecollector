package ebilly.admin.server.db;

import java.util.Date;

import javax.jdo.PersistenceManager;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

import ebilly.admin.server.core.AppLogger;
import ebilly.admin.server.core.db.BaseEntity;
import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.shared.AppConfig;

@PersistenceCapable(identityType = IdentityType.APPLICATION, detachable="true")
public class DbAppConfig extends BaseEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final AppLogger log = AppLogger.getLogger(DbAppConfig.class.getName());
	
	@Persistent
	@Extension(vendorName="datanucleus", key="gae.pk-name", value="true")
	private String appCode;
	
	@Persistent
	private String emailFromAddress;

	@Persistent
	private int maxEmailRetryCount;
	
	@Persistent
	private long maxAllowedRecordsWhileDataImport;
	
	@Persistent
	private int canSendEmails;
	
	@Persistent
	private String appUrl;
	
	@Persistent
	private Date lastPrSummarizedDate;
	
	@Persistent
	private String prSummaryContextJson; 
	
	@Persistent
	private String appAlertNotifyEmail;
	
	public DbAppConfig() {
		
	}
	
	public String getAppUrl() {
		return appUrl;
	}

	public void setAppUrl(String appUrl) {
		this.appUrl = appUrl;
	}

	public String getAppCode() {
		return this.appCode;
	}

	public String getEmailFromAddress() {
		return emailFromAddress;
	}

	public void setEmailFromAddress(String emailFromAddress) {
		this.emailFromAddress = emailFromAddress;
	}

	public Date getLastPrSummarizedDate() {
		return lastPrSummarizedDate;
	}

	public void setLastPrSummarizedDate(Date lastPrSummarizedDate) {
		this.lastPrSummarizedDate = lastPrSummarizedDate;
	}

	public String getAppAlertNotifyEmail() {
		return appAlertNotifyEmail;
	}

	public void setAppAlertNotifyEmail(String appAlertNotifyEmail) {
		this.appAlertNotifyEmail = appAlertNotifyEmail;
	}

	@Override
	protected Object getUniqueId() {
		throw new IllegalArgumentException("getUniqueId not supported");
	}
	
	public static DbAppConfig fetchAppConfigByCode(String code) {
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			DbAppConfig c =
				pm.getObjectById(DbAppConfig.class,code);
			return pm.detachCopy(c);
		} catch (Exception e ) {
			log.info("Could not fetch object", e);
		}
		return null;
	}
	
	@Override
	protected void onBeforeSave() {
		instance = null;
	}
	
	private static final transient String APP_CODE = "ecollector";
	private static transient DbAppConfig instance;
	public static DbAppConfig instance() {
		if (instance == null) {
			synchronized(DbAppConfig.class) {
				if (instance == null) {
					instance = fetchAppConfigByCode(APP_CODE);
					if (instance == null) {
						DbAppConfig cfg = new DbAppConfig();
						cfg.appCode = APP_CODE;
						cfg.setEmailFromAddress(AppConfig.EMAIL_FROM_ADDRESS);
						cfg.setMaxAllowedRecordsWhileDataImport(2000);
						cfg.save();
						instance = cfg;
						return instance;
					} else {
						return instance;
					}
				} else {
					return instance;
				}
			}
		} else {
			return instance;
		}
	}

	public int getMaxEmailRetryCount() {
		return this.maxEmailRetryCount;
	}
	
	public void setMaxEmailRetryCount(int maxEmailRetryCount) {
		this.maxEmailRetryCount = maxEmailRetryCount;
	}
	
	public void setMaxAllowedRecordsWhileDataImport(long v) {
		this.maxAllowedRecordsWhileDataImport = v;
	}

	public long getMaxAllowedRecordsWhileDataImport() {
		return this.maxAllowedRecordsWhileDataImport;
	}

	public int getCanSendEmails() {
		return canSendEmails;
	}

	public void setCanSendEmails(int canSendEmails) {
		this.canSendEmails = canSendEmails;
	}

	public String getPrSummaryContextJson() {
		return prSummaryContextJson;
	}

	public void setPrSummaryContextJson(String prSummaryContextJson) {
		this.prSummaryContextJson = prSummaryContextJson;
	}
}
