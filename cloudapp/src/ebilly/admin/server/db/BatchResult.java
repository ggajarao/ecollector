package ebilly.admin.server.db;

public class BatchResult<T> {
	
	private String mEncodedCursor;
	private T mResults;
	
	public BatchResult(T results, String encodedCursor) {
		mResults = results;
		mEncodedCursor = encodedCursor;
	}

	public String getEncodedCursor() {
		return mEncodedCursor;
	}

	public T getResults() {
		return mResults;
	}
}
