package ebilly.admin.server.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.FetchGroup;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import ebilly.admin.server.Util;
import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.shared.AppConfig;

@PersistenceCapable(identityType = IdentityType.APPLICATION, detachable="true")
@FetchGroup(name = "orgFg", 
		members={@Persistent(name="orgEros"), @Persistent(name="odvsCircle")})
public class OrgDivision implements Serializable {
	private static final long serialVersionUID = 1L;
	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
	private String id;
	
	@Persistent
	@Extension(vendorName="datanucleus", key="gae.pk-name", value="true")
	private String odvsCode;
	
	@Persistent
	private String odvsName;
	
	@Persistent
	private OrgCircle odvsCircle;
	
	/*@Persistent
	private String odvsCircleId;*/
	
	@Persistent(defaultFetchGroup = "true", mappedBy = "oeroDivision")
	private List<OrgEro> orgEros;

	public OrgDivision() {
		
	}
	
	public String getId() {
		return id;
	}
	
	/*	public AcademicRegulation getAcbRegulation() {
			return acbRegulation;
		}
	
		public void setAcbRegulation(AcademicRegulation acbRegulation) {
			this.acbRegulation = acbRegulation;
		}
	*/
	
	public List<OrgEro> getOrgEros() {
		return orgEros;
	}

	public void setOrgEros(List<OrgEro> orgEros) {
		this.orgEros = orgEros;
	}

	public void addOrgEro(OrgEro orgEro) {
		if (this.orgEros == null) {
			 this.orgEros = new ArrayList<OrgEro>();
		}
		/*if (this.getId() == null) throw new IllegalArgumentException("Save Division first then add Subdivision");
		orgSubdivision.setOsbdDivisionId(this.getId());*/
		this.orgEros.add(orgEro);
	}
	
	public OrgEro findOrgEro(Key eroId) {
		if (this.orgEros == null) return null;
		String strEroId = KeyFactory.keyToString(eroId);
		for (OrgEro ero : this.orgEros) {
			if (ero.getId().equals(strEroId)) {
				return ero;
			}
		}
		return null;
	}
	
	public OrgEro findOrgEro(String eroCode) {
		if (this.orgEros == null) return null;
		for (OrgEro ero : this.orgEros) {
			if (ero.getOeroCode().equals(eroCode)) {
				return ero;
			}
		}
		return null;
	}

	public String getOdvsCode() {
		return this.odvsCode;
	}
	
	public void setOdvsCode(String divisionCode) {
		this.odvsCode = divisionCode;
	}
	
	public String getOdvsName() {
		return odvsName;
	}

	public void setOdvsName(String odvsName) {
		this.odvsName = odvsName;
	}
	
	/*public String getOdvsCircleId() {
		return odvsCircleId;
	}
	
	public void setOdvsCircleId(String circleId) {
		this.odvsCircleId = circleId;
	}*/

	public void save()
	{
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			pm.makePersistent(this);
			Util.removeAppCacheValue(AppConfig.CACHE_ORG_STRUCTURE_DATA);
		}
		finally {
			pm.close();
		}
	}
	
	public static OrgDivision fetchDivisionById(String divisionId) {
		Key key = KeyFactory.stringToKey(divisionId);
		return OrgDivision.fetchDivisionById(key);
	}

	public static OrgDivision fetchDivisionById(Key key) {
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			pm.getFetchPlan().addGroup("acbFg");
			OrgDivision c = pm.getObjectById(OrgDivision.class, key);
			return pm.detachCopy(c);
		} catch (javax.jdo.JDOObjectNotFoundException e ) {
			//log.info("Could not fetch object", e);
		}
		return null;
	}
		
	@Override
	public String toString() {
		return "OrgDivision [id=" + id + ", odvsCode="
				+ odvsCode + ", odvsName=" + odvsName
				+ /*", odvsCircleId=" + odvsCircleId +*/ "]";
	}

	public static void delete(String entityId) {
		Key key = KeyFactory.stringToKey(entityId);
		OrgDivision r = OrgDivision.fetchDivisionById(key);
		if (r != null) {
			PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
			pm.deletePersistent(r);
			Util.removeAppCacheValue(AppConfig.CACHE_ORG_STRUCTURE_DATA);
		}
	}
}
