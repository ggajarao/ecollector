package ebilly.admin.server.db;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.KeyFactory;

import ebilly.admin.server.core.AppLogger;
import ebilly.admin.server.core.db.BaseEntityNonPersistent;
import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.AppConfig.IMPORT_STATUS;

@PersistenceCapable(identityType = IdentityType.APPLICATION, detachable="true")
public class ImportRequest extends BaseEntityNonPersistent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final AppLogger log = AppLogger.getLogger(ImportRequest.class.getName());
	
	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
	private String id;
	
	@Persistent
	private Date createdDate;
	
	@Persistent
	private Date lastModifiedDate;
	
	@Persistent
	private int active = 1;
	
	@Persistent
	private int changeNumber;
	
	@Persistent
	private String objectType;
	
	@Persistent
	private String fileContent;
	
	@Persistent
	private String fileName;
	
	@Persistent
	private String staticValue1;
	
	@Persistent
	private String staticValue2;
	
	@Persistent
	private String staticValue3;
	
	@Persistent
	private int status;
	
	@Persistent
	private String importResultFile;
	
	@Persistent
	private String importErrorMessage;
	
	@Persistent
	private String importSummary;
	
	public ImportRequest() {
		
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public int getChangeNumber() {
		return changeNumber;
	}

	public void setChangeNumber(int changeNumber) {
		this.changeNumber = changeNumber;
	}
	
	public String getId() {
		return id;
	}
	
	@Override
	public void setEntityId(String id) {
		this.id = id;
	}

	@Override
	protected Object getUniqueId() {
		throw new IllegalArgumentException("Unsupported UniqueId requested");
	}

	public String getObjectType() {
		return objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public String getFileContent() {
		return fileContent;
	}

	public void setFileContent(String fileContent) {
		this.fileContent = fileContent;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getStaticValue1() {
		return staticValue1;
	}

	public void setStaticValue1(String staticValue1) {
		this.staticValue1 = staticValue1;
	}

	public String getStaticValue2() {
		return staticValue2;
	}

	public void setStaticValue2(String staticValue2) {
		this.staticValue2 = staticValue2;
	}

	public String getStaticValue3() {
		return staticValue3;
	}

	public void setStaticValue3(String staticValue3) {
		this.staticValue3 = staticValue3;
	}
	
	public void setStatus(IMPORT_STATUS status) {
		this.status = status.getCode();
	}
	
	public IMPORT_STATUS getStatus() {
		return IMPORT_STATUS.asValue(this.status);
	}

	public String getImportResultFile() {
		return importResultFile;
	}

	public void setImportResultFile(String importResultFile) {
		this.importResultFile = importResultFile;
	}

	public String getImportErrorMessage() {
		return importErrorMessage;
	}

	public void setImportErrorMessage(String importErrorMessage) {
		this.importErrorMessage = importErrorMessage;
	}

	public String getImportSummary() {
		return importSummary;
	}

	public void setImportSummary(String importSummary) {
		this.importSummary = importSummary;
	}

	public static List<ImportRequest> fetchNewRequests() {
		return fetechImportRequestsByStatus(AppConfig.IMPORT_STATUS.NEW);
	}
	
	private static List<ImportRequest> fetechImportRequestsByStatus(IMPORT_STATUS status) {
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		StringBuffer q = new StringBuffer();
		q.append("select from ").append(ImportRequest.class.getName())
		 .append(" where active == 1 ")
		 .append("    && status == pStatus")
		 .append(" parameters java.lang.Integer pStatus ")
		 .append(" order by createdDate ascending ");
		 
		Query query = pm.newQuery(q.toString());
		try {
			List<ImportRequest> results = 
				(List<ImportRequest>) query.execute(status.getCode());
			if (results.isEmpty()) return Collections.emptyList();
			results = (List<ImportRequest>) pm.detachCopyAll(results);
			return results;
		} finally {
			query.closeAll();
		}
	}
	
	public static ImportRequest fetchImportRequestById(String id) {
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			ImportRequest c =
				pm.getObjectById(ImportRequest.class, 
						KeyFactory.stringToKey(id));
			return pm.detachCopy(c);
		} catch (javax.jdo.JDOObjectNotFoundException e ) {
			log.info("Could not fetch object ImportRequest", e);
		}
		return null;
	}
	
	private static ImportRequest updateRequestState(String importRequestId, 
			IMPORT_STATUS status) {
		ImportRequest ir = 
			ImportRequest.fetchImportRequestById(importRequestId);
		if (ir == null) {
			return null;
		}
		ir.setStatus(status);
		ir.save();
		return ImportRequest.fetchImportRequestById(importRequestId);
	}

	public static ImportRequest updateRequestStateToInprogress(
			String importRequestId) {
		return updateRequestState(importRequestId, IMPORT_STATUS.INPROGRESS);
	}

	public static ImportRequest updateRequestStateToFailed(String importRequestId) {
		return updateRequestState(importRequestId, IMPORT_STATUS.FAILED);
	}
}
