package ebilly.admin.server.db;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.datanucleus.query.JDOCursorHelper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import ebilly.admin.server.bo.PrBo;
import ebilly.admin.server.core.AppLogger;
import ebilly.admin.server.core.CoreUtils;
import ebilly.admin.server.core.db.BaseEntityNonPersistent;
import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.server.core.reports.ResultAggregatorFactory;
import ebilly.admin.shared.CommonUtil;
import ecollector.common.ServiceColumns;

@PersistenceCapable(identityType = IdentityType.APPLICATION, detachable="true")
public class DevicePr extends BaseEntityNonPersistent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final AppLogger log = AppLogger.getLogger(ImportRequest.class.getName());

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	@Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
	private String id;

	@Persistent
	private Date createdDate;
    
	/**
	 * DB WRITES OPTIMIZATION, EACH PROPERTY CONSUMES 2 WRITE OPERATIONS.
	 * MAKING UNNECESSARY PROPERTIES AS transient.
	 */
	transient private Date lastModifiedDate;
	transient private int active = 1;
	transient private int changeNumber;

	@Persistent private Text prFieldStore;
	
	private static enum PR_FIELDS {
		USC_NO,
		SC_NO,
		ERO,
		SECTION,
		DISTRIBUTION,
		RECEIPT_NUMBER,
		COLLECTION_DATE,
		COLLECTION_TIME,
		ARREARS_N_DEMAND,
		RC_COLLECTED,
		ACD_COLLECTED,
		OTHERS_COLLECTED,
		RC_CODE,
		MACHINE_CODE,
		LAST_PAID_RECEIPT_NO,
		LAST_PAID_DATE,
		LAST_PAID_AMOUNT,
		TC_SEAL_NO,
		REMARKS,
		REMARKS_AMOUNT,
		AGL_AMOUNT,
		AGL_SERVICES,
		MACHINE_NUMBER,
		NEW_ARREARS,
		CMD_COLLECTED,
		COLLECTION_DELTA,
		AMOUNT_COLLECTED;
	};
	
	/* Pr fields */
	transient private String USC_NO;
	transient private String SC_NO;
	transient private String ERO;
	transient private String SECTION;
	transient private String DISTRIBUTION;
	transient private String RECEIPT_NUMBER;
	transient private String COLLECTION_DATE;
	transient private String COLLECTION_TIME;
	transient private String ARREARS_N_DEMAND;
	transient private String RC_COLLECTED;
	transient private String ACD_COLLECTED;
	transient private String OTHERS_COLLECTED;
	transient private String RC_CODE;
	transient private String MACHINE_CODE;
	transient private String LAST_PAID_RECEIPT_NO;
	transient private String LAST_PAID_DATE;
	transient private String LAST_PAID_AMOUNT;
	transient private String TC_SEAL_NO;
	transient private String REMARKS;
	transient private String REMARKS_AMOUNT;
	transient private String AGL_AMOUNT;
	transient private String AGL_SERVICES;
	transient private String MACHINE_NUMBER;
	transient private String NEW_ARREARS;
	transient private String CMD_COLLECTED;
	transient private String COLLECTION_DELTA;
	transient private String AMOUNT_COLLECTED;
	/* End of Pr fields */
	
	/* Begin of Org Identifier Fields */
	transient private String companyCode;
	transient private String circleCode;
	transient private String divisionCode;
	transient private String eroCode;
	transient private String subDivisionCode;
	transient private String sectionCode;
	transient private String distCode;
	/* End of Org Identifier Fields */
	
	
	@Persistent private Date _collectionDate;
	
	@Extension(vendorName="datanucleus", key="gae.pk-name", value="true")
	private String _DEVICE_PR_CODE; 
	
	@Persistent private String deviceName;
	
	//@Persistent private String status; // THIS REPRESENTS SERVICE STAGE STATUS

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public int getChangeNumber() {
		return changeNumber;
	}

	public void setChangeNumber(int changeNumber) {
		this.changeNumber = changeNumber;
	}

	public String getId() {
		return id;
	}
	
	@Override
	public void setEntityId(String id) {
		this.id = id;
	}

	@Override
	protected Object getUniqueId() {
		throw new IllegalArgumentException("Unsupported UniqueId requested");
	}
	
	/*public String getDevicePrCode() {
		return devicePrCode;
	}*/

	public String getDeviceName() {
		return deviceName;
	}

	public String getUSC_NO() {
		assertPrFieldStore();
		return USC_NO;
	}

	public void setUSC_NO(String uSC_NO) {
		USC_NO = uSC_NO;
		deriveOrgIdentifiers(USC_NO);
	}

	public String getSC_NO() {
		assertPrFieldStore();
		return SC_NO;
	}

	public void setSC_NO(String sC_NO) {
		SC_NO = sC_NO;
	}

	public String getERO() {
		assertPrFieldStore();
		return ERO;
	}

	public void setERO(String eRO) {
		ERO = eRO;
	}

	public String getSECTION() {
		assertPrFieldStore();
		return SECTION;
	}

	public void setSECTION(String sECTION) {
		SECTION = sECTION;
	}

	public String getDISTRIBUTION() {
		assertPrFieldStore();
		return DISTRIBUTION;
	}

	public void setDISTRIBUTION(String dISTRIBUTION) {
		DISTRIBUTION = dISTRIBUTION;
	}

	public String getRECEIPT_NUMBER() {
		assertPrFieldStore();
		return RECEIPT_NUMBER;
	}

	public void setRECEIPT_NUMBER(String rECEIPT_NUMBER) {
		RECEIPT_NUMBER = rECEIPT_NUMBER;
	}

	public String getCOLLECTION_DATE() {
		assertPrFieldStore();
		return COLLECTION_DATE;
	}

	public void setCOLLECTION_DATE(String cOLLECTION_DATE) {
		COLLECTION_DATE = cOLLECTION_DATE;
	}

	public String getCOLLECTION_TIME() {
		assertPrFieldStore();
		return COLLECTION_TIME;
	}

	public void setCOLLECTION_TIME(String cOLLECTION_TIME) {
		COLLECTION_TIME = cOLLECTION_TIME;
	}

	public String getARREARS_N_DEMAND() {
		assertPrFieldStore();
		return ARREARS_N_DEMAND;
	}

	public void setARREARS_N_DEMAND(String aRREARS_N_DEMAND) {
		ARREARS_N_DEMAND = aRREARS_N_DEMAND;
	}

	public String getRC_COLLECTED() {
		assertPrFieldStore();
		return RC_COLLECTED;
	}

	public void setRC_COLLECTED(String rC_COLLECTED) {
		RC_COLLECTED = rC_COLLECTED;
	}

	public String getACD_COLLECTED() {
		assertPrFieldStore();
		return ACD_COLLECTED;
	}

	public void setACD_COLLECTED(String aCD_COLLECTED) {
		ACD_COLLECTED = aCD_COLLECTED;
	}

	public String getOTHERS_COLLECTED() {
		assertPrFieldStore();
		return OTHERS_COLLECTED;
	}

	public void setOTHERS_COLLECTED(String oTHERS_COLLECTED) {
		OTHERS_COLLECTED = oTHERS_COLLECTED;
	}

	public String getRC_CODE() {
		assertPrFieldStore();
		return RC_CODE;
	}

	public void setRC_CODE(String rC_CODE) {
		RC_CODE = rC_CODE;
	}

	public String getMACHINE_CODE() {
		assertPrFieldStore();
		return MACHINE_CODE;
	}

	public void setMACHINE_CODE(String mACHINE_CODE) {
		MACHINE_CODE = mACHINE_CODE;
	}

	public String getLAST_PAID_RECEIPT_NO() {
		assertPrFieldStore();
		return LAST_PAID_RECEIPT_NO;
	}

	public void setLAST_PAID_RECEIPT_NO(String lAST_PAID_RECEIPT_NO) {
		LAST_PAID_RECEIPT_NO = lAST_PAID_RECEIPT_NO;
	}

	public String getLAST_PAID_DATE() {
		assertPrFieldStore();
		return LAST_PAID_DATE;
	}

	public void setLAST_PAID_DATE(String lAST_PAID_DATE) {
		LAST_PAID_DATE = lAST_PAID_DATE;
	}

	public String getLAST_PAID_AMOUNT() {
		assertPrFieldStore();
		return LAST_PAID_AMOUNT;
	}

	public void setLAST_PAID_AMOUNT(String lAST_PAID_AMOUNT) {
		LAST_PAID_AMOUNT = lAST_PAID_AMOUNT;
	}

	public String getTC_SEAL_NO() {
		assertPrFieldStore();
		return TC_SEAL_NO;
	}

	public void setTC_SEAL_NO(String tC_SEAL_NO) {
		TC_SEAL_NO = tC_SEAL_NO;
	}

	public String getREMARKS() {
		assertPrFieldStore();
		return REMARKS;
	}

	public void setREMARKS(String rEMARKS) {
		REMARKS = rEMARKS;
	}

	public String getREMARKS_AMOUNT() {
		assertPrFieldStore();
		return REMARKS_AMOUNT;
	}

	public void setREMARKS_AMOUNT(String rEMARKS_AMOUNT) {
		REMARKS_AMOUNT = rEMARKS_AMOUNT;
	}

	public String getAGL_AMOUNT() {
		assertPrFieldStore();
		return AGL_AMOUNT;
	}

	public void setAGL_AMOUNT(String aGL_AMOUNT) {
		AGL_AMOUNT = aGL_AMOUNT;
	}

	public String getAGL_SERVICES() {
		assertPrFieldStore();
		return AGL_SERVICES;
	}

	public void setAGL_SERVICES(String aGL_SERVICES) {
		AGL_SERVICES = aGL_SERVICES;
	}

	public String getMACHINE_NUMBER() {
		assertPrFieldStore();
		return MACHINE_NUMBER;
	}

	public void setMACHINE_NUMBER(String mACHINE_NUMBER) {
		MACHINE_NUMBER = mACHINE_NUMBER;
	}

	public String getNEW_ARREARS() {
		assertPrFieldStore();
		return NEW_ARREARS;
	}

	public void setNEW_ARREARS(String nEW_ARREARS) {
		NEW_ARREARS = nEW_ARREARS;
	}

	public String getCMD_COLLECTED() {
		assertPrFieldStore();
		return CMD_COLLECTED;
	}

	public void setCMD_COLLECTED(String cMD_COLLECTED) {
		CMD_COLLECTED = cMD_COLLECTED;
	}

	public String getCOLLECTION_DELTA() {
		assertPrFieldStore();
		return COLLECTION_DELTA;
	}

	public void setCOLLECTION_DELTA(String cOLLECTION_DELTA) {
		COLLECTION_DELTA = cOLLECTION_DELTA;
	}

	/*public void setDevicePrCode(String devicePrCode) {
		this.devicePrCode = devicePrCode;
	}*/

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
		//this.devicePrCode = prepareUniqueCode(this.deviceName, this.RECEIPT_NUMBER);
	}

	public String get_DEVICE_PR_CODE() {
		return _DEVICE_PR_CODE;
	}

	public void set_DEVICE_PR_CODE(String _DEVICE_PR_CODE) {
		this._DEVICE_PR_CODE = _DEVICE_PR_CODE;
	}
	
	public Date getCollectionDate() {
		return _collectionDate;
	}

	public void setCollectionDate(Date collectionDate) {
		this._collectionDate = collectionDate;
	}
	
	/*public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}*/
	
	public String getAMOUNT_COLLECTED() {
		assertPrFieldStore();
		return AMOUNT_COLLECTED;
	}

	public void setAMOUNT_COLLECTED(String aMOUNT_COLLECTED) {
		AMOUNT_COLLECTED = aMOUNT_COLLECTED;
	}

	public String getCompanyCode() {
		if (companyCode == null) deriveOrgIdentifiers(USC_NO);
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getCircleCode() {
		if (circleCode == null) deriveOrgIdentifiers(USC_NO);
		return circleCode;
	}

	public void setCircleCode(String circleCode) {
		this.circleCode = circleCode;
	}

	public String getDivisionCode() {
		if (divisionCode == null) deriveOrgIdentifiers(USC_NO);
		return divisionCode;
	}

	public void setDivisionCode(String divisionCode) {
		this.divisionCode = divisionCode;
	}

	public String getEroCode() {
		return eroCode;
	}

	public void setEroCode(String eroCode) {
		this.eroCode = eroCode;
	}

	public String getSubDivisionCode() {
		if (subDivisionCode == null) deriveOrgIdentifiers(USC_NO);
		return subDivisionCode;
	}

	public void setSubDivisionCode(String subDivisionCode) {
		this.subDivisionCode = subDivisionCode;
	}

	public String getSectionCode() {
		if (sectionCode == null) deriveOrgIdentifiers(USC_NO);
		return sectionCode;
	}

	public void setSectionCode(String sectionCode) {
		this.sectionCode = sectionCode;
	}

	public String getDistCode() {
		if (distCode == null) deriveOrgIdentifiers(USC_NO);
		return distCode;
	}

	public void setDistCode(String distCode) {
		this.distCode = distCode;
	}

	private String getPrFieldStore() {
		if (prFieldStore != null) return prFieldStore.getValue();
		return null;
	}

	private void setPrFieldStore(String jsonPrFieldStore) {
		this.prFieldStore = new Text(jsonPrFieldStore);
	}

	/**** utility methods ********/
	
	private static final String datePattern = ServiceColumns.MACHINE_DATE_FORMAT + " " + ServiceColumns.MACHINE_TIME_FORMAT + " Z";
	
	protected void onBeforeSave() {
		String dateString = 
			this.COLLECTION_DATE + " " + 
			this.COLLECTION_TIME + " +0530";
		setCollectionDate(CoreUtils.getDateFromString(dateString, datePattern));
		
		String jsonPrFieldStore = preparePrFieldStore();
		setPrFieldStore(jsonPrFieldStore);
	}
	
	@Override
	protected void onAfterSave() {
		try {
			PrBo.recordOrgStructure(this);
		} catch (Throwable t) {
			log.error("Error recording Org Structure for Pr: "+this, t);
		}
	}
	
	transient private boolean arePrFieldsLoaded = false;
	
	private void assertPrFieldStore() {
		if (!arePrFieldsLoaded) loadPrFieldStore();
	}
	
	@SuppressWarnings("static-access")
	private void loadPrFieldStore() {
		if (arePrFieldsLoaded) return;
		String jsonPrFieldStore = this.getPrFieldStore();
		JsonObject ojson = null;
		if (jsonPrFieldStore != null && !jsonPrFieldStore.trim().isEmpty()) {
			jsonPrFieldStore = jsonPrFieldStore.trim();
			JsonParser parser = new JsonParser();
			ojson = (JsonObject) parser.parse(jsonPrFieldStore);
		}
		if (ojson == null) return; 
		
		ResultAggregatorFactory j = new ResultAggregatorFactory();
		setACD_COLLECTED(j.jsonAsString(ojson.getAsJsonPrimitive((PR_FIELDS.ACD_COLLECTED.toString()))));
		setAGL_AMOUNT(j.jsonAsString(ojson.getAsJsonPrimitive((PR_FIELDS.AGL_AMOUNT.toString()))));
		setAGL_SERVICES(j.jsonAsString(ojson.getAsJsonPrimitive((PR_FIELDS.AGL_SERVICES.toString()))));
		setAMOUNT_COLLECTED(j.jsonAsString(ojson.getAsJsonPrimitive((PR_FIELDS.AMOUNT_COLLECTED.toString()))));
		setARREARS_N_DEMAND(j.jsonAsString(ojson.getAsJsonPrimitive((PR_FIELDS.ARREARS_N_DEMAND.toString()))));
		setCMD_COLLECTED(j.jsonAsString(ojson.getAsJsonPrimitive((PR_FIELDS.CMD_COLLECTED.toString()))));
		setCOLLECTION_DATE(j.jsonAsString(ojson.getAsJsonPrimitive((PR_FIELDS.COLLECTION_DATE.toString()))));
		setCOLLECTION_DELTA(j.jsonAsString(ojson.getAsJsonPrimitive((PR_FIELDS.COLLECTION_DELTA.toString()))));
		setCOLLECTION_TIME(j.jsonAsString(ojson.getAsJsonPrimitive((PR_FIELDS.COLLECTION_TIME.toString()))));
		setDISTRIBUTION(j.jsonAsString(ojson.getAsJsonPrimitive((PR_FIELDS.DISTRIBUTION.toString()))));
		setERO(j.jsonAsString(ojson.getAsJsonPrimitive((PR_FIELDS.ERO.toString()))));
		setLAST_PAID_AMOUNT(j.jsonAsString(ojson.getAsJsonPrimitive((PR_FIELDS.LAST_PAID_AMOUNT.toString()))));
		setLAST_PAID_DATE(j.jsonAsString(ojson.getAsJsonPrimitive((PR_FIELDS.LAST_PAID_DATE.toString()))));
		setLAST_PAID_RECEIPT_NO(j.jsonAsString(ojson.getAsJsonPrimitive((PR_FIELDS.LAST_PAID_RECEIPT_NO.toString()))));
		setMACHINE_CODE(j.jsonAsString(ojson.getAsJsonPrimitive((PR_FIELDS.MACHINE_CODE.toString()))));
		setMACHINE_NUMBER(j.jsonAsString(ojson.getAsJsonPrimitive((PR_FIELDS.MACHINE_NUMBER.toString()))));
		setNEW_ARREARS(j.jsonAsString(ojson.getAsJsonPrimitive((PR_FIELDS.NEW_ARREARS.toString()))));
		setOTHERS_COLLECTED(j.jsonAsString(ojson.getAsJsonPrimitive((PR_FIELDS.OTHERS_COLLECTED.toString()))));
		setRC_CODE(j.jsonAsString(ojson.getAsJsonPrimitive((PR_FIELDS.RC_CODE.toString()))));
		setRC_COLLECTED(j.jsonAsString(ojson.getAsJsonPrimitive((PR_FIELDS.RC_COLLECTED.toString()))));
		setRECEIPT_NUMBER(j.jsonAsString(ojson.getAsJsonPrimitive((PR_FIELDS.RECEIPT_NUMBER.toString()))));
		setREMARKS(j.jsonAsString(ojson.getAsJsonPrimitive((PR_FIELDS.REMARKS.toString()))));
		setREMARKS_AMOUNT(j.jsonAsString(ojson.getAsJsonPrimitive((PR_FIELDS.REMARKS_AMOUNT.toString()))));
		setSC_NO(j.jsonAsString(ojson.getAsJsonPrimitive((PR_FIELDS.SC_NO.toString()))));
		setSECTION(j.jsonAsString(ojson.getAsJsonPrimitive((PR_FIELDS.SECTION.toString()))));
		setTC_SEAL_NO(j.jsonAsString(ojson.getAsJsonPrimitive((PR_FIELDS.TC_SEAL_NO.toString()))));
		setUSC_NO(j.jsonAsString(ojson.getAsJsonPrimitive((PR_FIELDS.USC_NO.toString()))));
		arePrFieldsLoaded = true;
	}
	
	private String preparePrFieldStore() {
		JsonObject prFieldStore = new JsonObject();
		prFieldStore.add(PR_FIELDS.ACD_COLLECTED.toString(), ResultAggregatorFactory.asJsonObject(this.getACD_COLLECTED()));
		prFieldStore.add(PR_FIELDS.AGL_AMOUNT.toString(), ResultAggregatorFactory.asJsonObject(this.getAGL_AMOUNT()));
		prFieldStore.add(PR_FIELDS.AGL_SERVICES.toString(), ResultAggregatorFactory.asJsonObject(this.getAGL_SERVICES()));
		prFieldStore.add(PR_FIELDS.AMOUNT_COLLECTED.toString(), ResultAggregatorFactory.asJsonObject(this.getAMOUNT_COLLECTED()));
		prFieldStore.add(PR_FIELDS.ARREARS_N_DEMAND.toString(), ResultAggregatorFactory.asJsonObject(this.getARREARS_N_DEMAND()));
		prFieldStore.add(PR_FIELDS.CMD_COLLECTED.toString(), ResultAggregatorFactory.asJsonObject(this.getCMD_COLLECTED()));
		prFieldStore.add(PR_FIELDS.COLLECTION_DATE.toString(), ResultAggregatorFactory.asJsonObject(this.getCOLLECTION_DATE()));
		prFieldStore.add(PR_FIELDS.COLLECTION_DELTA.toString(), ResultAggregatorFactory.asJsonObject(this.getCOLLECTION_DELTA()));
		prFieldStore.add(PR_FIELDS.COLLECTION_TIME.toString(), ResultAggregatorFactory.asJsonObject(this.getCOLLECTION_TIME()));
		prFieldStore.add(PR_FIELDS.DISTRIBUTION.toString(), ResultAggregatorFactory.asJsonObject(this.getDISTRIBUTION()));
		prFieldStore.add(PR_FIELDS.ERO.toString(), ResultAggregatorFactory.asJsonObject(this.getERO()));
		prFieldStore.add(PR_FIELDS.LAST_PAID_AMOUNT.toString(), ResultAggregatorFactory.asJsonObject(this.getLAST_PAID_AMOUNT()));
		prFieldStore.add(PR_FIELDS.LAST_PAID_DATE.toString(), ResultAggregatorFactory.asJsonObject(this.getLAST_PAID_DATE()));
		prFieldStore.add(PR_FIELDS.LAST_PAID_RECEIPT_NO.toString(), ResultAggregatorFactory.asJsonObject(this.getLAST_PAID_RECEIPT_NO()));
		prFieldStore.add(PR_FIELDS.MACHINE_CODE.toString(), ResultAggregatorFactory.asJsonObject(this.getMACHINE_CODE()));
		prFieldStore.add(PR_FIELDS.MACHINE_NUMBER.toString(), ResultAggregatorFactory.asJsonObject(this.getMACHINE_NUMBER()));
		prFieldStore.add(PR_FIELDS.NEW_ARREARS.toString(), ResultAggregatorFactory.asJsonObject(this.getNEW_ARREARS()));
		prFieldStore.add(PR_FIELDS.OTHERS_COLLECTED.toString(), ResultAggregatorFactory.asJsonObject(this.getOTHERS_COLLECTED()));
		prFieldStore.add(PR_FIELDS.RC_CODE.toString(), ResultAggregatorFactory.asJsonObject(this.getRC_CODE()));
		prFieldStore.add(PR_FIELDS.RC_COLLECTED.toString(), ResultAggregatorFactory.asJsonObject(this.getRC_COLLECTED()));
		prFieldStore.add(PR_FIELDS.RECEIPT_NUMBER.toString(), ResultAggregatorFactory.asJsonObject(this.getRECEIPT_NUMBER()));
		prFieldStore.add(PR_FIELDS.REMARKS.toString(), ResultAggregatorFactory.asJsonObject(this.getREMARKS()));
		prFieldStore.add(PR_FIELDS.REMARKS_AMOUNT.toString(), ResultAggregatorFactory.asJsonObject(this.getREMARKS_AMOUNT()));
		prFieldStore.add(PR_FIELDS.SC_NO.toString(), ResultAggregatorFactory.asJsonObject(this.getSC_NO()));
		prFieldStore.add(PR_FIELDS.SECTION.toString(), ResultAggregatorFactory.asJsonObject(this.getSECTION()));
		prFieldStore.add(PR_FIELDS.TC_SEAL_NO.toString(), ResultAggregatorFactory.asJsonObject(this.getTC_SEAL_NO()));
		prFieldStore.add(PR_FIELDS.USC_NO.toString(), ResultAggregatorFactory.asJsonObject(this.getUSC_NO()));
		return prFieldStore.toString();
	}

	public static DevicePr fetchDevicePrById(String id) {
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			DevicePr c =
				pm.getObjectById(DevicePr.class, 
						KeyFactory.stringToKey(id));
			c = pm.detachCopy(c);
			c.loadPrFieldStore();
			return c;
		} catch (javax.jdo.JDOObjectNotFoundException e ) {
			log.info("Could not fetch object DevicePr with Id: "+id, e);
		}
		return null;
	}
	
	public static DevicePr fetchDevicePrByDevicePrCode(String prCode) {
		if(prCode != null && !prCode.isEmpty()) {
			PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
			try {
				DevicePr c =
					pm.getObjectById(DevicePr.class, prCode);
				c = pm.detachCopy(c);
				c.loadPrFieldStore();
				return c;
			} catch (javax.jdo.JDOObjectNotFoundException e ) {
				log.info("Could not fetch object DevicePr with prCode: "+prCode+
						", error: " + e.getMessage());
			}
		}
		return null;
	}
	
	public static List<DevicePr> fetchDevicePrs(String deviceName) {
		//QUITE RESOURCE INTENSIVE OPERATION.
		throw new IllegalArgumentException("Unsupported operation");
		/*if (deviceName == null || deviceName.trim().equals("")) {
			return Collections.EMPTY_LIST;
		}
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		Query query = pm.newQuery(
				"select from " + DevicePr.class.getName() +
				" where deviceName == :pDeviceName ");
				// DATASTORE OPTIMIZATION: REMOVED ACTIVE FLAG AS ITS NOT PERSISTABLE
				//" where active == 1 && deviceName == :pDeviceName ");
		try {
			List<DevicePr> results = 
				(List<DevicePr>)query.execute(deviceName);
			results = (List<DevicePr>)pm.detachCopyAll(results);
			for (DevicePr pr : results) {
				pr.loadPrFieldStore();
			}
			return results;
		} finally {
			query.closeAll();
		}*/
	}
	
	public static List<DevicePr> fetchAllDevicePrs() {
		//QUITE RESOURCE INTENSIVE OPERATION.
		throw new IllegalArgumentException("Unsupported operation");
		
		/*PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		Query query = pm.newQuery(
				"select from " + DevicePr.class.getName()
				+" where active == 1 "); // DATASTORE OPTIMIZATION: REMOVED ACTIVE FLAG AS ITS NOT PERSISTABLE
				
		try {
			List<DevicePr> results = 
				(List<DevicePr>)query.execute();
			results = (List<DevicePr>)pm.detachCopyAll(results);
			for (DevicePr pr : results) {
				pr.loadPrFieldStore();
			}
			return results;
		} finally {
			query.closeAll();
		}*/
	}
	
	// Use cursor concept to avoid PR loss with boundary dates.
	//https://developers.google.com/appengine/docs/java/datastore/jdo/queries#Query_Cursors
	public static List<DevicePr> fetchDevicePrsCreatedBetween(Date createdAfter, Date createdBefore) {
		if (createdAfter == null || createdBefore == null ||
			createdAfter.after(createdBefore)) {
			return Collections.EMPTY_LIST;
		}
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		Query query = pm.newQuery(
				"select from " + DevicePr.class.getName() +
				" where createdDate >= :pCreatedAfter && createdDate <= :pCreatedBefore " +
				" order by createdDate ascending ");
				// DATASTORE OPTIMIZATION: REMOVED ACTIVE FLAG AS ITS NOT PERSISTABLE
				//" where active == 1 && createdDate >= :pCreatedAfter && createdDate <= :pCreatedBefore ");
		try {
			List<DevicePr> results = 
				(List<DevicePr>)query.execute(createdAfter, createdBefore);
			results = (List<DevicePr>)pm.detachCopyAll(results);
			for (DevicePr pr : results) {
				pr.loadPrFieldStore();
			}
			return results;
		} finally {
			query.closeAll();
		}
	}
	
	public static BatchResult<List<DevicePr>> fetchDevicePrsCreatedAfter(
			Date createdAfter, String wsCursor) {
		if (createdAfter == null) {
			BatchResult<List<DevicePr>> br = 
				new BatchResult<List<DevicePr>>(Collections.EMPTY_LIST,null);
			return br;
		}
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		Query query = pm.newQuery(
				"select from " + DevicePr.class.getName() +
				" where createdDate >= :pCreatedAfter " +
				" order by createdDate ascending ");
		// setting max results in a batch
		query.setRange(0, 1000);
		
		if (!CommonUtil.isEmpty(wsCursor)) {
			Map<String, Object> extensionMap = new HashMap<String,Object>();
			Cursor cursor = Cursor.fromWebSafeString(wsCursor);
			extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
			query.setExtensions(extensionMap);
		}
		
		try {
			List<DevicePr> results = 
				(List<DevicePr>)query.execute(createdAfter);
			Cursor cursor = JDOCursorHelper.getCursor(results);
			String cursorString = cursor.toWebSafeString();
			
			results = (List<DevicePr>)pm.detachCopyAll(results);
			for (DevicePr pr : results) {
				pr.loadPrFieldStore();
			}
			
			BatchResult<List<DevicePr>> batchResult = 
				new BatchResult<List<DevicePr>>(results,cursorString);
			
			return batchResult;
		} finally {
			query.closeAll();
		}
	}
	
	public void save() {
		//boolean isNew = this.isInMemory();
		//this.deriveOrgIdentifiers(this.USC_NO);
		if(fetchDevicePrByDevicePrCode(get_DEVICE_PR_CODE()) != null) {
			log.info("DevicePr already exist with Pr code: " +
					this.get_DEVICE_PR_CODE());
			return;
		}
		super.save();
		// Do not queue, we are taking a different approach
		//
		/*if (isNew) {
			Util.queuePrSummary(this);
		}*/
	}
	
	/**
	 * Uniqueue Code 13 Digit disect
	 *	
	 *	1211200000023
	 *	   000023 Sc No
	 *         00 Dist Code
	 *          2 Section Code
	 *          1 Subdivision Code
	 *          1 ERO Code
	 *          2 Division Code
	 *          1 Circle Code
	 * @param uscNo
	 */
	private void deriveOrgIdentifiers(String uscNo) {
		if (uscNo == null || uscNo.trim().length() == 0) {
			return;
		}
		uscNo = uscNo.trim();
		if (uscNo.length() != 13) {
			log.info("Bad UscNo: "+uscNo);
			return;
		}
		int endIndex = 0;
		
		String companyCode = "0";
		
		endIndex = 1;
		String circleCode = uscNo.substring(0,endIndex);
		
		endIndex += 1;
		String divisionCode = uscNo.substring(0,endIndex);
		
		endIndex += 1;
		String eroCode = uscNo.substring(0,endIndex);
		
		endIndex += 1;
		String subDivisionCode = uscNo.substring(0,endIndex);
		
		endIndex += 1;
		String sectionCode = uscNo.substring(0,endIndex);
		
		endIndex += 2;
		String distCode = uscNo.substring(0,endIndex);
		
		//endIndex += 6;
		//String secNo = uscNo.substring(0,endIndex);
		
		this.setCompanyCode(companyCode);
		this.setCircleCode(circleCode);
		this.setDivisionCode(divisionCode);
		this.setEroCode(eroCode);
		this.setSubDivisionCode(subDivisionCode);
		this.setSectionCode(sectionCode);
		this.setDistCode(distCode);
	}

	public boolean isHighValue() {
		boolean isHighValue = false;
		String distribution = 
			this.getDISTRIBUTION() != null ? this.getDISTRIBUTION().toUpperCase() : "";
		if (distribution.startsWith("HV-") || distribution.startsWith("NS-")) {
			isHighValue = true;
		} else {
			isHighValue = false;
		}
		return isHighValue;
	}

	@Override
	public String toString() {
		return "DevicePr [getCreatedDate()=" + getCreatedDate()
				+ ", getLastModifiedDate()=" + getLastModifiedDate()
				+ ", getActive()=" + getActive() + ", getChangeNumber()="
				+ getChangeNumber() + ", getId()=" + getId()
				+ ", getDeviceName()=" + getDeviceName() + ", getUSC_NO()="
				+ getUSC_NO() + ", getSC_NO()=" + getSC_NO() + ", getERO()="
				+ getERO() + ", getSECTION()=" + getSECTION()
				+ ", getDISTRIBUTION()=" + getDISTRIBUTION()
				+ ", getRECEIPT_NUMBER()=" + getRECEIPT_NUMBER()
				+ ", getCOLLECTION_DATE()=" + getCOLLECTION_DATE()
				+ ", getCOLLECTION_TIME()=" + getCOLLECTION_TIME()
				+ ", getARREARS_N_DEMAND()=" + getARREARS_N_DEMAND()
				+ ", getRC_COLLECTED()=" + getRC_COLLECTED()
				+ ", getACD_COLLECTED()=" + getACD_COLLECTED()
				+ ", getOTHERS_COLLECTED()=" + getOTHERS_COLLECTED()
				+ ", getRC_CODE()=" + getRC_CODE() + ", getMACHINE_CODE()="
				+ getMACHINE_CODE() + ", getLAST_PAID_RECEIPT_NO()="
				+ getLAST_PAID_RECEIPT_NO() + ", getLAST_PAID_DATE()="
				+ getLAST_PAID_DATE() + ", getLAST_PAID_AMOUNT()="
				+ getLAST_PAID_AMOUNT() + ", getTC_SEAL_NO()="
				+ getTC_SEAL_NO() + ", getREMARKS()=" + getREMARKS()
				+ ", getREMARKS_AMOUNT()=" + getREMARKS_AMOUNT()
				+ ", getAGL_AMOUNT()=" + getAGL_AMOUNT()
				+ ", getAGL_SERVICES()=" + getAGL_SERVICES()
				+ ", getMACHINE_NUMBER()=" + getMACHINE_NUMBER()
				+ ", getNEW_ARREARS()=" + getNEW_ARREARS()
				+ ", getCMD_COLLECTED()=" + getCMD_COLLECTED()
				+ ", getCOLLECTION_DELTA()=" + getCOLLECTION_DELTA()
				+ ", get_DEVICE_PR_CODE()=" + get_DEVICE_PR_CODE()
				+ ", getCollectionDate()=" + getCollectionDate()
				+ ", getAMOUNT_COLLECTED()=" + getAMOUNT_COLLECTED()
				+ ", getCompanyCode()=" + getCompanyCode()
				+ ", getCircleCode()=" + getCircleCode()
				+ ", getDivisionCode()=" + getDivisionCode()
				+ ", getEroCode()=" + getEroCode() + ", getSubDivisionCode()="
				+ getSubDivisionCode() + ", getSectionCode()="
				+ getSectionCode() + ", getDistCode()=" + getDistCode()
				+ ", isHighValue()=" + isHighValue() + "]";
	}
}