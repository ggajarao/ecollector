package ebilly.admin.server.db;

import java.io.Serializable;

import javax.jdo.PersistenceManager;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import ebilly.admin.server.Util;
import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.shared.AppConfig;

@PersistenceCapable(identityType = IdentityType.APPLICATION, detachable="true")
public class OrgDistribution implements Serializable {
	private static final long serialVersionUID = 1L;
	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
	private String id;
	
	@Persistent
	@Extension(vendorName="datanucleus", key="gae.pk-name", value="true")
	private String odstCode;
	
	@Persistent
	private String odstName;
	
	/*@Persistent 
	private String odstSectionId;*/
	
	@Persistent
	private OrgSection odstSection;

	public OrgDistribution() {
		
	}
	
	public String getId() {
		return id;
	}
	
	public String getOdstCode() {
		return odstCode;
	}

	public void setOdstCode(String odstCode) {
		this.odstCode = odstCode;
	}

	public void setOdstName(String odstName) {
		this.odstName = odstName;
	}
	
	public String getOdstName() {
		return this.odstName;
	}

	/*public String getOdstSectionId() {
		return odstSectionId;
	}

	public void setOdstSectionId(String odstSectionId) {
		this.odstSectionId = odstSectionId;
	}*/

	public void save()
	{
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			pm.makePersistent(this);
			Util.removeAppCacheValue(AppConfig.CACHE_ORG_STRUCTURE_DATA);
		}
		finally {
			pm.close();
		}
	}
	
	public static OrgDistribution fetchDistributionById(String distributionId) {
		Key key = KeyFactory.stringToKey(distributionId);
		return OrgDistribution.fetchDistributionById(key);
	}

	public static OrgDistribution fetchDistributionById(Key key) {
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			OrgDistribution c = pm.getObjectById(OrgDistribution.class, key);
			return pm.detachCopy(c);
		} catch (javax.jdo.JDOObjectNotFoundException e ) {
			//log.info("Could not fetch object", e);
		}
		return null;
	}
	
	@Override
	public String toString() {
		return "OrgDistribution [id=" + id + ", odstCode="
				+ odstCode + ", odstName=" + odstName + "]";
	}

	public static void delete(String entityId) {
		Key key = KeyFactory.stringToKey(entityId);
		OrgDistribution r = OrgDistribution.fetchDistributionById(key);
		if (r != null) {
			PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
			pm.deletePersistent(r);
			Util.removeAppCacheValue(AppConfig.CACHE_ORG_STRUCTURE_DATA);
		}
	}
}
