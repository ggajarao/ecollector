package ebilly.admin.server.db;

import java.io.Serializable;
import java.util.Date;

import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import ebilly.admin.shared.AppConfig.REMOTE_DEVICE_IMPORT_METHOD;
import ebilly.admin.shared.AppConfig.REMOTE_DEVICE_UPLOAD_STATES;

@PersistenceCapable(identityType = IdentityType.APPLICATION, detachable="true")
public class DeviceFile implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
	private String id;
	
	@Persistent
	private Device device;
	
	@Persistent
	private String deviceServicesFileKey;
	
	@Persistent
	private String deviceServicesFileName;
	
	// DATE ON WHICH THE FILE IS
	// UPLOADED INTO CLOUD
	@Persistent
	private Date deviceFileStagingDate;
	
	// REMOTE DEVICE UPLOAD STATE, WHTHER
	// FILE IMPORTED INTO REMOTE STATE OR
	// NOT IMPORTED
	@Persistent
	private String remoteDeviceFileUploadState;
	
	// THE DATE ON WHICH THE FILE IS UPLOADED INTO
	// REMOTE DEVICE
	@Persistent
	private Date remoteDeviceFileUploadDate;
	
	// THIS SPECIFIES HOW THE DEVICE SHOULD
	// IMPORT THIS FILE, WHETHER IT SHOULD
	// REMOVE EXISTING SERVICES OR NOT
	@Persistent
	private String remoteDeviceImportMethod;
	
	public DeviceFile() {
		
	}
	
	public String getDeviceServicesFileKey() {
		return deviceServicesFileKey;
	}
	
	public void setDeviceServicesFileKey(String blobKey) {
		deviceServicesFileKey = blobKey;
	}

	public String getDeviceServicesFileName() {
		return deviceServicesFileName;
	}

	public void setDeviceServicesFileName(String deviceFileName) {
		this.deviceServicesFileName = deviceFileName;
	}

	public Device fetchDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public Date getDeviceFileStagingDate() {
		return deviceFileStagingDate;
	}

	public void setDeviceFileStagingDate(Date deviceFileStagingDate) {
		this.deviceFileStagingDate = deviceFileStagingDate;
	}

	public String getRemoteDeviceFileUploadState() {
		return remoteDeviceFileUploadState;
	}

	public void setRemoteDeviceFileUploadState(REMOTE_DEVICE_UPLOAD_STATES remoteDeviceFileUploadState) {
		this.remoteDeviceFileUploadState = remoteDeviceFileUploadState.toString();
	}

	public Date getRemoteDeviceFileUploadDate() {
		return remoteDeviceFileUploadDate;
	}

	public void setRemoteDeviceFileUploadDate(Date remoteDeviceFileUploadDate) {
		this.remoteDeviceFileUploadDate = remoteDeviceFileUploadDate;
	}

	public String getId() {
		return id;
	}
	
	public String getRemoteDeviceImportMethod() {
		return this.remoteDeviceImportMethod;
	}
	
	public void setRemoteDeviceImportMethod(REMOTE_DEVICE_IMPORT_METHOD remoteDeviceImportMethod) {
		if (remoteDeviceImportMethod == null) this.remoteDeviceImportMethod = null;
		this.remoteDeviceImportMethod = remoteDeviceImportMethod.toString();
	}
}
