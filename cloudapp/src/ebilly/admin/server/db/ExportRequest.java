package ebilly.admin.server.db;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.KeyFactory;

import ebilly.admin.server.core.AppLogger;
import ebilly.admin.server.core.db.BaseEntityNonPersistent;
import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.AppConfig.EXPORT_STATUS;

@PersistenceCapable(identityType = IdentityType.APPLICATION, detachable="true")
public class ExportRequest extends BaseEntityNonPersistent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final AppLogger log = AppLogger.getLogger(ExportRequest.class.getName());
	
	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
	private String id;
	
	@Persistent
	private Date createdDate;
	
	@Persistent
	private Date lastModifiedDate;
	
	@Persistent
	private int active = 1;
	
	@Persistent
	private int changeNumber;
	
	@Persistent
	private String objectName;
	
	@Persistent
	private ArrayList<Filter> filters;
	
	@Persistent
	private int exportStatus;
	
	@Persistent
	private String resultFileKey;
	
	@Persistent
	private String errorMessage;
	
	@Persistent
	private String summaryMessage;
	
	@Persistent
	private int attempts;
	
	public ExportRequest() {
		
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public int getChangeNumber() {
		return changeNumber;
	}

	public void setChangeNumber(int changeNumber) {
		this.changeNumber = changeNumber;
	}
	
	public String getId() {
		return id;
	}
	
	@Override
	public void setEntityId(String id) {
		this.id = id;
	}

	@Override
	protected Object getUniqueId() {
		throw new IllegalArgumentException("Unsupported UniqueId requested");
	}

	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public ArrayList<Filter> fetchFilters() {
		if (this.filters == null) {
			this.filters = new ArrayList<Filter>();
		}
		return filters;
	}
	
	public String filtersAsReadableString() {
		ArrayList<Filter> fs = fetchFilters();
		StringBuffer s = new StringBuffer();
		for ( Filter f : fs ) {
			s.append("<li>");
			s.append(f.asReadableString());
		}
		return s.toString();
	}
	
	public void addFilter(Filter filter) {
		this.fetchFilters().add(filter);
	}

	public String getResultFileKey() {
		return resultFileKey;
	}

	public void setResultFileKey(String resultFile) {
		this.resultFileKey = resultFile;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getSummaryMessage() {
		return summaryMessage;
	}

	public void setSummaryMessage(String summaryMessage) {
		this.summaryMessage = summaryMessage;
	}

	public void setStatus(EXPORT_STATUS status) {
		this.exportStatus = status.getCode();
	}
	
	public EXPORT_STATUS getStatus() {
		return EXPORT_STATUS.asValue(this.exportStatus);
	}
	
	public int getAttempts() {
		return attempts;
	}

	public void setAttempts(int attempts) {
		this.attempts = attempts;
	}

	@Override
	public String toString() {
		return "ExportRequest [id=" + id + ", createdDate=" + createdDate
				+ ", lastModifiedDate=" + lastModifiedDate + ", active="
				+ active + ", changeNumber=" + changeNumber + ", objectName="
				+ objectName + ", filters=" + filters + ", exportStatus=" + exportStatus
				+ ", resultFileKey=" + resultFileKey + ", errorMessage="
				+ errorMessage + ", summaryMessage=" + summaryMessage
				+ ", attempts= " + attempts+"]";
	}

	public static List<ExportRequest> fetchNewRequests() {
		return fetechPrExportRequestsByStatus(AppConfig.EXPORT_STATUS.NEW);
	}
	
	public static List<ExportRequest> fetchInprogressRequests() {
		return fetechPrExportRequestsByStatus(AppConfig.EXPORT_STATUS.INPROGRESS);
	}
	
	private static List<ExportRequest> fetechPrExportRequestsByStatus(EXPORT_STATUS status) {
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		StringBuffer q = new StringBuffer();
		q.append("select from ").append(ExportRequest.class.getName())
		 .append(" where active == 1 ")
		 .append("    && exportStatus == pStatus")
		 .append("    && lastModifiedDate > pSince")
		 .append(" parameters java.lang.Integer pStatus, java.util.Date pSince")
		 .append(" order by lastModifiedDate ascending ");
		 
		Query query = pm.newQuery(q.toString());
		//query.getFetchPlan().addGroup("defaultFg");
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_MONTH, -1);
		Date sinceDate = c.getTime();
		try {
			List<ExportRequest> results = 
				(List<ExportRequest>) query.execute(status.getCode(), sinceDate);
			if (results.isEmpty()) return Collections.emptyList();
			results = (List<ExportRequest>) pm.detachCopyAll(results);
			return results;
		} finally {
			query.closeAll();
		}
	}
	
	public static ExportRequest fetchExportRequestById(String id) {
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			ExportRequest c =
				pm.getObjectById(ExportRequest.class, 
						KeyFactory.stringToKey(id));
			c.fetchFilters();
			return pm.detachCopy(c);
		} catch (javax.jdo.JDOObjectNotFoundException e ) {
			log.info("Could not fetch object ExportRequest, id: "+id, e);
		}
		return null;
	}
	
	private static ExportRequest updateRequestState(String exportRequestId, 
			EXPORT_STATUS status, String errorMessage) {
		ExportRequest ir = 
			ExportRequest.fetchExportRequestById(exportRequestId);
		if (ir == null) {
			return null;
		}
		
		if (EXPORT_STATUS.INPROGRESS.equals(status)) {
			ir.setAttempts(ir.getAttempts()+1);
		}
		
		ir.setStatus(status);
		if (errorMessage != null) ir.setErrorMessage(errorMessage);
		ir.save();
		return ExportRequest.fetchExportRequestById(exportRequestId);
	}
	
	public static ExportRequest updateRequestStateToCompleted(String exportRequestId) {
		return updateRequestState(exportRequestId, EXPORT_STATUS.COMPLETED, null);
	}

	public static ExportRequest updateRequestStateToInprogress(
			String exportRequestId) {
		return updateRequestState(exportRequestId, EXPORT_STATUS.INPROGRESS, null);
	}

	public static ExportRequest updateRequestStateToFailed(String exportRequestId, String errorMessage) {
		return updateRequestState(exportRequestId, EXPORT_STATUS.FAILED, null);
	}
	
	public static ExportRequest updateRequestStateToNew(String exportRequestId) {
		return updateRequestState(exportRequestId, EXPORT_STATUS.NEW, null);
	}
	
	public String prepareFileName() {
		StringBuffer s = new StringBuffer();
		if (this.objectName != null && this.objectName.trim().length() != 0) {
			s.append(this.objectName).append("-");
		}
		for (Filter f : this.fetchFilters()) {
			s.append(f.getFieldName()).append("-")
				.append(f.getValue()).append("-");
		}
		if (s.length() == 0) {
			s.append(System.currentTimeMillis());
		}
		return s.toString().replaceAll("[^A-Za-z0-9\\\\-]", "");
	}
}
