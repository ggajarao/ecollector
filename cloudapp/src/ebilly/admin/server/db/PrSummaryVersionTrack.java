package ebilly.admin.server.db;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import ebilly.admin.server.core.AppLogger;
import ebilly.admin.server.core.db.BaseEntityNonPersistent;
import ebilly.admin.server.core.db.DBUtils;

@PersistenceCapable(identityType = IdentityType.APPLICATION, detachable="true")
public class PrSummaryVersionTrack extends BaseEntityNonPersistent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final AppLogger log = AppLogger.getLogger(PrSummaryVersionTrack.class.getName());
	
	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
	private String id;
	
	@Persistent
	private Date createdDate;
	
	@Persistent
	private Date lastModifiedDate;
	
	@Persistent
	private int active = 1;
	
	@Persistent
	private int changeNumber;
	
	@Persistent
	private String prSummaryDataId;
	
	@Persistent
	private String newVersionNumber;
	
	@Persistent
	private String oldVersionNumber;
	
	@Persistent
	private String oldVersionDataFileKey;
	
	public PrSummaryVersionTrack() {
		
	}
	
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public int getChangeNumber() {
		return changeNumber;
	}

	public void setChangeNumber(int changeNumber) {
		this.changeNumber = changeNumber;
	}
	
	public String getId() {
		return id;
	}
	
	@Override
	public void setEntityId(String id) {
		this.id = id;
	}

	@Override
	protected Object getUniqueId() {
		throw new IllegalArgumentException("Unsupported UniqueId requested");
	}

	public String getPrSummaryDataId() {
		return prSummaryDataId;
	}

	public void setPrSummaryDataId(String prSummaryDataId) {
		this.prSummaryDataId = prSummaryDataId;
	}

	public String getNewVersionNumber() {
		return newVersionNumber;
	}

	public void setNewVersionNumber(String newVersionNumber) {
		this.newVersionNumber = newVersionNumber;
	}

	public String getOldVersionNumber() {
		return oldVersionNumber;
	}

	public void setOldVersionNumber(String oldVersionNumber) {
		this.oldVersionNumber = oldVersionNumber;
	}
	
	public String getOldVersionDataFileKey() {
		return oldVersionDataFileKey;
	}

	public void setOldVersionDataFileKey(String oldVersionDataFileKey) {
		this.oldVersionDataFileKey = oldVersionDataFileKey;
	}

	public static List<PrSummaryVersionTrack> fetchPrSummaryVersionsByPrSummaryDataId(String prSummaryDataId) {
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		StringBuffer q = new StringBuffer();
		q.append("select from ").append(PrSummaryVersionTrack.class.getName())
		 .append(" where active == 1 ")
		 .append("    && prSummaryDataId == pPrSummaryDataId")
		 .append(" parameters java.lang.String pPrSummaryDataId ")
		 .append(" order by createdDate ascending ");
		 
		Query query = pm.newQuery(q.toString());
		try {
			List<PrSummaryVersionTrack> results = 
				(List<PrSummaryVersionTrack>) query.execute(prSummaryDataId);
			if (results.isEmpty()) return Collections.emptyList();
			results = (List<PrSummaryVersionTrack>) pm.detachCopyAll(results);
			return results;
		} finally {
			query.closeAll();
		}
	}
}