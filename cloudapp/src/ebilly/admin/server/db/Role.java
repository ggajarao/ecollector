package ebilly.admin.server.db;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

import ebilly.admin.server.core.AppLogger;
import ebilly.admin.server.core.db.BaseEntity;
import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.shared.AppConfig;

@PersistenceCapable(identityType = IdentityType.APPLICATION, detachable="true")
public class Role extends BaseEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final AppLogger log = AppLogger.getLogger(Role.class.getName());

	@Persistent
	@Extension(vendorName="datanucleus", key="gae.pk-name", value="true")
	private String roleCode;
	
	@Persistent
	private String roleName;
	
	@Persistent
	private String roleDescription;
	
	public Role() {
		super();
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleDescription() {
		return roleDescription;
	}

	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}
	
	@Override
	protected Object getUniqueId() {
		return this.roleCode;
	}

	public static Role fetchRoleByCode(String code)
	{
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			Role r = pm.getObjectById(Role.class, code);
			return pm.detachCopy(r);
		} catch (javax.jdo.JDOObjectNotFoundException e ) {
			log.info("Could not fetch object", e);
		}
		return null;
		
//		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
//		StringBuffer q = new StringBuffer();
//		q.append("select from ").append(Role.class.getName())
//		 .append(" where roleCode == pCode ")
//		 .append("    && active == 1 ")
//		 .append(" parameters String pCode ");
//		 
//		Query query = pm.newQuery(q.toString());
//		try {
//			List<Role> results = (List<Role>) query.execute(code);
//			if (results.isEmpty()) return null;
//			results = (List<Role>) pm.detachCopyAll(results);
//			return results.get(0);
//		} finally {
//			query.closeAll();
//		}
	}
	
	public static Role fetchRoleById(String roleId) {
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		StringBuffer q = new StringBuffer();
		q.append("select from ").append(Role.class.getName())
		 .append(" where id == pRoleId ")
		 .append("    && active == 1 ")
		 .append(" parameters String pRoleId ");
		 
		Query query = pm.newQuery(q.toString());
		try {
			List<Role> results = (List<Role>) query.execute(roleId);
			if (results.isEmpty()) return null;
			results = (List<Role>) pm.detachCopyAll(results);
			return results.get(0);
		} finally {
			query.closeAll();
		}
	}
	
	public static List<Role> fetchAllRoles() {
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		StringBuffer q = new StringBuffer();
		q.append("select from ").append(Role.class.getName())
		 .append(" where active == 1 ");
		 
		Query query = pm.newQuery(q.toString());
		try {
			List<Role> results = (List<Role>) query.execute();
			if (results.isEmpty()) return null;
			results = (List<Role>) pm.detachCopyAll(results);
			return results;
		} finally {
			query.closeAll();
		}
	}
	
	public static void seedRoles() {
		Role r;
		
		r = new Role();
		r.setRoleCode(AppConfig.ROLE_CODE_SUPER_ADMIN);
		r.setRoleName("Super Administrator");
		r.setRoleDescription("With all possible access");
		r.simplePersist();
		
		r = new Role();
		r.setRoleCode(AppConfig.ROLE_CODE_APP_ADMIN);
		r.setRoleName("Application Administrator");
		r.setRoleDescription("Application Administrator Role");
		r.simplePersist();
		
		r = new Role();
		r.setRoleCode(AppConfig.ROLE_CODE_AAO_USER);
		r.setRoleName("AAO User");
		r.setRoleDescription("Asst., Account Officer");
		r.simplePersist();
		
		r = new Role();
		r.setRoleCode(AppConfig.ROLE_CODE_CASHIER_USER);
		r.setRoleName("Cashier User");
		r.setRoleDescription("Asst., Account Officer");
		r.simplePersist();
		
		r = new Role();
		r.setRoleCode(AppConfig.ROLE_CODE_DEVICE_USER);
		r.setRoleName("Device User");
		r.setRoleDescription("Device User Role");
		r.simplePersist();
	}	
}

