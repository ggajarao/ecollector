package ebilly.admin.server.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.FetchGroup;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import ebilly.admin.server.Util;
import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.shared.AppConfig;

@PersistenceCapable(identityType = IdentityType.APPLICATION, detachable="true")
@FetchGroup(name = "orgFgh", 
		members={@Persistent(name="ocrcDivisions"), @Persistent(name="ocmpCompany")})
public class OrgCircle implements Serializable {
	private static final long serialVersionUID = 1L;
	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
	private String id;
	
	@Persistent
	@Extension(vendorName="datanucleus", key="gae.pk-name", value="true")
	private String ocrcCode;
	
	@Persistent
	private String ocrcName;
	
	@Persistent
	private OrgCompany ocmpCompany; 
	
	/*@Persistent
	private String ocrcCompanyId;*/
	
	@Persistent(defaultFetchGroup = "true", mappedBy = "odvsCircle")
	private List<OrgDivision> ocrcDivisions;

	public OrgCircle() {
		
	}
	
	public String getId() {
		return id;
	}
	
	public List<OrgDivision> getOrgDivisions() {
		return ocrcDivisions;
	}

	public void setOrgDivisions(List<OrgDivision> orgDivisions) {
		this.ocrcDivisions = orgDivisions;
	}

	public void addOrgDivision(OrgDivision orgDivision) {
		if (this.ocrcDivisions == null) {
			 this.ocrcDivisions = new ArrayList<OrgDivision>();
		}
		/*if (this.getId() == null) throw new IllegalArgumentException("Save CIrcle first then add Division");
		orgDivision.setOdvsCircleId(this.getId());*/
		this.ocrcDivisions.add(orgDivision);
	}
	
	public OrgDivision findOrgDivision(Key divisionId) {
		if (this.ocrcDivisions == null) return null;
		String strDivisionId = KeyFactory.keyToString(divisionId);
		for (OrgDivision sem : this.ocrcDivisions) {
			if (sem.getId().equals(strDivisionId)) {
				return sem;
			}
		}
		return null;
	}
	
	public OrgDivision findOrgDivision(String divisionCode) {
		if (this.ocrcDivisions == null) return null;
		for (OrgDivision sem : this.ocrcDivisions) {
			if (sem.getOdvsCode().equals(divisionCode)) {
				return sem;
			}
		}
		return null;
	}
	
	public String getOcrcCode() {
		return ocrcCode;
	}

	public void setOcrcCode(String ocrcCode) {
		this.ocrcCode = ocrcCode;
	}

	public String getOcrcName() {
		return ocrcName;
	}

	public void setOcrcName(String ocrcName) {
		this.ocrcName = ocrcName;
	}

	/*public String getOcrcCompanyId() {
		return ocrcCompanyId;
	}

	public void setOcrcCompanyId(String ocrcCompanyId) {
		this.ocrcCompanyId = ocrcCompanyId;
	}*/

	public void save()
	{
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			pm.makePersistent(this);
			Util.removeAppCacheValue(AppConfig.CACHE_ORG_STRUCTURE_DATA);
		}
		finally {
			pm.close();
		}
	}
	
	public static OrgCircle fetchCircleById(String circleId) {
		Key key = KeyFactory.stringToKey(circleId);
		return OrgCircle.fetchCircleById(key);
	}

	public static OrgCircle fetchCircleById(Key key) {
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			pm.getFetchPlan().addGroup("acbFg");
			OrgCircle c = pm.getObjectById(OrgCircle.class, key);
			return pm.detachCopy(c);
		} catch (javax.jdo.JDOObjectNotFoundException e ) {
			//log.info("Could not fetch object", e);
		}
		return null;
	}
	
	@Override
	public String toString() {
		return "OrgCircle [id=" + id + ", ocrcCode="
				+ ocrcCode + ", ocrcName=" + ocrcName
				+ /*", ocrcCompanyId=" + ocrcCompanyId +*/ "]";
	}
	
	public static void delete(String entityId) {
		Key key = KeyFactory.stringToKey(entityId);
		OrgCircle r = OrgCircle.fetchCircleById(key);
		if (r != null) {
			PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
			pm.deletePersistent(r);
			Util.removeAppCacheValue(AppConfig.CACHE_ORG_STRUCTURE_DATA);
		}
	}
}
