package ebilly.admin.server.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.FetchGroup;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import ebilly.admin.server.Util;
import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.shared.AppConfig;

@PersistenceCapable(identityType = IdentityType.APPLICATION, detachable="true")
@FetchGroup(name = "orgFg", 
		members={@Persistent(name="orgSections"), @Persistent(name="osbdEro")})
public class OrgSubdivision implements Serializable {
	private static final long serialVersionUID = 1L;
	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
	private String id;
	
	@Persistent
	@Extension(vendorName="datanucleus", key="gae.pk-name", value="true")
	private String osbdCode;
	
	@Persistent
	private String osbdName;
	
	@Persistent
	private OrgEro osbdEro;
	
	/*@Persistent
	private String osbdDivisionId;*/
	
	@Persistent(defaultFetchGroup = "true", mappedBy = "osctSubdivision")
	private List<OrgSection> orgSections;

	public OrgSubdivision() {
		
	}
	
	public String getId() {
		return id;
	}
	
	/*	public AcademicRegulation getAcbRegulation() {
			return acbRegulation;
		}
	
		public void setAcbRegulation(AcademicRegulation acbRegulation) {
			this.acbRegulation = acbRegulation;
		}
	*/
	
	public List<OrgSection> getOrgSections() {
		return orgSections;
	}

	public void setOrgSections(List<OrgSection> orgSections) {
		this.orgSections = orgSections;
	}

	public void addOrgSection(OrgSection orgSection) {
		if (this.orgSections == null) {
			 this.orgSections = new ArrayList<OrgSection>();
		}
		/*if (this.getId() == null) throw new IllegalArgumentException("Save Subdivision first then add Section");
		orgSection.setOsctSubdivisionId(this.getId());*/
		this.orgSections.add(orgSection);
	}
	
	public OrgSection findOrgSection(Key sectionId) {
		if (this.orgSections == null) return null;
		String strSectionId = KeyFactory.keyToString(sectionId);
		for (OrgSection sect : this.orgSections) {
			if (sect.getId().equals(strSectionId)) {
				return sect;
			}
		}
		return null;
	}
	
	public OrgSection findOrgSection(String sectionCode) {
		if (this.orgSections == null) return null;
		for (OrgSection sect : this.orgSections) {
			if (sect.getOsctCode().equals(sectionCode)) {
				return sect;
			}
		}
		return null;
	}
	
	public String getOsbdCode() {
		return this.osbdCode;
	}
	
	public void setOsbdCode(String subdivisionCode) {
		this.osbdCode = subdivisionCode;
	}
	
	public String getOsbdName() {
		return osbdName;
	}

	public void setOsbdName(String osbdName) {
		this.osbdName = osbdName;
	}

	/*public String getOsbdDivisionId() {
		return osbdDivisionId;
	}

	public void setOsbdDivisionId(String osbdDivisionId) {
		this.osbdDivisionId = osbdDivisionId;
	}*/

	public void save()
	{
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			pm.makePersistent(this);
			Util.removeAppCacheValue(AppConfig.CACHE_ORG_STRUCTURE_DATA);
		}
		finally {
			pm.close();
		}
	}
	
	public static OrgSubdivision fetchSubdivisionById(String subdivisionId) {
		Key key = KeyFactory.stringToKey(subdivisionId);
		return OrgSubdivision.fetchSubdivisionById(key);
	}

	public static OrgSubdivision fetchSubdivisionById(Key key) {
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			pm.getFetchPlan().addGroup("acbFg");
			OrgSubdivision c = pm.getObjectById(OrgSubdivision.class, key);
			return pm.detachCopy(c);
		} catch (javax.jdo.JDOObjectNotFoundException e ) {
			//log.info("Could not fetch object", e);
		}
		return null;
	}

	@Override
	public String toString() {
		return "AcademicCourseBranch [id=" + id + ", osctCodee="
				+ osbdCode + ", osctName=" + osbdName + "]";
	}

	public static void delete(String entityId) {
		Key key = KeyFactory.stringToKey(entityId);
		OrgSubdivision r = OrgSubdivision.fetchSubdivisionById(key);
		if (r != null) {
			PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
			pm.deletePersistent(r);
			Util.removeAppCacheValue(AppConfig.CACHE_ORG_STRUCTURE_DATA);
		}
	}
}
