package ebilly.admin.server.db;

import java.util.Date;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.KeyFactory;

import ebilly.admin.server.Util;
import ebilly.admin.server.core.AppLogger;
import ebilly.admin.server.core.db.BaseEntityNonPersistent;
import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.AppConfig.DEVICE_COMMAND;
import ebilly.admin.shared.AppConfig.DEVICE_COMMAND_STATES;
import ebilly.admin.shared.CommonUtil;

@PersistenceCapable(identityType = IdentityType.APPLICATION, detachable="true")
public class DeviceCommand extends BaseEntityNonPersistent {
	
	private static final long serialVersionUID = 1L;
	
	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
	private String id;
	
	@Persistent
	private Date createdDate;

	@Persistent
	private Date lastModifiedDate;

	@Persistent
	private int active = 1;

	@Persistent
	private int changeNumber;
	
	@Persistent
	private String deviceName;
	
	// Device Command execution state
	@Persistent
	private String state;
	
	// THE DATE ON WHICH THE COMMAND IS CREATED
	@Persistent
	private Date commandDate;
	
	@Persistent
	private String commandCode;
	
	@Persistent
	private String commandParam1;
	
	@Persistent
	private String commandParam2;
	
	@Persistent
	private String commandParam3;
	
	@Persistent
	private Date executionDate;
	
	@Persistent
	private String executionNotes;
	
	@Persistent
	private long commandTimestamp;
	
	public DeviceCommand() {
		
	}
	
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public int getChangeNumber() {
		return changeNumber;
	}

	public void setChangeNumber(int changeNumber) {
		this.changeNumber = changeNumber;
	}

	public String getId() {
		return id;
	}

	@Override
	public void setEntityId(String id) {
		this.id = id;
	}

	@Override
	protected Object getUniqueId() {
		throw new IllegalArgumentException("Unsupported UniqueId requested");
	}
	
	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getState() {
		return state;
	}

	public void setState(DEVICE_COMMAND_STATES deviceCommandState) {
		this.state = deviceCommandState.toString();
	}

	public Date getCommandDate() {
		return commandDate;
	}

	public void setCommandDate(Date remoteDeviceFileUploadDate) {
		this.commandDate = remoteDeviceFileUploadDate;
	}

	public String getCommandCode() {
		return commandCode;
	}

	public void setCommandCode(DEVICE_COMMAND commandCode) {
		this.commandCode = commandCode.toString();
	}

	public String getCommandParam1() {
		return commandParam1;
	}

	public void setCommandParam1(String commandParam1) {
		this.commandParam1 = commandParam1;
	}

	public String getCommandParam2() {
		return commandParam2;
	}

	public void setCommandParam2(String commandParam2) {
		this.commandParam2 = commandParam2;
	}

	public String getCommandParam3() {
		return commandParam3;
	}

	public void setCommandParam3(String commandParam3) {
		this.commandParam3 = commandParam3;
	}

	public Date getExecutionDate() {
		return executionDate;
	}

	public void setExecutionDate(Date executionDate) {
		this.executionDate = executionDate;
	}

	public String getExecutionNotes() {
		return executionNotes;
	}

	public void setExecutionNotes(String executionNotes) {
		this.executionNotes = executionNotes;
	}
	
	public long getCommandTimestamp() {
		return commandTimestamp;
	}

	public void setCommandTimestamp(long commandTimestamp) {
		this.commandTimestamp = commandTimestamp;
	}
	
	@Override
	protected void onBeforeSave() {
		super.onBeforeSave();
		this.commandTimestamp = System.currentTimeMillis();
		this.commandDate = new Date();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void onAfterSave() {
		super.onAfterSave();
		
		List<DeviceCommand> deviceCommands = 
			(List<DeviceCommand>)Util.getAppCacheValue(
					AppConfig.CACHE_NEW_DEVICE_COMMANDS_BY_DEVICE_NAME+
					this.deviceName.toUpperCase());
		if (deviceCommands == null) return;
		if (AppConfig.DEVICE_COMMAND_STATES.NEW.toString().equals(this.state)) {
			// If status is new, and if it does not exits in cache
			// then add to cache
			if (!deviceCommands.contains(this)) {
					deviceCommands.add(this);
					Util.putAppCacheValue(
							AppConfig.CACHE_NEW_DEVICE_COMMANDS_BY_DEVICE_NAME+
							this.deviceName.toUpperCase(),deviceCommands);
			}
		} else {
			// If status is not new, and if it exits in cache
			// remove from the cache.
			if (deviceCommands.contains(this)) {
				deviceCommands.remove(this);
				Util.putAppCacheValue(
						AppConfig.CACHE_NEW_DEVICE_COMMANDS_BY_DEVICE_NAME+
						this.deviceName.toUpperCase(),deviceCommands);
			}
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof DeviceCommand)) return false;
		
		DeviceCommand other = (DeviceCommand)obj;
		if (this.id != null) {
			return this.id.equals(other.getId());
		} else {
			return false;
		}
	}

	/** *****************   Utility method **************** **/
	private static final AppLogger log = AppLogger.getLogger("DeviceCommand");
	
	public static DeviceCommand fetchDeviceCommandById(String id) {
		if (CommonUtil.isEmpty(id)) {
			log.info("fetchDeviceCommandById got null id");
			return null;
		}
		
		log.info("Fetching device command for id: "+ id);
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			DeviceCommand c =
				pm.getObjectById(DeviceCommand.class, 
						KeyFactory.stringToKey(id));
			return pm.detachCopy(c);
		} catch (javax.jdo.JDOObjectNotFoundException e ) {
			log.info("Could not fetch object DeviceCommand with Id: "+id, e);
		}
		return null;
	}
	
	private static List<DeviceCommand> fetchDeviceCommandsByDevice(
			String deviceName, DEVICE_COMMAND_STATES commandState) {
		AppLogger.getLogger("DeviceCommand").info("fetchDeviceCommandsByDevice, deviceName: ["+deviceName+"], commandState: "+commandState);
		if (deviceName == null || deviceName.trim().length() == 0 || commandState == null) {
			AppLogger.getLogger("DeviceCommand").info("Required parameters deviceName, or commandState missing !!");
			return null;
		}
			
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		Query query = pm.newQuery(
				"select from " + DeviceCommand.class.getName() +
				" where active == 1 && deviceName == :pdeviceName && state == :pState ");
		try {
			List<DeviceCommand> results = 
				(List<DeviceCommand>)query.execute(
						deviceName, commandState.toString());
			results = (List<DeviceCommand>)pm.detachCopyAll(results);
			return results;
		} finally {
			query.closeAll();
		}
	}

	@SuppressWarnings("unchecked")
	public static List<DeviceCommand> cFetchNewDeviceCommandsByDevice(
			String deviceName) {
		List<DeviceCommand> deviceCommands = 
			(List<DeviceCommand>)Util.getAppCacheValue(
					AppConfig.CACHE_NEW_DEVICE_COMMANDS_BY_DEVICE_NAME+
					deviceName.toUpperCase());
		if (deviceCommands != null) return deviceCommands;
		
		deviceCommands = DeviceCommand.fetchDeviceCommandsByDevice(deviceName, 
				DEVICE_COMMAND_STATES.NEW);
		if (deviceCommands != null) {
			Util.putAppCacheValue(
					AppConfig.CACHE_NEW_DEVICE_COMMANDS_BY_DEVICE_NAME+
					deviceName.toUpperCase(),deviceCommands);
		}
		return deviceCommands;
	}
}