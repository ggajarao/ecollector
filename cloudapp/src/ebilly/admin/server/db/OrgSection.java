package ebilly.admin.server.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.FetchGroup;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import ebilly.admin.server.Util;
import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.shared.AppConfig;

@PersistenceCapable(identityType = IdentityType.APPLICATION, detachable="true")
@FetchGroup(name = "orgFg", 
		members={@Persistent(name="orgDistributions"), @Persistent(name="osctSubdivision")})
public class OrgSection implements Serializable {
	private static final long serialVersionUID = 1L;
	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
	private String id;
	
	@Persistent
	@Extension(vendorName="datanucleus", key="gae.pk-name", value="true")
	private String osctCode;
	
	@Persistent
	private String osctName;
	
	@Persistent
	private OrgSubdivision osctSubdivision;
	
	/*@Persistent
	private String osctSubdivisionId;*/
	
	@Persistent(defaultFetchGroup = "true", mappedBy = "odstSection")
	private List<OrgDistribution> orgDistributions;

	public OrgSection() {
		
	}
	
	public String getId() {
		return id;
	}
	
	public List<OrgDistribution> getOrgDistributions() {
		return orgDistributions;
	}

	public void setOrgDistribution(List<OrgDistribution> orgDistributions) {
		this.orgDistributions = orgDistributions;
	}

	public void addOrgDistribution(OrgDistribution orgDistribution) {
		if (this.orgDistributions == null) {
			 this.orgDistributions = new ArrayList<OrgDistribution>();
		}
		/*if (this.getId() == null) throw new IllegalArgumentException("Save Section first then add Distribution");
		orgDistribution.setOdstSectionId(this.getId());*/
		this.orgDistributions.add(orgDistribution);
	}
	
	public OrgDistribution findOrgDistribution(Key distributionId) {
		if (this.orgDistributions == null) return null;
		String strDistributionId = KeyFactory.keyToString(distributionId);
		for (OrgDistribution dist : this.orgDistributions) {
			if (dist.getId().equals(strDistributionId)) {
				return dist;
			}
		}
		return null;
	}
	
	public OrgDistribution findOrgDistribution(String distributionCode) {
		if (this.orgDistributions == null) return null;
		for (OrgDistribution dist : this.orgDistributions) {
			if (dist.getOdstCode().equals(distributionCode)) {
				return dist;
			}
		}
		return null;
	}
	
	public String getOsctCode() {
		return this.osctCode;
	}
	
	public void setOsctCode(String sectionCode) {
		this.osctCode = sectionCode;
	}
	
	public String getOsctName() {
		return osctName;
	}

	public void setOsctName(String osctName) {
		this.osctName = osctName;
	}

	/*public String getOsctSubdivisionId() {
		return osctSubdivisionId;
	}

	public void setOsctSubdivisionId(String osctSubdivisionId) {
		this.osctSubdivisionId = osctSubdivisionId;
	}*/

	public void save()
	{
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			pm.makePersistent(this);
			Util.removeAppCacheValue(AppConfig.CACHE_ORG_STRUCTURE_DATA);
		}
		finally {
			pm.close();
		}
	}
	
	public static OrgSection fetchSectionById(String sectionId) {
		Key key = KeyFactory.stringToKey(sectionId);
		return OrgSection.fetchSectionById(key);
	}

	public static OrgSection fetchSectionById(Key key) {
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			pm.getFetchPlan().addGroup("acbFg");
			OrgSection c = pm.getObjectById(OrgSection.class, key);
			return pm.detachCopy(c);
		} catch (javax.jdo.JDOObjectNotFoundException e ) {
			//log.info("Could not fetch object", e);
		}
		return null;
	}

	@Override
	public String toString() {
		return "AcademicCourseBranch [id=" + id + ", osctCodee="
				+ osctCode + ", osctName=" + osctName + "]";
	}

	public static void delete(String entityId) {
		Key key = KeyFactory.stringToKey(entityId);
		OrgSection r = OrgSection.fetchSectionById(key);
		if (r != null) {
			PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
			pm.deletePersistent(r);
			Util.removeAppCacheValue(AppConfig.CACHE_ORG_STRUCTURE_DATA);
		}
	}
}
