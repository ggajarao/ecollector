package ebilly.admin.server.db;

import java.util.Collections;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

import com.google.appengine.api.datastore.Text;

import ebilly.admin.server.core.db.BaseEntity;
import ebilly.admin.server.core.db.DBUtils;

@PersistenceCapable(identityType = IdentityType.APPLICATION, detachable="true")
public class EmailSendRequest extends BaseEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Persistent
	private String toEmailAddress;
	
	@Persistent
	private String subject;
	
	@Persistent
	private Text emailMessage;
	
	@Persistent
	private int isSent;
	
	@Persistent
	private int sendAttempts;
	
	public String getToEmailAddress() {
		return toEmailAddress;
	}

	public void setToEmailAddress(String toEmailAddress) {
		this.toEmailAddress = toEmailAddress;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getEmailMessage() {
		if (emailMessage != null) return emailMessage.getValue();
		return null;
	}

	public void setEmailMessage(String emailMessage) {
		this.emailMessage = new Text(emailMessage);
	}

	public boolean isSent() {
		return isSent == 1;
	}

	public void setSent(boolean isSent) {
		if (isSent) {
			this.isSent = 1;
		} else {
			this.isSent = 0;
		}
	}

	public int getSendAttempts() {
		return sendAttempts;
	}

	public void setSendAttempts(int sendAttempts) {
		this.sendAttempts = sendAttempts;
	}

	public String getAudit() {
		return audit;
	}

	public void setAudit(String audit) {
		this.audit = audit;
	}

	@Persistent
	private String audit;
	
	public EmailSendRequest() {
		
	}
	
	@Override
	protected Object getUniqueId() {
		throw new IllegalArgumentException("getUniqueId not supported");
	}
	
	public static List<EmailSendRequest> fetchPendingEmailSendRequests(int attemptsUnder)
	{
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		StringBuffer q = new StringBuffer();
		q.append("select from ").append(EmailSendRequest.class.getName())
		 .append(" where isSent == 0 ")
		 .append("    && active == 1 ")
		 .append("    && sendAttempts <= pSendAttempts")
		 .append(" parameters int pSendAttempts");
		 
		Query query = pm.newQuery(q.toString());
		//query.setOrdering(" createdDate ascending ");
		try {
			List<EmailSendRequest> results = 
				(List<EmailSendRequest>) query.execute(attemptsUnder);
			if (results.isEmpty()) return Collections.emptyList();
			results = (List<EmailSendRequest>) pm.detachCopyAll(results);
			return results;
		} finally {
			query.closeAll();
		}
	}
}