package ebilly.admin.server.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.FetchGroup;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import ebilly.admin.server.Util;
import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.orgstruct.OrgCircleUi;
import ebilly.admin.shared.orgstruct.OrgCompanyUi;
import ebilly.admin.shared.orgstruct.OrgDistributionUi;
import ebilly.admin.shared.orgstruct.OrgDivisionUi;
import ebilly.admin.shared.orgstruct.OrgEroUi;
import ebilly.admin.shared.orgstruct.OrgSectionUi;
import ebilly.admin.shared.orgstruct.OrgSubdivisionUi;

@PersistenceCapable(identityType = IdentityType.APPLICATION, detachable="true")
@FetchGroup(name = "orgFg", 
		members={@Persistent(name="ocmpCircles")})
public class OrgCompany implements Serializable {
	private static final long serialVersionUID = 1L;
	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
	private String id;
	
	@Persistent
	@Extension(vendorName="datanucleus", key="gae.pk-name", value="true")
	private String ocmpCode;
	
	@Persistent private String ocmpName;
		
	@Persistent(mappedBy = "ocmpCompany")
	private List<OrgCircle> ocmpCircles; 

	public OrgCompany() {
		
	}
	
	public String getId() {
		return id;
	} 
	
	public String getCode() {
		return ocmpCode;
	}

	public List<OrgCircle> getOcmpCircles() {
		return ocmpCircles;
	}

	public void setOcmpCircles(List<OrgCircle> ocmpCircles) {
		this.ocmpCircles = ocmpCircles;
	}
	
	public void addOcmpCircle(OrgCircle orgCircle) {
		if (this.ocmpCircles == null) {
			this.ocmpCircles =
				new ArrayList<OrgCircle>();
		}
		this.ocmpCircles.add(orgCircle);
	}

	public String getOcmpCode() {
		return ocmpCode;
	}

	public void setOcmpCode(String ocmpCode) {
		this.ocmpCode = ocmpCode;
	}
	
	public String getOcmpName() {
		return ocmpName;
	}

	public void setOcmpName(String ocmpName) {
		this.ocmpName = ocmpName;
	}

	public void save()
	{
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			pm.makePersistent(this);
			Util.removeAppCacheValue(AppConfig.CACHE_ORG_STRUCTURE_DATA);
		}
		finally {
			pm.close();
		}
	}
	
	public static OrgCompany fetchCompanyById(Key key) {
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			OrgCompany c = pm.getObjectById(OrgCompany.class, key);
			return pm.detachCopy(c);
		} catch (javax.jdo.JDOObjectNotFoundException e ) {
			//log.info("Could not fetch object", e);
		}
		return null;
	}
	
	public static OrgCompany fetchCompanyByCode(String companyCode,
			boolean fetchCircles) {
    	PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			OrgCompany c = pm.getObjectById(OrgCompany.class, companyCode);
			if (fetchCircles) c.getOcmpCircles();
			return pm.detachCopy(c);
		} catch (javax.jdo.JDOObjectNotFoundException e ) {
			//log.info("Could not fetch object", e);
		}
		return null;
	}
	
	public static OrgCompany fetchOrgCompany() {
		/*Object o = Util.getAppCacheValue(AppConfig.CACHE_REGULATIONS);
		if (o != null) return (List<AcademicRegulation>)o;*/
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		StringBuffer q = new StringBuffer();
		q.append("select from ").append(OrgCompany.class.getName());
		Query query = pm.newQuery(q.toString());
		pm.getFetchPlan().setMaxFetchDepth(10);
		pm.getFetchPlan().addGroup("orgFg");
		try {
			List<OrgCompany> results = 
				(List<OrgCompany>) query.execute();
			if (results.isEmpty()) return null;
			results = (List<OrgCompany>) pm.detachCopyAll(results);
			return results.get(0);
		} finally {
			query.closeAll();
		}
	}
	
	public static OrgCompany fetchCompanyByIdAndFetchAllChildren(String companyId) {
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		pm.getFetchPlan().setMaxFetchDepth(10);
		pm.getFetchPlan().addGroup("orgFg");
		try {
			OrgCompany result = 
				pm.getObjectById(OrgCompany.class, 
						KeyFactory.stringToKey(companyId));
			
			if (result == null) return null;
			return pm.detachCopy(result);
		} finally {
			
		}
	}

	@Override
	public String toString() {
		return "AcademicRegulation [id=" + id + ", ocmpCode=" + ocmpCode
				+ ", ocmpName=" + ocmpName + "]";
	}

	public OrgCircle findCircle(Key courseBranchId) {
		if (this.getOcmpCircles() == null) return null;
		String strBranchId = KeyFactory.keyToString(courseBranchId);
		for (OrgCircle circle : this.getOcmpCircles()) {
			if (circle.getId().equals(strBranchId)) return circle;
		}
		return null;
	}
	
	public OrgCircle findCircle(String circleCode) {
		if (this.getOcmpCircles() == null) return null;
		for (OrgCircle circle : this.getOcmpCircles()) {
			if (circle.getOcrcCode().equals(circleCode)) return circle;
		}
		return null;
	}

	// Cache backed data fetch
	public static OrgCompanyUi cFetchOrgCompany() {
		Object o = Util.getAppCacheValue(AppConfig.CACHE_ORG_STRUCTURE_DATA);
		if (o != null) return (OrgCompanyUi)o;
		
		OrgCompany orgCompany = 
			OrgCompany.fetchOrgCompany();
		if (orgCompany == null) return null;
		OrgCompanyUi orgCompanyUi = new OrgCompanyUi();
		
		DBUtils.copyProperties(orgCompany, orgCompanyUi);
		for (OrgCircle orgCircle : orgCompany.getOcmpCircles()) {
			OrgCircleUi orgCircleUi = new OrgCircleUi();
			DBUtils.copyProperties(orgCircle, orgCircleUi);
			orgCircleUi.setOcmpCompany(orgCompanyUi);
			orgCompanyUi.addCircle(orgCircleUi);
			for (OrgDivision orgDivision : orgCircle.getOrgDivisions()) {
				OrgDivisionUi orgDivisionUi = new OrgDivisionUi();
				DBUtils.copyProperties(orgDivision, orgDivisionUi);
				orgDivisionUi.setOdvsCircle(orgCircleUi);
				orgCircleUi.addDivision(orgDivisionUi);
				for(OrgEro orgEro : orgDivision.getOrgEros()) {
					OrgEroUi orgEroUi = new OrgEroUi();
					DBUtils.copyProperties(orgEro, orgEroUi);
					orgEroUi.setOeroDivision(orgDivisionUi);
					orgDivisionUi.addOrgEro(orgEroUi);
					for (OrgSubdivision orgSubdivision : orgEro.getOrgSubdivisions()) {
						OrgSubdivisionUi orgSubdivisionUi = new OrgSubdivisionUi();
						DBUtils.copyProperties(orgSubdivision, orgSubdivisionUi);
						orgSubdivisionUi.setOsbdEro(orgEroUi);
						orgEroUi.addOrgSubdivisions(orgSubdivisionUi);
						for (OrgSection orgSection : orgSubdivision.getOrgSections()) {
							OrgSectionUi orgSectionUi = new OrgSectionUi();
							DBUtils.copyProperties(orgSection, orgSectionUi);
							orgSectionUi.setOsctSubdivision(orgSubdivisionUi);
							orgSubdivisionUi.addOrgSection(orgSectionUi);
							for (OrgDistribution orgDistribution : orgSection.getOrgDistributions()) {
								OrgDistributionUi orgDistributionUi = new OrgDistributionUi();
								DBUtils.copyProperties(orgDistribution, orgDistributionUi);
								orgDistributionUi.setOdstSection(orgSectionUi);
								orgSectionUi.addOrgDistribution(orgDistributionUi);
							}
						}
					}
				}
			}
		}
		Util.putAppCacheValue(AppConfig.CACHE_ORG_STRUCTURE_DATA,orgCompanyUi);
		return orgCompanyUi;
	}

	public static void delete(String entityId) {
		Key key = KeyFactory.stringToKey(entityId);
		OrgCompany r = OrgCompany.fetchCompanyById(key);
		if (r != null) {
			PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
			pm.deletePersistent(r);
			Util.removeAppCacheValue(AppConfig.CACHE_ORG_STRUCTURE_DATA);
		}
	}
}
