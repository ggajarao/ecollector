package ebilly.admin.server.db;

import java.io.Serializable;

import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import ebilly.admin.shared.AppConfig;

@PersistenceCapable(identityType = IdentityType.APPLICATION, detachable="true")
public class Filter implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
	private String id;
	
	@Persistent private String fieldName;
	@Persistent private String value;
	@Persistent private String operator;
	@Persistent private String conjunction;
	
	@Persistent private ExportRequest exportRequest;
	
	public Filter() {
		
	}
	
	public Filter(String fieldName, String value) {
		this.fieldName = fieldName;
		this.value = value;
	}

	public Filter(String fieldName, String value, String operator, String conjunction) {
		this.fieldName = fieldName;
		this.value = value;
		this.operator = operator;
		this.conjunction = conjunction;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getConjunction() {
		return conjunction;
	}

	public void setConjunction(String conjunction) {
		this.conjunction = conjunction;
	}

	public ExportRequest getExportRequest() {
		return exportRequest;
	}

	public void setExportRequest(ExportRequest exportRequest) {
		this.exportRequest = exportRequest;
	}

	@Override
	public String toString() {
		return "Filter [fieldName=" + fieldName + ", value=" + value
				+ ", operator=" + operator + ", conjunction=" + conjunction
				+ "]";
	}

	public Object asReadableString() {
		String readableOperator = "";
		if (AppConfig.FILTER_OPERATOR_EQUALS.equalsIgnoreCase(operator)) {
			readableOperator = "is";
		} else if (AppConfig.FILTER_OPERATOR_GREATER_EQUALS.equalsIgnoreCase(operator)) {
			readableOperator = "greater than"; 
		} else if (AppConfig.FILTER_OPERATOR_LESS_EQUALS.equalsIgnoreCase(operator)) {
			readableOperator = "less than"; 
		}
		return fieldName + " <i>" + readableOperator + "</i> <b>" + value+"</b>";
	}
}

