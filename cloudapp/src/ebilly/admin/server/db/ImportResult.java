package ebilly.admin.server.db;

import java.util.Date;

import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import ebilly.admin.server.core.AppLogger;
import ebilly.admin.server.core.db.BaseEntityNonPersistent;

@PersistenceCapable(identityType = IdentityType.APPLICATION, detachable="true")
public class ImportResult extends BaseEntityNonPersistent {

	private static final long serialVersionUID = 1L;

	private static final AppLogger log = AppLogger.getLogger(ImportRequest.class.getName());
	
	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
	private String id;
	
	@Persistent
	private Date createdDate;
	
	@Persistent
	private Date lastModifiedDate;
	
	@Persistent
	private int active = 1;
	
	@Persistent
	private int changeNumber;
	
	@Persistent
	private String importRequestId;
	
	@Persistent
	private String errorMessage;
	
	@Persistent
	private String header;
	
	@Persistent
	private String failureRecordsFile;
	
	public ImportResult() {
		
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public int getChangeNumber() {
		return changeNumber;
	}

	public void setChangeNumber(int changeNumber) {
		this.changeNumber = changeNumber;
	}
	
	public String getId() {
		return id;
	}
	
	@Override
	public void setEntityId(String id) {
		this.id = id;
	}

	@Override
	protected Object getUniqueId() {
		throw new IllegalArgumentException("Unsupported UniqueId requested");
	}

	public String getImportRequestId() {
		return importRequestId;
	}

	public void setImportRequestId(String importRequestId) {
		this.importRequestId = importRequestId;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getFailureRecordsFile() {
		return failureRecordsFile;
	}

	public void setFailureRecordsFile(String failureRecordsFile) {
		this.failureRecordsFile = failureRecordsFile;
	}
}
