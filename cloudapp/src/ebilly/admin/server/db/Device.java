package ebilly.admin.server.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.FetchGroup;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.KeyFactory;

import ebilly.admin.server.Util;
import ebilly.admin.server.bo.DeviceBo;
import ebilly.admin.server.core.AppLogger;
import ebilly.admin.server.core.db.BaseEntityNonPersistent;
import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.CommonUtil;

@PersistenceCapable(identityType = IdentityType.APPLICATION, detachable="true")
@FetchGroup(name = "deviceFg", members={@Persistent(name="deviceFiles")})
public class Device extends BaseEntityNonPersistent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final AppLogger log = AppLogger.getLogger(ImportRequest.class.getName());

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	@Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
	private String id;

	@Persistent
	private Date createdDate;

	@Persistent
	private Date lastModifiedDate;

	@Persistent
	private int active = 1;

	@Persistent
	private int changeNumber;

	@Persistent
	private String pairingStatus = "";

	@Persistent
	private String deviceIdentifier = "";

	@Persistent
	@Extension(vendorName="datanucleus", key="gae.pk-name", value="true")
	private String deviceName;

	@Persistent
	private String deviceNameSearchable;
	
	@Persistent
	private List<DeviceFile> deviceFiles;
	
	@Persistent
	private Date collectionExpirationDate;
	
	@Persistent
	private int collectionLimit;
	
	public Device() {

	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public int getChangeNumber() {
		return changeNumber;
	}

	public void setChangeNumber(int changeNumber) {
		this.changeNumber = changeNumber;
	}

	public String getId() {
		return id;
	}

	@Override
	public void setEntityId(String id) {
		this.id = id;
	}

	@Override
	protected Object getUniqueId() {
		throw new IllegalArgumentException("Unsupported UniqueId requested");
	}

	public String getDeviceIdentifier() {
		return deviceIdentifier;
	}

	public void setDeviceIdentifier(String deviceIdentifier) {
		this.deviceIdentifier = deviceIdentifier;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName.toUpperCase();
		this.deviceNameSearchable = this.deviceName.toUpperCase();
	}

	public Date getCollectionExpirationDate() {
		return collectionExpirationDate;
	}

	public void setCollectionExpirationDate(Date collectionExpirationDate) {
		this.collectionExpirationDate = collectionExpirationDate;
	}

	public int getCollectionLimit() {
		return collectionLimit;
	}

	public void setCollectionLimit(int collectionLimit) {
		this.collectionLimit = collectionLimit;
	}

	public void updateToPairedStatus() {
		this.pairingStatus = "PAIRED";
	}

	public void updateToWaitingStatus() {
		this.pairingStatus = "WAITING";
	}

	public void updateToUnpairedStatus() {
		this.pairingStatus = "UNPAIRED";
	}
	
	public void resetPairing() {
		this.pairingStatus = "";
		this.deviceIdentifier = "";
	}
	
	public List<DeviceFile> getDeviceFiles() {
		if (this.deviceFiles == null) {
			this.deviceFiles = new ArrayList<DeviceFile>();
		}
		return this.deviceFiles;
	}
	
	public void addDeviceFile(DeviceFile deviceFile) {
		this.getDeviceFiles().add(deviceFile);
	}
	
	@Override
	protected void onAfterSave() {
		super.onAfterSave();
		Util.putAppCacheValue(
				AppConfig.CACHE_DEVICE_BY_DEVICE_NAME+
				this.deviceName.toUpperCase(), this);
		Util.putAppCacheValue(
				AppConfig.CACHE_DEVICE_BY_ID+
				this.getId(), this);
	}

	/**** utility methods ********/
	public static Device fetchDeviceById(String id) {
		if (CommonUtil.isEmpty(id)) {
			log.info("fetchDeviceById got null id");
			return null;
		}
		
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			Device c =
				pm.getObjectById(Device.class, 
						KeyFactory.stringToKey(id));
			c.getDeviceFiles();
			return pm.detachCopy(c);
		} catch (javax.jdo.JDOObjectNotFoundException e ) {
			log.info("Could not fetch object Device with Id: "+id, e);
		}
		return null;
	}
	
	public static Device cFetchDeviceById(String id) {
		if (CommonUtil.isEmpty(id)) {
			log.info("fetchDeviceById got null id");
			return null;
		}
		
		Device c = (Device)Util.getAppCacheValue(AppConfig.CACHE_DEVICE_BY_ID+id);
		if (c != null) {
			return c;
		}
		
		c = Device.fetchDeviceById(id);
		if (c != null) {
			Util.putAppCacheValue(AppConfig.CACHE_DEVICE_BY_ID+c.getId(), 
					c);
		}
		return c;
	}

	private static Device fetchDeviceByName(String code)
	{
		if (code == null) return null;
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			Device r = pm.getObjectById(Device.class, code.toUpperCase());
			r.getDeviceFiles();
			return pm.detachCopy(r);
		} catch (javax.jdo.JDOObjectNotFoundException e ) {
			log.info("Could not fetch object", e);
		}
		return null;
	}
	
	public static Device cFetchDeviceByName(String code)
	{
		if (code == null || code.trim().length() == 0) return null;
		Device device = (Device) Util.getAppCacheValue(
				AppConfig.CACHE_DEVICE_BY_DEVICE_NAME+code.toUpperCase());
		if (device != null) return device;
		
		device = Device.fetchDeviceByName(code);
		if (device != null) {
			Util.putAppCacheValue(
					AppConfig.CACHE_DEVICE_BY_DEVICE_NAME+code.toUpperCase(), device);
		}
		return device;
	}
	
	public static Device fetchDeviceByLoginId(String loginId) {
		if (loginId == null || loginId.trim().length() == 0) {
			return null;
		}
		String deviceName = DeviceBo.fetchDeviceNameFromUserId(loginId);
		return cFetchDeviceByName(deviceName);
	}
	
	public boolean isPaired() {
		return this.pairingStatus.equals("PAIRED");
	}

	public boolean isWaiting() {
		return this.pairingStatus.equals("WAITING");
	}

	public String getPairingStatus() {
		return pairingStatus;
	}
	
	public static Device fetchDeviceByIdentifier(String deviceIdentifier) {
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		Query query = pm.newQuery(
				"select from " + Device.class.getName() +
				" where active == 1 && deviceIdentifier == :pDeviceIdentifier ");
		try {
			List<Device> results = 
				(List<Device>)query.execute(deviceIdentifier);
			if (results.size() != 0) {
				results.get(0).getDeviceFiles();
				results = (List<Device>)pm.detachCopyAll(results);
				return results.get(0);
			} else {
				return null;
			}
		} finally {
			query.closeAll();
		}
	}

	public void delete() {
		Device d = Device.fetchDeviceById(this.getId());
		if (d != null) {
			PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
			pm.deletePersistent(d);
			clearCache(d.getId(),d.getDeviceName());
		}
	}

	public static void delete(String deviceId) {
		Device r = Device.fetchDeviceById(deviceId);
		if (r != null) {
			String deviceName = r.getDeviceName();
			log.info("Deleting device: "+deviceName);
			PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
			pm.deletePersistent(r);
			clearCache(deviceId,deviceName);
		}
	}

	private static void clearCache(String deviceId, String deviceName) {
		Util.removeAppCacheValue(AppConfig.CACHE_DEVICE_BY_ID+deviceId);
		if (deviceName != null) {
			Util.removeAppCacheValue(AppConfig.CACHE_DEVICE_BY_DEVICE_NAME+deviceName.toUpperCase());
		}
	}
}
