package ebilly.admin.server.db;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.KeyFactory;
import com.google.cloud.sql.jdbc.internal.Util;

import ebilly.admin.server.core.AppLogger;
import ebilly.admin.server.core.db.BaseEntityNonPersistent;
import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.CommonUtil;

@PersistenceCapable(identityType = IdentityType.APPLICATION, detachable="true")
public class PrSummaryData extends BaseEntityNonPersistent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final AppLogger log = AppLogger.getLogger(PrSummaryData.class.getName());
	
	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
	private String id;
	
	@Persistent
	private Date createdDate;
	
	@Persistent
	private Date lastModifiedDate;
	
	@Persistent
	private int active = 1;
	
	@Persistent
	private int changeNumber;
	
	@Persistent
	private String month;
	
	@Persistent
	private String year;
	
	@Persistent
	private String dataFileKey;
	
	public PrSummaryData() {
		
	}
	
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public int getChangeNumber() {
		return changeNumber;
	}

	public void setChangeNumber(int changeNumber) {
		this.changeNumber = changeNumber;
	}
	
	public String getId() {
		return id;
	}
	
	@Override
	public void setEntityId(String id) {
		this.id = id;
	}

	@Override
	protected Object getUniqueId() {
		throw new IllegalArgumentException("Unsupported UniqueId requested");
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getDataFileKey() {
		return dataFileKey;
	}

	public void setDataFileKey(String dataFileKey) {
		this.dataFileKey = dataFileKey;
	}
	
	public static PrSummaryData fetchPrSummaryDataById(String id) {
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			PrSummaryData c =
				pm.getObjectById(PrSummaryData.class, 
						KeyFactory.stringToKey(id));
			return pm.detachCopy(c);
		} catch (javax.jdo.JDOObjectNotFoundException e ) {
			log.info("Could not fetch object PrSummaryData with Id: "+id, e);
		}
		return null;
	}
	
	public static String createMonthKey(Calendar cal) {
		return cal.get(Calendar.YEAR) + "-" + (1+cal.get(Calendar.MONTH));
	}
	
	public static String createYearKey(Calendar cal) {
		return cal.get(Calendar.YEAR)+"";
	}
	
	/**
	 * summaryMonth in the format YYYY-M eg: 2014-3
	 * 
	 * @param summaryMonth
	 * @return
	 */
	public static PrSummaryData createOrFetchPrSummaryByMonth(String summaryMonth) {
		if (summaryMonth == null || Util.isEmpty(summaryMonth)) {
			return null;
		}
		PrSummaryData monthsPrSummary = 
			PrSummaryData.fetchPrSummaryByMonth(summaryMonth);
		if (monthsPrSummary != null) {
			return monthsPrSummary;
		} else {
			return createPrSummaryForMonth(summaryMonth);
		}
	}
	
	public static PrSummaryData createOrFetchPrSummaryByYear(String summaryYear) {
		if (summaryYear == null || Util.isEmpty(summaryYear)) {
			return null;
		}
		PrSummaryData monthsPrSummary = 
			PrSummaryData.fetchPrSummaryByYear(summaryYear);
		if (monthsPrSummary != null) {
			return monthsPrSummary;
		} else {
			return createPrSummaryForYear(summaryYear);
		}
	}

	private static PrSummaryData createPrSummaryForYear(String summaryYear) {
		if (summaryYear == null || Util.isEmpty(summaryYear)) {
			return null;
		}
		PrSummaryData psd = new PrSummaryData();
		psd.setYear(summaryYear.trim());
		psd.save();
		return psd;
	}

	private static PrSummaryData createPrSummaryForMonth(String summaryMonth) {
		if (summaryMonth == null || Util.isEmpty(summaryMonth)) {
			return null;
		}
		PrSummaryData psd = new PrSummaryData();
		psd.setMonth(summaryMonth.trim());
		psd.save();
		return psd;
	}
	
	private static PrSummaryData fetchPrSummaryByYear(String summaryYear) {
		if (summaryYear == null || Util.isEmpty(summaryYear)) {
			return null;
		}
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		Query query = pm.newQuery(
				"select from " + PrSummaryData.class.getName() +
		" where active == 1 && year == :pyear ");
		try {
			List<PrSummaryData> results = 
				(List<PrSummaryData>)query.execute(summaryYear);
			List<PrSummaryData> prSummaryDataList = 
				(List<PrSummaryData>)pm.detachCopyAll(results);
			if (prSummaryDataList != null && !prSummaryDataList.isEmpty()) {
				if (prSummaryDataList.size() == 1) {
					return prSummaryDataList.get(0);
				} else {
					throw new IllegalArgumentException(
						"Multiple records found with year: "
						+summaryYear+", count: "+prSummaryDataList.size());
				}
			} else {
				return null;
			}
		} finally {
			query.closeAll();
		}
	}

	public static PrSummaryData fetchPrSummaryByMonth(String summaryMonth) {
		if (summaryMonth == null || Util.isEmpty(summaryMonth)) {
			return null;
		}
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		Query query = pm.newQuery(
				"select from " + PrSummaryData.class.getName() +
		" where active == 1 && month == :pmonth ");
		try {
			List<PrSummaryData> results = 
				(List<PrSummaryData>)query.execute(summaryMonth);
			List<PrSummaryData> prSummaryDataList = 
				(List<PrSummaryData>)pm.detachCopyAll(results);
			if (prSummaryDataList != null && !prSummaryDataList.isEmpty()) {
				if (prSummaryDataList.size() == 1) {
					return prSummaryDataList.get(0);
				} else {
					throw new IllegalArgumentException(
						"Multiple records found with month: "
						+summaryMonth+", count: "+prSummaryDataList.size());
				}
			} else {
				return null;
			}
		} finally {
			query.closeAll();
		}
	}
	
	/**
	 * Returns 26 Recent summary records in descending
	 * order of Month/Year.
	 * 
	 * @return List<PrSummaryData>
	 */
	public static List<PrSummaryData> fetchRecentSummaryRecords() {
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		Query query = pm.newQuery(
				"select from " + PrSummaryData.class.getName() +
				" where active == 1 ");
		query.setRange(0, 26);
		try {
			List<PrSummaryData> results = 
				(List<PrSummaryData>)query.execute();
			List<PrSummaryData> prSummaryDataList = 
				(List<PrSummaryData>)pm.detachCopyAll(results);
			if (prSummaryDataList != null && !prSummaryDataList.isEmpty()) {
				return prSummaryDataList;
			} else {
				return Collections.emptyList();
			}
		} finally {
			query.closeAll();
		}
	}

	public PrSummaryVersionTrack createNewSummaryVersion(String oldVersionNumber,
			String newVersionNumber) {
		PrSummaryVersionTrack versionTrack = new PrSummaryVersionTrack();
		versionTrack.setNewVersionNumber(newVersionNumber);
		versionTrack.setOldVersionNumber(oldVersionNumber);
		versionTrack.setOldVersionDataFileKey(this.getDataFileKey());
		versionTrack.setPrSummaryDataId(this.getId());
		versionTrack.save();
		return versionTrack;
	}

	public boolean isForMonth() {
		if (CommonUtil.isEmpty(this.month) && CommonUtil.isEmpty(this.year)) {
			throw new IllegalStateException("Month and Year are empty !"); 
		}
		return this.month != null;
	}
	
	
	@Override
	protected void onAfterSave() {
		if (this.isNewlyInserted) {
			ebilly.admin.server.Util.putAppCacheValue(
					AppConfig.CACHE_SUMMARY_DATA_KEY_LIST, null);
		}
	}
	
	transient boolean isNewlyInserted = false; 
	@Override
	protected void onBeforeSave() {
		if (this.getId() == null) {
			this.isNewlyInserted = true;
		}
	}
}