package ebilly.admin.server.bo;

import java.util.ArrayList;
import java.util.List;

import ebilly.admin.server.Util;
import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.server.db.Role;
import ebilly.admin.server.db.User;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.RoleUi;
import ebilly.admin.shared.UserUi;
import ebilly.admin.shared.viewdef.CrudResponse;

/**
 *  User creation, notifying user through email to change password,
 *  User updates etc.
 *
 */
public class UserBo {
	
	public static RoleUi cFindRolebyId(String roleId) {
		RoleUi[] roles = UserBo.cFetchAllRoles();
		for (RoleUi role : roles) {
			if (role.getId().equals(roleId)) {
				return role;
			}
		}
		return null;
	}
	
	public static RoleUi[] cFetchAdminRoles() {
		RoleUi[] roles = UserBo.cFetchAllRoles();
		List<RoleUi> uiRoles = new ArrayList<RoleUi>();
		for (RoleUi role : roles) {
			if (role.getRoleCode().equals(AppConfig.ROLE_CODE_APP_ADMIN)) {
				continue;
			}
			uiRoles.add(role);
		}
		return uiRoles.toArray(new RoleUi[]{});
	}
	
	public static RoleUi[] cFetchAllRoles() {
		RoleUi[] cachedRoles = (RoleUi[])
			Util.getAppCacheValue(AppConfig.CACHE_ROLES);
		if (cachedRoles != null) return cachedRoles;
		
		List<Role> roles = Role.fetchAllRoles();
		List<RoleUi> uiRoles = new ArrayList<RoleUi>();
		for (Role role : roles) {
			RoleUi uiRole = new RoleUi();
			DBUtils.copyProperties(role, uiRole);
			uiRoles.add(uiRole);
		}
		cachedRoles = uiRoles.toArray(new RoleUi[]{});
		Util.putAppCacheValue(AppConfig.CACHE_ROLES, cachedRoles);
		return cachedRoles;
	}
	
	public static CrudResponse deleteUser(String userId) {
		CrudResponse r = new CrudResponse();
		User u = User.fetchUserById(userId);
		if (u == null) {
			r.errorMessage = "User not found to delete";
			return r;
		}
		u.setActive(0);
		u.save();
		return r;
	}
	
	public static UserUi fetchUser(String userId) {
		if (CommonUtil.isEmpty(userId)) {
			return null;
		}
		UserUi u = new UserUi();
		User user = User.fetchUserById(userId);
		if (user == null) return null;
		DBUtils.copyProperties(user, u);
		return u;
	}

	public static CrudResponse resetUserPassword(String userId,
			String newPassword) {
		CrudResponse r = new CrudResponse();
		if (CommonUtil.isEmpty(userId) || CommonUtil.isEmpty(newPassword)) {
			r.errorMessage = "Invalid request";
			return r;
		}
		
		User u = User.fetchUserById(userId);
		if (u == null) {
			r.errorMessage = "User not found";
			return r;
		}
		u.setPassword(newPassword);
		u.save();
		r.successMessage = "Successfully reset User password";
		return r;
	}
	
	public static CrudResponse sendPasswordResetEmail(String userId, String emailAddress) {
		CrudResponse r = new CrudResponse();
		if (CommonUtil.isEmpty(userId) || CommonUtil.isEmpty(emailAddress)) {
			r.errorMessage = "Invalid request";
			return r;
		}
		
		/*if (!CommonUtil.isValidEmail(emailAddress)) {
			//r.errorMessage = "Invalid email address: "+emailAddress;
			r.errorMessage = emailAddress + " - is invalid. " + AppConfig.MESSAGE_INVALID_EMAIL;
			return r;
		}*/
		
		User u = User.fetchUserById(userId);
		if (u == null) {
			r.errorMessage = "User not found";
			return r;
		}
		u.setEmail(emailAddress);
		u.setPassword(Util.generateRandomPassword());
		u.save();
		
		String emailMessage = Util.buildPasswordChangeEmailMessage(u);
		try {
			Util.createEmailSendRequest(u.getEmail(),"Change Notification", emailMessage);
		} catch (Throwable t) {
			r.errorMessage = "Failed to send email to "+emailAddress+
			   ". Reason: "+t.getMessage();
			return r;
		}
		
		r.successMessage = "Password Reset Email sent to " + emailAddress;
		return r;
	}

	public static User createNewUser(String userLoginId,
			String pwd, String role) {
		
		User u = User.fetchUserByLoginId(userLoginId);
		if (u != null) {
			throw new IllegalArgumentException("User "+userLoginId+" already exists");
		}
		
		Role r = Role.fetchRoleByCode(role);
		if (r == null) {
			throw new IllegalArgumentException("Invalid role: "+role);
		}
		
		u = new User();
		u.setLoginId(userLoginId);
		u.setPassword(pwd);
		u.setRoleId(r.getId());
		u.save();
		return u;
	}
}
