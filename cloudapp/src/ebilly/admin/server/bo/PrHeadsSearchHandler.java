package ebilly.admin.server.bo;

import java.io.OutputStream;
import java.nio.channels.Channels;
import java.util.List;

import com.google.appengine.api.files.FileWriteChannel;

import ebilly.admin.server.db.ExportRequest;

public class PrHeadsSearchHandler extends PrSearchHandler {
	
	public PrHeadsSearchHandler() {
		
	}
	
	/**
	 * Write results into writeChannel.
	 * 
	 * If results are empty, then writeChannel will be null.
	 * 
	 * @param results
	 * @param writeChannel
	 * @param fileName
	 * @param request
	 * @throws Exception
	 */
	@Override
	protected void writeResults(Object results, FileWriteChannel writeChannel, 
			String fileName, ExportRequest request) 
	throws Exception {
		List<ebilly.admin.server.db.DevicePr> devicePrs = 
			(List<ebilly.admin.server.db.DevicePr>) results;
		request.setSummaryMessage(devicePrs.size()+" PRs");
		if (devicePrs.size() > 0 && writeChannel != null) {
			OutputStream os = Channels.newOutputStream(writeChannel);
			//DeviceBo.exportDevicePrs(devicePrs, os, fileName);
			PrBo.exportPrHeads(super.mDeviceName, super.mFromDate, super.mToDate,
					devicePrs,os,fileName);
		}
	}
	
	@Override
	protected String getExportFileContentType() {
		return "text/plain";
	}
	
	@Override
	protected String getExportFileName(ExportRequest exportRequest) {
		return exportRequest.prepareFileName() + ".txt";
	}
}
