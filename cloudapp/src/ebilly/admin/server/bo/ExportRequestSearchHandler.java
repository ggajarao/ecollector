package ebilly.admin.server.bo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.jdo.Query;

import ebilly.admin.server.Util;
import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.shared.viewdef.ResultField;
import ebilly.admin.shared.viewdef.ResultRecord;
import ebilly.admin.shared.viewdef.SearchRequest;
import ebilly.admin.shared.viewdef.SearchResult;

public class ExportRequestSearchHandler extends BaseSearchHandler {
	
	public ExportRequestSearchHandler() {
		
	}

	protected Map<String, Class> getEntityFieldDataMap() {
		return DBUtils.fieldDataTypeMap(ebilly.admin.server.db.ExportRequest.class);
	}

	/**
	 * 
	 * @return comma seperated list of entity names
	 */
	protected String getSearchEntityName() {
		return ebilly.admin.server.db.ExportRequest.class.getName();
	}
	
	protected void setOrderClause(Query query, SearchRequest request) {
		query.setOrdering(" lastModifiedDate descending ");
	}

	@Override
	protected SearchResult prepareResults(SearchRequest request, Object results) {
		List<ebilly.admin.server.db.ExportRequest> qResults = 
			(List<ebilly.admin.server.db.ExportRequest>) results;
		Map<String, Class> fieldDataTypeMap = 
			DBUtils.fieldDataTypeMap(ebilly.admin.server.db.ExportRequest.class);
		List<ResultRecord> records = new ArrayList<ResultRecord>();
		for (ebilly.admin.server.db.ExportRequest exportRequest : qResults) {
			ResultRecord record = new ResultRecord();
			Map<String, String> fieldValueMap = 
				DBUtils.toFieldStringValueMap(exportRequest, fieldDataTypeMap);
			
			boolean isImportRequestInQueue = false;
			String importRequestId = null;
			for (ResultField resultColumn : request.getResultFields()) {
				String value = "";
				value = fieldValueMap.get(resultColumn.getFieldName()) + "";
				if (resultColumn.getFieldName().equalsIgnoreCase("filters")) {
					value = exportRequest.filtersAsReadableString();
				}
				record.fieldValues.add(value);
			}
			
			String resultFileKey = request.getValue("resultFileKey", record);
			String fileName = Util.getFileNameFromBlobKey(resultFileKey);
			if (fileName == null) fileName = "";
			request.setValue("fileName", fileName, record);
			
			//record.fieldValues.add(isImportRequestInQueue+"");
			records.add(record);
		}
		return new SearchResult(records);
	}
}
