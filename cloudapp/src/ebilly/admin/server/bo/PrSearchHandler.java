package ebilly.admin.server.bo;

import java.io.OutputStream;
import java.nio.channels.Channels;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.jdo.Query;

import com.google.appengine.api.files.FileWriteChannel;

import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.server.db.ExportRequest;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.viewdef.Filter;
import ebilly.admin.shared.viewdef.FilterCriteria;
import ebilly.admin.shared.viewdef.ResultField;
import ebilly.admin.shared.viewdef.ResultRecord;
import ebilly.admin.shared.viewdef.SearchRequest;
import ebilly.admin.shared.viewdef.SearchResult;

public class PrSearchHandler extends BaseSearchHandler {
	
	protected String mFromDate;
	protected String mToDate;
	protected String mDeviceName;
	
	public PrSearchHandler() {
		
	}

	protected Map<String, Class> getEntityFieldDataMap() {
		return DBUtils.fieldDataTypeMap(ebilly.admin.server.db.DevicePr.class);
	}
	
	@Override
	protected void preProcessRequest(SearchRequest request) {
		super.preProcessRequest(request);
		Filter filter = request.getFilterCriteria().get("deviceName");
		if (filter != null && filter.getValue() != null) {
			filter.setValue(filter.getValue().trim().toUpperCase());
		}
		
		FilterCriteria filterCriteria = request.getFilterCriteria();
		for (Filter f : filterCriteria.getFilters()) {
			if (f.getFieldName().equals("_collectionDate") 
				&& f.getOperator().equals(AppConfig.FILTER_OPERATOR_GREATER_EQUALS)) {
				this.mFromDate = f.getValue();
			} else if (f.getFieldName().equals("_collectionDate") 
					   && f.getOperator().equals(AppConfig.FILTER_OPERATOR_LESS_EQUALS)) {
				this.mToDate = f.getValue();
			} else if (f.getFieldName().equals("deviceName")) {
				this.mDeviceName = f.getValue();
			}
		}
	}
	
	/**
	 * 
	 * @return comma seperated list of entity names
	 */
	protected String getSearchEntityName() {
		return ebilly.admin.server.db.DevicePr.class.getName();
	}
	
	protected void setOrderClause(Query query, SearchRequest request) {
		query.setOrdering(" _collectionDate descending ");
	}

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected SearchResult prepareResults(SearchRequest request, Object results) {
		List<ebilly.admin.server.db.DevicePr> qResults = 
			(List<ebilly.admin.server.db.DevicePr>) results;
		Map<String, Class> fieldDataTypeMap = 
			DBUtils.fieldDataTypeMap(ebilly.admin.server.db.DevicePr.class);
		List<ResultRecord> records = new ArrayList<ResultRecord>();
		for (ebilly.admin.server.db.DevicePr serviceStage : qResults) {
			ResultRecord record = new ResultRecord();
			Map<String, String> fieldValueMap = 
				DBUtils.toFieldStringValueMap(serviceStage, fieldDataTypeMap);
			
			for (ResultField resultColumn : request.getResultFields()) {
				String value = "";
				value = fieldValueMap.get(resultColumn.getFieldName()) + "";
				/*if (resultColumn.getFieldName().equalsIgnoreCase("status")) {
					//
				}*/ 
				record.fieldValues.add(value);
			}
			records.add(record);
		}
		return new SearchResult(records);
	}
	
	/**
	 * Write results into writeChannel.
	 * 
	 * If results are empty, then writeChannel will be null.
	 * 
	 * @param results
	 * @param writeChannel
	 * @param fileName
	 * @param request
	 * @throws Exception
	 */
	@Override
	protected void writeResults(Object results, FileWriteChannel writeChannel, 
			String fileName, ExportRequest request) 
	throws Exception {
		List<ebilly.admin.server.db.DevicePr> devicePrs = 
			(List<ebilly.admin.server.db.DevicePr>) results;
		request.setSummaryMessage(devicePrs.size()+" PRs");
		if (devicePrs.size() > 0 && writeChannel != null) {
			OutputStream os = Channels.newOutputStream(writeChannel);
			DeviceBo.exportDevicePrs(devicePrs, os, fileName);
		}
	}
	
	@Override
	protected String getExportFileContentType() {
		// TODO Auto-generated method stub
		return "application/zip";
	}
	
	@Override
	protected String getExportFileName(ExportRequest exportRequest) {
		return exportRequest.prepareFileName() + ".zip";
	}
}
