package ebilly.admin.server.bo;

import java.util.Calendar;

import ebilly.admin.server.Util;
import ebilly.admin.shared.viewdef.SearchRequest;

public class LivePrSearchHandler extends PrSearchHandler {
	
	public LivePrSearchHandler() {
		
	}
	
	@Override
	protected void preProcessRequest(SearchRequest request) {
		super.preProcessRequest(request);
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_MONTH, -1);
		String fromDate = Util.getFormattedDate(c.getTime());
		request.addSearchCriteria("_collectionDate", fromDate, "&&", ">=");
	}
}
