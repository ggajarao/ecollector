package ebilly.admin.server.bo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.jdo.Query;

import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.shared.viewdef.ResultField;
import ebilly.admin.shared.viewdef.ResultRecord;
import ebilly.admin.shared.viewdef.SearchRequest;
import ebilly.admin.shared.viewdef.SearchResult;

public class ImportRequestSearchHandler extends BaseSearchHandler {
	
	public ImportRequestSearchHandler() {
		
	}

	protected Map<String, Class> getEntityFieldDataMap() {
		return DBUtils.fieldDataTypeMap(ebilly.admin.server.db.ImportRequest.class);
	}

	/**
	 * 
	 * @return comma seperated list of entity names
	 */
	protected String getSearchEntityName() {
		return ebilly.admin.server.db.ImportRequest.class.getName();
	}
	
	protected void setOrderClause(Query query, SearchRequest request) {
		query.setOrdering(" lastModifiedDate descending ");
	}

	@Override
	protected SearchResult prepareResults(SearchRequest request, Object results) {
		List<ebilly.admin.server.db.ImportRequest> qResults = 
			(List<ebilly.admin.server.db.ImportRequest>) results;
		Map<String, Class> fieldDataTypeMap = 
			DBUtils.fieldDataTypeMap(ebilly.admin.server.db.ImportRequest.class);
		List<ResultRecord> records = new ArrayList<ResultRecord>();
		for (ebilly.admin.server.db.ImportRequest importRequest : qResults) {
			ResultRecord record = new ResultRecord();
			Map<String, String> fieldValueMap = 
				DBUtils.toFieldStringValueMap(importRequest, fieldDataTypeMap);
			
			boolean isImportRequestInQueue = false;
			String importRequestId = null;
			String status = null;
			for (ResultField resultColumn : request.getResultFields()) {
				String value = "";
				value = fieldValueMap.get(resultColumn.getFieldName()) + "";
				if (resultColumn.getFieldName().equalsIgnoreCase("status")) {
					status = value;
				} else if (resultColumn.getFieldName().equalsIgnoreCase("id")) {
					importRequestId = value;
				} /*else if (resultColumn.getFieldName().equalsIgnoreCase("nextInQueue")) {
					// NOTE: ORDER OF THIS COLUMN SHOULD BE LAST
					// IN VIEW DEFINITION !!
					
					// If this import requests status is new
					// and if it is not in import handler queue,
					// then allow user to manully queue.
					if (IMPORT_STATUS.NEW.toString().equalsIgnoreCase(status)) {
						value = ""+Util.isImportRequestInQueue(importRequestId);
					}
				}*/
				record.fieldValues.add(value);
			}
			
			
			//record.fieldValues.add(isImportRequestInQueue+"");
			records.add(record);
		}
		return new SearchResult(records);
	}
}
