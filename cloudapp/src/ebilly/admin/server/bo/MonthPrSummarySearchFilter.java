package ebilly.admin.server.bo;

import java.util.ArrayList;
import java.util.List;

import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.viewdef.Filter;
import ebilly.admin.shared.viewdef.FilterCriteria;
import ecollector.common.PrColumns;

public class MonthPrSummarySearchFilter {
	
	public static final byte ORG_COMPANY = 0x00;
	public static final byte ORG_CIRCLE = 0x01;
	public static final byte ORG_DIVISION = 0x02;
	public static final byte ORG_ERO = 0x03;
	public static final byte ORG_SUBDIVISION = 0x04;
	public static final byte ORG_SECTION = 0x05;
	public static final byte ORG_DISTRIBUTION = 0x6;
	
	private FilterCriteria mFilterCriteria = null;
	
	private String mSelectedOrg = "";
	
	public MonthPrSummarySearchFilter() {
		mFilterCriteria  = new FilterCriteria();
	}
	
	public MonthPrSummarySearchFilter(FilterCriteria filterCriteria) {
		mFilterCriteria = filterCriteria;
	}

	public boolean isForMonth() {
		return mFilterCriteria.doesFilterExists(AppConfig.F_MONTH_FILTER);
	}

	public String getSummaryMonth() {
		if (mFilterCriteria.doesFilterExists(AppConfig.F_MONTH_FILTER)) {
			return mFilterCriteria.getValue(AppConfig.F_MONTH_FILTER);
		}
		return "";
	}
	
	public List<String> findFiltersForValues(String fieldName) {
		List<String> foundFilters = new ArrayList<String>();
		for (Filter filter : this.mFilterCriteria.getFilters()) {
			if (filter.getFieldName().equals(fieldName)) {
				foundFilters.add(filter.getValue());
			}
		}
		return foundFilters;
	}

	List<String> getCircles() {
		return findFiltersForValues(AppConfig.F_CIRCLE);
	}
	
	List<String> getDivisions() {
		return findFiltersForValues(AppConfig.F_DIVISION);
	}
	
	List<String> getEros() {
		return findFiltersForValues(AppConfig.F_ERO);
	}
	
	List<String> getSubdivisions() {
		return findFiltersForValues(AppConfig.F_SUBDIVISION);
	}
	
	List<String> getSections() {
		return findFiltersForValues(AppConfig.F_SECTION);
	}
	
	List<String> getDistributions() {
		return findFiltersForValues(AppConfig.F_DISTRIBUTION);
	}
	
	public byte getSelectedOrgType() {
		MonthPrSummarySearchFilter summaryFilter = this;
		List<String> fCircles = summaryFilter.getCircles();
		List<String> fDivisions = summaryFilter.getDivisions();
		List<String> fEros = summaryFilter.getEros();
		List<String> fSubdivisions = summaryFilter.getSubdivisions();
		List<String> fSections = summaryFilter.getSections();
		List<String> fDistributions = summaryFilter.getDistributions();
		
		if (fCircles.isEmpty()) {
			mSelectedOrg = "0";
			return ORG_COMPANY;
		}
		if (fDivisions.isEmpty()) {
			mSelectedOrg = fCircles.get(0);
			return ORG_CIRCLE;
		}
		if (fEros.isEmpty()) {
			mSelectedOrg = fDivisions.get(0);
			return ORG_DIVISION;
		}
		if (fSubdivisions.isEmpty()) {
			mSelectedOrg = fEros.get(0);
			return ORG_ERO;
		}
		if (fSections.isEmpty()) {
			mSelectedOrg = fSubdivisions.get(0);
			return ORG_SUBDIVISION;
		}
		if (fDistributions.isEmpty()) {
			mSelectedOrg = fSections.get(0);
			return ORG_SECTION;
		}
		else {
			mSelectedOrg = fDistributions.get(0);
			return ORG_DISTRIBUTION;
		}
	}
	
	public String getSelectedOrg() {
		if (CommonUtil.isEmpty(mSelectedOrg)) {
			throw new IllegalStateException("Determine the Org Type first");
		}
		return mSelectedOrg;
	}
	
	public String asReadableString() {
		StringBuffer sb = new StringBuffer();
		if (this.isForMonth()) {
			sb.append("Month[").append(this.getSummaryMonth()).append("] ");
		} else {
			throw new IllegalArgumentException("implement this log !");
		}
		sb.append("Dimension[").append(this.toReadableOrgType(this.getSelectedOrgType()))
		  .append("=").append(this.getSelectedOrg()).append("] ");
		return sb.toString();
	}

	private Object toReadableOrgType(byte orgType) {
		switch(orgType) {
		case MonthPrSummarySearchFilter.ORG_COMPANY:
			return "Company";
		case MonthPrSummarySearchFilter.ORG_CIRCLE:
			return "Circle";
		case MonthPrSummarySearchFilter.ORG_DIVISION:
			return "Division";
		case MonthPrSummarySearchFilter.ORG_ERO:
			return "Ero";
		case MonthPrSummarySearchFilter.ORG_SUBDIVISION:
			return "Subdivision";
		case MonthPrSummarySearchFilter.ORG_SECTION:
			return "Section";
		case MonthPrSummarySearchFilter.ORG_DISTRIBUTION:
			return "Distribution";
		default:
			return "Unknownw";
	}
	}
}
