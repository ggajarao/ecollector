package ebilly.admin.server.bo;

import ebilly.admin.server.core.AppLogger;

public class TimeLogger {
	
	private static AppLogger log = AppLogger.getLogger("TimeLogger");
	
	private long startMillis = -1;
	public TimeLogger() {
		start();
	}
	
	public static TimeLogger create() {
		return new TimeLogger();
	}
	
	public void start() {
		startMillis = System.currentTimeMillis();
	}
	
	public void logElapsed(String msg) {
		log.info(msg + ((System.currentTimeMillis()-startMillis)/1000.0) + " Secs");
		startMillis = System.currentTimeMillis();
	}
}
