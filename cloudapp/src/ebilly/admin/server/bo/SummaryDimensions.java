package ebilly.admin.server.bo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;

import com.google.gson.JsonObject;

import ebilly.admin.server.core.reports.ResultAggregator;
import ebilly.admin.server.core.reports.ResultAggregatorFactory;
import ebilly.admin.server.core.reports.ResultAggregatorJacksonFactory;
import ebilly.admin.server.db.DevicePr;
import ebilly.admin.shared.viewdef.PivotCriteria;
import ebilly.admin.shared.viewdef.ResultField;
import ebilly.admin.shared.viewdef.ResultRecord;
import ebilly.admin.shared.viewdef.SearchRequest;
import ebilly.admin.shared.viewdef.SearchResult;
import ecollector.common.PrColumns;
import ecollector.common.ReportAbstractColumns;

public class SummaryDimensions {

	protected static final String M_VERSION = "version";

	protected static final String M_DIM_DISTRIBUTION = "dimDistribution";
	protected static final String M_DIM_DISTRIBUTION_DATE = "dimDistributionDate";
	protected static final String M_DIM_SECTION = "dimSection";
	protected static final String M_DIM_SECTION_DATE = "dimSectionDate";
	protected static final String M_DIM_SUBDIVISION = "dimSubdivision";
	protected static final String M_DIM_SUBDIVISION_DATE = "dimSubdivisionDate";
	protected static final String M_DIM_ERO = "dimEro";
	protected static final String M_DIM_ERO_DATE = "dimEroDate";
	protected static final String M_DIM_DIVISION = "dimDivision";
	protected static final String M_DIM_DIVISION_DATE = "dimDivisionDate";
	protected static final String M_DIM_CIRCLE = "dimCircle";
	protected static final String M_DIM_CIRCLE_DATE = "dimCircleDate";
	protected static final String M_DIM_COMPANY = "dimCompany";
	protected static final String M_DIM_COMPANY_DATE = "dimCompanyDate";
	protected static final String M_DIM_DEVICE = "dimDevice";
	protected static final String M_DIM_DEVICE_DATE = "dimDeviceDate";
	protected static final String M_DIM_DEVICE_DATE_SECTION = "dimDeviceDateSection";
	protected static final String M_DIM_DEVICE_DATE_SECTION_DISTRIBUTION = "dimDeviceDateSectionDist";
	

	private List<ResultField> mResultFields = null;
	private SearchRequest mSearchRequest = null;
	private List<PivotCriteria> mPivotCriteria = null;
	
	protected SummaryDimensions() {
		initResultFields();
		initSearchRequest();
		initPivotCriteria();
	}
	
	public SearchResult asSearchResults(List<DevicePr> devicePrs) {
		SearchResult searchResult = new SearchResult();
		if (devicePrs == null || devicePrs.isEmpty()) {
			return searchResult;
		}
		for (DevicePr devicePr : devicePrs) {
			searchResult.addResultRecord(asResultRecord(devicePr));
		}
		return searchResult;	
	}
	
	public SearchRequest getSearchRequest() {
		return this.mSearchRequest;
	}
	
	private ResultRecord asResultRecord(DevicePr devicePr) {
		//***************************************************
		// NOTE: The order of the fieldValues should be same
		//       as the fields defined in 'initResultFields()'
		//***************************************************
		ResultRecord record = new ResultRecord();
		record.fieldValues.add(devicePr.getARREARS_N_DEMAND());
		record.fieldValues.add("0");// ARREARS_N_DEMAND_COUNT
		record.fieldValues.add(devicePr.getRC_COLLECTED());
		record.fieldValues.add("0");// RC_COLLECTED_COUNT
		record.fieldValues.add(devicePr.getACD_COLLECTED());
		record.fieldValues.add("0");// ACD_COLLECTED_COUNT
		record.fieldValues.add(devicePr.getAGL_AMOUNT());
		record.fieldValues.add("0");// AGL_AMOUNT_COUNT
		record.fieldValues.add("0");// GRAND_TOTAL
		record.fieldValues.add("1");// TOTAL_PRS
		record.fieldValues.add(devicePr.getCompanyCode());
		record.fieldValues.add(devicePr.getCircleCode());
		record.fieldValues.add(devicePr.getDivisionCode());
		record.fieldValues.add(devicePr.getEroCode());
		record.fieldValues.add(devicePr.getSubDivisionCode());
		record.fieldValues.add(devicePr.getSectionCode());
		record.fieldValues.add(devicePr.getDistCode());
		record.fieldValues.add(devicePr.getDeviceName());
		record.fieldValues.add(devicePr.getCOLLECTION_DATE());
		return record;
	}

	private void initResultFields() {

		mResultFields = new ArrayList<ResultField>();

		mResultFields.add(new ResultField(PrColumns.ARREARS_N_DEMAND,java.lang.Double.class.getName()));
		mResultFields.add(new ResultField(ResultRecord.getCountFieldFor(PrColumns.ARREARS_N_DEMAND),java.lang.Integer.class.getName()));

		mResultFields.add(new ResultField(PrColumns.RC_COLLECTED,java.lang.Double.class.getName()));
		mResultFields.add(new ResultField(ResultRecord.getCountFieldFor(PrColumns.RC_COLLECTED),java.lang.Integer.class.getName()));

		mResultFields.add(new ResultField(PrColumns.ACD_COLLECTED,java.lang.Double.class.getName()));
		mResultFields.add(new ResultField(ResultRecord.getCountFieldFor(PrColumns.ACD_COLLECTED),java.lang.Integer.class.getName()));

		mResultFields.add(new ResultField(PrColumns.AGL_AMOUNT,java.lang.Double.class.getName()));
		mResultFields.add(new ResultField(ResultRecord.getCountFieldFor(PrColumns.AGL_AMOUNT),java.lang.Integer.class.getName()));

		mResultFields.add(new ResultField(ReportAbstractColumns.GRAND_TOTAL,java.lang.Double.class.getName()));
		mResultFields.add(new ResultField(ReportAbstractColumns.TOTAL_PRS,java.lang.Long.class.getName()));

		mResultFields.add(new ResultField(PrColumns._COMPANY_CODE,java.lang.String.class.getName()));
		mResultFields.add(new ResultField(PrColumns._CIRCLE_CODE,java.lang.String.class.getName()));
		mResultFields.add(new ResultField(PrColumns._DIVISION_CODE,java.lang.String.class.getName()));
		mResultFields.add(new ResultField(PrColumns._ERO_CODE,java.lang.String.class.getName()));
		mResultFields.add(new ResultField(PrColumns._SUBDIVISION_CODE,java.lang.String.class.getName()));
		mResultFields.add(new ResultField(PrColumns._SECTION_CODE,java.lang.String.class.getName()));
		mResultFields.add(new ResultField(PrColumns._DIST_CODE,java.lang.String.class.getName()));
		mResultFields.add(new ResultField(PrColumns._DEVICE_NAME,java.lang.String.class.getName()));
		mResultFields.add(new ResultField(PrColumns.COLLECTION_DATE,java.lang.String.class.getName()));
	}
	
	private void initSearchRequest() {
		mSearchRequest = new SearchRequest(null, mResultFields);
	}
	
	private void initPivotCriteria() {
		mPivotCriteria = new ArrayList<PivotCriteria>();
		mPivotCriteria.add(mSearchRequest.pivotCriteriaFor(ReportAbstractColumns.ARREARS_N_DEMAND, "SUM"));
		mPivotCriteria.add(mSearchRequest.pivotCriteriaFor(ReportAbstractColumns.ACD_COLLECTED, "SUM"));
		mPivotCriteria.add(mSearchRequest.pivotCriteriaFor(ReportAbstractColumns.AGL_AMOUNT, "SUM"));
		mPivotCriteria.add(mSearchRequest.pivotCriteriaFor(ReportAbstractColumns.RC_COLLECTED, "SUM"));
		mPivotCriteria.add(mSearchRequest.pivotCriteriaFor(ReportAbstractColumns.TOTAL_PRS, "SUM"));
	}

	protected List<ResultField> resultFields() {
		return mResultFields;
	}

	protected ResultAggregator loadDimension(String dimension, JsonObject ojson) {
		if (M_DIM_DISTRIBUTION.equals(dimension)) {
			return loadDimDistribution(ojson);
		} else if (M_DIM_DISTRIBUTION_DATE.equals(dimension)) {
			return loadDimDistributionDate(ojson);
		} else if (M_DIM_SECTION.equals(dimension)) {
			return loadDimSection(ojson);
		} else if (M_DIM_SECTION_DATE.equals(dimension)) {
			return loadDimSectionDate(ojson);
		} else if (M_DIM_SUBDIVISION.equals(dimension)) {
			return loadDimSubdivision(ojson);
		} else if (M_DIM_SUBDIVISION_DATE.equals(dimension)) {
			return loadDimSubdivisionDate(ojson);
		} else if (M_DIM_ERO.equals(dimension)) {
			return loadDimEro(ojson);
		} else if (M_DIM_ERO_DATE.equals(dimension)) {
			return loadDimEroDate(ojson);
		} else if (M_DIM_DIVISION.equals(dimension)) {
			return loadDimDivision(ojson);
		} else if (M_DIM_DIVISION_DATE.equals(dimension)) {
			return loadDimDivisionDate(ojson);
		} else if (M_DIM_CIRCLE.equals(dimension)) {
			return loadDimCircle(ojson);
		} else if (M_DIM_CIRCLE_DATE.equals(dimension)) {
			return loadDimCircleDate(ojson);
		} else if (M_DIM_COMPANY.equals(dimension)) {
			return loadDimCompany(ojson);
		} else if (M_DIM_COMPANY_DATE.equals(dimension)) {
			return loadDimCompanyDate(ojson);
		} else if (M_DIM_DEVICE.equals(dimension)) {
			return loadDimDevice(ojson);
		} else if (M_DIM_DEVICE_DATE.equals(dimension)) {
			return loadDimDeviceDate(ojson);
		} else if (M_DIM_DEVICE_DATE_SECTION.equals(dimension)) {
			return loadDimDeviceDateSection(ojson);
		} else if (M_DIM_DEVICE_DATE_SECTION_DISTRIBUTION.equals(dimension)) {
			return loadDimDeviceDateSectionDist(ojson);
		} else {
			throw new IllegalArgumentException("Unknown dimension "+dimension);
		}
	}

	protected ResultAggregator loadOrCreateDim(JsonObject ojson,
			String dimName, String[] dimUniqueFields) {
		JsonObject asJsonObject = null;
		ResultAggregator aggregator = null;
		if (ojson != null) {
			asJsonObject = ojson.getAsJsonObject(dimName);
			// PSR: Tuned
			// Remove the json object for this dimension
			// so that it minimizes the foot print.
			// 
			ojson.remove(dimName); 
		}
		
		SearchResult results = new SearchResult();
		
		if (asJsonObject != null) {
			TimeLogger tl = TimeLogger.create();
			aggregator = ResultAggregatorFactory.newResultAggregator(
					asJsonObject,results,dimUniqueFields, mPivotCriteria,
					mSearchRequest);
			tl.logElapsed("Unmarshalling aggregator for "+dimName+" took ");
		}
		if (aggregator == null) {
			aggregator = new ResultAggregator(results, 
					dimUniqueFields, mPivotCriteria, 
					mSearchRequest);
		}
		return aggregator;
	}
	
	protected ResultAggregator loadOrCreateDim(JsonParser jp,
			String dimName, String[] dimUniqueFields) 
	throws JsonParseException, IOException {
		ResultAggregator aggregator = null;
		SearchResult results = new SearchResult();
		
		if (jp != null) {
			
			JsonToken t = jp.nextToken(); // dimension name
			String jsonDimName = jp.getText();
			if (!dimName.equals(jsonDimName)) {
				throw new IllegalArgumentException(
						"Unexpected dimension during parsing, expected: "+
						dimName+", encountered: "+jsonDimName);
			}
			
			TimeLogger tl = TimeLogger.create();
			aggregator = ResultAggregatorJacksonFactory.newResultAggregator(
					jp,results,dimUniqueFields, mPivotCriteria,
					mSearchRequest);
			tl.logElapsed("Unmarshalling aggregator for "+dimName+" took ");
		} else {
			aggregator = new ResultAggregator(results, 
					dimUniqueFields, mPivotCriteria, 
					mSearchRequest);
		}
		return aggregator;
	}

	protected ResultAggregator loadDimDeviceDate(JsonObject ojson) {
		return loadOrCreateDim(
					ojson,
					M_DIM_DEVICE_DATE,
					new String[]{
							PrColumns._DEVICE_NAME, PrColumns.COLLECTION_DATE
					}); 
	}
	
	protected ResultAggregator loadDimDeviceDate(JsonParser ojson) 
	throws JsonParseException, IOException {
		return loadOrCreateDim(
					ojson,
					M_DIM_DEVICE_DATE,
					new String[]{
							PrColumns._DEVICE_NAME, PrColumns.COLLECTION_DATE
					}); 
	}
	
	protected ResultAggregator loadDimDeviceDateSection(JsonObject ojson) {
		return loadOrCreateDim(
					ojson,
					M_DIM_DEVICE_DATE_SECTION,
					new String[]{
							PrColumns._DEVICE_NAME, PrColumns.COLLECTION_DATE, PrColumns._SECTION_CODE
					}); 
	}
	
	protected ResultAggregator loadDimDeviceDateSection(JsonParser ojson) 
	throws JsonParseException, IOException {
		return loadOrCreateDim(
					ojson,
					M_DIM_DEVICE_DATE_SECTION,
					new String[]{
							PrColumns._DEVICE_NAME, PrColumns.COLLECTION_DATE, PrColumns._SECTION_CODE
					}); 
	}
	
	protected ResultAggregator loadDimDeviceDateSectionDist(JsonObject ojson) {
		return loadOrCreateDim(
					ojson,
					M_DIM_DEVICE_DATE_SECTION_DISTRIBUTION,
					new String[]{
							PrColumns._DEVICE_NAME, PrColumns.COLLECTION_DATE, PrColumns._SECTION_CODE, PrColumns._DIST_CODE
					}); 
	}
	
	protected ResultAggregator loadDimDeviceDateSectionDist(JsonParser ojson) 
	throws JsonParseException, IOException {
		return loadOrCreateDim(
					ojson,
					M_DIM_DEVICE_DATE_SECTION_DISTRIBUTION,
					new String[]{
							PrColumns._DEVICE_NAME, PrColumns.COLLECTION_DATE, PrColumns._SECTION_CODE, PrColumns._DIST_CODE
					}); 
	}

	protected ResultAggregator loadDimDevice(JsonObject ojson) {
		return loadOrCreateDim(
					ojson,
					M_DIM_DEVICE,
					new String[]{
							PrColumns._DEVICE_NAME
					}); 
	}
	
	protected ResultAggregator loadDimDevice(JsonParser ojson) 
	throws JsonParseException, IOException {
		return loadOrCreateDim(
					ojson,
					M_DIM_DEVICE,
					new String[]{
							PrColumns._DEVICE_NAME
					}); 
	}

	protected ResultAggregator loadDimCompanyDate(JsonObject ojson) {
		return loadOrCreateDim(
					ojson,
					M_DIM_COMPANY_DATE,
					new String[]{
							PrColumns._COMPANY_CODE, PrColumns.COLLECTION_DATE
					}
			);
	}	

	protected ResultAggregator loadDimCompanyDate(JsonParser ojson) throws JsonParseException, IOException {
		return loadOrCreateDim(
					ojson,
					M_DIM_COMPANY_DATE,
					new String[]{
							PrColumns._COMPANY_CODE, PrColumns.COLLECTION_DATE
					}
			);
	}
	
	protected ResultAggregator loadDimCompany(JsonObject ojson) {
		return loadOrCreateDim(
					ojson,
					M_DIM_COMPANY,
					new String[]{
							PrColumns._COMPANY_CODE
					}
			);
	}
	
	protected ResultAggregator loadDimCompany(JsonParser ojson) 
	throws JsonParseException, IOException {
		return loadOrCreateDim(
					ojson,
					M_DIM_COMPANY,
					new String[]{
							PrColumns._COMPANY_CODE
					}
			);
	}

	protected ResultAggregator loadDimCircleDate(JsonObject ojson) {
		return loadOrCreateDim(
					ojson,
					M_DIM_CIRCLE_DATE,
					new String[]{
							PrColumns._CIRCLE_CODE, PrColumns.COLLECTION_DATE
					}
			);
	}
	
	protected ResultAggregator loadDimCircleDate(JsonParser ojson) 
	throws JsonParseException, IOException {
		return loadOrCreateDim(
					ojson,
					M_DIM_CIRCLE_DATE,
					new String[]{
							PrColumns._CIRCLE_CODE, PrColumns.COLLECTION_DATE
					}
			);
	}

	protected ResultAggregator loadDimCircle(JsonObject ojson) {
		return loadOrCreateDim(
					ojson,
					M_DIM_CIRCLE,
					new String[]{
							PrColumns._CIRCLE_CODE
					}
			);
	}
	
	protected ResultAggregator loadDimCircle(JsonParser ojson) 
	throws JsonParseException, IOException {
		return loadOrCreateDim(
					ojson,
					M_DIM_CIRCLE,
					new String[]{
							PrColumns._CIRCLE_CODE
					}
			);
	}

	protected ResultAggregator loadDimDivisionDate(JsonObject ojson) {
		return loadOrCreateDim(
					ojson,
					M_DIM_DIVISION_DATE,
					new String[]{
							PrColumns._DIVISION_CODE, PrColumns.COLLECTION_DATE
					}
			);
	}
	
	protected ResultAggregator loadDimDivisionDate(JsonParser ojson) 
	throws JsonParseException, IOException {
		return loadOrCreateDim(
					ojson,
					M_DIM_DIVISION_DATE,
					new String[]{
							PrColumns._DIVISION_CODE, PrColumns.COLLECTION_DATE
					}
			);
	}

	protected ResultAggregator loadDimDivision(JsonObject ojson) {
		return loadOrCreateDim(
					ojson,
					M_DIM_DIVISION,
					new String[]{
							PrColumns._DIVISION_CODE
					}
			);
	}
	
	protected ResultAggregator loadDimDivision(JsonParser ojson) 
	throws JsonParseException, IOException {
		return loadOrCreateDim(
					ojson,
					M_DIM_DIVISION,
					new String[]{
							PrColumns._DIVISION_CODE
					}
			);
	}

	protected ResultAggregator loadDimSubdivisionDate(JsonObject ojson) {
		return loadOrCreateDim(
					ojson,
					M_DIM_SUBDIVISION_DATE,
					new String[]{
							PrColumns._SUBDIVISION_CODE, PrColumns.COLLECTION_DATE
					}
			);
	}
	
	protected ResultAggregator loadDimSubdivisionDate(JsonParser ojson) 
	throws JsonParseException, IOException {
		return loadOrCreateDim(
					ojson,
					M_DIM_SUBDIVISION_DATE,
					new String[]{
							PrColumns._SUBDIVISION_CODE, PrColumns.COLLECTION_DATE
					}
			);
	}
	
	protected ResultAggregator loadDimEro(JsonObject ojson) {
		return loadOrCreateDim(
					ojson,
					M_DIM_ERO,
					new String[]{
							PrColumns._ERO_CODE
					}
			);
	}

	protected ResultAggregator loadDimEro(JsonParser ojson) throws JsonParseException, IOException {
		return loadOrCreateDim(
					ojson,
					M_DIM_ERO,
					new String[]{
							PrColumns._ERO_CODE
					}
			);
	}
	
	protected ResultAggregator loadDimEroDate(JsonObject ojson) {
		return loadOrCreateDim(
					ojson,
					M_DIM_ERO_DATE,
					new String[]{
							PrColumns._ERO_CODE, PrColumns.COLLECTION_DATE
					}
			);
	}
	
	protected ResultAggregator loadDimEroDate(JsonParser ojson) 
	throws JsonParseException, IOException {
		return loadOrCreateDim(
					ojson,
					M_DIM_ERO_DATE,
					new String[]{
							PrColumns._ERO_CODE, PrColumns.COLLECTION_DATE
					}
			);
	}

	protected ResultAggregator loadDimSubdivision(JsonObject ojson) {
		return loadOrCreateDim(
					ojson,
					M_DIM_SUBDIVISION,
					new String[]{
							PrColumns._SUBDIVISION_CODE
					}
			);
	}
	
	protected ResultAggregator loadDimSubdivision(JsonParser ojson) 
	throws JsonParseException, IOException {
		return loadOrCreateDim(
					ojson,
					M_DIM_SUBDIVISION,
					new String[]{
							PrColumns._SUBDIVISION_CODE
					}
			);
	}

	protected ResultAggregator loadDimSectionDate(JsonObject ojson) {
		return loadOrCreateDim(
					ojson,
					M_DIM_SECTION_DATE,
					new String[]{
							PrColumns._SECTION_CODE, PrColumns.COLLECTION_DATE
					}
			);
	}

	protected ResultAggregator loadDimSectionDate(JsonParser ojson) 
	throws JsonParseException, IOException {
		return loadOrCreateDim(
					ojson,
					M_DIM_SECTION_DATE,
					new String[]{
							PrColumns._SECTION_CODE, PrColumns.COLLECTION_DATE
					}
			);
	}
	
	protected ResultAggregator loadDimSection(JsonObject ojson) {
		return loadOrCreateDim(
					ojson,
					M_DIM_SECTION,
					new String[]{
							PrColumns._SECTION_CODE
					}
			);
	}

	protected ResultAggregator loadDimSection(JsonParser ojson) 
	throws JsonParseException, IOException {
		return loadOrCreateDim(
					ojson,
					M_DIM_SECTION,
					new String[]{
							PrColumns._SECTION_CODE
					}
			);
	}
	
	protected ResultAggregator loadDimDistributionDate(JsonObject ojson) {
		return loadOrCreateDim(
					ojson,
					M_DIM_DISTRIBUTION_DATE,
					new String[]{
							PrColumns._DIST_CODE, PrColumns.COLLECTION_DATE
					}
			);
	}
	
	protected ResultAggregator loadDimDistributionDate(JsonParser ojson) 
	throws JsonParseException, IOException {
		return loadOrCreateDim(
					ojson,
					M_DIM_DISTRIBUTION_DATE,
					new String[]{
							PrColumns._DIST_CODE, PrColumns.COLLECTION_DATE
					}
			);
	}

	protected ResultAggregator loadDimDistribution(JsonObject ojson) {
		return loadOrCreateDim(
					ojson,
					M_DIM_DISTRIBUTION,
					new String[]{
							PrColumns._DIST_CODE
					}
			);
	}
	
	protected ResultAggregator loadDimDistribution(JsonParser ojson) 
	throws JsonParseException, IOException {
		return loadOrCreateDim(
					ojson,
					M_DIM_DISTRIBUTION,
					new String[]{
							PrColumns._DIST_CODE
					}
			);
	}

	protected void computeGrandTotal(ResultAggregator dim) {
		SearchResult results = dim.getAggregateSearchResultWithTypedData();
		List<ResultRecord> records = results.getResults();
		for (ResultRecord record : records) {
			double arrearsAndDemand = record.doubleValue(PrColumns.ARREARS_N_DEMAND);
			double rcAmount = record.doubleValue(PrColumns.RC_COLLECTED);
			double acdAmount = record.doubleValue(PrColumns.ACD_COLLECTED);
			double aglAmount = record.doubleValue(PrColumns.AGL_AMOUNT);
			double grandTotal = arrearsAndDemand + rcAmount + acdAmount + aglAmount;
			record.setValue(ReportAbstractColumns.GRAND_TOTAL, grandTotal);
		}
	}	
}