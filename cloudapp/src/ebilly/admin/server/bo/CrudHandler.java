package ebilly.admin.server.bo;

import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.viewdef.CrudRequest;
import ebilly.admin.shared.viewdef.CrudResponse;

public abstract class CrudHandler {
	
	private CrudRequest request;
	public CrudHandler(CrudRequest request) {
		this.request = request;
	}
	
	public CrudRequest getRequest() {
		return this.request;
	}
	
	public CrudResponse handleRequest() {
		
		if (AppConfig.CRUD_ACTION_RETRIEVE.equals(this.request.crudAction)) {
			return processRetrieve(this.request);
		} else if (AppConfig.CRUD_ACTION_CREATE_UPDATE.equals(this.request.crudAction)) {
			return processCreateUpdate(this.request);
		}
		return null;
	}
	
	protected abstract CrudResponse processCreateUpdate(CrudRequest request);
	protected abstract CrudResponse processRetrieve(CrudRequest request);

	public static CrudHandler getHandler(CrudRequest request) {
		if (AppConfig.CRUD_USER.equals(request.crudObject)) {
			return new UserCrudHandler(request);
		} else if (AppConfig.CRUD_DEVICE.equals(request.crudObject)) {
			return new DeviceCrudHandler(request);
		} else if (AppConfig.CRUD_ORGENIZATION.equals(request.crudObject)) {
			return new OrgenizationCrudHandler(request);
		} else if (AppConfig.CRUD_COLLECTION_DASHBOARD_DATA.equals(request.crudObject)) {
			return new CollectionDashboardDataCrudHandler(request);
		} else {
			throw new IllegalArgumentException("Unsupported crud object "+request.crudObject);
		}
	}
}

