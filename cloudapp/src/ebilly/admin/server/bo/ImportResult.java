package ebilly.admin.server.bo;

import java.util.ArrayList;
import java.util.List;

public class ImportResult {
	
	class FailureRecord {
		String[] recordData;
		String failureReason;
		int recordNumber;
		public FailureRecord(String[] recordData, String reason, int recordNumber) {
			this.recordData = recordData;
			this.failureReason = reason;
			this.recordNumber = recordNumber;
		}
		@Override
		public String toString() {
			return "\"RecordNumber: "
				+ recordNumber
				+" - "+ failureReason + "\","
				+recordDataAsCsv();
		}
		
		public String recordDataAsCsv() {
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < recordData.length; i++) {
				sb.append(recordData[i]);
				if (i < recordData.length - 1) {
					sb.append(",");
				}
			}
			return sb.toString();
		}
	}
	
	private List<FailureRecord> failureRecords = new ArrayList<FailureRecord>();
	private String[] header;
	private int successCounter;
	private int failureCounter;
	public StringBuffer errorMessage = new StringBuffer();
	
	public ImportResult() {
		
	}
	
	public void setHeader(String[] header) {
		this.header = header;
	}
	
	public String[] getHeader() {
		return this.header;
	}
	
	public String getHeaderAsCsv() {
		if (this.header == null) return "";
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < this.header.length; i++) {
			sb.append(this.header[i]);
			if (i < this.header.length - 1) {
				sb.append(",");
			}
		}
		return sb.toString();
	}
	
	public void addFailure(String[] record, String reason, int recordNumber) {
		failureRecords.add(new FailureRecord(record,reason,recordNumber));
	}
	
	public void countSuccess() {
		this.successCounter++;
	}
	
	public void countFailure() {
		this.failureCounter++;
	}
	
	public int getSuccessCounter() {
		return successCounter;
	}

	public int getFailureCounter() {
		return failureCounter;
	}

	public String getSummaryString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Import Summary\n");
		sb.append("SuccessCount: ").append(this.successCounter).append("\n");
		sb.append("FailureCount: ").append(this.failureCounter).append("\n");
		return sb.toString();
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer(this.getSummaryString());
		if (failureRecords.size() > 0) {
			sb.append("Following records failed to import \n");
			for (FailureRecord fr : failureRecords) {
				sb.append(fr.toString()).append("\n");
			}
		}
		return sb.toString();
	}
	public String getFailureRecordsAsCsvFormat() {
		StringBuffer sb = new StringBuffer();
		sb.append("Failure Reason,"+this.getHeaderAsCsv()).append("\n");
		if (failureRecords.size() > 0) {
			for (FailureRecord fr : failureRecords) {
				sb.append(fr.toString()).append("\n");
			}
		}
		return sb.toString();
	}
}

