package ebilly.admin.server.bo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import ebilly.admin.server.core.reports.ResultAggregator;
import ebilly.admin.server.db.DevicePr;
import ebilly.admin.shared.viewdef.PivotCriteria;
import ebilly.admin.shared.viewdef.ResultField;
import ebilly.admin.shared.viewdef.ResultRecord;
import ebilly.admin.shared.viewdef.SearchRequest;
import ebilly.admin.shared.viewdef.SearchResult;
import ecollector.common.PrColumns;
import ecollector.common.ReportAbstractColumns;

public class PrSummary {
	private ResultAggregator distributions;
	/*private ResultAggregator sections;
	private ResultAggregator subdivisions;
	private ResultAggregator divisions;
	private ResultAggregator circles;
	private ResultAggregator company;*/
	private static SearchRequest searchRequest;

	/** Aggregator setup **/
	private static String[] distUniqueFields = 
		new String[]{
				PrColumns._COMPANY_CODE,PrColumns._CIRCLE_CODE,
				PrColumns._DIVISION_CODE,PrColumns._SUBDIVISION_CODE,
				PrColumns._SECTION_CODE,PrColumns._DIST_CODE
	};
	private static String[] sectUniqueFields = 
		new String[]{
			PrColumns._COMPANY_CODE,PrColumns._CIRCLE_CODE,
			PrColumns._DIVISION_CODE,PrColumns._SUBDIVISION_CODE,
			PrColumns._SECTION_CODE
	};
	private static String[] subDivisionUniqueFields = 
		new String[]{
			PrColumns._COMPANY_CODE,PrColumns._CIRCLE_CODE,
			PrColumns._DIVISION_CODE,PrColumns._SUBDIVISION_CODE
	};
	private static String[] divisionUniqueFields = 
		new String[]{
			PrColumns._COMPANY_CODE,PrColumns._CIRCLE_CODE,
			PrColumns._DIVISION_CODE
	};
	private static String[] circleUniqueFields = 
		new String[]{
			PrColumns._COMPANY_CODE,PrColumns._CIRCLE_CODE
	};
	private static String[] companyUniqueFields = 
		new String[]{
			PrColumns._COMPANY_CODE
	};
	
	private static List<PivotCriteria> distPivotCriteria = new ArrayList<PivotCriteria>();
	private static List<PivotCriteria> forAnyPivotCriteria = new ArrayList<PivotCriteria>();
	private static List<PivotCriteria> subDivisionPivotCriteria = new ArrayList<PivotCriteria>();
	static {
		List<ResultField> resultFields = new ArrayList<ResultField>();
		resultFields.add(new ResultField(PrColumns.ARREARS_N_DEMAND,java.lang.Double.class.getName()));
		resultFields.add(new ResultField(ResultRecord.getCountFieldFor(PrColumns.ARREARS_N_DEMAND),java.lang.Integer.class.getName()));
		//resultFields.add(new ResultField(ReportAbstractColumns.ARREARS_N_DEMAND_COUNT,java.lang.Long.class.getName()));
		
		resultFields.add(new ResultField(PrColumns.RC_COLLECTED,java.lang.Double.class.getName()));
		resultFields.add(new ResultField(ResultRecord.getCountFieldFor(PrColumns.RC_COLLECTED),java.lang.Integer.class.getName()));
		//resultFields.add(new ResultField(ReportAbstractColumns.RC_COLLECTED_COUNT,java.lang.Long.class.getName()));
		
		resultFields.add(new ResultField(PrColumns.ACD_COLLECTED,java.lang.Double.class.getName()));
		resultFields.add(new ResultField(ResultRecord.getCountFieldFor(PrColumns.ACD_COLLECTED),java.lang.Integer.class.getName()));
		//resultFields.add(new ResultField(ReportAbstractColumns.ACD_COLLECTED_COUNT,java.lang.Long.class.getName()));
		
		resultFields.add(new ResultField(PrColumns.AGL_AMOUNT,java.lang.Double.class.getName()));
		resultFields.add(new ResultField(ResultRecord.getCountFieldFor(PrColumns.AGL_AMOUNT),java.lang.Integer.class.getName()));
		//resultFields.add(new ResultField(ReportAbstractColumns.AGL_AMOUNT_COUNT,java.lang.Long.class.getName()));
		
		resultFields.add(new ResultField(ReportAbstractColumns.GRAND_TOTAL,java.lang.Double.class.getName()));
		resultFields.add(new ResultField(ReportAbstractColumns.TOTAL_PRS,java.lang.Long.class.getName()));
		resultFields.add(new ResultField(PrColumns._COMPANY_CODE,java.lang.String.class.getName()));
		resultFields.add(new ResultField(PrColumns._CIRCLE_CODE,java.lang.String.class.getName()));
		resultFields.add(new ResultField(PrColumns._DIVISION_CODE,java.lang.String.class.getName()));
		resultFields.add(new ResultField(PrColumns._SUBDIVISION_CODE,java.lang.String.class.getName()));
		resultFields.add(new ResultField(PrColumns._SECTION_CODE,java.lang.String.class.getName()));
		resultFields.add(new ResultField(PrColumns._DIST_CODE,java.lang.String.class.getName()));
		searchRequest = new SearchRequest(null, resultFields);
		
		distPivotCriteria.add(searchRequest.pivotCriteriaFor(ReportAbstractColumns.ARREARS_N_DEMAND, "SUM"));
		distPivotCriteria.add(searchRequest.pivotCriteriaFor(ReportAbstractColumns.ACD_COLLECTED, "SUM"));
		distPivotCriteria.add(searchRequest.pivotCriteriaFor(ReportAbstractColumns.AGL_AMOUNT, "SUM"));
		distPivotCriteria.add(searchRequest.pivotCriteriaFor(ReportAbstractColumns.RC_COLLECTED, "SUM"));
		distPivotCriteria.add(searchRequest.pivotCriteriaFor(ReportAbstractColumns.TOTAL_PRS, "COUNT"));
		
		forAnyPivotCriteria.add(searchRequest.pivotCriteriaFor(ReportAbstractColumns.ARREARS_N_DEMAND, "SUM"));
		forAnyPivotCriteria.add(searchRequest.pivotCriteriaFor(ResultRecord.getCountFieldFor(ReportAbstractColumns.ARREARS_N_DEMAND), "SUM"));
		forAnyPivotCriteria.add(searchRequest.pivotCriteriaFor(ReportAbstractColumns.ACD_COLLECTED, "SUM"));
		forAnyPivotCriteria.add(searchRequest.pivotCriteriaFor(ResultRecord.getCountFieldFor(ReportAbstractColumns.ACD_COLLECTED), "SUM"));
		forAnyPivotCriteria.add(searchRequest.pivotCriteriaFor(ReportAbstractColumns.AGL_AMOUNT, "SUM"));
		forAnyPivotCriteria.add(searchRequest.pivotCriteriaFor(ResultRecord.getCountFieldFor(ReportAbstractColumns.AGL_AMOUNT), "SUM"));
		forAnyPivotCriteria.add(searchRequest.pivotCriteriaFor(ReportAbstractColumns.RC_COLLECTED, "SUM"));
		forAnyPivotCriteria.add(searchRequest.pivotCriteriaFor(ResultRecord.getCountFieldFor(ReportAbstractColumns.RC_COLLECTED), "SUM"));
		forAnyPivotCriteria.add(searchRequest.pivotCriteriaFor(ReportAbstractColumns.TOTAL_PRS, "COUNT"));
	}
	
	public PrSummary() {
		SearchResult results = createSearchResult();
		//List<PivotCriteria> distPivotCriteria = createDistPivotCriteria(searchRequest);
		distributions = new ResultAggregator(
				results,distUniqueFields,distPivotCriteria,searchRequest);
	}

	private SearchResult createSearchResult() {
		SearchResult results = new SearchResult();
		return results;
	}

	public SearchRequest getSearchRequest() {
		return searchRequest;
	}
	
	private void aggregate(ResultRecord record) {
		distributions.aggregateRecord(record);
	}
	
	public void aggregate(DevicePr devicePr) {
		ResultRecord record = new ResultRecord();
		record.fieldValues.add(devicePr.getARREARS_N_DEMAND());
		record.fieldValues.add("0");// ARREARS_N_DEMAND_COUNT
		record.fieldValues.add(devicePr.getRC_COLLECTED());
		record.fieldValues.add("0");// RC_COLLECTED_COUNT
		record.fieldValues.add(devicePr.getACD_COLLECTED());
		record.fieldValues.add("0");// ACD_COLLECTED_COUNT
		record.fieldValues.add(devicePr.getAGL_AMOUNT());
		record.fieldValues.add("0");// AGL_AMOUNT_COUNT
		record.fieldValues.add("0");// GRAND_TOTAL
		record.fieldValues.add("0");// TOTAL_PRS
		record.fieldValues.add(devicePr.getCompanyCode());
		record.fieldValues.add(devicePr.getCircleCode());
		record.fieldValues.add(devicePr.getDivisionCode());
		record.fieldValues.add(devicePr.getSubDivisionCode());
		record.fieldValues.add(devicePr.getSectionCode());
		record.fieldValues.add(devicePr.getDistCode());
		this.aggregate(record);
	}
	
	public SearchResult getDistributionSummary() {
		return distributions.getAggregateSearchResult();
	}
	
	public SearchResult getSectionSummary(SearchResult results) {
		ResultAggregator sectAggregator = new ResultAggregator(
				results,sectUniqueFields,forAnyPivotCriteria,searchRequest);
		return sectAggregator.applyAggregation();
	}
	
	public SearchResult getSubDivisionSummary(SearchResult results) {
		ResultAggregator sectAggregator = new ResultAggregator(
				results,subDivisionUniqueFields,forAnyPivotCriteria,searchRequest);
		return sectAggregator.applyAggregation();
	}
	
	public SearchResult getDivisionSummary(SearchResult results) {
		ResultAggregator sectAggregator = new ResultAggregator(
				results,divisionUniqueFields,forAnyPivotCriteria,searchRequest);
		return sectAggregator.applyAggregation();
	}
	
	public SearchResult getCircleSummary(SearchResult results) {
		ResultAggregator sectAggregator = new ResultAggregator(
				results,circleUniqueFields,forAnyPivotCriteria,searchRequest);
		return sectAggregator.applyAggregation();
	}
	
	public SearchResult getCompanySummary(SearchResult results) {
		ResultAggregator sectAggregator = new ResultAggregator(
				results,companyUniqueFields,forAnyPivotCriteria,searchRequest);
		return sectAggregator.applyAggregation();
	}

	private static long startMillies;
	public static void main(String[] args) {
		
		//buildUsc();
		//System.exit(0);
		
		
		startMillies = System.currentTimeMillis();
		PrSummary ps = new PrSummary();
		
		DevicePr devicePr = new DevicePr();
		int NUM_COMPANY = 1;
		int NUM_CIRCLE = 3;
		int NUM_DIVISION = 3;
		int NUM_SUB_DIV = 3;
		int NUM_SECTION = 3;
		int NUM_DIST = 32; // 2 digit
		int NUM_SC = 10;
		
		long estimatedPrs = 
			(1+NUM_COMPANY)*(1+NUM_CIRCLE)*
			(1+NUM_DIVISION)*(1+NUM_SUB_DIV)*
			(1+NUM_SECTION)*(1+NUM_DIST)*(1+NUM_SC);
		log("Summarizing "+estimatedPrs+" PRs");
		
		int and = 200;
		int rc = 50;
		int acd = 70;
		int agl = 60;
		
		int company = 0;
		int circle = 0;
		int division = 0;
		int sub_div = 0;
		int section = 0;
		int dist = 0;
		int sc = 0;
		long uscNo = 0;
		long totalPrs = 0;
		int prevPercent = 0; 
		for (; company <= NUM_COMPANY; company++) { circle = 0;
		for (; circle <= NUM_CIRCLE; circle++) { division = 0;
		for (; division <= NUM_DIVISION; division++) { sub_div = 0;
		for (; sub_div <= NUM_SUB_DIV; sub_div++) { section = 0;
		for (; section <= NUM_SECTION; section++) { dist = 0;
		for (; dist <= NUM_DIST; dist++) { sc = 0;
		for (; sc <= NUM_SC; sc++) {
			devicePr.setARREARS_N_DEMAND(and+"");
			devicePr.setRC_COLLECTED(rc+"");
			devicePr.setACD_COLLECTED(acd+"");
			devicePr.setAGL_AMOUNT(agl+"");
			
			// COMPUTE USC_NO
			uscNo = 
				(1+company) *1000000000000L +
			    (1+circle)  * 100000000000L +
			    (1+division)*  10000000000L +
			    (1+sub_div) *   1000000000L +
			    (1+section) *    100000000L +
			       (1+dist) *      1000000L +
			            sc;
			//log("UscNum["+uscNo+"]");
			devicePr.setUSC_NO(uscNo+"");
			ps.aggregate(devicePr);
			totalPrs++;
			int percent = (int)((totalPrs*100.0)/estimatedPrs);
			if ( prevPercent != percent) {
				prevPercent = percent;
				log("Aggregating "+totalPrs +" of "+estimatedPrs+ "( "+prevPercent+"% )");
			}
		}}}}}}}
		
		log("Aggregating Dimensions, TotalPrs: "+totalPrs);
		
		SearchResult distributionSummary = ps.getDistributionSummary();
		printSummary(distributionSummary.getResults(),"Distribution",false,ps.getSearchRequest());
		
		SearchResult sectionSummary = ps.getSectionSummary(distributionSummary);
		printSummary(sectionSummary.getResults(),"Section",false,ps.getSearchRequest());
		
		SearchResult subDivisionSummary = ps.getSubDivisionSummary(sectionSummary);
		printSummary(subDivisionSummary.getResults(),"SubDivision",false,ps.getSearchRequest());
		
		SearchResult divisionSummary = ps.getDivisionSummary(subDivisionSummary);
		printSummary(divisionSummary.getResults(),"Division",false,ps.getSearchRequest());
		
		SearchResult circleSummary = ps.getCircleSummary(divisionSummary);
		printSummary(circleSummary.getResults(),"Circle",true,ps.getSearchRequest());
		
		SearchResult companySummary = ps.getCompanySummary(circleSummary);  
		printSummary(companySummary.getResults(),"Company",true,ps.getSearchRequest());
		
		log("TotalPrs: "+totalPrs);
		long endMillies = System.currentTimeMillis();
		log("Pocessing time in millies: "+(endMillies-startMillies));
	}

	private static void printSummary(List<ResultRecord> results, 
									 String title, boolean showDetails, SearchRequest request) {
		log("  ***  "+title+"  ***  ");
		StringBuffer s = new StringBuffer();
		s.append("Unique Records: "+results.size());
		if (showDetails) {
			for (ResultRecord r : results) {
				s.append("\n");
				//for (Entry<String, Object> v : r.fieldTypedValueMap.entrySet()) {
				for (String key : request.getResultFieldNames()) {
					s.append(key).append("[").append(request.getValue(key, r)).append("]");
				}
			}
		}
		log(title+" Summary: "+s.toString());
		log("\n\n");
	}
	
	private static void buildUsc() {
		//  1 2 1 3 2 03 000002
		int company = 1;
		int circle = 2;
		int division = 1;
		int sub_div = 3;
		int section = 2;
		int dist = 3;
		int sc = 2;
		long uscNo = 
			company *1000000000000L +
		    circle  * 100000000000L +
		    division*  10000000000L +
		     sub_div*   1000000000L +
		     section*    100000000L +
		        dist*      1000000L +
		          sc;
		log("uscNo: ["+uscNo+"]");
	}
	
	public static void log(String s) {
		long deltaMillies = System.currentTimeMillis() - PrSummary.startMillies;
		System.out.println(deltaMillies+" "+s);
	}
}

/*

TotalPrs: 2646270
Pocessing time in millies: 205890
Max Mem Usage: ~400Mb

TotalPrs: 2646270
Pocessing time in millies: 106296

*/
