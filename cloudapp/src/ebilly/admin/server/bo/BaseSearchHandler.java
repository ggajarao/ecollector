package ebilly.admin.server.bo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.files.AppEngineFile;
import com.google.appengine.api.files.FileService;
import com.google.appengine.api.files.FileServiceFactory;
import com.google.appengine.api.files.FileWriteChannel;

import ebilly.admin.server.core.AppLogger;
import ebilly.admin.server.core.CoreUtils;
import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.server.db.ExportRequest;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.AppConfig.EXPORT_STATUS;
import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.viewdef.Filter;
import ebilly.admin.shared.viewdef.FilterCriteria;
import ebilly.admin.shared.viewdef.SearchRequest;
import ebilly.admin.shared.viewdef.SearchResult;

public abstract class BaseSearchHandler {
	
	protected final AppLogger log;
	
	public BaseSearchHandler() {
		 log = AppLogger.getLogger(this.getClass().getCanonicalName());
	}
	
	public SearchResult fetchResults(
			SearchRequest request) throws Exception {
		List<Object> queryParams = new ArrayList<Object>();
		Query query = prepareQuery(request, queryParams);
		try {
			Object results = 
				query.executeWithArray(queryParams.toArray());
			//if (results.isEmpty()) new ArrayList<ResultRecord>();
			//results = (List<Student>) pm.detachCopyAll(results);
			return prepareResults(request,results);
		} finally {
			query.closeAll();
		}
	}

	private Query prepareQuery(SearchRequest request, List<Object> queryParams) {
		preProcessRequest(request);
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		StringBuffer q = new StringBuffer();
		
		String entityName = (String)getSearchEntityName();
		q.append("select from ").append(entityName);
		if (!ebilly.admin.server.db.DevicePr.class.getName().equals(entityName)) {
			q.append(" where active == 1 ");
		}
		
		prepareSearchFilter(request,queryParams,q);
		
		log.debug("Generated Query: "+q.toString());
		log.debug("Query Params: "+Arrays.toString(queryParams.toArray()));
		
		Query query = pm.newQuery(q.toString());
		if (request.getFetchSize() > 0) {
			query.setRange(request.getPageNumber(), request.getFetchSize());
		}
		setOrderClause(query, request);
		return query;
	}

	protected void preProcessRequest(SearchRequest request) {
		
	}

	protected void setOrderClause(Query query, SearchRequest request) {
		return;
	}

	protected abstract SearchResult prepareResults(SearchRequest request, Object results);

	protected void prepareSearchFilter(SearchRequest request, 
			List<Object> queryParams, StringBuffer query) {
		boolean isWhereClauseFirstFilter = false;
		if (!query.toString().toUpperCase().contains("WHERE")) {
			query.append(" where ");
			isWhereClauseFirstFilter = true;
		}
		StringBuffer paramsClause = new StringBuffer(" parameters ");
		FilterCriteria filterCriteria = request.getFilterCriteria();
		Map<String, Class> fieldDataTypeMap = getEntityFieldDataMap();
		Map<String, Integer> paramNameCountMap = new HashMap<String, Integer>(); 
		for (Filter filter : filterCriteria.getFilters()) {
			String field = filter.getFieldName();
			String fieldValue = filter.getValue();
			if (CommonUtil.isEmpty(fieldValue)) continue;
			Class fieldType = fieldDataTypeMap.get(field);
			if (fieldType == null) throw new IllegalArgumentException("Invalid field specified, unknown type. Field: "+field);
			Object typedValue = CoreUtils.convertToTypedValue(fieldValue, fieldType);
			if (typedValue == null) throw new IllegalArgumentException("Field value could not be converted to its type, field: "+field+", value: "+fieldValue+", fieldType: "+fieldType);
			
			if (queryParams.size() > 0) paramsClause.append(", ");
			
			// Parameter name unique name generation
			String uniqueName = "0";
			Integer paramNameCount = paramNameCountMap.get(field);
			if (paramNameCount != null) {
				uniqueName = paramNameCount.intValue()+"";
				paramNameCountMap.put(field, Integer.valueOf(paramNameCount + 1));
			} else {
				paramNameCountMap.put(field, 1);
			}
			// End of Parameter name unique name generation
			
			String paramName = "p_"+field+uniqueName;
			if (!isWhereClauseFirstFilter) {
				query.append(" ").append(filter.getConjunction()).append(" ");
			}
			query.append(field)
			     .append(" ").append(filter.getOperator()).append(" ")
			     .append(paramName);
			queryParams.add(typedValue);
			paramsClause.append(fieldType.getName()).append(" ").append(paramName);
			isWhereClauseFirstFilter = false;
		}
		onBeforeAppendingParamsClause(query,queryParams,paramsClause);
		if (queryParams.size() > 0) query.append(paramsClause);
	}

	// Override this method to add custom query preperation
	// eg: range filteration, date > x and date < y
	protected void onBeforeAppendingParamsClause(StringBuffer query, 
			List<Object> queryParams, StringBuffer paramsClause) {
		
	}

	/**
	 * @return returns field data map of each entity participating in search
	 */
	protected abstract Map<String, Class> getEntityFieldDataMap();
	
	/**
	 * 
	 * @return comma seperated list of entity names
	 */
	protected abstract Object getSearchEntityName();

	public static BaseSearchHandler getHandler(String objectName) {
		BaseSearchHandler h = null;
		if (AppConfig.OBJ_USER.equals(objectName)) {
			h = new UserSearchHandler();
		} else if (AppConfig.OBJ_IMPORT_REQUEST.equals(objectName)) {
			h = new ImportRequestSearchHandler();
		} else if (AppConfig.OBJ_DEVICE.equals(objectName)) {
			h = new DeviceSearchHandler();
		} else if (AppConfig.OBJ_PR_LIVE_REPORT.equals(objectName)) {
			h = new LivePrSearchHandler();
		} else if (AppConfig.OBJ_PR_AGGREGATE_LIVE_REPORT.equals(objectName)) {
			h = new LivePrAggregateSearchHandler();
		} else if (AppConfig.OBJ_PR_REPORT.equals(objectName)) {
			h = new PrSearchHandler();
		} else if (AppConfig.OBJ_PR_HEADS_REPORT.equals(objectName)) {
			h = new PrHeadsSearchHandler();
		} else if (AppConfig.OBJ_PR_AGGREGATE_REPORT.equals(objectName)) {
			h = new PrAggregateSearchHandler();
		} else if (AppConfig.OBJ_EXPORT_REQUEST.equals(objectName)) {
			h = new ExportRequestSearchHandler();
		} else if (AppConfig.OBJ_MONTH_SUMMARY.equals(objectName)) {
			h = new MonthPrSummarySearchHandler();
		}
		return h;
	}

	private void processExportResult(ExportRequest request) {
		List<Object> queryParams = new ArrayList<Object>();
		Query query = null;
		try {
			request = ExportRequest.updateRequestStateToInprogress(request.getId());
			//if (request != null) return;
			SearchRequest searchRequest = prepareSearchRequest(request);
			searchRequest.setFetchSize(-1);
			query = prepareQuery(searchRequest, queryParams);
			
			Object results = 
				query.executeWithArray(queryParams.toArray());
			request = prepareExportResult(request, results);
			request = ExportRequest.updateRequestStateToCompleted(request.getId());
		} catch (Throwable t) {
			String msg = "Failed to export, reason: "+t.getMessage();
			Throwable cause = t.getCause();
			if (cause != null) {
				msg = msg + ", "+cause.getMessage();
			}
			log.info(msg, t);
			request = ExportRequest.updateRequestStateToFailed(request.getId(),msg);
		} finally {
			if (query != null) query.closeAll();
		}
	}
	
	protected SearchRequest prepareSearchRequest(ExportRequest request) {
		SearchRequest sr = new SearchRequest();
		for (ebilly.admin.server.db.Filter e : request.fetchFilters()) {
			sr.addSearchCriteria(e.getFieldName(),e.getValue(),e.getConjunction(),e.getOperator());
		}
		sr.setFetchSize(-1);
		return sr;
	}

	private ExportRequest prepareExportResult(ExportRequest exportRequest, 
			Object results) throws Exception {
		/*List<ebilly.admin.server.db.DevicePr> devicePrs = 
			(List<ebilly.admin.server.db.DevicePr>) results;*/
		
		String fileName = getExportFileName(exportRequest);
		FileService fileService = FileServiceFactory.getFileService();
		FileWriteChannel writeChannel = null;
		AppEngineFile file = null;
		if (results instanceof List) {
			List<Object> l = (List<Object>)results;
			if (l.size() > 0) {
				file = fileService.createNewBlobFile(getExportFileContentType(), fileName);
				writeChannel = fileService.openWriteChannel(file, true/*lock*/);
			}
		}
		try {
			writeResults(results,writeChannel, fileName, exportRequest);
		} finally {
			if (writeChannel != null) writeChannel.closeFinally();
		}
		if (file != null) {
			BlobKey bk = fileService.getBlobKey(file);
			log.info("File fullPath: "+file.getFullPath()+", fileNamePart: "+file.getNamePart()+
					" fileSystem: "+file.getFileSystem().getName());
			exportRequest.setResultFileKey(bk.getKeyString());
		}
		exportRequest.save();
		return exportRequest;
	}
	
	protected String getExportFileName(ExportRequest exportRequest) {
		return exportRequest.prepareFileName() + ".txt";
	}

	protected String getExportFileContentType() {
		return "text/plain";
	}

	/**
	 * Write results into writeChannel.
	 * 
	 * If results are empty, then writeChannel will be null.
	 * 
	 * @param results
	 * @param writeChannel
	 * @param fileName
	 * @param request
	 * @throws Exception
	 */
	protected void writeResults(Object results, FileWriteChannel writeChannel, String fileName, ExportRequest request) throws Exception{
		throw new IllegalArgumentException("Implementation pending...");
	}

	public static void handleExportRequest(ExportRequest er) {
		if (!er.getStatus().equals(EXPORT_STATUS.NEW)) {
			AppLogger.getLogger("BaseSearchHandler").
				info("ExportRequest record is not in NEW status, " +
					"erId: "+er.getId()+", status: "+er.getStatus());
			return;
		}
		
		/*ExportRequest savedEr = ExportRequest.fetchExportRequestById(er.getId()); 
			ExportRequest.updateRequestStateToInprogress(er.getId());*/
		
		BaseSearchHandler h = getHandler(er.getObjectName());
		h.processExportResult(er);
		
		/*Device d = Device.fetchDeviceByName("FROM-BACKEND-1359883902671");
		d.setDeviceIdentifier(System.currentTimeMillis()+"");
		d.save();*/
	}
}
