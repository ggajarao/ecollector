package ebilly.admin.server.bo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.jdo.Query;

import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.shared.viewdef.Filter;
import ebilly.admin.shared.viewdef.ResultField;
import ebilly.admin.shared.viewdef.ResultRecord;
import ebilly.admin.shared.viewdef.SearchRequest;
import ebilly.admin.shared.viewdef.SearchResult;

public class DeviceSearchHandler extends BaseSearchHandler {
	
	public DeviceSearchHandler() {
		
	}

	protected Map<String, Class> getEntityFieldDataMap() {
		return DBUtils.fieldDataTypeMap(ebilly.admin.server.db.Device.class);
	}
	
	protected void preProcessRequest(SearchRequest request) {
		Filter deviecNameSearchableFilter = 
			request.getFilterCriteria().get("deviceNameSearchable");
		String v = deviecNameSearchableFilter.getValue();
		if (v != null) {
			deviecNameSearchableFilter.setValue(v.toUpperCase());
		}
	}

	/**
	 * 
	 * @return comma seperated list of entity names
	 */
	protected String getSearchEntityName() {
		return ebilly.admin.server.db.Device.class.getName();
	}
	
	protected void setOrderClause(Query query, SearchRequest request) {
		query.setOrdering(" lastModifiedDate descending ");
	}

	@Override
	protected SearchResult prepareResults(SearchRequest request, Object results) {
		List<ebilly.admin.server.db.Device> qResults = 
			(List<ebilly.admin.server.db.Device>) results;
		Map<String, Class> fieldDataTypeMap = 
			DBUtils.fieldDataTypeMap(ebilly.admin.server.db.ImportRequest.class);
		List<ResultRecord> records = new ArrayList<ResultRecord>();
		for (ebilly.admin.server.db.Device importRequest : qResults) {
			ResultRecord record = new ResultRecord();
			Map<String, String> fieldValueMap = 
				DBUtils.toFieldStringValueMap(importRequest, fieldDataTypeMap);
			
			for (ResultField resultColumn : request.getResultFields()) {
				String value = "";
				value = fieldValueMap.get(resultColumn.getFieldName()) + "";
				/*if (resultColumn.getFieldName().equalsIgnoreCase("id")) {
					//
				}*/
				record.fieldValues.add(value);
			}
			records.add(record);
		}
		return new SearchResult(records);
	}
}

