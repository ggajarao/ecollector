package ebilly.admin.server.bo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import ebilly.admin.server.core.CoreUtils;
import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.server.db.User;
import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.viewdef.CrudRequest;
import ebilly.admin.shared.viewdef.CrudResponse;

public class UserCrudHandler extends CrudHandler {

	public UserCrudHandler(CrudRequest request) {
		super(request);
	}

	@Override
	protected CrudResponse processCreateUpdate(CrudRequest request) {
		Map<String, String> fieldValueMap = request.getFieldValueMap();
		String id = fieldValueMap.get("id");
		String loginId = fieldValueMap.get("loginId");
		
		CrudResponse response = new CrudResponse();
		User user = null;
		if (CommonUtil.isEmpty(id)) {
			user = new User();
		} else {
			user = User.fetchUserById(id);
			if (user == null) {
				response.errorMessage = "User not found";
				return response;
			}
		}
		
		if (!CommonUtil.isEmpty(loginId)) {
			User existingUser = User.fetchUserByLoginId(loginId);
			if (existingUser != null 
			    && !CommonUtil.isEmpty(user.getId())
			    && !existingUser.getId().equals(user.getId())) {
				response.errorMessage = "User Login Id is already used by an existing User";
				return response;
			}
		}
		
		List<String> fieldNames = new ArrayList<String>();
		List<String> values = new ArrayList<String>();
		Map<String, Class> fieldTypeMap = 
			DBUtils.fieldDataTypeMap(User.class);
		for (Entry<String, String> entry : fieldValueMap.entrySet()) {
			fieldNames.add(entry.getKey());
			values.add(entry.getValue());
		}
		Map<String, Object> fieldTypedValuemap = CoreUtils.toFieldTypedValuemap(fieldNames.toArray(new String[]{}), 
				values.toArray(new String[]{}), fieldTypeMap);
		
		DBUtils.copyPropertiesFromMap(user, fieldTypedValuemap);
		user.save();
		response.successMessage = "User saved Successfully";
		return response;
	}

	@Override
	protected CrudResponse processRetrieve(CrudRequest request) {
		Map<String, String> fieldValueMap = request.getFieldValueMap();
		String id = fieldValueMap.get("id");
		User user = User.fetchUserById(id);
		
		CrudResponse response = new CrudResponse();
		if (user == null) {
			response.errorMessage = "User not found";
			return response;
		}
		response.fieldValueMap = DBUtils.toFieldStringValueMap(user, 
				DBUtils.fieldDataTypeMap(User.class));
		return response;
	}
}
