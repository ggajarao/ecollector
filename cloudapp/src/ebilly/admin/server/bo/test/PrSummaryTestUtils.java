package ebilly.admin.server.bo.test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import junit.framework.TestCase;
import ebilly.admin.server.core.AppLogger;
import ebilly.admin.server.bo.PrBo;
import ebilly.admin.server.db.DevicePr;
import ebilly.admin.server.db.OrgCircle;
import ebilly.admin.server.db.OrgCompany;
import ebilly.admin.server.db.OrgDistribution;
import ebilly.admin.server.db.OrgDivision;
import ebilly.admin.server.db.OrgEro;
import ebilly.admin.server.db.OrgSection;
import ebilly.admin.server.db.OrgSubdivision;
import ebilly.admin.server.db.PrSummaryData;
import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.orgstruct.OrgCompanyUi;
import ebilly.admin.shared.viewdef.ResultRecord;
import ebilly.admin.shared.viewdef.SearchRequest;
import ecollector.common.ReportAbstractColumns;

public class PrSummaryTestUtils {
	
	private static final AppLogger log = AppLogger.getLogger("PrSummaryTestUtils");

	public static void clearExistingAggs() {
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 0;
				NUM_DIVISION = 0;
				NUM_ERO = 0;
				NUM_SUB_DIV = 0;
				NUM_SECTION = 0;
				NUM_DIST = 0; // 2 digit
				NUM_SC = 0;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		PrSummaryTestUtils.setupPrs(psd1);
		PrBo.summarizePrs();
		PrSummaryData oldMonthPrSummary = 
			PrSummaryData.createOrFetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		oldMonthPrSummary.setActive(0);
		oldMonthPrSummary.save();
		// ensure that we are starting afresh instead of summarizing on exiting data.
		PrSummaryData newMonthPrSummary = 
			PrSummaryData.createOrFetchPrSummaryByMonth(
					PrSummaryData.createMonthKey(Calendar.getInstance()));
		TestCase.assertTrue("Could not clear existing summary data", 
				!oldMonthPrSummary.getId().equals(newMonthPrSummary.getId()));
	}
	
	public static void setupPrs(PrSetupData psd) {
		final PsdIterator iterator = new PsdIterator(psd);
		iterator.start(new PrItemCallback() {
			public void iteratingOver(DevicePr devicePr, int index) {
				devicePr.save();
				if (index % 1000 == 0) {
					log("Completed inserting "+index+" of "+iterator.size()+", new Id: "+devicePr.getId()+", uscNo: "+devicePr.getUSC_NO());
					log("Summarizing PRs.");
					PrBo.summarizePrs();
					log("\n\nDone Summarizing PRs.\n\n");				
				}	
			}
		});
		log("Summarizing PRs.");
		PrBo.summarizePrs();
	}
	
	public static interface PrItemCallback {
		void iteratingOver(DevicePr devicePr, int index); 
	}
	
	public static class PsdIterator {
		private PrSetupData psd = null;
		
		//int NUM_COMPANY = psd.NUM_COMPANY;
		int NUM_CIRCLE;
		int NUM_DIVISION;
		int NUM_ERO;
		int NUM_SUB_DIV;
		int NUM_SECTION;
		int NUM_DIST; // 2 digit
		int NUM_SC;
		
		long estimatedPrs;
		
		public PsdIterator(PrSetupData psd) {
			this.psd = psd;
			//NUM_COMPANY = psd.NUM_COMPANY;
			NUM_CIRCLE = psd.NUM_CIRCLE;
			NUM_DIVISION = psd.NUM_DIVISION;
			NUM_ERO = psd.NUM_ERO;
			NUM_SUB_DIV = psd.NUM_SUB_DIV;
			NUM_SECTION = psd.NUM_SECTION;
			NUM_DIST = psd.NUM_DIST; // 2 digit
			NUM_SC = psd.NUM_SC;
			
			estimatedPrs = 
				(1+NUM_CIRCLE)*(1+NUM_DIVISION)*
				(1+NUM_ERO)*(1+NUM_SUB_DIV)*
				(1+NUM_SECTION)*(1+NUM_DIST)*(1+NUM_SC);
		}
		
		public long size() {
			return estimatedPrs;
		}
		
		public void start(PrItemCallback pic) {
			log("Starting to iterate over "+estimatedPrs+" PRs");
			//int company = 0;
			int circle = 0;
			int division = 0;
			int ero = 0;
			int sub_div = 0;
			int section = 0;
			int dist = 0;
			int sc = 0;
			long uscNo = 0;
			
			int insertedCount = 0;
			
			//Map<String,String> idUniquenessMap = new HashMap<String,String>();
			//for (; company <= NUM_COMPANY; company++) { circle = 0;
			for (; circle <= NUM_CIRCLE; circle++) { division = 0;
			for (; division <= NUM_DIVISION; division++) { ero = 0;
			for (; ero <= NUM_ERO; ero++) { sub_div = 0;
			for (; sub_div <= NUM_SUB_DIV; sub_div++) { section = 0;
			for (; section <= NUM_SECTION; section++) { dist = 0;
			for (; dist <= NUM_DIST; dist++) { sc = 0;
			for (; sc <= NUM_SC; sc++) {
				// COMPUTE USC_NO
				uscNo = 
					(1+circle)  *1000000000000L +
				    (1+division)* 100000000000L +
				    (1+ero)     *  10000000000L +
				    (1+sub_div) *   1000000000L +
				    (1+section) *    100000000L +
				       (1+dist) *      1000000L +
				            sc;
				
				insertedCount++;
				if (insertedCount >= 43000) {
					log("skipping uscNo: "+uscNo);
					return;
				}
				
				DevicePr devicePr = newInmemoryPr(psd, uscNo+"");
				pic.iteratingOver(devicePr,insertedCount);
			}}}}}}}
			
			//log("Number of ids uniquely found: "+idUniquenessMap.size());
		}
	}
	
	public static DevicePr createPr(PrSetupData psd, String uscNo) {
		DevicePr devicePr = newInmemoryPr(psd, uscNo);
		devicePr.save();		
		return devicePr;
	}
	
	public static DevicePr newInmemoryPr(PrSetupData psd, String uscNo) {
		String collectionDate = psd.collectionDate;
		String collectionTime = psd.collectionTime;
		
		DevicePr devicePr = new DevicePr();
		devicePr.setARREARS_N_DEMAND(psd.and+"");
		devicePr.setRC_COLLECTED(psd.rc+"");
		devicePr.setACD_COLLECTED(psd.acd+"");
		devicePr.setAGL_AMOUNT(psd.agl+"");
		
		devicePr.setCOLLECTION_DATE(collectionDate);
		devicePr.setCOLLECTION_TIME(collectionTime);
		//log("UscNum["+uscNo+"]");
		devicePr.setUSC_NO(uscNo);
		devicePr.set_DEVICE_PR_CODE(psd.deviceName+"_"+uscNo);
		devicePr.setDeviceName(psd.deviceName);
		devicePr.setSECTION("Section"+devicePr.getSectionCode());
		devicePr.setDISTRIBUTION("Distribution"+devicePr.getDistCode());
		devicePr.setERO("ERO"+devicePr.getEroCode());
		return devicePr;
	}
	
	public static void log(String msg) {
		System.out.println(msg);
	}

	public static void assertRecordHeads(SearchRequest searchRequest, String dimName, PrHead prHead, ResultRecord record) {
		String and = searchRequest.getValue(ReportAbstractColumns.ARREARS_N_DEMAND, record);
		TestCase.assertTrue(dimName+" dimension, value 'Arrears and Demand' missmatch. Expected: "+prHead.and+", got: "+and,
				and.equals(prHead.and+""));
		String acd = searchRequest.getValue(ReportAbstractColumns.ACD_COLLECTED, record);
		TestCase.assertTrue(dimName+" dimension, value ACD missmatch. Expected: "+prHead.acd+", got: "+acd,
				acd.equals(prHead.acd+""));
		String agl = searchRequest.getValue(ReportAbstractColumns.AGL_AMOUNT, record);
		TestCase.assertTrue(dimName+" dimension, value AGL missmatch. Expected: "+prHead.agl+", got: "+agl,
				agl.equals(prHead.agl+""));
		String rc = searchRequest.getValue(ReportAbstractColumns.RC_COLLECTED, record);
		TestCase.assertTrue(dimName+" dimension, value RC missmatch. Expected: "+prHead.rc+", got: "+rc,
				rc.equals(prHead.rc+""));
		String grandTotal = searchRequest.getValue(ReportAbstractColumns.GRAND_TOTAL, record);
		TestCase.assertTrue(dimName+" dimension, vale Grand total mismatch. Expected: "+prHead.grandTotal+", got: "+grandTotal,
				grandTotal.equals(prHead.grandTotal+""));
	}
	
	public static void createSamplePrs() {
		AppLogger.getLogger("PrSummaryTestUtils").info("CREATING SAMPLE PRs");
		PrSetupData psd1 = new PrSetupData() {
			public void init() {
				NUM_COMPANY = 0;
				NUM_CIRCLE = 6;
				NUM_DIVISION = 3;
				NUM_ERO = 3;
				NUM_SUB_DIV = 3;
				NUM_SECTION = 3;
				NUM_DIST = 3; // 2 digit
				NUM_SC = 0;
				
				and = 230;
				rc = 75;
				acd = 150;
				agl = 30;
			}
		};
		
		Calendar cal = Calendar.getInstance();
		for (int i=0; i < 1; i++) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
			//psd1.deviceName = "device-"+System.currentTimeMillis();
			psd1.collectionDate = PrSetupData.getDateInDevicePrFormat(cal);
			PrSummaryTestUtils.setupOrgData(psd1);
			PrSummaryTestUtils.setupPrs(psd1);			
		}
		AppLogger.getLogger("PrSummaryTestUtils").info("COMPLETED CREATING SAMPLE PRs");
	}
	
	public static void setupOrgData(PrSetupData psd) {
		final PsdIterator iterator = new PsdIterator(psd);
		final List<DevicePr> devicePrs = new ArrayList<DevicePr>();
		iterator.start(new PrItemCallback() {
			public void iteratingOver(DevicePr devicePr, int index) {
				log.info("Creating Org Structure for "+ devicePr.getUSC_NO()+", iteraton at : "+index);
				devicePrs.add(devicePr);
				if (index % 1000 == 0) {
					PrSummaryTestUtils.recordOrgStructure(devicePrs);
					devicePrs.clear();
				}	
			}
		});
		PrSummaryTestUtils.recordOrgStructure(devicePrs);
	}
	
	public static void recordOrgStructure(List<DevicePr> devicePrs) {
		String companyCode = "0";
		
		OrgCompany orgCompany = OrgCompany.fetchOrgCompany();
		if (orgCompany == null) {
			orgCompany = new OrgCompany();
			orgCompany.setOcmpCode(companyCode);
			orgCompany.setOcmpName("HQ");
		}
		
		for (DevicePr devicePr : devicePrs) {
			log.info("Creating org structure for UscNo: "+devicePr.getUSC_NO());
			String circleCode = devicePr.getCircleCode();
			String divisionCode = devicePr.getDivisionCode();
			String eroCode = devicePr.getEroCode();
			String subdivisionCode = devicePr.getSubDivisionCode();
			String sectionCode = devicePr.getSectionCode();
			String distCode = devicePr.getDistCode();
			
			String eroName = devicePr.getERO();
			String sectionName = devicePr.getSECTION();
			String distributionName = devicePr.getDISTRIBUTION();
			
			OrgCircle orgCircle = orgCompany.findCircle(circleCode);
			if (orgCircle == null) {
				orgCircle = new OrgCircle();
				orgCircle.setOcrcCode(circleCode);
				orgCircle.setOcrcName("Circle"+circleCode);
				orgCompany.addOcmpCircle(orgCircle);
			}
			OrgDivision orgDivision = orgCircle.findOrgDivision(divisionCode);
			if (orgDivision == null) {
				orgDivision = new OrgDivision();
				orgDivision.setOdvsCode(divisionCode);
				orgDivision.setOdvsName("Division"+divisionCode);
				orgCircle.addOrgDivision(orgDivision);
			}
			
			OrgEro orgEro = orgDivision.findOrgEro(eroCode);
			if (orgEro == null) {
				orgEro = new OrgEro();
				orgEro.setOeroCode(eroCode);
				orgEro.setOeroName(eroName);
				orgDivision.addOrgEro(orgEro);
			}
			
			OrgSubdivision orgSubdivision = orgEro.findOrgSubdivision(subdivisionCode);
			if (orgSubdivision == null) {
				orgSubdivision = new OrgSubdivision();
				orgSubdivision.setOsbdCode(subdivisionCode);
				orgSubdivision.setOsbdName("Subdivision"+subdivisionCode);
				orgEro.addOrgSubdivision(orgSubdivision);
			}
			
			OrgSection orgSection = orgSubdivision.findOrgSection(sectionCode);
			if (orgSection == null) {
				orgSection = new OrgSection();
				orgSection.setOsctCode(sectionCode);
				orgSection.setOsctName(sectionName);
				orgSubdivision.addOrgSection(orgSection);
			}
			
			OrgDistribution orgDistribution = orgSection.findOrgDistribution(distCode);
			if (orgDistribution == null) {
				orgDistribution = new OrgDistribution();
				orgDistribution.setOdstCode(distCode);
				orgDistribution.setOdstName(distributionName);
				orgSection.addOrgDistribution(orgDistribution);
			}
		}
		orgCompany.save();
	}
}
