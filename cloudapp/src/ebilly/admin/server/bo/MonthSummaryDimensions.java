package ebilly.admin.server.bo;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import ebilly.admin.server.core.reports.ResultAggregator;
import ebilly.admin.server.core.reports.ResultAggregatorFactory;
import ebilly.admin.server.core.reports.ResultAggregatorJacksonFactory;
import ebilly.admin.server.db.DevicePr;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.viewdef.ResultField;
import ebilly.admin.shared.viewdef.ResultRecord;
import ebilly.admin.shared.viewdef.SearchRequest;
import ebilly.admin.shared.viewdef.SearchResult;
import ecollector.common.PrColumns;

class MonthSummaryDimensions extends SummaryDimensions {
	private static final String VERSION = "3.0";
	private String loadedVersion;
	
	private ResultAggregator mDimDistribution = null;
	private ResultAggregator mDimDistributionDate = null;
	private ResultAggregator mDimSection = null;
	private ResultAggregator mDimSectionDate = null;
	private ResultAggregator mDimSubdivision = null;
	private ResultAggregator mDimSubdivisionDate = null;
	private ResultAggregator mDimEro = null;
	private ResultAggregator mDimEroDate = null;
	private ResultAggregator mDimDivision = null;
	private ResultAggregator mDimDivisionDate = null;
	private ResultAggregator mDimCircle = null;
	private ResultAggregator mDimCircleDate = null;
	private ResultAggregator mDimCompany = null;
	private ResultAggregator mDimCompanyDate = null;
	private ResultAggregator mDimDeviceDate = null;
	private ResultAggregator mDimDevice = null;
	private ResultAggregator mDimDeviceDateSection = null;
	private ResultAggregator mDimDeviceDateSectionDist = null;
	
	public MonthSummaryDimensions(InputStream jsonStream) 
	throws JsonParseException, IOException {
		JsonFactory j = new JsonFactory();
		org.codehaus.jackson.JsonParser jp = null;
		if (jsonStream != null) {
			jp = j.createJsonParser(jsonStream);
			loadedVersion = ResultAggregatorJacksonFactory.jsonAsVersionString(jp,M_VERSION);
		}
		
		if (isVersionChanged()) {
			// Do not load from existing summary data but to 
			// start a new summary calculation starting for 
			// this new version
			jp = null;
		}
		
		mDimDistribution = super.loadDimDistribution(jp);
		mDimDistributionDate = super.loadDimDistributionDate(jp);
		mDimSection = super.loadDimSection(jp);
		mDimSectionDate = super.loadDimSectionDate(jp);
		mDimSubdivision = super.loadDimSubdivision(jp);
		mDimSubdivisionDate = super.loadDimSubdivisionDate(jp);
		mDimEro = super.loadDimEro(jp);
		mDimEroDate = super.loadDimEroDate(jp);
		mDimDivision = super.loadDimDivision(jp);
		mDimDivisionDate = super.loadDimDivisionDate(jp);
		mDimCircle = super.loadDimCircle(jp);
		mDimCircleDate = super.loadDimCircleDate(jp);
		mDimCompany = super.loadDimCompany(jp);
		mDimCompanyDate = super.loadDimCompanyDate(jp);
		mDimDeviceDate = super.loadDimDeviceDate(jp);
		mDimDevice = super.loadDimDevice(jp);
		mDimDeviceDateSection = super.loadDimDeviceDateSection(jp);
		mDimDeviceDateSectionDist = super.loadDimDeviceDateSectionDist(jp);
	}
	
	public MonthSummaryDimensions(StringBuffer json) {
		JsonParser parser = new JsonParser();
		JsonObject ojson = null;
		if (json != null && json.length() != 0) {
			TimeLogger tl = TimeLogger.create();
			ojson = (JsonObject) parser.parse(json.toString());
			tl.logElapsed("Pasing json string of size("+json.length()+") took ");
			json.setLength(0); json = null; //PSR TUNED
			loadedVersion = ResultAggregatorFactory.jsonAsString(ojson.getAsJsonPrimitive(M_VERSION));
		}
		
		if (isVersionChanged()) {
			// Do not load from existing summary data but to 
			// start a new summary calculation starting for 
			// this new version
			ojson = null;
		}
		
		mDimDistribution = super.loadDimDistribution(ojson);
		mDimDistributionDate = super.loadDimDistributionDate(ojson);
		mDimSection = super.loadDimSection(ojson);
		mDimSectionDate = super.loadDimSectionDate(ojson);
		mDimSubdivision = super.loadDimSubdivision(ojson);
		mDimSubdivisionDate = super.loadDimSubdivisionDate(ojson);
		mDimEro = super.loadDimEro(ojson);
		mDimEroDate = super.loadDimEroDate(ojson);
		mDimDivision = super.loadDimDivision(ojson);
		mDimDivisionDate = super.loadDimDivisionDate(ojson);
		mDimCircle = super.loadDimCircle(ojson);
		mDimCircleDate = super.loadDimCircleDate(ojson);
		mDimCompany = super.loadDimCompany(ojson);
		mDimCompanyDate = super.loadDimCompanyDate(ojson);
		mDimDeviceDate = super.loadDimDeviceDate(ojson);
		mDimDevice = super.loadDimDevice(ojson);
		mDimDeviceDateSection = super.loadDimDeviceDateSection(ojson);
		mDimDeviceDateSectionDist = super.loadDimDeviceDateSectionDist(ojson);
	}
	
	public boolean isVersionChanged() {
		if (VERSION.equalsIgnoreCase(loadedVersion)) {
			return false;
		} else {
			return true;
		}
	}
	
	public String asJsonString() {
		JsonObject ojson = new JsonObject();
		ojson.add(M_VERSION, 								ResultAggregatorFactory.asJsonObject(VERSION));
		ojson.add(M_DIM_DISTRIBUTION, 						ResultAggregatorFactory.asJsonObjectOnlyAggMap(mDimDistribution));
		ojson.add(M_DIM_DISTRIBUTION_DATE, 					ResultAggregatorFactory.asJsonObjectOnlyAggMap(mDimDistributionDate));
		ojson.add(M_DIM_SECTION, 							ResultAggregatorFactory.asJsonObjectOnlyAggMap(mDimSection));
		ojson.add(M_DIM_SECTION_DATE, 						ResultAggregatorFactory.asJsonObjectOnlyAggMap(mDimSectionDate));
		ojson.add(M_DIM_SUBDIVISION, 						ResultAggregatorFactory.asJsonObjectOnlyAggMap(mDimSubdivision));
		ojson.add(M_DIM_SUBDIVISION_DATE, 					ResultAggregatorFactory.asJsonObjectOnlyAggMap(mDimSubdivisionDate));
		ojson.add(M_DIM_ERO, 								ResultAggregatorFactory.asJsonObjectOnlyAggMap(mDimEro));
		ojson.add(M_DIM_ERO_DATE, 							ResultAggregatorFactory.asJsonObjectOnlyAggMap(mDimEroDate));
		ojson.add(M_DIM_DIVISION, 							ResultAggregatorFactory.asJsonObjectOnlyAggMap(mDimDivision));
		ojson.add(M_DIM_DIVISION_DATE, 						ResultAggregatorFactory.asJsonObjectOnlyAggMap(mDimDivisionDate));
		ojson.add(M_DIM_CIRCLE, 							ResultAggregatorFactory.asJsonObjectOnlyAggMap(mDimCircle));
		ojson.add(M_DIM_CIRCLE_DATE, 						ResultAggregatorFactory.asJsonObjectOnlyAggMap(mDimCircleDate));
		ojson.add(M_DIM_COMPANY, 							ResultAggregatorFactory.asJsonObjectOnlyAggMap(mDimCompany));
		ojson.add(M_DIM_COMPANY_DATE, 						ResultAggregatorFactory.asJsonObjectOnlyAggMap(mDimCompanyDate));
		ojson.add(M_DIM_DEVICE_DATE, 						ResultAggregatorFactory.asJsonObjectOnlyAggMap(mDimDeviceDate));
		ojson.add(M_DIM_DEVICE, 							ResultAggregatorFactory.asJsonObjectOnlyAggMap(mDimDevice));
		ojson.add(M_DIM_DEVICE_DATE_SECTION,				ResultAggregatorFactory.asJsonObjectOnlyAggMap(mDimDeviceDateSection));
		ojson.add(M_DIM_DEVICE_DATE_SECTION_DISTRIBUTION,	ResultAggregatorFactory.asJsonObjectOnlyAggMap(mDimDeviceDateSectionDist));
		return ojson.toString();
	}
	
	public void writeAsJson(OutputStream os) throws IOException {
		JsonFactory f = new JsonFactory();
		JsonGenerator g = f.createJsonGenerator(os);
		
		g.writeStartObject();
		
		g.writeStringField(M_VERSION, VERSION);
		
		g.writeFieldName(M_DIM_DISTRIBUTION);
		ResultAggregatorJacksonFactory.asJsonStream(mDimDistribution, g);
		
		g.writeFieldName(M_DIM_DISTRIBUTION_DATE);
		ResultAggregatorJacksonFactory.asJsonStream(mDimDistributionDate, g);
		g.writeFieldName(M_DIM_SECTION);
		ResultAggregatorJacksonFactory.asJsonStream(mDimSection, g);
		g.writeFieldName(M_DIM_SECTION_DATE);
		ResultAggregatorJacksonFactory.asJsonStream(mDimSectionDate, g);
		g.writeFieldName(M_DIM_SUBDIVISION);
		ResultAggregatorJacksonFactory.asJsonStream(mDimSubdivision, g);
		g.writeFieldName(M_DIM_SUBDIVISION_DATE);
		ResultAggregatorJacksonFactory.asJsonStream(mDimSubdivisionDate, g);
		g.writeFieldName(M_DIM_ERO);
		ResultAggregatorJacksonFactory.asJsonStream(mDimEro, g);
		g.writeFieldName(M_DIM_ERO_DATE);
		ResultAggregatorJacksonFactory.asJsonStream(mDimEroDate, g);
		g.writeFieldName(M_DIM_DIVISION);
		ResultAggregatorJacksonFactory.asJsonStream(mDimDivision, g);
		g.writeFieldName(M_DIM_DIVISION_DATE);
		ResultAggregatorJacksonFactory.asJsonStream(mDimDivisionDate, g);
		g.writeFieldName(M_DIM_CIRCLE);
		ResultAggregatorJacksonFactory.asJsonStream(mDimCircle, g);
		g.writeFieldName(M_DIM_CIRCLE_DATE);
		ResultAggregatorJacksonFactory.asJsonStream(mDimCircleDate, g);
		g.writeFieldName(M_DIM_COMPANY);
		ResultAggregatorJacksonFactory.asJsonStream(mDimCompany, g);
		g.writeFieldName(M_DIM_COMPANY_DATE);
		ResultAggregatorJacksonFactory.asJsonStream(mDimCompanyDate, g);
		g.writeFieldName(M_DIM_DEVICE_DATE);
		ResultAggregatorJacksonFactory.asJsonStream(mDimDeviceDate, g);
		g.writeFieldName(M_DIM_DEVICE);
		ResultAggregatorJacksonFactory.asJsonStream(mDimDevice, g);
		g.writeFieldName(M_DIM_DEVICE_DATE_SECTION);
		ResultAggregatorJacksonFactory.asJsonStream(mDimDeviceDateSection, g);
		g.writeFieldName(M_DIM_DEVICE_DATE_SECTION_DISTRIBUTION);
		ResultAggregatorJacksonFactory.asJsonStream(mDimDeviceDateSectionDist, g);
		g.writeEndObject();
		
		g.close();
	}

	public void aggregate(List<DevicePr> devicePrs) {
		SearchResult prSearchResults = super.asSearchResults(devicePrs);
		mDimDistribution.aggregateRecordsRetainResults(prSearchResults);
		mDimDistributionDate.aggregateRecordsRetainResults(prSearchResults);
		mDimSection.aggregateRecordsRetainResults(prSearchResults);
		mDimSectionDate.aggregateRecordsRetainResults(prSearchResults);
		mDimSubdivision.aggregateRecordsRetainResults(prSearchResults);
		mDimSubdivisionDate.aggregateRecordsRetainResults(prSearchResults);
		mDimEro.aggregateRecordsRetainResults(prSearchResults);
		mDimEroDate.aggregateRecordsRetainResults(prSearchResults);
		mDimDivision.aggregateRecordsRetainResults(prSearchResults);
		mDimDivisionDate.aggregateRecordsRetainResults(prSearchResults);
		mDimCircle.aggregateRecordsRetainResults(prSearchResults);
		mDimCircleDate.aggregateRecordsRetainResults(prSearchResults);
		mDimCompany.aggregateRecordsRetainResults(prSearchResults);
		mDimCompanyDate.aggregateRecordsRetainResults(prSearchResults);
		mDimDeviceDate.aggregateRecordsRetainResults(prSearchResults);
		mDimDevice.aggregateRecordsRetainResults(prSearchResults);
		mDimDeviceDateSection.aggregateRecordsRetainResults(prSearchResults);
		mDimDeviceDateSectionDist.aggregateRecordsRetainResults(prSearchResults);
		updateGrandTotalFields();
	}
	
	private void updateGrandTotalFields() {
		super.computeGrandTotal(mDimDistribution);
		super.computeGrandTotal(mDimDistributionDate);
		super.computeGrandTotal(mDimSection);
		super.computeGrandTotal(mDimSectionDate);
		super.computeGrandTotal(mDimSubdivision);
		super.computeGrandTotal(mDimSubdivisionDate);
		super.computeGrandTotal(mDimEro);
		super.computeGrandTotal(mDimEroDate);
		super.computeGrandTotal(mDimDivision);
		super.computeGrandTotal(mDimDivisionDate);
		super.computeGrandTotal(mDimCircle);
		super.computeGrandTotal(mDimCircleDate);
		super.computeGrandTotal(mDimCompany);
		super.computeGrandTotal(mDimCompanyDate);
		super.computeGrandTotal(mDimDeviceDate);
		super.computeGrandTotal(mDimDevice);
		super.computeGrandTotal(mDimDeviceDateSection);
		super.computeGrandTotal(mDimDeviceDateSectionDist);
	}

	public ResultAggregator getDimDistribution() {
		return mDimDistribution;
	}

	public ResultAggregator getDimDistributionDate() {
		return mDimDistributionDate;
	}

	public ResultAggregator getDimSection() {
		return mDimSection;
	}

	public ResultAggregator getDimSectionDate() {
		return mDimSectionDate;
	}

	public ResultAggregator getDimSubdivision() {
		return mDimSubdivision;
	}

	public ResultAggregator getDimSubdivisionDate() {
		return mDimSubdivisionDate;
	}

	public ResultAggregator getDimEro() {
		return mDimEro;
	}

	public ResultAggregator getDimEroDate() {
		return mDimEroDate;
	}	
	
	public ResultAggregator getDimDivision() {
		return mDimDivision;
	}

	public ResultAggregator getDimDivisionDate() {
		return mDimDivisionDate;
	}

	public ResultAggregator getDimCircle() {
		return mDimCircle;
	}

	public ResultAggregator getDimCircleDate() {
		return mDimCircleDate;
	}

	public ResultAggregator getDimCompany() {
		return mDimCompany;
	}

	public ResultAggregator getDimCompanyDate() {
		return mDimCompanyDate;
	}

	public ResultAggregator getDimDeviceDate() {
		return mDimDeviceDate;
	}

	public ResultAggregator getDimDevice() {
		return mDimDevice;
	}
	
	public ResultAggregator getDimDeviceDateSection() {
		return mDimDeviceDateSection;
	}
	
	public ResultAggregator getDimDeviceDateSectionDist() {
		return mDimDeviceDateSectionDist;
	}

	public String getOldVersionNumber() {
		return loadedVersion;
	}

	public String getNewVersionNumber() {
		return VERSION;
	}

	public SearchResult prepareSearchResults(
			MonthPrSummarySearchFilter summaryFilter) {
		SearchResult searchResult = new SearchResult();
		byte orgType = summaryFilter.getSelectedOrgType();
		String org = summaryFilter.getSelectedOrg();
		switch(orgType) {
			case MonthPrSummarySearchFilter.ORG_COMPANY:
				searchResult = 
					findDimRecords(mDimCompany, mDimCompanyDate, 
							org,PrColumns._COMPANY_CODE);
				break;
			case MonthPrSummarySearchFilter.ORG_CIRCLE:
				searchResult = 
					findDimRecords(mDimCircle, mDimCircleDate, 
							org,PrColumns._CIRCLE_CODE);
				break;
			case MonthPrSummarySearchFilter.ORG_DIVISION:
				searchResult = 
					findDimRecords(mDimDivision, mDimDivisionDate, 
							org,PrColumns._DIVISION_CODE);
				break;
			case MonthPrSummarySearchFilter.ORG_ERO:
				searchResult = 
					findDimRecords(mDimEro, mDimEroDate, 
							org,PrColumns._ERO_CODE);
				break;
			case MonthPrSummarySearchFilter.ORG_SUBDIVISION:
				searchResult = 
					findDimRecords(mDimSubdivision, mDimSubdivisionDate, 
							org,PrColumns._SUBDIVISION_CODE);
				break;
			case MonthPrSummarySearchFilter.ORG_SECTION:
				searchResult = 
					findDimRecords(mDimSection, mDimSectionDate, 
							org,PrColumns._SECTION_CODE);
				break;
			case MonthPrSummarySearchFilter.ORG_DISTRIBUTION:
				searchResult = 
					findDimRecords(mDimDistribution, mDimDistributionDate, 
							org,PrColumns._DIST_CODE);
				break;
			default:
				searchResult.setErrorMessage("Unknown Org");
		}
		return searchResult;
	}

	private SearchResult findDimRecords(ResultAggregator agg,
			ResultAggregator dateAgg, String orgCode, String orgColumn) {
		
		List<ResultRecord> resultRecords = new ArrayList<ResultRecord>();
		addFilteredRecords(resultRecords,
				agg.getAggregateSearchResult(),agg.getSearchRequest(),orgColumn,orgCode);
		
		// Dimensions month record. Replace collection_date field with display name
		// as this will be the first record that is shown.
		if (!resultRecords.isEmpty()) {
			ResultRecord monthRecord = resultRecords.get(0);
			agg.getSearchRequest().setValue(PrColumns.COLLECTION_DATE, AppConfig.PR_SUMMARY_MONTH_AGGREGATE_LABEL, monthRecord);
		}
		addFilteredRecords(resultRecords,
				dateAgg.getAggregateSearchResult(),dateAgg.getSearchRequest(),orgColumn,orgCode);
		
		// Sort on COLLECTION_DATE field in descending order.
		SearchResult sr = new SearchResult(resultRecords);
		sr.sort(new ResultField[]{agg.getSearchRequest().findField(PrColumns.COLLECTION_DATE)}, 
				/*ascending?*/false, 
				agg.getSearchRequest());
		return sr;
	}
	
	private void addFilteredRecords(List<ResultRecord> resultRecords, 
			SearchResult searchResults, SearchRequest searchRequest,
			String filterColumn, String filterValue) {
		for (ResultRecord resultRecord : searchResults.getResults()) {
			String recValue = searchRequest.getValue(filterColumn, resultRecord);
			if (filterValue.equals(recValue)) {
				resultRecords.add(resultRecord);
			}
		}
	}	
}
