package ebilly.admin.server.bo;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import ebilly.admin.server.Util;
import ebilly.admin.server.core.AppLogger;
import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.server.core.reports.ResultAggregatorFactory;
import ebilly.admin.server.db.BatchResult;
import ebilly.admin.server.db.DbAppConfig;
import ebilly.admin.server.db.Device;
import ebilly.admin.server.db.DevicePr;
import ebilly.admin.server.db.OrgCircle;
import ebilly.admin.server.db.OrgCompany;
import ebilly.admin.server.db.OrgDistribution;
import ebilly.admin.server.db.OrgDivision;
import ebilly.admin.server.db.OrgEro;
import ebilly.admin.server.db.OrgSection;
import ebilly.admin.server.db.OrgSubdivision;
import ebilly.admin.server.db.PrSummaryData;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.CollectionDashboardPiggyBack;
import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.DevicePrUi;
import ebilly.admin.shared.PushPrRequest;
import ebilly.admin.shared.orgstruct.OrgCompanyUi;
import ebilly.admin.shared.viewdef.CrudResponse;
import ecollector.common.ServiceColumns;

public class PrBo {
	
	private static final AppLogger log = AppLogger.getLogger(PrBo.class.getName());
	
	public static String importPrsRaw(String serializedServices, String deviceIdentifier) {
		if (serializedServices == null || serializedServices.trim().equals("")
			|| deviceIdentifier == null || deviceIdentifier.trim().equals("")) {
			AppLogger.getLogger("DeviceServiceImport").info(
					"Required params missing, deviceIdentifier: "+deviceIdentifier
					+", payload: ["+serializedServices+"]");
			return "";
		}
		
		String deviceName = null;
		Device d = Device.fetchDeviceByIdentifier(deviceIdentifier);
		deviceName = d.getDeviceName();
		
		if (deviceName == null || deviceName.trim().equals("")) {
			AppLogger.getLogger("DeviceServiceImport").info(
					"Device name got null for deviceIdentifier: "+deviceIdentifier);
			return "";
		}
		
		
		List<String> successServiceIds = new ArrayList<String>();
		String[] serializedServiceRecords = 
			serializedServices.split(ServiceColumns.C_RECORD_SEPERATOR);
		for (String serializedServiceRecord : serializedServiceRecords) {
			if (serializedServiceRecord == null ||
				serializedServiceRecord.trim().equals("")) {
				continue;
			}
			
			String[] serviceFieldValues = 
				serializedServiceRecord.split(ServiceColumns.C_FIELD_SEPERATOR);
			if (serviceFieldValues == null || 
				serviceFieldValues.length != ServiceColumns.PUSH_PULL_COLUMN_ORDER.size()) {
				AppLogger.getLogger("DeviceServiceImport").info(
						"Invalid service record format, record: "
						+serializedServiceRecord);
				continue;
			}
			
			Map<String,String> fieldValueMap = new HashMap<String,String>();
			for (int i = 0; i < ServiceColumns.PUSH_PULL_COLUMN_ORDER.size(); i++) {
				fieldValueMap.put(
						ServiceColumns.PUSH_PULL_COLUMN_ORDER.get(i),
						serviceFieldValues[i]);
			}
			
			try {
				DevicePr s = new DevicePr();
				s.setDeviceName(deviceName);
				DBUtils.copyPropertiesFromMap(s, fieldValueMap);
				s.save();
				successServiceIds.add(s.getRECEIPT_NUMBER());
			} catch(Exception e) {
				AppLogger.getLogger("DeviceServiceImport").info(
						"Error while saving Device service: "
						+serializedServiceRecord,e);
			}
		}
		
		// convert success service ids as csv string
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < successServiceIds.size(); i++) {
			String serviceId = successServiceIds.get(i);
			if (serviceId == null) continue;
			sb.append(serviceId);
			if (i < successServiceIds.size() - 1) {
				sb.append(",");
			}
		}
		return sb.toString();
	}

	public static CrudResponse importPrs(PushPrRequest request) {
		CrudResponse r = new CrudResponse();
		if (request == null) {
			r.errorMessage = "Bad request 1";
			return r;
		}
		
		if (CommonUtil.isEmpty(request.deviceIdentifier)) {
			r.errorMessage = "Bad request 2";
			return r;
		}

		List<DevicePrUi> devicePrs = request.devicePrs;
		if (devicePrs == null || devicePrs.size() == 0) {
			r.errorMessage = "Bad request 4";
			return r;
		}
		
		Device d = Device.fetchDeviceByIdentifier(request.deviceIdentifier);
		if (d == null) {
			r.errorMessage = "Bad request 3";
			return r;
		}
		
		List<String> successRecords = new ArrayList<String>();
		List<Object> prCodes = new ArrayList<Object>();
		for (DevicePrUi dpUi : devicePrs) {
			try {
				DevicePr dp = new DevicePr();
				DBUtils.copyPropertiesV2(dpUi, dp);
				dp.setDeviceName(d.getDeviceName());
				prCodes.add(dp.get_DEVICE_PR_CODE());
				dp.save();
				successRecords.add(dpUi.get_DEVICE_PR_CODE());
			} catch (Exception e) {
				AppLogger.getLogger("PrBo").info(
						"Error while saving DevicePr: "+dpUi.toString(),e);
			}
		}
		Util.log(prCodes, "Pushed Pr codes: ", 50);
		
		StringBuffer s = new StringBuffer();
		for(int i=0; i<successRecords.size(); i++) {
			s.append(successRecords.get(i));
			if (i < successRecords.size() - 1) {
				s.append(",");
			}
		}
		r.successMessage = s.toString();
		return r;
	}
	
	public static MonthsPrSummary fetchPrSummaryByMonth(String summaryMonth) throws Exception {
		PrSummaryData monthSummaryData = PrSummaryData.fetchPrSummaryByMonth(summaryMonth);
		return new MonthsPrSummary(monthSummaryData);
	}
	
	public static CrudResponse summarizePrs() {
		
		CrudResponse response = new CrudResponse();
		
		// fetch last computed datetime as lastSummarizedDatetime
		// fetch prs pushed since lastSummarizedDatetime as prsToSummarize
		// Identify months, years from the prsToSummarize
		// for each month in months aggregate corresponding prs into corresponding 
		//    month's PrSummaryData and save PrSummaryData
		// for each year in years aggregate corresponding prs into corresponding
		//    year's PrSummaryData and save PrSummaryData
		
		PrSummarizerContext summarizeCtxt = new PrSummarizerContext();
		try {
			PrBo.summarizePrs(summarizeCtxt);
		} catch (Throwable e) {
			response.errorMessage = e.getMessage();
		} finally {
			summarizeCtxt.markCompleted();
		}
		
		return response;
		
	}
	
	static class DevicePrsByKey {
		private Map<String, List<DevicePr>> prsByKey = null;
		public DevicePrsByKey() {
			prsByKey = new HashMap<String, List<DevicePr>>();
		}
		public List<DevicePr> getPrs(String key) {
			List<DevicePr> list = prsByKey.get(key);
			if (list == null) {
				list = new ArrayList<DevicePr>();
				prsByKey.put(key, list);
			}
			return list;
		}
		
		public Set<String> getKeys() {
			return prsByKey.keySet();
		}
		
		public void put(String key, DevicePr pr) {
			this.getPrs(key).add(pr);
		}
	}
	
	// TODO: Optimizations during large data set fetch 
	// http://stackoverflow.com/questions/7198738/for-google-app-engine-java-how-do-i-set-and-use-chunk-size-in-fetchoptions
	private static void summarizePrs(PrSummarizerContext summarizerCtxt)
	throws Throwable {
		log.info("Querying if there are PRs created after : "+
				summarizerCtxt.getAfterDate()+" ... ");
		boolean hasMore = summarizerCtxt.hasMore();
		if (!hasMore) {
			log.info("No PRs found created after : "+summarizerCtxt.getAfterDate());
			return;
		}
	    while (hasMore) {
	    	List<DevicePr> devicePrs = summarizerCtxt.getDevicePrs();
	    	log.info("PRs records found, count: "+devicePrs.size());
			summarizePrBatch(devicePrs,summarizerCtxt);
			hasMore = summarizerCtxt.hasMore();
			log.info("Querying if there exists more PRs...");
			if (!hasMore) {
				log.info("No more PRs found");
			}
		}
	}
	
	// Older version.
	/*private static void summarizePrs(PrSummarizerContext summarizerCtxt) {
		List<DevicePr> devicePrs = summarizerCtxt.getDevicePrs();
		summarizePrBatch(devicePrs,summarizerCtxt);
	}*/
	
	private static void summarizePrBatch(List<DevicePr> devicePrs, PrSummarizerContext summarizerCtxt)
	throws Throwable {
		if (devicePrs.isEmpty()) return;
		DevicePrsByKey yearPrs = new DevicePrsByKey();
		DevicePrsByKey monthPrs = new DevicePrsByKey();
		Calendar cal = Calendar.getInstance();
		List<Object> prs = new ArrayList<Object>();
		String monthKey = null;
		for (DevicePr pr : devicePrs) {
			cal.setTime(pr.getCollectionDate());
			yearPrs.put(PrSummaryData.createYearKey(cal), pr);
			monthKey = PrSummaryData.createMonthKey(cal);
			monthPrs.put(monthKey, pr);
			prs.add("Id:" + pr.getId() + "-Mon:" + monthKey + "-Usc:"+pr.getUSC_NO());
		}
		Util.log(prs, "Device Prs in this batch: ", 25);
		
		Map<String, PrSummaryData> monthWiseSummaryMap = 
				new HashMap<String, PrSummaryData>();
		Set<String> months = monthPrs.getKeys();
		PrSummaryData monthWisePrSummary = null;
		try {
			for (String month : months) {
				PrSummaryData monthPrSummaryData = 
					PrSummaryData.createOrFetchPrSummaryByMonth(month);
				//PrBo.summarizeMonthPrs(monthPrSummaryData,monthPrs.getPrs(month));
				TimeLogger tl = TimeLogger.create();
				MonthsPrSummary monthsSummary = new MonthsPrSummary(monthPrSummaryData);
				tl.logElapsed("Instantiating monthsSummary took ");
				tl.start();

				// There will be no impact of this code in main line.
				// This is used only for JUnit test cases. This context 
				// maintains state of number if months processed.
				// JUnits will set a month number to fail the process.
				// Once the processed month counter reached the above specified
				// number then process will be failed by throwing an exception.
				JUnitBatchProcessContext.incrementMonthCounter();
				JUnitBatchProcessContext.fail();
				
				monthWisePrSummary = monthsSummary.summarize(monthPrs.getPrs(month));
				monthWiseSummaryMap.put(month, monthWisePrSummary);
				// Set last Pr created date as batch end date.
				//Date lastPrCreatedDate = devicePrs.get((devicePrs.size() - 1)).getCreatedDate();
				//summarizerCtxt.setPreviousBatchEndDate(lastPrCreatedDate);
				tl.logElapsed("Summarize PRs for the month: "+ month +" took ");
			}
			
			// There will be no impact of this code in main line.
			// This is used only for JUnit test cases. This context 
			// maintains state of number if batches processed.
			// JUnits will set a batch number to fail the process.
			// Once the processed batch counter reached the above specified
			// number then process will be failed by throwing an exception.
			JUnitBatchProcessContext.incrementBatchCounter();
			JUnitBatchProcessContext.fail();

			// Save month wise summary data of this batch
			for(String month : monthWiseSummaryMap.keySet()) {
				log.info("Saving summary data for month: " + month);
				monthWiseSummaryMap.get(month).save();
			}
			
			// TODO: PENDING YEAR SUMMRY IMPLEMENTATION
			/*Set<String> years = yearPrs.getKeys();
			for (String year : years) {
				PrSummaryData yearPrSummaryData = 
					PrSummaryData.createOrFetchPrSummaryByYear(year);
				PrBo.summarizeYearPrs(yearPrSummaryData, yearPrs.getPrs(year));
			}*/
		} catch (Throwable e) {
			// Set batch failure status in context.
			summarizerCtxt.setBatchSucess(false);
			String msg = "Failed to summarize prs between "
				+summarizerCtxt.dateWindowAsString()
				+", count: "+devicePrs.size();
			log.error(msg, e);
			Util.notifyAppAlert(msg,e);
			throw e;
		}

		
		
	}
	
	public static void exportPrHeads (
			String deviceName, String fromDate, String toDate, 
			List<DevicePr> devicePrs, OutputStream os,
			String fileName) throws IOException {
		PrHeadsExporter exporter = 
			new PrHeadsExporter(deviceName,fromDate,toDate,devicePrs,os,fileName);
		exporter.export();
	}

	public static void recordOrgStructure(DevicePr devicePr) {
		String companyCode = devicePr.getCompanyCode();
		String circleCode = devicePr.getCircleCode();
		String divisionCode = devicePr.getDivisionCode();
		String eroCode = devicePr.getEroCode();
		String subdivisionCode = devicePr.getSubDivisionCode();
		String sectionCode = devicePr.getSectionCode();
		String distCode = devicePr.getDistCode();
		
		String sectionName = devicePr.getSECTION();
		String eroName = devicePr.getERO();
		String distributionName = devicePr.getDISTRIBUTION();
		
		if (CommonUtil.isEmpty(companyCode) || CommonUtil.isEmpty(circleCode) ||
			CommonUtil.isEmpty(divisionCode) || CommonUtil.isEmpty(eroCode) ||
			CommonUtil.isEmpty(subdivisionCode) || CommonUtil.isEmpty(sectionCode) ||
			CommonUtil.isEmpty(distCode) || CommonUtil.isEmpty(sectionName) ||
			CommonUtil.isEmpty(eroName) || CommonUtil.isEmpty(distributionName)) {
			return;
		}
		
		boolean foundAMiss = false;
		OrgCompanyUi orgCompanyUi = OrgCompany.cFetchOrgCompany();
		if (orgCompanyUi == null) foundAMiss = true;
		if (!foundAMiss && !orgCompanyUi.doesCircleExists(circleCode)) foundAMiss = true;
		if (!foundAMiss && !orgCompanyUi.doesDivisionExists(divisionCode)) foundAMiss = true;
		if (!foundAMiss && !orgCompanyUi.doesEroExists(eroCode)) foundAMiss = true;
		if (!foundAMiss && !orgCompanyUi.doesSubdivisionExists(subdivisionCode)) foundAMiss = true;
		if (!foundAMiss && !orgCompanyUi.doesSectionExists(sectionCode)) foundAMiss = true;
		if (!foundAMiss && !orgCompanyUi.doesDistributionExists(distCode)) foundAMiss = true;
		
		if (!foundAMiss) return;
		
		OrgCompany orgCompany = OrgCompany.fetchOrgCompany();
		if (orgCompany == null) {
			orgCompany = new OrgCompany();
			orgCompany.setOcmpCode(companyCode);
			orgCompany.setOcmpName("HQ");
		}
		
		OrgCircle orgCircle = orgCompany.findCircle(circleCode);
		if (orgCircle == null) {
			orgCircle = new OrgCircle();
			orgCircle.setOcrcCode(circleCode);
			orgCircle.setOcrcName("Circle"+circleCode);
			orgCompany.addOcmpCircle(orgCircle);
		}
		OrgDivision orgDivision = orgCircle.findOrgDivision(divisionCode);
		if (orgDivision == null) {
			orgDivision = new OrgDivision();
			orgDivision.setOdvsCode(divisionCode);
			orgDivision.setOdvsName("Division"+divisionCode);
			orgCircle.addOrgDivision(orgDivision);
		}
		
		OrgEro orgEro = orgDivision.findOrgEro(eroCode);
		if (orgEro == null) {
			orgEro = new OrgEro();
			orgEro.setOeroCode(eroCode);
			orgEro.setOeroName(eroName);
			orgDivision.addOrgEro(orgEro);
		}
		
		OrgSubdivision orgSubdivision = orgEro.findOrgSubdivision(subdivisionCode);
		if (orgSubdivision == null) {
			orgSubdivision = new OrgSubdivision();
			orgSubdivision.setOsbdCode(subdivisionCode);
			orgSubdivision.setOsbdName("Subdivision"+subdivisionCode);
			orgEro.addOrgSubdivision(orgSubdivision);
		}
		
		OrgSection orgSection = orgSubdivision.findOrgSection(sectionCode);
		if (orgSection == null) {
			orgSection = new OrgSection();
			orgSection.setOsctCode(sectionCode);
			orgSection.setOsctName(sectionName);
			orgSubdivision.addOrgSection(orgSection);
		}
		
		OrgDistribution orgDistribution = orgSection.findOrgDistribution(distCode);
		if (orgDistribution == null) {
			orgDistribution = new OrgDistribution();
			orgDistribution.setOdstCode(distCode);
			orgDistribution.setOdstName(distributionName);
			orgSection.addOrgDistribution(orgDistribution);
		}
		orgCompany.save();
	}
	
	/**
	 * Returns recent 26 list of summary data keys - either Month or Year
	 * 
	 * @return List<String>
	 */
	public static List<String> cFetchSummaryDataList() {
		List<String> keys = 
			(List<String>)Util.getAppCacheValue(AppConfig.CACHE_SUMMARY_DATA_KEY_LIST);
		if (keys == null) {
			List<PrSummaryData> summaryDataList = 
				PrSummaryData.fetchRecentSummaryRecords();
			keys = new ArrayList<String>();
			for (PrSummaryData psd : summaryDataList) {
				if (psd.isForMonth()) {
					keys.add(psd.getMonth());
				} else {
					keys.add(psd.getYear());
				}
			}
			Collections.sort(keys, new Comparator<String>() {
				@Override
				public int compare(String o1, String o2) {
					// Descending order
					return -1*o1.compareTo(o2);
				}
			});
			Util.putAppCacheValue(AppConfig.CACHE_SUMMARY_DATA_KEY_LIST, keys);
		}
		return keys;
	}
	
	public static CrudResponse prepareCollectionDashboardData() {
		CrudResponse response = new CrudResponse();
		CollectionDashboardPiggyBack dashboardData = 
			new CollectionDashboardPiggyBack();
		dashboardData.setOrgCompanyUi(OrgCompany.cFetchOrgCompany());
		dashboardData.setSummaryDataKeys(PrBo.cFetchSummaryDataList());
		response.crudEntity = dashboardData;
		return response;
	}
	
	/**
	 * This class will be used in JUnits.
	 * There will be no impact on production code.
	 * State maintained in this class is -
	 * 1. batchCounter: Used to specify number of batches processed.
	 * 2. failAtBatchNumber: Used to specify batch number to fail the process.
	 * 
	 * JUnits will sets the failAtBatchNumber while starting the test case.
	 * Once the batch counter reached the failAtBatchNumber then fail()
	 * will throw an exception to fail the process.
	 * 
	 * Example:
	 * 
	 * 	Class BatchProcesser {
	 *     	public static void process() {
	 *     		int numberOfBatches = 5;
	 *	       	for(int i=0; i < numberOfBatches; i++) {
	 *		   		System.out.println("Processing started for batch: " + i);
	 *				// Your code follows here ...
	 *				JUnitBatchProcessContext.incrementBacthCounter();
	 *				JUnitBatchProcessContext.fail();
	 *				// Your code follows here ...
	 *				System.out.println("Processing completed for batch: " + i);
	 *		   	}
	 *     	}
	 * 	}
	 * 
	 *  Class JunitTestClass {
	 *  	public void summerizePrsTest() {
	 *          // Your code follows here
	 *  		JUnitBatchProcessContext.setFailAtBacthNumber(2);
	 *          // Once the batches in the process method
	 *          // reached to failAtbatchNumber process
	 *          // will by throwing an exception.
	 *          BatchProcesser.process();
	 *  	}
	 *  }
	 *
	 */
	public static class JUnitBatchProcessContext {
		
		private static int batchCounter = 0;
		
		private static int monthCounter = 0;
		
		private static int failAtBatchNumber = -1;
		
		private static int failAtMonthNumber = -1;
		
		public static void incrementBatchCounter() {
			batchCounter ++;
		}
		
		public static void incrementMonthCounter() {
			monthCounter ++;
		}
		
		public static void setFailAtBacthNumber(int failAtBatchNumber) {
			resetState();
			JUnitBatchProcessContext.failAtBatchNumber = failAtBatchNumber;
		}
		
		public static void resetState() {
			batchCounter = 0;
			failAtBatchNumber = -1;
			monthCounter = 0;
			failAtMonthNumber = -1;
		}
		
		public static void fail() throws Exception {
			String msg = "Failing process at bacth number: ";
			if(monthCounter == failAtMonthNumber) {
				msg = msg + monthCounter + " since failAtMonthNumber is matched";
				resetState();
				throw new Exception(msg);
			} else if(batchCounter == failAtBatchNumber) {
				msg = msg + batchCounter + " since failAtBatchNumber is matched";
				resetState();
				throw new Exception(msg);
			}
		}

		public static void setFailAtMonthNumber(int failAtMonthNumber) {
			JUnitBatchProcessContext.failAtMonthNumber = failAtMonthNumber;
		}
		
	}
}

class PrSummarizerContext {
	
	private static final String FIELD_CURSOR = "cursor";
	private static final String FIELD_CURSOR_QUERY_DATE = "cursorQueryDate";
	private static final String DATE_TIME_FORMAT = "dd/MM/yyyy hh:mm:ss SSS a";

	private final AppLogger log = 
		AppLogger.getLogger(PrSummarizerContext.class.getName()); 
	
	private Date mFromDate;
	private Date mToDate;
	private List<DevicePr> mDevicePrs;
	
	private Date cursorQueryDate = null;
	private String cursor = null;
	private boolean isBatchSucess = true;
	private String previousBatchCursorState = null;
	private Date previousBatchEndDate = null;
	
	public PrSummarizerContext() {
		initContext();
	}
	
	public String dateWindowAsString() {
		return 
			Util.getFormattedDate(mFromDate,DATE_TIME_FORMAT) 
			+" - "+
			Util.getFormattedDate(mToDate,DATE_TIME_FORMAT);
	}

	public void markCompleted() {
		String jsonState = this.getStateAsJson();
		log.info("Saving last Pr Summarized date as "+mToDate+", state: "+jsonState);
		Date lastPrSummarizedDate = mToDate;
		if(!this.isBatchSucess()) {
			lastPrSummarizedDate = this.getPreviousBatchEndDate();
		}
		
		DbAppConfig appConfig = DbAppConfig.instance();
		if (appConfig != null) {
			appConfig.setLastPrSummarizedDate(lastPrSummarizedDate);
			appConfig.setPrSummaryContextJson(jsonState);
			appConfig.save();
		}
	}
	
	private Date fetchLastPrSummarizedDate() {
		if (mFromDate != null) return mFromDate;
		DbAppConfig appConfig = DbAppConfig.instance();
		mFromDate = appConfig.getLastPrSummarizedDate();
		if (mFromDate == null) {
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, -1);
			mFromDate = cal.getTime();
		}
		return mFromDate;
	}

	private void initContext() {
		initState();
		this.setPreviousBatchEndDate(fetchLastPrSummarizedDate());
		//DbAppConfig appConfig = DbAppConfig.instance();
		//mFromDate = appConfig.getLastPrSummarizedDate();
		/*if (mFromDate == null) {
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, -1);
			mFromDate = cal.getTime();
		}*/
		mToDate = new Date(System.currentTimeMillis());
		log.info("summerizePrs - Summarizing PRs between "+mFromDate+" and "+mToDate);
		//mDevicePrs = DevicePr.fetchDevicePrsCreatedBetween(mFromDate,mToDate);
		//log.info("summerizePrs - Total PRs to summarize: "+mDevicePrs.size());
	}
	
	@SuppressWarnings("static-access")
	private void initState() {
		DbAppConfig appConfig = DbAppConfig.instance();
		String jsonCtxt = appConfig.getPrSummaryContextJson();
		if (jsonCtxt == null) return;
		JsonObject ojson = null;
		if (jsonCtxt != null && !jsonCtxt.trim().isEmpty()) {
			jsonCtxt = jsonCtxt.trim();
			JsonParser parser = new JsonParser();
			ojson = (JsonObject) parser.parse(jsonCtxt);
		}
		if (ojson == null) return; 
		
		ResultAggregatorFactory j = new ResultAggregatorFactory();
		setCursor(j.jsonAsString(ojson.getAsJsonPrimitive((PrSummarizerContext.FIELD_CURSOR))));
		setPreviousBatchCursorState(this.getCursor());
		String strCursorQueryDate = j.jsonAsString(ojson.getAsJsonPrimitive((PrSummarizerContext.FIELD_CURSOR_QUERY_DATE)));
		setCursorQueryDateFromString(strCursorQueryDate);
	}
	
	@SuppressWarnings("static-access")
	private String getStateAsJson() {
		String cursor = this.getCursor();
		if(!isBatchSucess) {
			cursor = this.getPreviousBatchCursorState();
		}	
		
		ResultAggregatorFactory j = new ResultAggregatorFactory();
		JsonObject json = new JsonObject();
		json.add(PrSummarizerContext.FIELD_CURSOR, j.asJsonObject(cursor));
		json.add(PrSummarizerContext.FIELD_CURSOR_QUERY_DATE, j.asJsonObject(this.getCursorQueryDateAsString()));
		return json.toString();
	}

	public Date getAfterDate() {
		return mFromDate;
	}

	public Date getBeforeDate() {
		return mToDate;
	}
	
	public List<DevicePr> getDevicePrs() {
		return mDevicePrs;
	}
	
	private void setDevicePrs(List<DevicePr> devicePrs) {
		mDevicePrs = devicePrs;
	}
	
	public boolean hasMore() {
		fetchDeltaPrsSinceLastFetch();
		return !getDevicePrs().isEmpty();
	}
	
	/**
	 * Queries for PRs that were created after
	 * the last fetch by means of cursor. If cursor
	 * not found then uses the date returned from 
	 * fetchLastPrSummarizedDate() to query created
	 * PR records after this date.
	 */
	private void fetchDeltaPrsSinceLastFetch() {
		String cursor = getCursor();
		Date createdAfterDate = getCursorQueryDate();
		if (createdAfterDate == null) {
			createdAfterDate = this.getAfterDate();
		}
		BatchResult<List<DevicePr>> deltaPrsBatch = 
			DevicePr.fetchDevicePrsCreatedAfter(createdAfterDate, cursor);
		this.setCursor(deltaPrsBatch.getEncodedCursor());
		this.setCursorQueryDate(createdAfterDate);
		this.setDevicePrs(deltaPrsBatch.getResults());
		this.setPreviousBatchCursorState(cursor);
	}

	private Date getCursorQueryDate() {
		return cursorQueryDate;
	}
	
	private String getCursorQueryDateAsString() {
		return Util.getFormattedDate(getCursorQueryDate(), DATE_TIME_FORMAT);
	}

	private void setCursorQueryDate(Date cursorQueryDate) {
		this.cursorQueryDate = cursorQueryDate;
	}
	
	private void setCursorQueryDateFromString(String date) {
		if (date == null) return;
		Date dt = Util.getDateFromString(date, DATE_TIME_FORMAT);
		this.setCursorQueryDate(dt);
	}

	private String getCursor() {
		return cursor;
	}

	private void setCursor(String cursor) {
		this.cursor = cursor;
	}
	
	public boolean isBatchSucess() {
		return isBatchSucess;
	}

	public void setBatchSucess(boolean isBatchSucess) {
		this.isBatchSucess = isBatchSucess;
	}

	private String getPreviousBatchCursorState() {
		return previousBatchCursorState;
	}

	private void setPreviousBatchCursorState(String previousBatchCursorState) {
		this.previousBatchCursorState = previousBatchCursorState;
	}

	public Date getPreviousBatchEndDate() {
		return previousBatchEndDate;
	}

	public void setPreviousBatchEndDate(Date previousBatchEndDate) {
		this.previousBatchEndDate = previousBatchEndDate;
	}

}