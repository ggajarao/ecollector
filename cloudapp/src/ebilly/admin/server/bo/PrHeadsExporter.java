package ebilly.admin.server.bo;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import ebilly.admin.server.core.reports.ResultAggregator;
import ebilly.admin.server.db.DevicePr;
import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.viewdef.PivotCriteria;
import ebilly.admin.shared.viewdef.ResultField;
import ebilly.admin.shared.viewdef.ResultRecord;
import ebilly.admin.shared.viewdef.SearchRequest;
import ebilly.admin.shared.viewdef.SearchResult;
import ecollector.common.FormatUtils;

public class PrHeadsExporter {
	private static final int REPORT_FORMAT_SC_NUMBER_COL_LENGTH = 15;
	private static final int REPORT_FORMAT_RC_COL_LENGTH = 15;
	private static final int REPORT_FORMAT_CC_COL_LENGTH = 15;
	private static final int REPORT_FORMAT_ACD_COL_LENGTH = 15;
	private static final int REPORT_FORMAT_AGL_COL_LENGTH = 15;
	
	private static final int REPORT_COL1_WIDTH = 22;
	private static final int REPORT_COL2_WIDTH = 25;
	
	private static final int REPORT_LINE_LENGTH = REPORT_COL1_WIDTH + REPORT_COL2_WIDTH;
	
	private String NEW_LINE = "\r\n";
	private String mDeviceName;
	private String mFromDate;
	private String mToDate;
	private List<DevicePr> mDevicePrs;
	private OutputStream mOs;
	private String mFileName;
	public PrHeadsExporter(String deviceName, String fromDate, String toDate, 
			List<DevicePr> devicePrs, OutputStream os,
			String fileName) {
		this.mDeviceName = deviceName;
		this.mFromDate = fromDate;
		this.mToDate = toDate;
		this.mDevicePrs = devicePrs;
		this.mOs = os;
		this.mFileName = fileName;
		
		if (CommonUtil.isEmpty(mFromDate)) {
			mFromDate = "";
		}
		if (CommonUtil.isEmpty(mToDate)) {
			mToDate = ebilly.admin.server.Util.getFormattedDate(new Date());
		}
		
		initAggregatorBase();
	}
	
	private SearchRequest mSearchRequest;
	List<PivotCriteria> mPivotList;
	private void initAggregatorBase() {
		List<ResultField> resultFields = new ArrayList<ResultField>();
		resultFields.add(new ResultField("SECTION",java.lang.String.class.getName()));
		resultFields.add(new ResultField("DISTRIBUTION",java.lang.String.class.getName()));
		resultFields.add(new ResultField("CC",java.lang.Double.class.getName()));
		resultFields.add(new ResultField("NS",java.lang.Double.class.getName()));
		resultFields.add(new ResultField("RC",java.lang.Double.class.getName()));
		resultFields.add(new ResultField("ACD",java.lang.Double.class.getName()));
		resultFields.add(new ResultField("AGL",java.lang.Double.class.getName()));
		resultFields.add(new ResultField("PR_COUNT",java.lang.Double.class.getName()));
		mSearchRequest = new SearchRequest(null, resultFields);
		mPivotList = new ArrayList<PivotCriteria>();
		mPivotList.add(mSearchRequest.pivotCriteriaFor("CC","SUM"));
		mPivotList.add(mSearchRequest.pivotCriteriaFor("NS","SUM"));
		mPivotList.add(mSearchRequest.pivotCriteriaFor("RC","SUM"));
		mPivotList.add(mSearchRequest.pivotCriteriaFor("ACD","SUM"));
		mPivotList.add(mSearchRequest.pivotCriteriaFor("AGL","SUM"));
		mPivotList.add(mSearchRequest.pivotCriteriaFor("PR_COUNT","COUNT"));
	}
	
	public void export() throws IOException {
		StringBuffer buffer =  new StringBuffer();
		buffer.setLength(0);
		lineSeparator(buffer);
		buffer
			.append(NEW_LINE)
			.append(FormatUtils.prepareLineRpad("PR Heads Report",REPORT_LINE_LENGTH));
		lineSeparator(buffer);
		mOs.write(buffer.toString().getBytes());
		
		sortPrsByDeviceNameSectionNameDistributionName(mDevicePrs);
		
		String lastSection = "";
		String lastDistribution = "";
		String distribution = "";
		String section = "";
		String deviceName = "";
		String lastDeviceName = "";
		ResultAggregator secDistAgg = null;
		ResultAggregator secAgg = null;
		boolean sectionChanged = false;
		for (DevicePr devicePr : mDevicePrs) {
			distribution = devicePr.getDISTRIBUTION(); 
			section = devicePr.getSECTION();
			deviceName = devicePr.getDeviceName();
			
			if (!lastSection.equalsIgnoreCase(section)) {
				
				if (secDistAgg != null) {
					buffer.setLength(0);
					prepareHeadsTotalBuffer("Distribution",buffer,lastDeviceName,lastDistribution,null,secDistAgg);
					mOs.write(buffer.toString().getBytes());
				}
				lastDistribution = distribution;
				secDistAgg = createSectionDistributionAggregator();
				
				if (secAgg != null) {
					buffer.setLength(0);
					prepareHeadsTotalBuffer("Section",buffer,lastDeviceName,null,lastSection,secAgg);
					mOs.write(buffer.toString().getBytes());
				}				
				buffer.setLength(0);
				buffer.append(NEW_LINE).append(
						FormatUtils.prepareLineCalign("Section: "+section, REPORT_LINE_LENGTH));
				mOs.write(buffer.toString().getBytes());
				secAgg = createSectionAggregator();
				lastSection = section;
				sectionChanged = true;
			} else {
				sectionChanged = false;
			}
				
			if (!sectionChanged && !lastDistribution.equalsIgnoreCase(distribution)) {
				if (secDistAgg != null) {
					buffer.setLength(0);
					prepareHeadsTotalBuffer("Distribution",buffer,lastDeviceName,lastDistribution,null,secDistAgg);
					mOs.write(buffer.toString().getBytes());
				}
				lastDistribution = distribution;
				secDistAgg = createSectionDistributionAggregator();
			}
			
			secDistAgg.aggregateRecord(createResultRecord(devicePr));
			secAgg.aggregateRecord(createResultRecord(devicePr));
			lastDeviceName = deviceName;
		}
		
		if (secDistAgg != null) {
			// dump the section, distribution head totals
			buffer.setLength(0);
			prepareHeadsTotalBuffer("Distribution",buffer,deviceName,distribution,null,secDistAgg);
			mOs.write(buffer.toString().getBytes());
		}
		
		if (secAgg != null) {
			buffer.setLength(0);
			prepareHeadsTotalBuffer("Section",buffer,deviceName,null,section,secAgg);
			mOs.write(buffer.toString().getBytes());
		}
		
		buffer.setLength(0);
		lineSeparator(buffer);
		mOs.write(buffer.toString().getBytes());
	}

	private void prepareHeadsTotalBuffer(String label, StringBuffer buffer, String deviceName, 
			String distribution, String section, ResultAggregator aggregator) {
		lineSeparator(buffer);
		if (section != null) {
			buffer
				.append(NEW_LINE)
				.append(FormatUtils.prepareLineRpad("Section: ", REPORT_COL1_WIDTH))
			    .append(FormatUtils.prepareLineLpad(section, REPORT_COL2_WIDTH));
		}
		
		if (distribution != null) {
			buffer
				.append(NEW_LINE)
				.append(FormatUtils.prepareLineRpad("Distribution: ", REPORT_COL1_WIDTH))
		        .append(FormatUtils.prepareLineLpad(distribution, REPORT_COL2_WIDTH));
		}
		
		buffer
			.append(NEW_LINE)
			.append(FormatUtils.prepareLineRpad("SCM Name: ", REPORT_COL1_WIDTH))
        	.append(FormatUtils.prepareLineLpad(deviceName, REPORT_COL2_WIDTH));
		
		
		if (!CommonUtil.isEmpty(mFromDate) && !CommonUtil.isEmpty(mToDate)) {
			buffer
				.append(NEW_LINE)
				.append(FormatUtils.prepareLineRpad("Between: ", REPORT_COL1_WIDTH))
	        	.append(FormatUtils.prepareLineLpad(mFromDate+" - "+mToDate, REPORT_COL2_WIDTH));
		}
		prepareTotalsBuffer(label,aggregator,buffer);
	}

	private void sortPrsByDeviceNameSectionNameDistributionName(
			List<DevicePr> mDevicePrs2) {
		Comparator<DevicePr> comparator = new Comparator<DevicePr>() {
			private StringBuffer sb1 = new StringBuffer();
			private StringBuffer sb2 = new StringBuffer();
			
			@Override
			public int compare(DevicePr o1, DevicePr o2) {
				if (o1 == null || o2 == null) return 0;
				
				sb1.setLength(0); sb2.setLength(0);
				
				String dn1 = o1.getDeviceName();
				String dt1 = o1.getDISTRIBUTION();
				String st1 = o1.getSECTION();
				String sn1 = o1.getSC_NO();
				sb1.append(dn1).append(st1).append(dt1).append(sn1);
				
				String dn2 = o2.getDeviceName();
				String dt2 = o2.getDISTRIBUTION();
				String st2 = o2.getSECTION();
				String sn2 = o2.getSC_NO();
				sb2.append(dn2).append(st2).append(dt2).append(sn2);
				int result = sb1.toString().compareToIgnoreCase(sb2.toString());
				//System.out.println(sb1.toString()+ "::" +sb2.toString()+" ? "+result);
				return result;
			}
		};
		Collections.sort(mDevicePrs2,comparator);
	}

	private void preparePrHead(DevicePr devicePr, StringBuffer buffer) {
		DevicePr pr = devicePr;
		buffer.append(NEW_LINE)
		.append(FormatUtils.prepareLineLpad(pr.getSC_NO(), REPORT_FORMAT_SC_NUMBER_COL_LENGTH))
		.append(FormatUtils.prepareLineLpad(FormatUtils.amountFormat(FormatUtils.decimalPadding(pr.getARREARS_N_DEMAND())), REPORT_FORMAT_CC_COL_LENGTH))
		.append(FormatUtils.prepareLineLpad(FormatUtils.amountFormat(FormatUtils.decimalPadding(pr.getRC_COLLECTED())), REPORT_FORMAT_RC_COL_LENGTH))
		.append(FormatUtils.prepareLineLpad(FormatUtils.amountFormat(FormatUtils.decimalPadding(pr.getACD_COLLECTED())), REPORT_FORMAT_ACD_COL_LENGTH))
		.append(FormatUtils.prepareLineLpad(FormatUtils.amountFormat(FormatUtils.decimalPadding(pr.getAGL_AMOUNT())), REPORT_FORMAT_AGL_COL_LENGTH));
	}

	private void lineSeparator(StringBuffer heading) {
		heading.append(NEW_LINE);
		int colSize = REPORT_LINE_LENGTH;			
		for (int i = 0; i < colSize; i++) {
			heading.append('-');
		}
	}
	
	private ResultAggregator createSectionAggregator() {
		SearchResult results = new SearchResult();
		String[] uniqueFields = new String[]{"SECTION"};
		ResultAggregator agg = new ResultAggregator(results,uniqueFields,mPivotList,mSearchRequest);
		return agg;
	}
	
	private ResultAggregator createSectionDistributionAggregator() {
		SearchResult results = new SearchResult();
		String[] uniqueFields = new String[]{"SECTION", "DISTRIBUTION"};
		ResultAggregator agg = new ResultAggregator(results,uniqueFields,mPivotList,mSearchRequest);
		return agg;
	}
	
	private ResultRecord createResultRecord(DevicePr devicePr) {
		boolean isHighValue = devicePr.isHighValue();
		ResultRecord record = new ResultRecord();
		record.fieldValues.add(devicePr.getSECTION());
		record.fieldValues.add(devicePr.getDISTRIBUTION());
		if (isHighValue) {
			record.fieldValues.add("0"); // CC
			record.fieldValues.add(devicePr.getARREARS_N_DEMAND()); // NS
		} else {
			record.fieldValues.add(devicePr.getARREARS_N_DEMAND()); // CC
			record.fieldValues.add("0"); // NS
		}
		record.fieldValues.add(devicePr.getRC_COLLECTED());
		record.fieldValues.add(devicePr.getACD_COLLECTED());
		record.fieldValues.add(devicePr.getAGL_AMOUNT());
		record.fieldValues.add("1");
		return record;
	}
	
	private void prepareTotalsBuffer(String label, ResultAggregator secDistAgg, StringBuffer buffer) {
		SearchResult aggregateSearchResult = secDistAgg.getAggregateSearchResult();
		List<ResultRecord> results = aggregateSearchResult.getResults();
		if (results.size() == 0) return;
		ResultRecord totsRec = results.get(0);
		String totalPrs = secDistAgg.getSearchRequest().getValue("PR_COUNT", totsRec);
		String totCc = secDistAgg.getSearchRequest().getValue("CC", totsRec);
		String totNs = secDistAgg.getSearchRequest().getValue("NS", totsRec);
		String totRc = secDistAgg.getSearchRequest().getValue("RC", totsRec);
		String totAcd = secDistAgg.getSearchRequest().getValue("ACD", totsRec);
		String totAgl = secDistAgg.getSearchRequest().getValue("AGL", totsRec);
		
		totCc = FormatUtils.decimalPadding(totCc);
		totNs = FormatUtils.decimalPadding(totNs);
		totRc = FormatUtils.decimalPadding(totRc);
		totAcd = FormatUtils.decimalPadding(totAcd);
		totAgl = FormatUtils.decimalPadding(totAgl);
		
		lineSeparator(buffer);
		buffer
			.append(NEW_LINE)
			.append(FormatUtils.prepareLineRpad("Number of PRs:", REPORT_COL1_WIDTH))
			.append(FormatUtils.prepareLineLpad(totalPrs, REPORT_COL2_WIDTH));
		lineSeparator(buffer);
		
		buffer
		.append(NEW_LINE)
		.append(FormatUtils.prepareLineRpad("CC", REPORT_COL1_WIDTH-1)).append(":") 
		.append(FormatUtils.prepareLineLpad(FormatUtils.amountFormat(totCc), REPORT_COL2_WIDTH))
		.append(NEW_LINE)
		.append(FormatUtils.prepareLineRpad("NS", REPORT_COL1_WIDTH-1)).append(":") 
		.append(FormatUtils.prepareLineLpad(FormatUtils.amountFormat(totNs), REPORT_COL2_WIDTH))
		.append(NEW_LINE)
		.append(FormatUtils.prepareLineRpad("RC", REPORT_COL1_WIDTH-1)).append(":")
		.append(FormatUtils.prepareLineLpad(FormatUtils.amountFormat(totRc), REPORT_COL2_WIDTH))
		.append(NEW_LINE)
		.append(FormatUtils.prepareLineRpad("ACD", REPORT_COL1_WIDTH-1)).append(":")
		.append(FormatUtils.prepareLineLpad(FormatUtils.amountFormat(totAcd), REPORT_COL2_WIDTH))
		.append(NEW_LINE)
		.append(FormatUtils.prepareLineRpad("AGL", REPORT_COL1_WIDTH-1)).append(":")
		.append(FormatUtils.prepareLineLpad(FormatUtils.amountFormat(totAgl), REPORT_COL2_WIDTH));
		lineSeparator(buffer);
		
		double grandTotal = 
			parseDouble(totCc)+
			parseDouble(totNs)+
			parseDouble(totRc)+
			parseDouble(totAcd)+
			parseDouble(totAgl);
		buffer
		.append(NEW_LINE)
		.append(FormatUtils.prepareLineRpad(label+" Total ", REPORT_COL1_WIDTH-1)).append(":") 
		.append(FormatUtils.prepareLineLpad(FormatUtils.amountFormat(FormatUtils.decimalPadding(grandTotal+"")), REPORT_COL2_WIDTH));
		lineSeparator(buffer);
		buffer.append(NEW_LINE);
		if ("Section".equalsIgnoreCase(label)) {
			buffer.append(NEW_LINE).append(NEW_LINE).append(NEW_LINE);
		}
	}
	
	private double parseDouble(String value) {
		double v = 0;
		try {v = Double.parseDouble(value);}catch(NumberFormatException e){}
		return v;
	}
}