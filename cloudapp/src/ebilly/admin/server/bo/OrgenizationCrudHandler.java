package ebilly.admin.server.bo;

import ebilly.admin.server.db.OrgCompany;
import ebilly.admin.shared.viewdef.CrudRequest;
import ebilly.admin.shared.viewdef.CrudResponse;

public class OrgenizationCrudHandler extends CrudHandler {

	public OrgenizationCrudHandler(CrudRequest request) {
		super(request);
	}

	@Override
	protected CrudResponse processCreateUpdate(CrudRequest request) {
		CrudResponse response = new CrudResponse();
		response.errorMessage = "Unsupported Operation";
		return response;
	}

	@Override
	protected CrudResponse processRetrieve(CrudRequest request) {
		CrudResponse response = new CrudResponse();
		response.crudEntity = OrgCompany.cFetchOrgCompany();
		return response;
	}	
}
