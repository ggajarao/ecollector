package ebilly.admin.server.bo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.jdo.Query;

import ebilly.admin.server.ContextUtil;
import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.server.db.User;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.RoleUi;
import ebilly.admin.shared.UserContext;
import ebilly.admin.shared.viewdef.ResultField;
import ebilly.admin.shared.viewdef.ResultRecord;
import ebilly.admin.shared.viewdef.SearchRequest;
import ebilly.admin.shared.viewdef.SearchResult;

public class UserSearchHandler extends BaseSearchHandler {
	
	public UserSearchHandler() {
		
	}

	protected Map<String, Class> getEntityFieldDataMap() {
		return DBUtils.fieldDataTypeMap(User.class);
	}

	/**
	 * 
	 * @return comma seperated list of entity names
	 */
	protected String getSearchEntityName() {
		return User.class.getName();
	}
	
	protected void setOrderClause(Query query, SearchRequest request) {
		query.setOrdering(" lastModifiedDate descending ");
	}

	@Override
	protected SearchResult prepareResults(SearchRequest request, Object results) {
		UserContext uc = ContextUtil.getInstance().getUserContext();
		
		List<User> qResults = (List<User>) results; 
		Map<String, Class> fieldDataTypeMap = DBUtils.fieldDataTypeMap(User.class);
		List<ResultRecord> records = new ArrayList<ResultRecord>();
		for (User user : qResults) {
			// Filter users based on current user role
			RoleUi r = UserBo.cFindRolebyId(user.getRoleId());
			
			if (!uc.getRoleCode().equalsIgnoreCase(AppConfig.ROLE_CODE_SUPER_ADMIN)) {
				if (r.getRoleCode().equalsIgnoreCase(AppConfig.ROLE_CODE_DEVICE_USER)) {
					continue;
				}
			}
			
			if (uc.getRoleCode().equalsIgnoreCase(AppConfig.ROLE_CODE_APP_ADMIN)) {
				if (r.getRoleCode().equalsIgnoreCase(AppConfig.ROLE_CODE_SUPER_ADMIN)) {
					continue;
				}
			} else if (!uc.getRoleCode().equalsIgnoreCase(AppConfig.ROLE_CODE_SUPER_ADMIN)) {
				continue;
			}
			
			ResultRecord record = new ResultRecord();
			Map<String, String> fieldValueMap = 
				DBUtils.toFieldStringValueMap(user, fieldDataTypeMap);
			for (ResultField resultColumn : request.getResultFields()) {
				String value = "";
				if ("Role".equalsIgnoreCase(resultColumn.getFieldName())) {
					if (r != null) {
						value = r.getRoleName();
					}
				} else {
					value = fieldValueMap.get(resultColumn.getFieldName()) + "";
				}
				
				record.fieldValues.add(value);
			}
			records.add(record);
		}
		return new SearchResult(records);
	}
}
