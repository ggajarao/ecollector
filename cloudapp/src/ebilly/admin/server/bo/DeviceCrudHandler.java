package ebilly.admin.server.bo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import ebilly.admin.server.core.CoreUtils;
import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.server.db.Device;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.DeviceFileUi;
import ebilly.admin.shared.DeviceUi;
import ebilly.admin.shared.viewdef.CrudRequest;
import ebilly.admin.shared.viewdef.CrudResponse;
import ebilly.admin.shared.viewdef.ResultRecord;
import ebilly.admin.shared.viewdef.SearchResult;
import ebilly.admin.shared.viewdef.SectionData;

public class DeviceCrudHandler extends CrudHandler {

	public DeviceCrudHandler(CrudRequest request) {
		super(request);
	}

	@Override
	protected CrudResponse processCreateUpdate(CrudRequest request) {
		return DeviceBo.createDevice(request);
	}

	@Override
	protected CrudResponse processRetrieve(CrudRequest request) {
		Map<String, String> fieldValueMap = request.getFieldValueMap();
		String id = fieldValueMap.get("id");
		DeviceUi device = DeviceBo.fetchDeviceById(id);
		
		CrudResponse response = new CrudResponse();
		if (device == null) {
			response.errorMessage = "Device not found";
			return response;
		}
		response.fieldValueMap = DBUtils.toFieldStringValueMap(device, 
				DBUtils.fieldDataTypeMap(DeviceUi.class));
		response.crudEntity = device;
		
		Map<String, List<String>> sectionResultColumnMap = request.getSectionResultColumnMap();
		
		for (Entry<String, List<String>> entry : sectionResultColumnMap.entrySet()) {
			String sectionName = entry.getKey();
			List<String> sectionResultColumns = entry.getValue();
			if (sectionName.equals(AppConfig.SECTION_DEVICE_FILES)) {
				getDeviceFileSection(device, 
						sectionResultColumns,response.sections);
			}
		}
		return response;
	}

	private void getDeviceFileSection(DeviceUi device,
			List<String> sectionResultColumns, List<SectionData> sections) {
		List<DeviceFileUi> deviceFiles = device.getDeviceFiles();
		//AcademicRegulationData ruData = AcademicRegulation.cFetchRegulationData();
		Map<String, Class> deviceFileFieldDataTypeMap = DBUtils.fieldDataTypeMap(DeviceFileUi.class);
		List<ResultRecord> resultRecords = new ArrayList<ResultRecord>();
		for (DeviceFileUi deviceFileUi : deviceFiles) {
			Map<String, String> deviceFieldValueMap = 
				DBUtils.toFieldStringValueMap(deviceFileUi, deviceFileFieldDataTypeMap);
			ResultRecord record = new ResultRecord();
			for (String resultColumn : sectionResultColumns) {
				String value = deviceFieldValueMap.get(resultColumn);
				record.fieldValues.add(""+value);
			}
			resultRecords.add(record);
		}
		sections.add(new SectionData(AppConfig.SECTION_DEVICE_FILES,"",
				     new SearchResult(resultRecords)));
	}
}
