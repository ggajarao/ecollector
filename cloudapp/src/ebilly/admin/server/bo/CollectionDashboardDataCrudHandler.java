package ebilly.admin.server.bo;

import ebilly.admin.shared.viewdef.CrudRequest;
import ebilly.admin.shared.viewdef.CrudResponse;

public class CollectionDashboardDataCrudHandler extends CrudHandler {

	public CollectionDashboardDataCrudHandler(CrudRequest request) {
		super(request);
	}

	@Override
	protected CrudResponse processCreateUpdate(CrudRequest request) {
		CrudResponse response = new CrudResponse();
		response.errorMessage = "Unsupported Operation";
		return response;
	}

	@Override
	protected CrudResponse processRetrieve(CrudRequest request) {
		return PrBo.prepareCollectionDashboardData();
	}
}
