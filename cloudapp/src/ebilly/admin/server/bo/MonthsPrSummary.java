package ebilly.admin.server.bo;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import org.codehaus.jackson.JsonParseException;

import ebilly.admin.server.Util;
import ebilly.admin.server.core.AppLogger;
import ebilly.admin.server.db.DevicePr;
import ebilly.admin.server.db.PrSummaryData;
import ebilly.admin.shared.AppConfig;

public class MonthsPrSummary {
	private PrSummaryData prSummaryData;
	private MonthSummaryDimensions mSummaryDimensions = null;
	public MonthsPrSummary(PrSummaryData prSummaryData) 
	throws JsonParseException, IOException {
		this.prSummaryData = prSummaryData;
		//initDimensionAggregatorsUsingGson();
		initDimensionAggregatorsUsingJackson();
	}
	
	private void initDimensionAggregatorsUsingJackson() 
	throws JsonParseException, IOException {
		
		// Dont do this unless its really needed.
		// very expensive in terms of log file size
		//AppLogger.getLogger("MonthsPrSummary").info("Dumping json: "+Util.readBlobFile(prSummaryData.getDataFileKey()));
		
		InputStream is = null;
		try {
			is = Util.openBlobFileStream(prSummaryData.getDataFileKey());
		} catch (Exception e) {
			AppLogger.getLogger(MonthsPrSummary.class.getName()).error(
					"Error reading blob file with key: "+prSummaryData.getDataFileKey()
					+", for PrSummaryData id: "+prSummaryData.getId(), e);
		}
		try {
			mSummaryDimensions = new MonthSummaryDimensions(is);
			if (mSummaryDimensions.isVersionChanged()) {
				prSummaryData.createNewSummaryVersion(mSummaryDimensions.getOldVersionNumber(),mSummaryDimensions.getNewVersionNumber());
			}
		} finally {
			if (is != null) {
				try{is.close();}catch(IOException e){
					AppLogger.getLogger(MonthsPrSummary.class.getName()).error(
						"Could not close the inputstream opnened for reading json string",e);
				}
			}
		}	
	}
	
	private void initDimensionAggregatorsUsingGson() {
		StringBuffer sbJson = new StringBuffer("{}");
		try {
			sbJson = Util.readBlobFile(prSummaryData.getDataFileKey());
		} catch (Exception e) {
			AppLogger.getLogger(MonthsPrSummary.class.getName()).error(
					"Error reading blob file with key: "+prSummaryData.getDataFileKey()
					+", for PrSummaryData id: "+prSummaryData.getId(), e);
		}
		mSummaryDimensions = new MonthSummaryDimensions(sbJson);
		if (mSummaryDimensions.isVersionChanged()) {
			prSummaryData.createNewSummaryVersion(mSummaryDimensions.getOldVersionNumber(),mSummaryDimensions.getNewVersionNumber());
		}
	}
	
	
	public void summarizeAndSave(List<DevicePr> devicePrs) throws IOException {
		this.summarize(devicePrs);
		prSummaryData.save();
	}

	public PrSummaryData summarize(List<DevicePr> devicePrs) throws IOException {
		mSummaryDimensions.aggregate(devicePrs);
		
		//Prepare json using Gson
		//String dataFileKey = prepareJsonFileUsingGson();
		
		//Prepare json using jackson
		String dataFileKey = prepareJsonFileUsingJackson();
		
		prSummaryData.setDataFileKey(dataFileKey);
		
		return prSummaryData;
	}
	
	private String prepareJsonFileUsingJackson() throws IOException {
		String fileName = prSummaryData.getMonth()+"_"+Util.getCurrentTimestamp()+".json";
		String dataFileKey = Util.createBlobFile(
				AppConfig.MIME_TYPE_JSON, fileName,
				new Util.BlobFileWriteListener() {
					public void readyToWrite(OutputStream os) throws IOException {
						TimeLogger tl = TimeLogger.create();
						mSummaryDimensions.writeAsJson(os);
						tl.logElapsed("Preparing json file using jackson stream took ");
					}
				});
		return dataFileKey;
	}
	
	private String prepareJsonFileUsingGson() throws IOException {
		String dimensionJson = mSummaryDimensions.asJsonString();
		String fileName = prSummaryData.getMonth()+".json";
		String dataFileKey = Util.createBlobFileBuffered(dimensionJson, AppConfig.MIME_TYPE_JSON, fileName);
		return dataFileKey;
	}

	public MonthSummaryDimensions dimensions() {
		return mSummaryDimensions;
	}
}