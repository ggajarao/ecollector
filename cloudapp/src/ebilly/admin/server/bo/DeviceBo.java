package ebilly.admin.server.bo;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.zip.ZipInputStream;

import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobInfoFactory;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreInputStream;
import com.google.appengine.api.datastore.KeyFactory;

import ebilly.admin.server.Util;
import ebilly.admin.server.core.AppLogger;
import ebilly.admin.server.core.CoreUtils;
import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.server.db.Device;
import ebilly.admin.server.db.DeviceCommand;
import ebilly.admin.server.db.DeviceFile;
import ebilly.admin.server.db.DevicePr;
import ebilly.admin.server.db.User;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.AppConfig.DEVICE_COMMAND;
import ebilly.admin.shared.AppConfig.DEVICE_COMMAND_STATES;
import ebilly.admin.shared.AppConfig.REMOTE_DEVICE_IMPORT_METHOD;
import ebilly.admin.shared.AppConfig.REMOTE_DEVICE_UPLOAD_STATES;
import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.DeviceCommandUi;
import ebilly.admin.shared.DeviceFileUi;
import ebilly.admin.shared.DeviceSummaryReport;
import ebilly.admin.shared.DeviceUi;
import ebilly.admin.shared.UserContext;
import ebilly.admin.shared.viewdef.CrudRequest;
import ebilly.admin.shared.viewdef.CrudResponse;
import ecollector.common.DeviceFileReader;
import ecollector.common.FormatUtils;
import ecollector.common.ServiceDataExporter;

public class DeviceBo {
	public static String pair(String deviceName) {
		if (CommonUtil.isEmpty(deviceName)) {
			return ":D";
		}
		deviceName = deviceName.trim();
		Device d = Device.cFetchDeviceByName(deviceName);
		if (d == null) {
			return ";)";
		}
		if (d.isPaired()) {
			return "-2";
		}
		try {
			SecureRandom s = new SecureRandom();
			BigInteger b = new BigInteger(130, s);
			String di = b.toString(16);
			d.updateToWaitingStatus();
			d.setDeviceIdentifier(di);
			d.save();
			
			User u = User.fetchUserByLoginId("D_"+d.getDeviceName());
			if (u == null) {
				try {
					u = DeviceBo.createDeviceUser(d);
				} catch(Exception e) {
					AppLogger.getLogger(DeviceBo.class.getName()).info("Failed to create device user",e);
					u = new User();
				}
			}
			return di+"|"+u.getLoginId()+"|"+u.getPassword();
		} catch (Exception e) {
			return "-1";
		}
	}
	
	public static String pairingAcknowledge(String deviceName, String deviceIdentifier) {
		if (CommonUtil.isEmpty(deviceName) || CommonUtil.isEmpty(deviceIdentifier)) {
			return ":D";
		}
		
		deviceIdentifier = deviceIdentifier.trim();
		deviceName = deviceName.trim();
		Device d = Device.cFetchDeviceByName(deviceName);
		if (d == null) {
			return ";)";
		}
		
		if (!d.isWaiting()) {
			return ":\\"; 
		}
		
		try {
			String di = d.getDeviceIdentifier();
			if (di != null && di.equals(deviceIdentifier)) {
				d.updateToPairedStatus();
				d.save();
				return "Ok";
			} else {
				return ":B";
			}
		} catch (Exception e) {
			return "-1";
		}		
	}
	
	public static void exportDeviceServiceStageRecords(String deviceName, Writer writer)
	throws Exception {
		List<DevicePr> devicePrs = 
			DevicePr.fetchDevicePrs(deviceName);
		exportDevicePrs(devicePrs, writer);
	}

	public static void exportDevicePrs(List<DevicePr> devicePrs,
			OutputStream outputStream, String fileName) throws Exception {
		fileName = fileName.toUpperCase();
		fileName = fileName.replaceAll(".ZIP", ".TXT");
		ServiceDataExporter exporter = null;
		try {
			exporter = 
				new ServiceDataExporter(outputStream,true/*compress*/,fileName);
			writeToExporter(devicePrs, exporter);
		} finally {
			if (exporter != null) exporter.close();
		}
	}
	
	public static void exportDevicePrs(List<DevicePr> devicePrs,
			Writer writer) throws Exception {
		ServiceDataExporter exporter = new ServiceDataExporter(writer);
		writeToExporter(devicePrs, exporter);
	}
	
	private static void writeToExporter(List<DevicePr> devicePrs, ServiceDataExporter exporter) 
	throws Exception {
		Map<String, Class> fieldDataTypeMap = 
			DBUtils.fieldDataTypeMap(ebilly.admin.server.db.DevicePr.class);
		/*try {*/
			for (DevicePr st : devicePrs) {
				Map<String, String> fieldValueMap = 
					DBUtils.toFieldStringValueMap(st, fieldDataTypeMap);
				
				/*Convert field name to upper case, otherwise they will be in bean property name form*/
				Map<String,String> uValueMap = 
					new HashMap<String,String>();
				for (Entry<String,String> e : fieldValueMap.entrySet()) {
					uValueMap.put(e.getKey().toUpperCase(),e.getValue());
				}
				exporter.export(uValueMap);
			}
		/*} catch (IOException e) {
			String msg = "IO Error writing services, cause: "+e.getMessage();
			AppLogger.getLogger("DeviceBo").info(msg, e);
		} catch (Exception ex) {
			ex.printStackTrace();
			String msg = "Error writing PRs, cause: "+ex.getMessage();
			AppLogger.getLogger("DeviceBo").info(msg, ex);
		}*/
	}

	public static DeviceUi fetchDevice(String deviceName) {
		if (CommonUtil.isEmpty(deviceName)) {
			return null;
		}
		Device d = Device.cFetchDeviceByName(deviceName);
		if (d == null) {
			AppLogger.getLogger(DeviceBo.class.getName()).info("Fetch device, no device found with name: "+deviceName);
		}
		return prepareDeviceUi(d);
	}
	
	public static DeviceUi fetchDeviceById(String deviceId) {
		if (CommonUtil.isEmpty(deviceId)) {
			return null;
		}
		Device d = Device.cFetchDeviceById(deviceId);
		if (d == null) {
			AppLogger.getLogger(DeviceBo.class.getName()).info("Fetch device, no device found with Id: "+deviceId);
		}
		return prepareDeviceUi(d);
	}
	
	private static DeviceUi prepareDeviceUi(Device d) {
		if (d == null) return null;
		DeviceUi dUi = new DeviceUi();
		DBUtils.copyProperties(d, dUi);
		for (DeviceFile df : d.getDeviceFiles()) {
			DeviceFileUi dfUi = new DeviceFileUi();
			DBUtils.copyProperties(df, dfUi);
			dUi.addDeviceFile(dfUi);
		}
		dUi.setDeviceSummaryReport(DeviceBo.cFetchDeviceSummaryReport(dUi.getId()));
		return dUi;
	}

	public static CrudResponse acceptDeviceServiceFile(String deviceId,
			String importMethod/*true - to clear existing data, false - otherwise*/,
			String blobKey) {
		CrudResponse r = new CrudResponse();
		if (CommonUtil.isEmpty(deviceId)) {
			r.errorMessage = "Invalid request param device id";
			return r;
		}
		
		if (CommonUtil.isEmpty(blobKey)) {
			r.errorMessage = "Unable to save uploaded file";
		}
		
		BlobKey bBlobKey = new BlobKey(blobKey);
		
		// TODO: Validate file
		// 	Import options into device
		// 		1. Owerwite existing services
		//      2. Keep existing services and import new services
		//      3.
		BlobstoreInputStream blobStoreInputStream = null;
		try {
			blobStoreInputStream = new BlobstoreInputStream(bBlobKey);
		} catch (Exception e) {
			r.errorMessage = "Error reading uploaded file, "+e.getMessage();
			return r;
		}
		
		String fileName = null;
		BlobInfoFactory f = new BlobInfoFactory();
		BlobInfo blobInfo = f.loadBlobInfo(bBlobKey);
		if (blobInfo != null) {
			fileName = blobInfo.getFilename();
		}
		
		InputStream dataFileIs = blobStoreInputStream;
		if (fileName != null && fileName.toUpperCase().endsWith(".ZIP")) {
			dataFileIs = new ZipInputStream(blobStoreInputStream);
			try {
				((ZipInputStream)dataFileIs).getNextEntry();
			} catch (IOException e) {
				r.errorMessage = "Corrupt file, "+e.getMessage();
				return r;
			}
		}
		
		String fileValidationResult = DeviceFileReader.isValidFile(dataFileIs);
		if ("NO".equalsIgnoreCase(fileValidationResult)) {
			r.errorMessage = "Invalid file format, not acceptable";
			return r;
		} else if (!"YES".equalsIgnoreCase(fileValidationResult)) {
			r.errorMessage = "Invalid file format, not acceptable. "+fileValidationResult;
			return r;
		}
		
		Device d = Device.cFetchDeviceById(deviceId);
		if (d == null) {
			r.errorMessage = "Device not found with given id";
			return r;
		}
		
		DeviceFile df = new DeviceFile();
		df.setDeviceServicesFileKey(blobKey);
		df.setDeviceServicesFileName(fileName);
		df.setDeviceFileStagingDate(ebilly.admin.server.Util.getCurrentDate());
		df.setRemoteDeviceFileUploadState(REMOTE_DEVICE_UPLOAD_STATES.READY_TO_UPLOAD_TO_REMOTE_DEVICE);
		
		if (Boolean.valueOf(importMethod)) {
			df.setRemoteDeviceImportMethod(REMOTE_DEVICE_IMPORT_METHOD.CLEAR_EXISTING_DATA);
		} else {
			df.setRemoteDeviceImportMethod(REMOTE_DEVICE_IMPORT_METHOD.ADD_TO_EXISTING_DATA);
		}
		d.addDeviceFile(df);
		try {
			d.save();
		} catch (Exception e) {
			AppLogger.getLogger("DeviceBo").info("Failed accept device service file, deviceId: "+deviceId
					+", blobKey: "+blobKey, e);
			r.errorMessage = "Error accepting device file, reason: "+e.getMessage();
		}
		return r;
	}

	public static CrudResponse removeDeviceFiles(String[] deviceFileIds) {
		CrudResponse r = new CrudResponse();
		
		if (deviceFileIds == null || deviceFileIds.length == 0) {
			r.errorMessage = "No device files to remove";
			return r;
		}
		
		String deviceId = KeyFactory.keyToString(KeyFactory.stringToKey(deviceFileIds[0]).getParent());
		Device d = Device.cFetchDeviceById(deviceId);
		if (d == null) {
			r.errorMessage = "No device found with given id";
			return r;
		}
		
		List<DeviceFile> deviceFiles = d.getDeviceFiles();
		if (deviceFiles.isEmpty()) {
			r.errorMessage = "This device has no files";
			return r;
		}
		List<DeviceFile> removableDeviceFiles = new ArrayList<DeviceFile>();
		for (DeviceFile df : deviceFiles) {
			for (String deviceFileId : deviceFileIds) {
				if (df.getId().equals(deviceFileId)) {
					removableDeviceFiles.add(df);
				}
			}
		}
		try {
			deviceFiles.removeAll(removableDeviceFiles);
			d.save();
			r.successMessage = "Successfully Removed Device File(s)";
		} catch (Exception e) {
			String msg = "Unable to remove device fiels from device: "+d.getDeviceName();
			AppLogger.getLogger("DeviceBo").info(msg, e);
			r.errorMessage = msg;
		}
		return r;
	}

	public static DeviceFileUi[] fetchDownloadableDeviceFiles(
			String deviceIdentifier) {
		if (CommonUtil.isEmpty(deviceIdentifier)) {
			throw new IllegalArgumentException("Requireme param missing");
		}
		Device d = Device.fetchDeviceByIdentifier(deviceIdentifier);
		if (d == null) {
			throw new IllegalArgumentException("Unknown device");
		}
		List<DeviceFileUi> downloadableFiels = new ArrayList<DeviceFileUi>();
		List<DeviceFile> deviceFiles = d.getDeviceFiles();
		for (DeviceFile df : deviceFiles) {
			if (AppConfig.REMOTE_DEVICE_UPLOAD_STATES.READY_TO_UPLOAD_TO_REMOTE_DEVICE.toString()
					.equalsIgnoreCase(df.getRemoteDeviceFileUploadState())) {
				DeviceFileUi dfUi = new DeviceFileUi();
				DBUtils.copyProperties(df, dfUi);
				downloadableFiels.add(dfUi);
			}
		}
		return (DeviceFileUi[])downloadableFiels.toArray(new DeviceFileUi[]{});
	}

	public static CrudResponse deviceFileDownloadedInDevice(
			String deviceIdentifier, String deviceFileId) {
		CrudResponse r = new CrudResponse();
		if (CommonUtil.isEmpty(deviceIdentifier)) {
			r.errorMessage = "Requireme param missing";
			return r;
		}
		if (CommonUtil.isEmpty(deviceFileId)) {
			r.errorMessage = "Requireme param missing";
			return r;
		}
		Device d = Device.fetchDeviceByIdentifier(deviceIdentifier);
		if (d == null) {
			r.errorMessage = "Unknown device";
			return r;
		}
		List<DeviceFile> deviceFiles = d.getDeviceFiles();
		for (DeviceFile df : deviceFiles) {
			if (deviceFileId.equals(df.getId())) {
				df.setRemoteDeviceFileUploadState(AppConfig.REMOTE_DEVICE_UPLOAD_STATES.UPLOADED_TO_REMOTE_DEVICE);
				df.setRemoteDeviceFileUploadDate(ebilly.admin.server.Util.getCurrentDate());
				d.save();
				r.successMessage = "done";
				return r;
			}
		}
		r.errorMessage = "Device file not found";
		return r;
	}

	public static User createDeviceUser(Device device) {
		return UserBo.createNewUser(
				"D_"+device.getDeviceName(),
				ebilly.admin.server.Util.generateDeviceUserPassword(),
				AppConfig.ROLE_CODE_DEVICE_USER);
	}
	
	public static User fetchDeviceUser(String deviceId) {
		Device device = Device.cFetchDeviceById(deviceId);
		if (device != null) {
			return User.fetchUserByLoginId("D_"+device.getDeviceName());
		} else {
			return null;
		}
	}
	
	public static String fetchDeviceNameFromUserId(String loginId) {
		if (loginId == null || loginId.trim().length() == 0) return "";
		int startIndex = loginId.indexOf("D_");
		if (startIndex != -1) {
			return loginId.substring("D_".length());
		} else {
			return ""; 
		}
	}
	
	public static CrudResponse createDeviceCommand(String deviceId, 
			DEVICE_COMMAND deviceCommand, 
			String commandParam1, String commandParam2, String commandParam3) {
		CrudResponse response = new CrudResponse();
		if (deviceId == null || deviceId.trim().length() == 0) {
			response.errorMessage = "Device Id required";
			return response;
		}
		
		if (deviceCommand == null) {
			response.errorMessage = "Device command required";
			return response;
		}
		try {
			Device d = Device.cFetchDeviceById(deviceId);
			if (d == null) {
				response.errorMessage = "Device not found";
				return response;
			}
			DeviceCommand dc = new DeviceCommand();
			dc.setDeviceName(d.getDeviceName());
			dc.setCommandCode(deviceCommand);
			dc.setCommandParam1(commandParam1);
			dc.setCommandParam2(commandParam2);
			dc.setCommandParam3(commandParam3);
			dc.setCommandDate(new Date());
			dc.setState(DEVICE_COMMAND_STATES.NEW);
			dc.save();
			
			DeviceCommandUi dcUi = new DeviceCommandUi();
			DBUtils.copyProperties(dc, dcUi);
			response.successMessage = "Device Command created successfully";
			response.crudEntity = dcUi;
		} catch (Throwable t) {
			response.errorMessage = "Failed to create device command";
			AppLogger.getLogger("DeviceBO").error(response.errorMessage,t);
			return response;
		}
		return response;
	}
	
	public static CrudResponse updateDeviceCommand(String deviceCommandId,
			String commandState, String executionNotes) {
		CrudResponse response = new CrudResponse();
		
		DEVICE_COMMAND_STATES deviceCommandState = 
			DEVICE_COMMAND_STATES.asEnumValue(commandState);
		
		if (deviceCommandId == null || deviceCommandId.trim().length() == 0) {
			response.errorMessage = "Required device command id missing";
			return response;
		}
		
		if (deviceCommandState == null) {
			response.errorMessage = "Device command state is required or unknown";
			return response;
		}
		
		AppLogger.getLogger("DeviceBo").info("UpdateDeviceCommand - deviceCommandId: "+
				deviceCommandId+", deviceCommandState: "+deviceCommandState+
				", executionNotes: "+executionNotes);
		try {
			DeviceCommand dc = DeviceCommand.fetchDeviceCommandById(deviceCommandId);
			if (dc == null) {
				response.errorMessage = "Device command not found with id: "+deviceCommandId;
				return response;
			}
			dc.setState(deviceCommandState);
			dc.setExecutionDate(new Date());
			if (executionNotes != null) {
				int endIndex = executionNotes.length();
				if (endIndex > 255)	endIndex = 255;
				dc.setExecutionNotes(executionNotes.substring(0, endIndex));
			}
			dc.save();
			response.successMessage = "Device command updated successfully";
		} catch (Throwable t) {
			response.errorMessage = "Error updating device command Id: "+deviceCommandId+
				", state: "+deviceCommandState.toString()+", executionNotes: "+executionNotes;
			AppLogger.getLogger("DeviceBo").error(response.errorMessage, t);
			return response;
		}
		return response;
	}
	
	public static void setDeviceDetailsToContext(UserContext uc) {
		if (uc == null || !uc.isDeviceUser()) {
			return;
		}
		
		Device d = Device.fetchDeviceByLoginId(uc.getLoginId());
		if (d == null) return;

		DeviceUi dui = new DeviceUi();
		
		uc.setDeviceUi(dui);
		
		dui.setId(d.getId());
		dui.setCloudTime(new Date());
		dui.setCollectionExpirationDate(d.getCollectionExpirationDate());
		dui.setCollectionLimit(d.getCollectionLimit());
		
		/* Set device commands */
		List<DeviceCommand> deviceCommands = 
			DeviceCommand.cFetchNewDeviceCommandsByDevice(d.getDeviceName());
		if (deviceCommands != null && !deviceCommands.isEmpty()) {
			for (DeviceCommand dc : deviceCommands) {
				DeviceCommandUi dcUi = new DeviceCommandUi();
				DBUtils.copyProperties(dc, dcUi);
				dui.addDeviceCommand(dcUi);
			}
		}
	}
	
	public static CrudResponse resetDeviceCollection(String deviceId) {
		CrudResponse response = new CrudResponse();
		if (deviceId == null || deviceId.trim().length() == 0) {
			response.errorMessage = "Required param missing";
			return response;
		}
		return createDeviceCommand(deviceId, 
				DEVICE_COMMAND.RESET_COLLECTION,null, null, null);
	}
	
	public static CrudResponse resetDevicePairing(String deviceId) {
		CrudResponse response = new CrudResponse();
		if (deviceId == null || deviceId.trim().length() == 0) {
			response.errorMessage = "Required param missing";
			return response;
		}
		Device d = Device.cFetchDeviceById(deviceId);
		if (d == null) {
			response.errorMessage = "Unknown Device";
			return response;
		}
		try {
			d.resetPairing();
			d.save();
			response.successMessage = "Device is successfully Unpaired";
		} catch (Throwable t) {
			AppLogger.getLogger("DeviceBo").error("Problem Unpairing deviceId: "+deviceId, t);
			response.errorMessage = "Problem Unpairing Device";
		}
		return response;
	}

	public static CrudResponse fetchDeviceSummaryReport(String deviceId) {
		CrudResponse r = new CrudResponse();
		if (deviceId == null) {
			r.errorMessage = "Required param missing";
			return r;
		}
		deviceId = deviceId.trim();
		DeviceSummaryReport deviceSummary = DeviceBo.cFetchDeviceSummaryReport(deviceId);
		if (deviceSummary != null) {
			
		}
		r.crudEntity = deviceSummary;
		return r;
	}
	
	public static DeviceSummaryReport cFetchDeviceSummaryReport(String deviceId) {
		DeviceSummaryReport deviceSummary = 
			(DeviceSummaryReport)Util.getAppCacheValue(
					AppConfig.CACHE_DEVICE_SUMMARY_BY_DEVICE_ID+deviceId);
		if (deviceSummary != null) {
			deviceSummary.setmCollectionMargin("Rs "+FormatUtils.amountFormat(deviceSummary.getmCollectionMargin())+"/-");
			deviceSummary.setmTotalCollection("Rs "+FormatUtils.amountFormat(deviceSummary.getmTotalCollection())+"/-");
			deviceSummary.setmTotalServicesCount(FormatUtils.amountFormat(deviceSummary.getmTotalServicesCount()));
			deviceSummary.setmTotalPrCount(FormatUtils.amountFormat(deviceSummary.getmTotalPrCount()));
		}
		return deviceSummary;
	}

	public static CrudResponse createDevice(CrudRequest request) {
		Map<String, String> fieldValueMap = request.getFieldValueMap();
		String id = fieldValueMap.get("id");
		String deviceName = fieldValueMap.get("deviceName");
		
		CrudResponse response = new CrudResponse();
		Device device = null;
		boolean isNewDevice = false;
		if (CommonUtil.isEmpty(id)) {
			device = new Device();
			isNewDevice = true;
		} else {
			device = Device.fetchDeviceById(id);
			if (device == null) {
				response.errorMessage = "Device not found";
				return response;
			}
		}
		
		if (CommonUtil.isEmpty(id) && !CommonUtil.isEmpty(deviceName)) {
			Device existingDevice = Device.cFetchDeviceByName(deviceName);
			if (existingDevice != null) {
				response.errorMessage = "Device Name is already used by an existing Device";
				return response;
			}
		}
		
		List<String> fieldNames = new ArrayList<String>();
		List<String> values = new ArrayList<String>();
		Map<String, Class> fieldTypeMap = 
			DBUtils.fieldDataTypeMap(Device.class);
		for (Entry<String, String> entry : fieldValueMap.entrySet()) {
			fieldNames.add(entry.getKey());
			values.add(entry.getValue());
		}
		Map<String, Object> fieldTypedValuemap = CoreUtils.toFieldTypedValuemap(fieldNames.toArray(new String[]{}), 
				values.toArray(new String[]{}), fieldTypeMap);
		
		DBUtils.copyPropertiesFromMap(device, fieldTypedValuemap);
		device.save();
		
		if (isNewDevice) {
			try {
				DeviceBo.createDeviceUser(device);
			} catch (Exception e) {
				device.delete();
				response.errorMessage = "Failed to create a user for device, please choose a different name";
				return response;
			}
		}
		
		response.successMessage = "Device saved successfully";
		return response;
	}

	public static void deleteDevice(String deviceId) {
		User deviceUser = DeviceBo.fetchDeviceUser(deviceId);
		if (deviceUser != null) {
			UserBo.deleteUser(deviceUser.getId());
		}
		Device.delete(deviceId);
		
	}
}