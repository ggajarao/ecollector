package ebilly.admin.server.bo;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import ebilly.admin.server.core.AppLogger;
import ebilly.admin.server.db.PrSummaryData;
import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.viewdef.ResultField;
import ebilly.admin.shared.viewdef.ResultRecord;
import ebilly.admin.shared.viewdef.SearchRequest;
import ebilly.admin.shared.viewdef.SearchResult;
import ecollector.common.FormatUtils;

public class MonthPrSummarySearchHandler extends BaseSearchHandler {
	
	private static AppLogger log = AppLogger.getLogger(MonthPrSummarySearchHandler.class.getName());
	
	public MonthPrSummarySearchHandler() {
		
	}
	
	@Override
	public SearchResult fetchResults(SearchRequest request) throws Exception {
		long startMillis = System.currentTimeMillis();
		SearchResult results = new SearchResult();
		MonthPrSummarySearchFilter summaryFilter = 
			new MonthPrSummarySearchFilter(request.getFilterCriteria());
		PrSummaryData summaryData = null; 
		if (summaryFilter.isForMonth()) {
			String summaryMonth = summaryFilter.getSummaryMonth();
			if (CommonUtil.isEmpty(summaryMonth)) {
				summaryMonth = PrSummaryData.createMonthKey(Calendar.getInstance());
			}
			summaryData = PrSummaryData.fetchPrSummaryByMonth(summaryMonth);
		} else {
			results.setErrorMessage("Unsupported Operation");
			return results;
		}
		
		if (summaryData == null) {
			return results;
		}
		
		SearchResult result = prepareSearchResults(request, summaryData, summaryFilter);
		log.info(
				FormatUtils.decimalPadding(""+((System.currentTimeMillis()-startMillis)/1000.0))+
				"Secs to search summary using filters: "+summaryFilter.asReadableString()+
				", results: "+result.getResults().size());
		return result;
	}

	private SearchResult prepareSearchResults(SearchRequest request,
			PrSummaryData summaryData, MonthPrSummarySearchFilter summaryFilter) {
		SearchResult results = new SearchResult();
		MonthsPrSummary monthPrSummary;
		try {
			monthPrSummary = new MonthsPrSummary(summaryData);
		} catch (Exception e) {
			log.info("Exception while preparing MonthPrSummary, "+
					", Month: "+summaryData.getMonth()+
					", Year: "+summaryData.getYear()+
					", with fileKey: "+summaryData.getDataFileKey(), e);
			results.setErrorMessage("Report Unavailable");
			return results;
		}
		
		SearchResult summarySearchResults = monthPrSummary.dimensions().prepareSearchResults(summaryFilter);
		SearchRequest summarySearchRequest = monthPrSummary.dimensions().getSearchRequest();
		
		ResultRecord resultRecord = null;
		for (ResultRecord summaryResultRecord : summarySearchResults.getResults()) {
			resultRecord = new ResultRecord();
			for (ResultField field : request.getResultFields()) {
				resultRecord.fieldValues.add(
					summarySearchRequest.getValue(
							field.getFieldName(), 
							summaryResultRecord)
				);
			}
			results.addResultRecord(resultRecord);
		}
		return results;
	}

	protected Map<String, Class> getEntityFieldDataMap() {
		return new HashMap<String, Class>();
	}

	/**
	 * 
	 * @return comma seperated list of entity names
	 */
	protected String getSearchEntityName() {
		throw new IllegalArgumentException("Unsupported search o");
	}
	
	@Override
	protected SearchResult prepareResults(SearchRequest request, Object results) {
		throw new IllegalArgumentException("Unsupported search operation");
	}
}
