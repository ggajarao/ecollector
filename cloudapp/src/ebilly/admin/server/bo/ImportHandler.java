package ebilly.admin.server.bo;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.channels.Channels;
import java.util.Map;

import org.apache.commons.csv.CSVParser;

import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobInfoFactory;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.files.AppEngineFile;
import com.google.appengine.api.files.FileReadChannel;
import com.google.appengine.api.files.FileService;
import com.google.appengine.api.files.FileServiceFactory;

import ebilly.admin.server.Util;
import ebilly.admin.server.core.AppLogger;
import ebilly.admin.server.core.CoreUtils;
import ebilly.admin.server.db.ImportRequest;
import ebilly.admin.shared.AppConfig.IMPORT_STATUS;
import ebilly.admin.shared.CommonUtil;

public abstract class ImportHandler {

	public static ImportResult handleRequest(ImportRequest importRequest) {
		
		if (!IMPORT_STATUS.NEW.equals(importRequest.getStatus())) {
			AppLogger.getLogger("ImportHandler")
				.info("ImportRequest status is not new, cannot handle request. Id: "
						+importRequest.getId()+", status: "+importRequest.getStatus());
			return null;
		}
		
		// Set import request state to In-progress
		importRequest = 
			ImportRequest.updateRequestStateToInprogress(importRequest.getId());
		
		String objectType = importRequest.getObjectType();
		ImportResult importResult = null;
		boolean isRequestSuccess = false;
		try {
			/*if (AppConfig.OBJ_SERVICES.equalsIgnoreCase(objectType)) {
				importResult = (new ServicesImportHandler(importRequest)).importData();
			} else */{
				importResult = new ImportResult();
				importResult.errorMessage
					.append("Unsupported Object Type - ").append(objectType);
			}
			persistResults(importResult, importRequest);
			isRequestSuccess = true;
		} catch(Throwable t) {
			AppLogger.getLogger("ImportHandler").error("Error while import", t);
			importResult = new ImportResult();
			importResult.errorMessage
				.append("Error while import, cause: ").append(t.getMessage());
		} finally {
			if (!isRequestSuccess) {
				importRequest = 
					ImportRequest.updateRequestStateToFailed(importRequest.getId());
			}
		}
		
		return importResult;
	}
	
	private static void persistResults(ImportResult importResult, 
			ImportRequest importRequest) {
		ebilly.admin.server.db.ImportResult dIr = 
			new ebilly.admin.server.db.ImportResult();
		dIr.setErrorMessage(
				importResult.errorMessage.toString()+"\n"+
				importResult.getSummaryString());
		String failreRecordsBlobKey;
		try {
			failreRecordsBlobKey = prepareFailreRecordsFile(importResult, importRequest);
			if (!CommonUtil.isEmpty(failreRecordsBlobKey)) {
				dIr.setFailureRecordsFile(failreRecordsBlobKey);
			}
		} catch (IOException e) {
			AppLogger.getLogger("ImportHandler")
				.info("Error while preparing results file, importId: "
						+importRequest.getId(), e);
		}
		dIr.setHeader(importResult.getHeaderAsCsv());
		dIr.setImportRequestId(importRequest.getId());
		dIr.save();
		
		// Persist state of ImportRequest
		if (importResult.errorMessage.length() > 0 ) {
			// consider there are some errors
			importRequest.setStatus(IMPORT_STATUS.FAILED);
			importRequest.setImportErrorMessage(
					importResult.errorMessage.toString());			
		} else {
			importRequest.setStatus(IMPORT_STATUS.COMPLETED);
			importRequest.setImportResultFile(dIr.getFailureRecordsFile());
			importRequest.setImportSummary(importResult.getSummaryString());
		}
		importRequest.save();
	}

	private static String prepareFailreRecordsFile(ImportResult importResult, 
			ImportRequest importRequest) throws IOException {
		if (importResult.getFailureCounter() <= 0) {
			return null;
		}
		//FileService fs = FileServiceFactory.getFileService();
		// Get import file name, to be used for naming results file.
		BlobKey blobKey = new BlobKey(importRequest.getFileContent());
		BlobInfoFactory f = new BlobInfoFactory();
		BlobInfo blobInfo = f.loadBlobInfo(blobKey);
		String importFileName = blobInfo.getFilename();
		String fileName = "ImportResults - "+importFileName;
		String fileContent = importResult.getFailureRecordsAsCsvFormat();
		return Util.createBlobFile(fileContent, "text/csv", fileName);
	}

	protected ImportRequest importRequest;
	ImportHandler(ImportRequest importRequest) {
		this.importRequest = importRequest;
	}
	
	protected ImportResult importData() {
		ImportResult importResult = new ImportResult();
		String sBlobKey = importRequest.getFileContent();
		BufferedReader reader;
		FileReadChannel readChannel = null;
		try {
			FileService fs = FileServiceFactory.getFileService();
			AppEngineFile blobFile = 
				fs.getBlobFile(new BlobKey(sBlobKey));
			readChannel =
		        fs.openReadChannel(blobFile, false);
		    reader =
		        new BufferedReader(Channels.newReader(readChannel, "UTF8"));
		    importFileData(reader,importResult);
		} catch (Exception e1) {
			e1.printStackTrace();
			String message = 
				"Exception while importing data from blobKey: "
				+sBlobKey;
			AppLogger.getLogger("ImportHandler").info(message,e1);
			importResult.errorMessage.append(message).append("Cause: ")
				.append(e1.getMessage());
			return importResult;
		} finally {
			if (readChannel != null) {
				try {
					readChannel.close();
				} catch (IOException e) {}
			}
		}
		importCompleted();
		return importResult;
	}

	abstract protected void importFileData(BufferedReader reader, ImportResult importResult) throws IOException;

	protected ImportResult importDataOld() {
		
		ImportResult importResult = new ImportResult();
		String sBlobKey = importRequest.getFileContent();
		
		CSVParser csvParser = null;
		String[][] csvData = null;
		FileReadChannel readChannel = null;
		try {
			FileService fs = FileServiceFactory.getFileService();
			AppEngineFile blobFile = 
				fs.getBlobFile(new BlobKey(sBlobKey));
			readChannel =
		        fs.openReadChannel(blobFile, false);
		    BufferedReader reader =
		        new BufferedReader(Channels.newReader(readChannel, "UTF8"));
		    csvParser = new CSVParser(reader);
		    csvData = csvParser.getAllValues();
		} catch (Exception e1) {
			e1.printStackTrace();
			String message = 
				"Exception while reading data from blobKey: "
				+sBlobKey;
			AppLogger.getLogger("ImportHandler").info(message,e1);
			importResult.errorMessage.append(message).append("Cause: ")
				.append(e1.getMessage());
			return importResult;
		} finally {
			if (readChannel != null) {
				try {
					readChannel.close();
				} catch (IOException e) {}
			}
		}
		
		if (csvData == null) {
			importResult.errorMessage.append("File is empty !");
			return importResult;
		}
		
		String[] header = csvData[0];
		for (int i = 0; i < header.length; i++ ) {
			header[i] = header[i]!=null?header[i].trim():"";
		}
		importResult.setHeader(header);
		Map<String, Class> fieldTypeMap = getFieldTypeMap();
		for (int i = 1; i < csvData.length; i++) {
			try {
				Map<String, Object> recordData = 
					CoreUtils.toFieldTypedValuemap(header, csvData[i], 
							fieldTypeMap);
				if (recordData.isEmpty()) {
					importResult.countFailure();
					importResult.addFailure(csvData[i], "Empty Record", i);
					continue;
				}
				importRecordData(recordData);
				importResult.countSuccess();
			} catch (Exception e) {
				e.printStackTrace();
				if ( e instanceof java.lang.reflect.InvocationTargetException) {
					e = (Exception) ((java.lang.reflect.InvocationTargetException) e).getTargetException();
				}
				importResult.countFailure();
				importResult.addFailure(csvData[i], e.getMessage(), i);
			}
		}
		importCompleted();
		return importResult;
	}

	protected abstract void importRecordData(Map<String, Object> recordData) 
	throws Exception;

	protected abstract Map<String, Class> getFieldTypeMap();
	
	protected void importCompleted() {
		
	}
}

