package ebilly.admin.server.bo;

import ebilly.admin.shared.viewdef.SearchRequest;
import ebilly.admin.shared.viewdef.SearchResult;


public class LivePrAggregateSearchHandler extends PrAggregateSearchHandler {

	public LivePrAggregateSearchHandler() {

	}
	
	@Override
	public SearchResult fetchResults(SearchRequest request) throws Exception {
		LivePrSearchHandler ssrh = new LivePrSearchHandler();
		request.setFetchSize(-1);
		SearchResult result = ssrh.fetchResults(request);
		return this.prepareResults(request, result);
	}
}
