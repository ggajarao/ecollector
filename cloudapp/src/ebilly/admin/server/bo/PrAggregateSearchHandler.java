package ebilly.admin.server.bo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.server.core.reports.ResultAggregator;
import ebilly.admin.server.db.DevicePr;
import ebilly.admin.shared.viewdef.PivotCriteria;
import ebilly.admin.shared.viewdef.ResultField;
import ebilly.admin.shared.viewdef.SearchRequest;
import ebilly.admin.shared.viewdef.SearchResult;


public class PrAggregateSearchHandler extends BaseSearchHandler {

	@Override
	public SearchResult fetchResults(SearchRequest request) throws Exception {
		PrSearchHandler ssrh = new PrSearchHandler();
		request.setFetchSize(-1);
		SearchResult result = ssrh.fetchResults(request);
		return this.prepareResults(request, result);
	}

	@Override
	protected SearchResult prepareResults(SearchRequest request, Object r) {
		SearchResult results = (SearchResult)r;

		String[] uniqueFields = new String[]{"dISTRIBUTION"};
		List<PivotCriteria> pivotCriteria = new ArrayList<PivotCriteria>();
		pivotCriteria.add(request.pivotCriteriaFor("_DEVICE_PR_CODE", "COUNT"));
		pivotCriteria.add(request.pivotCriteriaFor("aMOUNT_COLLECTED", "SUM"));
		
		// Aggregates by Semester
		ResultAggregator agg = 
			new ResultAggregator(results,uniqueFields,pivotCriteria,request);
		SearchResult aggResults = agg.applyAggregation();


		// Aggregates across Semesters
		// change the data type for _DEVICE_PR_CODE so that sum will be 
		// performed across records
		ResultField rf = request.findField("_DEVICE_PR_CODE");
		if (rf != null) rf.setFieldType(java.lang.Integer.class.getName());
		pivotCriteria = new ArrayList<PivotCriteria>();
		pivotCriteria.add(request.pivotCriteriaFor("_DEVICE_PR_CODE", "SUM"));
		pivotCriteria.add(request.pivotCriteriaFor("aMOUNT_COLLECTED", "SUM"));
		
		ResultAggregator aggAcrossSems = 
			new ResultAggregator(aggResults,new String[]{"-"},pivotCriteria,request);
		SearchResult aggAcrossSemsResults = aggAcrossSems.applyAggregation();
		// Change the label of semester to Total, as this record
		// represents the grand total record.
		if (!aggAcrossSemsResults.isEmpty()) {
			request.setValue("dISTRIBUTION", "Total", aggAcrossSemsResults.getResults().get(0));
			//request.setValue("_DEVICE_PR_CODE", "", aggAcrossSemsResults.getResults().get(0));
		}

		return aggResults.append(aggAcrossSemsResults);
	}

	@Override
	protected Map<String, Class> getEntityFieldDataMap() {
		return DBUtils.fieldDataTypeMap(DevicePr.class);
	}

	@Override
	protected Object getSearchEntityName() {
		return null;
	}
}
