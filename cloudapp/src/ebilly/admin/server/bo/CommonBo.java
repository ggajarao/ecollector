package ebilly.admin.server.bo;

import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;

import ebilly.admin.server.Util;
import ebilly.admin.server.core.AppLogger;
import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.server.db.Device;
import ebilly.admin.server.db.ExportRequest;
import ebilly.admin.server.db.Filter;
import ebilly.admin.shared.AddItemRequest;
import ebilly.admin.shared.AppConfig.EXPORT_STATUS;
import ebilly.admin.shared.CrudRequestEntity;
import ebilly.admin.shared.EntitySelection;
import ebilly.admin.shared.EntityUi;
import ebilly.admin.shared.ExportRequestUi;
import ebilly.admin.shared.viewdef.CrudResponse;
import ebilly.admin.shared.viewdef.SearchRequest;

public class CommonBo {
	
	private static final AppLogger log = AppLogger.getLogger("CommonBo");
	
	public static CrudResponse processDeleteRequest(EntitySelection request) {
		CrudResponse response = new CrudResponse();
		if (request == null || request.getDeleteEntityList().isEmpty()) {
			response.errorMessage = "Nothing to delete";
			return response;
		}
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			List<String> sasRecordsIds = new ArrayList<String>();
			for (EntityUi e : request.getDeleteEntityList()) {
				if (e.getEntityName().equals(Device.class.getName())) {
					DeviceBo.deleteDevice(e.getEntityId());
				} else {
					response.errorMessage = "Deletion of entity : " +
					e.getEntityName() + " is not supported";
					return response;
				}
			}
		} catch (Throwable t) {
			response.errorMessage = t.getMessage();
			AppLogger.getLogger("CommonBo").error("Error while processing delete request", t);
			return response;
		}
		response.successMessage = "Deleted Successfully";
		return response;
	}

	public static CrudResponse processAddRequest(AddItemRequest request) {
		CrudResponse response = new CrudResponse();
		if (request == null || request.getEntityList().isEmpty()) {
			response.errorMessage = "Nothing to Add";
			return response;
		}
		PersistenceManager pm = DBUtils.getPMF().getPersistenceManager();
		try {
			for (CrudRequestEntity e : request.getEntityList()) {
				/*if (e.getEntityName().equals(AcademicRegulation.class.getName())) {
					AcademicRegulation.delete(e.getEntityId());
				} else if (e.getEntityName().equals(AcademicCourseBranch.class.getName())) {
					AcademicCourseBranch.delete(e.getEntityId());
				} else if (e.getEntityName().equals(AcademicSemester.class.getName())) {
					AcademicSemester.delete(e.getEntityId());
				} else if (e.getEntityName().equals(AcademicSubject.class.getName())) {
					AcademicSubject.delete(e.getEntityId());
				} else*/ {
					response.errorMessage = "Addition of entity : " +
					e.getEntityName() + " is not supported";
					return response;
				}
			}
		} catch (Throwable t) {
			response.errorMessage = t.getMessage();
			AppLogger.getLogger("CommonBo").error("Error while processing delete request", t);
			return response;
		}
		response.successMessage = "Deleted Successfully";
		return response;
	}

	public static CrudResponse createExportRequest(SearchRequest searchRequest) {
		CrudResponse r = new CrudResponse();
		if (searchRequest.getObjectName() == null || searchRequest.getObjectName().trim().length() == 0) {
			r.errorMessage = "Unknown Object";
			return r;
		}
		
		try {
			ExportRequest er = new ExportRequest();
			er.setObjectName(searchRequest.getObjectName());
			er.setStatus(EXPORT_STATUS.NEW);
			
			for (ebilly.admin.shared.viewdef.Filter f : searchRequest.getFilterCriteria().getFilters()) {
				Filter erFilter = new Filter();
				DBUtils.copyProperties(f, erFilter);
				er.addFilter(erFilter);
			}
			er.save();
			ExportRequestUi erUi = new ExportRequestUi();
			DBUtils.copyProperties(er, erUi);
			for (ebilly.admin.shared.viewdef.Filter f : searchRequest.getFilterCriteria().getFilters()) {
				erUi.addFilter(f);
			}
			r.crudEntity = erUi;
			Util.queueExportRequest(er.getId());
		} catch (Exception e) {
			log.info("failed operation, reason: "+e.getMessage(), e);
			r.errorMessage = "Failed operation, reason: "+e.getMessage();
		}
		return r;
	}
}

