package ebilly.admin.server;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ebilly.admin.server.bo.PrBo;
import ebilly.admin.server.core.AppLogger;
import ebilly.admin.shared.viewdef.CrudResponse;

public class PrSummarizer extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final AppLogger log = AppLogger.getLogger(PrSummarizer.class.getName());
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
	throws ServletException, IOException {
		handleRequest(req,resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
	throws ServletException, IOException {
		handleRequest(req,resp);
	}

	private void handleRequest(HttpServletRequest req, HttpServletResponse resp) 
	throws ServletException, IOException {
		PrintWriter writer = resp.getWriter();
		CrudResponse crudResponse = PrBo.summarizePrs();
		if (crudResponse.isSuccess()) {
			writer.println(crudResponse.successMessage);
		} else {
			writer.println(crudResponse.errorMessage);
		}
		writer.flush();
	}
}