package ebilly.admin.server;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ebilly.admin.server.core.AppLogger;
import ebilly.admin.server.db.DbAppConfig;
import ebilly.admin.server.db.EmailSendRequest;
import ebilly.admin.shared.AppConfig;

public class EmailSender extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		process(req,resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		process(req,resp);
	}
	
	private static final long DAY_MILLIES = 24*60*60*1000;
	private void process(HttpServletRequest req, HttpServletResponse resp) 
	throws IOException {
		
		int canSendEmails = DbAppConfig.instance().getCanSendEmails();
		if (canSendEmails == 0) {
			return;
		}
		
		Date lastSendFailureDate = 
			(Date)Util.getAppCacheValue(AppConfig.CACHE_LAST_EMAIL_SEND_FAILURE);
		if (lastSendFailureDate != null) {
			long delta = System.currentTimeMillis() - lastSendFailureDate.getTime();
			if (delta < DAY_MILLIES) { // within a day
				writeMessage(delta+ " < " + DAY_MILLIES + " ? true. Aborting, because a day has not past since last error", resp);
				return;
			}
		}
		List<EmailSendRequest> emailSendRequests = 
			EmailSendRequest.fetchPendingEmailSendRequests(
					DbAppConfig.instance().getMaxEmailRetryCount());
		int successCounter = 0;
		int total = emailSendRequests.size();
		try {
			
			for (EmailSendRequest r : emailSendRequests) {
				Util.sendEmail(r);
				successCounter ++;
			}
			writeMessage("Successfully Sent "+successCounter+" email(s) at this time",resp);
			return;
		} catch (Exception e) {
			String msg = 
				"Total emails attempted to send: "+total+
				", successfully sent: "+successCounter+
				", errored at sending next email, reason: "+ e.getMessage();
			AppLogger.getLogger("EmailSender").info(msg);
			writeMessage(msg, resp);
			Util.putAppCacheValue(AppConfig.CACHE_LAST_EMAIL_SEND_FAILURE, 
					new Date(System.currentTimeMillis()));
			return;
		}
	}

	private void writeMessage(String msg, HttpServletResponse resp) 
		throws IOException {
		resp.setContentType("text/html");
		ServletOutputStream out = resp.getOutputStream();
		out.write(msg.getBytes());
		out.flush();
		
	}
}

