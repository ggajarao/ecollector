package ebilly.admin.server;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ebilly.admin.server.bo.ImportHandler;
import ebilly.admin.server.core.AppLogger;
import ebilly.admin.server.db.ImportRequest;
import ebilly.admin.shared.AppConfig.IMPORT_STATUS;
import ebilly.admin.shared.CommonUtil;

public class ImportRequestHandler extends HttpServlet {
	
	private static final AppLogger log = AppLogger.getLogger(ImportRequestHandler.class.getName());
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
	throws ServletException, IOException {
		handleRequest(req,resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
	throws ServletException, IOException {
		handleRequest(req,resp);
	}

	private void handleRequest(HttpServletRequest req, HttpServletResponse resp) 
	throws ServletException, IOException {
		String irId = req.getParameter("irId");
		if (CommonUtil.isEmpty(irId)) {
			AppLogger.getLogger("ImportRequestHandler").info("Got empty import request Id, irId: "+irId);
			return;
		}
		
		ImportRequest ir = ImportRequest.fetchImportRequestById(irId);
		if (ir == null) {
			AppLogger.getLogger("ImportRequestHandler").info("ImportRequest record not found, irId: "+irId);
			return;
		}
		
		if (!ir.getStatus().equals(IMPORT_STATUS.NEW)) {
			AppLogger.getLogger("ImportRequestHandler").info("ImportRequest record is not in NEW status, " +
					"irId: "+irId);
			return;			
		}
		
		ImportHandler.handleRequest(ir);
	}
}