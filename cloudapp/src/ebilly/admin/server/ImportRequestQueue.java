package ebilly.admin.server;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ebilly.admin.shared.CommonUtil;

public class ImportRequestQueue extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		process(req,resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		process(req,resp);
	}
	
	@SuppressWarnings("unchecked")
	private void process(HttpServletRequest req, HttpServletResponse resp) 
	throws IOException {
		PrintWriter out = resp.getWriter();
		String action = req.getParameter("a");
		if ("q".equalsIgnoreCase(action)) {
			// queue Import Request id
			String irId = req.getParameter("irId");
			if (!CommonUtil.isEmpty(irId)) {
				Util.queueImportRequest(irId);
				out.write("Done");
			} else {
				out.write("Failed");
			}
		} else {
			out.write("!");
		}
	}
}

