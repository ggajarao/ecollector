package ebilly.admin.server;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobInfoFactory;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;

import ebilly.admin.server.bo.DeviceBo;
import ebilly.admin.server.core.AppLogger;
import ebilly.admin.server.db.ImportRequest;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.AppConfig.IMPORT_STATUS;
import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.viewdef.CrudResponse;

public class DataUploadHandler extends HttpServlet {
	
	private static final AppLogger log = AppLogger.getLogger(DataUploadHandler.class.getName());
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
	throws ServletException, IOException {
		handleRequest(req,resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
	throws ServletException, IOException {
		handleRequest(req,resp);
	}

	private void handleRequest(HttpServletRequest req, HttpServletResponse resp) 
	throws ServletException, IOException {

		resp.setContentType("text/html");
		
		/*UserContext userContext = 
			(UserContext)req.getSession().getAttribute(
					Util.SESSION_ATTRIB_AUTH_TOKEN);
		
		if (userContext == null) {
			resp.getOutputStream().print("You are not authorized to import data !");
			return;
		}*/

		String objectType = null;
		String staticValue1 = null;
		String staticValue2 = null;
		String staticValue3 = null;
		String fileName = null;
		//String[][] fileContent = null;
		
		
		objectType = req.getParameter("objectType");
		staticValue1 = req.getParameter("staticValue1");
		staticValue2 = req.getParameter("staticValue2");
		staticValue3 = req.getParameter("staticValue3");
		String blobKey = null;
		try {
			Map<String, List<BlobKey>> uploadedBlobs = 
				BlobstoreServiceFactory.getBlobstoreService().getUploads(req);
			List<BlobKey> list = uploadedBlobs.get("dataFile");
			if (list != null && !list.isEmpty()) {
				blobKey = list.get(0).getKeyString();
				if (CommonUtil.isEmpty(blobKey)) {
					resp.getOutputStream().print("Failed to upload content !!");
					return;
				}
				BlobInfoFactory f = new BlobInfoFactory();
				BlobInfo blobInfo = f.loadBlobInfo(new BlobKey(blobKey));
				if (blobInfo != null) {
					fileName = blobInfo.getFilename();
				}
			}
			
			if (AppConfig.OBJ_DEVICE_SERVICE_FILE.equalsIgnoreCase(objectType)) {
				CrudResponse cr = DeviceBo.acceptDeviceServiceFile(staticValue1/*DeviceId*/, staticValue2/*Whether to clear existing data or not*/, blobKey);
				if (!cr.isSuccess()) {
					resp.getOutputStream().print(cr.errorMessage);
					resp.getOutputStream().flush();
				}
			} else {
				ImportRequest importRequest = new ImportRequest();
				importRequest.setStaticValue1(staticValue1);
				importRequest.setStaticValue2(staticValue2);
				importRequest.setStaticValue3(staticValue3);
				importRequest.setObjectType(objectType);
				importRequest.setStatus(IMPORT_STATUS.NEW);
				importRequest.setFileName(fileName);
				importRequest.setFileContent(blobKey);
				importRequest.save();
				Util.queueImportRequest(importRequest.getId());
				resp.getOutputStream().print("Import Job has been scheduled successfully");
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error while scheduing import job", e);
			resp.getOutputStream().print("Error: "+e.getMessage());
		}
	}
}

