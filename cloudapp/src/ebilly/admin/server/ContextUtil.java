package ebilly.admin.server;

import ebilly.admin.shared.UserContext;

public class ContextUtil {
	private transient ThreadLocal<UserContext> mUserContext = null;
	private static ContextUtil mInstance = null;
	private ContextUtil() {
		
	}
	
	public static ContextUtil getInstance() {
		if (mInstance != null) return mInstance; 
		else {
			synchronized (ContextUtil.class) {
				if (mInstance != null) {
					return mInstance;
				} else {
					mInstance = new ContextUtil();
					return mInstance;
				}
			}
		}
	}
	
	private void createThreadLocal() {
		if (mUserContext == null) {
			mUserContext = new ThreadLocal<UserContext>();
		}
	}
	
	public void pushUserContext(UserContext userContext) {
		synchronized (ContextUtil.class) {
			createThreadLocal();
			mUserContext.set(userContext);
		}
	}
	
	public UserContext getUserContext() {
		UserContext uc = mUserContext.get();
		if (uc == null) {
			throw new IllegalArgumentException("Unautorized Access");
		}
		return uc;
	}
}
