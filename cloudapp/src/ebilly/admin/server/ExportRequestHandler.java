package ebilly.admin.server;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ebilly.admin.server.bo.BaseSearchHandler;
import ebilly.admin.server.core.AppLogger;
import ebilly.admin.server.db.ExportRequest;
import ebilly.admin.shared.CommonUtil;

public class ExportRequestHandler extends HttpServlet {
	
	private static final AppLogger log = AppLogger.getLogger(ExportRequestHandler.class.getName());
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
	throws ServletException, IOException {
		handleRequest(req,resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
	throws ServletException, IOException {
		handleRequest(req,resp);
	}

	private void handleRequest(HttpServletRequest req, HttpServletResponse resp) 
	throws ServletException, IOException {
		String erId = req.getParameter("erId");
		if (CommonUtil.isEmpty(erId)) {
			log.info("Got empty export request Id, erId: "+erId);
			return;
		}
		
		ExportRequest er = ExportRequest.fetchExportRequestById(erId);
		if (er == null) {
			log.info("ImportRequest record not found, irId: "+erId);
			return;
		}
		
		BaseSearchHandler.handleExportRequest(er);
	}
}