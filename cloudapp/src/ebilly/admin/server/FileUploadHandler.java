package ebilly.admin.server;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;

import ebilly.admin.server.core.AppLogger;

public class FileUploadHandler extends HttpServlet {
	
	private static final AppLogger log = AppLogger.getLogger(FileUploadHandler.class.getName());
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
	throws ServletException, IOException {
		handleRequest(req,resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
	throws ServletException, IOException {
		handleRequest(req,resp);
	}

	private void handleRequest(HttpServletRequest req, HttpServletResponse resp) 
	throws ServletException, IOException {
		log.info("FileUploadHandler::handleRequest method entry");
		resp.setContentType("text/plain");
		
		String blobKey = null;
		try {
			Map<String, List<BlobKey>> uploadedBlobs = 
				BlobstoreServiceFactory.getBlobstoreService().getUploads(req);
			StringBuffer blobKeyList = new StringBuffer();
			
			if (uploadedBlobs == null) {
				log.info("FileUploadHandler::handleRequest uploadedBlobs got null!!");
				respond("", resp);
				return;
			}
			
			log.info("FileUploadHandler::handleRequest uploadedBlobs size: "+uploadedBlobs.size());
			for (Entry<String, List<BlobKey>> e : uploadedBlobs.entrySet()) {
				List<BlobKey> blobKeys = e.getValue();
				for (BlobKey bk : blobKeys) {
					if (blobKeyList.length() != 0) {
						blobKeyList.append(",");
					}
					blobKeyList.append(bk.getKeyString());
				}
			}
			log.info("FileUploadHandler::handleRequest blobKeyList: "+blobKeyList.toString());
			respond(blobKeyList.toString(),resp);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("FileUploadHandler::handleRequest error preparing blobKeyList", e);
			//resp.getOutputStream().print("Error: "+e.getMessage());
		}
	}
	
	private void respond(String message, HttpServletResponse resp) 
	throws IOException {
		resp.setContentType("text/text");
		resp.getWriter().println(message);
	}
}

