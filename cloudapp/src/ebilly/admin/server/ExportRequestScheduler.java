package ebilly.admin.server;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ebilly.admin.server.core.AppLogger;
import ebilly.admin.server.db.ExportRequest;

public class ExportRequestScheduler extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		process(req,resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		process(req,resp);
	}
	
	private static final long THRESHOLD_MILLIES = 12 /*mins*/ * 60 * 1000; 
	private static final long MAX_ATTEMPTS = 3;
	private static final AppLogger log = AppLogger.getLogger(ExportRequestScheduler.class.getName());
	private void process(HttpServletRequest req, HttpServletResponse resp) 
	throws IOException {
		try {
			List<ExportRequest> newRequests = ExportRequest.fetchNewRequests();
			for (ExportRequest r : newRequests) {
				boolean elapsedThreshold = isRequestElapsedThreshold(r);
				if (elapsedThreshold) {
					Util.queueExportRequest(r.getId());
					log.info("id: "+r.getId()+" queued to process.");
				} else {
					log.info("id: "+r.getId()+" can wait.");
				}
			}
			
			List<ExportRequest> inprogressRequests = ExportRequest.fetchInprogressRequests();
			for (ExportRequest r : inprogressRequests) {
				boolean elapsedThreshold = isRequestElapsedThreshold(r);
				if (elapsedThreshold) {
					if (r.getAttempts() > MAX_ATTEMPTS) {
						log.info("id: "+r.getId()+" updated to failed state.");
						ExportRequest.updateRequestStateToFailed(r.getId(), "Scheduler max retries reached");
					} else {
						log.info("id: "+r.getId()+" updated to new state and queue to process.");
						ExportRequest.updateRequestStateToNew(r.getId());
						Util.queueExportRequest(r.getId());
					}
				} else {
					log.info("id: "+r.getId()+" can wait.");
				}
			}
			
			return;
		} catch (Exception e) {
			log.info("Exception processing",e);
			e.printStackTrace();
			return;
		}
	}

	private boolean isRequestElapsedThreshold(ExportRequest r) {
		Date lastModifiedDate = r.getLastModifiedDate();
		Date currentDate = new Date();
		long elapsedMillies = currentDate.getTime() - lastModifiedDate.getTime();
		boolean elapsedThreshold = elapsedMillies > THRESHOLD_MILLIES;
		log.info("id: "+r.getId()+
				", status: "+r.getStatus()+
				", lastModifiedDate: "+lastModifiedDate+", currentDate: "+currentDate+
				"elapsedMillies: "+elapsedMillies+", elapsedThreshold? "+elapsedThreshold);
		return elapsedThreshold;
	}

	private void writeMessage(String msg, HttpServletResponse resp) 
		throws IOException {
		resp.setContentType("text/html");
		ServletOutputStream out = resp.getOutputStream();
		out.write(msg.getBytes());
		out.flush();
		
	}
}

