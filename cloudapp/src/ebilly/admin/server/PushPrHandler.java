package ebilly.admin.server;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ebilly.admin.server.bo.PrBo;
import ebilly.admin.shared.CommonUtil;
import ecollector.common.NetConstants;

public class PushPrHandler extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		process(req,resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		process(req,resp);
	}
	
	private void process(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String action = req.getParameter(NetConstants.REQUEST_PARAM_ACTION);
		String payLoad = req.getParameter(NetConstants.PUSH_SERVICE_PAYLOAD);
		String deviceIdentifier = req.getParameter(NetConstants.REQUEST_PARAM_DEVICE_IDENTIFIER);
		
		if (CommonUtil.isEmpty(action)) {
			respond(":)",resp);
			return;
		}
		if (NetConstants.ACTION_PUSH.equalsIgnoreCase(action)) {
			if (CommonUtil.isEmpty(payLoad)) {
				respond("",resp);
				return;
			}
			respond(PrBo.importPrsRaw(
					payLoad, deviceIdentifier),resp);
			return;
		} else {
			respond("",resp);
			return;
		}
	}

	private void respond(String message, HttpServletResponse resp) 
	throws IOException {
		resp.setContentType("text/text");
		resp.getWriter().println(message);
	}
}