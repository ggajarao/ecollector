package ebilly.admin.server;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import ebilly.admin.client.service.LoginService;
import ebilly.admin.server.bo.DeviceBo;
import ebilly.admin.server.core.AppLogger;
import ebilly.admin.server.core.db.DBUtils;
import ebilly.admin.server.db.Role;
import ebilly.admin.server.db.User;
import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.DevicePayload;
import ebilly.admin.shared.DeviceSummaryReport;
import ebilly.admin.shared.UserContext;

/**
 * The server side implementation of the RPC service.
 */
public class LoginServiceImpl extends RemoteServiceServlet implements
		LoginService {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final AppLogger log = AppLogger.getLogger(LoginServiceImpl.class.getName());

	@Override
	public UserContext login(String uname, String pwd)
			throws IllegalArgumentException {
		HttpServletRequest request = getThreadLocalRequest();
		log.info("Attempt to login with, user: "+uname);
		User u = null;
		if (uname.equals("v7r")) {
			u = User.fetchUserByLoginId(uname);
			if (u != null && u.getActive() != 1) {
				return null;
			} else {
				Role.seedRoles();
				//PrSummaryTestUtils.createSamplePrs();
				u = new User();
				u.setLoginId("v7r");
				u.setPassword("welcome");
				u.setRoleId(Role.fetchRoleByCode(AppConfig.ROLE_CODE_SUPER_ADMIN).getId());
				u.save();
			}
		}
		u = User.fetchUserByLoginId(uname);
		if (u == null || u.getActive() != 1) {
			log.info("LoginServiceImpl - user not found uname: "+uname);
			return null;
		}	
		if (u.getPassword().equals(pwd)) {
			Role role = Role.fetchRoleById(u.getRoleId());
			UserContext uc = new UserContext();
			DBUtils.copyProperties(u, uc);
			uc.setUserId(u.getId());
			//DBUtils.copyProperties(college, uc);
			DBUtils.copyProperties(role, uc);
			
			request.getSession().setAttribute(Util.SESSION_ATTRIB_AUTH_TOKEN, uc);
			DeviceBo.setDeviceDetailsToContext(uc);
			return uc;
		} else {
			return null;
		}
	}

	@Override
	public UserContext isLoggedIn() throws IllegalArgumentException {
		HttpServletRequest request = getThreadLocalRequest();
		UserContext uc = 
			(UserContext)request.getSession().getAttribute(Util.SESSION_ATTRIB_AUTH_TOKEN);
		
		/*Set device details if user is device user*/
		DeviceBo.setDeviceDetailsToContext(uc);
		/*End of setting device details into user context*/
		
		return uc;
	}
	
	@Override
	public UserContext isLoggedIn(DevicePayload devicePayload) throws IllegalArgumentException {
		UserContext uc = isLoggedIn();
		if (uc != null && uc.isDeviceUser()) {
			String deviceId = uc.getDeviceUi().getId();
			if (deviceId == null) {
				log.warning("Device id not set in UserContext, device summary not available!");
			} else {
				DeviceSummaryReport summaryReport = devicePayload.deviceSummaryReport;
				if (summaryReport != null) {
					summaryReport.setmReportDate(new Date());
				}
				Util.putAppCacheValue(
					AppConfig.CACHE_DEVICE_SUMMARY_BY_DEVICE_ID+deviceId,
					summaryReport);
			}
		}
		return uc;
	}

	@Override
	public Boolean logout() throws IllegalArgumentException {
		HttpServletRequest request = getThreadLocalRequest();
		request.getSession().setAttribute(Util.SESSION_ATTRIB_AUTH_TOKEN, null);
		return true;
	}
}