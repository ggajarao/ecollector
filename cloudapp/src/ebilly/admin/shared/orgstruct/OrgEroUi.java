package ebilly.admin.shared.orgstruct;

import java.util.ArrayList;
import java.util.List;

import ebilly.admin.shared.EntityUi;

public class OrgEroUi extends EntityUi {
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String oeroCode;
	private String oeroName;
	private OrgDivisionUi oeroDivision;
	
	/**
	 *  @gwt.typeArgs <ebilly.admin.shared.orgstruct.OrgSubdivisionUi>
	 */
	private List<OrgSubdivisionUi> orgSubdivisions = new ArrayList<OrgSubdivisionUi>();

	public OrgEroUi() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getOeroCode() {
		return oeroCode;
	}

	public void setOeroCode(String oeroCode) {
		this.oeroCode = oeroCode;
	}

	public String getOeroName() {
		return oeroName;
	}

	public void setOeroName(String oeroName) {
		this.oeroName = oeroName;
	}

	public OrgDivisionUi getOeroDivision() {
		return oeroDivision;
	}

	public void setOeroDivision(OrgDivisionUi oeroDivision) {
		this.oeroDivision = oeroDivision;
	}

	public List<OrgSubdivisionUi> getOrgSubdivisions() {
		return orgSubdivisions;
	}

	public void addOrgSubdivisions(OrgSubdivisionUi orgSubdivision) {
		this.orgSubdivisions.add(orgSubdivision);
	}
}
