package ebilly.admin.shared.orgstruct;

import java.util.ArrayList;
import java.util.List;

import ebilly.admin.shared.EntityUi;

public class OrgSubdivisionUi extends EntityUi {
	private static final long serialVersionUID = 1L;

	private String id;
	private String osbdCode;
	private String osbdName;
	private OrgEroUi osbdEro;
	
	/**
	 *  @gwt.typeArgs <ebilly.admin.shared.orgstruct.OrgSectionUi>
	 */
	private List<OrgSectionUi> orgSections = new ArrayList<OrgSectionUi>();
	
	public OrgSubdivisionUi() {
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getOsbdCode() {
		return osbdCode;
	}

	public void setOsbdCode(String osbdCode) {
		this.osbdCode = osbdCode;
	}

	public String getOsbdName() {
		return osbdName;
	}

	public void setOsbdName(String osbdName) {
		this.osbdName = osbdName;
	}

	public OrgEroUi getOsbdEro() {
		return osbdEro;
	}

	public void setOsbdEro(OrgEroUi osbdEro) {
		this.osbdEro = osbdEro;
	}

	public List<OrgSectionUi> getOrgSections() {
		return orgSections;
	}

	public void addOrgSection(OrgSectionUi orgSection) {
		this.orgSections.add(orgSection);
	}
}
