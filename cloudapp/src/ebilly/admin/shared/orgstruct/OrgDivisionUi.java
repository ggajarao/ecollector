package ebilly.admin.shared.orgstruct;

import java.util.ArrayList;
import java.util.List;

import ebilly.admin.shared.EntityUi;

public class OrgDivisionUi extends EntityUi {
	private static final long serialVersionUID = 1L;

	private String id;
	private String odvsCode;
	private String odvsName;
	private OrgCircleUi odvsCircle;
	
	/**
	 *  @gwt.typeArgs <ebilly.admin.shared.orgstruct.OrgEroUi>
	 */
	private List<OrgEroUi> orgEros = new ArrayList<OrgEroUi>();
	
	public OrgDivisionUi() {
	
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getOdvsCode() {
		return odvsCode;
	}

	public void setOdvsCode(String odvsCode) {
		this.odvsCode = odvsCode;
	}

	public String getOdvsName() {
		return odvsName;
	}

	public void setOdvsName(String odvsName) {
		this.odvsName = odvsName;
	}

	public OrgCircleUi getOdvsCircle() {
		return odvsCircle;
	}

	public void setOdvsCircle(OrgCircleUi odvsCircle) {
		this.odvsCircle = odvsCircle;
	}

	public List<OrgEroUi> getOrgEros() {
		return orgEros;
	}

	public void addOrgEro(OrgEroUi orgEro) {
		this.orgEros.add(orgEro);
	}
}
