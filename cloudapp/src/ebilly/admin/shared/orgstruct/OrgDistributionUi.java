package ebilly.admin.shared.orgstruct;

import ebilly.admin.shared.EntityUi;

public class OrgDistributionUi extends EntityUi {
	private static final long serialVersionUID = 1L;
	private String id;
	private String odstCode;
	private String odstName;
	private OrgSectionUi odstSection;
	
	public OrgDistributionUi() {
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOdstCode() {
		return odstCode;
	}

	public void setOdstCode(String odstCode) {
		this.odstCode = odstCode;
	}

	public String getOdstName() {
		return odstName;
	}

	public void setOdstName(String odstName) {
		this.odstName = odstName;
	}

	public OrgSectionUi getOdstSection() {
		return odstSection;
	}

	public void setOdstSection(OrgSectionUi odstSection) {
		this.odstSection = odstSection;
	}
}
