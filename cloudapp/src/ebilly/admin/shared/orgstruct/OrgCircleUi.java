package ebilly.admin.shared.orgstruct;

import java.util.ArrayList;
import java.util.List;

import ebilly.admin.shared.EntityUi;

public class OrgCircleUi extends EntityUi {
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String ocrcCode;
	private String ocrcName;
	private OrgCompanyUi ocmpCompany;
	
	/**
	 *  @gwt.typeArgs <ebilly.admin.shared.orgstruct.OrgDivisionUi>
	 */
	private List<OrgDivisionUi> ocrcDivisions = new ArrayList<OrgDivisionUi>();
	
	public OrgCircleUi() {
		
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOcrcCode() {
		return ocrcCode;
	}

	public void setOcrcCode(String ocrcCode) {
		this.ocrcCode = ocrcCode;
	}

	public String getOcrcName() {
		return ocrcName;
	}

	public void setOcrcName(String ocrcName) {
		this.ocrcName = ocrcName;
	}

	public OrgCompanyUi getOcmpCompany() {
		return ocmpCompany;
	}

	public void setOcmpCompany(OrgCompanyUi ocmpCompany) {
		this.ocmpCompany = ocmpCompany;
	}

	public List<OrgDivisionUi> getOrgDivisions() {
		return ocrcDivisions;
	}

	public void addDivision(OrgDivisionUi divisionUi) {
		this.ocrcDivisions.add(divisionUi);
	}
}
