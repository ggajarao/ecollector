package ebilly.admin.shared.orgstruct;

import java.util.ArrayList;
import java.util.List;

import javax.jdo.annotations.Extension;
import javax.jdo.annotations.Persistent;

import ebilly.admin.server.db.OrgDistribution;
import ebilly.admin.server.db.OrgSubdivision;
import ebilly.admin.shared.EntityUi;

public class OrgSectionUi extends EntityUi {
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String osctCode;
	private String osctName;
	private OrgSubdivisionUi osctSubdivision;
	
	/**
	 *  @gwt.typeArgs <ebilly.admin.shared.orgstruct.OrgDistributionUi>
	 */
	private List<OrgDistributionUi> orgDistributions = new ArrayList<OrgDistributionUi>();
	
	public OrgSectionUi() {
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getOsctCode() {
		return osctCode;
	}

	public void setOsctCode(String osctCode) {
		this.osctCode = osctCode;
	}

	public String getOsctName() {
		return osctName;
	}

	public void setOsctName(String osctName) {
		this.osctName = osctName;
	}

	public OrgSubdivisionUi getOsctSubdivision() {
		return osctSubdivision;
	}

	public void setOsctSubdivision(OrgSubdivisionUi osctSubdivision) {
		this.osctSubdivision = osctSubdivision;
	}

	public List<OrgDistributionUi> getOrgDistributions() {
		return orgDistributions;
	}

	public void addOrgDistribution(OrgDistributionUi orgDistribution) {
		this.orgDistributions.add(orgDistribution);
	}
}
