package ebilly.admin.shared.orgstruct;

import java.util.ArrayList;
import java.util.List;

import ebilly.admin.shared.EntityUi;

public class OrgCompanyUi extends EntityUi {
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String ocmpCode;
	private String ocmpName;
	
	/**
	 *  @gwt.typeArgs <ebilly.admin.shared.orgstruct.OrgCircleUi>
	 */
	private List<OrgCircleUi> ocmpCircles = new ArrayList<OrgCircleUi>();
	
	public OrgCompanyUi() {
		
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOcmpCode() {
		return ocmpCode;
	}

	public void setOcmpCode(String ocmpCode) {
		this.ocmpCode = ocmpCode;
	}

	public String getOcmpName() {
		return ocmpName;
	}

	public void setOcmpName(String ocmpName) {
		this.ocmpName = ocmpName;
	}

	public List<OrgCircleUi> getOrgCircles() {
		return ocmpCircles;
	}

	public void addCircle(OrgCircleUi ocmpCircle) {
		this.ocmpCircles.add(ocmpCircle);
	}

	public boolean doesCircleExists(String circleCode) {
		return findCircle(circleCode) != null;
	}
	
	public OrgCircleUi findCircle(String circleCode) {
		for (OrgCircleUi circle : this.getOrgCircles()) {
			if (circle.getOcrcCode().equals(circleCode)) return circle;
		}
		return null;
	}

	public boolean doesDivisionExists(String divisionCode) {
		return findDivision(divisionCode) != null;
	}
	
	public OrgDivisionUi findDivision(String divisionCode) {
		for (OrgCircleUi circle : getOrgCircles()) {
			for (OrgDivisionUi division : circle.getOrgDivisions()) {
				if (division.getOdvsCode().equals(divisionCode)) return division;
			}
		}
		return null;
	}

	public boolean doesEroExists(String eroCode) {
		return findEro(eroCode) != null;
	}
	
	public OrgEroUi findEro(String eroCode) {
		for (OrgCircleUi circle : getOrgCircles()) {
			for (OrgDivisionUi division : circle.getOrgDivisions()) {
				for (OrgEroUi ero : division.getOrgEros()) {
					if (ero.getOeroCode().equals(eroCode)) return ero;
				}
			}
		}
		return null;
	}

	public boolean doesSubdivisionExists(String subdivisionCode) {
		return findSubdivision(subdivisionCode) != null;
	}
	
	public OrgSubdivisionUi findSubdivision(String subdivisionCode) {
		for (OrgCircleUi circle : getOrgCircles()) {
			for (OrgDivisionUi division : circle.getOrgDivisions()) {
				for (OrgEroUi ero : division.getOrgEros()) {
					for (OrgSubdivisionUi subdivision : ero.getOrgSubdivisions()) {
						if (subdivision.getOsbdCode().equals(subdivisionCode)) return subdivision;
					}
				}
			}
		}
		return null;
	}

	public boolean doesSectionExists(String sectionCode) {
		return findSection(sectionCode) != null;
	}

	public OrgSectionUi findSection(String sectionCode) {
		for (OrgCircleUi circle : getOrgCircles()) {
			for (OrgDivisionUi division : circle.getOrgDivisions()) {
				for (OrgEroUi ero : division.getOrgEros()) {
					for (OrgSubdivisionUi subdivision : ero.getOrgSubdivisions()) {
						for (OrgSectionUi section : subdivision.getOrgSections()) {
							if (section.getOsctCode().equals(sectionCode)) return section;
						}
					}
				}
			}
		}
		return null;
	}
	
	public boolean doesDistributionExists(String distCode) {
		return findDistribution(distCode) != null;
	}

	public OrgDistributionUi findDistribution(String distCode) {
		for (OrgCircleUi circle : getOrgCircles()) {
			for (OrgDivisionUi division : circle.getOrgDivisions()) {
				for (OrgEroUi ero : division.getOrgEros()) {
					for (OrgSubdivisionUi subdivision : ero.getOrgSubdivisions()) {
						for (OrgSectionUi section : subdivision.getOrgSections()) {
							for (OrgDistributionUi dist : section.getOrgDistributions()) {
								if (dist.getOdstCode().equals(distCode)) return dist;
							}
						}
					}
				}
			}
		}
		return null;
	}
}