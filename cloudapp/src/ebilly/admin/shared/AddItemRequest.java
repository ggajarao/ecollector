package ebilly.admin.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

public class AddItemRequest implements Serializable, IsSerializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 *  @gwt.typeArgs <com.college.shared.DeleteRequestEntity>
	 */
	List<CrudRequestEntity> entityList = new ArrayList<CrudRequestEntity>();
	public AddItemRequest() {
		
	}
	
	public void add(String entityName, String entityId) {
		this.entityList.add(new CrudRequestEntity(entityName,entityId));
	}
	
	public void add(String entityName, String entityId, String value1, String value2) {
		this.entityList.add(new CrudRequestEntity(entityName,entityId,value1,value2));
	}
	
	public List<CrudRequestEntity> getEntityList() {
		return this.entityList;
	}

	public void remove(String entityId) {
		if (CommonUtil.isEmpty(entityId)) {
			return;
		}
		CrudRequestEntity e = null;
		for (int i = 0; i < this.entityList.size(); i++) {
			e = this.entityList.get(i);
			if (e.getEntityId().equals(entityId)) {
				this.entityList.remove(i);
				return;
			}
		}
	}
}

