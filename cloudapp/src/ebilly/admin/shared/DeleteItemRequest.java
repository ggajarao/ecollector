package ebilly.admin.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

public class DeleteItemRequest implements Serializable, IsSerializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 *  @gwt.typeArgs <com.college.shared.DeleteRequestEntity>
	 */
	List<CrudRequestEntity> deleteEntityList = new ArrayList<CrudRequestEntity>();
	public DeleteItemRequest() {
		
	}
	
	public void add(String entityName, String entityId) {
		this.deleteEntityList.add(new CrudRequestEntity(entityName,entityId));
	}
	
	public List<CrudRequestEntity> getDeleteEntityList() {
		return this.deleteEntityList;
	}

	public void remove(String entityId) {
		if (CommonUtil.isEmpty(entityId)) {
			return;
		}
		CrudRequestEntity e = null;
		for (int i = 0; i < this.deleteEntityList.size(); i++) {
			e = this.deleteEntityList.get(i);
			if (e.getEntityId().equals(entityId)) {
				this.deleteEntityList.remove(i);
				return;
			}
		}
	}
}

