package ebilly.admin.shared.viewdef;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.user.client.rpc.IsSerializable;

public class CrudRequest implements Serializable, IsSerializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public String crudObject;
	
	public String crudAction;
	
	/**
	 * @gwt.typeArgs <java.lang.String,java.lang.String>
	 */
	private Map<String, String> fieldValueMap = new HashMap<String,String>();
	
	private Map<String, List<String>> sectionResultColumnMap = new HashMap<String, List<String>>();

	public CrudRequest() {
		
	}
	
	public void setFieldValueMap(Map<String, String> fieldValueMap) {
		this.fieldValueMap = fieldValueMap;
	}
	
	public void addFieldValue(String field, String value) {
		this.fieldValueMap.put(field, value);
	}
	
	public Map<String, String> getFieldValueMap() {
		return this.fieldValueMap;
	}
	
	public void addSectionResultColumns(String sectionName, List<String> resultColumns) {
		this.sectionResultColumnMap.put(sectionName, resultColumns);
	}
	
	public Map<String, List<String>> getSectionResultColumnMap() {
		return this.sectionResultColumnMap;
	}
}