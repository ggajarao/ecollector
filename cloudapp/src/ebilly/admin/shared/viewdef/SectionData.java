package ebilly.admin.shared.viewdef;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class SectionData implements Serializable, IsSerializable{
	
	private static final long serialVersionUID = 1L;
	
	public String sectionName;
	
	public String sectionSetName;
	
	public SearchResult searchResult;
	
	public SectionData() {
		
	}
	
	public SectionData(String sectionName, String sectionSetName, 
			SearchResult searchResult) {
		this.sectionName = sectionName;
		this.sectionSetName = sectionSetName;
		this.searchResult = searchResult;
	}
}

