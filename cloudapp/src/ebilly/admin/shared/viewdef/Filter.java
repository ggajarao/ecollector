package ebilly.admin.shared.viewdef;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Filter implements Serializable, IsSerializable {

	private static final long serialVersionUID = 1L;
	private String fieldName; 
	private String value;
	private String operator;
	private String conjunction;
	
	public Filter() {
		
	}
	
	public Filter(String fieldName, String value) {
		this.fieldName = fieldName;
		this.value = value;
	}
	
	public Filter(String fieldName, String value, String operator, String conjunction) {
		this.fieldName = fieldName;
		this.value = value;
		this.operator = operator;
		this.conjunction = conjunction;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getConjunction() {
		return conjunction;
	}

	public void setConjunction(String conjunction) {
		this.conjunction = conjunction;
	}

	@Override
	public String toString() {
		return "Filter [fieldName=" + fieldName + ", value=" + value
				+ ", operator=" + operator + ", conjunction=" + conjunction
				+ "]";
	}
	
	public boolean equals(Object o) {
		if (!(o instanceof Filter)) return false;
		Filter f2 = (Filter)o;
		
		if (!this.fieldName.equals(f2.fieldName)) return false;
		if (!this.value.equals(f2.value)) return false;
		if (!this.operator.equals(f2.operator)) return false;
		if (!this.conjunction.equals(f2.conjunction)) return false;
		
		return true;
	}
}

