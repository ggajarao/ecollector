package ebilly.admin.shared.viewdef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

import ebilly.admin.shared.AppConfig;
import ebilly.admin.shared.CommonUtil;

public class SearchRequest implements Serializable, IsSerializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String objectName;
	
	/**
	 * 
	 * @gwt.typeArgs <java.lang.String,java.lang.String>
	 */
	//private HashMap<String, String> filterCriteria = new HashMap<String, String>();
	
	private FilterCriteria filterCriteria = new FilterCriteria();
	
	/**
	 * 
	 * @gwt.typeArgs <com.college.shared.viewdef.ResultField>
	 */
	private List<ResultField> resultFields;
	
	//private SearchResultDefinition resultDef;
	
	private int fetchSize = 1000;
	private long fetchPageNumber = 0;
	
	public SearchRequest() {
		
	}
	
	/*public SearchRequest(String objectName, List<String> resultColumns) {
		this.objectName = objectName;
		// TODO: BUILD SEARCH RESULT DEF FROM RESULT COLUMNS
		//this.resultColumns = resultColumns;
	}*/
	
	public SearchRequest(String objectName, List<ResultField> resultFields) {
		this.objectName = objectName;
		this.resultFields = resultFields;
		//this.resultDef = resultDef;
	}
	
	public String getObjectName() {
		return this.objectName;
	}
	
	public void addSearchCriteria(String fieldName, String value) {
		this.addSearchCriteria(fieldName, value, AppConfig.FILTER_CONJUNCTION_AND, AppConfig.FILTER_OPERATOR_EQUALS);
		//this.filterCriteria.put(fieldName, value);
	}
	
	public void addSearchCriteria(String fieldName, String value, String conjunction, String operator) {
		//this.filterCriteria.put(fieldName, value);
		Filter filter = new Filter(fieldName,value);
		filter.setOperator(operator);
		filter.setConjunction(conjunction);
		this.filterCriteria.add(filter);
	}
	
	public FilterCriteria getFilterCriteria() {
		return this.filterCriteria;
	}

	public void setFetchSize(int fetchSize) {
		this.fetchSize = fetchSize;		
	}
	
	public int getFetchSize() {
		return this.fetchSize;
	}

	public long getPageNumber() {
		return this.fetchPageNumber ;
	}
	
	public void setPageNumber(long fetchPageNumber) {
		this.fetchPageNumber = fetchPageNumber;
	}
	
	public List<ResultField> getResultFields() {
		return this.resultFields;
		//return this.resultDef.getColumns();
	}
	
	public List<String> getResultFieldNames() {
		List<String> names = new ArrayList<String>();
		for (ResultField f : this.getResultFields()) {
			names.add(f.getFieldName());
		}
		return names;
	}
	
	public List<ResultRecord> findRecords(String fieldName, String fieldValue, List<ResultRecord> results) {
		if (results == null || results.isEmpty() || fieldName == null || fieldValue == null) {
			return new ArrayList<ResultRecord>();
		}
		List<ResultRecord> matchRecords = new ArrayList<ResultRecord>();
		String recFieldValue = null;
		for (int i = 0; i < results.size(); i++) {
			ResultRecord resultRecord = results.get(i);
			if (fieldValue.equals(this.getValue(fieldName, resultRecord))) {
				matchRecords.add(resultRecord);
			}
		}
		return matchRecords;
	}
	
    public String getValue(String fieldName, ResultRecord record) {
		if (isEmpty(fieldName)) return "";
		if (record.fieldValues == null || record.fieldValues.isEmpty() 
				|| record.fieldValues.size() != this.resultFields.size() ) {
			return "";
		}
		
		for (int i = 0; i < this.resultFields.size(); i++) {
			ResultField field = this.resultFields.get(i);
			if (fieldName.equals(field.getFieldName())) {
				return record.fieldValues.get(i);
			}
		}
		return "";
	}
    
    public void setValue(String fieldName, String value, ResultRecord record) {
    	if (isEmpty(fieldName)) return;
		if (record.fieldValues == null || record.fieldValues.isEmpty() 
				|| record.fieldValues.size() != this.resultFields.size() ) {
			return;
		}
		
		for (int i = 0; i < this.resultFields.size(); i++) {
			ResultField field = this.resultFields.get(i);
			if (fieldName.equals(field.getFieldName())) {
				record.fieldValues.remove(i);
				record.fieldValues.add(i, value);
				return;
			}
		}
    }
    
    public ResultField findField(String fieldName) {
    	if (isEmpty(fieldName)) return null;
    	
		for (int i = 0; i < this.resultFields.size(); i++) {
			ResultField field = this.resultFields.get(i);
			if (fieldName.equals(field.getFieldName())) {
				return field;
			}
		}
		return null;
    }
    
    public PivotCriteria pivotCriteriaFor(String fieldName, String pivotFunction) {
    	ResultField f = this.findField(fieldName);
    	if (f == null) return null;
    	return new PivotCriteria(f.getFieldName(),f.getFieldType(),pivotFunction); 
    }
    
    public static boolean isEmpty(String value) {
		return value == null || value.trim().length() == 0;		
	}
    
    public boolean equals(Object o) {
    	if (!(o instanceof SearchRequest)) return false;
    	SearchRequest sr2 = (SearchRequest)o;
    	if (!this.objectName.equals(sr2.objectName)) return false;
    	if (!this.filterCriteria.equals(sr2.filterCriteria)) return false;
    	if (this.resultFields.size() != sr2.resultFields.size()) return false;
    	if (this.fetchSize != sr2.fetchSize) return false;
    	if (this.fetchPageNumber != sr2.fetchPageNumber) return false;
    	return true;
    }
    
    public SearchResult convertToRawResults(SearchResult searchResult) {
    	SearchResult rawResults = new SearchResult();
    	for (ResultRecord record : searchResult.getResults()) {
    		rawResults.addResultRecord(convertToRawRecord(record));
    	}
    	return rawResults;
    }

	public ResultRecord convertToRawRecord(ResultRecord record) {
		if (record.fieldTypedValueMap == null || 
				record.fieldTypedValueMap.isEmpty()) {
			// If typed value map is empty or non existing then there is
			// nothing to do.
			return record;
		}
		record.fieldValues.clear();
		for (ResultField rf : this.resultFields) {
			record.fieldValues.add(""+record.fieldTypedValueMap.get(
					CommonUtil.toBeanProperty(rf.getFieldName())));
		}
		// PSR: Tuned.
		// Saving memory, since we are transforming the typed data
		// into fieldValues. When fieldTypedValueMap is
		// required we should transform again using initTypedData()
		// method.
		record.fieldTypedValueMap.clear();
		return record;
	}
}
