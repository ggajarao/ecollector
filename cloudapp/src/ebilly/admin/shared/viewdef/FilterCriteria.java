package ebilly.admin.shared.viewdef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ebilly.admin.shared.CommonUtil;
import com.google.gwt.user.client.rpc.IsSerializable;

public class FilterCriteria implements Serializable, IsSerializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 
	 * @gwt.typeArgs <com.college.shared.viewdef.Filter>
	 */
	private List<Filter> filterCriteria = new ArrayList<Filter>();
	
	public FilterCriteria() {
		
	}

	public void add(Filter filter) {
		this.filterCriteria.add(filter);
	}
	
	public String getValue(String fieldName) {
		Filter f = get(fieldName);
		if (f == null) return null;
		else return f.getValue();
	}
	
	public Filter get(String fieldName) {
		if (CommonUtil.isEmpty(fieldName)) return null;
		for (Filter f : this.filterCriteria) {
			if (fieldName.equals(f.getFieldName())) {
				return f;
			}
		}
		return null;
	}

	public List<Filter> getFilters() {
		return this.filterCriteria;
	}

	public Filter removeFilter(String fieldName) {
		Filter f = this.get(fieldName);
		if (f != null) {
			this.filterCriteria.remove(f);
		}
		return f;
	}
	
	public boolean equals(Object o) {
		if (!(o instanceof FilterCriteria)) return false;
		
		FilterCriteria fc2 = (FilterCriteria)o;
		if (this.filterCriteria.size() != fc2.filterCriteria.size()) return false;
		for (int i = 0; i < this.filterCriteria.size(); i++) {
			if (!this.filterCriteria.get(i).equals(fc2.filterCriteria.get(i))) return false;
		}
		return true;
	}

	public boolean doesFilterExists(String fieldName) {
		return this.get(fieldName) != null;
	}
}