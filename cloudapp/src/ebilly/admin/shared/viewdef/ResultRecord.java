package ebilly.admin.shared.viewdef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gwt.user.client.rpc.IsSerializable;

public class ResultRecord implements Serializable, IsSerializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * @gwt.typeArgs <java.lang.String>
	 */
	public List<String> fieldValues = new ArrayList<String>();

	public transient Map<String, Object> fieldTypedValueMap;
	
	public ResultRecord() {
		
	}
	
	public ResultRecord(List<String> fieldValues) {
		for (String fieldValue : fieldValues) {
			this.fieldValues.add(fieldValue);
		}
	}
	
	private Object getFieldValue(String fieldName) {
		return this.fieldTypedValueMap.get(toBeanProperty(fieldName));
	}
	
	public int intValue(String fieldName) {
		Object o = getFieldValue(fieldName);
		return (o==null)?0:(Integer)o;
	}
	
	public double doubleValue(String fieldName) {
		Object o = getFieldValue(fieldName);
		return (o==null)?0:(Double)o;
	}
	
	public long longValue(String fieldName) {
		Object o = getFieldValue(fieldName);
		return (o==null)?0:(Long)o;
	}
	
	public String stringValue(String fieldName) {
		Object o = getFieldValue(fieldName);
		return (o==null)?"":o.toString();
	}

	public Object asString() {
		StringBuffer s = new StringBuffer();
		for (String v : this.fieldValues) {
			s.append(v).append(",");
		}
		return s;
	}
	
	public void bumpCounter(String fieldName) {
		String countField = getCountFieldFor(fieldName);
		Object count = this.fieldTypedValueMap.get(countField);
		if (count == null) {
			count = Integer.valueOf("1");
		} else {
			count = Integer.valueOf(1 + ((Integer)count).intValue());
		}
		this.fieldTypedValueMap.put(countField, count);
	}
	
	public int countedValue(String fieldName) {
		String countField = getCountFieldFor(fieldName);
		Object i = getFieldValue(countField);
		return (i == null) ? 0 :(Integer)i;
	}
	
	public static String getCountFieldFor(String fieldName) {
		return fieldName+"$$COUNT";
	}
	
	public void setValue(String field, Object value) {
		this.fieldTypedValueMap.put(toBeanProperty(field), value);
	}

	@Override
	public String toString() {
		if (fieldTypedValueMap != null) {
			return fieldTypedValueMap.toString();
		} else {
			return "ResultRecord [fieldValues=" + fieldValues + "]";
		}
	}
	
	private static String toBeanProperty(String propertyName)
	{
	    if (propertyName != null && propertyName.trim().length() != 0)
	    {
	        return new StringBuilder().append(
	            Character.toLowerCase(propertyName.charAt(0))).append(
	                propertyName.substring(1)).toString();
	    }
	    return propertyName;
	}
	
	public boolean equals(Object o) {
		if (!(o instanceof ResultRecord)) return false;
		ResultRecord r2 = (ResultRecord)o;
		if (this.fieldValues.size() != r2.fieldValues.size()) return false;
		for (int i = 0; i < this.fieldValues.size(); i++) {
			if (!this.fieldValues.get(i).equals(r2.fieldValues.get(i))) return false;
		}
		return true;
	}

	public ResultRecord copy() {
		ResultRecord r = new ResultRecord();
		for(String fieldValue : this.fieldValues) {
			r.fieldValues.add(fieldValue);
		}
		if (this.fieldTypedValueMap != null) {
			for (Entry<String,Object> e : this.fieldTypedValueMap.entrySet()) {
				r.fieldTypedValueMap.put(e.getKey(), e.getValue());
			}
		}
		return r;
	}
}