package ebilly.admin.shared.viewdef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ebilly.admin.shared.CommonUtil;
import com.google.gwt.user.client.rpc.IsSerializable;

public class SearchResult implements Serializable, IsSerializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String objectName;
	
	private String errorMessage;
	
	/**
	 * gwt.typeArgs <java.util.HashMap<String, String>>
	 */
	//private List<HashMap<String, String>> results = new ArrayList<HashMap<String, String>>();
	
	/**
	 * @gwt.typeArgs <com.college.shared.viewdef.ResultRecord>
	 */
	private List<ResultRecord> results = new ArrayList<ResultRecord>();
	
	public SearchResult() {
		
	}
	
	public SearchResult(String objectName) {
		this.objectName = objectName;
	}
	
	public SearchResult(List<ResultRecord> records) {
		this.results = records;
	}

	public String getObjectName() {
		return this.objectName;
	}
	
	public List<ResultRecord> getResults() {
		return this.results;
	}
	
	public boolean hasErrors() {
		return !CommonUtil.isEmpty(this.errorMessage);
	}

	public void setErrorMessage(String message) {
		this.errorMessage = message;
	}
	
	public String getErrorMessage() {
		return this.errorMessage;
	}

	public SearchResult append(SearchResult anotherSearchResult) {
		this.results.addAll(anotherSearchResult.results);
		return this;
	}
	
	public void addResultRecord(ResultRecord resultRecord) {
		this.results.add(resultRecord);
	}

	public boolean isEmpty() {
		return this.results.isEmpty();
	}
	
	public String asString() {
		StringBuffer s = new StringBuffer();
		for (ResultRecord r : this.results) {
			s.append(r.asString()).append("\n");
		}
		return s.toString();
	}
	
	public void sort(ResultField[] orderByFields, boolean ascending, SearchRequest searchRequest) {
		Collections.sort(this.results,new ResultRecordComparator(orderByFields, ascending, searchRequest));
	}

	@Override
	public String toString() {
		return "SearchResult [objectName=" + objectName + ", errorMessage="
				+ errorMessage + ", results=" + results + "]";
	}

	public void clearFieldTypedData() {
		for (ResultRecord r : this.results) {
			r.fieldTypedValueMap = null;
		}
	}
}

class ResultRecordComparator implements Comparator<ResultRecord>, Serializable, IsSerializable {
	private static final long serialVersionUID = 1L;
	
	private ResultField[] orderByFields;
	private boolean isAscending;
	private SearchRequest searchRequest;
	public ResultRecordComparator() {
	
	}
	
	public ResultRecordComparator(
			ResultField[] orderByFields, boolean ascending,
			SearchRequest searchRequest) {
		this.orderByFields = orderByFields;
		this.isAscending = ascending;
		this.searchRequest = searchRequest;
	}

	@Override
	public int compare(ResultRecord r1, ResultRecord r2) {
		int recordResultant = 0;
		for (ResultField rf : this.orderByFields) {
			int fieldResultant = 0;
			String v1 = this.searchRequest.getValue(rf.getFieldName(), r1);
			String v2 = this.searchRequest.getValue(rf.getFieldName(), r2);
			
			if (rf.getFieldType().equals(java.lang.String.class.getName())) {
				fieldResultant = v1.compareToIgnoreCase(v2);
			} else if (rf.getFieldType().equals(java.lang.Integer.class.getName())) {
				int i1 = Integer.parseInt(v1);
				int i2 = Integer.parseInt(v2);
				fieldResultant = i1 - i2;
			} else if (rf.getFieldType().equals(java.lang.Long.class.toString())) {
				long i1 = Long.parseLong(v1);
				long i2 = Long.parseLong(v2);
				fieldResultant = (int)(i1 - i2);
			} else if (rf.getFieldType().equals(java.lang.Double.class.getName())) {
				double i1 = Double.parseDouble(v1);
				double i2 = Double.parseDouble(v2);
				fieldResultant = (int)(i1 - i2);
			}
			recordResultant = recordResultant + fieldResultant;
		}
		if (this.isAscending) {
			return recordResultant;
		} else {
			return -1 * recordResultant;
		}
	}
}
