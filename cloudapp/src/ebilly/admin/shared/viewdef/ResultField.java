package ebilly.admin.shared.viewdef;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class ResultField implements Serializable, IsSerializable {
	
	@Override
	public String toString() {
		return "ResultField [fieldName=" + fieldName + ", fieldType="
				+ fieldType + "]";
	}
	private static final long serialVersionUID = 1L;
	
	private String fieldName;
	private String fieldType;
	
	public ResultField() {
		
	}
	
	public ResultField(String fieldName, String fieldType) {
		this.fieldName = fieldName;
		this.fieldType = fieldType;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getFieldType() {
		return fieldType;
	}
	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
}

