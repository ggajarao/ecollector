package ebilly.admin.shared.viewdef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.user.client.rpc.IsSerializable;

import ebilly.admin.shared.CommonUtil;
import ebilly.admin.shared.EntityUi;

public class CrudResponse implements Serializable, IsSerializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public String errorMessage;
	public String successMessage;
	
	/**
	 * @gwt.typeArgs <java.lang.String,java.lang.String>
	 */
	public Map<String, String> fieldValueMap = new HashMap<String, String>();

	/**
	 * @gwt.typeArgs <java.lang.String,com.college.shared.viewdef.SearchResult>
	 */
	/*public Map<String, SearchResult> sectionValueMap =
		new HashMap<String, SearchResult>();*/
	
	/**
	 * @gwt.typeArgs <java.lang.String,com.college.shared.viewdef.SearchResult>
	 */
	public List<SectionData> sections = new ArrayList<SectionData>();
	
	public boolean isSuccess() {
		return errorMessage == null || errorMessage.trim().length() == 0;
	}
	
	public EntityUi crudEntity;
	
	public CrudResponse() {
		
	}
}

