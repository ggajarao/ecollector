package ebilly.admin.shared.viewdef;

public class PivotCriteria {
	private String fieldName;
	private String fieldType;
	private String pivotFunction;
	public PivotCriteria(String fieldName, String fieldType, String pivotFunction) {
		this.fieldName = fieldName;
		this.fieldType = fieldType;
		this.pivotFunction = pivotFunction;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getFieldType() {
		return fieldType;
	}
	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
	public String getPivotFunction() {
		return pivotFunction;
	}
	public void setPivotFunction(String pivotFunction) {
		this.pivotFunction = pivotFunction;
	}
	
	public boolean equals(Object o) {
		if (!(o instanceof PivotCriteria)) return false;
		PivotCriteria p2 = (PivotCriteria)o;
		if (!this.fieldName.equals(p2.fieldName)) return false;
		if (!this.fieldType.equals(p2.fieldType)) return false;
		if (!this.pivotFunction.equals(p2.pivotFunction)) return false;
		return true;
	}
}