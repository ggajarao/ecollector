package ebilly.admin.shared;


public class CollectionDashboardFilterSelection {
	public static final int COMPANY = 0;
	public static final int CIRCLE = 1;
	public static final int DIVISION = 2;
	public static final int ERO = 3;
	public static final int SUBDIVISION = 4;
	public static final int SECTION = 5;
	public static final int DISTRIBUTION = 6;
	
	private String selectedSummaryDataKey = null;
	
	private String selectedCompany = "0";
	private String selectedCircle = null;
	private String selectedDivision = null;
	private String selectedEro = null;
	private String selectedSubdivision = null;
	private String selectedSection = null;
	private String selectedDistribution = null;
	private String selectedDeviceName = null;
	
	public CollectionDashboardFilterSelection() {
		
	}
	public void setSelection(int orgType, String orgValue) {
		CollectionDashboardFilterSelection filterSelection = this;
		filterSelection.setSelectedOrgType(orgType);
		switch(orgType) {
		case CollectionDashboardFilterSelection.COMPANY:
			filterSelection.setSelectedCompany(orgValue);
			break;
		case CollectionDashboardFilterSelection.CIRCLE:
			filterSelection.setSelectedCircle(orgValue);
			break;
		case CollectionDashboardFilterSelection.DIVISION:
			filterSelection.setSelectedDivision(orgValue);
			break;
		case CollectionDashboardFilterSelection.ERO:
			filterSelection.setSelectedEro(orgValue);
			break;
		case CollectionDashboardFilterSelection.SUBDIVISION:
			filterSelection.setSelectedSubdivision(orgValue);
			break;	
		case CollectionDashboardFilterSelection.SECTION:
			filterSelection.setSelectedSection(orgValue);
			break;
		case CollectionDashboardFilterSelection.DISTRIBUTION:
			filterSelection.setSelectedDistribution(orgValue);
			break;	
		}
	}
	public String getSelectedCompany() {
		return selectedCompany;
	}
	public void setSelectedCompany(String selectedCompany) {
		this.selectedCompany = selectedCompany;
	}
	public String getSelectedCircle() {
		return selectedCircle;
	}
	public void setSelectedCircle(String selectedCircle) {
		this.selectedCircle = selectedCircle;
	}
	public String getSelectedDivision() {
		return selectedDivision;
	}
	public void setSelectedDivision(String selectedDivision) {
		this.selectedDivision = selectedDivision;
	}
	public String getSelectedEro() {
		return selectedEro;
	}
	public void setSelectedEro(String selectedEro) {
		this.selectedEro = selectedEro;
	}
	public String getSelectedSubdivision() {
		return selectedSubdivision;
	}
	public void setSelectedSubdivision(String selectedSubdivision) {
		this.selectedSubdivision = selectedSubdivision;
	}
	public String getSelectedSection() {
		return selectedSection;
	}
	public void setSelectedSection(String selectedSection) {
		this.selectedSection = selectedSection;
	}
	public String getSelectedDistribution() {
		return selectedDistribution;
	}
	public void setSelectedDistribution(String selectedDistribution) {
		this.selectedDistribution = selectedDistribution;
	}
	public String getSelectedDeviceName() {
		return selectedDeviceName;
	}
	public void setSelectedDeviceName(String selectedDeviceName) {
		this.selectedDeviceName = selectedDeviceName;
	}
	
	public String getSelectedSummaryDataKey() {
		return selectedSummaryDataKey;
	}

	public void setSelectedSummaryDataKey(String selectedSummaryDataKey) {
		this.selectedSummaryDataKey = selectedSummaryDataKey;
	}
	
	public int getSelectedOrgType() {
		if (this.selectedDistribution != null) return CollectionDashboardFilterSelection.DISTRIBUTION;
		if (this.selectedSection != null) return SECTION;
		if (this.selectedSubdivision != null) return SUBDIVISION;
		if (this.selectedEro != null) return ERO;
		if (this.selectedDivision != null) return DIVISION;
		if (this.selectedCircle != null) return CIRCLE;
		return COMPANY;
	}
	
	public void setSelectedOrgType(int orgType) {
		switch(orgType) {
		case CollectionDashboardFilterSelection.COMPANY:
			setSelectedCircle(null);
		case CollectionDashboardFilterSelection.CIRCLE:
			setSelectedDivision(null);
		case CollectionDashboardFilterSelection.DIVISION:
			setSelectedEro(null);
		case CollectionDashboardFilterSelection.ERO:
			setSelectedSubdivision(null);
		case CollectionDashboardFilterSelection.SUBDIVISION:
			setSelectedSection(null);
		case CollectionDashboardFilterSelection.SECTION:
			setSelectedDistribution(null);
		case CollectionDashboardFilterSelection.DISTRIBUTION:
				
		}
	}
	
	public String getSelectedOrgValue() {
		switch(getSelectedOrgType()) {
		case CollectionDashboardFilterSelection.COMPANY:
			return getSelectedCompany();
		case CollectionDashboardFilterSelection.CIRCLE:
			return getSelectedCircle();
		case CollectionDashboardFilterSelection.DIVISION:
			return getSelectedDivision();
		case CollectionDashboardFilterSelection.ERO:
			return getSelectedEro();
		case CollectionDashboardFilterSelection.SUBDIVISION:
			return getSelectedSubdivision();
		case CollectionDashboardFilterSelection.SECTION:
			return getSelectedSection();
		case CollectionDashboardFilterSelection.DISTRIBUTION:
			return getSelectedDistribution();
			default: 
				throw new IllegalArgumentException("Unknown Org "+getSelectedOrgType());
		}
	}
	
	/**
	 * Returns SelectedMonthKey+SelectedOrgValue
	 * @return
	 */
	public String getSelectedFilter() {
		return this.getSelectedSummaryDataKey()+this.getSelectedOrgValue();
	}
}
