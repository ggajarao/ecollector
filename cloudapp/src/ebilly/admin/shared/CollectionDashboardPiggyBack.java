package ebilly.admin.shared;

import java.util.ArrayList;
import java.util.List;

import ebilly.admin.shared.orgstruct.OrgCompanyUi;
import ebilly.admin.shared.viewdef.ResultField;

public class CollectionDashboardPiggyBack extends EntityUi {
	private static final long serialVersionUID = 1L;
	
	private OrgCompanyUi orgCompanyUi = null;
	
	/**
	 * 
	 * @gwt.typeArgs <java.lang.String>
	 */
	private List<String> summaryDataKeys = null;
	
	
	/*transient private Map<String, SearchResult> orgDimResultMap = 
		new HashMap<String, SearchResult>();*/
	
	transient private static List<ResultField> resultFields = new ArrayList<ResultField>();
	static {
		resultFields.add(new ResultField("GRAND_TOTAL",java.lang.String.class.getName()));
		resultFields.add(new ResultField("TOTAL_PRS",java.lang.String.class.getName()));
		resultFields.add(new ResultField("ARREARS_N_DEMAND",java.lang.String.class.getName()));
		resultFields.add(new ResultField("CMD_COLLECTED",java.lang.String.class.getName()));
		resultFields.add(new ResultField("RC_COLLECTED",java.lang.String.class.getName()));
		resultFields.add(new ResultField("ACD_COLLECTED",java.lang.String.class.getName()));
		resultFields.add(new ResultField("AGL_AMOUNT",java.lang.String.class.getName()));
		resultFields.add(new ResultField("COLLECTION_DATE",java.lang.String.class.getName()));
	}
	public CollectionDashboardPiggyBack() {
		
	}

	public OrgCompanyUi getOrgCompanyUi() {
		return orgCompanyUi;
	}

	public void setOrgCompanyUi(OrgCompanyUi orgCompanyUi) {
		this.orgCompanyUi = orgCompanyUi;
	}

	public List<String> getSummaryDataKeys() {
		return summaryDataKeys;
	}

	public void setSummaryDataKeys(List<String> summaryDataKeys) {
		this.summaryDataKeys = summaryDataKeys;
	}
	
	/*public void fetchOrgDimResult(CollectionDashboardFilterSelection selection, 
			ActionListener onResultsAvailable) {
		String orgValue = selection.getSelectedOrgValue();
		SearchResult result = orgDimResultMap.get(orgValue);
		if (result != null) {
			onResultsAvailable.onAfterAction(result);
		} else {
			loadDimResult(selection, onResultsAvailable);
		}
	}*/

	/*private void loadDimResult(
			final CollectionDashboardFilterSelection selection, 
			final ActionListener onResultsAvailable) {
		SearchRequest request = null;
		SummarySearchRequestFactory requestFactory = null;
		switch(selection.getSelectedOrgType()) {
		case CollectionDashboardFilterSelection.COMPANY:
			requestFactory = SummarySearchRequestFactory.companySearchRequestFactory;
			break;
		case CollectionDashboardFilterSelection.CIRCLE:
			requestFactory = SummarySearchRequestFactory.circleSearchRequestFactory;
			break;
		case CollectionDashboardFilterSelection.DIVISION:
			requestFactory = SummarySearchRequestFactory.divisionSearchRequestFactory;
			break;
		case CollectionDashboardFilterSelection.ERO:
			requestFactory = SummarySearchRequestFactory.eroSearchRequestFactory;
			break;
		case CollectionDashboardFilterSelection.SUBDIVISION:
			requestFactory = SummarySearchRequestFactory.subdivisionSearchRequestFactory;
			break;
		case CollectionDashboardFilterSelection.SECTION:
			requestFactory = SummarySearchRequestFactory.sectionSearchRequestFactory;
			break;
		case CollectionDashboardFilterSelection.DISTRIBUTION:
			requestFactory = SummarySearchRequestFactory.distributionSearchRequestFactory;
			break;		
		}
		request = requestFactory.createSearchRequest(selection.getSelectedOrgValue(), 
				selection.getSelectedSummaryDataKey());
		
		Application.secureService.searchView(request, new BaseServiceResponseHandler<SearchResult>() {
			@Override
			public void onSuccess(SearchResult result) {
				orgDimResultMap.put(selection.getSelectedOrgValue(), result);
				onResultsAvailable.onAfterAction(result);
			}
		} );
	}*/
}