package ebilly.admin.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

public class DeviceUi extends EntityUi implements Serializable, IsSerializable {
	
	private String id;
	private String pairingStatus = "";
	private String deviceIdentifier = "";
	private String deviceName;
	private String deviceNameSearchable;
	private String deviceServicesFileKey;
	private List<DeviceFileUi> deviceFiles;
	private Date collectionExpirationDate;
	private int collectionLimit;
	private Date cloudTime;
	private List<DeviceCommandUi> deviceCommands;
	private DeviceSummaryReport deviceSummaryReport;
	
	public DeviceUi() {
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPairingStatus() {
		return pairingStatus;
	}

	public void setPairingStatus(String pairingStatus) {
		this.pairingStatus = pairingStatus;
	}

	public String getDeviceIdentifier() {
		return deviceIdentifier;
	}

	public void setDeviceIdentifier(String deviceIdentifier) {
		this.deviceIdentifier = deviceIdentifier;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getDeviceNameSearchable() {
		return deviceNameSearchable;
	}

	public void setDeviceNameSearchable(String deviceNameSearchable) {
		this.deviceNameSearchable = deviceNameSearchable;
	}

	public String getDeviceServicesFileKey() {
		return deviceServicesFileKey;
	}

	public void setDeviceServicesFileKey(String deviceServicesFileKey) {
		this.deviceServicesFileKey = deviceServicesFileKey;
	}
	
	public List<DeviceFileUi> getDeviceFiles() {
		if (this.deviceFiles == null) {
			this.deviceFiles = new ArrayList<DeviceFileUi>();
		}
		return this.deviceFiles;
	}
	
	public void addDeviceFile(DeviceFileUi deviceFile) {
		this.getDeviceFiles().add(deviceFile);
	}

	public Date getCollectionExpirationDate() {
		return collectionExpirationDate;
	}

	public void setCollectionExpirationDate(Date collectionExpirationDate) {
		this.collectionExpirationDate = collectionExpirationDate;
	}

	public int getCollectionLimit() {
		return collectionLimit;
	}

	public void setCollectionLimit(int collectionLimit) {
		this.collectionLimit = collectionLimit;
	}

	public Date getCloudTime() {
		return cloudTime;
	}

	public void setCloudTime(Date cloudTime) {
		this.cloudTime = cloudTime;
	}
	
	public List<DeviceCommandUi> getDeviceCommands() {
		if (this.deviceCommands == null) {
			this.deviceCommands = new ArrayList<DeviceCommandUi>();
		}
		return this.deviceCommands;
	}
	
	public void addDeviceCommand(DeviceCommandUi deviceCommand) {
		this.getDeviceCommands().add(deviceCommand);
	}

	public DeviceSummaryReport deviceSummaryReport() {
		return deviceSummaryReport;
	}

	public void setDeviceSummaryReport(DeviceSummaryReport deviceSummaryReport) {
		this.deviceSummaryReport = deviceSummaryReport;
	}
}