package ebilly.admin.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

public class EntitySelection implements Serializable, IsSerializable{
	private static final long serialVersionUID = 1L;
	/**
	 *  @gwt.typeArgs <com.college.shared.DeleteRequestEntity>
	 */
	List<EntityUi> entityList = new ArrayList<EntityUi>();
	public EntitySelection() {
		
	}
	
	public void add(String entityName, String entityId) {
		this.entityList.add(new EntityUi(entityName,entityId));
	}
	
	public List<EntityUi> getDeleteEntityList() {
		return this.entityList;
	}
	
	public boolean isEmpty() {
		return this.entityList.isEmpty();
	}

	public void remove(String entityId) {
		if (CommonUtil.isEmpty(entityId)) {
			return;
		}
		EntityUi e = null;
		for (int i = 0; i < this.entityList.size(); i++) {
			e = this.entityList.get(i);
			if (e.getEntityId().equals(entityId)) {
				this.entityList.remove(i);
				return;
			}
		}
	}

	public List<String> getEntityIds() {
		List<String> entityIds = new ArrayList<String>();
		for (EntityUi e : this.entityList) {
			entityIds.add(e.getEntityId());
		}
		return entityIds;
	}

	public void clear() {
		this.entityList.clear();
	}

	public boolean isSelected(String entityId) {
		return findEntity(entityId) != null;
	}
	
	private EntityUi findEntity(String entityId) {
		for (EntityUi e : this.entityList) {
			if (e.getEntityId().equals(entityId)) {
				return e;
			}
		}
		return null;
	}
}
