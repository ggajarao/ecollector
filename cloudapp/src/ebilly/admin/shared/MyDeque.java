package ebilly.admin.shared;

import java.util.ArrayList;

public class MyDeque<E> {
    private ArrayList<E> list;
    public MyDeque() {
        list = new ArrayList<E>();
    }

    public E pop() {
        if (list.size() == 0) return null;
        return list.remove(list.size()-1);
    }

    public void push(E element) {
        list.add(list.size(), element);
    }
    
    public E peek() {
    	if (list.size() == 0) return null;
        return list.get(list.size()-1);
    }
    
    public int size() {
        return list.size();
    }
}