package ebilly.admin.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class UserContext implements IsSerializable, Serializable {
	
	private static final long serialVersionUID = 1L;

	private String loginId;
	
	private String userId;
	
	private String roleCode;
	
	private DeviceUi deviceUi;
	
	public UserContext() {
		
	}
	
	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String id) {
		this.userId = id;
	}

	public DeviceUi getDeviceUi() {
		return deviceUi;
	}

	public void setDeviceUi(DeviceUi deviceUi) {
		this.deviceUi = deviceUi;
	}
	
	public boolean isDeviceUser() {
		return AppConfig.ROLE_CODE_DEVICE_USER.equalsIgnoreCase(this.roleCode);
	}
}