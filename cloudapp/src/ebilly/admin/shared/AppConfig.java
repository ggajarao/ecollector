package ebilly.admin.shared;

public class AppConfig {
	
	// App Roles
	public static final String ROLE_CODE_SUPER_ADMIN = "SUPER_ADMIN";
	public static final String ROLE_CODE_APP_ADMIN = "APP_ADMIN";
	public static final String ROLE_CODE_AAO_USER = "AAO_USER";
	public static final String ROLE_CODE_CASHIER_USER = "CASHIER_USER";
	public static final String ROLE_CODE_DEVICE_USER = "DEVICE_USER";
	
	// Import Objects
	public static final String OBJ_USER = "User";
	public static final String OBJ_IMPORT_REQUEST = "Import Request";
	//public static final String OBJ_SERVICES = "Services";
	public static final String OBJ_DEVICE_SERVICE_FILE = "Device Service File";
	public static final String OBJ_DEVICE = "Device";
	public static final String OBJ_PR = "PR";
	public static final String OBJ_PR_LIVE_REPORT = "pr_live";
	public static final String OBJ_PR_AGGREGATE_LIVE_REPORT = "pr_agg_live";
	public static final String OBJ_PR_REPORT = "pr_report";
	public static final String OBJ_PR_AGGREGATE_REPORT = "pr_agg_report";
	public static final String OBJ_EXPORT_REQUEST = "Export Request";
	public static final String OBJ_PR_SUMMARY = "PR Summary";
	public static final String OBJ_PR_HEADS_REPORT = "pr_heads_report";
	
	// Crud Actions
	public static final String CRUD_ACTION_CREATE_UPDATE = "act_c_u";
	public static final String CRUD_ACTION_RETRIEVE = "act_r";
	public static final String CRUD_USER = "User";
	public static final String CRUD_DEVICE = "Device";
	public static final String CRUD_APP_CONFIG = "App Config";
	public static final String CRUD_CACHE_STATISTICS = "Cache Statistics";
	public static final String CRUD_COLLECTION_DASHBOARD_DATA = "collectionDashboardData";
	
	// Application cache keys
	//public static final String CACHE_EXAM_NOTIFICATION = "ExamNotification";
	public static final String CACHE_DEVICE_BY_DEVICE_NAME = "deviceByName";
	public static final String CACHE_ROLES = "ROLES";
	public static final String CACHE_IMPORT_REQUEST_QUEUE = "ImportRequestQueue";
	public static final String CACHE_LAST_EMAIL_SEND_FAILURE = "lastEmailSendFailure";
	public static final String CACHE_NEW_DEVICE_COMMANDS_BY_DEVICE_NAME = "deviceCommandByName";
	public static final String CACHE_DEVICE_SUMMARY_BY_DEVICE_ID = "device.name.summary.";
	public static final String CACHE_DEVICE_BY_ID = "device.id.";
	public static final String CACHE_ORG_STRUCTURE_DATA = "orgStructureData";
	public static final String CACHE_SUMMARY_DATA_KEY_LIST = "summaryData.keys";
	
	// Field types
	public static final String FIELD_TYPE_PASSWORD = "password";
	
	// Email service constants
	public static final String EMAIL_APP_NOTIFY_ADDRESS = "vamsi.vamsi@gmail.com";
	public static final String EMAIL_FROM_ADDRESS = "vamsi.vamsi@gmail.com";
	
	public static final String FILTER_CONJUNCTION_AND = "&&";
	public static final String FILTER_OPERATOR_EQUALS = "==";
	public static final String FILTER_OPERATOR_GREATER_EQUALS = ">=";
	public static final String FILTER_OPERATOR_LESS_EQUALS = "<=";
	
	/**
	 * PR Summary Filters
	 */
	public static final String F_MONTH_FILTER = "MONTH_FILTER";
	public static final String F_COMPANY = "company";
	public static final String F_CIRCLE = "circle";
	public static final String F_DIVISION = "division";
	public static final String F_ERO = "ero";
	public static final String F_SUBDIVISION = "subdivision";
	public static final String F_SECTION = "section";
	public static final String F_DISTRIBUTION = "distribution";
	
	// Mime types
	public static final String MIME_TYPE_JSON = "application/json";
	
	// Messages
	public static final String MESSAGE_INVALID_EMAIL = "Email Address is invalid, should of form user@domain.subdomain. Eg: ramu@acollege.com";

	public static final String SECTION_DEVICE_FILES = "Device files";
	public static final String CRUD_ORGENIZATION = "Orgenization";
	public static final String OBJ_MONTH_SUMMARY = "Monthly Collection Summary";
	public static final String PR_SUMMARY_MONTH_AGGREGATE_LABEL = "Month";
	
	public static enum IMPORT_STATUS {
		NEW(0), INPROGRESS(1), COMPLETED(2), DELETED(3), FAILED(4);
		private int code;
		IMPORT_STATUS(int code) {
			this.code = code;
		}
		public int getCode() {
			return this.code;
		}
		public static IMPORT_STATUS asValue(int i) {
			switch (i) {
			case 0: return NEW;
			case 1: return INPROGRESS;
			case 2: return COMPLETED;
			case 3: return DELETED;
			case 4: return FAILED;
			}
			return null;
		}
	};
	
	public static enum EXPORT_STATUS {
		NEW(0), INPROGRESS(1), COMPLETED(2), DELETED(3), FAILED(4);
		private int code;
		EXPORT_STATUS(int code) {
			this.code = code;
		}
		public int getCode() {
			return this.code;
		}
		public static EXPORT_STATUS asValue(int i) {
			switch (i) {
			case 0: return NEW;
			case 1: return INPROGRESS;
			case 2: return COMPLETED;
			case 3: return DELETED;
			case 4: return FAILED;
			}
			return null;
		}
	};
	
	public enum REMOTE_DEVICE_UPLOAD_STATES {
		READY_TO_UPLOAD_TO_REMOTE_DEVICE,
		UPLOADED_TO_REMOTE_DEVICE
	};
	
	public enum REMOTE_DEVICE_IMPORT_METHOD {
		CLEAR_EXISTING_DATA,
		ADD_TO_EXISTING_DATA
	}
	
	public enum DEVICE_COMMAND_STATES {
		NEW,
		DELETED,
		DEVICE_RECEIVED,
		EXECUTED,
		EXECUTION_FAILED,
		READY_TO_EXECUTE,
		EXECUTING,
		UPDATED_TO_CLOUD;
		
		public String toString() {
			int v = -1;
			switch (this) {
			case NEW: v = 1; break;
			case DELETED: v = 2; break;
			case DEVICE_RECEIVED: v = 3; break;
			case EXECUTED : v = 4; break;
			case EXECUTION_FAILED : v = 5; break;
			case READY_TO_EXECUTE : v = 6; break;
			case EXECUTING : v = 7; break;
			case UPDATED_TO_CLOUD : v = 8; break;
			}
			return (v < 0)?"":v+"";
		}
		
		public static DEVICE_COMMAND_STATES asEnumValue(String value) {
			int i = -1;
			try {
				i = Integer.parseInt(value);
			} catch(NumberFormatException n) {}
			switch (i) {
			case 1: return NEW;
			case 2: return DELETED;
			case 3: return DEVICE_RECEIVED;
			case 4: return EXECUTED;
			case 5: return EXECUTION_FAILED;
			case 6: return READY_TO_EXECUTE;
			case 7: return EXECUTING;
			case 8: return UPDATED_TO_CLOUD;
			default: return null;
			}
		}
	};
	
	public enum DEVICE_COMMAND {
		RESET_COLLECTION
	}
}
