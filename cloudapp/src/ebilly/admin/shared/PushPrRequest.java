package ebilly.admin.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

public class PushPrRequest implements Serializable, IsSerializable {
	private static final long serialVersionUID = 1L;
	
	public String deviceIdentifier;
	
	public List<DevicePrUi> devicePrs = new ArrayList<DevicePrUi>();
}
