package ebilly.admin.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class RoleUi implements Serializable, IsSerializable {
	private String id;
	private String roleCode;
	private String roleName;
	private String roleDescription;
	
	public RoleUi() {
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleDescription() {
		return roleDescription;
	}

	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}
}