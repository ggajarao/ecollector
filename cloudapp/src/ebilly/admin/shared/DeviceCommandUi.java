package ebilly.admin.shared;

import java.io.Serializable;
import java.util.Date;

import com.google.gwt.user.client.rpc.IsSerializable;

public class DeviceCommandUi extends EntityUi implements Serializable, IsSerializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String deviceName;
	private String state;
	private Date commandDate;
	private String commandCode;
	private String commandParam1;
	private String commandParam2;
	private String commandParam3;
	private Date executionDate;
	private String executionNotes;
	private long commandTimestamp;
	
	public DeviceCommandUi() {
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Date getCommandDate() {
		return commandDate;
	}

	public void setCommandDate(Date commandDate) {
		this.commandDate = commandDate;
	}

	public String getCommandCode() {
		return commandCode;
	}

	public void setCommandCode(String commandCode) {
		this.commandCode = commandCode;
	}

	public String getCommandParam1() {
		return commandParam1;
	}

	public void setCommandParam1(String commandParam1) {
		this.commandParam1 = commandParam1;
	}

	public String getCommandParam2() {
		return commandParam2;
	}

	public void setCommandParam2(String commandParam2) {
		this.commandParam2 = commandParam2;
	}

	public String getCommandParam3() {
		return commandParam3;
	}

	public void setCommandParam3(String commandParam3) {
		this.commandParam3 = commandParam3;
	}

	public Date getExecutionDate() {
		return executionDate;
	}

	public void setExecutionDate(Date executionDate) {
		this.executionDate = executionDate;
	}

	public String getExecutionNotes() {
		return executionNotes;
	}

	public void setExecutionNotes(String executionNotes) {
		this.executionNotes = executionNotes;
	}

	public long getCommandTimestamp() {
		return commandTimestamp;
	}

	public void setCommandTimestamp(long commandTimestamp) {
		this.commandTimestamp = commandTimestamp;
	}
}
