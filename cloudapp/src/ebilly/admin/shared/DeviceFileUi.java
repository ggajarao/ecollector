package ebilly.admin.shared;

import java.io.Serializable;
import java.util.Date;

import com.google.gwt.user.client.rpc.IsSerializable;

public class DeviceFileUi implements Serializable, IsSerializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String deviceServicesFileKey;
	private String deviceServicesFileName;
	private Date deviceFileStagingDate;
	private String remoteDeviceFileUploadState;
	private Date remoteDeviceFileUploadDate;
	private String remoteDeviceImportMethod;
	
	public DeviceFileUi() {
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDeviceServicesFileKey() {
		return deviceServicesFileKey;
	}

	public void setDeviceServicesFileKey(String deviceServicesFileKey) {
		this.deviceServicesFileKey = deviceServicesFileKey;
	}

	public String getDeviceServicesFileName() {
		return deviceServicesFileName;
	}

	public void setDeviceServicesFileName(String deviceServicesFileName) {
		this.deviceServicesFileName = deviceServicesFileName;
	}

	public Date getDeviceFileStagingDate() {
		return deviceFileStagingDate;
	}

	public void setDeviceFileStagingDate(Date deviceFileStagingDate) {
		this.deviceFileStagingDate = deviceFileStagingDate;
	}

	public String getRemoteDeviceFileUploadState() {
		return remoteDeviceFileUploadState;
	}

	public void setRemoteDeviceFileUploadState(String remoteDeviceFileUploadState) {
		this.remoteDeviceFileUploadState = remoteDeviceFileUploadState;
	}

	public Date getRemoteDeviceFileUploadDate() {
		return remoteDeviceFileUploadDate;
	}

	public void setRemoteDeviceFileUploadDate(Date remoteDeviceFileUploadDate) {
		this.remoteDeviceFileUploadDate = remoteDeviceFileUploadDate;
	}
	
	public String getRemoteDeviceImportMethod() {
		return remoteDeviceImportMethod;
	}

	public void setRemoteDeviceImportMethod(String remoteDeviceImportMethod) {
		this.remoteDeviceImportMethod = remoteDeviceImportMethod;
	}
}
