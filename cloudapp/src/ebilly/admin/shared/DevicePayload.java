package ebilly.admin.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class DevicePayload implements IsSerializable, Serializable {
	private static final long serialVersionUID = 1L;
	
	public DeviceSummaryReport deviceSummaryReport;
	
	public DevicePayload() {
		
	}
}
