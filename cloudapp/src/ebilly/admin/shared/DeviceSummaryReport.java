package ebilly.admin.shared;

import java.util.Date;

public class DeviceSummaryReport extends EntityUi {
	private static final long serialVersionUID = 1L;
	
	private String mTotalServicesCount;
	private String mTotalPrCount;
	private String mTotalReadyToPushServicesCount;
	private String mTotalCollection;
	private String mCollectionMargin;
	private String mTotalPushedServicesCount;
	private Date mReportDate;
	public DeviceSummaryReport() {
		
	}

	public String getmTotalServicesCount() {
		return mTotalServicesCount;
	}

	public void setmTotalServicesCount(String mTotalServicesCount) {
		this.mTotalServicesCount = mTotalServicesCount;
	}

	public String getmTotalPrCount() {
		return mTotalPrCount;
	}

	public void setmTotalPrCount(String mTotalPrCount) {
		this.mTotalPrCount = mTotalPrCount;
	}

	public String getmTotalReadyToPushServicesCount() {
		return mTotalReadyToPushServicesCount;
	}

	public void setmTotalReadyToPushServicesCount(String mTotalReadyToPushServicesCount) {
		this.mTotalReadyToPushServicesCount = mTotalReadyToPushServicesCount;
	}

	public String getmTotalCollection() {
		return mTotalCollection;
	}

	public void setmTotalCollection(String mTotalCollection) {
		this.mTotalCollection = mTotalCollection;
	}

	public String getmCollectionMargin() {
		return mCollectionMargin;
	}

	public void setmCollectionMargin(String mCollectionMargin) {
		this.mCollectionMargin = mCollectionMargin;
	}

	public String getmTotalPushedServicesCount() {
		return mTotalPushedServicesCount;
	}

	public void setmTotalPushedServicesCount(String mTotalPushedServicesCount) {
		this.mTotalPushedServicesCount = mTotalPushedServicesCount;
	}

	public Date getmReportDate() {
		return mReportDate;
	}

	public void setmReportDate(Date mReportDate) {
		this.mReportDate = mReportDate;
	}
}
