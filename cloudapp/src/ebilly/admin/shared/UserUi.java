package ebilly.admin.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class UserUi implements Serializable, IsSerializable {
	private String id;
	private String loginId;
	private String password;
	private String email;
	private String roleId;
	
	public UserUi() {
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
}