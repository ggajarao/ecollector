package ebilly.admin.shared;


public class DevicePrUi extends EntityUi {
	
	private String id;
	private String USC_NO;
	private String SC_NO;
	private String ERO;
	private String SECTION;
	private String DISTRIBUTION;
	private String RECEIPT_NUMBER;
	private String COLLECTION_DATE;
	private String COLLECTION_TIME;
	private String ARREARS_N_DEMAND;
	private String RC_COLLECTED;
	private String ACD_COLLECTED;
	private String OTHERS_COLLECTED;
	private String RC_CODE;
	private String MACHINE_CODE;
	private String LAST_PAID_RECEIPT_NO;
	private String LAST_PAID_DATE;
	private String LAST_PAID_AMOUNT;
	private String TC_SEAL_NO;
	private String REMARKS;
	private String REMARKS_AMOUNT;
	private String AGL_AMOUNT;
	private String AGL_SERVICES;
	private String MACHINE_NUMBER;
	private String NEW_ARREARS;
	private String CMD_COLLECTED;
	private String COLLECTION_DELTA;
	private String AMOUNT_COLLECTED;
	/* End of Pr fields */
	private String _DEVICE_PR_CODE; 
	private String deviceName;
	
	public DevicePrUi() {
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUSC_NO() {
		return USC_NO;
	}

	public void setUSC_NO(String uSC_NO) {
		USC_NO = uSC_NO;
	}

	public String getSC_NO() {
		return SC_NO;
	}

	public void setSC_NO(String sC_NO) {
		SC_NO = sC_NO;
	}

	public String getERO() {
		return ERO;
	}

	public void setERO(String eRO) {
		ERO = eRO;
	}

	public String getSECTION() {
		return SECTION;
	}

	public void setSECTION(String sECTION) {
		SECTION = sECTION;
	}

	public String getDISTRIBUTION() {
		return DISTRIBUTION;
	}

	public void setDISTRIBUTION(String dISTRIBUTION) {
		DISTRIBUTION = dISTRIBUTION;
	}

	public String getRECEIPT_NUMBER() {
		return RECEIPT_NUMBER;
	}

	public void setRECEIPT_NUMBER(String rECEIPT_NUMBER) {
		RECEIPT_NUMBER = rECEIPT_NUMBER;
	}

	public String getCOLLECTION_DATE() {
		return COLLECTION_DATE;
	}

	public void setCOLLECTION_DATE(String cOLLECTION_DATE) {
		COLLECTION_DATE = cOLLECTION_DATE;
	}

	public String getCOLLECTION_TIME() {
		return COLLECTION_TIME;
	}

	public void setCOLLECTION_TIME(String cOLLECTION_TIME) {
		COLLECTION_TIME = cOLLECTION_TIME;
	}

	public String getARREARS_N_DEMAND() {
		return ARREARS_N_DEMAND;
	}

	public void setARREARS_N_DEMAND(String aRREARS_N_DEMAND) {
		ARREARS_N_DEMAND = aRREARS_N_DEMAND;
	}

	public String getRC_COLLECTED() {
		return RC_COLLECTED;
	}

	public void setRC_COLLECTED(String rC_COLLECTED) {
		RC_COLLECTED = rC_COLLECTED;
	}

	public String getACD_COLLECTED() {
		return ACD_COLLECTED;
	}

	public void setACD_COLLECTED(String aCD_COLLECTED) {
		ACD_COLLECTED = aCD_COLLECTED;
	}

	public String getOTHERS_COLLECTED() {
		return OTHERS_COLLECTED;
	}

	public void setOTHERS_COLLECTED(String oTHERS_COLLECTED) {
		OTHERS_COLLECTED = oTHERS_COLLECTED;
	}

	public String getRC_CODE() {
		return RC_CODE;
	}

	public void setRC_CODE(String rC_CODE) {
		RC_CODE = rC_CODE;
	}

	public String getMACHINE_CODE() {
		return MACHINE_CODE;
	}

	public void setMACHINE_CODE(String mACHINE_CODE) {
		MACHINE_CODE = mACHINE_CODE;
	}

	public String getLAST_PAID_RECEIPT_NO() {
		return LAST_PAID_RECEIPT_NO;
	}

	public void setLAST_PAID_RECEIPT_NO(String lAST_PAID_RECEIPT_NO) {
		LAST_PAID_RECEIPT_NO = lAST_PAID_RECEIPT_NO;
	}

	public String getLAST_PAID_DATE() {
		return LAST_PAID_DATE;
	}

	public void setLAST_PAID_DATE(String lAST_PAID_DATE) {
		LAST_PAID_DATE = lAST_PAID_DATE;
	}

	public String getLAST_PAID_AMOUNT() {
		return LAST_PAID_AMOUNT;
	}

	public void setLAST_PAID_AMOUNT(String lAST_PAID_AMOUNT) {
		LAST_PAID_AMOUNT = lAST_PAID_AMOUNT;
	}

	public String getTC_SEAL_NO() {
		return TC_SEAL_NO;
	}

	public void setTC_SEAL_NO(String tC_SEAL_NO) {
		TC_SEAL_NO = tC_SEAL_NO;
	}

	public String getREMARKS() {
		return REMARKS;
	}

	public void setREMARKS(String rEMARKS) {
		REMARKS = rEMARKS;
	}

	public String getREMARKS_AMOUNT() {
		return REMARKS_AMOUNT;
	}

	public void setREMARKS_AMOUNT(String rEMARKS_AMOUNT) {
		REMARKS_AMOUNT = rEMARKS_AMOUNT;
	}

	public String getAGL_AMOUNT() {
		return AGL_AMOUNT;
	}

	public void setAGL_AMOUNT(String aGL_AMOUNT) {
		AGL_AMOUNT = aGL_AMOUNT;
	}

	public String getAGL_SERVICES() {
		return AGL_SERVICES;
	}

	public void setAGL_SERVICES(String aGL_SERVICES) {
		AGL_SERVICES = aGL_SERVICES;
	}

	public String getMACHINE_NUMBER() {
		return MACHINE_NUMBER;
	}

	public void setMACHINE_NUMBER(String mACHINE_NUMBER) {
		MACHINE_NUMBER = mACHINE_NUMBER;
	}

	public String getNEW_ARREARS() {
		return NEW_ARREARS;
	}

	public void setNEW_ARREARS(String nEW_ARREARS) {
		NEW_ARREARS = nEW_ARREARS;
	}

	public String getCMD_COLLECTED() {
		return CMD_COLLECTED;
	}

	public void setCMD_COLLECTED(String cMD_COLLECTED) {
		CMD_COLLECTED = cMD_COLLECTED;
	}

	public String getCOLLECTION_DELTA() {
		return COLLECTION_DELTA;
	}

	public void setCOLLECTION_DELTA(String cOLLECTION_DELTA) {
		COLLECTION_DELTA = cOLLECTION_DELTA;
	}

	public String get_DEVICE_PR_CODE() {
		return _DEVICE_PR_CODE;
	}

	public void set_DEVICE_PR_CODE(String _DEVICE_PR_CODE) {
		this._DEVICE_PR_CODE = _DEVICE_PR_CODE;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getAMOUNT_COLLECTED() {
		return AMOUNT_COLLECTED;
	}

	public void setAMOUNT_COLLECTED(String aMOUNT_COLLECTED) {
		AMOUNT_COLLECTED = aMOUNT_COLLECTED;
	}

	@Override
	public String toString() {
		return "DevicePrUi [id=" + id + ", USC_NO=" + USC_NO + ", SC_NO="
				+ SC_NO + ", ERO=" + ERO + ", SECTION=" + SECTION
				+ ", DISTRIBUTION=" + DISTRIBUTION + ", RECEIPT_NUMBER="
				+ RECEIPT_NUMBER + ", COLLECTION_DATE=" + COLLECTION_DATE
				+ ", COLLECTION_TIME=" + COLLECTION_TIME
				+ ", ARREARS_N_DEMAND=" + ARREARS_N_DEMAND + ", RC_COLLECTED="
				+ RC_COLLECTED + ", ACD_COLLECTED=" + ACD_COLLECTED
				+ ", OTHERS_COLLECTED=" + OTHERS_COLLECTED + ", RC_CODE="
				+ RC_CODE + ", MACHINE_CODE=" + MACHINE_CODE
				+ ", LAST_PAID_RECEIPT_NO=" + LAST_PAID_RECEIPT_NO
				+ ", LAST_PAID_DATE=" + LAST_PAID_DATE + ", LAST_PAID_AMOUNT="
				+ LAST_PAID_AMOUNT + ", TC_SEAL_NO=" + TC_SEAL_NO
				+ ", REMARKS=" + REMARKS + ", REMARKS_AMOUNT=" + REMARKS_AMOUNT
				+ ", AGL_AMOUNT=" + AGL_AMOUNT + ", AGL_SERVICES="
				+ AGL_SERVICES + ", MACHINE_NUMBER=" + MACHINE_NUMBER
				+ ", NEW_ARREARS=" + NEW_ARREARS + ", CMD_COLLECTED="
				+ CMD_COLLECTED + ", COLLECTION_DELTA=" + COLLECTION_DELTA
				+ ", _DEVICE_PR_CODE=" + _DEVICE_PR_CODE + ", deviceName="
				+ deviceName + "AMOUNT_COLLECTED=" + AMOUNT_COLLECTED +"]";
	}
}
