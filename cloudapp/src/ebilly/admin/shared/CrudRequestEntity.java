package ebilly.admin.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class CrudRequestEntity implements Serializable, IsSerializable {
	private String entityName;
	private String entityId;
    private String value1;
    private String value2;
	public CrudRequestEntity() {
		
	}
	
	public CrudRequestEntity(String entityName, String entityId) {
		this.entityName = entityName;
		this.entityId = entityId;
	}
	
	public CrudRequestEntity(String entityName, String entityId, String value1, String value2) {
		this.entityName = entityName;
		this.entityId = entityId;
		this.value1 = value1;
		this.value2 = value2;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getValue1() {
		return value1;
	}

	public void setValue1(String value1) {
		this.value1 = value1;
	}

	public String getValue2() {
		return value2;
	}

	public void setValue2(String value2) {
		this.value2 = value2;
	}
}