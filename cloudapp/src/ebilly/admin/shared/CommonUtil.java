package ebilly.admin.shared;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

//import com.google.gwt.i18n.client.DateTimeFormat;
//import com.google.gwt.regexp.shared.RegExp;

public class CommonUtil {
	
	public static final String DEFAULT_DATE_FORMAT = "dd/MM/yyyy";
	public static final String DEFAULT_DATE_TIME_FORMAT = "dd/MM/yyyy hh:mm:ss a";
	public static final String MONTH_YEAR = "MMM-yyyy";
	public static final String YEAR = "yyyy";
	
	public static boolean isEmpty(String value) {
		return value == null || value.trim().length() == 0;		
	}

	// Refactored to UiUtil 
	/*public static Date getDateFromString(String value) {
		if (CommonUtil.isEmpty(value)) return null;
		DateTimeFormat dtFormat = DateTimeFormat.getFormat(CommonUtil.DEFAULT_DATE_FORMAT);
		return dtFormat.parse(value.trim());
	}
	
	public static Date getDateFromString(String value, String format) {
		if (CommonUtil.isEmpty(value)) return null;
		DateTimeFormat dtFormat = DateTimeFormat.getFormat(CommonUtil.DEFAULT_DATE_FORMAT);
		return dtFormat.parse(value.trim());
	}
	
	public static String getFormattedDate(Date date) {
		if (date == null) return "";
		DateTimeFormat dtFormat = DateTimeFormat.getFormat(CommonUtil.DEFAULT_DATE_FORMAT);
		return dtFormat.format(date);
	}*/
	
	public static String getReadableDate(String date) {
		/*SimpleDateFormat sdf = new SimpleDateFormat(MACHINE_DATE_FORMAT);
		SimpleDateFormat rsdf = new SimpleDateFormat(READABLE_DATE_FORMAT);
		try {
			return rsdf.format(sdf.parse(date));
		} catch (ParseException e) {}
		return "N/A";*/
		if (date == null || date.length() != 8) {
			return date;
		}
		
		return   date.substring(0, 2) + "/" 
		       + date.substring(2, 4) + "/" 
		       + date.substring(4, date.length());
	}
	
	public static Date getCurrentDate() {
		return new Date();
	}
	
	// Refactored to View instead of having in shared
	/*private static final RegExp rfc2822 = RegExp.compile(
	        "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$"
	);
	
	public static boolean isValidEmail(String email) {
		return rfc2822.test(email);
	}*/

	public static boolean isNull(String value) {
		if (CommonUtil.isEmpty(value)) return true;
		if ("NULL".equalsIgnoreCase(value.trim())) return true;
		return false;
	}
	
	private static Map<String,Object> appCache = new HashMap<String,Object>();
	public static Object getAppCacheValue(String key) {
		if (appCache == null) return null;
		return appCache.get(key);
	}
	
	public static void removeAppCacheValue(String key) {
		if (appCache == null) return;
		appCache.remove(key);
	}
	
	public static void putAppCacheValue(String key, Object value) {
		if (appCache == null) return;
		appCache.put(key, value);
	}
	
	public static void clearAppCache() {
		appCache.clear();
	}

	public static String getDownloadUrl(String blobKey) {
		return "/cloudapp/secure/serveFile?a=r&bk="+blobKey;
	}
	
	public static String getPushToImportRequestQueueUrl(String importRequestId) {
		return "/cloudapp/secure/irq?a=q&irId="+importRequestId;
	}
	
	public static String getDownloadDeviceDataUrl(String deviceName) {
		return "/cloudapp/secure/downloadDeviceData?dn="+deviceName;
	}
	
	public String prepareElapsedTimeText(long deltaTicks) {
		int SECOND = 1;
		int MINUTE = 60 * 1;
		int HOUR = MINUTE * 60;
		int DAY = HOUR * 24;
		
		int days = (int) (deltaTicks / DAY);
		int hours = (int)( (deltaTicks - (days*DAY)) / HOUR);  
		int minutes = (int)( (deltaTicks - (days*DAY + hours*HOUR)  ) / MINUTE );
		//int seconds = (int)( (deltaTicks - (days*DAY + hours*HOUR + minutes*MINUTE)  ) / SECOND );
		return days+"d "+hours+"h " + minutes + "m "/* + seconds + "s"*/;
	}
	
	public static String toBeanProperty(String propertyName)
	{
	    if (!isEmpty(propertyName))
	    {
	        return new StringBuilder().append(
	            Character.toLowerCase(propertyName.charAt(0))).append(
	                propertyName.substring(1)).toString();
	    }
	    return propertyName;
	}
}