package ebilly.admin.shared;

import java.util.ArrayList;

import ebilly.admin.shared.viewdef.Filter;

public class ExportRequestUi extends EntityUi {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;
	private String objectName;
	private ArrayList<Filter> filters;
	private int status;
	private String resultFileKey;
	private String errorMessage;
	private String summaryMessage;
	
	public ExportRequestUi() {
		
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public ArrayList<Filter> getFilters() {
		if (filters == null) {
			filters = new ArrayList<Filter>();
		}
		return filters;
	}

	public void addFilter(Filter filter) {
		getFilters().add(filter);
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getResultFileKey() {
		return resultFileKey;
	}

	public void setResultFileKey(String resultFileKey) {
		this.resultFileKey = resultFileKey;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getSummaryMessage() {
		return summaryMessage;
	}

	public void setSummaryMessage(String summaryMessage) {
		this.summaryMessage = summaryMessage;
	}	
}
