package ebilly.admin.shared;

import java.util.ArrayList;
import java.util.List;

import ebilly.admin.shared.viewdef.ResultField;
import ebilly.admin.shared.viewdef.SearchRequest;

public abstract class SummarySearchRequestFactory {
	protected List<ResultField> resultFields = new ArrayList<ResultField>();
	
	public SummarySearchRequestFactory() {
		resultFields.add(new ResultField("GRAND_TOTAL",java.lang.String.class.getName()));
		resultFields.add(new ResultField("TOTAL_PRS",java.lang.String.class.getName()));
		resultFields.add(new ResultField("ARREARS_N_DEMAND",java.lang.String.class.getName()));
		resultFields.add(new ResultField("CMD_COLLECTED",java.lang.String.class.getName()));
		resultFields.add(new ResultField("RC_COLLECTED",java.lang.String.class.getName()));
		resultFields.add(new ResultField("ACD_COLLECTED",java.lang.String.class.getName()));
		resultFields.add(new ResultField("AGL_AMOUNT",java.lang.String.class.getName()));
		resultFields.add(new ResultField("COLLECTION_DATE",java.lang.String.class.getName()));
	}
	
	abstract public SearchRequest createSearchRequest(String dimValue, String monthFilterValue);
	
	public static final SummarySearchRequestFactory companySearchRequestFactory = new CompanySearchRequestFactory();
	public static final SummarySearchRequestFactory circleSearchRequestFactory = new CircleSearchRequestFactory();
	public static final SummarySearchRequestFactory divisionSearchRequestFactory = new DivisionSearchRequestFactory();
	public static final SummarySearchRequestFactory eroSearchRequestFactory = new EroSearchRequestFactory();
	public static final SummarySearchRequestFactory subdivisionSearchRequestFactory = new SubdivisionSearchRequestFactory();
	public static final SummarySearchRequestFactory sectionSearchRequestFactory = new SectionSearchRequestFactory();
	public static final SummarySearchRequestFactory distributionSearchRequestFactory = new DistributionSearchRequestFactory();	
}

class CompanySearchRequestFactory extends SummarySearchRequestFactory {
	
	@Override
	public SearchRequest createSearchRequest(String company, String monthFilterValue) {
		SearchRequest sr = new SearchRequest(AppConfig.OBJ_MONTH_SUMMARY,resultFields);
		sr.addSearchCriteria(AppConfig.F_COMPANY,company,AppConfig.FILTER_OPERATOR_EQUALS,AppConfig.FILTER_CONJUNCTION_AND);
		sr.addSearchCriteria(AppConfig.F_MONTH_FILTER, monthFilterValue);
		return sr;
	}
}

class CircleSearchRequestFactory extends SummarySearchRequestFactory {
	
	public SearchRequest createSearchRequest(String circle, String monthFilterValue) {
		SearchRequest sr = new SearchRequest(AppConfig.OBJ_MONTH_SUMMARY,resultFields);
		sr.addSearchCriteria(AppConfig.F_CIRCLE,circle,AppConfig.FILTER_OPERATOR_EQUALS,AppConfig.FILTER_CONJUNCTION_AND);
		sr.addSearchCriteria(AppConfig.F_MONTH_FILTER, monthFilterValue);
		return sr;
	}
}

class DivisionSearchRequestFactory extends SummarySearchRequestFactory {
	
	public SearchRequest createSearchRequest(String division, String monthFilterValue) {
		String circle = division.substring(0,1);
		
		SearchRequest sr = new SearchRequest(AppConfig.OBJ_MONTH_SUMMARY,resultFields);
		sr.addSearchCriteria(AppConfig.F_CIRCLE,circle,AppConfig.FILTER_OPERATOR_EQUALS,AppConfig.FILTER_CONJUNCTION_AND);
		sr.addSearchCriteria(AppConfig.F_DIVISION,division,AppConfig.FILTER_OPERATOR_EQUALS,AppConfig.FILTER_CONJUNCTION_AND);
		sr.addSearchCriteria(AppConfig.F_MONTH_FILTER, monthFilterValue);
		return sr;
	}
}
	
class EroSearchRequestFactory extends SummarySearchRequestFactory {
	
	public SearchRequest createSearchRequest(String ero, String monthFilterValue) {
		String circle = ero.substring(0,1);
		String division = ero.substring(0,2);
		
		SearchRequest sr = new SearchRequest(AppConfig.OBJ_MONTH_SUMMARY,resultFields);
		sr.addSearchCriteria(AppConfig.F_CIRCLE,circle,AppConfig.FILTER_OPERATOR_EQUALS,AppConfig.FILTER_CONJUNCTION_AND);
		sr.addSearchCriteria(AppConfig.F_DIVISION,division,AppConfig.FILTER_OPERATOR_EQUALS,AppConfig.FILTER_CONJUNCTION_AND);
		sr.addSearchCriteria(AppConfig.F_ERO,ero,AppConfig.FILTER_OPERATOR_EQUALS,AppConfig.FILTER_CONJUNCTION_AND);
		sr.addSearchCriteria(AppConfig.F_MONTH_FILTER, monthFilterValue);
		return sr;
	}
}
	
class SubdivisionSearchRequestFactory extends SummarySearchRequestFactory {
	
	public SearchRequest createSearchRequest(String subdivision, String monthFilterValue) {
		String circle = subdivision.substring(0,1);
		String division = subdivision.substring(0,2);
		String ero = subdivision.substring(0,3);
		
		SearchRequest sr = new SearchRequest(AppConfig.OBJ_MONTH_SUMMARY,resultFields);
		sr.addSearchCriteria(AppConfig.F_CIRCLE,circle,AppConfig.FILTER_OPERATOR_EQUALS,AppConfig.FILTER_CONJUNCTION_AND);
		sr.addSearchCriteria(AppConfig.F_DIVISION,division,AppConfig.FILTER_OPERATOR_EQUALS,AppConfig.FILTER_CONJUNCTION_AND);
		sr.addSearchCriteria(AppConfig.F_ERO,ero,AppConfig.FILTER_OPERATOR_EQUALS,AppConfig.FILTER_CONJUNCTION_AND);
		sr.addSearchCriteria(AppConfig.F_SUBDIVISION,subdivision,AppConfig.FILTER_OPERATOR_EQUALS,AppConfig.FILTER_CONJUNCTION_AND);
		sr.addSearchCriteria(AppConfig.F_MONTH_FILTER, monthFilterValue);
		return sr;
	}
}

class SectionSearchRequestFactory extends SummarySearchRequestFactory {
	
	public SearchRequest createSearchRequest(String section, String monthFilterValue) {
		String circle = section.substring(0,1);
		String division = section.substring(0,2);
		String ero = section.substring(0,3);
		String subdivision = section.substring(0,4);
		
		SearchRequest sr = new SearchRequest(AppConfig.OBJ_MONTH_SUMMARY,resultFields);
		sr.addSearchCriteria(AppConfig.F_CIRCLE,circle,AppConfig.FILTER_OPERATOR_EQUALS,AppConfig.FILTER_CONJUNCTION_AND);
		sr.addSearchCriteria(AppConfig.F_DIVISION,division,AppConfig.FILTER_OPERATOR_EQUALS,AppConfig.FILTER_CONJUNCTION_AND);
		sr.addSearchCriteria(AppConfig.F_ERO,ero,AppConfig.FILTER_OPERATOR_EQUALS,AppConfig.FILTER_CONJUNCTION_AND);
		sr.addSearchCriteria(AppConfig.F_SUBDIVISION,subdivision,AppConfig.FILTER_OPERATOR_EQUALS,AppConfig.FILTER_CONJUNCTION_AND);
		sr.addSearchCriteria(AppConfig.F_SECTION,section,AppConfig.FILTER_OPERATOR_EQUALS,AppConfig.FILTER_CONJUNCTION_AND);
		sr.addSearchCriteria(AppConfig.F_MONTH_FILTER, monthFilterValue);
		return sr;
	}
}

class DistributionSearchRequestFactory extends SummarySearchRequestFactory {
	
	public SearchRequest createSearchRequest(String distribution, String monthFilterValue) {
		String circle = distribution.substring(0,1);
		String division = distribution.substring(0,2);
		String ero = distribution.substring(0,3);
		String subdivision = distribution.substring(0,4);
		String section = distribution.substring(0,5);
		
		SearchRequest sr = new SearchRequest(AppConfig.OBJ_MONTH_SUMMARY,resultFields);
		sr.addSearchCriteria(AppConfig.F_CIRCLE,circle,AppConfig.FILTER_OPERATOR_EQUALS,AppConfig.FILTER_CONJUNCTION_AND);
		sr.addSearchCriteria(AppConfig.F_DIVISION,division,AppConfig.FILTER_OPERATOR_EQUALS,AppConfig.FILTER_CONJUNCTION_AND);
		sr.addSearchCriteria(AppConfig.F_ERO,ero,AppConfig.FILTER_OPERATOR_EQUALS,AppConfig.FILTER_CONJUNCTION_AND);
		sr.addSearchCriteria(AppConfig.F_SUBDIVISION,subdivision,AppConfig.FILTER_OPERATOR_EQUALS,AppConfig.FILTER_CONJUNCTION_AND);
		sr.addSearchCriteria(AppConfig.F_SECTION,section,AppConfig.FILTER_OPERATOR_EQUALS,AppConfig.FILTER_CONJUNCTION_AND);
		sr.addSearchCriteria(AppConfig.F_DISTRIBUTION,distribution,AppConfig.FILTER_OPERATOR_EQUALS,AppConfig.FILTER_CONJUNCTION_AND);
		sr.addSearchCriteria(AppConfig.F_MONTH_FILTER, monthFilterValue);
		return sr;
	}	
}
