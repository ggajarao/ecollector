package ebilly.admin.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class EntityUi implements Serializable, IsSerializable {
	private static final long serialVersionUID = 1L;

	private String entityName;
	private String entityId;
	
	public EntityUi() {
		
	}

	public EntityUi(String entityName, String entityId) {
		this.entityName = entityName;
		this.entityId = entityId;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}	
}
